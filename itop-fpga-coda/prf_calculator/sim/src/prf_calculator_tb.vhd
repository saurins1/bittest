library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity prf_calculator_tb is
  	generic (
          REGISTER_W32    : INTEGER := 32;
          AVG_LEVEL_MIN    : INTEGER := 2;
          AVG_LEVEL_MAX    : INTEGER := 32
  	);	
end;

architecture bench of prf_calculator_tb is

    COMPONENT prf_calculator is
        generic (
            REGISTER_W32            : INTEGER := 32;
            AVG_LEVEL_MIN           : INTEGER := 2;
            AVG_LEVEL_MAX           : INTEGER := 32
        );
        port (
            -- Sync
            aclk         : in STD_LOGIC;
            aresetn      : in STD_LOGIC;
            
            -- Control
            START_INSPECTION  		: in STD_LOGIC;
            ACQUISITION_PROVIDED  	: in STD_LOGIC;
            TRANSMITTER_PRF   	  	: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            AVG_LEVEL   	  	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            
            -- Output
            PRF_RESULT   	  		: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0)
        );
    end COMPONENT;

  signal aclk: std_logic:= '0';
  signal aresetn: std_logic:= '0';
  
  signal START_INSPECTION : std_logic:= '0';
  signal ACQUISITION_PROVIDED : std_logic:= '0';
  signal TRANSMITTER_PRF: std_logic_vector(REGISTER_W32-1 downto 0):= (others => '0');
  signal AVG_LEVEL: std_logic_vector(REGISTER_W32-1 downto 0):= (others => '0');
  
  signal PRF_RESULT: std_logic_vector(REGISTER_W32-1 downto 0);

  constant aclk_period: time := 10 ns;

  signal PRF_TARGET: std_logic_vector(REGISTER_W32-1 downto 0);
  signal start_start_inspection : std_logic:= '0';

begin
	
	-- aclk process 
    aclk_process :process
    begin
        aclk <= not aclk;
        wait for aclk_period/2;
        aclk <= not aclk;
        wait for aclk_period/2;
    end process;
   
  -- Insert values for generic parameters !!
  uut: prf_calculator generic map ( REGISTER_W32             => REGISTER_W32,
                                        AVG_LEVEL_MIN             => AVG_LEVEL_MIN,
                                        AVG_LEVEL_MAX             => AVG_LEVEL_MAX)
                             port map ( aclk                     => aclk,
                                        aresetn                  => aresetn,
                                        START_INSPECTION         => START_INSPECTION,
                                        ACQUISITION_PROVIDED     => ACQUISITION_PROVIDED,
                                        TRANSMITTER_PRF          => TRANSMITTER_PRF,
                                        AVG_LEVEL                => AVG_LEVEL,
                                        PRF_RESULT               => PRF_RESULT
                                        );
                                        
                                        

	stimulus: process
	begin
		aresetn <= '0';
		wait for aclk_period*10;
		aresetn <= '1';
		wait for aclk_period*10;
		-- 100hz
		start_start_inspection <= '1';
		PRF_TARGET <= std_logic_vector(to_unsigned(1000000, PRF_TARGET'length));
		AVG_LEVEL <= std_logic_vector(to_unsigned(1, AVG_LEVEL'length));
		TRANSMITTER_PRF <= std_logic_vector(to_unsigned(1000001, PRF_TARGET'length));
		wait for aclk_period*900000000;
		start_start_inspection <= '0';
		wait;
	end process;
	
	-- Control
    control_prf_process: process(aclk) 
        variable my_counter  : integer range 0 to 900000000 :=0;        
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                ACQUISITION_PROVIDED  <= '0';        
                START_INSPECTION <= '0';   
                my_counter := 0;                        
            else
                if(start_start_inspection = '1') then
                    START_INSPECTION <= '1'; 
                    if(my_counter >= (unsigned(PRF_TARGET)-4)) then
                        ACQUISITION_PROVIDED  <= '1';
                        my_counter := 0;
                    else
                        ACQUISITION_PROVIDED  <= '0';
                        my_counter := my_counter + 1;
                    end if;
                else
                    my_counter := 0;
                    START_INSPECTION <= '0'; 
                    ACQUISITION_PROVIDED  <= '0';
                end if;                                     
            end if;
        end if;
    end process;
	  																																					
end;