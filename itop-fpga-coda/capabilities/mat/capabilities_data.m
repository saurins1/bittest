clear all;close all;

ram_size = 1024;
capabilities_size = 180;

% CAPABILITIES 
%capabilities = xlsread('capabilities.xlsx','180:1');
capabilities = xlsread('capabilities_tgil.xlsx','180:1');
capabilities = [capabilities(:,2)' zeros(1,ram_size-capabilities_size)];
capabilities = capabilities';

%capabilities_bin = dec2bin(abs(capabilities),32);
capabilities_bin = dec2bin(typecast(int32(capabilities),'uint32'),32);
dlmwrite('capabilities.txt',capabilities_bin,'delimiter','');
