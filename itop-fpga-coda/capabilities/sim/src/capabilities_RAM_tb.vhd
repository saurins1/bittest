library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity capabilities_RAM_tb is
    generic (
        ADDR_LENGTH    : INTEGER := 10;
        REGISTER_W32   : INTEGER := 32
    );
end;

architecture bench of capabilities_RAM_tb is

  component capabilities_RAM
  	  generic (
  	      ADDR_LENGTH    : INTEGER := 10;
          REGISTER_W32   : INTEGER := 32
      );
      port(
          aclk     : IN STD_LOGIC;
          en      : IN STD_LOGIC;
          we      : IN STD_LOGIC;
          addr    : IN STD_LOGIC_VECTOR(ADDR_LENGTH-1 downto 0);
          din     : IN STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
          dout    : OUT STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0)
      );
  end component;

  signal aclk: STD_LOGIC:= '0';
  signal aresetn: STD_LOGIC:= '0';
  signal en: STD_LOGIC:= '0';
  signal we: STD_LOGIC:= '0';
  signal addr: STD_LOGIC_VECTOR(ADDR_LENGTH-1 downto 0):= (others => '0');
  signal din: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');
  signal dout: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0) ;

  constant aclk_period: time := 10 ns;
  
  signal addr_first: STD_LOGIC;

begin

	-- aclk process 
    clk_process :process
    begin
        aclk <= not aclk;
        wait for aclk_period/2;
        aclk <= not aclk;
        wait for aclk_period/2;
    end process;

  -- Insert values for generic parameters !!
  uut: capabilities_RAM generic map ( ADDR_LENGTH  => ADDR_LENGTH,
                                      REGISTER_W32 => REGISTER_W32 )
                           port map ( aclk          => aclk,
                                      en           => en,
                                      we           => we,
                                      addr         => addr,
                                      din          => din,
                                      dout         => dout );

	stimulus: process
	begin
		aresetn <= '0';
		wait for aclk_period*10;
		aresetn <= '1';
		wait;
	end process;

    read_bram_inst: process(aclk, aresetn)  
        variable my_counter  : integer range 0 to 16383 :=0;
    begin 
        if (aresetn = '0') then
			en <= '0';
			we <= '0';
			addr <= (others => '0');
			din <= (others => '0');
			addr_first <= '1';
        elsif (aclk'event and aclk = '1') then
            en <= '1';
            if(addr_first = '1') then
                addr <= (others => '0');
                addr_first <= '0';
            else
                if(unsigned(addr) >= 179) then
                    addr <= (others => '0');
                else
                    addr <= std_logic_vector(unsigned(addr)+1);
                end if;
            end if;                                                                                                     
        end if;
    end process; 

end;