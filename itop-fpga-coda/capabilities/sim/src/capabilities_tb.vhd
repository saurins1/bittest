library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity capabilities_tb is
  	generic (
        REGISTER_W32            : INTEGER := 32;
        BRAM_TREADY_SYNC        : INTEGER := 3;
        WAIT_FOR_BRAM           : INTEGER := 16
    );
end;

architecture bench of capabilities_tb is

  component capabilities
  	generic (
            REGISTER_W32            : INTEGER := 32;
  		    BRAM_TREADY_SYNC        : INTEGER := 3;
  		    WAIT_FOR_BRAM           : INTEGER := 16
  	);
  	port (
  	        aclk         : in STD_LOGIC;
            aresetn      : in STD_LOGIC;
            READ_CAPABILITIES  : in STD_LOGIC;
            CAPABILITIES_READY : out STD_LOGIC;
  		    CAPABILITIES_SIZE  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            CAPABILITIES_AXIS_TREADY  : in STD_LOGIC;
            CAPABILITIES_AXIS_TDATA   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            CAPABILITIES_AXIS_TLAST   : out STD_LOGIC;
            CAPABILITIES_AXIS_TVALID  : out STD_LOGIC;
            CAPABILITIES_AXIS_TKEEP   : out std_logic_vector((REGISTER_W32/8)-1 downto 0)
  	);
  end component;

  signal aclk: STD_LOGIC:= '0';
  signal aresetn: STD_LOGIC:= '0';
  
  signal READ_CAPABILITIES: STD_LOGIC := '0';
  signal CAPABILITIES_READY: STD_LOGIC;
  signal CAPABILITIES_SIZE: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal CAPABILITIES_AXIS_TREADY: STD_LOGIC := '0';
  signal CAPABILITIES_AXIS_TDATA: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal CAPABILITIES_AXIS_TLAST: STD_LOGIC;
  signal CAPABILITIES_AXIS_TVALID: STD_LOGIC;
  signal CAPABILITIES_AXIS_TKEEP: std_logic_vector((REGISTER_W32/8)-1 downto 0) ;

  constant aclk_period: time := 10 ns;
  signal start_sm: STD_LOGIC:= '0';
  
  type states_capabilities_sm is (idle, read_on, read_off); 
  signal state_capabilities_sm : states_capabilities_sm; 
  
  signal capabilities_counter: UNSIGNED(REGISTER_W32-1 downto 0):= (others => '0');
  signal tready_const: UNSIGNED(REGISTER_W32-1 downto 0):= (others => '0');
  signal tready_pwm_on: STD_LOGIC;

begin

	-- aclk process 
    aclk_process :process
    begin
        aclk <= not aclk;
        wait for aclk_period/2;
        aclk <= not aclk;
        wait for aclk_period/2;
    end process;

  -- Insert values for generic parameters !!
  uut: capabilities generic map ( REGISTER_W32             => REGISTER_W32,
                                  BRAM_TREADY_SYNC         => BRAM_TREADY_SYNC,
                                  WAIT_FOR_BRAM            => WAIT_FOR_BRAM )
                       port map ( aclk                     => aclk,
                                  aresetn                  => aresetn,
                                  READ_CAPABILITIES        => READ_CAPABILITIES,
                                  CAPABILITIES_READY       => CAPABILITIES_READY,
                                  CAPABILITIES_SIZE        => CAPABILITIES_SIZE,
                                  CAPABILITIES_AXIS_TREADY => CAPABILITIES_AXIS_TREADY,
                                  CAPABILITIES_AXIS_TDATA  => CAPABILITIES_AXIS_TDATA,
                                  CAPABILITIES_AXIS_TLAST  => CAPABILITIES_AXIS_TLAST,
                                  CAPABILITIES_AXIS_TVALID => CAPABILITIES_AXIS_TVALID,
                                  CAPABILITIES_AXIS_TKEEP  => CAPABILITIES_AXIS_TKEEP );

	stimulus: process
	begin
		aresetn <= '0';
		wait for aclk_period*10;
		aresetn <= '1';
		wait;
	end process;

    read_capabilities_inst: process(aclk, aresetn)  
        variable my_counter  : integer range 0 to 16383 :=0;
        variable tready_counter  : integer range 0 to 16383 :=0;
    begin 
        if (aresetn = '0') then
			READ_CAPABILITIES <= '0';
			capabilities_counter <= (others => '0');
			CAPABILITIES_AXIS_TREADY <= '0';
			tready_const <= to_unsigned(8, tready_const'length);
			tready_pwm_on <= '0';	
			--tready_pwm_on <= '1'; 
			my_counter := 0;														  	
            state_capabilities_sm <= idle;  
        elsif (aclk'event and aclk = '1') then
            case state_capabilities_sm is  
                when idle =>       
                    CAPABILITIES_AXIS_TREADY <= '0';  
                    if(my_counter >= 32) then
                        if(CAPABILITIES_READY = '1') then
                            my_counter := 0;
                            tready_counter := 0;
                            READ_CAPABILITIES <= '1';
                            state_capabilities_sm <= read_on;
                        end if;
                    else
                        my_counter := my_counter + 1;
                    end if;
                when read_on =>               
                    if(tready_pwm_on = '1') then
                        if(tready_counter >= tready_const) then
                            CAPABILITIES_AXIS_TREADY <= not CAPABILITIES_AXIS_TREADY;
                            tready_counter := 0;
                        else
                            tready_counter := tready_counter + 1;
                        end if; 
                    else
                        CAPABILITIES_AXIS_TREADY <= '1';
                    end if;
                    if(CAPABILITIES_AXIS_TLAST = '1') then
                        state_capabilities_sm <= read_off;
                    end if;
                when read_off =>
				    READ_CAPABILITIES <= '0';
				    state_capabilities_sm <= idle;													
                when others =>
                    null;                   
            end case;                                                                                            
        end if;
    end process; 
    
end;