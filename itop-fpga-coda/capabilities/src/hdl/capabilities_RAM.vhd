----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 12/12/2017 02:34:04 PM
-- Design Name: 
-- Module Name: capabilities_RAM - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use std.textio.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity capabilities_RAM is
	generic (
	    ADDR_LENGTH    : INTEGER := 10;
        REGISTER_W32   : INTEGER := 32
    );
    port(
        aclk     : IN STD_LOGIC;
        en      : IN STD_LOGIC;
        we      : IN STD_LOGIC;
        addr    : IN STD_LOGIC_VECTOR(ADDR_LENGTH-1 downto 0);
        din     : IN STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        dout    : OUT STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0)
    );
end capabilities_RAM;

architecture Behavioral of capabilities_RAM is

    type ram_type is array(0 to 2**ADDR_LENGTH-1) of BIT_VECTOR(REGISTER_W32-1 downto 0);
    
    impure function initRamFromFile (ram_file_name : in string) return ram_type is
        FILE ram_file: text is in ram_file_name;
        variable ram_file_line : line;
        variable RAM: ram_type;      
    begin
        for i in ram_type'range loop
            readline (ram_file, ram_file_line);
            read (ram_file_line, RAM(i));
        end loop;
        return RAM;
    end function;
    
    signal RAM : ram_type := initRamFromFile("capabilities.txt");

begin

    process (aclk)
    begin
        if aclk'event and aclk = '1' then
            if(en = '1') then
                if (we = '1') then
                    RAM(conv_integer(addr)) <= to_bitvector(din);
                end if;
                dout <= to_stdlogicvector(RAM(conv_integer(addr)));
            end if;
        end if;
    end process;

end Behavioral;
