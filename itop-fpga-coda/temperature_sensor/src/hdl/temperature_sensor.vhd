----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 01/09/2017 10:07:23 AM
-- Design Name: 
-- Module Name: temperature sensor - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity temperature_sensor is
	generic (
		REGISTER_W32       : INTEGER := 32;
	    REGISTER_W16       : INTEGER := 16;
	    --TEMP_MEASURE_CYCLE : INTEGER := 60; -- in secs 
	    --TEMP_MEASURE_CYCLE : INTEGER := 5; -- in secs 
	    TEMP_MEASURE_CYCLE : INTEGER := 1; -- in secs 
		TEMP_WORD_WIDTH    : INTEGER := 16;
		TEMP_RESULT_WIDTH  : INTEGER := 12;
		SPI_CS_SCK_CYCLES  : INTEGER := 2;
		SPI_CLK_DIV        : INTEGER := 100;
		--THERMO_NOT_CONNECTED : INTEGER := 4096;   
		THERMO_NOT_CONNECTED : INTEGER := -1;  
		TEMP_AVG_MAX       : INTEGER := 32
	);
	port (
	    -- Sync
	    aclk         : in STD_LOGIC;
        aresetn      : in STD_LOGIC;
        
        TEMPERATURE_AVG   : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);	     
		TEMPERATURE       : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
        TEMP_CSn      : out STD_LOGIC;
        TEMP_SCK      : out STD_LOGIC;
        TEMP_SDO      : in STD_LOGIC      
	);
end temperature_sensor;

architecture arch_imp of temperature_sensor is
   	
	-- SPI
	signal TEMP_CSn_i   : STD_LOGIC;
	signal TEMP_SCK_i   : STD_LOGIC;
	signal TEMP_SDO_i   : STD_LOGIC;

	signal TEMP_SCK_i_d1 : STD_LOGIC;
	   		
    -- Read Temp SM
    type states_temp_sm is (idle, read_temperature_start, read_temperature); 
    signal state_temp_sm : states_temp_sm;

	signal temperature_i   		: STD_LOGIC_VECTOR(TEMP_WORD_WIDTH-1 downto 0);
	signal read_temperature_on  : STD_LOGIC;

    signal usec_counter : INTEGER range 0 to 100000000;
    signal sec_counter : INTEGER range 0 to 100000000;
    constant tics_100mhz_1sec : INTEGER range 0 to 200000000 := 100000000;
    --constant tics_100mhz_1sec : INTEGER range 0 to 200000000 := 1000000;
    
    signal temperature_result_i : STD_LOGIC_VECTOR(TEMP_RESULT_WIDTH downto 0);
    --signal temperature_result_i : STD_LOGIC_VECTOR(TEMP_RESULT_WIDTH+10 downto 0);
    signal temperature_result_flag  : STD_LOGIC;
    
    -- AVG
    signal temperature_avg_i : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal temperature_avg_d1 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal temperature_avg_clear  : STD_LOGIC;
    signal temperature_avg_clear_ack  : STD_LOGIC;
    
    signal temperature_avg_counter : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal temperature_avg_counter_wr : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal temperature_avg_counter_rd : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal temperature_acc : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal temperature_avg_rd_first  : STD_LOGIC;
    
    signal temperature_divide: integer range 0 to 1024 :=0;
    
    type TEMP_MATRIX_TYPE is array ( 0 to TEMP_AVG_MAX-1 ) of STD_LOGIC_VECTOR(TEMP_RESULT_WIDTH downto 0);
    --type TEMP_MATRIX_TYPE is array ( 0 to TEMP_AVG_MAX-1 ) of STD_LOGIC_VECTOR(TEMP_RESULT_WIDTH+10 downto 0);
    signal temp_matrix : TEMP_MATRIX_TYPE;

    -- SM
    type states_generate_spi_sm is (idle, csn_on, sck_on, csn_off, spi_end); 
    signal state_generate_spi_sm : states_generate_spi_sm;
    
    type states_temp_avg_sm is (idle, temp_acc, temp_substract, temp_divide, temp_clear, temp_end); 
    signal state_temp_avg_sm : states_temp_avg_sm;
         
begin

	-- Temp AVG
    temp_avg_sm: process(aclk, aresetn) 
		variable avg_counter  : integer range 0 to 65536 :=0; 
		variable temp_matrix_index_wr  : integer range 0 to 65536 :=0; 
		variable temp_matrix_index_rd  : integer range 0 to 65536 :=0; 
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				temperature_avg_i   <= (others => '0');
				temperature_acc <= (others => '0');
				temperature_avg_counter <= (others => '0');
				temperature_avg_counter_wr <= (others => '0');
				temperature_avg_counter_rd <= (others => '0');
				temperature_avg_rd_first <= '1';
				avg_counter := 0;
				temp_matrix_index_wr := 0;
				temp_matrix_index_rd := 0;
				temperature_avg_clear <= '0';
				temperature_avg_clear_ack <= '0';
				--TEMPERATURE   <= (others => '0');
				temperature_divide <= 0;
				-- state
				state_temp_avg_sm <= idle;
        	else
        	    temperature_avg_d1 <= TEMPERATURE_AVG;
                if(temperature_avg_d1 /= TEMPERATURE_AVG) then
                    temperature_avg_clear <= '1';
                else
                    if(temperature_avg_clear_ack = temperature_avg_clear) then
                        temperature_avg_clear <= '0';
                    end if;
                end if;
        	    case state_temp_avg_sm is
        	       when idle =>
        	           if(temperature_avg_clear = '1') then
        	               state_temp_avg_sm <= temp_clear;  
        	           else
                           if(temperature_result_flag = '1') then                            
                               if(unsigned(TEMPERATURE_AVG) >= 2 and unsigned(TEMPERATURE_AVG) < 3) then
                                   temperature_avg_i <= std_logic_vector(to_unsigned(2, temperature_avg_i'length));
                                   temperature_divide <= 1;
                               elsif(unsigned(TEMPERATURE_AVG) >= 4 and unsigned(TEMPERATURE_AVG) < 8) then
                                   temperature_avg_i <= std_logic_vector(to_unsigned(4, temperature_avg_i'length));
                                   temperature_divide <= 2;
                               elsif(unsigned(TEMPERATURE_AVG) >= 8 and unsigned(TEMPERATURE_AVG) < 16) then
                                   temperature_avg_i <= std_logic_vector(to_unsigned(8, temperature_avg_i'length));
                                   temperature_divide <= 3;
                               elsif(unsigned(TEMPERATURE_AVG) >= 16 and unsigned(TEMPERATURE_AVG) < 32) then
                                   temperature_avg_i <= std_logic_vector(to_unsigned(16, temperature_avg_i'length));
                                   temperature_divide <= 4;
                               elsif(unsigned(TEMPERATURE_AVG) >= 32) then
                                   temperature_avg_i <= std_logic_vector(to_unsigned(32, temperature_avg_i'length));
                                   temperature_divide <= 5;
                               else
                                   temperature_avg_i <= std_logic_vector(to_unsigned(1, temperature_avg_i'length));
                                   temperature_divide <= 1;
                               end if;
                               if(unsigned(TEMPERATURE_AVG) > 1 and signed(temperature_result_i) /= THERMO_NOT_CONNECTED) then
                                   state_temp_avg_sm <= temp_acc;   
                               else
                                   state_temp_avg_sm <= temp_clear;  
                               end if;
                           end if;
                       end if;                 
        	       when temp_acc =>
        	           if(temperature_avg_clear = '1') then
        	               state_temp_avg_sm <= temp_clear;  
        	           else
        	               temperature_acc <= std_logic_vector(signed(temperature_acc)+resize(signed(temperature_result_i), temperature_acc'length));
        	               state_temp_avg_sm <= temp_substract;  
        	           end if;        	        	            
        	       when temp_substract =>
                        temp_matrix(temp_matrix_index_wr) <= temperature_result_i;   
                        if(avg_counter < unsigned(temperature_avg_i)) then
                            temperature_avg_counter <= std_logic_vector(unsigned(temperature_avg_counter)+1); 
                            avg_counter := avg_counter+1; 
                        else
                            -- Substract
                            temperature_acc <= std_logic_vector(signed(temperature_acc)-resize(signed(temp_matrix(temp_matrix_index_rd)), temperature_acc'length)); 
                        end if;
                        state_temp_avg_sm <= temp_divide;
        	       when temp_divide => 
        	           if(avg_counter = unsigned(temperature_avg_i)) then
                           TEMPERATURE <= std_logic_vector(resize(signed(temperature_acc(temperature_divide+TEMP_RESULT_WIDTH downto temperature_divide)), TEMPERATURE'length)); 
                           --TEMPERATURE <= std_logic_vector(resize(signed(temperature_acc(temperature_divide+TEMP_RESULT_WIDTH+10 downto temperature_divide)), TEMPERATURE'length)); 
        	               if(temp_matrix_index_rd = (unsigned(temperature_avg_i)-1)) then
                               temp_matrix_index_rd := 0;
                               temperature_avg_counter_rd <= (others => '0');
                           else
                               if(temperature_avg_rd_first = '1') then
                                   temperature_avg_rd_first <= '0';
                                   temp_matrix_index_rd := 0;
                                   temperature_avg_counter_rd <= (others => '0');
                               else
                                   temp_matrix_index_rd := temp_matrix_index_rd + 1;
                                   temperature_avg_counter_rd <= std_logic_vector(unsigned(temperature_avg_counter_rd)+1); 
                               end if;    
                           end if;  
        	           else
        	               if(avg_counter = 1) then
                               TEMPERATURE <= std_logic_vector(resize(signed(temperature_result_i), TEMPERATURE'length));
                           end if;
        	           end if;  
        	           if(temp_matrix_index_wr = (unsigned(temperature_avg_i)-1)) then
        	               temp_matrix_index_wr := 0;
        	               temperature_avg_counter_wr <= (others => '0');
        	           else
        	               temp_matrix_index_wr := temp_matrix_index_wr + 1;
        	               temperature_avg_counter_wr <= std_logic_vector(unsigned(temperature_avg_counter_wr)+1); 
        	           end if;
        	           state_temp_avg_sm <= temp_end;     
        	       when temp_clear =>
        	           TEMPERATURE <= std_logic_vector(resize(signed(temperature_result_i), TEMPERATURE'length));
                       temperature_avg_counter <= (others => '0');
                       avg_counter := 0;
				       temp_matrix_index_wr := 0;
                       temp_matrix_index_rd := 0;
                       temperature_avg_counter_rd <= (others => '0');
                       temperature_avg_counter_wr <= (others => '0');
                       temperature_avg_rd_first <= '1';
                       temperature_acc <= (others => '0'); 
                       temperature_avg_clear_ack <= temperature_avg_clear;
                       if(temperature_avg_clear = '0') then
                           state_temp_avg_sm <= temp_end; 
                       end if;
        	       when temp_end =>
        	           if(temperature_result_flag = '0') then
        	               state_temp_avg_sm <= idle;
        	           end if;
				   when others =>
                       null;
               end case;
           end if;
        end if;
   end process;
        	             								
	-- Read Temp SM
    temp_sm: process(aclk, aresetn) 
		variable temp_counter  : integer range 0 to 65536 :=0; 
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				-- counters
				usec_counter <= 0;
				sec_counter <= TEMP_MEASURE_CYCLE;
				-- temperature result
				--TEMPERATURE   <= (others => '0');
				temperature_i <= (others => '0');
				TEMP_SCK_i_d1 <= '0';
				temperature_result_i <= (others => '0');
				temperature_result_flag <= '0';
				read_temperature_on <= '0';
				-- state
				state_temp_sm <= idle;
        	else			
				TEMP_SCK_i_d1 <= TEMP_SCK_i;
				case state_temp_sm is
					when idle =>
						read_temperature_on <= '0';
						temperature_result_flag <= '0';
						if(usec_counter >= tics_100mhz_1sec) then
							if(sec_counter >= (TEMP_MEASURE_CYCLE-1)) then
								state_temp_sm <= read_temperature_start;	
							else
								sec_counter <= sec_counter + 1;	
							end if;
						else
							usec_counter <= usec_counter + 1;
						end if;
					when read_temperature_start =>
			  			read_temperature_on <= '1';
						if(TEMP_CSn_i = '0') then
							temp_counter  := 1;
							state_temp_sm <= read_temperature;		
						end if;
			  		when read_temperature =>
						if(TEMP_CSn_i = '0') then
							if(TEMP_SCK_i = '1' and TEMP_SCK_i_d1 = '0') then
								if(temp_counter <= TEMP_WORD_WIDTH) then
									temperature_i(TEMP_WORD_WIDTH-temp_counter) <= TEMP_SDO_i;
									temp_counter := temp_counter + 1;
								end if;
							end if;
						else
							usec_counter <= 0;
							sec_counter <= 0;
							temperature_result_flag <= '1';
							if(temperature_i(2) = '0') then
							    -- thermocouple conected
							    --TEMPERATURE <= "00000000000000000000" & temperature_i(14 downto 3);
							    temperature_result_i <= "0" & temperature_i(14 downto 3); 
							    --temperature_result_i <= "0" & temperature_i(14 downto 3)&"0000000000"; 
							else
							    -- thermocouple not conected
							    --TEMPERATURE <= std_logic_vector(to_unsigned(THERMO_NOT_CONNECTED, TEMPERATURE'length));
							    temperature_result_i <= std_logic_vector(to_signed(THERMO_NOT_CONNECTED, temperature_result_i'length));
							end if;
							state_temp_sm <= idle;		
						end if;														  
					when others =>
						null;
				end case;
			end if;
         end if;
    end process;
			 
	-- Generate CSn and SCK
    spi_sm: process(aclk, aresetn) 
		variable spi_counter  : integer range 0 to 16384 :=0; 
		variable sck_counter  : integer range 0 to 16384 :=0;
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				TEMP_SCK_i <= '0';
				TEMP_CSn_i <= '1';
				-- state
				state_generate_spi_sm <= idle;
        	else			
				case state_generate_spi_sm is
					when idle =>
						TEMP_SCK_i <= '0';
						TEMP_CSn_i <= '1';
						spi_counter := 0;
						if(read_temperature_on = '1') then
							state_generate_spi_sm <= csn_on;	
						end if;
					when csn_on =>
			  			TEMP_CSn_i <= '0';
						if(spi_counter >= SPI_CLK_DIV*SPI_CS_SCK_CYCLES) then
							spi_counter := 0;
							sck_counter := 0;
							state_generate_spi_sm <= sck_on;
						else
							spi_counter := spi_counter + 1;	
						end if;
			  		when sck_on =>
						if(spi_counter >= SPI_CLK_DIV/2) then
							spi_counter := 0;							
							if(sck_counter >= (TEMP_WORD_WIDTH*2)) then
								state_generate_spi_sm <= csn_off;	
							else
							    TEMP_SCK_i <= not TEMP_SCK_i;
								sck_counter := sck_counter + 1;	
							end if;
						else
							spi_counter := spi_counter + 1;
						end if;	
					when csn_off =>
						if(spi_counter >= SPI_CLK_DIV*SPI_CS_SCK_CYCLES) then
							TEMP_CSn_i <= '1';
							state_generate_spi_sm <= spi_end;
						else
							spi_counter := spi_counter + 1;	
						end if;
					when spi_end =>
						if(read_temperature_on = '0') then
							state_generate_spi_sm <= idle;	
						end if;
					when others =>
						null;
				end case;
			end if;
         end if;
    end process;
			 	  																					   
	TEMP_CSn_inst   : OBUF 
		generic map (
			IOSTANDARD=>"LVCMOS25", 
			DRIVE=>8, 
			SLEW=>"SLOW"
		)
		port map ( 
			I => TEMP_CSn_i, 
			O=> TEMP_CSn 
		);
																					   
	TEMP_SCK_inst   : OBUF 
		generic map (
			IOSTANDARD=>"LVCMOS25", 
			DRIVE=>8, 
			SLEW=>"SLOW"
		)
		port map ( 
			I => TEMP_SCK_i, 
			O=> TEMP_SCK 
		);																		   
      
--	TEMP_SDO_inst   : IBUF 
--		generic map (
--			IOSTANDARD=>"LVCMOS25"
--		)
--		port map ( 
--			I => TEMP_SDO, 
--			O=> TEMP_SDO_i 
--		);

    TEMP_SDO_i <= TEMP_SDO;
																					   					     					      									 	   			                 
end arch_imp;
