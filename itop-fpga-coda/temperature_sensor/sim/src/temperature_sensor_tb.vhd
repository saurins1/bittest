library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity temperature_sensor_tb is
	generic (
        REGISTER_W32       : INTEGER := 32;
        REGISTER_W16       : INTEGER := 16;
        TEMP_MEASURE_CYCLE : INTEGER := 1; -- in secs 
        TEMP_WORD_WIDTH    : INTEGER := 16;
        TEMP_RESULT_WIDTH  : INTEGER := 12;
        SPI_CS_SCK_CYCLES  : INTEGER := 2;
        SPI_CLK_DIV        : INTEGER := 100;
		--THERMO_NOT_CONNECTED : INTEGER := 4096;   
        THERMO_NOT_CONNECTED : INTEGER := -1;  
        TEMP_AVG_MAX       : INTEGER := 32
    );
end;

architecture bench of temperature_sensor_tb is

  component temperature_sensor
	generic (
      REGISTER_W32       : INTEGER := 32;
      REGISTER_W16       : INTEGER := 16;
      TEMP_MEASURE_CYCLE : INTEGER := 1; -- in secs 
      TEMP_WORD_WIDTH    : INTEGER := 16;
      TEMP_RESULT_WIDTH  : INTEGER := 12;
      SPI_CS_SCK_CYCLES  : INTEGER := 2;
      SPI_CLK_DIV        : INTEGER := 100;
		--THERMO_NOT_CONNECTED : INTEGER := 4096;   
      THERMO_NOT_CONNECTED : INTEGER := -1;  
      TEMP_AVG_MAX       : INTEGER := 32
  );
  	port (
  	    aclk         : in STD_LOGIC;
          aresetn      : in STD_LOGIC;
          TEMPERATURE_AVG   : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);	
  		TEMPERATURE   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
          TEMP_CSn      : out STD_LOGIC;
          TEMP_SCK      : out STD_LOGIC;
          TEMP_SDO      : in STD_LOGIC      
  	);
  end component;

  signal aclk: std_logic:= '0';
  signal aresetn: std_logic:= '0';

  signal TEMPERATURE_AVG: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):=(others => '0');
  signal TEMPERATURE: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

  signal TEMP_CSn: STD_LOGIC;
  signal TEMP_SCK: STD_LOGIC;
  signal TEMP_SDO: STD_LOGIC:= '0';

  constant aclk_period: time := 10 ns;

  signal temperature_i: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  signal temperature_counter: UNSIGNED(REGISTER_W16-1 downto 0);

  signal TEMP_SCK_d1: STD_LOGIC;
  signal TEMP_CSn_d1: STD_LOGIC;

begin
	
	temperature_i <= "0" & std_logic_vector(temperature_counter(11 downto 0)) & "000";
	
    update_SDO_inst: process(aclk, aresetn)
		variable sck_counter  : integer range 0 to 15383 :=0; 
    begin 
		if (rising_edge(aclk)) then
            if (aresetn = '0') then
   				TEMP_SDO <= '0';
				temperature_counter <= (others => '0');
				TEMP_SCK_d1 <= '0';
				TEMP_CSn_d1 <= '0';
				sck_counter := 0;
            else
				TEMP_SCK_d1 <= TEMP_SCK;
				TEMP_CSn_d1 <= TEMP_CSn;
				if(TEMP_CSn = '0') then
				    TEMP_SDO <= temperature_i(REGISTER_W16-1-sck_counter);
					if(TEMP_SCK = '1' and TEMP_SCK_d1 = '0') then
					    if(sck_counter < 15) then
						    sck_counter := sck_counter + 1;	
					    end if;
					end if;
				else
					if(TEMP_CSn = '1' and TEMP_CSn_d1 = '0') then
						temperature_counter <= temperature_counter + 1;
					end if;
					sck_counter := 0;	
				end if;
            end if;
        end if;
    end process;
	
	stimulus: process
	begin
		aresetn <= '0';
		TEMPERATURE_AVG  <= std_logic_vector(to_unsigned(8, TEMPERATURE_AVG'length));
		--TEMPERATURE_AVG  <= std_logic_vector(to_unsigned(32, TEMPERATURE_AVG'length));
		wait for aclk_period*10;
		aresetn <= '1';
		wait;
	end process;
	
	-- aclk process 
    aclk_process :process
    begin
        aclk <= not aclk;
        wait for aclk_period/2;
        aclk <= not aclk;
        wait for aclk_period/2;
    end process;	

  -- Insert values for generic parameters !!
  uut: temperature_sensor generic map ( REGISTER_W32       => REGISTER_W32,
                                        REGISTER_W16       => REGISTER_W16,
                                        TEMP_MEASURE_CYCLE => TEMP_MEASURE_CYCLE,
                                        TEMP_WORD_WIDTH    => TEMP_WORD_WIDTH,
                                        TEMP_RESULT_WIDTH  => TEMP_RESULT_WIDTH,
                                        SPI_CS_SCK_CYCLES  => SPI_CS_SCK_CYCLES,
                                        THERMO_NOT_CONNECTED => THERMO_NOT_CONNECTED,
                                        TEMP_AVG_MAX       => TEMP_AVG_MAX,
                                        SPI_CLK_DIV        => SPI_CLK_DIV )
                             port map ( aclk               => aclk,
                                        aresetn            => aresetn,
                                        TEMPERATURE_AVG    => TEMPERATURE_AVG,
                                        TEMPERATURE        => TEMPERATURE,
                                        TEMP_CSn           => TEMP_CSn,
                                        TEMP_SCK           => TEMP_SCK,
                                        TEMP_SDO           => TEMP_SDO );


end;