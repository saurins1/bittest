library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity clk_wiz_0_tb is
end;

architecture bench of clk_wiz_0_tb is

    component clk_wiz_0
    port
     (-- Clock in ports
      -- Clock out ports
      clk_out1          : out    std_logic;
      clk_out2          : out    std_logic;
      clk_out3          : out    std_logic;
      clk_out4          : out    std_logic;
      -- Status and control signals
      reset             : in     std_logic;
      locked            : out    std_logic;
      clk_in1           : in     std_logic
     );
    end component;

  signal aclk: std_logic:= '0';
  signal aresetn: std_logic:= '0';
  
  signal areset: std_logic;

  signal clk_out1: STD_LOGIC;
  signal clk_out2: STD_LOGIC;
  signal clk_out3: STD_LOGIC;
  signal clk_out4: STD_LOGIC;
  signal locked: STD_LOGIC;
  
  constant aclk_period: time := 10 ns;

begin
	
	stimulus: process
	begin
		--aresetn <= '1';
		aresetn <= '0';
		wait for aclk_period*100;
		--aresetn <= '0';
		aresetn <= '1';
		wait;
	end process;
	
	-- aclk process 
    aclk_process :process
    begin
        aclk <= not aclk;
        wait for aclk_period/2;
        aclk <= not aclk;
        wait for aclk_period/2;
    end process;	
    
    areset <= not aresetn;

  -- Insert values for generic parameters !!
  uut: clk_wiz_0 
       port map ( 
         -- Clock out ports  
        clk_out1 => clk_out1,
        clk_out2 => clk_out2,
        clk_out3 => clk_out3,
        clk_out4 => clk_out4,
        -- Status and control signals                
        --reset => aresetn,
        reset => areset,
        locked => locked,
        -- Clock in ports
        clk_in1 => aclk
        );

end;