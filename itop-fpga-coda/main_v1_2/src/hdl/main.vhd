----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/14/2017 11:51:43 AM
-- Design Name: 
-- Module Name: VOLTA_main - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity main is
	generic (
		-- General
		N_CHANNELS		: INTEGER := 1;
		REGISTER_W64    : INTEGER := 64;
		REGISTER_W48    : INTEGER := 48;
	    REGISTER_W32    : INTEGER := 32;
	    REGISTER_W21    : INTEGER := 21;
        REGISTER_W16    : INTEGER := 16;
		REGISTER_W14    : INTEGER := 14;
		REGISTER_W8     : INTEGER := 8;
		REGISTER_W6     : INTEGER := 6;
		REGISTER_W4     : INTEGER := 4;
		REGISTER_W2     : INTEGER := 2;
		
		-- Scan mode
        SCAN_MODE_NORMAL           : INTEGER := 0;
        SCAN_MODE_MRUT             : INTEGER := 10;
        SCAN_MODE_MRUT_PITCH_CATH  : INTEGER := 11;
        SCAN_MODE_MRUT_DIR         : INTEGER := 12;
        
        -- Trigger type
        TRIGGER_TYPE_NORMAL     : INTEGER := 0;
        TRIGGER_TYPE_SW         : INTEGER := 10;
        TRIGGER_TYPE_SW_PWM     : INTEGER := 11;
        TRIGGER_TYPE_ENC        : INTEGER := 20;
        TRIGGER_TYPE_ROBOT      : INTEGER := 30;
        TRIGGER_TYPE_ROBOT_ENC  : INTEGER := 40;
		
		-- external trigger
		DISPOSITION_PASS  : INTEGER := 1;
		DISPOSITION_FAIL  : INTEGER := 2;
		DISPOSITION_NODET : INTEGER := 0;
		
		-- Coinc
        TRIGGER_WAIT    : INTEGER := 32;
        MIN_COINC_LEVEL : INTEGER := 2;
		
		-- Avg
        MIN_AVG_LEVEL       : INTEGER := 2;
        BRAM_ADDR_WIDTH     : INTEGER := 16;
        BRAM_DATA_WIDTH     : INTEGER := 23;
        AVG_TYPE_NORMAL         : INTEGER := 0;
        AVG_TYPE_PIPELINE       : INTEGER := 1;
        AVG_TYPE_PIPELINE_ACC   : INTEGER := 2;
        AVG_TYPE_ACC            : INTEGER := 3;
        
        -- Dsp
        RECTIFICATION_DISABLED    : INTEGER := 0;
        RECTIFICATION_MED   : INTEGER := 1;
        RECTIFICATION_FULL  : INTEGER := 2;
		
		-- FIR CORR
        N_TAPS              : INTEGER    := 512;
        N_SETS_TAP          : INTEGER    := 3;
        OVERSAMPLING        : INTEGER    := 4;
        WAIT_CYCLES         : INTEGER    := 4
	);
port (
    -- sync
    aclk                    : in STD_LOGIC; 
    aresetn                 : in STD_LOGIC; 
    aresetn_main            : in STD_LOGIC; 
    
    -- DEBUG
    CONFIGURATION_SIZE      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CONFIGURATION_ON        : out STD_LOGIC;
    MAIN_DMA_STATE          : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    
    CONFIGURATION_HEADER_SIZE      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CONFIGURATION_CHANNELS         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CONFIGURATION_CHANNEL0_OFFSET  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CONFIGURATION_ENCODER_OFFSET   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CONFIGURATION_DIGITALIO_OFFSET : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CONFIGURATION_CHANNEL0_SIZE    : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CONFIGURATION_TRIGGER_TYPE     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CONFIGURATION_CHANNEL0_ENABLED : out STD_LOGIC;
    CONFIGURATION_CHANNEL0_ON      : out STD_LOGIC;
    
    -- CONTROL REGISTERS
    DMA_UPLOAD_CONTROL      : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);   
    DMA_DOWNLOAD_CONTROL    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    START_INSPECTION_CONTROL: in STD_LOGIC; 
    SCAN_MODE_CONTROL       : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    CSCAN_MODE_CONTROL      : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
    TRIGGER_CONTROL         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    RESET_CONTROL           : in STD_LOGIC;
    ENABLE_CONTROL          : in STD_LOGIC;
	CHANNEL_CONTROL         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    -- STATUS REGISTERS
    DMA_UPLOAD_STATUS       : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);   
    DMA_DOWNLOAD_STATUS     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    START_INSPECTION_STATUS : out STD_LOGIC; 
    SCAN_MODE_STATUS        : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    CSCAN_MODE_STATUS       : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
    TRIGGER_STATUS          : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    RESET_STATUS            : out STD_LOGIC;
    ENABLE_STATUS           : out STD_LOGIC;
	CHANNEL_STATUS          : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    TEMPERATURE_STATUS      : in std_logic_vector(REGISTER_W32-1 downto 0);
    
    -- ENCODER
    ENCODER1_DIR     : in STD_LOGIC_VECTOR (REGISTER_W2-1 downto 0);  
    ENCODER1_COUNT   : in STD_LOGIC_VECTOR (REGISTER_W32-1 downto 0); 
    ENCODER1_SPEED   : in STD_LOGIC_VECTOR (REGISTER_W32-1 downto 0);  
    
    ENCODER2_DIR     : in STD_LOGIC_VECTOR (REGISTER_W2-1 downto 0);  
    ENCODER2_COUNT   : in STD_LOGIC_VECTOR (REGISTER_W32-1 downto 0);
    ENCODER2_SPEED   : in STD_LOGIC_VECTOR (REGISTER_W32-1 downto 0);  
    
    ENCODER_RESET_TOP     : in STD_LOGIC;
    ENCODER_RESET_MAIN    : out STD_LOGIC;
    
    -- MOTOR PWM
    ENCODER_RESET_PWM  : out STD_LOGIC;
	
	-- DIO
	IO_INPUTS   			: in  STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
	IO_OUTPUTS  			: out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
	
	-- AVG
    DSP_AVG_LEVEL    : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    DSP_AVG_COUNTER  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    DSP_AVG_TYPE     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    
    -- HVPS EN
    HVPS_EN          : out STD_LOGIC;
    	
	---------------------------------------------- CH0 ----------------------------------------------
	
	-- Channel HW
    CH0_CHANNEL_HW   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	
	CH0_CHANNEL_TRIGGER_TYPE   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	CH0_STATE_A	  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    CH0_STATE_B   : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    CH0_STATE_C   : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    	
	-- Config output receiver
	CH0_RECEIVER_SAMPLING_FREQUENCY         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
    CH0_RECEIVER_DATA_WINDOW                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
    CH0_RECEIVER_DELAY                      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
    CH0_RECEIVER_ANALOG_GAIN                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    CH0_RECEIVER_DIGITAL_GAIN               : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    CH0_RECEIVER_EXTERNAL_MULTIPLEXER_EN    : out STD_LOGIC;
			
	-- Config output transmitter
	CH0_TRANSMITTER_VOLTAGE                 : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
    CH0_TRANSMITTER_BURST_FREQUENCY         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
    CH0_TRANSMITTER_N_CYCLES                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    CH0_TRANSMITTER_N_BURST                 : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    CH0_TRANSMITTER_DELAY                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CH0_TRANSMITTER_DELTA_DELAY             : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CH0_TRANSMITTER_DELTA_ADD               : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CH0_TRANSMITTER_DIRECTIONAL_PHASING     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    CH0_TRANSMITTER_START_CYCLE	            : out STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
		
	-- Config output magnet
	CH0_MAGNET_MODE                         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    CH0_MAGNET_PULSE_WIDTH                  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    CH0_MAGNET_INITIAL_DELAY                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CH0_MAGNET_RAMP_UP_VOLTAGE              : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    CH0_MAGNET_VOLTAGE                      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);   
	
	-- Config analog filters
    CH0_DSP_ANALOG_FILTER                      : out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
    CH0_DSP_COINC_LEVEL                     : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    CH0_DSP_AVG_LEVEL                       : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    CH0_DSP_AVG_TYPE                        : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	
	CH0_DSP_BURST_LEVEL                     : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	CH0_DSP_BURST_COUNTER					: out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		
	-- Config DAC curves
	CH0_CONFIG_DAC_AXIS_TREADY    : in STD_LOGIC;
	CH0_CONFIG_DAC_AXIS_TDATA	  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	CH0_CONFIG_DAC_AXIS_TLAST	  : out STD_LOGIC;
	CH0_CONFIG_DAC_AXIS_TVALID    : out STD_LOGIC;
			
	-- Signal input
	CH0_SIGNAL_COINC_WINDOW  	  : out STD_LOGIC;
	CH0_SIGNAL_AVG_WINDOW  		  : out STD_LOGIC;
		
	CH0_SIGNAL_INPUT_AXIS_TREADY  : out STD_LOGIC;
	CH0_SIGNAL_INPUT_AXIS_TDATA	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	CH0_SIGNAL_INPUT_AXIS_TLAST	  : in STD_LOGIC;
	CH0_SIGNAL_INPUT_AXIS_TVALID  : in STD_LOGIC;
			
    -- SYSTEM SIGNALS
    SW_RESET                : out STD_LOGIC;
	
    -- DMA INPUT
    DMA_S_axis_tready       : out STD_LOGIC;
    DMA_S_axis_tkeep        : in STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0);
    DMA_S_axis_tdata        : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    DMA_S_axis_tlast        : in STD_LOGIC;
    DMA_S_axis_tvalid       : in STD_LOGIC;
    -- DMA OUTPUT
    DMA_M_axis_tvalid       : out STD_LOGIC;
    DMA_M_axis_tdata        : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    DMA_M_axis_tlast        : out STD_LOGIC;
    DMA_M_axis_tkeep        : out STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0);
    DMA_M_axis_tready       : in STD_LOGIC
    
);
end main;

architecture Behavioral of main is
	
	---------------------------------------------- CHANNEL ----------------------------------------------
	COMPONENT channel IS
    generic (
        -- General
        CHANNEL_ID        : INTEGER := 0;
        REGISTER_W64    : INTEGER := 64;
        REGISTER_W48    : INTEGER := 48;
        REGISTER_W32    : INTEGER := 32;
        REGISTER_W21    : INTEGER := 21;
        REGISTER_W16    : INTEGER := 16;
        REGISTER_W14    : INTEGER := 14;
        REGISTER_W8     : INTEGER := 8;
        REGISTER_W4     : INTEGER := 4;
        REGISTER_W2     : INTEGER := 2;
        
        -- Scan mode
        SCAN_MODE_NORMAL           : INTEGER := 0;
        SCAN_MODE_MRUT             : INTEGER := 10;
        SCAN_MODE_MRUT_PITCH_CATH  : INTEGER := 11;
        SCAN_MODE_MRUT_DIR         : INTEGER := 12;
        
        -- Trigger type
        TRIGGER_TYPE_NORMAL     : INTEGER := 0;
        TRIGGER_TYPE_SW         : INTEGER := 10;
        TRIGGER_TYPE_SW_PWM     : INTEGER := 11;
        TRIGGER_TYPE_ENC        : INTEGER := 20;
        TRIGGER_TYPE_ROBOT      : INTEGER := 30;
        TRIGGER_TYPE_ROBOT_ENC  : INTEGER := 40;
        
        -- external trigger
        DISPOSITION_PASS  : INTEGER := 1;
        DISPOSITION_FAIL  : INTEGER := 2;
        DISPOSITION_NODET : INTEGER := 0;
        
        -- Coinc
        TRIGGER_WAIT    : INTEGER := 32;
        MIN_COINC_LEVEL : INTEGER := 2;
        
        -- Avg
        MIN_AVG_LEVEL       : INTEGER := 2;
        BRAM_ADDR_WIDTH     : INTEGER := 16;
        BRAM_DATA_WIDTH     : INTEGER := 23;
        AVG_TYPE_NORMAL         : INTEGER := 0;
        AVG_TYPE_PIPELINE       : INTEGER := 1;
        AVG_TYPE_PIPELINE_ACC   : INTEGER := 2;
        AVG_TYPE_ACC            : INTEGER := 3;
        
        -- Dsp
        RECTIFICATION_DISABLED    : INTEGER := 0;
        RECTIFICATION_MED   : INTEGER := 1;
        RECTIFICATION_FULL  : INTEGER := 2;
               
        -- FIR CORR
        N_TAPS              : INTEGER    := 512;
        N_SETS_TAP          : INTEGER    := 3;
        OVERSAMPLING        : INTEGER    := 2;
        WAIT_CYCLES         : INTEGER    := 4
    );
    port (
        -- Sync
        aclk         : in STD_LOGIC;
        aresetn      : in STD_LOGIC;
        
        IDLE_STATE_IN            : in STD_LOGIC;
        IDLE_STATE_OUT        : out STD_LOGIC;
        
        -- Control    
        CONFIGURE_CONTROL        : in STD_LOGIC;
        
        -- Start inspection
        START_INSPECTION_CONTROL  : in STD_LOGIC;
        START_INSPECTION_STATUS   : out STD_LOGIC;
        
        -- Start top inspection
        START_STOP_INSPECTION_CONTROL  : in STD_LOGIC;
        START_STOP_INSPECTION_STATUS   : out STD_LOGIC;
        SYSTEM_READY_STATUS            : out STD_LOGIC;
        DISPOSITION          : out STD_LOGIC; 
        DISPOSITION_BITS  : out STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0); --
        
        -- Scan mode
        SCAN_MODE_CONTROL : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        SCAN_MODE_STATUS  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        
        -- Trigger type
        TRIGGER_TYPE_CONTROL : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRIGGER_TYPE_STATUS  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        
        -- Trigger sw
        TRIGGER_SW_CONTROL : in STD_LOGIC;
        TRIGGER_SW_STATUS  : out STD_LOGIC;
        
        -- PWM
        PWM_MOTOR_EN : in STD_LOGIC;
        PWM_END      : in STD_LOGIC;
        PWM_MOTOR_MOVE    : out STD_LOGIC;
        
        -- LRUT
        LRUT_EN          : out STD_LOGIC;
        
        --Pulser Status
        PULSER_STATUS    : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        
        --Voltage Status
        VOLTAGE_STATUS   : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
              
        -- TEMPERATURE
        TEMPERATURE_STATUS : in std_logic_vector(REGISTER_W32-1 downto 0);
        
        -- Channel HW
        CHANNEL_HW       : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        
        -- Ascan
        READ_ASCAN        : in STD_LOGIC;
        ASCAN_READY       : out STD_LOGIC;
        ASCAN_SIZE        : out STD_LOGIC_VECTOR(REGISTER_W21-1 downto 0);
        
        -- DEBUG
        CHANNEL_ENABLED   : out STD_LOGIC;
        CHANNEL_TRIGGER   : out STD_LOGIC;
        CONFIG_ON         : out STD_LOGIC;
        CHANNEL_STATE_A      : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        CHANNEL_STATE_B   : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        CHANNEL_STATE_C   : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
                  
        -- Signal input
        SIGNAL_COINC_WINDOW        : out STD_LOGIC;
        SIGNAL_COINC_COUNTER      : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        SIGNAL_AVG_WINDOW            : out STD_LOGIC;
        SIGNAL_AVG_COUNTER        : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        
        SIGNAL_INPUT_AXIS_TREADY  : out STD_LOGIC;
        SIGNAL_INPUT_AXIS_TDATA      : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        SIGNAL_INPUT_AXIS_TLAST      : in STD_LOGIC;
        SIGNAL_INPUT_AXIS_TVALID  : in STD_LOGIC;
        
        -- Config
        CONFIG_INPUT_LOAD            : in STD_LOGIC;
        CONFIG_INPUT_AXIS_TREADY  : out STD_LOGIC;
        CONFIG_INPUT_AXIS_TDATA      : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        CONFIG_INPUT_AXIS_TLAST      : in STD_LOGIC;
        CONFIG_INPUT_AXIS_TVALID  : in STD_LOGIC;
        
        -- ENCODERS
        ENCODER1_POS        : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        ENCODER1_DIR        : in STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
        ENCODER1_RPM        : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        ENCODER1_TRIGGER_COUNTS : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        
        ENCODER2_POS        : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        ENCODER2_DIR        : in STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
        ENCODER2_RPM        : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        ENCODER2_TRIGGER_COUNTS : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        
        ENCODER3_POS        : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        ENCODER3_DIR        : in STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
        ENCODER3_RPM        : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        ENCODER3_TRIGGER_COUNTS : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        
		ENCODER_MAX_OVERSPEED_ALARM : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
               
		ENCODER_RESET_TOP      : in STD_LOGIC;
        ENCODER_RESET_CHANNEL  : out STD_LOGIC;
        MAX_OVERSPEED_ALARM_1  : out STD_LOGIC;
        MAX_OVERSPEED_ALARM_2  : out STD_LOGIC;
        
        -- IO
        IO_INPUTS              : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        IO_OUTPUTS        : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        
        -- Config output receiver
        RECEIVER_SAMPLING_FREQUENCY         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        RECEIVER_DATA_WINDOW                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        RECEIVER_DELAY                      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        RECEIVER_ANALOG_GAIN                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  --db*1024
        RECEIVER_DIGITAL_GAIN               : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  --V*1024*1024   
        RECEIVER_EXTERNAL_MULTIPLEXER_EN    : out STD_LOGIC;
        
        -- Config output transmitter
        TRANSMITTER_VOLTAGE                 : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        TRANSMITTER_BURST_FREQUENCY         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        TRANSMITTER_N_CYCLES                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_N_BURST                 : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_DELAY                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_DELTA_DELAY             : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_DELTA_ADD               : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        TRANSMITTER_DIRECTIONAL_PHASING     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        TRANSMITTER_START_CYCLE                : out STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
        
        -- Config output magnet
        MAGNET_MODE                         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        MAGNET_PULSE_WIDTH                  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        MAGNET_INITIAL_DELAY                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        MAGNET_RAMP_UP_VOLTAGE              : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        MAGNET_VOLTAGE                      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        
        -- Condig analog filters
        DSP_ANALOG_FILTER                      : out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        DSP_COINC_LEVEL                     : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        DSP_AVG_LEVEL                       : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        DSP_AVG_TYPE                        : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        
        -- Config DAC curves
        CONFIG_DAC_AXIS_TREADY    : in STD_LOGIC;
        CONFIG_DAC_AXIS_TDATA      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        CONFIG_DAC_AXIS_TLAST      : out STD_LOGIC;
        CONFIG_DAC_AXIS_TVALID    : out STD_LOGIC;
                    
        -- ASCAN
        ASCAN_AXIS_TREADY  : in STD_LOGIC;
        ASCAN_AXIS_TDATA   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        ASCAN_AXIS_TLAST   : out STD_LOGIC;
        ASCAN_AXIS_TVALID  : out STD_LOGIC;
        ASCAN_AXIS_TKEEP   : out std_logic_vector((REGISTER_W32/8)-1 downto 0)
    );
    END COMPONENT channel;

	---------------------------------------------- CH0 ----------------------------------------------	
	
	-- Control
	signal CH0_START_INSPECTION_STATUS  	: STD_LOGIC;
	
	signal CH0_START_STOP_INSPECTION_CONTROL: STD_LOGIC;
	signal CH0_START_STOP_INSPECTION_STATUS : STD_LOGIC;
	
	signal CH0_SETUP_SELECTED_CONTROL: STD_LOGIC;
	signal CH0_SETUP_BIT_0_CONTROL: STD_LOGIC;
	signal CH0_SETUP_BIT_1_CONTROL: STD_LOGIC;
	signal CH0_SETUP_BIT_2_CONTROL: STD_LOGIC;
		
	signal CH0_PART_RESET_CONTROL: STD_LOGIC;
	
	signal CH0_SYSTEM_READY_STATUS: STD_LOGIC;
	signal CH0_INSPECTION_IN_PROGRESS_STATUS: STD_LOGIC;
	signal CH0_DISPOSITION_BIT_0_STATUS: STD_LOGIC;
	signal CH0_DISPOSITION_BIT_1_STATUS: STD_LOGIC;
		
	signal CH0_DISPOSITION		  	: STD_LOGIC; 
	signal CH0_DISPOSITION_BITS  	: STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
	signal CH0_SCAN_MODE_STATUS 	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH0_TRIGGER_TYPE_STATUS 	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

	-- Ascan
	signal CH0_READ_ASCAN  	  		: STD_LOGIC;
	signal CH0_ASCAN_READY  	  	: STD_LOGIC;
	signal CH0_ASCAN_SIZE	      	: STD_LOGIC_VECTOR(REGISTER_W21-1 downto 0);
				
	-- Config
	signal CH0_CONFIG_INPUT_LOAD	  	  	: STD_LOGIC;
	signal CH0_CONFIG_INPUT_AXIS_TREADY  	: STD_LOGIC;
	signal CH0_CONFIG_INPUT_AXIS_TDATA	  	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH0_CONFIG_INPUT_AXIS_TLAST	  	: STD_LOGIC;
	signal CH0_CONFIG_INPUT_AXIS_TVALID  	: STD_LOGIC;
		
	-- ENCODERS
	signal CH0_ENCODER1_POS  	  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH0_ENCODER1_DIR  	  : STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
	signal CH0_ENCODER1_RPM  	  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH0_ENCODER1_TRIGGER_COUNTS : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
	signal CH0_ENCODER2_POS  	  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH0_ENCODER2_DIR  	  : STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
	signal CH0_ENCODER2_RPM  	  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH0_ENCODER2_TRIGGER_COUNTS : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
	signal CH0_ENCODER3_POS  	  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH0_ENCODER3_DIR  	  : STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
	signal CH0_ENCODER3_RPM  	  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH0_ENCODER3_TRIGGER_COUNTS : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	
	signal CH0_ENCODER_RESET  : STD_LOGIC;
    signal CH0_MAX_OVERSPEED_ALARM_1  : STD_LOGIC;
    signal CH0_MAX_OVERSPEED_ALARM_2  : STD_LOGIC;
							
	-- ASCAN
	signal CH0_ASCAN_AXIS_TREADY  : STD_LOGIC;
	signal CH0_ASCAN_AXIS_TDATA   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH0_ASCAN_AXIS_TLAST   : STD_LOGIC;
	signal CH0_ASCAN_AXIS_TVALID  : STD_LOGIC;
	signal CH0_ASCAN_AXIS_TKEEP   : STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0);
	
	
	signal COINC_LEVEL : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal COINC_COUNTER : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);

    signal AVG_LEVEL : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal AVG_COUNTER : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal avg_type : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

	---------------------------------------------- MAIN ----------------------------------------------

	signal configure_control            : std_logic;

    signal enable_status_i              : std_logic;
    signal reset_status_i               : std_logic;
    signal start_inspection_status_i    : std_logic;
    signal scan_mode_status_i           : std_logic_vector(REGISTER_W32-1 downto 0);
    signal cscan_mode_status_i          : std_logic_vector(REGISTER_W32-1 downto 0); 
    signal trigger_status_i          	: std_logic_vector(REGISTER_W32-1 downto 0); 
    
    signal trigger_type_control         : std_logic_vector(REGISTER_W32-1 downto 0);
    signal trigger_type_status_i        : std_logic_vector(REGISTER_W32-1 downto 0); 
    
    -- SW trigger
    signal trigger_sw_status            : std_logic_vector(N_CHANNELS-1 downto 0);
    
    -- dma state machine
    type dma_states is (idle, prepare_write_configuration, write_configuration, write_configuration_ack, 
                              prepare_read_configuration_1, prepare_read_configuration_2, read_configuration, read_configuration_end, 
                              prepare_read_capability_1, prepare_read_capability_2, read_capability, read_capability_end,
                              start_send_ascan, send_ascan_ch0, send_ascan_ch1, stop_send_ascan); 
    signal dma_state : dma_states; 
    
    -- UPLOAD
    signal dma_upload_control_ascan_request     : std_logic;
    signal dma_upload_control_cscan_request     : std_logic;
    signal dma_upload_control_config_request    : std_logic;
    signal dma_upload_control_capability_request: std_logic;
    signal dma_upload_control_ascan_availability: std_logic;
    signal dma_upload_control_cscan_availability: std_logic;
    signal dma_upload_control_scan_ready        : std_logic;
    signal dma_upload_control_config_ready      : std_logic;
    signal dma_upload_control_capability_ready  : std_logic;
    signal dma_upload_control_data_size         : std_logic_vector(REGISTER_W21-1 downto 0);
        
    signal dma_upload_status_ascan_request      : std_logic;
    signal dma_upload_status_cscan_request      : std_logic;
    signal dma_upload_status_config_request     : std_logic;
    signal dma_upload_status_capability_request : std_logic;
    signal dma_upload_status_ascan_availability : std_logic;
    signal dma_upload_status_cscan_availability : std_logic;
    signal dma_upload_status_scan_ready         : std_logic;
    signal dma_upload_status_config_ready       : std_logic;
    signal dma_upload_status_capability_ready   : std_logic;
    signal dma_upload_status_data_size          : std_logic_vector(REGISTER_W21-1 downto 0);
    
	signal channel_selected_for_upload  : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
       
    -- DOWNLOAD
    signal dma_download_control_config_request      : std_logic;
    signal dma_download_control_ascan_request       : std_logic;
    signal dma_download_control_data_size           : std_logic_vector(REGISTER_W21-1 downto 0);
        
    signal dma_download_status_config_request       : std_logic;
    signal dma_download_status_ascan_request        : std_logic;
    signal dma_download_status_data_size            : std_logic_vector(REGISTER_W21-1 downto 0);
         
    signal sw_reset_i                   : std_logic;

	---------------------------------------------- CONFIGURATION ----------------------------------------------
	
    signal config_header_size        : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal config_channels           : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal config_channel0_offset    : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal config_encoder_offset     : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal config_digitalio_offset   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal config_channel0_size      : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

	signal config_trigger_type: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

	signal config_encoder_x_axis_enabled: STD_LOGIC;
	signal config_encoder_x_axis_type: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal config_encoder_x_axis_start: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal config_encoder_x_axis_stop: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal config_encoder_x_axis_trigger_counts: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

	signal config_encoder_y_axis_enabled: STD_LOGIC;
	signal config_encoder_y_axis_type: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal config_encoder_y_axis_start: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);	
	signal config_encoder_y_axis_stop: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal config_encoder_y_axis_trigger_counts: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal config_max_overpeed_alarm: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

	signal config_digitalio_input_enable: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal config_digitalio_output_enable: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal config_digitalio_output_value: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	
	signal config_counter : UNSIGNED(REGISTER_W32-1 downto 0);
	
    COMPONENT main_configuration IS
        generic (
            REGISTER_W32            : INTEGER := 32;
            BRAM_TREADY_SYNC        : INTEGER := 3
        );
        port (
            -- Sync
            aclk         : in STD_LOGIC;
            aresetn      : in STD_LOGIC;
            
            -- Control
            READ_CONFIGURATION  : in STD_LOGIC;
            CONFIGURATION_READY : out STD_LOGIC;
            CONFIGURATION_SIZE  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            
            -- Input
            CONFIGURATION_IN_AXIS_TREADY  : out STD_LOGIC;
            CONFIGURATION_IN_AXIS_TDATA   : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            CONFIGURATION_IN_AXIS_TLAST   : in STD_LOGIC;
            CONFIGURATION_IN_AXIS_TVALID  : in STD_LOGIC;
            
            -- Output
            CONFIGURATION_OUT_AXIS_TREADY  : in STD_LOGIC;
            CONFIGURATION_OUT_AXIS_TDATA   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            CONFIGURATION_OUT_AXIS_TLAST   : out STD_LOGIC;
            CONFIGURATION_OUT_AXIS_TVALID  : out STD_LOGIC;
            CONFIGURATION_OUT_AXIS_TKEEP   : out STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0)
        );
    END COMPONENT main_configuration;
    
    signal main_configuration_ready    : STD_LOGIC;
    signal main_configuration_size     : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    
    signal main_configuration_in_axis_tready   : STD_LOGIC;
    signal main_configuration_in_axis_tdata    : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal main_configuration_in_axis_tlast    : STD_LOGIC;
    signal main_configuration_in_axis_tvalid   : STD_LOGIC;
    
    signal main_configuration_out_axis_tready  : STD_LOGIC;
    signal main_configuration_out_axis_tdata   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal main_configuration_out_axis_tlast   : STD_LOGIC;
    signal main_configuration_out_axis_tvalid  : STD_LOGIC;
    signal main_configuration_out_axis_tkeep   : STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0);
    
    signal read_configuration_flag : STD_LOGIC;
    signal write_configuration_flag : STD_LOGIC;
      
    -- tkeep
    constant tkeep_all_bytes : STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0):= (others => '1');
    
    ---------------------------------------------- CAPABILITIES ----------------------------------------------
    
    COMPONENT capabilities IS
        generic (
            RAM_ADDR_LENGTH         : INTEGER := 10;
            REGISTER_W32            : INTEGER := 32;
            BRAM_TREADY_SYNC        : INTEGER := 3;
            WAIT_FOR_BRAM           : INTEGER := 16
        );
        port (
            -- Sync
            aclk         : in STD_LOGIC;
            aresetn      : in STD_LOGIC;
            
            -- Control
            READ_CAPABILITIES  : in STD_LOGIC;
            CAPABILITIES_READY : out STD_LOGIC;
            CAPABILITIES_SIZE  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
                            
            -- Capabilities
            CAPABILITIES_AXIS_TREADY  : in STD_LOGIC;
            CAPABILITIES_AXIS_TDATA   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            CAPABILITIES_AXIS_TLAST   : out STD_LOGIC;
            CAPABILITIES_AXIS_TVALID  : out STD_LOGIC;
            CAPABILITIES_AXIS_TKEEP   : out STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0)
        );
    END COMPONENT capabilities;
    
    signal read_capability_flag : STD_LOGIC;
    
    --signal read_capabilities  : STD_LOGIC;
    signal capabilities_ready  : STD_LOGIC;
    signal capabilities_size  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
                    
    signal capabilities_axis_tready  : STD_LOGIC;
    signal capabilities_axis_tdata   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal capabilities_axis_tlast   : STD_LOGIC;
    signal capabilities_axis_tvalid  : STD_LOGIC;
    signal capabilities_axis_tkeep   : STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0);
       
	---------------------------------------------- I/O ----------------------------------------------
	
	signal IO_OUTPUTS_i : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal IO_INPUTS_i  : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	
	signal io_start_stop_inspection_bit : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
    signal io_setup_selected_bit        : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
    signal io_setup_bit_0_bit           : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
    signal io_setup_bit_1_bit           : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
    signal io_setup_bit_2_bit           : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
    signal io_part_reset_bit            : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
    
    signal io_system_ready_bit          : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
    signal io_inspection_in_progress_bit: STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
    signal io_disposition_bit_0_bit     : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
    signal io_disposition_bit_1_bit     : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);

	---------------------------------------------- SEQUENCER ----------------------------------------------

	component channel_sequencer_1 is
		generic (
			CHANNEL_SEPPARATION	: INTEGER := 163840;
			REGISTER_W32    : INTEGER := 32;
			REGISTER_W16    : INTEGER := 16;
			REGISTER_W8     : INTEGER := 8
		);
		port (
			-- Sync
			aclk         : in STD_LOGIC;
			aresetn      : in STD_LOGIC;

			-- Control
			CHANNEL_SEQUENCER_EN : in STD_LOGIC;
			RECEIVER_DATA_WINDOW : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			HVPS_EN              : out STD_LOGIC;

			-- Tready
			TREADY_IN  : in STD_LOGIC;
			TREADY_OUT : out STD_LOGIC
		);
	end component channel_sequencer_1;

	signal CH0_RECEIVER_DATA_WINDOW_i      : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
	signal channel_en : STD_LOGIC;
	signal channel_trigger : STD_LOGIC;
	
    signal tready_in   : STD_LOGIC;
    signal tready_out  : STD_LOGIC;
		
begin

    CONFIGURATION_HEADER_SIZE      <= config_header_size;
    CONFIGURATION_CHANNELS         <= config_channels;
    CONFIGURATION_CHANNEL0_OFFSET  <= config_channel0_offset;
    CONFIGURATION_ENCODER_OFFSET   <= config_encoder_offset;
    CONFIGURATION_DIGITALIO_OFFSET <= config_digitalio_offset;
    CONFIGURATION_CHANNEL0_SIZE    <= config_channel0_size;
    CONFIGURATION_TRIGGER_TYPE     <= config_trigger_type;
	     	
	enable_status_i <= ENABLE_CONTROL; 
	ENABLE_STATUS <= enable_status_i;
	
	reset_status_i  <= RESET_CONTROL;  
	RESET_STATUS <= reset_status_i; 
	
	sw_reset_i <= RESET_CONTROL or (not ENABLE_CONTROL);  
	SW_RESET <= sw_reset_i; 
	   	     	
    control_process: process(aclk)
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				configure_control  <= '0'; 	   
				cscan_mode_status_i <= (others => '0');
        	else	
				cscan_mode_status_i <= CSCAN_MODE_CONTROL; 	
--				if((START_INSPECTION_CONTROL = start_inspection_status_i) and
--				   (SCAN_MODE_CONTROL = scan_mode_status_i) and
--				   (TRIGGER_TYPE_CONTROL = trigger_type_status_i)) then
----				   (TRIGGER_TYPE_CONTROL = trigger_type_status_i) and
----				   (TRIGGER_CONTROL(N_CHANNELS-1 downto 0) = trigger_sw_status(N_CHANNELS-1 downto 0))) then
--					configure_control  <= '0'; 	   
--				else
--					configure_control  <= '1'; 	   
--				end if;
			end if;
        end if;
    end process;
    
    CSCAN_MODE_STATUS <= cscan_mode_status_i; 
    					 
	START_INSPECTION_STATUS <= start_inspection_status_i;
	SCAN_MODE_STATUS <= scan_mode_status_i;		
				   
	start_inspection_status_i <= CH0_START_INSPECTION_STATUS;
	scan_mode_status_i <= CH0_SCAN_MODE_STATUS;

	TRIGGER_TYPE_CONTROL <= config_trigger_type;
	trigger_type_status_i <= CH0_TRIGGER_TYPE_STATUS;			   
			
	TRIGGER_STATUS <= trigger_status_i; 
    trigger_status_i(REGISTER_W32-1 downto N_CHANNELS) <= (others => '0');
    trigger_status_i(N_CHANNELS-1 downto 0) <= trigger_sw_status;
   					
    -- UPLOAD DMA PORT (master) source selection
	DMA_M_axis_tvalid <= capabilities_axis_tvalid when read_capability_flag = '1' else
	                     main_configuration_out_axis_tvalid when read_configuration_flag = '1' else
						 CH0_ASCAN_AXIS_TVALID when unsigned(channel_selected_for_upload) = 1 else 
						 '0';
		
	DMA_M_axis_tdata <= capabilities_axis_tdata when read_capability_flag = '1' else
	                    main_configuration_out_axis_tdata when read_configuration_flag = '1' else
						CH0_ASCAN_AXIS_TDATA when unsigned(channel_selected_for_upload) = 1 else 
						(others =>'0');
		
	DMA_M_axis_tlast <= capabilities_axis_tlast when read_capability_flag = '1' else
	                    main_configuration_out_axis_tlast when read_configuration_flag = '1' else
						CH0_ASCAN_AXIS_TLAST when unsigned(channel_selected_for_upload) = 1 else 
						'0';
						
	DMA_M_axis_tkeep <= capabilities_axis_tkeep when read_capability_flag = '1' else
	                    main_configuration_out_axis_tkeep when read_configuration_flag = '1' else
                        CH0_ASCAN_AXIS_TKEEP when unsigned(channel_selected_for_upload) = 1 else 
                        (others =>'0');
		
	CH0_ASCAN_AXIS_TREADY <= DMA_M_axis_tready when unsigned(channel_selected_for_upload) = 1 else
							'0';
							
	main_configuration_out_axis_tready  <= DMA_M_axis_tready when read_configuration_flag = '1' else
	                       '0';
	                       
	capabilities_axis_tready  <= DMA_M_axis_tready when read_capability_flag = '1' else
                           '0'; 
                           
    -- MAIN configuration input enable (DMA_S) when  write_configuration_flag = '1'                                                 
    main_configuration_in_axis_tdata <= DMA_S_axis_tdata when write_configuration_flag = '1' else
                                        (others => '0'); 
                                        
    main_configuration_in_axis_tlast <= DMA_S_axis_tlast when write_configuration_flag = '1' else
                                        '0';          
                                        
    main_configuration_in_axis_tvalid <= DMA_S_axis_tvalid when write_configuration_flag = '1' else
                                        '0';  
                                                          	                       	
    -- DMA   
    dma_process: process(aclk)
		variable var_counter : integer range 0 to 65536 :=0;			
    begin 
        if (rising_edge(aclk)) then
            --if (aresetn_main = '0') then
            if (aresetn = '0') then
				-- DOWNLOAD
				dma_download_status_config_request <= '0';
				dma_download_status_ascan_request <= '0';
				-- UPLOAD
				dma_upload_status_ascan_request <= '0';
				dma_upload_status_cscan_request <= '0';
				dma_upload_status_config_request <= '0';
				dma_upload_status_capability_request <= '0';
				dma_upload_status_ascan_availability <= '0';
				dma_upload_status_cscan_availability <= '0';
				dma_upload_status_scan_ready <= '0';
				dma_upload_status_config_ready <= '0';
				dma_upload_status_capability_ready <= '0';
				dma_upload_status_data_size <= (others => '0');
				CH0_READ_ASCAN <= '0';		
				channel_selected_for_upload <= (others => '0');
				-- CONFIG
				CH0_CONFIG_INPUT_LOAD <= '0';
				CH0_CONFIG_INPUT_AXIS_TDATA <= (others => '0');
				CH0_CONFIG_INPUT_AXIS_TLAST <= '0';
				CH0_CONFIG_INPUT_AXIS_TVALID <= '0';

				config_header_size <= (others => '0');
				config_channels <= (others => '0');
				config_channel0_offset <= (others => '0');
				config_encoder_offset <= (others => '0');
				config_digitalio_offset <= (others => '0');
				config_channel0_size <= (others => '0');

				config_trigger_type <= (others => '0');
				config_encoder_x_axis_enabled <= '0';
				config_encoder_x_axis_type <= (others => '0');
				config_encoder_x_axis_start <= (others => '0');
				config_encoder_x_axis_stop <= (others => '0');
				config_encoder_x_axis_trigger_counts <= (others => '0');
				config_encoder_y_axis_enabled <= '0';
				config_encoder_y_axis_type <= (others => '0');
				config_encoder_y_axis_start <= (others => '0');
				config_encoder_y_axis_stop <= (others => '0');
				config_encoder_y_axis_trigger_counts <= (others => '0');
				config_max_overpeed_alarm <= (others => '0');

				config_digitalio_input_enable <= (others => '0');
				config_digitalio_output_enable <= (others => '0');
				config_digitalio_output_value <= (others => '0');
				io_start_stop_inspection_bit <= (others => '0');
                io_setup_selected_bit <= (others => '0');
                io_setup_bit_0_bit <= (others => '0');
                io_setup_bit_1_bit <= (others => '0');
                io_setup_bit_2_bit <= (others => '0');
                io_part_reset_bit <= (others => '0');
                io_system_ready_bit <= (others => '0');
                io_inspection_in_progress_bit <= (others => '0');
                io_disposition_bit_0_bit <= (others => '0');
                io_disposition_bit_1_bit <= (others => '0');
				
				CONFIGURATION_ON <= '0';
				CONFIGURATION_SIZE <= (others => '0');
				config_counter <= (others => '0');
                
                write_configuration_flag <= '0';
                read_configuration_flag <= '0';
                read_capability_flag <= '0';
                
				-- state
				dma_state <= idle;            
        	else
				case dma_state is        
					when idle =>
						DMA_S_axis_tready <= '1';
						var_counter := 0;
						if(dma_upload_control_capability_ready = '1') then
						    dma_state <= prepare_read_capability_1;
						elsif(dma_download_control_config_request = '1') then
							dma_state <= prepare_write_configuration;  
					    elsif(dma_upload_control_config_ready = '1') then
					        dma_state <= prepare_read_configuration_1;
					    elsif(dma_upload_control_ascan_request = '1') then
					       if(unsigned(channel_selected_for_upload) = 1) then
					           dma_state <= send_ascan_ch0;
					       end if;
					    else            
                            if(CH0_ASCAN_READY = '1') then
                                dma_state <= start_send_ascan; 
                            else
                                dma_upload_status_scan_ready <= '0'; 
                            end if;
                        end if;
                    -- Handshake
                    when prepare_read_capability_1 =>
                        if(capabilities_ready = '1') then
                            dma_upload_status_capability_ready <= capabilities_ready;
                            dma_upload_status_data_size <= capabilities_size(REGISTER_W21-1 downto 0);
                            dma_state <= prepare_read_capability_2;
                        end if;
                    -- Handshake
                    when prepare_read_capability_2 =>
                        if(dma_upload_control_capability_ready = '0') then
                            dma_upload_status_capability_ready <= '0';
                            dma_state <= read_capability;
                        end if;
                    -- Handshake
                    when read_capability =>
                        if(dma_upload_control_capability_request = '1') then
                            dma_upload_status_capability_request <= '1';
                            read_capability_flag <= '1';
                            dma_state <= read_capability_end;
                        end if;
                    -- Handshake   
                    when read_capability_end =>
                        if(dma_upload_control_capability_request = '0') then
                            dma_upload_status_capability_request <= '0';
                            read_capability_flag <= '0';
                            dma_state <= idle;
                        end if;  
                    -- Handshake 
                    when prepare_read_configuration_1 =>
                        if(main_configuration_ready = '1') then
                            dma_upload_status_config_ready <= main_configuration_ready;
                            dma_upload_status_data_size <= main_configuration_size(REGISTER_W21-1 downto 0);
                            dma_state <= prepare_read_configuration_2;
                        end if;
                    when prepare_read_configuration_2 =>
                        if(dma_upload_control_config_ready = '0') then
                            dma_upload_status_config_ready <= '0';
                            dma_state <= read_configuration;
                        end if;                 
                    -- Handshake
					when read_configuration =>
					    if(dma_upload_control_config_request = '1') then
					       dma_upload_status_config_request <= '1';
					       read_configuration_flag <= '1';
					       dma_state  <= read_configuration_end;
                        end if;		
                    -- Handshake
                    when read_configuration_end => 
                        if(dma_upload_control_config_request = '0') then
                            dma_upload_status_config_request <= '0';
                            read_configuration_flag <= '0';
                            dma_state <= idle;
                        end if;
                    -- Handshake                                       			   					  
					when prepare_write_configuration =>
					    if(CH0_CONFIG_INPUT_AXIS_TREADY = '1'and main_configuration_in_axis_tready = '1') then
					       	config_header_size <= (others => '1');
                           	config_channels <= (others => '1');
                           	config_channel0_offset <= (others => '1');
                           	config_encoder_offset <= (others => '1');
                           	config_digitalio_offset <= (others => '1');
                           	config_channel0_size <= (others => '1');
							dma_download_status_config_request <= '1';
							config_counter <= (others => '0');
							write_configuration_flag <= '1';
							dma_state <= write_configuration;  
					    end if;
					-- Write config (split by channels & store in main_configuration the original one)
					when write_configuration =>
						if(DMA_S_axis_tvalid = '1' and DMA_S_axis_tkeep = tkeep_all_bytes) then
						    config_counter <= config_counter + 1;
							if(var_counter = 0) then
								config_header_size <= DMA_S_axis_tdata;	
							elsif(var_counter = 1) then
								config_channels <= DMA_S_axis_tdata;	
							elsif(var_counter = 2) then
								config_channel0_offset <= DMA_S_axis_tdata;	
							elsif(var_counter = 3) then
								config_encoder_offset <= DMA_S_axis_tdata;		
							elsif(var_counter = 4) then
								config_digitalio_offset <= DMA_S_axis_tdata;
						    elsif(var_counter = unsigned(config_channel0_offset)) then
						        config_channel0_size <= DMA_S_axis_tdata;
						        CH0_CONFIG_INPUT_AXIS_TDATA <= DMA_S_axis_tdata;	
                                CH0_CONFIG_INPUT_AXIS_TVALID <= '1';
                                CH0_CONFIG_INPUT_LOAD <= '1';
							elsif(	(var_counter > unsigned(config_channel0_offset)) and 
									(var_counter <= (unsigned(config_channel0_offset)+unsigned(config_channel0_size)-1)) 	)then
								CH0_CONFIG_INPUT_AXIS_TDATA <= DMA_S_axis_tdata;	
								CH0_CONFIG_INPUT_AXIS_TVALID <= '1';
								CH0_CONFIG_INPUT_LOAD <= '1';
								if(var_counter = (unsigned(config_channel0_offset)+unsigned(config_channel0_size)-1)) then
									CH0_CONFIG_INPUT_AXIS_TLAST <= '1';	
								end if;
							elsif(	(var_counter > (unsigned(config_channel0_offset)+unsigned(config_channel0_size)-1)) and
									(var_counter < (unsigned(config_digitalio_offset))) ) then 
						        CH0_CONFIG_INPUT_AXIS_TVALID <= '0';
                                CH0_CONFIG_INPUT_AXIS_TLAST  <= '0';
                                CH0_CONFIG_INPUT_LOAD        <= '0';
								if(var_counter = unsigned(config_encoder_offset))then
									config_trigger_type	<= DMA_S_axis_tdata;
								elsif(var_counter = unsigned(config_encoder_offset)+1)then
									config_encoder_x_axis_enabled <= DMA_S_axis_tdata(0);
								elsif(var_counter = unsigned(config_encoder_offset)+2)then
                                    config_encoder_x_axis_type  <= DMA_S_axis_tdata;
								elsif(var_counter = unsigned(config_encoder_offset)+3)then
									config_encoder_x_axis_start	<= DMA_S_axis_tdata;
								elsif(var_counter = unsigned(config_encoder_offset)+4)then
									config_encoder_x_axis_stop	<= DMA_S_axis_tdata;
								elsif(var_counter = unsigned(config_encoder_offset)+5)then
									config_encoder_x_axis_trigger_counts <= DMA_S_axis_tdata;
								elsif(var_counter = unsigned(config_encoder_offset)+6)then
									config_encoder_y_axis_enabled <= DMA_S_axis_tdata(0);
								elsif(var_counter = unsigned(config_encoder_offset)+7)then
                                    config_encoder_y_axis_type  <= DMA_S_axis_tdata;
								elsif(var_counter = unsigned(config_encoder_offset)+8)then
									config_encoder_y_axis_start <= DMA_S_axis_tdata;
								elsif(var_counter = unsigned(config_encoder_offset)+9)then
									config_encoder_y_axis_stop <= DMA_S_axis_tdata;
								elsif(var_counter = unsigned(config_encoder_offset)+10)then
									config_encoder_y_axis_trigger_counts <= DMA_S_axis_tdata;
--							    elsif(var_counter = unsigned(config_encoder_offset)+11)then
--                                    config_max_overpeed_alarm <= DMA_S_axis_tdata;    			
								end if;
							elsif(var_counter = (unsigned(config_digitalio_offset))) then 	
								config_digitalio_input_enable <= DMA_S_axis_tdata;
							elsif(var_counter = (unsigned(config_digitalio_offset)+1)) then 	
                                config_digitalio_output_enable<= DMA_S_axis_tdata;    
							elsif(var_counter = (unsigned(config_digitalio_offset)+2)) then 	
                                config_digitalio_output_value <= DMA_S_axis_tdata;   
							elsif(var_counter > (unsigned(config_digitalio_offset)+2)) then	
							    if(var_counter = unsigned(config_digitalio_offset)+3)then
                                    io_start_stop_inspection_bit    <= DMA_S_axis_tdata(REGISTER_W8-1 downto 0);
                                elsif(var_counter = unsigned(config_digitalio_offset)+4)then
                                    io_setup_selected_bit <= DMA_S_axis_tdata(REGISTER_W8-1 downto 0);
                                elsif(var_counter = unsigned(config_digitalio_offset)+5)then
                                    io_setup_bit_0_bit <= DMA_S_axis_tdata(REGISTER_W8-1 downto 0);    
                                elsif(var_counter = unsigned(config_digitalio_offset)+6)then
                                    io_setup_bit_1_bit <= DMA_S_axis_tdata(REGISTER_W8-1 downto 0);        
                                elsif(var_counter = unsigned(config_digitalio_offset)+7)then
                                    io_setup_bit_2_bit <= DMA_S_axis_tdata(REGISTER_W8-1 downto 0); 
                                elsif(var_counter = unsigned(config_digitalio_offset)+8)then
                                    io_part_reset_bit <= DMA_S_axis_tdata(REGISTER_W8-1 downto 0);                                          
                                -- 9 to 18 reserved INPUTS                        
                                -- OUTPUTS 
                                elsif(var_counter = unsigned(config_digitalio_offset)+19)then
                                    io_system_ready_bit <= DMA_S_axis_tdata(REGISTER_W8-1 downto 0);  
                                elsif(var_counter = unsigned(config_digitalio_offset)+20)then
                                    io_inspection_in_progress_bit <= DMA_S_axis_tdata(REGISTER_W8-1 downto 0);     
                                elsif(var_counter = unsigned(config_digitalio_offset)+21)then
                                    io_disposition_bit_0_bit <= DMA_S_axis_tdata(REGISTER_W8-1 downto 0);   
                                elsif(var_counter = unsigned(config_digitalio_offset)+22)then
                                    io_disposition_bit_1_bit <= DMA_S_axis_tdata(REGISTER_W8-1 downto 0);    
                                end if;    
                                -- 23 to 34 reserved INPUTS   
					        end if;
							var_counter := var_counter + 1;
						else
							CH0_CONFIG_INPUT_AXIS_TVALID <= '0';	
							CH0_CONFIG_INPUT_AXIS_TLAST  <= '0';	
						end if;
						if(DMA_S_axis_tlast = '1') then
							dma_state <= write_configuration_ack; 
						end if;
				    -- Handshake
					when write_configuration_ack =>
						CH0_CONFIG_INPUT_AXIS_TVALID <= '0';	
						CH0_CONFIG_INPUT_AXIS_TLAST  <= '0';
						if(dma_download_control_config_request = '0') then
							dma_download_status_config_request <= '0';
							CONFIGURATION_ON <= '1';
                            CONFIGURATION_SIZE <= std_logic_vector(config_counter);
                            write_configuration_flag <= '0';
							dma_state <= idle; 
						end if;
				    -- Upload ASCAN
					when start_send_ascan =>
					    dma_upload_status_scan_ready <= '1'; 
						if(CH0_ASCAN_READY = '1') then
						    dma_upload_status_data_size <= CH0_ASCAN_SIZE;
						    channel_selected_for_upload	<= std_logic_vector(to_unsigned(1, channel_selected_for_upload'length));					 
						end if;
						dma_state <= idle;	
				    -- Handshake
					when send_ascan_ch0 => 										
						if(dma_upload_control_ascan_request = '1' and (unsigned(CH0_ASCAN_SIZE) = unsigned(dma_upload_control_data_size))) then
							dma_upload_status_ascan_request <= '1';
							dma_upload_status_scan_ready <= '0';
							CH0_READ_ASCAN	<= '1';																															
						end if;
						if(CH0_ASCAN_AXIS_TLAST = '1') then
							dma_state <= stop_send_ascan;																
						end if;																	
					when stop_send_ascan =>
						CH0_READ_ASCAN	<= '0';																		
						if(dma_upload_control_ascan_request = '0') then
							dma_upload_status_ascan_request <= '0';
							dma_state <= idle; 
						end if;              
					when others =>
						null;                   
				end case;  
			end if;
        end if;
    end process;
    
    DSP_AVG_LEVEL   <= AVG_LEVEL;
    DSP_AVG_TYPE    <= avg_type;
    DSP_AVG_COUNTER <= AVG_COUNTER; 
    CHANNEL_STATUS  <= (others => '0');
    
    MAIN_DMA_STATE <= std_logic_vector(to_unsigned(0, MAIN_DMA_STATE'length)) when dma_state = idle else
                       std_logic_vector(to_unsigned(1, MAIN_DMA_STATE'length)) when dma_state = prepare_write_configuration else
                       std_logic_vector(to_unsigned(2, MAIN_DMA_STATE'length)) when dma_state = write_configuration else
                       std_logic_vector(to_unsigned(3, MAIN_DMA_STATE'length)) when dma_state = write_configuration_ack else
                       std_logic_vector(to_unsigned(4, MAIN_DMA_STATE'length)) when dma_state = prepare_read_configuration_1 else
                       std_logic_vector(to_unsigned(5, MAIN_DMA_STATE'length)) when dma_state = prepare_read_configuration_2 else
                       std_logic_vector(to_unsigned(6, MAIN_DMA_STATE'length)) when dma_state = read_configuration else
                       std_logic_vector(to_unsigned(7, MAIN_DMA_STATE'length)) when dma_state = read_configuration_end else
                       std_logic_vector(to_unsigned(8, MAIN_DMA_STATE'length)) when dma_state = prepare_read_capability_1 else
                       std_logic_vector(to_unsigned(9, MAIN_DMA_STATE'length)) when dma_state = prepare_read_capability_2 else
                       std_logic_vector(to_unsigned(10, MAIN_DMA_STATE'length)) when dma_state = read_capability else
                       std_logic_vector(to_unsigned(11, MAIN_DMA_STATE'length)) when dma_state = read_capability_end else
                       std_logic_vector(to_unsigned(12, MAIN_DMA_STATE'length)) when dma_state = start_send_ascan else
                       std_logic_vector(to_unsigned(13, MAIN_DMA_STATE'length)) when dma_state = send_ascan_ch0 else
                       std_logic_vector(to_unsigned(14, MAIN_DMA_STATE'length)) when dma_state = send_ascan_ch1 else
                       std_logic_vector(to_unsigned(15, MAIN_DMA_STATE'length));
                       
    
    -- DMA Control signal management
    DMA_signals: process(aclk)
    begin 
        if (rising_edge(aclk)) then
            --if (aresetn_main = '0') then
            if (aresetn = '0') then
				-- CONTROL 
				dma_upload_control_ascan_request        <= '0';    
				dma_upload_control_cscan_request        <= '0';
				dma_upload_control_config_request       <= '0'; 
				dma_upload_control_capability_request   <= '0'; 
				dma_upload_control_scan_ready           <= '0'; 
				dma_upload_control_config_ready         <= '0'; 
				dma_upload_control_capability_ready     <= '0';   
				dma_upload_control_data_size            <= (others => '0');  
				-- STATUS  
				dma_download_control_config_request     <= '0';    
				dma_download_control_ascan_request      <= '0';
				dma_download_control_data_size          <= (others => '0'); 
        	else
				-- CONTROL 
				dma_upload_control_ascan_request <= DMA_UPLOAD_CONTROL(0);
				dma_upload_control_cscan_request <= DMA_UPLOAD_CONTROL(1);
				dma_upload_control_config_request <= DMA_UPLOAD_CONTROL(2);
				dma_upload_control_capability_request <= DMA_UPLOAD_CONTROL(3);
				dma_upload_control_ascan_availability <= DMA_UPLOAD_CONTROL(4);
				dma_upload_control_Cscan_availability <= DMA_UPLOAD_CONTROL(5);
				dma_upload_control_scan_ready <= DMA_UPLOAD_CONTROL(6);
				dma_upload_control_config_ready <= DMA_UPLOAD_CONTROL(7);
				dma_upload_control_capability_ready <= DMA_UPLOAD_CONTROL(8);
				dma_upload_control_data_size <= DMA_UPLOAD_CONTROL(31 downto 11);
				-- STATUS  
				dma_download_control_config_request <= DMA_DOWNLOAD_CONTROL(0);
				dma_download_control_ascan_request <= DMA_DOWNLOAD_CONTROL(1);
				dma_download_control_data_size <= DMA_DOWNLOAD_CONTROL(31 downto 11);  
			end if;
        end if;
    end process;
    
    dma_download_status_data_size <= dma_download_control_data_size;

    DMA_DOWNLOAD_STATUS <= dma_download_status_data_size & "000000000" & dma_download_status_ascan_request & dma_download_status_config_request;
               
    DMA_UPLOAD_STATUS <= dma_upload_status_data_size & "00" & dma_upload_status_capability_ready & dma_upload_status_config_ready & 
        dma_upload_status_scan_ready & dma_upload_status_cscan_availability & dma_upload_status_ascan_availability & 
        dma_upload_status_capability_request &  dma_upload_status_config_request & dma_upload_status_cscan_request & dma_upload_status_ascan_request;
        
        
    ------------------------------------------ MAIN CONFIGURATION ------------------------------------------      
        
    main_configuration_inst: main_configuration
        generic map(
            REGISTER_W32     =>  REGISTER_W32
        )
        port map(
            -- Sync
            aclk         => aclk,
            aresetn      => aresetn,
            
            -- Control
            READ_CONFIGURATION  => read_configuration_flag,
            CONFIGURATION_READY => main_configuration_ready,
            CONFIGURATION_SIZE  => main_configuration_size,
                
            -- Input
            CONFIGURATION_IN_AXIS_TREADY  => main_configuration_in_axis_tready,
            CONFIGURATION_IN_AXIS_TDATA   => main_configuration_in_axis_tdata,
            CONFIGURATION_IN_AXIS_TLAST   => main_configuration_in_axis_tlast,
            CONFIGURATION_IN_AXIS_TVALID  => main_configuration_in_axis_tvalid,
                
            -- Output
            CONFIGURATION_OUT_AXIS_TREADY  => main_configuration_out_axis_tready,
            CONFIGURATION_OUT_AXIS_TDATA   => main_configuration_out_axis_tdata,
            CONFIGURATION_OUT_AXIS_TLAST   => main_configuration_out_axis_tlast,
            CONFIGURATION_OUT_AXIS_TVALID  => main_configuration_out_axis_tvalid,
            CONFIGURATION_OUT_AXIS_TKEEP   => main_configuration_out_axis_tkeep
        );
         
    ------------------------------------------ CAPABILITIES ------------------------------------------
    
    capabilities_inst: capabilities
        generic map (
            REGISTER_W32     =>  REGISTER_W32
        )
        port map(
            -- Sync
            aclk         => aclk,
            aresetn      => aresetn,
                
            -- Control
            READ_CAPABILITIES  => read_capability_flag,
            CAPABILITIES_READY => capabilities_ready,
            CAPABILITIES_SIZE  => capabilities_size,
                                
            -- Capabilities
            CAPABILITIES_AXIS_TREADY  => capabilities_axis_tready,
            CAPABILITIES_AXIS_TDATA   => capabilities_axis_tdata,
            CAPABILITIES_AXIS_TLAST   => capabilities_axis_tlast,
            CAPABILITIES_AXIS_TVALID  => capabilities_axis_tvalid,
            CAPABILITIES_AXIS_TKEEP   => capabilities_axis_tkeep
        );
													
    ------------------------------------------ SEQUENCER ------------------------------------------													
	channel_sequencer_1_inst: channel_sequencer_1
		port map(
			-- Sync
			aclk         => aclk,
			aresetn      => aresetn,

			-- Control
			CHANNEL_SEQUENCER_EN => '1',
			--CHANNEL_SEQUENCER_EN => '0',
			RECEIVER_DATA_WINDOW => CH0_RECEIVER_DATA_WINDOW_i,
			HVPS_EN => HVPS_EN,

			-- Tready
			TREADY_IN  => tready_in,
			TREADY_OUT => tready_out
		);
		
    burst_sel: process(aclk)
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                CH0_DSP_BURST_LEVEL <= (others => '0');
                CH0_DSP_BURST_COUNTER <= (others => '0');
                CH0_DSP_COINC_LEVEL <= (others => '0');
                CH0_DSP_AVG_LEVEL <= (others => '0');
            else
                CH0_DSP_COINC_LEVEL <= COINC_LEVEL;
                CH0_DSP_AVG_LEVEL <= AVG_LEVEL;	
                CH0_DSP_BURST_LEVEL <= COINC_LEVEL;
                CH0_DSP_BURST_COUNTER <= COINC_COUNTER;
--                if(unsigned(cscan_mode_status_i) = SCAN_MODE_NORMAL) then
--                    CH0_DSP_BURST_LEVEL <= COINC_LEVEL;
--                    CH0_DSP_BURST_COUNTER <= COINC_COUNTER;
--                else
--                    CH0_DSP_BURST_LEVEL <= AVG_LEVEL;
--                    CH0_DSP_BURST_COUNTER <= AVG_COUNTER;
--                end if;
            end if;
        end if;
    end process;
													
--	CH0_DSP_COINC_LEVEL <= COINC_LEVEL;
--	CH0_DSP_AVG_LEVEL <= AVG_LEVEL;	
														
--	CH0_DSP_BURST_LEVEL <= COINC_LEVEL;	
--	CH0_DSP_BURST_COUNTER <= COINC_COUNTER;	
	
	CH0_RECEIVER_DATA_WINDOW <= CH0_RECEIVER_DATA_WINDOW_i;		
	
	CH0_SIGNAL_INPUT_AXIS_TREADY <= tready_out;
	
	------------------------------------------ ENCODER ------------------------------------------
    encoder_process: process(aclk)
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                -- Encoder RESET
                ENCODER_RESET_MAIN <= '1';
                ENCODER_RESET_PWM  <= '1';
                -- Encoder channel
                CH0_ENCODER1_POS <= (others => '0');
                CH0_ENCODER1_DIR <= (others => '0');
                CH0_ENCODER1_RPM <= (others => '0');
                CH0_ENCODER1_TRIGGER_COUNTS    <= (others => '0');  
                CH0_ENCODER2_POS <= (others => '0');
                CH0_ENCODER2_DIR <= (others => '0');
                CH0_ENCODER2_RPM <= (others => '0');
                CH0_ENCODER2_TRIGGER_COUNTS    <= (others => '0');  
                CH0_ENCODER3_POS <= (others => '0');
                CH0_ENCODER3_DIR <= (others => '0');
                CH0_ENCODER3_RPM <= (others => '0');
                CH0_ENCODER3_TRIGGER_COUNTS <= (others => '0');  
                CH0_CHANNEL_TRIGGER_TYPE    <= (others => '0');  
            else
                -- Encoder RESET
                ENCODER_RESET_MAIN <= CH0_ENCODER_RESET;
                ENCODER_RESET_PWM  <= '0';
                -- Encoder channel
	            CH0_ENCODER1_POS <= ENCODER1_COUNT;
                CH0_ENCODER1_DIR <= ENCODER1_DIR;    
                CH0_ENCODER1_RPM <= ENCODER1_SPEED;
                CH0_ENCODER1_TRIGGER_COUNTS    <= config_encoder_x_axis_trigger_counts;                                                                                
                
                CH0_ENCODER2_POS <= ENCODER2_COUNT;
                CH0_ENCODER2_DIR <= ENCODER2_DIR;
                CH0_ENCODER2_RPM <= ENCODER2_SPEED;
                CH0_ENCODER2_TRIGGER_COUNTS    <= config_encoder_y_axis_trigger_counts;                                                                                
                
                CH0_ENCODER3_POS <= (others => '0');
                CH0_ENCODER3_DIR <= (others => '0');
                CH0_ENCODER3_RPM <= (others => '0');
                CH0_ENCODER3_TRIGGER_COUNTS    <= (others => '0');    
                
                CH0_CHANNEL_TRIGGER_TYPE <= CH0_TRIGGER_TYPE_STATUS;
            end if;
        end if;
    end process;
													   
	---------------------------------------------- CH0 ----------------------------------------------
		
    inputs_process: process(aclk)
    begin 
        if (rising_edge(aclk)) then
            --if (aresetn_main = '0') then
            if (aresetn = '0') then
                IO_INPUTS_i(15 downto 0) <= (others => '0');
                CH0_START_STOP_INSPECTION_CONTROL <= '0';
                CH0_SETUP_SELECTED_CONTROL <= '0';
                CH0_SETUP_BIT_0_CONTROL <= '0';
                CH0_SETUP_BIT_1_CONTROL <= '0';
                CH0_SETUP_BIT_2_CONTROL <= '0';
                CH0_PART_RESET_CONTROL  <= '0';
            else
                IO_INPUTS_i(7 downto 0) <= config_digitalio_input_enable(7 downto 0) and IO_INPUTS;    
                --IO_INPUTS_i(7 downto 0) <= config_digitalio(7 downto 0) and IO_INPUTS;   
                --CH0_START_STOP_INSPECTION_CONTROL <= IO_INPUTS_i(0);  
                if(unsigned(io_start_stop_inspection_bit) <= 6) then
                    CH0_START_STOP_INSPECTION_CONTROL <= IO_INPUTS_i(to_integer(unsigned(io_start_stop_inspection_bit(2 downto 0)))); 
                else
                    CH0_START_STOP_INSPECTION_CONTROL <= '0';
                end if;
                if(unsigned(io_setup_selected_bit) <= 6) then
                    CH0_SETUP_SELECTED_CONTROL <= IO_INPUTS_i(to_integer(unsigned(io_setup_selected_bit(2 downto 0))));  
                else
                    CH0_SETUP_SELECTED_CONTROL <= '0';
                end if;
                if(unsigned(io_setup_bit_0_bit) <= 6) then
                    CH0_SETUP_BIT_0_CONTROL <= IO_INPUTS_i(to_integer(unsigned(io_setup_bit_0_bit(2 downto 0))));  
                else
                    CH0_SETUP_BIT_0_CONTROL <= '0';
                end if;
                if(unsigned(io_setup_bit_1_bit) <= 6) then
                    CH0_SETUP_BIT_1_CONTROL <= IO_INPUTS_i(to_integer(unsigned(io_setup_bit_1_bit(2 downto 0))));  
                else
                    CH0_SETUP_BIT_1_CONTROL <= '0';   
                end if;
                if(unsigned(io_setup_bit_2_bit) <= 6) then
                    CH0_SETUP_BIT_2_CONTROL <= IO_INPUTS_i(to_integer(unsigned(io_setup_bit_2_bit(2 downto 0))));  
                else
                    CH0_SETUP_BIT_2_CONTROL <= '0';    
                end if;                 
                if(unsigned(io_part_reset_bit) <= 6) then
                    CH0_PART_RESET_CONTROL  <= IO_INPUTS_i(to_integer(unsigned(io_part_reset_bit(2 downto 0)))); 
                else
                    CH0_PART_RESET_CONTROL <= '0';    
                end if;                                                                    
            end if;
        end if;
    end process;														
													
	--IO_OUTPUTS_i(0) <= CH0_INSPECTION_INPROGESS and CH1_INSPECTION_INPROGESS;
    -- CHANNEL 0 and CHANNEL 1 syncronized for inspection in progress
    outputs_process: process(aclk)
        variable var_counter : integer range 0 to 64 :=0;
    begin 
        if (rising_edge(aclk)) then
            --if (aresetn_main = '0') then
            if (aresetn = '0') then
				IO_OUTPUTS_i(15 downto 0) <= (others => '0');
				var_counter:=0;
        	else      	    
        	    --IO_OUTPUTS_i(7 downto 1) <= config_digitalio_output_enable(7 downto 1);   
        	    if(unsigned(io_inspection_in_progress_bit) <= 6) then   	    
			         IO_OUTPUTS_i(to_integer(unsigned(io_inspection_in_progress_bit(2 downto 0)))) <= (CH0_START_STOP_INSPECTION_STATUS and 
			                 config_digitalio_output_enable(to_integer(unsigned(io_inspection_in_progress_bit(2 downto 0))))) or
			                 config_digitalio_output_value(to_integer(unsigned(io_inspection_in_progress_bit(2 downto 0))));	
			    end if;    
			    if(unsigned(io_system_ready_bit) <= 6) then            
			         IO_OUTPUTS_i(to_integer(unsigned(io_system_ready_bit(2 downto 0)))) <= (CH0_SYSTEM_READY_STATUS and 
                             config_digitalio_output_enable(to_integer(unsigned(io_system_ready_bit(2 downto 0))))) or
                             config_digitalio_output_value(to_integer(unsigned(io_system_ready_bit(2 downto 0))));
                end if;      
                if(unsigned(io_disposition_bit_0_bit) <= 6) then         
			         IO_OUTPUTS_i(to_integer(unsigned(io_disposition_bit_0_bit(2 downto 0)))) <= (CH0_DISPOSITION_BIT_0_STATUS and 
                             config_digitalio_output_enable(to_integer(unsigned(io_disposition_bit_0_bit(2 downto 0))))) or
                             config_digitalio_output_value(to_integer(unsigned(io_disposition_bit_0_bit(2 downto 0))));
                end if; 
                if(unsigned(io_disposition_bit_1_bit) <= 6) then             
			         IO_OUTPUTS_i(to_integer(unsigned(io_disposition_bit_1_bit(2 downto 0)))) <= (CH0_DISPOSITION_BIT_1_STATUS and 
                             config_digitalio_output_enable(to_integer(unsigned(io_disposition_bit_1_bit(2 downto 0))))) or   
                             config_digitalio_output_value(to_integer(unsigned(io_disposition_bit_1_bit(2 downto 0))));
                end if;             
                if(var_counter >=16) then
                    var_counter:=0;
                else
                    if(var_counter /= unsigned(io_inspection_in_progress_bit(2 downto 0)) and
                       var_counter /= unsigned(io_system_ready_bit(2 downto 0)) and
                       var_counter /= unsigned(io_disposition_bit_0_bit(2 downto 0)) and
                       var_counter /= unsigned(io_disposition_bit_1_bit(2 downto 0))) then
                        --IO_OUTPUTS_i(var_counter) <= '0';
                        IO_OUTPUTS_i(var_counter) <= config_digitalio_output_value(var_counter) and config_digitalio_output_enable(var_counter);
                    end if;
                    var_counter:=var_counter+1;
                end if;                                                          																
			end if;
        end if;
    end process;
    
    CH0_DISPOSITION_BIT_0_STATUS <= '0';
    CH0_DISPOSITION_BIT_1_STATUS <= '0';

	IO_OUTPUTS <= IO_OUTPUTS_i(7 downto 0);
																																																																																		
	CONFIGURATION_CHANNEL0_ENABLED <= channel_en;																			
	
	channel_0_inst: channel
	generic map(
        -- General
        CHANNEL_ID        => 0,
        REGISTER_W64    => REGISTER_W64,
        REGISTER_W48    => REGISTER_W48,
        REGISTER_W32    => REGISTER_W32,
        REGISTER_W21    => REGISTER_W21,
        REGISTER_W16    => REGISTER_W16,
        REGISTER_W14    => REGISTER_W14,
        REGISTER_W8     => REGISTER_W8,
        REGISTER_W4     => REGISTER_W4,
        REGISTER_W2     => REGISTER_W2,
        
        -- Scan mode
        SCAN_MODE_NORMAL            => SCAN_MODE_NORMAL,
        SCAN_MODE_MRUT              => SCAN_MODE_MRUT,
        SCAN_MODE_MRUT_PITCH_CATH   => SCAN_MODE_MRUT_PITCH_CATH,
        SCAN_MODE_MRUT_DIR          => SCAN_MODE_MRUT_DIR,
             
        -- Trigger type
        TRIGGER_TYPE_NORMAL     => TRIGGER_TYPE_NORMAL,
        TRIGGER_TYPE_SW         => TRIGGER_TYPE_SW,
        TRIGGER_TYPE_SW_PWM     => TRIGGER_TYPE_SW_PWM,
        TRIGGER_TYPE_ENC        => TRIGGER_TYPE_ENC,
        TRIGGER_TYPE_ROBOT      => TRIGGER_TYPE_ROBOT,
        TRIGGER_TYPE_ROBOT_ENC  => TRIGGER_TYPE_ROBOT_ENC,
        
        -- external trigger
        DISPOSITION_PASS  => DISPOSITION_PASS,
        DISPOSITION_FAIL  => DISPOSITION_FAIL,
        DISPOSITION_NODET => DISPOSITION_NODET,
        
        -- Coinc
        TRIGGER_WAIT    => TRIGGER_WAIT,
        MIN_COINC_LEVEL => MIN_COINC_LEVEL,
        
        -- Avg
        MIN_AVG_LEVEL     => MIN_AVG_LEVEL,
        BRAM_ADDR_WIDTH => BRAM_ADDR_WIDTH,
        BRAM_DATA_WIDTH => BRAM_DATA_WIDTH, 
        AVG_TYPE_NORMAL   => AVG_TYPE_NORMAL,
        AVG_TYPE_PIPELINE => AVG_TYPE_PIPELINE,
        AVG_TYPE_PIPELINE_ACC => AVG_TYPE_PIPELINE_ACC,
        AVG_TYPE_ACC      => AVG_TYPE_ACC,
        
        -- Dsp
        RECTIFICATION_DISABLED              => RECTIFICATION_DISABLED,
        RECTIFICATION_MED                   => RECTIFICATION_MED,
        RECTIFICATION_FULL                  => RECTIFICATION_FULL,
              
        -- FIR CORR
        N_TAPS              => N_TAPS,
        N_SETS_TAP          => N_SETS_TAP,
        OVERSAMPLING        => OVERSAMPLING,
        WAIT_CYCLES         => WAIT_CYCLES
    )
	port map(
	    -- Sync
	    aclk         => aclk,
        aresetn      => aresetn,
        
        IDLE_STATE_IN  => '1',
        IDLE_STATE_OUT => open,
		
		-- Control
		CONFIGURE_CONTROL => configure_control, 
		
		-- Start inspection
		START_INSPECTION_CONTROL => START_INSPECTION_CONTROL, 
		START_INSPECTION_STATUS  => CH0_START_INSPECTION_STATUS, 
		
		-- Start top inspection
		START_STOP_INSPECTION_CONTROL => CH0_START_STOP_INSPECTION_CONTROL, 
		START_STOP_INSPECTION_STATUS  => CH0_START_STOP_INSPECTION_STATUS, 
		SYSTEM_READY_STATUS  => CH0_SYSTEM_READY_STATUS, 
		DISPOSITION		  => CH0_DISPOSITION,
		DISPOSITION_BITS  => CH0_DISPOSITION_BITS,
		
		-- Scan mode
		SCAN_MODE_CONTROL => SCAN_MODE_CONTROL, 
		SCAN_MODE_STATUS  => CH0_SCAN_MODE_STATUS, 
		
		-- Trigger type
		TRIGGER_TYPE_CONTROL => TRIGGER_TYPE_CONTROL, 
		TRIGGER_TYPE_STATUS  => CH0_TRIGGER_TYPE_STATUS, 
		
		-- Trigger sw
        TRIGGER_SW_CONTROL    => TRIGGER_CONTROL(0),
        TRIGGER_SW_STATUS     => trigger_sw_status(0),
        
		-- PWM
        PWM_MOTOR_EN    => '0',
        PWM_END         => '0',
        PWM_MOTOR_MOVE  => open,
        
        -- LRUT
        LRUT_EN         => open,
        
        -- Pulser status
        PULSER_STATUS   => (others => '0'),
        
        -- Voltage status
        VOLTAGE_STATUS  => (others => '0'),
        
        -- Temperature status
        TEMPERATURE_STATUS => TEMPERATURE_STATUS,
        
        -- Channel HW
        CHANNEL_HW      => CH0_CHANNEL_HW,
		
		-- Ascan
		READ_ASCAN  	  => CH0_READ_ASCAN,
		ASCAN_READY  	  => CH0_ASCAN_READY,
		ASCAN_SIZE	      => CH0_ASCAN_SIZE,
			
		-- DEBUG
		CHANNEL_ENABLED   => channel_en,
		CHANNEL_TRIGGER   => channel_trigger,
		CONFIG_ON         => CONFIGURATION_CHANNEL0_ON,
		CHANNEL_STATE_A   => CH0_STATE_A, 
		CHANNEL_STATE_B   => CH0_STATE_B, 
		CHANNEL_STATE_C   => CH0_STATE_C, 
					
		-- Signal input
		SIGNAL_COINC_WINDOW  	  => CH0_SIGNAL_COINC_WINDOW, 
		SIGNAL_COINC_COUNTER  	  => COINC_COUNTER, 
		SIGNAL_AVG_WINDOW  	  	  => CH0_SIGNAL_AVG_WINDOW, 
		SIGNAL_AVG_COUNTER  	  => AVG_COUNTER, 
		
		SIGNAL_INPUT_AXIS_TREADY  => tready_in, 
		SIGNAL_INPUT_AXIS_TDATA	  => CH0_SIGNAL_INPUT_AXIS_TDATA, 
		SIGNAL_INPUT_AXIS_TLAST	  => CH0_SIGNAL_INPUT_AXIS_TLAST, 
		SIGNAL_INPUT_AXIS_TVALID  => CH0_SIGNAL_INPUT_AXIS_TVALID, 
		
		-- Config
		CONFIG_INPUT_LOAD	  	  => CH0_CONFIG_INPUT_LOAD, 
		CONFIG_INPUT_AXIS_TREADY  => CH0_CONFIG_INPUT_AXIS_TREADY, 
		CONFIG_INPUT_AXIS_TDATA	  => CH0_CONFIG_INPUT_AXIS_TDATA, 
		CONFIG_INPUT_AXIS_TLAST	  => CH0_CONFIG_INPUT_AXIS_TLAST, 
		CONFIG_INPUT_AXIS_TVALID  => CH0_CONFIG_INPUT_AXIS_TVALID, 
		
		-- ENCODERS
		ENCODER1_POS  	  => CH0_ENCODER1_POS, 
		ENCODER1_DIR  	  => CH0_ENCODER1_DIR, 
		ENCODER1_RPM  	  => CH0_ENCODER1_RPM, 
		ENCODER1_TRIGGER_COUNTS => CH0_ENCODER1_TRIGGER_COUNTS,
		
		ENCODER2_POS  	  => CH0_ENCODER2_POS,
		ENCODER2_DIR  	  => CH0_ENCODER2_DIR,
		ENCODER2_RPM  	  => CH0_ENCODER2_RPM,
		ENCODER2_TRIGGER_COUNTS => CH0_ENCODER2_TRIGGER_COUNTS,
		
		ENCODER3_POS  	  => CH0_ENCODER3_POS,
		ENCODER3_DIR  	  => CH0_ENCODER3_DIR,
		ENCODER3_RPM  	  => CH0_ENCODER3_RPM,
		ENCODER3_TRIGGER_COUNTS => CH0_ENCODER3_TRIGGER_COUNTS,
		
		ENCODER_MAX_OVERSPEED_ALARM => config_max_overpeed_alarm,
        
        ENCODER_RESET_TOP      => ENCODER_RESET_TOP,
        ENCODER_RESET_CHANNEL  => CH0_ENCODER_RESET,
        MAX_OVERSPEED_ALARM_1  => CH0_MAX_OVERSPEED_ALARM_1,
        MAX_OVERSPEED_ALARM_2  => CH0_MAX_OVERSPEED_ALARM_2,
		
		-- IO
		IO_INPUTS  	  	  => IO_INPUTS_i,
		IO_OUTPUTS  	  => IO_OUTPUTS_i,
		
		-- Config output receiver
		RECEIVER_SAMPLING_FREQUENCY         => CH0_RECEIVER_SAMPLING_FREQUENCY, 
        RECEIVER_DATA_WINDOW                => CH0_RECEIVER_DATA_WINDOW_i,  
        RECEIVER_DELAY                      => CH0_RECEIVER_DELAY,  
        RECEIVER_ANALOG_GAIN                => CH0_RECEIVER_ANALOG_GAIN,
        RECEIVER_DIGITAL_GAIN               => CH0_RECEIVER_DIGITAL_GAIN,  
        RECEIVER_EXTERNAL_MULTIPLEXER_EN    => CH0_RECEIVER_EXTERNAL_MULTIPLEXER_EN,
		
		-- Config output transmitter
		TRANSMITTER_VOLTAGE                 => CH0_TRANSMITTER_VOLTAGE, 
        TRANSMITTER_BURST_FREQUENCY         => CH0_TRANSMITTER_BURST_FREQUENCY,
        TRANSMITTER_N_CYCLES                => CH0_TRANSMITTER_N_CYCLES,
        TRANSMITTER_N_BURST                 => CH0_TRANSMITTER_N_BURST,
        TRANSMITTER_DELAY                   => CH0_TRANSMITTER_DELAY,
        TRANSMITTER_DELTA_DELAY             => CH0_TRANSMITTER_DELTA_DELAY,
        TRANSMITTER_DELTA_ADD               => CH0_TRANSMITTER_DELTA_ADD ,
        TRANSMITTER_DIRECTIONAL_PHASING     => CH0_TRANSMITTER_DIRECTIONAL_PHASING, 
        TRANSMITTER_START_CYCLE             => CH0_TRANSMITTER_START_CYCLE,
		
		-- Config output magnet
		MAGNET_MODE                         => CH0_MAGNET_MODE,
        MAGNET_PULSE_WIDTH                  => CH0_MAGNET_PULSE_WIDTH, 
        MAGNET_INITIAL_DELAY                => CH0_MAGNET_INITIAL_DELAY,
        MAGNET_RAMP_UP_VOLTAGE              => CH0_MAGNET_RAMP_UP_VOLTAGE, 
        MAGNET_VOLTAGE                      => CH0_MAGNET_VOLTAGE,  
		
		-- Config analog filters
		DSP_ANALOG_FILTER  					=> CH0_DSP_ANALOG_FILTER,
		DSP_COINC_LEVEL  					=> COINC_LEVEL,
		DSP_AVG_LEVEL  					    => AVG_LEVEL,
		DSP_AVG_TYPE                        => avg_type,
						
		-- Config DAC curves
		CONFIG_DAC_AXIS_TREADY    => CH0_CONFIG_DAC_AXIS_TREADY,
		CONFIG_DAC_AXIS_TDATA	  => CH0_CONFIG_DAC_AXIS_TDATA,
		CONFIG_DAC_AXIS_TLAST	  => CH0_CONFIG_DAC_AXIS_TLAST,
		CONFIG_DAC_AXIS_TVALID    => CH0_CONFIG_DAC_AXIS_TVALID,
					
		-- ASCAN
		ASCAN_AXIS_TREADY  => CH0_ASCAN_AXIS_TREADY,
		ASCAN_AXIS_TDATA   => CH0_ASCAN_AXIS_TDATA,
		ASCAN_AXIS_TLAST   => CH0_ASCAN_AXIS_TLAST,
		ASCAN_AXIS_TVALID  => CH0_ASCAN_AXIS_TVALID,
		ASCAN_AXIS_TKEEP   => CH0_ASCAN_AXIS_TKEEP
	);
		     																																					       
end Behavioral;