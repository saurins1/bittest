----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 05/05/2018 10:07:23 AM
-- Design Name: 
-- Module Name: top_dma_test_fpga - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tgil_top is
	generic (
		-- General
		N_CHANNELS		: INTEGER := 1;
		REGISTER_W64    : INTEGER := 64;
		REGISTER_W48    : INTEGER := 48;
	    REGISTER_W32    : INTEGER := 32;
	    REGISTER_W30    : INTEGER := 30;
	    REGISTER_W21    : INTEGER := 21;
        REGISTER_W16    : INTEGER := 16;
		REGISTER_W14    : INTEGER := 14;
		REGISTER_W10	: INTEGER := 10;
		REGISTER_W8     : INTEGER := 8;
		REGISTER_W7     : INTEGER := 7;
		REGISTER_W6     : INTEGER := 6;
		REGISTER_W4     : INTEGER := 4;
		REGISTER_W2     : INTEGER := 2;
		
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_ADDR_WIDTH	: integer	:= 11;
		
        -- Scan mode
        SCAN_MODE_NORMAL  : INTEGER := 0;
        SCAN_MODE_MRUT    : INTEGER := 10;
        
        -- Trigger type
        TRIGGER_TYPE_NORMAL     : INTEGER := 0;
        TRIGGER_TYPE_SW         : INTEGER := 10;
        TRIGGER_TYPE_ENC        : INTEGER := 20;
        TRIGGER_TYPE_ROBOT      : INTEGER := 30;
        TRIGGER_TYPE_ROBOT_ENC  : INTEGER := 40;
		
		-- External trigger
		DISPOSITION_PASS  : INTEGER := 1;
		DISPOSITION_FAIL  : INTEGER := 2;
		DISPOSITION_NODET : INTEGER := 0;
		
		-- Coinc
        TRIGGER_WAIT    : INTEGER := 32;
        MIN_COINC_LEVEL : INTEGER := 2;
		
        -- Avg
        MIN_AVG_LEVEL       : INTEGER := 2;
        AVG_TYPE_NORMAL     : INTEGER := 0;
        AVG_TYPE_PIPELINE   : INTEGER := 1;
		
		-- Ascan header size
		ASCAN_HEADER_SIZE : INTEGER := 43;
		
		-- FIR CORR
        N_TAPS              : INTEGER    := 512;
        N_SETS_TAP          : INTEGER    := 3;
        OVERSAMPLING        : INTEGER    := 4;
        WAIT_CYCLES         : INTEGER    := 4;
		
        C_S00_AXI_ADDR_WIDTH    : integer    := 11
    );
    port (
		DDR_addr 	: inout STD_LOGIC_VECTOR ( 14 downto 0 );
		DDR_ba 		: inout STD_LOGIC_VECTOR ( 2 downto 0 );
		DDR_cas_n 	: inout STD_LOGIC;
		DDR_ck_n 	: inout STD_LOGIC;
		DDR_ck_p 	: inout STD_LOGIC;
		DDR_cke 	: inout STD_LOGIC;
		DDR_cs_n 	: inout STD_LOGIC;
		DDR_dm 		: inout STD_LOGIC_VECTOR ( 3 downto 0 );
		DDR_dq 		: inout STD_LOGIC_VECTOR ( 31 downto 0 );
		DDR_dqs_n 	: inout STD_LOGIC_VECTOR ( 3 downto 0 );
		DDR_dqs_p 	: inout STD_LOGIC_VECTOR ( 3 downto 0 );
		DDR_odt 	: inout STD_LOGIC;
		DDR_ras_n 	: inout STD_LOGIC;
		DDR_reset_n : inout STD_LOGIC;
		DDR_we_n 	: inout STD_LOGIC;
		
		FIXED_IO_ddr_vrn 	: inout STD_LOGIC;
		FIXED_IO_ddr_vrp 	: inout STD_LOGIC;
		FIXED_IO_mio 		: inout STD_LOGIC_VECTOR ( 53 downto 0 );
		FIXED_IO_ps_clk 	: inout STD_LOGIC;
		FIXED_IO_ps_porb 	: inout STD_LOGIC;
		FIXED_IO_ps_srstb 	: inout STD_LOGIC;

		-- ADC
		ADC_PAR_SERn : out STD_LOGIC;
		ADC_ENCp     : out STD_LOGIC;
		ADC_ENCn     : out STD_LOGIC;
		ADC_CLKOUTp  : in STD_LOGIC;
		ADC_CLKOUTn  : in STD_LOGIC;
		ADC_OFp      : in STD_LOGIC;
		ADC_OFn      : in STD_LOGIC;
		ADC_DATAp    : in STD_LOGIC_VECTOR(REGISTER_W7-1 downto 0);
		ADC_DATAn    : in STD_LOGIC_VECTOR(REGISTER_W7-1 downto 0);
		ADC_CSn      : out STD_LOGIC;
		ADC_SCK      : out STD_LOGIC;
		ADC_SDI      : out STD_LOGIC;
		ADC_SDO      : in STD_LOGIC;
		
		-- DAC
		DAC_GAIN  	: out STD_LOGIC_VECTOR(REGISTER_W10-1 downto 0);  		
		DAC_CLK   	: out STD_LOGIC; 
		DAC_SLEEP 	: out STD_LOGIC;  
		
		-- DIO
		IO_INPUTS  	: in  STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
		IO_OUTPUTS  : out  STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);

		-- MAG PULSE
		--MAG_PULSE1 	: out STD_LOGIC; -- Will use with general output
		--MAG_PULSE2 	: out STD_LOGIC;
		
		-- PULSER
		PULSER_CONTROLp   : out STD_LOGIC;
		PULSER_CONTROLn   : out STD_LOGIC;
	
		-- ENCODERS
		ENCA 	: in STD_LOGIC;
		ENCB 	: in STD_LOGIC;
		ENC_DIF_SE 	: in STD_LOGIC; -- 0 = DIFF CHA CHB (quad) - 1 = Separate encoders CHA CHB
		
		-- TEMP SENSOR
		TEMP_CSn      : out STD_LOGIC_VECTOR(2 downto 0);
		TEMP_SCK      : out STD_LOGIC
    );
end tgil_top;

architecture Behavioral of tgil_top is
	
	------------------  ZYNQ WRAPPER  ------------------ 
	----------------------------------------------------
	
	component design_top_wrapper is
	  port (
		DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
		DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
		DDR_cas_n : inout STD_LOGIC;
		DDR_ck_n : inout STD_LOGIC;
		DDR_ck_p : inout STD_LOGIC;
		DDR_cke : inout STD_LOGIC;
		DDR_cs_n : inout STD_LOGIC;
		DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
		DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
		DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
		DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
		DDR_odt : inout STD_LOGIC;
		DDR_ras_n : inout STD_LOGIC;
		DDR_reset_n : inout STD_LOGIC;
		DDR_we_n : inout STD_LOGIC;
        FCLK_CLK0 : out STD_LOGIC;
--        FCLK_CLK1 : out STD_LOGIC;
--        FCLK_CLK2 : out STD_LOGIC;
		FIXED_IO_ddr_vrn : inout STD_LOGIC;
		FIXED_IO_ddr_vrp : inout STD_LOGIC;
		FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
		FIXED_IO_ps_clk : inout STD_LOGIC;
		FIXED_IO_ps_porb : inout STD_LOGIC;
		FIXED_IO_ps_srstb : inout STD_LOGIC;
		M00_AXI_ps_araddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
		M00_AXI_ps_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
		M00_AXI_ps_arready : in STD_LOGIC_VECTOR ( 0 to 0 );
		M00_AXI_ps_arvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
		M00_AXI_ps_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
		M00_AXI_ps_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
		M00_AXI_ps_awready : in STD_LOGIC_VECTOR ( 0 to 0 );
		M00_AXI_ps_awvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
		M00_AXI_ps_bready : out STD_LOGIC_VECTOR ( 0 to 0 );
		M00_AXI_ps_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
		M00_AXI_ps_bvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
		M00_AXI_ps_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
		M00_AXI_ps_rready : out STD_LOGIC_VECTOR ( 0 to 0 );
		M00_AXI_ps_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
		M00_AXI_ps_rvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
		M00_AXI_ps_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
		M00_AXI_ps_wready : in STD_LOGIC_VECTOR ( 0 to 0 );
		M00_AXI_ps_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
		M00_AXI_ps_wvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
		M_AXIS_MM2S_dma_0_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
		M_AXIS_MM2S_dma_0_tkeep : out STD_LOGIC_VECTOR ( 3 downto 0 );
		M_AXIS_MM2S_dma_0_tlast : out STD_LOGIC;
		M_AXIS_MM2S_dma_0_tready : in STD_LOGIC;
		M_AXIS_MM2S_dma_0_tvalid : out STD_LOGIC;
		S_AXIS_dma_0_tdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
		S_AXIS_dma_0_tkeep : in STD_LOGIC_VECTOR ( 3 downto 0 );
		S_AXIS_dma_0_tlast : in STD_LOGIC;
		S_AXIS_dma_0_tready : out STD_LOGIC;
		S_AXIS_dma_0_tvalid : in STD_LOGIC;
		peripheral_aresetn : out STD_LOGIC_VECTOR ( 0 to 0 )
	  );
	end component design_top_wrapper;
		
    -- Ports of Axi master lite Bus Interface M00_AXI design_1_wrapper
    signal M00_AXI_ps_araddr : STD_LOGIC_VECTOR ( 31 downto 0 );
    signal M00_AXI_ps_arprot : STD_LOGIC_VECTOR ( 2 downto 0 );
    signal M00_AXI_ps_arready : STD_LOGIC_VECTOR ( 0 to 0 );
    signal M00_AXI_ps_arvalid : STD_LOGIC_VECTOR ( 0 to 0 );
    signal M00_AXI_ps_awaddr : STD_LOGIC_VECTOR ( 31 downto 0 );
    signal M00_AXI_ps_awprot : STD_LOGIC_VECTOR ( 2 downto 0 );
    signal M00_AXI_ps_awready : STD_LOGIC_VECTOR ( 0 to 0 );
    signal M00_AXI_ps_awvalid : STD_LOGIC_VECTOR ( 0 to 0 );
    signal M00_AXI_ps_bready : STD_LOGIC_VECTOR ( 0 to 0 );
    signal M00_AXI_ps_bresp : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal M00_AXI_ps_bvalid : STD_LOGIC_VECTOR ( 0 to 0 );
    signal M00_AXI_ps_rdata : STD_LOGIC_VECTOR ( 31 downto 0 );
    signal M00_AXI_ps_rready : STD_LOGIC_VECTOR ( 0 to 0 );
    signal M00_AXI_ps_rresp : STD_LOGIC_VECTOR ( 1 downto 0 );
    signal M00_AXI_ps_rvalid : STD_LOGIC_VECTOR ( 0 to 0 );
    signal M00_AXI_ps_wdata : STD_LOGIC_VECTOR ( 31 downto 0 );
    signal M00_AXI_ps_wready : STD_LOGIC_VECTOR ( 0 to 0 );
    signal M00_AXI_ps_wstrb : STD_LOGIC_VECTOR ( 3 downto 0 );
    signal M00_AXI_ps_wvalid : STD_LOGIC_VECTOR ( 0 to 0 );

    -- Ports of Axi Slave Bus Interface M_AXIS_MM2S_dma_0
    signal M_AXIS_MM2S_dma_0_tready    : std_logic;
    signal M_AXIS_MM2S_dma_0_tdata     : std_logic_vector(REGISTER_W32-1 downto 0);
    signal M_AXIS_MM2S_dma_0_tkeep     : std_logic_vector((REGISTER_W32/8)-1 downto 0);
    signal M_AXIS_MM2S_dma_0_tlast     : std_logic;
    signal M_AXIS_MM2S_dma_0_tvalid    : std_logic;
    
    -- Ports of Axi Slave Bus Interface M_AXIS_MM2S_dma_0
    signal S_AXIS_dma_0_tready    : std_logic;
    signal S_AXIS_dma_0_tdata     : std_logic_vector(REGISTER_W32-1 downto 0);
    signal S_AXIS_dma_0_tkeep     : std_logic_vector((REGISTER_W32/8)-1 downto 0);
    signal S_AXIS_dma_0_tlast     : std_logic;
    signal S_AXIS_dma_0_tvalid    : std_logic;
    
	------------------  S00_AXI  ------------------
	-----------------------------------------------

	component tgil_top_S00_AXI is
		generic (
			REGISTER_W32    : INTEGER := 32;
			REGISTER_W16    : INTEGER := 16;
			REGISTER_W8     : INTEGER := 8;
			REGISTER_W6     : INTEGER := 6;
			REGISTER_W4     : INTEGER := 4;
			C_S_AXI_DATA_WIDTH	: integer	:= 32;
			C_S_AXI_ADDR_WIDTH	: integer	:= 11
		);
		port (
			-- Users to add ports here
			-- CONTROL REGISTERS
			DMA_UPLOAD_CONTROL      : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);   
			DMA_DOWNLOAD_CONTROL    : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0); 
			START_INSPECTION_CONTROL: out std_logic; 
			SCAN_MODE_CONTROL       : out std_logic_vector(REGISTER_W32-1 downto 0);  
			CSCAN_MODE_CONTROL      : out std_logic_vector(REGISTER_W32-1 downto 0);  
			TRIGGER_CONTROL         : out std_logic_vector(REGISTER_W32-1 downto 0);
			RESET_CONTROL           : out std_logic;
			ENABLE_CONTROL          : out std_logic;
			CHANNEL_CONTROL         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			-- STATUS REGISTERS
			DMA_UPLOAD_STATUS       : in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);   
			DMA_DOWNLOAD_STATUS     : in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0); 
			START_INSPECTION_STATUS : in std_logic; 
			SCAN_MODE_STATUS        : in std_logic_vector(REGISTER_W32-1 downto 0);  
			CSCAN_MODE_STATUS       : in std_logic_vector(REGISTER_W32-1 downto 0);  
			TRIGGER_STATUS          : in std_logic_vector(REGISTER_W32-1 downto 0);
			RESET_STATUS            : in std_logic;
			ENABLE_STATUS           : in std_logic;
			CHANNEL_STATUS          : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			
            -- AVG
            DSP_AVG_LEVEL    : in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
            DSP_AVG_COUNTER  : in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
            DSP_AVG_TYPE     : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			
			ADC_DEBUG : out std_logic_vector(REGISTER_W32-1 downto 0);

			-- DEBUG
			CONFIGURATION_SIZE                      : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			CONFIGURATION_ON                        : in STD_LOGIC;
			MAIN_DMA_STATE                          : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);

			-- Config output receiver
			CH0_RECEIVER_SAMPLING_FREQUENCY         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
			CH0_RECEIVER_DATA_WINDOW                : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
			CH0_RECEIVER_DELAY                      : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
			CH0_RECEIVER_ANALOG_GAIN                : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			CH0_RECEIVER_DIGITAL_GAIN               : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
			CH0_RECEIVER_EXTERNAL_MULTIPLEXER_EN    : in STD_LOGIC;

			-- Config output transmitter
			CH0_TRANSMITTER_VOLTAGE                 : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
			CH0_TRANSMITTER_BURST_FREQUENCY         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
			CH0_TRANSMITTER_N_CYCLES                : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
			CH0_TRANSMITTER_DELAY                   : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
			CH0_TRANSMITTER_DIRECTIONAL_PHASING     : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 

			-- Config output magnet
			CH0_MAGNET_MODE                         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
			CH0_MAGNET_PULSE_WIDTH                  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
			CH0_MAGNET_INITIAL_DELAY                : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

			-- Config analog filters
			CH0_DSP_ANALOG_FILTER                   : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);   

			-- Debug
			CH0_CHANNEL_TRIGGER_TYPE    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			CH0_STATE_A                 : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
			CH0_STATE_B                 : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
			CH0_STATE_C                 : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);    

			-- Config parameters
			CONFIGURATION_HEADER_SIZE      : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			CONFIGURATION_CHANNELS         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			CONFIGURATION_CHANNEL0_OFFSET  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			CONFIGURATION_ENCODER_OFFSET   : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			CONFIGURATION_DIGITALIO_OFFSET : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			CONFIGURATION_CHANNEL0_SIZE    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			CONFIGURATION_TRIGGER_TYPE     : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			CH0_SIGNAL_INPUT_AXIS_TREADY   : in STD_LOGIC;
			CONFIGURATION_CHANNEL0_ENABLED : in STD_LOGIC;
			CONFIGURATION_CHANNEL0_ON      : in STD_LOGIC;

			CH0_DSP_COINC_LEVEL                     : in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
			CH0_DSP_AVG_LEVEL                       : in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
			
			ADC_WRITE_COUNTER : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);  
            ADC_READ_COUNTER  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
            
            ADC_CONFIG_EX     		: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            ADC_CONFIG_EX_VALID     : out STD_LOGIC;
            ADC_CONFIG_LOADED       : in STD_LOGIC;
            ADC_CONFIG_CURRENT      : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
            ADC_CONFIG_REQUEST      : in STD_LOGIC;
            ADC_CONFIG_REQUEST_ACK  : out STD_LOGIC;

			-- User ports ends
			-- Do not modify the ports beyond this line

			-- Global Clock Signal
			S_AXI_ACLK	: in std_logic;
			-- Global Reset Signal. This Signal is Active LOW
			S_AXI_ARESETN	: in std_logic;
			-- Write address (issued by master, acceped by Slave)
			S_AXI_AWADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
			-- Write channel Protection type. This signal indicates the
				-- privilege and security level of the transaction, and whether
				-- the transaction is a data access or an instruction access.
			S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
			-- Write address valid. This signal indicates that the master signaling
				-- valid write address and control information.
			S_AXI_AWVALID	: in std_logic;
			-- Write address ready. This signal indicates that the slave is ready
				-- to accept an address and associated control signals.
			S_AXI_AWREADY	: out std_logic;
			-- Write data (issued by master, acceped by Slave) 
			S_AXI_WDATA	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
			-- Write strobes. This signal indicates which byte lanes hold
				-- valid data. There is one write strobe bit for each eight
				-- bits of the write data bus.    
			S_AXI_WSTRB	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
			-- Write valid. This signal indicates that valid write
				-- data and strobes are available.
			S_AXI_WVALID	: in std_logic;
			-- Write ready. This signal indicates that the slave
				-- can accept the write data.
			S_AXI_WREADY	: out std_logic;
			-- Write response. This signal indicates the status
				-- of the write transaction.
			S_AXI_BRESP	: out std_logic_vector(1 downto 0);
			-- Write response valid. This signal indicates that the channel
				-- is signaling a valid write response.
			S_AXI_BVALID	: out std_logic;
			-- Response ready. This signal indicates that the master
				-- can accept a write response.
			S_AXI_BREADY	: in std_logic;
			-- Read address (issued by master, acceped by Slave)
			S_AXI_ARADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
			-- Protection type. This signal indicates the privilege
				-- and security level of the transaction, and whether the
				-- transaction is a data access or an instruction access.
			S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
			-- Read address valid. This signal indicates that the channel
				-- is signaling valid read address and control information.
			S_AXI_ARVALID	: in std_logic;
			-- Read address ready. This signal indicates that the slave is
				-- ready to accept an address and associated control signals.
			S_AXI_ARREADY	: out std_logic;
			-- Read data (issued by slave)
			S_AXI_RDATA	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
			-- Read response. This signal indicates the status of the
				-- read transfer.
			S_AXI_RRESP	: out std_logic_vector(1 downto 0);
			-- Read valid. This signal indicates that the channel is
				-- signaling the required read data.
			S_AXI_RVALID	: out std_logic;
			-- Read ready. This signal indicates that the master can
				-- accept the read data and response information.
			S_AXI_RREADY	: in std_logic
		);
	end component tgil_top_S00_AXI;
	
	signal adc_debug : std_logic_vector(REGISTER_W32-1 downto 0);
	
	--Debug
    signal CONFIGURATION_SIZE             	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal CONFIGURATION_ON               	: STD_LOGIC;
    
    signal CONFIGURATION_HEADER_SIZE      	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal CONFIGURATION_CHANNELS         	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal CONFIGURATION_CHANNEL0_OFFSET  	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal CONFIGURATION_ENCODER_OFFSET   	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal CONFIGURATION_DIGITALIO_OFFSET 	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal CONFIGURATION_CHANNEL0_SIZE    	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal CONFIGURATION_TRIGGER_TYPE  	  	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal CONFIGURATION_CHANNEL0_ENABLED 	: STD_LOGIC;
    signal CONFIGURATION_CHANNEL0_ON 		: STD_LOGIC;

	------------------  MAIN  ------------------
	--------------------------------------------
    
	component main is
	generic (
        -- General
        N_CHANNELS        : INTEGER := 1;
        REGISTER_W64    : INTEGER := 64;
        REGISTER_W48    : INTEGER := 48;
        REGISTER_W32    : INTEGER := 32;
        REGISTER_W21    : INTEGER := 21;
        REGISTER_W16    : INTEGER := 16;
        REGISTER_W14    : INTEGER := 14;
        REGISTER_W8     : INTEGER := 8;
        REGISTER_W6     : INTEGER := 6;
        REGISTER_W4     : INTEGER := 4;
        REGISTER_W2     : INTEGER := 2;
        
        -- Scan mode
        SCAN_MODE_NORMAL  : INTEGER := 0;
        SCAN_MODE_MRUT    : INTEGER := 10;
        
        -- Trigger type
        TRIGGER_TYPE_NORMAL     : INTEGER := 0;
        TRIGGER_TYPE_SW         : INTEGER := 10;
        TRIGGER_TYPE_ENC        : INTEGER := 20;
        TRIGGER_TYPE_ROBOT      : INTEGER := 30;
        TRIGGER_TYPE_ROBOT_ENC  : INTEGER := 40;
        
        -- external trigger
        DISPOSITION_PASS  : INTEGER := 1;
        DISPOSITION_FAIL  : INTEGER := 2;
        DISPOSITION_NODET : INTEGER := 0;
        
        -- Coinc
        TRIGGER_WAIT    : INTEGER := 32;
        MIN_COINC_LEVEL : INTEGER := 2;
        
        -- Avg
        MIN_AVG_LEVEL       : INTEGER := 2;
        AVG_TYPE_NORMAL     : INTEGER := 0;
        AVG_TYPE_PIPELINE   : INTEGER := 1;
        
        -- Ascan header size
        ASCAN_HEADER_SIZE : INTEGER := 43;
        
        -- FIR CORR
        N_TAPS              : INTEGER    := 512;
        N_SETS_TAP          : INTEGER    := 3;
        OVERSAMPLING        : INTEGER    := 4;
        WAIT_CYCLES         : INTEGER    := 4
    );
	port (
		-- sync
		aclk                    : in STD_LOGIC; 
		aresetn                 : in STD_LOGIC; 

		-- DEBUG
		CONFIGURATION_SIZE      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		CONFIGURATION_ON        : out STD_LOGIC;
		MAIN_DMA_STATE          : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);

		CONFIGURATION_HEADER_SIZE      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		CONFIGURATION_CHANNELS         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		CONFIGURATION_CHANNEL0_OFFSET  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		CONFIGURATION_ENCODER_OFFSET   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		CONFIGURATION_DIGITALIO_OFFSET : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		CONFIGURATION_CHANNEL0_SIZE    : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		CONFIGURATION_TRIGGER_TYPE     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		CONFIGURATION_CHANNEL0_ENABLED : out STD_LOGIC;
		CONFIGURATION_CHANNEL0_ON      : out STD_LOGIC;

		-- CONTROL REGISTERS
		DMA_UPLOAD_CONTROL      : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);   
		DMA_DOWNLOAD_CONTROL    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
		START_INSPECTION_CONTROL: in STD_LOGIC; 
		SCAN_MODE_CONTROL       : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
		CSCAN_MODE_CONTROL      : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
		TRIGGER_CONTROL         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		RESET_CONTROL           : in STD_LOGIC;
		ENABLE_CONTROL          : in STD_LOGIC;
		CHANNEL_CONTROL         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		-- STATUS REGISTERS
		DMA_UPLOAD_STATUS       : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);   
		DMA_DOWNLOAD_STATUS     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
		START_INSPECTION_STATUS : out STD_LOGIC; 
		SCAN_MODE_STATUS        : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
		CSCAN_MODE_STATUS       : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
		TRIGGER_STATUS          : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		RESET_STATUS            : out STD_LOGIC;
		ENABLE_STATUS           : out STD_LOGIC;
		CHANNEL_STATUS          : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

		-- ENCODER
		ENCODER1_DIR     : in STD_LOGIC_VECTOR (REGISTER_W2-1 downto 0);  
		ENCODER1_COUNT   : in STD_LOGIC_VECTOR (REGISTER_W32-1 downto 0);  

		ENCODER2_DIR     : in STD_LOGIC_VECTOR (REGISTER_W2-1 downto 0);  
		ENCODER2_COUNT   : in STD_LOGIC_VECTOR (REGISTER_W32-1 downto 0);  

		-- DIO
		IO_INPUTS   			: in  STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
		IO_OUTPUTS  			: out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
		
	    -- AVG
        DSP_AVG_LEVEL    : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
        DSP_AVG_COUNTER  : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
        DSP_AVG_TYPE     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

		---------------------------------------------- CH0 ----------------------------------------------

		CH0_CHANNEL_TRIGGER_TYPE   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		CH0_STATE_A	  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		CH0_STATE_B   : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		CH0_STATE_C   : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);

		-- Config output receiver
		CH0_RECEIVER_SAMPLING_FREQUENCY         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
		CH0_RECEIVER_DATA_WINDOW                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
		CH0_RECEIVER_DELAY                      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
		CH0_RECEIVER_ANALOG_GAIN                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
		CH0_RECEIVER_DIGITAL_GAIN               : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
		CH0_RECEIVER_EXTERNAL_MULTIPLEXER_EN    : out STD_LOGIC;

		-- Config output transmitter
		CH0_TRANSMITTER_VOLTAGE                 : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
		CH0_TRANSMITTER_BURST_FREQUENCY         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
		CH0_TRANSMITTER_N_CYCLES                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
		CH0_TRANSMITTER_N_BURST                 : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
		CH0_TRANSMITTER_DELAY                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		CH0_TRANSMITTER_DELTA_DELAY             : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		CH0_TRANSMITTER_DELTA_ADD               : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		CH0_TRANSMITTER_DIRECTIONAL_PHASING     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 

		-- Config output magnet
		CH0_MAGNET_MODE                         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
		CH0_MAGNET_PULSE_WIDTH                  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
		CH0_MAGNET_INITIAL_DELAY                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		CH0_MAGNET_RAMP_UP_VOLTAGE              : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
		CH0_MAGNET_VOLTAGE                      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);   

		-- Config analog filters
		CH0_DSP_ANALOG_FILTER  					: out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
		CH0_DSP_COINC_LEVEL                     : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
		CH0_DSP_AVG_LEVEL                       : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);

		CH0_DSP_BURST_LEVEL                     : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
		CH0_DSP_BURST_COUNTER					: out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);

		-- Config DAC curves
		CH0_CONFIG_DAC_AXIS_TREADY    : in STD_LOGIC;
		CH0_CONFIG_DAC_AXIS_TDATA	  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		CH0_CONFIG_DAC_AXIS_TLAST	  : out STD_LOGIC;
		CH0_CONFIG_DAC_AXIS_TVALID    : out STD_LOGIC;

		-- Signal input
		CH0_SIGNAL_COINC_WINDOW  	  : out STD_LOGIC;
		CH0_SIGNAL_AVG_WINDOW  		  : out STD_LOGIC;

		CH0_SIGNAL_INPUT_AXIS_TREADY  : out STD_LOGIC;
		CH0_SIGNAL_INPUT_AXIS_TDATA	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		CH0_SIGNAL_INPUT_AXIS_TLAST	  : in STD_LOGIC;
		CH0_SIGNAL_INPUT_AXIS_TVALID  : in STD_LOGIC;

		-- SYSTEM SIGNALS
		SW_RESET                : out STD_LOGIC;

		-- DMA INPUT
		DMA_S_axis_tready       : out STD_LOGIC;
		DMA_S_axis_tkeep        : in STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0);
		DMA_S_axis_tdata        : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		DMA_S_axis_tlast        : in STD_LOGIC;
		DMA_S_axis_tvalid       : in STD_LOGIC;
		-- DMA OUTPUT
		DMA_M_axis_tvalid       : out STD_LOGIC;
		DMA_M_axis_tdata        : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		DMA_M_axis_tlast        : out STD_LOGIC;
		DMA_M_axis_tkeep        : out STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0);
		DMA_M_axis_tready       : in STD_LOGIC

	);
	end component main;

    signal SW_RESET                : STD_LOGIC;
    signal MAIN_DMA_STATE          : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    
	-- CONTROL REGISTERS
	signal DMA_UPLOAD_CONTROL      : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);   
	signal DMA_DOWNLOAD_CONTROL    : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
	signal START_INSPECTION_CONTROL: STD_LOGIC; 
	signal SCAN_MODE_CONTROL       : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
	signal CSCAN_MODE_CONTROL      : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
	signal TRIGGER_CONTROL         : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal RESET_CONTROL           : STD_LOGIC;
	signal ENABLE_CONTROL          : STD_LOGIC;
	signal CHANNEL_CONTROL         : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

	-- STATUS REGISTERS
	signal DMA_UPLOAD_STATUS       : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);   
	signal DMA_DOWNLOAD_STATUS     : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
	signal START_INSPECTION_STATUS : STD_LOGIC; 
	signal SCAN_MODE_STATUS        : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
	signal CSCAN_MODE_STATUS       : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
	signal TRIGGER_STATUS          : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal RESET_STATUS            : STD_LOGIC;
	signal ENABLE_STATUS           : STD_LOGIC;
	signal CHANNEL_STATUS          : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

    -- Encoder 1,2
    --signal 
        	  
	signal io_inputs_i   :  STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
	signal io_outputs_i  : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
	
	signal DSP_AVG_LEVEL    : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
	signal DSP_AVG_COUNTER  : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
	signal DSP_AVG_TYPE     : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

	-------- CH0 --------
	
	signal CH0_CHANNEL_TRIGGER_TYPE   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH0_STATE_A	 : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal CH0_STATE_B   : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal CH0_STATE_C   : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);

	-- Config output receiver
	signal CH0_RECEIVER_SAMPLING_FREQUENCY         : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
	signal CH0_RECEIVER_DATA_WINDOW                : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
	signal CH0_RECEIVER_DELAY                      : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
	signal CH0_RECEIVER_ANALOG_GAIN                : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH0_RECEIVER_DIGITAL_GAIN               : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
	signal CH0_RECEIVER_EXTERNAL_MULTIPLEXER_EN    : STD_LOGIC;

	-- Config output transmitter
	signal CH0_TRANSMITTER_VOLTAGE                 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
	signal CH0_TRANSMITTER_BURST_FREQUENCY         : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
	signal CH0_TRANSMITTER_N_CYCLES                : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    signal CH0_TRANSMITTER_N_BURST                 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
	signal CH0_TRANSMITTER_DELAY                   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH0_TRANSMITTER_DELTA_DELAY             : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH0_TRANSMITTER_DELTA_ADD               : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH0_TRANSMITTER_DIRECTIONAL_PHASING     : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 

	-- Config output magnet
	signal CH0_MAGNET_MODE                         : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
	signal CH0_MAGNET_PULSE_WIDTH                  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
	signal CH0_MAGNET_INITIAL_DELAY                : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH0_MAGNET_RAMP_UP_VOLTAGE              : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    signal CH0_MAGNET_VOLTAGE                      : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);	
	
	-- Config analog filters
	signal CH0_DSP_ANALOG_FILTER  				   : STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
	signal CH0_DSP_COINC_LEVEL                     : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
    signal CH0_DSP_AVG_LEVEL                       : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);

	signal CH0_DSP_BURST_LEVEL                     : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
    signal CH0_DSP_BURST_COUNTER                   : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
      
	-- Config DAC curves
	signal CH0_CONFIG_DAC_AXIS_TREADY    	: STD_LOGIC;
	signal CH0_CONFIG_DAC_AXIS_TDATA	  	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH0_CONFIG_DAC_AXIS_TLAST	  	: STD_LOGIC;
	signal CH0_CONFIG_DAC_AXIS_TVALID    	: STD_LOGIC;

	-- Signal input
	signal CH0_SIGNAL_COINC_WINDOW  	  : STD_LOGIC;
	signal CH0_SIGNAL_AVG_WINDOW  		  : STD_LOGIC;

	signal CH0_SIGNAL_INPUT_AXIS_TREADY  	: STD_LOGIC;
	signal CH0_SIGNAL_INPUT_AXIS_TDATA	  	: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal CH0_SIGNAL_INPUT_AXIS_TLAST	  	: STD_LOGIC;
	signal CH0_SIGNAL_INPUT_AXIS_TVALID  	: STD_LOGIC;
	
	signal ch0_signal_input_axis_tready_i  	: STD_LOGIC;
    signal ch0_signal_input_axis_tdata_i    : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal ch0_signal_input_axis_tlast_i    : STD_LOGIC;
    signal ch0_signal_input_axis_tvalid_i   : STD_LOGIC;

	------------------  DIO  ------------------
	-------------------------------------------

	component dio is
		generic (
			REGISTER_W8     : INTEGER := 8; 
			REGISTER_W6     : INTEGER := 6; 
			FILTER_LEVEL    : INTEGER := 4
		);
		port (
			-- Sync
			aclk 	: in STD_LOGIC;
			aresetn	: in STD_LOGIC;

			-- Input
			IO_INPUTS_I 	: in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);	
			IO_INPUTS_O 	: out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);

			-- Output
			RX_DATA_WINDOW_ON : in STD_LOGIC;
			IO_OUTPUT_I 	: in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);	
			IO_OUTPUT_O 	: out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0)            
		);
	end component dio;
		
	------------------  PULSER & RECEIVER  ------------------
	---------------------------------------------------------

    component pulser_receiver is
        generic (
            REGISTER_W32    : INTEGER := 32;
            REGISTER_W30    : INTEGER := 30;
            REGISTER_W16    : INTEGER := 16;
            REGISTER_W14    : INTEGER := 14;
            REGISTER_W8     : INTEGER := 8;
            REGISTER_W7     : INTEGER := 7;
            REGISTER_W4     : INTEGER := 4;
            REGISTER_W2     : INTEGER := 2;
            CONFIG_DONE_WAIT: INTEGER := 4;
            SAMPLE_FREQ     : INTEGER := 100;
            N_CONFIG_REGISTERS : INTEGER := 5;
            SPI_WAIT_CYCLES : INTEGER := 512;
            SPI_2X_CLK_DIV  : INTEGER := 100       
        );
        port (
            -- Sync
            aclk	    : in STD_LOGIC;
            aclk_100 	: in STD_LOGIC;
            aclk_200 	: in STD_LOGIC;
            aresetn		: in STD_LOGIC;
            
            -------- PULSER --------
            ------------------------
            
            -- Control
            BURST_LEVEL             : in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
            BURST_COUNTER           : in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
            
            -- Magnet  
            MAGNET_MODE             : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
            MAGNET_PULSE_WIDTH      : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
            MAGNET_INITIAL_DELAY    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            
            MAG_PULSE               : out STD_LOGIC;
            
            -- Burst sequence
            TX_DATA_DELAY			: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);		--delays in 100MHz samples
            TX_DELTA_DELAY			: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);		--delays in 100MHz samples
            TX_DELTA_ADD            : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);     --delays in 100MHz samples
            RX_SAMPLING_FREQUENCY  	: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  	-- 100 MHz divider for sample freq
            RX_DATA_DELAY			: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);		--delays in 100MHz samples
            RX_DATA_WINDOW			: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);		--data window in sample freq samples
            
            RX_DATA_WINDOW_ON		: out STD_LOGIC;
                    
            -- Tx Outputs
            HALF_CYCLE_FREQ	: in STD_LOGIC_VECTOR(REGISTER_W30-1 downto 0);
            NO_HALF_CYCLES	: in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
            START_CYCLE		: in STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
            
            TXp		: out STD_LOGIC;
            TXn		: out STD_LOGIC; 
            
            -------- RECEIVER --------
            --------------------------
    
            -------- ADC --------		
            -- Programing mode seletion
            ADC_PAR_SERn : out STD_LOGIC;
    
            -- encode (type of edge for conversion starts)
            ADC_ENCp     : out STD_LOGIC;
            ADC_ENCn     : out STD_LOGIC;
    
            -- Data output clock
            ADC_CLKOUTp  : in STD_LOGIC;
            ADC_CLKOUTn  : in STD_LOGIC;
    
            -- Overflow
            ADC_OFp      : in STD_LOGIC;
            ADC_OFn      : in STD_LOGIC;
    
            -- Data in 
            ADC_DATAp    : in STD_LOGIC_VECTOR(REGISTER_W7-1 downto 0);
            ADC_DATAn    : in STD_LOGIC_VECTOR(REGISTER_W7-1 downto 0);
    
            -- External config		
            ADC_RESET_EX     		: in STD_LOGIC;	
            ADC_CONFIG_EX     		: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            ADC_CONFIG_EX_VALID     : in STD_LOGIC;
            ADC_CONFIG_LOADED 		: out STD_LOGIC;
            ADC_CONFIG_CURRENT     	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    
            ADC_CONFIG_REQUEST		: out STD_LOGIC;
            ADC_CONFIG_REQUEST_ACK	: in STD_LOGIC;
    
            ADC_CONFIG_EX_DONE		: out STD_LOGIC;
    
            -- SPI
            ADC_SPI_CONFIG : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0); -- debug
    
            ADC_CSn      : out STD_LOGIC;
            ADC_SCK      : out STD_LOGIC;
            ADC_SDI      : out STD_LOGIC;
            ADC_SDO      : in STD_LOGIC;  
            
            -- DEBUG --
            ADC_WRITE_COUNTER : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);  
            ADC_READ_COUNTER : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
            ADC_ENC     : out STD_LOGIC;
            ADC_CLKOUT  : out STD_LOGIC;
            ADC_DATA    : out STD_LOGIC_VECTOR(REGISTER_W14-1 downto 0);
            ADC_SPI_CSn      : out STD_LOGIC;
            ADC_SPI_SCK      : out STD_LOGIC;
            ADC_SPI_SDI      : out STD_LOGIC;
            ADC_SPI_SDO      : out STD_LOGIC;
            
            -------- CHANNEL RECEIVER --------
            -- Receiver data stream output to other units
            CH_SIGNAL_AXIS_TREADY  					: in STD_LOGIC;
            CH_SIGNAL_AXIS_TDATA	  				: out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
            CH_SIGNAL_AXIS_TLAST	  				: out STD_LOGIC;
            CH_SIGNAL_AXIS_TVALID  					: out STD_LOGIC          
        );
    end component pulser_receiver;

--	signal adc_enc_p     	: STD_LOGIC;
--	signal adc_enc_n     	: STD_LOGIC;
	signal adc_enc     	    : STD_LOGIC;
--	signal adc_clkout_p     : STD_LOGIC;
--	signal adc_clkout_n     : STD_LOGIC;
	signal adc_clkout       : STD_LOGIC;

--	signal adc_data_p     		: STD_LOGIC_VECTOR(REGISTER_W7-1 downto 0);
--	signal adc_data_n     		: STD_LOGIC_VECTOR(REGISTER_W7-1 downto 0);
	signal adc_data     		: STD_LOGIC_VECTOR(REGISTER_W14-1 downto 0);

	signal adc_spi_cs_n     : STD_LOGIC;
	signal adc_spi_sck     	: STD_LOGIC;
	signal adc_spi_sdi     	: STD_LOGIC;
	signal adc_spi_sdo     	: STD_LOGIC;

	signal pulser_control_p     	: STD_LOGIC;
	signal pulser_control_n     	: STD_LOGIC;
	
	signal rx_data_window_on		: std_logic;

	-- ADC config
	signal ADC_CONFIG_EX     		: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal ADC_CONFIG_EX_VALID     	: STD_LOGIC;
	signal ADC_CONFIG_LOADED 		: STD_LOGIC;
	signal ADC_CONFIG_CURRENT     	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal ADC_CONFIG_REQUEST		: STD_LOGIC;
	signal ADC_CONFIG_REQUEST_ACK	: STD_LOGIC;
	signal ADC_CONFIG_EX_DONE		: STD_LOGIC;
	signal ADC_SPI_CONFIG 			: STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0); -- debug
	
	signal ADC_WRITE_COUNTER     	: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal ADC_READ_COUNTER     	: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);	
	
	------------------  DAC AMPLIFIER  ------------------
	-----------------------------------------------------

	component dac is
		generic (
			REGISTER_W32    : INTEGER := 32;
			REGISTER_W10    : INTEGER := 10
		);
		port (
			-- Sync
			aclk 	: in STD_LOGIC;
			aresetn	: in STD_LOGIC;

			-- Input
			RECEIVER_ANALOG_GAIN	: in STD_LOGIC_VECTOR(REGISTER_W10-1 downto 0);	
			START_INSPECTION		: in STD_LOGIC;

			-- Output
			DAC_GAIN  	: out STD_LOGIC_VECTOR(REGISTER_W10-1 downto 0);  		
			DAC_CLK   	: out STD_LOGIC; 
			DAC_SLEEP 	: out STD_LOGIC             
		);
	end component dac;

	signal dac_gain_i  		: STD_LOGIC_VECTOR(REGISTER_W10-1 downto 0);
	signal dac_clk_i    	: STD_LOGIC; 
	signal dac_sleep_i    	: STD_LOGIC; 

	------------------  QUAD ENCODER  ------------------
	----------------------------------------------------

	component Quad_Encoder is
        Port (  clock           : in STD_LOGIC; 
            reset           : in STD_LOGIC; 
--			Enc_Set			: in STD_LOGIC; 
            Enc_CHA         : in STD_LOGIC; 
            Enc_CHB        	: in STD_LOGIC; 
--			Enc_Count_Set	: in STD_LOGIC_VECTOR (31 downto 0);
            Enc_Dir        	: out STD_LOGIC_VECTOR (1 downto 0);  -- Direction 00 unknown 01 positive 10 negative 11 error
            Enc_Raw_Count 	: out STD_LOGIC_VECTOR (31 downto 0)  -- Distance in pulses (ticks)
        );
	end component Quad_Encoder;
	
    signal ENCODER1_DIR     : STD_LOGIC_VECTOR (REGISTER_W2-1 downto 0);  -- Direction 00 unknown 01 positive 10 negative 11 error
    signal ENCODER1_COUNT   : STD_LOGIC_VECTOR (REGISTER_W32-1 downto 0);  -- Distance in pulses (ticks)
    
    signal ENCODER2_DIR     : STD_LOGIC_VECTOR (REGISTER_W2-1 downto 0);  -- Direction 00 unknown 01 positive 10 negative 11 error
    signal ENCODER2_COUNT   : STD_LOGIC_VECTOR (REGISTER_W32-1 downto 0);  -- Distance in pulses (ticks)


	------------------  TEMP SENSOR  ------------------
	----------------------------------------------------

	component temperature_sensor is
		generic (
			REGISTER_W32       : INTEGER := 32;
			REGISTER_W16       : INTEGER := 16;
			TEMP_MEASURE_CYCLE : INTEGER := 60; -- in secs 
			TEMP_WORD_WIDTH    : INTEGER := 16;
			SPI_CS_SCK_CYCLES  : INTEGER := 2;
			SPI_CLK_DIV        : INTEGER := 100       
		);
		port (
			-- Sync
			aclk         : in STD_LOGIC;
			aresetn      : in STD_LOGIC;

			TEMPERATURE   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

			TEMP_CSn      : out STD_LOGIC;
			TEMP_SCK      : out STD_LOGIC;
			TEMP_SDO      : in STD_LOGIC      
		);
	end component temperature_sensor;

    signal temperature   : STD_LOGIC_VECTOR (REGISTER_W32-1 downto 0);
    signal temp_sdo      : STD_LOGIC; 

	------------------  CLK & RESET ------------------
	--------------------------------------------------
	
    component clk_wiz_0
       port(
           -- Clock in ports
           clk_in1         : in     std_logic;
           -- Clock out ports
           clk_out1          : out    std_logic;
           clk_out2          : out    std_logic;
           clk_out3          : out    std_logic;
           clk_out4          : out    std_logic;
           -- Status and control signals
           reset             : in     std_logic;
           locked            : out    std_logic
       );
     end component;
     
    signal aclk         : STD_LOGIC;
	
    signal aclk_100     : STD_LOGIC;
    signal aclk_100_main     : STD_LOGIC;
    signal aclk_200     : STD_LOGIC;
    signal aclk_50      : STD_LOGIC;
      
    signal aresetn_i    : STD_LOGIC_VECTOR ( 0 downto 0 );
    signal aresetn      : STD_LOGIC;

	signal areset      	: STD_LOGIC;

	signal aresetn_sync : STD_LOGIC;
	signal areset_sync  : STD_LOGIC;

  	signal mmcm_locked	: std_logic;

	------------------  DEBUG FACILITIES ------------------
	-------------------------------------------------------

--	COMPONENT vio_0
--	  PORT (
--		clk : IN STD_LOGIC;
--		probe_in0 : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--		probe_out0 : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
--		probe_out1 : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
--	  );
--	END COMPONENT;
	
--	signal vio_probe_in0  : STD_LOGIC_VECTOR(3 DOWNTO 0);
--	signal vio_probe_out0 : STD_LOGIC_VECTOR(3 DOWNTO 0);
--	signal vio_probe_out1 : STD_LOGIC_VECTOR(31 DOWNTO 0);

--	COMPONENT ila_100

--	PORT (
--		clk : IN STD_LOGIC;
--		trig_in : IN STD_LOGIC;
--		trig_in_ack : OUT STD_LOGIC;
--		probe0 : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
--		probe1 : IN STD_LOGIC_VECTOR(13 DOWNTO 0)
--	);
--	END COMPONENT  ;
		
--	signal ila_trig_in 		: STD_LOGIC;
--	signal ila_trig_in_ack 	: STD_LOGIC;
--	signal ila_probe0   : STD_LOGIC_VECTOR (19 downto 0);
--	signal ila_probe1   : STD_LOGIC_VECTOR (13 downto 0);
	
--	signal trigger_adc_on : STD_LOGIC;
--	signal trigger_dac_on : STD_LOGIC;
--	signal trigger_pulser_on : STD_LOGIC;
	
--	signal trigger_adc : STD_LOGIC;
--    signal trigger_dac : STD_LOGIC;
--    signal trigger_pulser : STD_LOGIC;
    
    signal reset_adc : STD_LOGIC;
    signal trigger_sw_adc : STD_LOGIC;
  	
begin

	------------------  DEBUG FACILITIES ------------------
	-------------------------------------------------------
	
	reset_adc <= adc_debug(0);
    trigger_sw_adc <= adc_debug(1);
	
--	vio_probe_in0 <= "0" & trigger_dac_on & trigger_pulser_on & trigger_adc_on;
	
--	vio_0_inst : vio_0
--	  	PORT MAP (
--			clk => aclk,
--			probe_in0 => vio_probe_in0,
--			probe_out0 => vio_probe_out0,
--			probe_out1 => vio_probe_out1
--	  	);
	
--	trigger_adc_on <= vio_probe_out0(0);
--	trigger_pulser_on <= vio_probe_out0(1);
--	trigger_dac_on <= vio_probe_out0(2);
	
--	trigger_adc <= trigger_adc_on and (reset_adc or trigger_sw_adc);
--	trigger_dac <= trigger_dac_on;
--	trigger_pulser <= trigger_pulser_on and pulser_control_p;
	
--	ila_trig_in <= trigger_adc or trigger_dac or trigger_pulser;
	
--	ila_100_inst : ila_100
--		PORT MAP (
--			clk => aclk,
--			--clk => aclk_200,
--			trig_in => ila_trig_in,
--			trig_in_ack => ila_trig_in_ack,
--			probe0 => ila_probe0,
--			probe1 => ila_probe1
--		);
		
--	ila_probe0 <= adc_spi_cs_n & adc_spi_sck & adc_spi_sdi & adc_spi_sdo & adc_enc & adc_clkout & adc_data;
--	ila_probe1 <= pulser_control_p & pulser_control_n & dac_sleep_i & dac_clk_i & dac_gain_i;
			
	------------------  MAG_PULSE ------------------
	--------------------------------------------------

    --MAG_PULSE1 <= '0';
    --MAG_PULSE2 <= '0';
	
	------------------  CLK & RESET ------------------
	--------------------------------------------------
	
	areset <= not aresetn;
	
    Inst_MMCM_200 : clk_wiz_0
        port map ( 

            -- Clock in ports
            clk_in1 => aclk,
            -- Clock out ports  
            clk_out1 => aclk_200,
            clk_out2 => aclk_100,
            clk_out3 => aclk_50,
            clk_out4 => aclk_100_main,
            -- Status and control signals                
            reset => areset,
            locked => mmcm_locked            
        );
		 
    sync_reset: process(aclk_100, aresetn, mmcm_locked)   
    --sync_reset: process(aclk_100, aresetn) 
    begin
        --if ( (aresetn = '0') or (mmcm_locked = '0')) then
		if ( (aresetn = '0') or (mmcm_locked = '0') or (SW_RESET = '1')) then
		--if ( aresetn = '0') then
			aresetn_sync <= '0';
		elsif (rising_edge(aclk_100)) then
			aresetn_sync <= '1';	
		end if;
	end process;
			
	areset_sync <= not(aresetn_sync); 
							

--    attribute HIODELAY_GROUP: string;
--    attribute HIODELAY_GROUP of AdcIdlyCtrl_l : label is " AdcIdlyCtrl"
--    AdcIdlyCtrl_l : IDELAYCTRL 
--    port map (REFCLK => Clk200M_s, RST => Reset125_s, RDY => AdcIdlyCtrlRdy_s);
			
	------------------  DIO  ------------------
	-------------------------------------------
		
	dio_inst: dio
		generic map(
			REGISTER_W8     => REGISTER_W8,
			REGISTER_W6     => REGISTER_W6
		)
		port map(
			-- Sync
			aclk 	=> aclk_100_main,
			aresetn	=> aresetn_sync,

			-- Input
			IO_INPUTS_I 	=> IO_INPUTS,
			IO_INPUTS_O 	=> io_inputs_i,

			-- Output
			RX_DATA_WINDOW_ON => rx_data_window_on,
			IO_OUTPUT_I 	=> io_outputs_i,
			IO_OUTPUT_O 	=> IO_OUTPUTS           
		);
			
	--IO_OUTPUTS(6) <= hvps_en; 
	--IO_OUTPUTS(7) not conected to the baseboard
	--IO_INPUTS_I(7) <= encoder_alarm; 
	--IO_INPUTS_I(6) <= SDO temp sensor; 

	------------------  PULSER & RECEIVER  ------------------
	---------------------------------------------------------
					
	PULSER_CONTROLp <= pulser_control_p;
	PULSER_CONTROLn <= pulser_control_n;
				
	pulser_receiver_inst: pulser_receiver 
		generic map(
			REGISTER_W32    => REGISTER_W32,
			REGISTER_W30    => REGISTER_W30,
			REGISTER_W16    => REGISTER_W16,
			REGISTER_W14    => REGISTER_W14,
			REGISTER_W8     => REGISTER_W8,
			REGISTER_W7     => REGISTER_W7,
			REGISTER_W4     => REGISTER_W4,
			REGISTER_W2     => REGISTER_W2   
		)
		port map(
			-- Sync
			aclk        => aclk,
			aclk_100 	=> aclk_100,
			aclk_200 	=> aclk_100,
			aresetn		=> aresetn_sync,

			-------- PULSER --------
			------------------------
			
            -- Control
            BURST_LEVEL             => CH0_DSP_BURST_LEVEL,
            BURST_COUNTER           => CH0_DSP_BURST_COUNTER,
            
            -- Magnet  
            MAGNET_MODE             => CH0_MAGNET_MODE,
            MAGNET_PULSE_WIDTH      => CH0_MAGNET_PULSE_WIDTH,
            MAGNET_INITIAL_DELAY    => CH0_MAGNET_INITIAL_DELAY,
            
            MAG_PULSE               => open, -- link to general output

			-- Burst sequence
			TX_DATA_DELAY			=> CH0_TRANSMITTER_DELAY,
			TX_DELTA_DELAY			=> CH0_TRANSMITTER_DELTA_DELAY,
			TX_DELTA_ADD			=> CH0_TRANSMITTER_DELTA_ADD,
			RX_SAMPLING_FREQUENCY  	=> CH0_RECEIVER_SAMPLING_FREQUENCY,
			RX_DATA_DELAY			=> CH0_RECEIVER_DELAY,
			RX_DATA_WINDOW			=> CH0_RECEIVER_DATA_WINDOW,		
            
			RX_DATA_WINDOW_ON       => rx_data_window_on,

			-- Tx Outputs
			HALF_CYCLE_FREQ	=> CH0_TRANSMITTER_BURST_FREQUENCY(29 downto 0),
			NO_HALF_CYCLES	=> CH0_TRANSMITTER_N_CYCLES(7 downto 0),	
			
			START_CYCLE     => "00",

			TXp		=> pulser_control_p,
			TXn		=> pulser_control_n,

			-------- RECEIVER --------
			--------------------------

			-------- ADC --------		
			-- Programing mode seletion
			ADC_PAR_SERn => ADC_PAR_SERn,
			--ADC_PAR_SERn => open,

			-- encode (type of edge for conversion starts)
--			ADC_ENCp     => adc_enc_p,
--			ADC_ENCn     => adc_enc_n,
			ADC_ENCp     => ADC_ENCp,
			ADC_ENCn     => ADC_ENCn,
			
			-- Data output clock
			ADC_CLKOUTp  => ADC_CLKOUTp,
			ADC_CLKOUTn  => ADC_CLKOUTn,

			-- Overflow
			ADC_OFp      => ADC_OFp,
			ADC_OFn      => ADC_OFn,

			-- Data in 
			ADC_DATAp    => ADC_DATAp,
			ADC_DATAn    => ADC_DATAn,

			-- External config		
			ADC_RESET_EX            => reset_adc,
			
			ADC_CONFIG_EX     		=> ADC_CONFIG_EX,
			ADC_CONFIG_EX_VALID     => ADC_CONFIG_EX_VALID,
			ADC_CONFIG_LOADED 		=> ADC_CONFIG_LOADED,
			ADC_CONFIG_CURRENT     	=> ADC_CONFIG_CURRENT,

			ADC_CONFIG_REQUEST		=> ADC_CONFIG_REQUEST,
			ADC_CONFIG_REQUEST_ACK	=> ADC_CONFIG_REQUEST_ACK,

			ADC_CONFIG_EX_DONE		=> ADC_CONFIG_EX_DONE,

			-- SPI
			ADC_SPI_CONFIG => ADC_SPI_CONFIG,

			ADC_CSn      => ADC_CSn,
			ADC_SCK      => ADC_SCK,
			ADC_SDI      => ADC_SDI,
			ADC_SDO      => ADC_SDO,
			
			-- DEBUG --
            ADC_WRITE_COUNTER => ADC_WRITE_COUNTER,
            ADC_READ_COUNTER  => ADC_READ_COUNTER,
            ADC_ENC => adc_enc,
            ADC_CLKOUT => adc_clkout,
            ADC_DATA => adc_data,
			ADC_SPI_CSn => adc_spi_cs_n,
            ADC_SPI_SCK => adc_spi_sck,
            ADC_SPI_SDI => adc_spi_sdi,
            ADC_SPI_SDO => adc_spi_sdo,

			-------- CHANNEL RECEIVER --------
			-- Receiver data stream output to other units
			CH_SIGNAL_AXIS_TREADY	=> CH0_SIGNAL_INPUT_AXIS_TREADY,

			CH_SIGNAL_AXIS_TDATA	=> CH0_SIGNAL_INPUT_AXIS_TDATA,
			CH_SIGNAL_AXIS_TLAST	=> CH0_SIGNAL_INPUT_AXIS_TLAST,
			CH_SIGNAL_AXIS_TVALID  	=> CH0_SIGNAL_INPUT_AXIS_TVALID       
		);
		
	-- For the moment external config of ADC is not used
--	ADC_CONFIG_EX <= (others => '0');	 
--	ADC_CONFIG_EX_VALID <= '0';
--	ADC_CONFIG_REQUEST_ACK <= '0';	

	------------------  DAC AMPLIFIER  ------------------
	-----------------------------------------------------

	AD9760ARUZ50_dac: dac
		generic map(
			REGISTER_W32    => REGISTER_W32,
			REGISTER_W10    => REGISTER_W10
		)
		port map(
			-- Sync
			aclk 	=> aclk_50,
			aresetn	=> aresetn_sync,

			-- Input
			RECEIVER_ANALOG_GAIN	=> CH0_RECEIVER_ANALOG_GAIN(REGISTER_W10-1 downto 0),	
			START_INSPECTION		=> START_INSPECTION_CONTROL,

			-- Output
			DAC_GAIN  	=> dac_gain_i,  		
			DAC_CLK   	=> dac_clk_i,
			DAC_SLEEP 	=> dac_sleep_i          
		);
		
	DAC_GAIN <= dac_gain_i;
	DAC_CLK <= dac_clk_i;
	DAC_SLEEP <= dac_sleep_i;
	
	------------------  QUAD ENCODER  ------------------
	----------------------------------------------------

	Inst_Quad_Encoder1 : Quad_Encoder
        Port map(  
            clock	=> aclk_100_main, 
            reset 	=> areset_sync,
--			Enc_Set			: in STD_LOGIC; 
            Enc_CHA => ENCA,
            Enc_CHB => ENCB,
--			Enc_Count_Set	: in STD_LOGIC_VECTOR (31 downto 0);
            Enc_Dir => ENCODER1_DIR,
            Enc_Raw_Count => ENCODER1_COUNT
        );
	
	-- There is not physical encoder 2
	ENCODER2_DIR 	<= (others => '0');
	ENCODER2_COUNT 	<= (others => '0');

	------------------  TEMP SENSOR  ------------------
	----------------------------------------------------

	MAX6675_temperature_sensor: temperature_sensor
		generic map(
			REGISTER_W32	=> REGISTER_W32,
			REGISTER_W16    => REGISTER_W16
		)
		port map(
			-- Sync
			aclk 		=> aclk_100_main,
			aresetn     => aresetn_sync,

			TEMPERATURE => temperature,

			TEMP_CSn	=> TEMP_CSn(0),
			TEMP_SCK    => TEMP_SCK,
			TEMP_SDO    => temp_sdo   
		);

    -- For the moment not used
    TEMP_CSn(1) <= '0';
    TEMP_CSn(2) <= '0';
    
    -- SDO conected to input
    temp_sdo  <= IO_INPUTS(6);
	  
	------------------  ZYNQ WRAPPER  ------------------
	----------------------------------------------------
 	
	design_top_wrapper_inst: design_top_wrapper
	  port map (
        DDR_addr    => DDR_addr, 
        DDR_ba      => DDR_ba,
        DDR_cas_n   => DDR_cas_n,
        DDR_ck_n    => DDR_ck_n,
        DDR_ck_p    => DDR_ck_p,
        DDR_cke     => DDR_cke,
        DDR_cs_n    => DDR_cs_n,
        DDR_dm      => DDR_dm,
        DDR_dq      => DDR_dq,
        DDR_dqs_n   => DDR_dqs_n,
        DDR_dqs_p   => DDR_dqs_p,
        DDR_odt     => DDR_odt,
        DDR_ras_n   => DDR_ras_n,
        DDR_reset_n => DDR_reset_n,
        DDR_we_n    => DDR_we_n,
		  
		FCLK_CLK0 	=> aclk,
--		FCLK_CLK1 	=> aclk_200,
--		FCLK_CLK2 	=> aclk_50,
		  
        FIXED_IO_ddr_vrn    => FIXED_IO_ddr_vrn,
        FIXED_IO_ddr_vrp    => FIXED_IO_ddr_vrp,
        FIXED_IO_mio        => FIXED_IO_mio,
        FIXED_IO_ps_clk     => FIXED_IO_ps_clk,
        FIXED_IO_ps_porb    => FIXED_IO_ps_porb,
        FIXED_IO_ps_srstb   => FIXED_IO_ps_srstb,
		  
        M00_AXI_ps_araddr  => M00_AXI_ps_araddr,
        M00_AXI_ps_arprot  => M00_AXI_ps_arprot,
        M00_AXI_ps_arready => M00_AXI_ps_arready,
        M00_AXI_ps_arvalid => M00_AXI_ps_arvalid,
        M00_AXI_ps_awaddr  => M00_AXI_ps_awaddr,
        M00_AXI_ps_awprot  => M00_AXI_ps_awprot,
        M00_AXI_ps_awready => M00_AXI_ps_awready,
        M00_AXI_ps_awvalid => M00_AXI_ps_awvalid,
        M00_AXI_ps_bready  => M00_AXI_ps_bready,
        M00_AXI_ps_bresp   => M00_AXI_ps_bresp,
        M00_AXI_ps_bvalid  => M00_AXI_ps_bvalid,
        M00_AXI_ps_rdata   => M00_AXI_ps_rdata,
        M00_AXI_ps_rready  => M00_AXI_ps_rready,
        M00_AXI_ps_rresp   => M00_AXI_ps_rresp,
        M00_AXI_ps_rvalid  => M00_AXI_ps_rvalid,
        M00_AXI_ps_wdata   => M00_AXI_ps_wdata,
        M00_AXI_ps_wready  => M00_AXI_ps_wready,
        M00_AXI_ps_wstrb   => M00_AXI_ps_wstrb,
        M00_AXI_ps_wvalid  => M00_AXI_ps_wvalid,
		  
		M_AXIS_MM2S_dma_0_tdata 	=> S_AXIS_dma_0_tdata,
		M_AXIS_MM2S_dma_0_tkeep 	=> S_AXIS_dma_0_tkeep,
		M_AXIS_MM2S_dma_0_tlast 	=> S_AXIS_dma_0_tlast,
		M_AXIS_MM2S_dma_0_tready 	=> S_AXIS_dma_0_tready,
		M_AXIS_MM2S_dma_0_tvalid 	=> S_AXIS_dma_0_tvalid,
		  
		S_AXIS_dma_0_tdata 	=> M_AXIS_MM2S_dma_0_tdata,
		S_AXIS_dma_0_tkeep 	=> M_AXIS_MM2S_dma_0_tkeep,
		S_AXIS_dma_0_tlast 	=> M_AXIS_MM2S_dma_0_tlast,
		S_AXIS_dma_0_tready => M_AXIS_MM2S_dma_0_tready,
		S_AXIS_dma_0_tvalid => M_AXIS_MM2S_dma_0_tvalid,
		  
		peripheral_aresetn 	=> aresetn_i
	  );
	  	
	aresetn <= aresetn_i(0); 
	
	------------------  S00_AXI  ------------------
	-----------------------------------------------

	tgil_top_S00_AXI_inst: tgil_top_S00_AXI
		generic map(
		    REGISTER_W32        => REGISTER_W32,
		    REGISTER_W16        => REGISTER_W16,
		    REGISTER_W8         => REGISTER_W8,
			REGISTER_W6     	=> REGISTER_W6,
			REGISTER_W4     	=> REGISTER_W4,
			C_S_AXI_DATA_WIDTH	=> C_S_AXI_DATA_WIDTH,
			C_S_AXI_ADDR_WIDTH	=> C_S_AXI_ADDR_WIDTH
		)
		port map(

			DMA_UPLOAD_CONTROL      => DMA_UPLOAD_CONTROL,   
			DMA_DOWNLOAD_CONTROL    => DMA_DOWNLOAD_CONTROL,
			START_INSPECTION_CONTROL=> START_INSPECTION_CONTROL,
			SCAN_MODE_CONTROL       => SCAN_MODE_CONTROL, 
			CSCAN_MODE_CONTROL      => CSCAN_MODE_CONTROL,  
			TRIGGER_CONTROL         => TRIGGER_CONTROL,
			RESET_CONTROL           => RESET_CONTROL,
			ENABLE_CONTROL          => ENABLE_CONTROL,
			CHANNEL_CONTROL         => CHANNEL_CONTROL,

			DMA_UPLOAD_STATUS       => DMA_UPLOAD_STATUS,  
			DMA_DOWNLOAD_STATUS     => DMA_DOWNLOAD_STATUS, 
			START_INSPECTION_STATUS => START_INSPECTION_STATUS,
			SCAN_MODE_STATUS        => SCAN_MODE_STATUS,
			CSCAN_MODE_STATUS       => CSCAN_MODE_STATUS,  
			TRIGGER_STATUS          => TRIGGER_STATUS,
			RESET_STATUS            => RESET_STATUS,
			ENABLE_STATUS           => ENABLE_STATUS,
			CHANNEL_STATUS          => CHANNEL_STATUS,
			
			DSP_AVG_LEVEL           => DSP_AVG_LEVEL,
            DSP_AVG_COUNTER         => DSP_AVG_COUNTER,
            DSP_AVG_TYPE            => DSP_AVG_TYPE,
			
			ADC_DEBUG => adc_debug,
			
			-- DEBUG
			CONFIGURATION_SIZE                      => CONFIGURATION_SIZE,
            CONFIGURATION_ON                        => CONFIGURATION_ON,
            MAIN_DMA_STATE                          => MAIN_DMA_STATE,
           
            -- Config output receiver
            CH0_RECEIVER_SAMPLING_FREQUENCY         => CH0_RECEIVER_SAMPLING_FREQUENCY,
            CH0_RECEIVER_DATA_WINDOW                => CH0_RECEIVER_DATA_WINDOW,
            CH0_RECEIVER_DELAY                      => CH0_RECEIVER_DELAY,  
            CH0_RECEIVER_ANALOG_GAIN                => CH0_RECEIVER_ANALOG_GAIN,
            CH0_RECEIVER_DIGITAL_GAIN               => CH0_RECEIVER_DIGITAL_GAIN,
            CH0_RECEIVER_EXTERNAL_MULTIPLEXER_EN    => CH0_RECEIVER_EXTERNAL_MULTIPLEXER_EN,
                    
            -- Config output transmitter
            CH0_TRANSMITTER_VOLTAGE                 => CH0_TRANSMITTER_VOLTAGE,
            CH0_TRANSMITTER_BURST_FREQUENCY         => CH0_TRANSMITTER_BURST_FREQUENCY, 
            CH0_TRANSMITTER_N_CYCLES                => CH0_TRANSMITTER_N_CYCLES, 
            CH0_TRANSMITTER_DELAY                   => CH0_TRANSMITTER_DELAY, 
            CH0_TRANSMITTER_DIRECTIONAL_PHASING     => CH0_TRANSMITTER_DIRECTIONAL_PHASING, 
                
            -- Config output magnet
            CH0_MAGNET_MODE                         => CH0_MAGNET_MODE, 
            CH0_MAGNET_PULSE_WIDTH                  => CH0_MAGNET_PULSE_WIDTH,
            CH0_MAGNET_INITIAL_DELAY                => CH0_MAGNET_INITIAL_DELAY,
                
            -- Config analog filters
            CH0_DSP_ANALOG_FILTER                   => CH0_DSP_ANALOG_FILTER,    
            
            -- Debug
            CH0_CHANNEL_TRIGGER_TYPE                => CH0_CHANNEL_TRIGGER_TYPE,  
            CH0_STATE_A                             => CH0_STATE_A,  
            CH0_STATE_B                             => CH0_STATE_B,  
            CH0_STATE_C                             => CH0_STATE_C,  
                                  
            -- Config Parameters
            CONFIGURATION_HEADER_SIZE       => CONFIGURATION_HEADER_SIZE,
            CONFIGURATION_CHANNELS          => CONFIGURATION_CHANNELS,
            CONFIGURATION_CHANNEL0_OFFSET   => CONFIGURATION_CHANNEL0_OFFSET,
            CONFIGURATION_ENCODER_OFFSET    => CONFIGURATION_ENCODER_OFFSET,
            CONFIGURATION_DIGITALIO_OFFSET  => CONFIGURATION_DIGITALIO_OFFSET,
            CONFIGURATION_CHANNEL0_SIZE     => CONFIGURATION_CHANNEL0_SIZE,
            CONFIGURATION_TRIGGER_TYPE      => CONFIGURATION_TRIGGER_TYPE,
            CH0_SIGNAL_INPUT_AXIS_TREADY    => CH0_SIGNAL_INPUT_AXIS_TREADY,
            CONFIGURATION_CHANNEL0_ENABLED  => CONFIGURATION_CHANNEL0_ENABLED,
            CONFIGURATION_CHANNEL0_ON       => CONFIGURATION_CHANNEL0_ON,
            
            CH0_DSP_COINC_LEVEL => CH0_DSP_COINC_LEVEL,
            CH0_DSP_AVG_LEVEL   => CH0_DSP_AVG_LEVEL,
            
            ADC_WRITE_COUNTER => ADC_WRITE_COUNTER,  
            ADC_READ_COUNTER  => ADC_READ_COUNTER,
            
            ADC_CONFIG_EX  => ADC_CONFIG_EX,
            ADC_CONFIG_EX_VALID  => ADC_CONFIG_EX_VALID,
            ADC_CONFIG_LOADED  => ADC_CONFIG_LOADED,
            ADC_CONFIG_CURRENT  => ADC_CONFIG_CURRENT,
            ADC_CONFIG_REQUEST  => ADC_CONFIG_REQUEST,
            ADC_CONFIG_REQUEST_ACK  => ADC_CONFIG_REQUEST_ACK,

            s_axi_aclk    	=> aclk,
            s_axi_aresetn   => aresetn,
            s_axi_awaddr    => M00_AXI_ps_awaddr(C_S00_AXI_ADDR_WIDTH-1 downto 0),
            s_axi_awprot    => M00_AXI_ps_awprot,
            s_axi_awvalid   => M00_AXI_ps_awvalid(0),
            s_axi_awready   => M00_AXI_ps_awready(0),
            s_axi_wdata    	=> M00_AXI_ps_wdata,
            s_axi_wstrb    	=> M00_AXI_ps_wstrb,
            s_axi_wvalid    => M00_AXI_ps_wvalid(0),
            s_axi_wready    => M00_AXI_ps_wready(0),
            s_axi_bresp    	=> M00_AXI_ps_bresp,
            s_axi_bvalid    => M00_AXI_ps_bvalid(0),
            s_axi_bready    => M00_AXI_ps_bready(0),
            s_axi_araddr    => M00_AXI_ps_araddr(C_S00_AXI_ADDR_WIDTH-1 downto 0),
            s_axi_arprot    => M00_AXI_ps_arprot,
            s_axi_arvalid   => M00_AXI_ps_arvalid(0),
            s_axi_arready   => M00_AXI_ps_arready(0),
            s_axi_rdata    	=> M00_AXI_ps_rdata,
            s_axi_rresp    	=> M00_AXI_ps_rresp,
            s_axi_rvalid    => M00_AXI_ps_rvalid(0),
            s_axi_rready    => M00_AXI_ps_rready(0)
		);
				
	------------------  MAIN  ------------------
	--------------------------------------------
	    		
	main_inst: main
		generic map(
			-- General
			N_CHANNELS		=> N_CHANNELS,
			REGISTER_W64    => REGISTER_W64,
			REGISTER_W48    => REGISTER_W48,
			REGISTER_W32    => REGISTER_W32,
			REGISTER_W21    => REGISTER_W21,
			REGISTER_W16    => REGISTER_W16,
			REGISTER_W14    => REGISTER_W14,
			REGISTER_W8     => REGISTER_W8,
			REGISTER_W6     => REGISTER_W6,
			REGISTER_W4     => REGISTER_W4,
			REGISTER_W2     => REGISTER_W2,
			
			-- Scan mode
            SCAN_MODE_NORMAL  => SCAN_MODE_NORMAL,
            SCAN_MODE_MRUT    => SCAN_MODE_MRUT,
            
			-- Trigger type
            TRIGGER_TYPE_NORMAL     => TRIGGER_TYPE_NORMAL,
            TRIGGER_TYPE_SW         => TRIGGER_TYPE_SW,
            TRIGGER_TYPE_ENC        => TRIGGER_TYPE_ENC,
            TRIGGER_TYPE_ROBOT      => TRIGGER_TYPE_ROBOT,
            TRIGGER_TYPE_ROBOT_ENC  => TRIGGER_TYPE_ROBOT_ENC,
 
			-- external trigger
            DISPOSITION_PASS  => DISPOSITION_PASS,
            DISPOSITION_FAIL  => DISPOSITION_FAIL,
            DISPOSITION_NODET => DISPOSITION_NODET,

			-- Coinc
			TRIGGER_WAIT      => TRIGGER_WAIT,
			MIN_COINC_LEVEL   => MIN_COINC_LEVEL,

			-- Avg
			MIN_AVG_LEVEL     => MIN_AVG_LEVEL,
            AVG_TYPE_NORMAL   => AVG_TYPE_NORMAL,
            AVG_TYPE_PIPELINE => AVG_TYPE_PIPELINE,

			-- Ascan header size
			ASCAN_HEADER_SIZE => ASCAN_HEADER_SIZE,

			-- FIR CORR
			N_TAPS              => N_TAPS,
			N_SETS_TAP          => N_SETS_TAP,
			OVERSAMPLING        => OVERSAMPLING,
			WAIT_CYCLES         => WAIT_CYCLES
		)
		port map(
			-- sync
			aclk                    => aclk,
			aresetn                 => aresetn_sync, 
			-- DEBUG
			CONFIGURATION_SIZE      => CONFIGURATION_SIZE,
            CONFIGURATION_ON        => CONFIGURATION_ON,
            MAIN_DMA_STATE          => MAIN_DMA_STATE,
                        
            CONFIGURATION_HEADER_SIZE       => CONFIGURATION_HEADER_SIZE,
            CONFIGURATION_CHANNELS          => CONFIGURATION_CHANNELS,
            CONFIGURATION_CHANNEL0_OFFSET   => CONFIGURATION_CHANNEL0_OFFSET,
            CONFIGURATION_ENCODER_OFFSET    => CONFIGURATION_ENCODER_OFFSET,
            CONFIGURATION_DIGITALIO_OFFSET  => CONFIGURATION_DIGITALIO_OFFSET,
            CONFIGURATION_CHANNEL0_SIZE     => CONFIGURATION_CHANNEL0_SIZE,
            CONFIGURATION_TRIGGER_TYPE      => CONFIGURATION_TRIGGER_TYPE,
            CONFIGURATION_CHANNEL0_ENABLED  => CONFIGURATION_CHANNEL0_ENABLED,
            CONFIGURATION_CHANNEL0_ON       => CONFIGURATION_CHANNEL0_ON,
                        
			-- CONTROL REGISTERS
			DMA_UPLOAD_CONTROL      => DMA_UPLOAD_CONTROL,   
			DMA_DOWNLOAD_CONTROL    => DMA_DOWNLOAD_CONTROL,
			START_INSPECTION_CONTROL=> START_INSPECTION_CONTROL,
			SCAN_MODE_CONTROL       => SCAN_MODE_CONTROL, 
			CSCAN_MODE_CONTROL      => CSCAN_MODE_CONTROL,  
			TRIGGER_CONTROL         => TRIGGER_CONTROL,
			RESET_CONTROL           => RESET_CONTROL,
			ENABLE_CONTROL          => ENABLE_CONTROL,
			CHANNEL_CONTROL         => CHANNEL_CONTROL,
			-- STATUS REGISTERS
			DMA_UPLOAD_STATUS       => DMA_UPLOAD_STATUS,  
			DMA_DOWNLOAD_STATUS     => DMA_DOWNLOAD_STATUS, 
			START_INSPECTION_STATUS => START_INSPECTION_STATUS,
			SCAN_MODE_STATUS        => SCAN_MODE_STATUS,
			CSCAN_MODE_STATUS       => CSCAN_MODE_STATUS,  
			TRIGGER_STATUS          => TRIGGER_STATUS,
			RESET_STATUS            => RESET_STATUS,
			ENABLE_STATUS           => ENABLE_STATUS,
			CHANNEL_STATUS          => CHANNEL_STATUS,

            ENCODER1_DIR            => ENCODER1_DIR,
            ENCODER1_COUNT          => ENCODER1_COUNT,
			
            ENCODER2_DIR            => ENCODER2_DIR,
            ENCODER2_COUNT          => ENCODER2_COUNT,

			IO_INPUTS   			=> io_inputs_i,
			IO_OUTPUTS  			=> io_outputs_i,
			
			DSP_AVG_LEVEL   		=> DSP_AVG_LEVEL,
            DSP_AVG_COUNTER         => DSP_AVG_COUNTER,
			DSP_AVG_TYPE   			=> DSP_AVG_TYPE,		
			
			-------- CH0 --------
			
			CH0_CHANNEL_TRIGGER_TYPE => CH0_CHANNEL_TRIGGER_TYPE,
            CH0_STATE_A => CH0_STATE_A,
            CH0_STATE_B => CH0_STATE_B,
            CH0_STATE_C => CH0_STATE_C,

			-- Config output receiver
			CH0_RECEIVER_SAMPLING_FREQUENCY         => CH0_RECEIVER_SAMPLING_FREQUENCY, 
			CH0_RECEIVER_DATA_WINDOW                => CH0_RECEIVER_DATA_WINDOW, 
			CH0_RECEIVER_DELAY                      => CH0_RECEIVER_DELAY, 
            CH0_RECEIVER_ANALOG_GAIN                => CH0_RECEIVER_ANALOG_GAIN,
            CH0_RECEIVER_DIGITAL_GAIN               => CH0_RECEIVER_DIGITAL_GAIN,
			CH0_RECEIVER_EXTERNAL_MULTIPLEXER_EN    => CH0_RECEIVER_EXTERNAL_MULTIPLEXER_EN,

			-- Config output transmitter
			CH0_TRANSMITTER_VOLTAGE                 => CH0_TRANSMITTER_VOLTAGE,
			CH0_TRANSMITTER_BURST_FREQUENCY         => CH0_TRANSMITTER_BURST_FREQUENCY, 
			CH0_TRANSMITTER_N_CYCLES                => CH0_TRANSMITTER_N_CYCLES,  
			CH0_TRANSMITTER_N_BURST                 => CH0_TRANSMITTER_N_BURST,
			CH0_TRANSMITTER_DELAY                   => CH0_TRANSMITTER_DELAY,
			CH0_TRANSMITTER_DELTA_DELAY             => CH0_TRANSMITTER_DELTA_DELAY,
			CH0_TRANSMITTER_DELTA_ADD               => CH0_TRANSMITTER_DELTA_ADD,
			CH0_TRANSMITTER_DIRECTIONAL_PHASING     => CH0_TRANSMITTER_DIRECTIONAL_PHASING, 

			-- Config output magnet
			CH0_MAGNET_MODE                         => CH0_MAGNET_MODE,
			CH0_MAGNET_PULSE_WIDTH                  => CH0_MAGNET_PULSE_WIDTH, 
			CH0_MAGNET_INITIAL_DELAY                => CH0_MAGNET_INITIAL_DELAY,
			CH0_MAGNET_RAMP_UP_VOLTAGE              => CH0_MAGNET_RAMP_UP_VOLTAGE, 
            CH0_MAGNET_VOLTAGE                      => CH0_MAGNET_VOLTAGE,
            
			-- Config analog filters
			CH0_DSP_ANALOG_FILTER  					=> CH0_DSP_ANALOG_FILTER,
			CH0_DSP_COINC_LEVEL                     => CH0_DSP_COINC_LEVEL,
			CH0_DSP_AVG_LEVEL                       => CH0_DSP_AVG_LEVEL,
			
			CH0_DSP_BURST_LEVEL                     => CH0_DSP_BURST_LEVEL,
			CH0_DSP_BURST_COUNTER                   => CH0_DSP_BURST_COUNTER,   
			
			-- Config DAC curves
			CH0_CONFIG_DAC_AXIS_TREADY    => CH0_CONFIG_DAC_AXIS_TREADY,
			CH0_CONFIG_DAC_AXIS_TDATA	  => CH0_CONFIG_DAC_AXIS_TDATA,
			CH0_CONFIG_DAC_AXIS_TLAST	  => CH0_CONFIG_DAC_AXIS_TLAST,
			CH0_CONFIG_DAC_AXIS_TVALID    => CH0_CONFIG_DAC_AXIS_TVALID,

			-- Signal input
			CH0_SIGNAL_COINC_WINDOW  	  => CH0_SIGNAL_COINC_WINDOW,
			CH0_SIGNAL_AVG_WINDOW  		  => CH0_SIGNAL_AVG_WINDOW,

			CH0_SIGNAL_INPUT_AXIS_TREADY  => CH0_SIGNAL_INPUT_AXIS_TREADY,
			CH0_SIGNAL_INPUT_AXIS_TDATA	  => CH0_SIGNAL_INPUT_AXIS_TDATA,
			CH0_SIGNAL_INPUT_AXIS_TLAST	  => CH0_SIGNAL_INPUT_AXIS_TLAST,
			CH0_SIGNAL_INPUT_AXIS_TVALID  => CH0_SIGNAL_INPUT_AXIS_TVALID,
					
			-- SYSTEM SIGNALS
			SW_RESET                => SW_RESET,
			
			-- DMA INPUT
			DMA_S_axis_tready       => S_AXIS_dma_0_tready,
			DMA_S_axis_tkeep        => S_AXIS_dma_0_tkeep,
			DMA_S_axis_tdata        => S_AXIS_dma_0_tdata,
			DMA_S_axis_tlast        => S_AXIS_dma_0_tlast,
			DMA_S_axis_tvalid       => S_AXIS_dma_0_tvalid,
			-- DMA OUTPUT
			DMA_M_axis_tvalid       => M_AXIS_MM2S_dma_0_tvalid,
			DMA_M_axis_tdata        => M_AXIS_MM2S_dma_0_tdata,
			DMA_M_axis_tlast        => M_AXIS_MM2S_dma_0_tlast,
			DMA_M_axis_tkeep        => open,
			DMA_M_axis_tready       => M_AXIS_MM2S_dma_0_tready
		);
		
		M_AXIS_MM2S_dma_0_tkeep <= (others => '1');
		
end Behavioral;
