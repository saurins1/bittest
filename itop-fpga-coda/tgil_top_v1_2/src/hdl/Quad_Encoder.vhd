library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Quad_Encoder is
    Port (  clock           : in STD_LOGIC; 
            reset           : in STD_LOGIC; 
--			Enc_Set			: in STD_LOGIC; 
            Enc_CHA         : in STD_LOGIC; 
            Enc_CHB        	: in STD_LOGIC; 
--			Enc_Count_Set	: in STD_LOGIC_VECTOR (31 downto 0);
            Enc_Dir        	: out STD_LOGIC_VECTOR (1 downto 0);  -- Direction 00 unknown 01 positive 10 negative 11 error
            Enc_Raw_Count 	: out STD_LOGIC_VECTOR (31 downto 0)  -- Distance in pulses (ticks)
    );
end Quad_Encoder;

architecture arch_imp of Quad_Encoder is
   
    signal Enc_CHA_Filt     : std_logic := '0';
    signal Enc_CHB_Filt     : std_logic := '0';

    signal Enc_CHA_d1     : std_logic := '0';
    signal Enc_CHB_d1     : std_logic := '0';
    signal Enc_CHA_d2     : std_logic := '0';
    signal Enc_CHB_d2     : std_logic := '0';
    signal Enc_CHA_d3     : std_logic := '0';
    signal Enc_CHB_d3     : std_logic := '0';
    signal Enc_CHA_d4     : std_logic := '0';
    signal Enc_CHB_d4     : std_logic := '0';
    signal Enc_CHA_d5     : std_logic := '0';
    signal Enc_CHB_d5     : std_logic := '0';
    signal Enc_CHA_d6     : std_logic := '0';
    signal Enc_CHB_d6     : std_logic := '0';
    
    signal Enc_Pos_i        : signed(31 downto 0) := to_signed(0, 32);
    signal Enc_Dir_i        : std_logic_vector(1 downto 0) := "00";
	signal Enc_Dir_i_d1     : std_logic_vector(1 downto 0) := "00";
	signal Enc_Dir_Filt     : std_logic_vector(1 downto 0) := "00";
    
    type states_enc is (s0, s00, s01, s10, s11); 
    signal state_enc : states_enc; 

begin
	
	Enc_Dir_Filt <= "00" when ( Enc_Dir_i = "00" and Enc_Dir_i_d1 = "00" ) else
					"01" when ( Enc_Dir_i = "01" and Enc_Dir_i_d1 = "01" ) else
					"10" when ( Enc_Dir_i = "10" and Enc_Dir_i_d1 = "10" ) else
					"11" when ( Enc_Dir_i = "11" and Enc_Dir_i_d1 = "11" ) else
					Enc_Dir_Filt;
    
	Enc_Raw_Count  <= std_logic_vector(Enc_Pos_i); 
	Enc_Dir <= Enc_Dir_Filt;
	
	Enc_CHA_Filt <= '0' when ( Enc_CHA_d3 = '0' and Enc_CHA_d4 = '0' and Enc_CHA_d5 = '0' and Enc_CHA_d6 = '0' ) else
					'1' when ( Enc_CHA_d3 = '1' and Enc_CHA_d4 = '1' and Enc_CHA_d5 = '1' and Enc_CHA_d6 = '1' ) else
					Enc_CHA_Filt;
	
	Enc_CHB_Filt <= '0' when ( Enc_CHB_d3 = '0' and Enc_CHB_d4 = '0' and Enc_CHB_d5 = '0' and Enc_CHB_d6 = '0' ) else
					'1' when ( Enc_CHB_d3 = '1' and Enc_CHB_d4 = '1' and Enc_CHB_d5 = '1' and Enc_CHB_d6 = '1' ) else
					Enc_CHB_Filt;

    input_filter: process(clock)
    begin 
        if (clock'event and clock = '1') then
			if (reset = '1') then
				Enc_CHA_d1	<= '0';
				Enc_CHA_d2	<= '0';
				Enc_CHA_d3	<= '0';
				Enc_CHA_d4	<= '0';
				Enc_CHA_d5	<= '0';
				Enc_CHA_d6	<= '0';
				Enc_CHB_d1	<= '0';
				Enc_CHB_d2	<= '0';
				Enc_CHB_d3	<= '0';
				Enc_CHB_d4	<= '0';
				Enc_CHB_d5	<= '0';
				Enc_CHB_d6	<= '0';
			else
				Enc_CHA_d1	<= Enc_CHA;
				Enc_CHA_d2	<= Enc_CHA_d1;
				Enc_CHA_d3	<= Enc_CHA_d2;
				Enc_CHA_d4	<= Enc_CHA_d3;
				Enc_CHA_d5	<= Enc_CHA_d4;
				Enc_CHA_d6	<= Enc_CHA_d5;
				Enc_CHB_d1	<= Enc_CHB;
				Enc_CHB_d2	<= Enc_CHB_d1;
				Enc_CHB_d3	<= Enc_CHB_d2;
				Enc_CHB_d4	<= Enc_CHB_d3;
				Enc_CHB_d5	<= Enc_CHB_d4;
				Enc_CHB_d6	<= Enc_CHB_d5;
			end if;
		end if;
    end process;	
	
    -- Position/Direction calculation SM
    encoder_SM: process(clock)
    begin 
        if (clock'event and clock = '1') then
			if (reset = '1') then
				state_enc <= s0;                    
				Enc_Dir_i <= "00";
				Enc_Dir_i_d1 <= "00";
				Enc_Pos_i <= to_signed(0, 32);
			else
				Enc_Dir_i_d1 <= Enc_Dir_i;
				case state_enc is	
					when s0 =>
						if(Enc_CHA_Filt = '0' and Enc_CHB_Filt = '0') then
							state_enc <= s00;
						elsif(Enc_CHA_Filt = '0' and Enc_CHB_Filt = '1') then  
							state_enc <= s01;
						elsif(Enc_CHA_Filt = '1' and Enc_CHB_Filt = '0') then       
							state_enc <= s10;
						elsif(Enc_CHA_Filt = '1' and Enc_CHB_Filt = '1') then
							state_enc <= s11;
						end if;                	
					when s00 =>
						if(Enc_CHA_Filt = '1' and Enc_CHB_Filt = '0') then
							state_enc <= s10;
							if ( Enc_Dir_i_d1 = "01" ) then
								Enc_Pos_i <= Enc_Pos_i + 1;
							end if;
							ENC_DIR_i <= "01";
						elsif(Enc_CHA_Filt = '0' and Enc_CHB_Filt = '1') then  
							state_enc <= s01;
							if ( Enc_Dir_i_d1 = "10" ) then
								Enc_Pos_i <= Enc_Pos_i - 1;
							end if;
							ENC_DIR_i <= "10";
						elsif(Enc_CHA_Filt = '1' and Enc_CHB_Filt = '1') then
							state_enc <= s11;
							ENC_DIR_i <= "11";
						end if;
					when s01 =>
						if(Enc_CHA_Filt = '0' and Enc_CHB_Filt = '0') then
							state_enc <= s00;
							if ( Enc_Dir_i_d1 = "01" ) then
								Enc_Pos_i <= Enc_Pos_i + 1;
							end if;
							ENC_DIR_i <= "01";
						elsif(Enc_CHA_Filt = '1' and Enc_CHB_Filt = '1') then  
							state_enc <= s11;
							if ( Enc_Dir_i_d1 = "10" ) then
								Enc_Pos_i <= Enc_Pos_i - 1;
							end if;
							ENC_DIR_i <= "10";
						elsif(Enc_CHA_Filt = '1' and Enc_CHB_Filt = '0') then
							state_enc <= s10;
							ENC_DIR_i <= "11";
						end if;
					when s10 =>
						if(Enc_CHA_Filt = '1' and Enc_CHB_Filt = '1') then
							state_enc <= s11;
							if ( Enc_Dir_i_d1 = "01" ) then
								Enc_Pos_i <= Enc_Pos_i + 1;
							end if;
							ENC_DIR_i <= "01";
						elsif(Enc_CHA_Filt = '0' and Enc_CHB_Filt = '0') then  
							state_enc <= s00;
							if ( Enc_Dir_i_d1 = "10" ) then
								Enc_Pos_i <= Enc_Pos_i - 1;
							end if;
							ENC_DIR_i <= "10";
						elsif(Enc_CHA_Filt = '0' and Enc_CHB_Filt = '1') then
							state_enc <= s01;
							ENC_DIR_i <= "11";
						end if;           
					when s11 =>
						if(Enc_CHA_Filt = '1' and Enc_CHB_Filt = '0') then
							state_enc <= s10;
							Enc_Pos_i <= Enc_Pos_i + 1;                   
							ENC_DIR_i <= "01";
						elsif(Enc_CHA_Filt = '0' and Enc_CHB_Filt = '1') then  
							state_enc <= s01;
							Enc_Pos_i <= Enc_Pos_i - 1;                   
							ENC_DIR_i <= "10";
						elsif(Enc_CHA_Filt = '0' and Enc_CHB_Filt = '0') then
							state_enc <= s00;
							ENC_DIR_i <= "11";
						end if;            
				end case;
			end if;
		end if;
    end process;
    
end arch_imp;
