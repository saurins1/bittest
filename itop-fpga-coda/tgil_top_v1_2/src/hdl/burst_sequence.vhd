----------------------------------------------------------------------------------
-- Company: Innerspec Technologies
-- Engineers: Wei Jiang
-- 
-- Design Name: 
-- Module Name: BURST_SEQUENCE - BURST_SEQUENCE_arch 
-- Project Name: ITOP Rollamte
-- Target Devices: 
-- Tool versions: 
-- Description: Generate Data_window for RX
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Revision 0.02 - DAC Delay add, need pass parameters
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity BURST_SEQUENCE is
	port (	CLOCK					: in std_logic;
			RESET					: in std_logic;
			CH_TRIG					: in std_logic;
			TX_DATA_DELAY			: in std_logic_vector(31 downto 0);		--delays in 100MHz samples
			RX_SAMPLING_FREQUENCY  	: in STD_LOGIC_VECTOR(31 downto 0);  	-- 100 MHz divider for sample freq
			RX_DATA_DELAY			: in std_logic_vector(31 downto 0);		--delays in 100MHz samples
			RX_DATA_WINDOW			: in std_logic_vector(31 downto 0);		--data window in sample freq samples
			-- Outputs
			CH_ACTIVE				: out std_logic;
			TX_TRIGGER				: out std_logic;
			RX_DATA_VALID			: out std_logic;
			RX_DATA_WINDOW_ON		: out std_logic
			);
end BURST_SEQUENCE;

architecture BURST_SEQUENCE_arch of BURST_SEQUENCE is

type BURST_SEQ_STATES is (RST_STATE, IDLE_STATE,
		INIT_DATA_DELAY_STATE, DATA_WINDOW_STATE, DATA_WINDOW_DCM_STATE);
signal BURST_SM_STATE : BURST_SEQ_STATES := RST_STATE;

signal RX_FREQi : std_logic_vector(7 downto 0) := "00000001";
signal Count_FREQ : std_logic_vector(7 downto 0);
signal COUNT_DATA_WINDOW : std_logic_vector(31 downto 0) := (others => '0');
signal COUNT_DATA_DELAY : std_logic_vector(31 downto 0) := (others => '0');
--signal DAC_Gain_Delay		: std_logic_vector(31 downto 0) := std_logic_vector(to_unsigned(50,32));

signal CH_TRIG_d1 : std_logic := '0';

begin

	RX_FREQi <= RX_SAMPLING_FREQUENCY(7 downto 0);
	
	process (RESET, CLOCK)
	begin
		if (rising_edge(CLOCK)) then
			if (RESET = '1' ) then
				Count_FREQ <= "00000001";
			else
				if ( Count_Freq < RX_FREQi ) then
					Count_Freq <= Count_Freq + 1;
				else
					Count_FREQ <= "00000001";
				end if;
			end if;	
		end if;
	end process;	
	
	process (RESET, CLOCK)
	begin
		if (rising_edge(CLOCK)) then
			CH_TRIG_d1 <= CH_TRIG;
			
			case BURST_SM_STATE is
				when RST_STATE =>
					if (RESET = '1') then
						BURST_SM_STATE <= RST_STATE;
					else
						BURST_SM_STATE <= IDLE_STATE;
					end if;

					COUNT_DATA_WINDOW <= (others => '0');
					COUNT_DATA_DELAY <= (others => '0');
					CH_ACTIVE <= '0';
					RX_DATA_VALID <= '0';
					RX_DATA_WINDOW_ON <= '0';
					TX_TRIGGER <= '0';
					
				when IDLE_STATE =>
					if (CH_TRIG = '1' and CH_TRIG_d1 = '0') then
						BURST_SM_STATE <= INIT_DATA_DELAY_STATE;
					else
						BURST_SM_STATE <= IDLE_STATE;
					end if;

					COUNT_DATA_WINDOW <= (others => '0');
					COUNT_DATA_DELAY <= (others => '0');
					CH_ACTIVE <= '0';
					RX_DATA_VALID <= '0';
					RX_DATA_WINDOW_ON <= '0';
					TX_TRIGGER <= '0';
										
				when INIT_DATA_DELAY_STATE =>
					COUNT_DATA_DELAY <= COUNT_DATA_DELAY + 1;
					CH_ACTIVE <= '1';
					RX_DATA_VALID <= '0';
					RX_DATA_WINDOW_ON <= '0';
					if ( COUNT_DATA_DELAY < RX_DATA_DELAY ) then
						BURST_SM_STATE <= INIT_DATA_DELAY_STATE;
					else
						COUNT_DATA_WINDOW <= (others => '0');
						if ( RX_FREQi = 0 or RX_FREQi = 1 ) then
							BURST_SM_STATE <= DATA_WINDOW_STATE;
						else
							BURST_SM_STATE <= DATA_WINDOW_DCM_STATE;
						end if;
					end if;
					
					if (COUNT_DATA_DELAY < TX_DATA_DELAY) then
					   TX_TRIGGER <= '0';
					else
					   TX_TRIGGER <=  '1';
					end if;
								
				when DATA_WINDOW_STATE =>
					COUNT_DATA_DELAY <= COUNT_DATA_DELAY + 1;
					COUNT_DATA_WINDOW <= COUNT_DATA_WINDOW + 1;
					if (COUNT_DATA_WINDOW < RX_DATA_WINDOW) then
						CH_ACTIVE <= '1';
						RX_DATA_VALID <= '1';
						RX_DATA_WINDOW_ON <= '1';
						BURST_SM_STATE <= DATA_WINDOW_STATE;
					else
						CH_ACTIVE <= '0';
						RX_DATA_VALID <= '0';
						RX_DATA_WINDOW_ON <= '0';
						BURST_SM_STATE <= IDLE_STATE;
					end if;

					if (COUNT_DATA_DELAY < TX_DATA_DELAY) then
					   TX_TRIGGER <= '0';
					else
					   TX_TRIGGER <=  '1';
					end if;

				when DATA_WINDOW_DCM_STATE =>
					COUNT_DATA_DELAY <= COUNT_DATA_DELAY + 1;
					if( Count_FREQ = RX_FREQi and COUNT_DATA_WINDOW < RX_DATA_WINDOW) then
						RX_DATA_VALID <= '1';
						COUNT_DATA_WINDOW <= COUNT_DATA_WINDOW + 1;
					else
						RX_DATA_VALID <= '0';
					end if;
					
					if (COUNT_DATA_WINDOW < RX_DATA_WINDOW) then
						CH_ACTIVE <= '1';
						RX_DATA_WINDOW_ON <= '1';
						BURST_SM_STATE <= DATA_WINDOW_DCM_STATE;
					else
						CH_ACTIVE <= '0';
						RX_DATA_WINDOW_ON <= '0';
						BURST_SM_STATE <= IDLE_STATE;
					end if;

					if (COUNT_DATA_DELAY < TX_DATA_DELAY) then
					   TX_TRIGGER <= '0';
					else
					   TX_TRIGGER <=  '1';
					end if;					
	
				when others =>
					BURST_SM_STATE <= IDLE_STATE;
					
			end case;
		end if;
	end process;
-----------------------------------------------------------------------------
end BURST_SEQUENCE_arch;

