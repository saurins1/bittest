#
# Vivado (TM) v2016.4 (64-bit)
#
# build.tcl: Tcl script for re-creating project 'tgil_top_v1'
#

################################################################################
# define names
################################################################################

set design_name design_top
set project_name tgil_top_v1_2

################################################################################
# define paths
################################################################################

# Set the reference directory for source file relative paths (by default the value is script directory path)
set origin_dir 		"."
set path_constrs 	$origin_dir/constrs

set path_sim 		$origin_dir/sim
set path_src 		$origin_dir/src/hdl
set path_ip 		$origin_dir/src/ip
set path_bd 		$origin_dir/src/bd

set common_repo 	"itop-fpga-common"

# Set the directory path for the original project from where this script was exported
set orig_proj_dir "[file normalize "$origin_dir/$project_name"]"

################################################################################
# setup an project
################################################################################

# Create project
create_project $project_name $origin_dir/$project_name -part xc7z020clg400-1 -force 

# Set the directory path for the new project
set proj_dir [get_property directory [current_project]]

# Set project properties
set obj [get_projects $project_name]
set_property "board_part" "em.avnet.com:microzed_7020:part0:1.1" $obj
set_property "default_lib" "xil_defaultlib" $obj
set_property "sim.ip.auto_export_scripts" "1" $obj
set_property "simulator_language" "Mixed" $obj
set_property "target_language" "VHDL" $obj
set_property "xpm_libraries" "XPM_CDC XPM_MEMORY" $obj

################################################################################
# setup constraints
################################################################################

# Create 'constrs_1' fileset (if not found)
if {[string equal [get_filesets -quiet constrs_1] ""]} {
  create_fileset -constrset constrs_1
}

add_files -fileset constrs_1      $path_constrs/tgil_top_constraints.xdc

################################################################################
# setup simulation
################################################################################

# Create 'sim_1' fileset (if not found)
if {[string equal [get_filesets -quiet sim_1] ""]} {
  create_fileset -simset sim_1
}

# Set 'sim_1' fileset object
set obj [get_filesets sim_1]
set files [list \
 "[file normalize "$path_sim/src/clk_wiz_0_tb.vhd"]"\
]
add_files -norecurse -fileset $obj $files

# Set 'sim_1' fileset file properties for remote files
set file "$path_sim/src/clk_wiz_0_tb.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sim_1] [list "*$file"]]
set_property "file_type" "VHDL" $file_obj

# Set 'sim_1' fileset file properties for local files
# None

# Set 'sim_1' fileset properties
set obj [get_filesets sim_1]
set_property "top" "clk_wiz_0_tb" $obj
set_property "transport_int_delay" "0" $obj
set_property "transport_path_delay" "0" $obj
set_property "xelab.nosort" "1" $obj
set_property "xelab.unifast" "" $obj
set_property "xsim.view" "$path_sim/simulation_files/clk_wiz_0_tb_behav.wcfg \
$path_sim/simulation_files/clk_wiz_0_tb_behav.wcfg" $obj

################################################################################
# setup sources
################################################################################

# Create 'sources_1' fileset (if not found)
if {[string equal [get_filesets -quiet sources_1] ""]} {
  create_fileset -srcset sources_1
}

# Create block design
source $path_bd/design_top.tcl

# Generate the wrapper
make_wrapper -files [get_files *${design_name}.bd] -top

# Add wrapper
add_files -norecurse $origin_dir/${project_name}/${project_name}.srcs/sources_1/bd/${design_name}/hdl/${design_name}_wrapper.vhd

# Digital amplifier
read_ip		$origin_dir/../digital_amplifier/src/ip/digital_amplifier_multiplier_16_32/digital_amplifier_multiplier_16_32.xci
add_files	$origin_dir/../../$common_repo/digital_amplifier/src/hdl/digital_amplifier.vhd

# Coincidence
read_ip		$origin_dir/../coincidence_filter_v1_1/src/ip/coincidence_filter_BRAM/coincidence_filter_BRAM.xci
add_files	$origin_dir/../../$common_repo/coincidence_filter_v1_1/src/hdl/coincidence_filter.vhd

# Averaging
read_ip		$origin_dir/../average_filter_v1_1/src/ip/average_filter_BRAM/average_filter_BRAM.xci
read_ip		$origin_dir/../average_filter_v1_1/src/ip/average_filter_divider_24_8/average_filter_divider_24_8.xci
read_ip		$origin_dir/../average_filter_v1_1/src/ip/average_filter_multiplier_16_8/average_filter_multiplier_16_8.xci
add_files	$origin_dir/../../$common_repo/average_filter_v1_1/src/hdl/average_filter.vhd

# Fir corr
read_ip		$origin_dir/../fir_correlation_light/src/ip/fir_correlation_DSP/fir_correlation_DSP.xci
read_ip		$origin_dir/../fir_correlation_light/src/ip/fir_correlation_taps_BRAM/fir_correlation_taps_BRAM.xci
read_ip		$origin_dir/../fir_correlation_light/src/ip/fir_correlation_signal_BRAM/fir_correlation_signal_BRAM.xci
add_files	$origin_dir/../../$common_repo/fir_correlation_light/src/hdl/fir_correlation.vhd

# Gates
add_files	$origin_dir/../../$common_repo/gates_processor/src/hdl/divisor_complement_32_16.vhd
add_files	$origin_dir/../../$common_repo/gates_processor/src/hdl/divisor_complement_64_64.vhd
add_files	$origin_dir/../../$common_repo/gates_processor/src/hdl/gates_processor.vhd
add_files	$origin_dir/../../$common_repo/gates_processor/src/hdl/gate2_compute.vhd
add_files	$origin_dir/../../$common_repo/gates_processor/src/hdl/gatex_compute.vhd

# Configuration
read_ip		$origin_dir/../channel_configuration/src/ip/channel_configuration_BRAM/channel_configuration_BRAM.xci
add_files	$origin_dir/../../$common_repo/channel_configuration/src/hdl/channel_configuration.vhd

#clock for Digitizer Unit
read_ip		$path_ip/clk_wiz_0/clk_wiz_0.xci

#debug vio and ila unit
read_ip		$path_ip/vio_0/vio_0.xci
read_ip		$path_ip/ila_100/ila_100.xci

# Channel 
add_files	$origin_dir/../../$common_repo/channel_v1_2/src/hdl/channel.vhd

# Capabilities
add_files	$origin_dir/../capabilities/src/hdl/capabilities_RAM.vhd
add_files	$origin_dir/../../$common_repo/capabilities/src/hdl/capabilities.vhd
add_files	$origin_dir/../capabilities/src/hdl/capabilities.txt

# Main configuration
read_ip		$origin_dir/../main_configuration/src/ip/main_configuration_BRAM/main_configuration_BRAM.xci
add_files	$origin_dir/../main_configuration/src/hdl/main_configuration.vhd

# Main
add_files	$origin_dir/../main_v1_1/src/hdl/main.vhd

# Channel sequencer
add_files	$origin_dir/../../$common_repo/channel_sequencer/src/hdl/channel_sequencer_1.vhd

# Pulser Receiver

#Pulser
add_files	$origin_dir/../../$common_repo/pulser/src/hdl/pulser.vhd
add_files	$origin_dir/../../$common_repo/pulser/src/hdl/tx_outputs.vhd
add_files	$origin_dir/../../$common_repo/pulser/src/hdl/burst_sequence.vhd

# receiver
add_files	$origin_dir/../receiver/src/hdl/receiver.vhd
add_files	$origin_dir/../receiver/src/hdl/adc_controller.vhd
add_files	$origin_dir/../receiver/src/hdl/channel_receiver.vhd
add_files	$origin_dir/../../$common_repo/spi/src/hdl/spi_master_standard.vhd

add_files	$origin_dir/../pulser_receiver/src/hdl/pulser_receiver.vhd

# Dac analog amplifier
add_files	$origin_dir/../dac/src/hdl/dac.vhd

# DIO
add_files	$origin_dir/../dio/src/hdl/dio.vhd
add_files	$origin_dir/../../$common_repo/input_digital_filter/src/hdl/input_digital_filter.vhd

# Temp sensor
add_files $origin_dir/../temperature_sensor/src/hdl/temperature_sensor.vhd

# This project sources
add_files	$path_src/tgil_top.vhd
add_files	$path_src/tgil_top_S00_AXI.vhd
add_files   	$path_src/Quad_Encoder.vhd

# Update the compile order
update_compile_order -fileset sources_1
update_compile_order -fileset sim_1

# Ensure parameter propagation has been performed
close_bd_design [current_bd_design]
open_bd_design [get_files ${design_name}.bd]
validate_bd_design -force
save_bd_design

puts "INFO: Project created:${design_name}"

# Delete temp vivado files
file delete {*}[glob vivado*]

set_property STEPS.PHYS_OPT_DESIGN.IS_ENABLED true [get_runs impl_1]
set_property STEPS.POST_ROUTE_PHYS_OPT_DESIGN.IS_ENABLED true [get_runs impl_1]

