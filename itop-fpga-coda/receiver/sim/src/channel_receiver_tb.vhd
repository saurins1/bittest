library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity channel_receiver_tb is
  	generic (
  	    REGISTER_W32    : INTEGER := 32;
  	    REGISTER_W16    : INTEGER := 16  
  	);
end;

architecture bench of channel_receiver_tb is

  component channel_receiver
  	generic (
  	    REGISTER_W32    : INTEGER := 32;
  	    REGISTER_W16    : INTEGER := 16  
  	);
  	port (
  		aclk                    				: in STD_LOGIC; 
  		aresetn                 				: in STD_LOGIC; 
  		RX_DATA_VALID							: in std_logic;
  		RX_DATA_WINDOW_ON						: in std_logic;
  		RX_DATA_WINDOW							: in std_logic_vector(REGISTER_W32-1 downto 0);
  		CH_ADC_DATA_IN							: in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  		CH_SIGNAL_AXIS_TREADY  					: in STD_LOGIC;
  		CH_SIGNAL_AXIS_TDATA	  				: out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  		CH_SIGNAL_AXIS_TLAST	  				: out STD_LOGIC;
  		CH_SIGNAL_AXIS_TVALID  					: out STD_LOGIC
  	);
  end component;

  signal aclk: STD_LOGIC;
  signal aresetn: STD_LOGIC;
  signal RX_DATA_VALID: std_logic;
  signal RX_DATA_WINDOW_ON: std_logic;
  signal RX_DATA_WINDOW: std_logic_vector(REGISTER_W32-1 downto 0);
  signal CH_ADC_DATA_IN: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  signal CH_SIGNAL_AXIS_TREADY: STD_LOGIC;
  signal CH_SIGNAL_AXIS_TDATA: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  signal CH_SIGNAL_AXIS_TLAST: STD_LOGIC;
  signal CH_SIGNAL_AXIS_TVALID: STD_LOGIC ;

  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;

begin

  -- Insert values for generic parameters !!
  uut: channel_receiver generic map ( REGISTER_W32          => REGISTER_W32,
                                      REGISTER_W16          => REGISTER_W16 )
                           port map ( aclk                  => aclk,
                                      aresetn               => aresetn,
                                      RX_DATA_VALID         => RX_DATA_VALID,
                                      RX_DATA_WINDOW_ON     => RX_DATA_WINDOW_ON,
                                      RX_DATA_WINDOW        => RX_DATA_WINDOW,
                                      CH_ADC_DATA_IN        => CH_ADC_DATA_IN,
                                      CH_SIGNAL_AXIS_TREADY => CH_SIGNAL_AXIS_TREADY,
                                      CH_SIGNAL_AXIS_TDATA  => CH_SIGNAL_AXIS_TDATA,
                                      CH_SIGNAL_AXIS_TLAST  => CH_SIGNAL_AXIS_TLAST,
                                      CH_SIGNAL_AXIS_TVALID => CH_SIGNAL_AXIS_TVALID );

  stimulus: process
  begin
  
    -- Put initialisation code here


    -- Put test bench stimulus code here

    stop_the_clock <= true;
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      aclk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;