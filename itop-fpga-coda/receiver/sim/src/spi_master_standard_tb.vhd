library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity spi_master_tb is      
    Generic (   
        N : positive := 16;
        CPOL : std_logic := '0';
        CPHA : std_logic := '0';
        PREFETCH : positive := 3;
        SPI_2X_CLK_DIV : positive := 100
    );
end;

architecture bench of spi_master_tb is

  component spi_master
      Generic (   
          N : positive := 32;
          CPOL : std_logic := '0';
          CPHA : std_logic := '0';
          PREFETCH : positive := 2;
          SPI_2X_CLK_DIV : positive := 5);
      Port (  
          sclk_i : in std_logic := 'X';
          pclk_i : in std_logic := 'X';
          rst_i : in std_logic := 'X';
          spi_ssel_o : out std_logic; -- csn
          spi_sck_o : out std_logic; -- sck
          spi_mosi_o : out std_logic; -- sdi
          spi_miso_i : in std_logic := 'X'; --sdo
          di_req_o : out std_logic;
          di_i : in  std_logic_vector (N-1 downto 0) := (others => 'X');
          wren_i : in std_logic := 'X';
          wr_ack_o : out std_logic;
          do_valid_o : out std_logic;
          do_o : out  std_logic_vector (N-1 downto 0);
          done_o : out std_logic
      );                      
  end component;

  signal sclk_i: std_logic := '0';
  signal pclk_i: std_logic := '0';
  signal rst_i: std_logic := '0';
  signal spi_ssel_o: std_logic;
  signal spi_sck_o: std_logic;
  signal spi_sck_o_d1: std_logic;
  signal spi_mosi_o: std_logic;
  signal spi_miso_i: std_logic := '0';
  signal di_req_o: std_logic;
  signal di_i: std_logic_vector (N-1 downto 0) := (others => '0');
  signal wren_i: std_logic := '0';
  signal wr_ack_o: std_logic;
  signal do_valid_o: std_logic;
  signal do_o: std_logic_vector (N-1 downto 0);
  signal done_o: std_logic ;

  constant clock_period: time := 10 ns;
   
  type states_spi_sm is (idle, write_start, write_end, write_wait); 
  signal state_spi_sm : states_spi_sm; 
  
  signal write_spi_flag: std_logic := '0';
  
  signal di_data: std_logic_vector (7 downto 0) := (others => '0');
  signal do_data: std_logic_vector (7 downto 0) := (others => '0');
  
begin

  di_i <= "0" & "0000001" & di_data;

  -- Insert values for generic parameters !!
  uut: spi_master generic map ( N              => N,
                                CPOL           => CPOL,
                                CPHA           => CPHA,
                                PREFETCH       => PREFETCH,
                                SPI_2X_CLK_DIV => SPI_2X_CLK_DIV)
                     port map ( sclk_i         => sclk_i,
                                pclk_i         => pclk_i,
                                rst_i          => rst_i,
                                spi_ssel_o     => spi_ssel_o,
                                spi_sck_o      => spi_sck_o,
                                spi_mosi_o     => spi_mosi_o,
                                spi_miso_i     => spi_miso_i,
                                di_req_o       => di_req_o,
                                di_i           => di_i,
                                wren_i         => wren_i,
                                wr_ack_o       => wr_ack_o,
                                do_valid_o     => do_valid_o,
                                do_o           => do_o,
                                done_o         => done_o );
                                
                                
	-- GENERAL SPI SM
    spi_sm_inst: process(sclk_i, rst_i)  
        variable my_counter  : integer range 0 to 16383 :=0;    
    begin 
        if (rst_i = '1') then
            wren_i <= '0';
            di_data <= (others => '0');
			-- state																		  	
            state_spi_sm <= write_wait;  
        elsif (sclk_i'event and sclk_i = '1') then
            case state_spi_sm is  
                when idle =>   
                    wren_i <= '0';   
                    if(write_spi_flag = '1') then
                        di_data <= std_logic_vector(unsigned(di_data)+1);
                        state_spi_sm <= write_start; 
                    end if;
                when write_start => 
                    wren_i <= '1';
				    state_spi_sm <= write_end; 		  					 
                when write_end =>
				    if(wr_ack_o = '1') then
				        wren_i <= '0';
				        my_counter := 0;
				        state_spi_sm <= write_wait;
				    end if;							 
				when write_wait =>
				    if(done_o = '1') then							  
                        if(	my_counter >= 128) then
                            state_spi_sm <= idle; 
                        else
                            my_counter := my_counter + 1;
                        end if;	
                    end if;				 
                when others =>
                    null;                   
            end case;                                                                                            
        end if;
    end process; 
    
    do_data <= do_o(7 downto 0);
    				
	-- SDO SPI SM (read the SDO line after write addr)
    spi_sdo_sm_inst: process(sclk_i, rst_i)  
        variable my_counter  : integer range 0 to 16383 :=0;    
    begin 
        if (rst_i = '1') then
            spi_miso_i <= '0';
			spi_sck_o_d1 <= '0';
        elsif (sclk_i'event and sclk_i = '1') then
            spi_miso_i <= spi_mosi_o;
			spi_sck_o_d1 <= spi_sck_o;                                                                                           
        end if;
    end process; 

    stimulus: process
    begin
        rst_i <= '1';
        wait for 100 ns;
        rst_i <= '0';
        write_spi_flag <= '1';
        wait;
    end process;

	-- sclk process 
    sclk_process :process
    begin
        sclk_i <= not sclk_i;
        wait for clock_period/2;
        sclk_i <= not sclk_i;
        wait for clock_period/2;
    end process;
    
	-- pclk process 
    pclk_process :process
    begin
        pclk_i <= not pclk_i;
        wait for clock_period/2;
        pclk_i <= not pclk_i;
        wait for clock_period/2;
    end process;
    
end;