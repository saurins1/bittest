library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity receiver_tb is
  	generic (
  		REGISTER_W32    : INTEGER := 32;
  		REGISTER_W16    : INTEGER := 16;
  		REGISTER_W14    : INTEGER := 14;
  		REGISTER_W8     : INTEGER := 8;
  		REGISTER_W7     : INTEGER := 7;
  		REGISTER_W4     : INTEGER := 4;
  		CONFIG_DONE_WAIT: INTEGER := 4;
  		SAMPLE_FREQ     : INTEGER := 100;
  		N_CONFIG_REGISTERS : INTEGER := 5;
  		SPI_WAIT_CYCLES : INTEGER := 512;
  		SPI_2X_CLK_DIV  : INTEGER := 100       
  	);
end;

architecture bench of receiver_tb is

  component receiver
  	generic (
  		REGISTER_W32    : INTEGER := 32;
  		REGISTER_W16    : INTEGER := 16;
  		REGISTER_W14    : INTEGER := 14;
  		REGISTER_W8     : INTEGER := 8;
  		REGISTER_W7     : INTEGER := 7;
  		REGISTER_W4     : INTEGER := 4;
  		CONFIG_DONE_WAIT: INTEGER := 4;
  		SAMPLE_FREQ     : INTEGER := 100;
  		N_CONFIG_REGISTERS : INTEGER := 5;
  		SPI_WAIT_CYCLES : INTEGER := 128;
  		SPI_2X_CLK_DIV  : INTEGER := 100       
  	);
  	port (
	    aclk        : in STD_LOGIC;
      aclk_100     : in STD_LOGIC;
      aclk_200     : in STD_LOGIC;
      aresetn        : in STD_LOGIC;    
  		ADC_PAR_SERn : out STD_LOGIC;
  		ADC_ENCp     : out STD_LOGIC;
  		ADC_ENCn     : out STD_LOGIC;
  		ADC_CLKOUTp  : in STD_LOGIC;
  		ADC_CLKOUTn  : in STD_LOGIC;
  		ADC_OFp      : in STD_LOGIC;
  		ADC_OFn      : in STD_LOGIC;
  		ADC_DATAp    : in STD_LOGIC_VECTOR(REGISTER_W7-1 downto 0);
  		ADC_DATAn    : in STD_LOGIC_VECTOR(REGISTER_W7-1 downto 0);
  		ADC_RESET_EX     		: in STD_LOGIC;	
  		ADC_CONFIG_EX     		: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		ADC_CONFIG_EX_VALID     : in STD_LOGIC;
  		ADC_CONFIG_LOADED 		: out STD_LOGIC;
  		ADC_CONFIG_CURRENT     	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		ADC_CONFIG_REQUEST		: out STD_LOGIC;
  		ADC_CONFIG_REQUEST_ACK	: in STD_LOGIC;
  		ADC_CONFIG_EX_DONE		: out STD_LOGIC;
  		ADC_SPI_CONFIG : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
  		ADC_CSn      : out STD_LOGIC;
  		ADC_SCK      : out STD_LOGIC;
  		ADC_SDI      : out STD_LOGIC;
  		ADC_SDO      : in STD_LOGIC;
  				-- DEBUG --
          ADC_WRITE_COUNTER : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);  
          ADC_READ_COUNTER : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
                  ADC_ENC     : out STD_LOGIC;
          ADC_CLKOUT  : out STD_LOGIC;
          ADC_DATA    : out STD_LOGIC_VECTOR(REGISTER_W14-1 downto 0);
          ADC_SPI_CSn      : out STD_LOGIC;
          ADC_SPI_SCK      : out STD_LOGIC;
          ADC_SPI_SDI      : out STD_LOGIC;
          ADC_SPI_SDO      : out STD_LOGIC;
          
          
          
          
  		RX_DATA_VALID							: in std_logic;
  		RX_DATA_WINDOW_ON						: in std_logic;
  		RX_DATA_WINDOW							: in std_logic_vector(REGISTER_W32-1 downto 0);
  		CH_SIGNAL_AXIS_TREADY  					: in STD_LOGIC;
  		CH_SIGNAL_AXIS_TDATA	  				: out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  		CH_SIGNAL_AXIS_TLAST	  				: out STD_LOGIC;
  		CH_SIGNAL_AXIS_TVALID  					: out STD_LOGIC           
  	);
  end component;

  signal aclk_100: STD_LOGIC:= '0';
  signal aclk_200: STD_LOGIC:= '0';
  signal aresetn: STD_LOGIC:= '0';

  signal ADC_PAR_SERn: STD_LOGIC;
  signal ADC_ENCp: STD_LOGIC;
  signal ADC_ENCn: STD_LOGIC;
  signal ADC_CLKOUTp: STD_LOGIC:= '0';
  signal ADC_CLKOUTn: STD_LOGIC:= '0';
  signal ADC_OFp: STD_LOGIC:= '0';
  signal ADC_OFn: STD_LOGIC:= '0';
  signal ADC_DATAp: STD_LOGIC_VECTOR(REGISTER_W7-1 downto 0):= (others => '0');
  signal ADC_DATAn: STD_LOGIC_VECTOR(REGISTER_W7-1 downto 0):= (others => '0');
  signal ADC_CONFIG_EX: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');
  signal ADC_CONFIG_EX_VALID: STD_LOGIC:= '0';
  signal ADC_CONFIG_LOADED: STD_LOGIC;
  signal ADC_CONFIG_CURRENT: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal ADC_CONFIG_REQUEST: STD_LOGIC;
  signal ADC_CONFIG_REQUEST_ACK: STD_LOGIC;
  signal ADC_CONFIG_EX_DONE: STD_LOGIC;
  signal ADC_SPI_CONFIG: STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
  signal ADC_CSn: STD_LOGIC;
  signal ADC_SCK: STD_LOGIC;
  signal ADC_SDI: STD_LOGIC;
  signal ADC_SDO: STD_LOGIC;
  
  
  signal ADC_WRITE_COUNTER: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  signal ADC_READ_COUNTER: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  signal ADC_ENC     : STD_LOGIC;
    signal ADC_CLKOUT  : STD_LOGIC;
    signal ADC_DATA_i    : STD_LOGIC_VECTOR(REGISTER_W14-1 downto 0);
    signal ADC_SPI_CSn      : STD_LOGIC;
    signal ADC_SPI_SCK      : STD_LOGIC;
    signal ADC_SPI_SDI      : STD_LOGIC;
    signal ADC_SPI_SDO      : STD_LOGIC;
  
  
  
  signal RX_DATA_VALID: std_logic;
  signal RX_DATA_WINDOW_ON: std_logic;
  signal RX_DATA_WINDOW: std_logic_vector(REGISTER_W32-1 downto 0);
  signal CH_SIGNAL_AXIS_TREADY: STD_LOGIC;
  signal CH_SIGNAL_AXIS_TDATA: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  signal CH_SIGNAL_AXIS_TLAST: STD_LOGIC;
  signal CH_SIGNAL_AXIS_TVALID: STD_LOGIC ;

  constant clock_period: time := 10 ns;

  type states_adc_controller_sm is (idle, acquire_data, acquire_data_wait); 
  signal state_adc_controller_sm : states_adc_controller_sm; 
  
  signal ADC_CONFIG_EX_i: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');
  
  signal ADC_DATA: STD_LOGIC_VECTOR(REGISTER_W14-1 downto 0):= (others => '0');
  signal ADC_DATA_d1: STD_LOGIC_VECTOR(REGISTER_W14-1 downto 0):= (others => '0');
  signal ADC_DATA_flag: STD_LOGIC;
  signal CH_SIGNAL_AXIS_TVALID_counter: unsigned(REGISTER_W32-1 downto 0);
  

begin
	
	ADC_OFp <= '0';
	ADC_OFn <= '0';

	ADC_CLKOUTp <= transport aclk_100 after clock_period/16;
	ADC_CLKOUTn <= not ADC_CLKOUTp;

	--ADC_DATAp <= (others => '1');
	ADC_DATAn <= not ADC_DATAp;

	-- SDO SPI (read the SDO line after write addr)
    spi_sdo_sm_inst: process(aclk_100, aresetn)  
        variable my_counter  : integer range 0 to 16383 :=0;    
    begin 
        if (aresetn = '0') then
            ADC_SDO <= '0';
        elsif (aclk_100'event and aclk_100 = '1') then
            ADC_SDO <= ADC_SDI;                                                                                        
        end if;
    end process; 

    stimulus: process
    begin
        aresetn <= '0';
		CH_SIGNAL_AXIS_TREADY <= '0';
		RX_DATA_WINDOW  <= std_logic_vector(to_unsigned(8192, RX_DATA_WINDOW'length));
        wait for 100 ns;
        aresetn <= '1';
		CH_SIGNAL_AXIS_TREADY <= '1';
        wait;
    end process;
    
    ADC_CONFIG_EX_i <= ADC_CONFIG_CURRENT;
	-- GENERAL ADC SM
    adc_sm_inst: process(aclk_100, aresetn)  
        variable my_counter  : integer range 0 to 16383 :=0;    
    begin 
        if (aresetn = '0') then
			ADC_CONFIG_EX <= (others => '0');
			ADC_CONFIG_EX_VALID <= '0';
			ADC_CONFIG_REQUEST_ACK <= '0';
			ADC_DATA <= (others => '0');
			RX_DATA_VALID <= '0';	
			RX_DATA_WINDOW_ON <= '0';
			CH_SIGNAL_AXIS_TVALID_counter <= (others => '0');														
			-- state																		  	
            state_adc_controller_sm <= idle;  
        elsif (aclk_100'event and aclk_100 = '1') then
            if(CH_SIGNAL_AXIS_TVALID = '1') then
                CH_SIGNAL_AXIS_TVALID_counter <= CH_SIGNAL_AXIS_TVALID_counter + 1;
            else
                CH_SIGNAL_AXIS_TVALID_counter <= (others => '0');
            end if;
            case state_adc_controller_sm is  
                when idle =>   
					RX_DATA_VALID <= '0';	
					RX_DATA_WINDOW_ON <= '0';									
                    if(ADC_CONFIG_EX_DONE = '1') then
						my_counter := 0;
                        state_adc_controller_sm <= acquire_data_wait; 
                    end if;
                when acquire_data =>
					RX_DATA_VALID <= '1';	
					RX_DATA_WINDOW_ON <= '1';								
					if(my_counter >= 8191) then
						my_counter := 0;
						state_adc_controller_sm <= acquire_data_wait; 
					else
						if(unsigned(ADC_DATA) >= 1024) then
							ADC_DATA <= (others => '0');								
						else								
							ADC_DATA <= std_logic_vector(unsigned(ADC_DATA)+1);
						end if;
						my_counter := my_counter + 1;	
					end if;	  					 
                when acquire_data_wait =>
					RX_DATA_VALID <= '0';	
					RX_DATA_WINDOW_ON <= '0';
					ADC_DATA <= (others => '0');										
					if(my_counter >= 4) then
						my_counter := 0;
						state_adc_controller_sm <= acquire_data; 								
					else
						my_counter := my_counter + 1;									
					end if;										
                when others =>
                    null;                   
            end case;                                                                                            
        end if;
    end process;
      
    process(aclk_200) 
    begin
       if (rising_edge(aclk_200)) then
            if (aresetn = '0') then
                ADC_DATA_d1 <= (others => '0');
            else
                ADC_DATA_d1 <= ADC_DATA;
            end if;
       end if;
    end process;
    
    process(aclk_200) 
    begin
       if (rising_edge(aclk_200)) then
            if (aresetn = '0') then
                ADC_DATAp <= (others => '0');
                ADC_DATA_flag <= '0';
            else 
                if(ADC_DATA_flag = '0') then    
                    ADC_DATAp(0) <= ADC_DATA(0);
                    ADC_DATAp(1) <= ADC_DATA(2);
                    ADC_DATAp(2) <= ADC_DATA(4);
                    ADC_DATAp(3) <= ADC_DATA(6);
                    ADC_DATAp(4) <= ADC_DATA(8);
                    ADC_DATAp(5) <= ADC_DATA(10);
                    ADC_DATAp(6) <= ADC_DATA(12);
                else
                    ADC_DATAp(0) <= ADC_DATA_d1(1);
                    ADC_DATAp(1) <= ADC_DATA_d1(3);
                    ADC_DATAp(2) <= ADC_DATA_d1(5);
                    ADC_DATAp(3) <= ADC_DATA_d1(7);
                    ADC_DATAp(4) <= ADC_DATA_d1(9);
                    ADC_DATAp(5) <= ADC_DATA_d1(11);
                    ADC_DATAp(6) <= ADC_DATA_d1(13);
                end if;
                ADC_DATA_flag <= not ADC_DATA_flag;
            end if;
       end if;
    end process;
				
	-- aclk process 
    aclk_process :process
    begin
        aclk_100 <= not aclk_100;
		aclk_200 <= not aclk_200;
        wait for clock_period/4;
		aclk_200 <= not aclk_200;
		wait for clock_period/4;
        aclk_100 <= not aclk_100;
		aclk_200 <= not aclk_200;
		wait for clock_period/4;
		aclk_200 <= not aclk_200;	
        wait for clock_period/4;
    end process;

  -- Insert values for generic parameters !!
  uut: receiver generic map ( REGISTER_W32           => REGISTER_W32,
                              REGISTER_W16           => REGISTER_W16,
                              REGISTER_W14           => REGISTER_W14,
                              REGISTER_W8            => REGISTER_W8,
                              REGISTER_W7            => REGISTER_W7,
                              REGISTER_W4            => REGISTER_W4,
                              CONFIG_DONE_WAIT       => CONFIG_DONE_WAIT,
                              SAMPLE_FREQ            => SAMPLE_FREQ,
                              N_CONFIG_REGISTERS     => N_CONFIG_REGISTERS,
                              SPI_WAIT_CYCLES        => SPI_WAIT_CYCLES,
                              SPI_2X_CLK_DIV         => SPI_2X_CLK_DIV )
                   port map ( aclk                   => aclk_100,
                              aclk_100               => aclk_100,
                              aclk_200               => aclk_200,
                              aresetn                => aresetn,
                              ADC_PAR_SERn           => ADC_PAR_SERn,
                              ADC_ENCp               => ADC_ENCp,
                              ADC_ENCn               => ADC_ENCn,
                              ADC_CLKOUTp            => ADC_CLKOUTp,
                              ADC_CLKOUTn            => ADC_CLKOUTn,
                              ADC_OFp                => ADC_OFp,
                              ADC_OFn                => ADC_OFn,
                              ADC_DATAp              => ADC_DATAp,
                              ADC_DATAn              => ADC_DATAn,
                              ADC_RESET_EX           => '0',
                              ADC_CONFIG_EX          => ADC_CONFIG_EX,
                              ADC_CONFIG_EX_VALID    => ADC_CONFIG_EX_VALID,
                              ADC_CONFIG_LOADED      => ADC_CONFIG_LOADED,
                              ADC_CONFIG_CURRENT     => ADC_CONFIG_CURRENT,
                              ADC_CONFIG_REQUEST     => ADC_CONFIG_REQUEST,
                              ADC_CONFIG_REQUEST_ACK => ADC_CONFIG_REQUEST_ACK,
                              ADC_CONFIG_EX_DONE     => ADC_CONFIG_EX_DONE,
                              ADC_SPI_CONFIG         => ADC_SPI_CONFIG,
                              ADC_CSn                => ADC_CSn,
                              ADC_SCK                => ADC_SCK,
                              ADC_SDI                => ADC_SDI,
                              ADC_SDO                => ADC_SDO,
                              ADC_WRITE_COUNTER      => ADC_WRITE_COUNTER,
                              ADC_READ_COUNTER       => ADC_READ_COUNTER,
                              ADC_ENC      => ADC_ENC,
                              ADC_CLKOUT       => ADC_CLKOUT,
                              ADC_DATA       => ADC_DATA_i,
                              ADC_SPI_CSn       => ADC_SPI_CSn,
                              ADC_SPI_SCK       => ADC_SPI_SCK,
                              ADC_SPI_SDI       => ADC_SPI_SDI,
                              ADC_SPI_SDO       => ADC_SPI_SDO,
                              RX_DATA_VALID          => RX_DATA_VALID,
                              RX_DATA_WINDOW_ON      => RX_DATA_WINDOW_ON,
                              RX_DATA_WINDOW         => RX_DATA_WINDOW,
                              CH_SIGNAL_AXIS_TREADY  => CH_SIGNAL_AXIS_TREADY,
                              CH_SIGNAL_AXIS_TDATA   => CH_SIGNAL_AXIS_TDATA,
                              CH_SIGNAL_AXIS_TLAST   => CH_SIGNAL_AXIS_TLAST,
                              CH_SIGNAL_AXIS_TVALID  => CH_SIGNAL_AXIS_TVALID );


end;