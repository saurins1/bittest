----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 01/09/2017 10:07:23 AM
-- Design Name: 
-- Module Name: Pulser - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity receiver is
	generic (
		REGISTER_W32    : INTEGER := 32;
		REGISTER_W16    : INTEGER := 16;
		REGISTER_W14    : INTEGER := 14;
		REGISTER_W8     : INTEGER := 8;
		REGISTER_W7     : INTEGER := 7;
		REGISTER_W4     : INTEGER := 4;
		CONFIG_DONE_WAIT: INTEGER := 4;
		SAMPLE_FREQ     : INTEGER := 100;
		N_CONFIG_REGISTERS : INTEGER := 5;
		SPI_WAIT_CYCLES : INTEGER := 512;
		SPI_2X_CLK_DIV  : INTEGER := 100       
	);
	port (
	    -- Sync
	    aclk        : in STD_LOGIC;
	    aclk_100 	: in STD_LOGIC;
		aclk_200 	: in STD_LOGIC;
        aresetn		: in STD_LOGIC;	

		-------- ADC --------
		
		-- Programing mode seletion
		ADC_PAR_SERn : out STD_LOGIC;

		-- encode (type of edge for conversion starts)
		ADC_ENCp     : out STD_LOGIC;
		ADC_ENCn     : out STD_LOGIC;

		-- Data output clock
		ADC_CLKOUTp  : in STD_LOGIC;
		ADC_CLKOUTn  : in STD_LOGIC;

		-- Overflow
		ADC_OFp      : in STD_LOGIC;
		ADC_OFn      : in STD_LOGIC;

		-- Data in 
		ADC_DATAp    : in STD_LOGIC_VECTOR(REGISTER_W7-1 downto 0);
		ADC_DATAn    : in STD_LOGIC_VECTOR(REGISTER_W7-1 downto 0);

		-- External config	
		ADC_RESET_EX     		: in STD_LOGIC;	
		ADC_CONFIG_EX     		: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		ADC_CONFIG_EX_VALID     : in STD_LOGIC;
		ADC_CONFIG_LOADED 		: out STD_LOGIC;
		ADC_CONFIG_CURRENT     	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

		ADC_CONFIG_REQUEST		: out STD_LOGIC;
		ADC_CONFIG_REQUEST_ACK	: in STD_LOGIC;

		ADC_CONFIG_EX_DONE		: out STD_LOGIC;

		-- SPI
		ADC_SPI_CONFIG : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0); -- debug

		ADC_CSn      : out STD_LOGIC;
		ADC_SCK      : out STD_LOGIC;
		ADC_SDI      : out STD_LOGIC;
		ADC_SDO      : in STD_LOGIC;  
		
		-- DEBUG --
        ADC_WRITE_COUNTER : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);  
        ADC_READ_COUNTER : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        ADC_ENC     : out STD_LOGIC;
        ADC_CLKOUT  : out STD_LOGIC;
        ADC_DATA    : out STD_LOGIC_VECTOR(REGISTER_W14-1 downto 0);
        ADC_SPI_CSn      : out STD_LOGIC;
        ADC_SPI_SCK      : out STD_LOGIC;
        ADC_SPI_SDI      : out STD_LOGIC;
        ADC_SPI_SDO      : out STD_LOGIC;
		
		-------- CHANNEL RECEIVER --------
		RX_DATA_VALID							: in std_logic;
		RX_DATA_WINDOW_ON						: in std_logic;
		RX_DATA_WINDOW							: in std_logic_vector(REGISTER_W32-1 downto 0);

		-- Receiver data stream output to other units
		CH_SIGNAL_AXIS_TREADY  					: in STD_LOGIC;
		CH_SIGNAL_AXIS_TDATA	  				: out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		CH_SIGNAL_AXIS_TLAST	  				: out STD_LOGIC;
		CH_SIGNAL_AXIS_TVALID  					: out STD_LOGIC           
	);
end receiver;

architecture arch_imp of receiver is
	
	-- ADC 
	component adc_controller is
		generic (
			REGISTER_W32    : INTEGER := 32;
			REGISTER_W16    : INTEGER := 16;
			REGISTER_W14    : INTEGER := 14;
			REGISTER_W8     : INTEGER := 8;
			REGISTER_W7     : INTEGER := 7;
			REGISTER_W4     : INTEGER := 4;
			CONFIG_DONE_WAIT: INTEGER := 4;
			SAMPLE_FREQ     : INTEGER := 100;
			N_CONFIG_REGISTERS : INTEGER := 5;
			SPI_WAIT_CYCLES : INTEGER := 512;
			SPI_2X_CLK_DIV  : INTEGER := 100       
		);
		port (
			-- Sync
			aclk         : in STD_LOGIC;
			aclk_100     : in STD_LOGIC;
			aclk_200     : in STD_LOGIC;
			aresetn      : in STD_LOGIC;

			-- Programing mode seletion
			ADC_PAR_SERn : out STD_LOGIC;

			-- encode (type of edge for conversion starts)
			ADC_ENCp     : out STD_LOGIC;
			ADC_ENCn     : out STD_LOGIC;

			-- Data output clock
			ADC_CLKOUTp  : in STD_LOGIC;
			ADC_CLKOUTn  : in STD_LOGIC;

			-- Overflow
			ADC_OFp      : in STD_LOGIC;
			ADC_OFn      : in STD_LOGIC;

			-- Data in 
			ADC_DATAp    : in STD_LOGIC_VECTOR(REGISTER_W7-1 downto 0);
			ADC_DATAn    : in STD_LOGIC_VECTOR(REGISTER_W7-1 downto 0);

			-- Data acquired
			ADC_DATA        : out STD_LOGIC_VECTOR(REGISTER_W14-1 downto 0);

			-- Extrnal config  		
			ADC_RESET_EX     		: in STD_LOGIC;	
			ADC_CONFIG_EX     		: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			ADC_CONFIG_EX_VALID     : in STD_LOGIC;
			ADC_CONFIG_LOADED 		: out STD_LOGIC;
			ADC_CONFIG_CURRENT     	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

			ADC_CONFIG_REQUEST		: out STD_LOGIC;
			ADC_CONFIG_REQUEST_ACK	: in STD_LOGIC;

			ADC_CONFIG_EX_DONE		: out STD_LOGIC;

			-- SPI 
			ADC_SPI_CONFIG : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0); -- debug

			ADC_CSn      : out STD_LOGIC;
			ADC_SCK      : out STD_LOGIC;
			ADC_SDI      : out STD_LOGIC;
			ADC_SDO      : in STD_LOGIC;
			
			-- DEBUG --
            ADC_WRITE_COUNTER : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);  
            ADC_READ_COUNTER : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
            ADC_ENC     : out STD_LOGIC;
            ADC_CLKOUT  : out STD_LOGIC;
            ADC_SPI_CSn      : out STD_LOGIC;
            ADC_SPI_SCK      : out STD_LOGIC;
            ADC_SPI_SDI      : out STD_LOGIC;
            ADC_SPI_SDO      : out STD_LOGIC
		);
	end component adc_controller;

	signal adc_data_i 	: STD_LOGIC_VECTOR(REGISTER_W14-1 downto 0);
	
	---- CHANNEL RECEIVER ----
	component channel_receiver is
		generic (
			REGISTER_W32    : INTEGER := 32;
			REGISTER_W16    : INTEGER := 16  
		);
		port (
			-- axis sync
			aclk                    				: in STD_LOGIC; 
			aresetn                 				: in STD_LOGIC; 

			RX_DATA_VALID							: in std_logic;
			RX_DATA_WINDOW_ON						: in std_logic;
			RX_DATA_WINDOW							: in std_logic_vector(REGISTER_W32-1 downto 0);

			-- Receiver data from digitizer units
			CH_ADC_DATA_IN							: in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);

			-- Receiver data stream output to other units
			CH_SIGNAL_AXIS_TREADY  					: in STD_LOGIC;
			CH_SIGNAL_AXIS_TDATA	  				: out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
			CH_SIGNAL_AXIS_TLAST	  				: out STD_LOGIC;
			CH_SIGNAL_AXIS_TVALID  					: out STD_LOGIC
		);
	end component channel_receiver;

	signal ch_adc_data_in	: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	     
begin
	
	---- ADC ----
	LTC5152CUJ_14_PBF_adc_controller: adc_controller
		generic map(
			REGISTER_W32    => REGISTER_W32,
			REGISTER_W16    => REGISTER_W16,
			REGISTER_W14    => REGISTER_W14,
			REGISTER_W8     => REGISTER_W8,
			REGISTER_W7     => REGISTER_W7,
			REGISTER_W4     => REGISTER_W4,
			CONFIG_DONE_WAIT=> CONFIG_DONE_WAIT,
			SAMPLE_FREQ     => SAMPLE_FREQ,
			N_CONFIG_REGISTERS => N_CONFIG_REGISTERS,
			SPI_WAIT_CYCLES => SPI_WAIT_CYCLES,
			SPI_2X_CLK_DIV  => SPI_2X_CLK_DIV     
		)
		port map(
			-- Sync
			aclk         => aclk,
			aclk_100     => aclk_100,
			aclk_200     => aclk_200,
			aresetn      => aresetn,

			-- Programing mode seletion
			ADC_PAR_SERn => ADC_PAR_SERn,

			-- encode (type of edge for conversion starts)
			ADC_ENCp     => ADC_ENCp,
			ADC_ENCn     => ADC_ENCn,

			-- Data output clock
			ADC_CLKOUTp  => ADC_CLKOUTp,
			ADC_CLKOUTn  => ADC_CLKOUTn,

			-- Overflow
			ADC_OFp      => ADC_OFp,
			ADC_OFn      => ADC_OFn,

			-- Data in 
			ADC_DATAp    => ADC_DATAp,
			ADC_DATAn    => ADC_DATAn,

			-- Data acquired
			ADC_DATA        => adc_data_i,

			-- Extrnal config 
			ADC_RESET_EX            => ADC_RESET_EX,		
			ADC_CONFIG_EX     		=> ADC_CONFIG_EX,
			ADC_CONFIG_EX_VALID     => ADC_CONFIG_EX_VALID,
			ADC_CONFIG_LOADED 		=> ADC_CONFIG_LOADED,
			ADC_CONFIG_CURRENT     	=> ADC_CONFIG_CURRENT,

			ADC_CONFIG_REQUEST		=> ADC_CONFIG_REQUEST,
			ADC_CONFIG_REQUEST_ACK	=> ADC_CONFIG_REQUEST_ACK,

			ADC_CONFIG_EX_DONE		=> ADC_CONFIG_EX_DONE,

			-- SPI 
			ADC_SPI_CONFIG => ADC_SPI_CONFIG,

			ADC_CSn      => ADC_CSn,
			ADC_SCK      => ADC_SCK,
			ADC_SDI      => ADC_SDI,
			ADC_SDO      => ADC_SDO,
			
			-- DEBUG  
			ADC_WRITE_COUNTER => ADC_WRITE_COUNTER,
			ADC_READ_COUNTER => ADC_READ_COUNTER,
			ADC_ENC => ADC_ENC,
			ADC_CLKOUT => ADC_CLKOUT,
			ADC_SPI_CSn => ADC_SPI_CSn,
			ADC_SPI_SCK => ADC_SPI_SCK,
			ADC_SPI_SDI => ADC_SPI_SDI,
			ADC_SPI_SDO => ADC_SPI_SDO
		);
				
	ch_adc_data_in <= std_logic_vector(resize(signed(adc_data_i), ch_adc_data_in'length));
	
	ADC_DATA <= adc_data_i;
	
	---- CHANNEL RECEIVER ----
	channel_receiver_inst: channel_receiver
		generic map(
			REGISTER_W32    => REGISTER_W32,
			REGISTER_W16    => REGISTER_W16  
		)
		port map(
			-- axis sync
			aclk 		=> aclk,
			aresetn 	=> aresetn, 

			RX_DATA_VALID		=> RX_DATA_VALID,
			RX_DATA_WINDOW_ON	=> RX_DATA_WINDOW_ON,
			RX_DATA_WINDOW		=> RX_DATA_WINDOW,

			-- Receiver data from digitizer units
			CH_ADC_DATA_IN		=> ch_adc_data_in,

			-- Receiver data stream output to other units
			CH_SIGNAL_AXIS_TREADY 	=> CH_SIGNAL_AXIS_TREADY,
			CH_SIGNAL_AXIS_TDATA	=> CH_SIGNAL_AXIS_TDATA,
			CH_SIGNAL_AXIS_TLAST	=> CH_SIGNAL_AXIS_TLAST,
			CH_SIGNAL_AXIS_TVALID  	=> CH_SIGNAL_AXIS_TVALID
		);
																					   					     					      									 	   			                 
end arch_imp;
