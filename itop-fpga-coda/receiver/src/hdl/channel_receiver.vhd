----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez - Wei Jiang
-- 
-- Create Date: 01/09/2017 10:07:23 AM
-- Design Name: 
-- Module Name: channel receiver - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity channel_receiver is
	generic (
	    REGISTER_W32    : INTEGER := 32;
	    REGISTER_W16    : INTEGER := 16  
	);
	port (
		-- axis sync
		aclk                    				: in STD_LOGIC; 
		aresetn                 				: in STD_LOGIC; 

		RX_DATA_VALID							: in std_logic;
		RX_DATA_WINDOW_ON						: in std_logic;
		RX_DATA_WINDOW							: in std_logic_vector(REGISTER_W32-1 downto 0);

		-- Receiver data from digitizer units
		CH_ADC_DATA_IN							: in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);

		-- Receiver data stream output to other units
		CH_SIGNAL_AXIS_TREADY  					: in STD_LOGIC;
		CH_SIGNAL_AXIS_TDATA	  				: out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		CH_SIGNAL_AXIS_TLAST	  				: out STD_LOGIC;
		CH_SIGNAL_AXIS_TVALID  					: out STD_LOGIC
	);
end channel_receiver;

architecture Behavioral of channel_receiver is

signal count_data_window : std_logic_vector(REGISTER_W32-1 downto 0);
signal rx_data_window_m1 : std_logic_vector(REGISTER_W32-1 downto 0);
	
begin
	
	rx_data_window_m1 <= std_logic_vector(unsigned(RX_DATA_WINDOW) - 1);
	
	process (aclk)
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				count_data_window <= (others => '0');
				CH_SIGNAL_AXIS_TDATA <= (others => '0');		
				CH_SIGNAL_AXIS_TLAST <= '0';
				CH_SIGNAL_AXIS_TVALID <= '0';
        	else
				if(CH_SIGNAL_AXIS_TREADY = '1' and RX_DATA_WINDOW_ON = '1') then
					if(RX_DATA_VALID = '1') then
						CH_SIGNAL_AXIS_TVALID <= '1';
						if(count_data_window = rx_data_window_m1 ) then
							CH_SIGNAL_AXIS_TLAST <= '1';	
						end if;
						if(CH_ADC_DATA_IN(13 downto 0) = "10000000000000") then
						    CH_SIGNAL_AXIS_TDATA <= "1110000000000001";
						else
						    CH_SIGNAL_AXIS_TDATA <= CH_ADC_DATA_IN;	
					    end if;
						count_data_window <= std_logic_vector(unsigned(count_data_window) + 1);
					else
						CH_SIGNAL_AXIS_TLAST <= '0';
						CH_SIGNAL_AXIS_TVALID <= '0';
					end if;
				else
					count_data_window <= (others => '0');
					CH_SIGNAL_AXIS_TLAST <= '0';
					CH_SIGNAL_AXIS_TVALID <= '0';
				end if;
			end if;
		end if;
	end process;	

end Behavioral;
			