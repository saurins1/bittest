#
# Vivado (TM) v2016.4 (64-bit)
#
# build.tcl: Tcl script for re-creating project 'channel'
#

################################################################################
# define names
################################################################################

set project_name channel

################################################################################
# define paths
################################################################################

# Set the reference directory for source file relative paths (by default the value is script directory path)
set origin_dir 		"."

set common_repo 	"itop-fpga-common"

set path_sim 		$origin_dir/sim
set path_src 		$origin_dir/src/hdl
set path_src_common 	$origin_dir/../../$common_repo/channel_v1_4_fft/src/hdl
set path_ip 		$origin_dir/src/ip

# Set the directory path for the original project from where this script was exported
set orig_proj_dir "[file normalize "$origin_dir/$project_name"]"

################################################################################
# setup an project
################################################################################

# Create project
create_project $project_name $origin_dir/$project_name -part xc7z020clg400-1 -force 

# Set the directory path for the new project
set proj_dir [get_property directory [current_project]]

# Set project properties
set obj [get_projects $project_name]
set_property "board_part" "em.avnet.com:microzed_7020:part0:1.1" $obj
set_property "default_lib" "xil_defaultlib" $obj
set_property "ip_cache_permissions" "read write" $obj
set_property "sim.ip.auto_export_scripts" "1" $obj
set_property "simulator_language" "Mixed" $obj
set_property "target_language" "VHDL" $obj
set_property "xpm_libraries" "XPM_CDC XPM_MEMORY" $obj

################################################################################
# setup constraints
################################################################################

# Create 'constrs_1' fileset (if not found)
if {[string equal [get_filesets -quiet constrs_1] ""]} {
  create_fileset -constrset constrs_1
}

#add_files -fileset constrs_1      $path_constrs/thickness_gate_constraints.xdc

################################################################################
# setup simulation
################################################################################

# Create 'sim_1' fileset (if not found)
if {[string equal [get_filesets -quiet sim_1] ""]} {
  create_fileset -simset sim_1
}

# Set 'sim_1' fileset object
set obj [get_filesets sim_1]
set files [list \
 "[file normalize "$path_sim/src/channel_tb.vhd"]"\
 "[file normalize "$path_sim/simulation_files/configuration.dat"]"\
 "[file normalize "$path_sim/simulation_files/configuration_1.dat"]"\
 "[file normalize "$path_sim/simulation_files/configuration_1.dat"]"\
 "[file normalize "$path_sim/simulation_files/ascan.dat"]"\
 "[file normalize "$path_sim/simulation_files/wave1.dat"]"\
 "[file normalize "$path_sim/simulation_files/wave2.dat"]"\
 "[file normalize "$path_sim/simulation_files/channel_tb_behav.wcfg"]"\
]
add_files -norecurse -fileset $obj $files

# Set 'sim_1' fileset file properties for remote files
set file "$path_sim/src/channel_tb.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sim_1] [list "*$file"]]
set_property "file_type" "VHDL" $file_obj

set file "$path_sim/simulation_files/configuration.dat"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sim_1] [list "*$file"]]
set_property "file_type" "Data Files" $file_obj

set file "$path_sim/simulation_files/configuration_1.dat"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sim_1] [list "*$file"]]
set_property "file_type" "Data Files" $file_obj

set file "$path_sim/simulation_files/configuration_1.dat"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sim_1] [list "*$file"]]
set_property "file_type" "Data Files" $file_obj

set file "$path_sim/simulation_files/ascan.dat"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sim_1] [list "*$file"]]
set_property "file_type" "Data Files" $file_obj

set file "$path_sim/simulation_files/wave1.dat"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sim_1] [list "*$file"]]
set_property "file_type" "Data Files" $file_obj

set file "$path_sim/simulation_files/wave2.dat"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sim_1] [list "*$file"]]
set_property "file_type" "Data Files" $file_obj

# Set 'sim_1' fileset file properties for local files
# None

# Set 'sim_1' fileset properties
set obj [get_filesets sim_1]
set_property "top" "channel_tb" $obj
set_property "transport_int_delay" "0" $obj
set_property "transport_path_delay" "0" $obj
set_property "xelab.nosort" "1" $obj
set_property "xelab.unifast" "" $obj
set_property "xsim.view" "$path_sim/simulation_files/channel_tb_behav.wcfg \
$path_sim/simulation_files/channel_tb_behav.wcfg" $obj

################################################################################
# setup sources
################################################################################

# Create 'sources_1' fileset (if not found)
if {[string equal [get_filesets -quiet sources_1] ""]} {
  create_fileset -srcset sources_1
}

# Digital amplifier
read_ip		$origin_dir/../digital_amplifier/src/ip/digital_amplifier_multiplier_16_32/digital_amplifier_multiplier_16_32.xci
add_files	$origin_dir/../../$common_repo/digital_amplifier/src/hdl/digital_amplifier.vhd

# Dsp
add_files	$origin_dir/../../$common_repo/dsp_filter/src/hdl/dsp_filter.vhd

# Coincidence
read_ip		$origin_dir/../coincidence_filter_v1_2/src/ip/coincidence_filter_BRAM/coincidence_filter_BRAM.xci
add_files	$origin_dir/../../$common_repo/coincidence_filter_v1_2/src/hdl/coincidence_filter.vhd

# Averaging
read_ip		$origin_dir/../average_filter_v1_2/src/ip/average_filter_BRAM/average_filter_BRAM.xci
read_ip		$origin_dir/../average_filter_v1_2/src/ip/average_filter_divider_24_8/average_filter_divider_24_8.xci
read_ip		$origin_dir/../average_filter_v1_2/src/ip/average_filter_multiplier_16_8/average_filter_multiplier_16_8.xci
add_files	$origin_dir/../../$common_repo/average_filter_v1_2/src/hdl/average_filter.vhd

# Fir corr
read_ip		$origin_dir/../fir_correlation_light/src/ip/fir_correlation_DSP/fir_correlation_DSP.xci
read_ip		$origin_dir/../fir_correlation_light/src/ip/fir_correlation_taps_BRAM/fir_correlation_taps_BRAM.xci
read_ip		$origin_dir/../fir_correlation_light/src/ip/fir_correlation_signal_BRAM/fir_correlation_signal_BRAM.xci
add_files	$origin_dir/../../$common_repo/fir_correlation_light/src/hdl/fir_correlation.vhd

# Gates
add_files	$origin_dir/../../$common_repo/gates_processor/src/hdl/divisor_complement_32_16.vhd
add_files	$origin_dir/../../$common_repo/gates_processor/src/hdl/divisor_complement_64_64.vhd
add_files	$origin_dir/../../$common_repo/gates_processor_fft/src/hdl/gates_processor.vhd
add_files	$origin_dir/../../$common_repo/gates_processor_fft/src/hdl/gate2_compute.vhd
add_files	$origin_dir/../../$common_repo/gates_processor/src/hdl/gatex_compute.vhd
add_files	$origin_dir/../../$common_repo/fft_gate/src/hdl/fft_gate.vhd
add_files	$origin_dir/../../$common_repo/fft_gate/src/hdl/fft_gate_cerror.vhd
read_ip		$origin_dir/../fft_gate/src/ip/fft_gate_FFT/fft_gate_FFT.xci
read_ip		$origin_dir/../fft_gate/src/ip/fft_gate_BRAM/fft_gate_BRAM.xci
read_ip		$origin_dir/../fft_gate/src/ip/fft_gate_FIFO/fft_gate_FIFO.xci
read_ip		$origin_dir/../fft_gate/src/ip/fft_MUL/fft_MUL.xci
read_ip		$origin_dir/../fft_gate/src/ip/fft_complex_MUL/fft_complex_MUL.xci
read_ip		$origin_dir/../fft_gate/src/ip/fft_gate_CORDIC/fft_gate_CORDIC.xci
read_ip		$origin_dir/../fft_gate/src/ip/fft_MUL_2/fft_MUL_2.xci

# Configuration
read_ip		$origin_dir/../channel_configuration/src/ip/channel_configuration_fft_BRAM/channel_configuration_BRAM.xci
add_files	$origin_dir/../../$common_repo/channel_configuration/src/hdl/channel_configuration.vhd

# PRF calculator
read_ip		$origin_dir/../prf_calculator/src/ip/prf_calculator_divider_32_16/prf_calculator_divider_32_16.xci
add_files	$origin_dir/../../$common_repo/prf_calculator/src/hdl/prf_calculator.vhd

#decimator Filter
read_ip		$origin_dir/../decimator_filter/src/ip/decimator_filter_fft_BRAM/decimator_filter_BRAM.xci
add_files	$origin_dir/../../$common_repo/decimator_filter/src/hdl/decimator_filter.vhd
add_files	$origin_dir/../../$common_repo/ascan_decimator/src/hdl/ascan_decimator.vhd

# Channel 
add_files	$path_src_common/channel.vhd

# Update the compile order
update_compile_order -fileset sources_1
update_compile_order -fileset sim_1

puts "INFO: Project created:${project_name}"

# Delete temp vivado files
file delete {*}[glob vivado*]
