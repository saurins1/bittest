clear all;close all;

% CONFIG 
config = csvread('configuration.csv',1,2);
config = [0 config'];

file_config = fopen('configuration.dat','w');
fwrite(file_config,config,'int64');
fclose(file_config);

% ASCAN

file_ascan = fopen('ascan.dat','r');
ascan = fread(file_ascan,'int64');
fclose(file_ascan);

figure(1);
plot(ascan);

% CORRELATION
correlation_start = 302;
correlation_end = 1801;
correlation_size = correlation_end - correlation_start + 1;

correlation_result_x = xcorr(ascan(correlation_start:correlation_end));
correlation_result = correlation_result_x(correlation_size:2*correlation_size-1);

file_correlation = fopen('correlation.dat','w');
fwrite(file_correlation,correlation_result,'int64');
fclose(file_correlation);

figure(2);
plot(correlation_result);

%TOF

%G1 PEAK

y_minus_peak_g1 = 1577;
y_plus_peak_g1  = 1557;
y_peak_g1       = 1591;
peak_tof_g1     = 271;

gate_start_peak_g1 = 300;
rx_delay_g1 = 0;

divition_g1 = round((1024/2)*(y_minus_peak_g1 - y_plus_peak_g1) / (y_minus_peak_g1 + y_plus_peak_g1 - y_peak_g1 - y_peak_g1 ));
tof_g1 = (gate_start_peak_g1+rx_delay_g1+peak_tof_g1)*1024 + divition_g1;

%G2 PEAK

y_minus_peak_g2 = 167941331;
y_plus_peak_g2  = 168452795;
y_peak_g2       = 169631217;
peak_tof_g2     = 8736;

gate_start_peak_g2 = 0;
rx_delay_g2 = 0;

divition_g2 = round((1024/2)*(y_minus_peak_g2 - y_plus_peak_g2) / (y_minus_peak_g2 + y_plus_peak_g2 - y_peak_g2 - y_peak_g2 ));
tof_g2 = (gate_start_peak_g2+rx_delay_g2+peak_tof_g2)*1024 + divition_g2;

%G2 PEAK

y1_g2 = 578;
y2_g2  = 670;
threshold_g2  = 600;
ef_tof_g2     = 90;

gate_start_ef_g2 = 300;

divition_ef_g2 = round((1024)*(threshold_g2 - y1_g2) / (y2_g2 -y1_g2));
ef_tof_g2 = (gate_start_ef_g2+rx_delay_g2+ef_tof_g2)*1024 + divition_ef_g2;

y_minus_peak_g2 = 167941331;
y_plus_peak_g2  = 168452795;
y_peak_g2       = 169631217;
peak_tof_g2     = 8736;

gate_start_peak_g2 = 0;
rx_delay_g2 = 0;

divition_g2 = round((1024/2)*(y_minus_peak_g2 - y_plus_peak_g2) / (y_minus_peak_g2 + y_plus_peak_g2 - y_peak_g2 - y_peak_g2 ));
tof_g2 = (gate_start_peak_g2+rx_delay_g2+peak_tof_g2)*1024 + divition_g2;

%G3 PEAK

y_minus_peak_g3 = 1238;
y_plus_peak_g3  = 1237;
y_peak_g3       = 1248;
peak_tof_g3     = 1256;

gate_start_peak_g3 = 8000;
rx_delay_g3 = 0;

divition_g3 = round((1024/2)*(y_minus_peak_g3 - y_plus_peak_g3) / (y_minus_peak_g3 + y_plus_peak_g3 - y_peak_g3 - y_peak_g3 ));
tof_g3 = (gate_start_peak_g3+rx_delay_g3+peak_tof_g3)*1024 + divition_g3;