----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 01/17/2017 10:07:23 AM
-- Design Name: 
-- Module Name: dio - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity dio is
	generic (
		REGISTER_W8     : INTEGER := 8; 
		REGISTER_W6     : INTEGER := 6; 
		FILTER_LEVEL    : INTEGER := 4
	);
	port (
	    -- Sync
	    aclk 	: in STD_LOGIC;
        aresetn	: in STD_LOGIC;
        
		RX_DATA_WINDOW_ON : in STD_LOGIC;
        PULSER_CONTROL_P  : in STD_LOGIC;
        PULSER_CONTROL_N  : in STD_LOGIC;
        HVPS_EN           : in STD_LOGIC;
        	
		-- Input
		IO_INPUTS_I 	: in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);	
		IO_INPUTS_O 	: out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
		
		-- Output		
		IO_OUTPUT_I 	: in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);	
		IO_OUTPUT_O 	: out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0)            
	);
end dio;

architecture arch_imp of dio is
	
	-- Digital filter
	component input_digital_filter is
		generic (
			FILTER_LEVEL		: INTEGER := 4
		);
		Port ( 
			-- sync
			aclk     : in STD_LOGIC;
			aresetn  : in STD_LOGIC;

			-- input/output
			DIN 	: in STD_LOGIC; 
			CE 		: in STD_LOGIC; 
			DOUT 	: out STD_LOGIC
		);
	end component input_digital_filter;
	
	signal hvps_en_final : STD_LOGIC;
	-- State machine A
    type states_hvps_en is (hvps_on_1, hvps_on_2, hvps_off);
    signal state_hvps_en : states_hvps_en;
	       
begin
		
	inputs_process: process(aclk)
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                IO_OUTPUT_O(REGISTER_W8-1 downto 0) <= (others => '0');
            else
                -- Generic Output
                IO_OUTPUT_O(5 downto 0) <= IO_OUTPUT_I(5 downto 0);   
                -- HVPS
                --IO_OUTPUT_O(6) <= RX_DATA_WINDOW_ON; --not RX_DATA_WINDOW_ON
                IO_OUTPUT_O(6) <= hvps_en_final; --not RX_DATA_WINDOW_ON
                -- Not used
                IO_OUTPUT_O(7) <= '0';                                                      
            end if;
        end if;
    end process;

	-- Generic Input
	input_digital_filters: for i in 0 to REGISTER_W6-1 generate
		input_digital_filter_inst: input_digital_filter 
			generic map(
				FILTER_LEVEL => FILTER_LEVEL
			)
			Port map( 
				-- sync
				aclk     => aclk,
				aresetn  => aresetn,

				-- input/output
				DIN 	=> IO_INPUTS_I(i), 
				CE 		=> '1',
				DOUT 	=> IO_INPUTS_O(i)
			);
	 end generate;
	 
	 -- Encoder Alarm
	 input_digital_filter_inst: input_digital_filter 
         generic map(
             FILTER_LEVEL => FILTER_LEVEL
         )
         Port map( 
             -- sync
             aclk     => aclk,
             aresetn  => aresetn,

             -- input/output
             DIN     => IO_INPUTS_I(7), 
             CE         => '1',
             DOUT     => IO_INPUTS_O(7)
         );
	 
	 -- Temp sensor SDO
	 IO_INPUTS_O(6) <= IO_INPUTS_I(6);
	 
	 -- HVPS state machine
     hvps_en_state_machine: process(aclk) 
     begin 
         if (rising_edge(aclk)) then
             if (aresetn = '0') then
                 hvps_en_final <= '0';
                 state_hvps_en <= hvps_off;
             else
                 case state_hvps_en is
                     when hvps_off =>
                        hvps_en_final <= '0';
                        if(PULSER_CONTROL_P = '1' or PULSER_CONTROL_N = '1' or HVPS_EN = '1') then
                            state_hvps_en <= hvps_on_1;
                        end if;
                     when hvps_on_1 =>
                        hvps_en_final <= '1';
                        if(RX_DATA_WINDOW_ON = '1') then
                            state_hvps_en <= hvps_on_2;
                        end if;
                     when hvps_on_2 =>
                        if(RX_DATA_WINDOW_ON = '0') then
                            state_hvps_en <= hvps_off;
                        end if;
					 when others =>
                         null;
                 end case;
             end if;
          end if;
     end process;
                       																				   					     					      									 	   			                 
end arch_imp;
