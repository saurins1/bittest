library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity average_filter_divider_24_8_tb is
end;

architecture bench of average_filter_divider_24_8_tb is

	COMPONENT average_filter_divider_24_8
	  PORT (
		aclk : IN STD_LOGIC;
		aclken : IN STD_LOGIC;
		aresetn : IN STD_LOGIC;
		s_axis_divisor_tvalid : IN STD_LOGIC;
        s_axis_divisor_tlast : IN STD_LOGIC;
		s_axis_divisor_tdata : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
		s_axis_dividend_tvalid : IN STD_LOGIC;
        s_axis_dividend_tlast : IN STD_LOGIC;
		s_axis_dividend_tdata : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
		m_axis_dout_tvalid : OUT STD_LOGIC;
        m_axis_dout_tlast : OUT STD_LOGIC;
		m_axis_dout_tdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
	  );
	END COMPONENT;

  	signal aclk: std_logic:= '0';
  	signal aresetn: std_logic:= '0';
  	signal aclken: std_logic:= '0';

    signal s_axis_divisor_tvalid : STD_LOGIC:= '0';
    signal s_axis_divisor_tlast : STD_LOGIC:= '0';
    signal s_axis_divisor_tdata : STD_LOGIC_VECTOR(7 DOWNTO 0):= (others => '0');
    signal s_axis_dividend_tvalid : STD_LOGIC:= '0';
    signal s_axis_dividend_tlast : STD_LOGIC:= '0';
    signal s_axis_dividend_tdata : STD_LOGIC_VECTOR(23 DOWNTO 0):= (others => '0');
    signal m_axis_dout_tvalid : STD_LOGIC;
    signal m_axis_dout_tlast : STD_LOGIC;
    signal m_axis_dout_tdata : STD_LOGIC_VECTOR(31 DOWNTO 0);

  	constant aclk_period: time := 10 ns;

	signal start_divider : STD_LOGIC:= '0';
	
	signal s_axis_dividend_result : STD_LOGIC_VECTOR(23 DOWNTO 0):= (others => '0');

begin
	
	-- aclk process 
    aclk_process :process
    begin
        aclk <= not aclk;
        wait for aclk_period/2;
        aclk <= not aclk;
        wait for aclk_period/2;
    end process;

  -- Insert values for generic parameters !!
	uut : average_filter_divider_24_8
	  PORT MAP (
		aclk => aclk,
		aclken => aclken,
		aresetn => aresetn,
		s_axis_divisor_tvalid => s_axis_divisor_tvalid,
		s_axis_divisor_tlast => s_axis_divisor_tlast,
		s_axis_divisor_tdata => s_axis_divisor_tdata,
		s_axis_dividend_tvalid => s_axis_dividend_tvalid,
		s_axis_dividend_tlast => s_axis_dividend_tlast,
		s_axis_dividend_tdata => s_axis_dividend_tdata,
		m_axis_dout_tvalid => m_axis_dout_tvalid,
		m_axis_dout_tlast => m_axis_dout_tlast,
		m_axis_dout_tdata => m_axis_dout_tdata
	  );

	stimulus: process
	begin
		aresetn <= '0';
		aclken <= '1';
		wait for aclk_period*10;
		aresetn <= '1';
		wait for aclk_period*10;
		start_divider <= '1';
		wait;
	end process;
	
	s_axis_dividend_result <= m_axis_dout_tdata(31 downto 8);

    signal_input_inst: process(aclk, aresetn)  
        variable my_counter  : integer range 0 to 16383 :=0;  
		variable divider_counter  : integer range 0 to 16383 :=0;
    begin 
        if (aresetn = '0') then
			s_axis_divisor_tvalid <= '0';
			s_axis_divisor_tlast <= '0';
			s_axis_divisor_tdata <= std_logic_vector(to_unsigned(2, s_axis_divisor_tdata'length));	
			s_axis_dividend_tvalid <= '0';
			s_axis_dividend_tlast <= '0';
			s_axis_dividend_tdata <= (others => '0'); 
			divider_counter := 0;
			my_counter := 0;														  
        elsif (aclk'event and aclk = '1') then
        	if(start_divider = '1') then
				if(my_counter >= 64) then
					s_axis_divisor_tvalid <= '1';												  
					s_axis_dividend_tdata <= std_logic_vector(unsigned(s_axis_dividend_tdata) + 2);	
					s_axis_dividend_tvalid <= '1';
					if(divider_counter >= 64) then
					    s_axis_divisor_tlast <= '1';
                        s_axis_dividend_tlast <= '1';
						divider_counter := 0;
						my_counter := 0;										  
					else
						divider_counter := divider_counter + 1;											  
					end if;
				else
					s_axis_divisor_tvalid <= '0';
					s_axis_divisor_tvalid <= '0';	
					s_axis_divisor_tlast <= '0';
                    s_axis_dividend_tlast <= '0';
					my_counter := my_counter + 1;											  
				end if;												  													  
			else
				s_axis_divisor_tvalid <= '0';
				s_axis_divisor_tvalid <= '0';	
				s_axis_divisor_tlast <= '0';
                s_axis_dividend_tlast <= '0';									  
			end if;
        end if;
    end process; 
																																					
end;