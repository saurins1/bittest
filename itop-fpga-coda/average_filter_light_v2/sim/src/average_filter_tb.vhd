library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity average_filter_tb is
  	generic (
          REGISTER_W32    : INTEGER := 32;
          REGISTER_W16    : INTEGER := 16;
          REGISTER_W8     : INTEGER := 8;
          TRIGGER_WAIT    : INTEGER := 32;
          MIN_AVG_LEVEL : INTEGER := 2
  	);	
end;

architecture bench of average_filter_tb is

  component average_filter
  	generic (
          REGISTER_W32    : INTEGER := 32;
          REGISTER_W16    : INTEGER := 16;
          REGISTER_W8     : INTEGER := 8;
          TRIGGER_WAIT    : INTEGER := 32;
          MIN_AVG_LEVEL : INTEGER := 2
  	);
  	port (
  	    aclk         : in std_logic;
        aresetn      : in std_logic;
  	    AVG_LEVEL	    : in std_logic_vector(REGISTER_W32-1 downto 0);
  	    DATA_WINDOW	    : in std_logic_vector(REGISTER_W32-1 downto 0);
  	    SAMPLE_FREQ	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  	    DATA_WINDOW_RESULT	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  	    SAMPLE_FREQ_RESULT	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  	    AVG_COUNTER         : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
  	    AVG_WINDOW  	  : out STD_LOGIC;
  		SIGNAL_INPUT_AXIS_TREADY  : out std_logic;
  		SIGNAL_INPUT_AXIS_TDATA	  : in std_logic_vector(REGISTER_W16-1 downto 0);
  		SIGNAL_INPUT_AXIS_TLAST	  : in std_logic;
  		SIGNAL_INPUT_AXIS_TVALID  : in std_logic;
  		AVG_RESULT_AXIS_TVALID    : out std_logic;
  		AVG_RESULT_AXIS_TDATA	  : out std_logic_vector(REGISTER_W16-1 downto 0);
  		AVG_RESULT_AXIS_TLAST	  : out std_logic;
  		AVG_RESULT_AXIS_TREADY    : in std_logic
  	);
  end component;

  signal aclk: std_logic:= '0';
  signal aresetn: std_logic:= '0';

  signal AVG_LEVEL: std_logic_vector(REGISTER_W32-1 downto 0):= (others => '0');
  signal DATA_WINDOW: std_logic_vector(REGISTER_W32-1 downto 0):= (others => '0');
  signal SAMPLE_FREQ: std_logic_vector(REGISTER_W32-1 downto 0):= (others => '0');
  signal DATA_WINDOW_RESULT: std_logic_vector(REGISTER_W32-1 downto 0);
  signal SAMPLE_FREQ_RESULT: std_logic_vector(REGISTER_W32-1 downto 0);
  signal AVG_COUNTER : std_logic_vector(REGISTER_W8-1 downto 0);
  signal AVG_WINDOW : std_logic;
  signal SIGNAL_INPUT_AXIS_TREADY: std_logic;
  signal SIGNAL_INPUT_AXIS_TDATA: std_logic_vector(REGISTER_W16-1 downto 0):= (others => '0');
  signal SIGNAL_INPUT_AXIS_TLAST: std_logic:= '0';
  signal SIGNAL_INPUT_AXIS_TVALID: std_logic:= '0';
  signal AVG_RESULT_AXIS_TVALID: std_logic;
  signal AVG_RESULT_AXIS_TDATA: std_logic_vector(REGISTER_W16-1 downto 0);
  signal AVG_RESULT_AXIS_TLAST: std_logic;
  signal AVG_RESULT_AXIS_TREADY: std_logic:= '0';

  constant aclk_period: time := 10 ns;

  type states_signal_sm is (idle, open_file, load_signal, load_signal_wait, close_file); 
  signal state_signal_sm : states_signal_sm; 

  type states_noise_sm is (idle, open_file, load_noise, close_file); 
  signal state_noise_sm : states_noise_sm; 

  signal start_sm: STD_LOGIC:= '0';

  signal decimation_level: std_logic_vector(REGISTER_W32-1 downto 0):= (others => '0');
  signal load_signal_counter: unsigned(REGISTER_W32-1 downto 0):= (others => '0');

  signal noise_tdata: std_logic_vector(REGISTER_W16-1 downto 0):= (others => '0');
  signal noise_window: std_logic_vector(REGISTER_W32-1 downto 0):= (others => '0');
  signal noise_en: STD_LOGIC:= '0';

begin
	
	-- aclk process 
    aclk_process :process
    begin
        aclk <= not aclk;
        wait for aclk_period/2;
        aclk <= not aclk;
        wait for aclk_period/2;
    end process;

  -- Insert values for generic parameters !!
  uut: average_filter generic map ( REGISTER_W32             => REGISTER_W32,
                                        REGISTER_W16             => REGISTER_W16,
                                        REGISTER_W8              => REGISTER_W8,
                                        TRIGGER_WAIT             => TRIGGER_WAIT,
                                        MIN_AVG_LEVEL          => MIN_AVG_LEVEL )
                             port map ( aclk                     => aclk,
                                        aresetn                  => aresetn,
                                        AVG_LEVEL              => AVG_LEVEL,
                                        DATA_WINDOW              => DATA_WINDOW,
                                        SAMPLE_FREQ              => SAMPLE_FREQ,
                                        DATA_WINDOW_RESULT       => DATA_WINDOW_RESULT,
                                        SAMPLE_FREQ_RESULT       => SAMPLE_FREQ_RESULT,
                                        AVG_COUNTER              => AVG_COUNTER,
                                        AVG_WINDOW               => AVG_WINDOW,
                                        SIGNAL_INPUT_AXIS_TREADY => SIGNAL_INPUT_AXIS_TREADY,
                                        SIGNAL_INPUT_AXIS_TDATA  => SIGNAL_INPUT_AXIS_TDATA,
                                        SIGNAL_INPUT_AXIS_TLAST  => SIGNAL_INPUT_AXIS_TLAST,
                                        SIGNAL_INPUT_AXIS_TVALID => SIGNAL_INPUT_AXIS_TVALID,
                                        AVG_RESULT_AXIS_TVALID => AVG_RESULT_AXIS_TVALID,
                                        AVG_RESULT_AXIS_TDATA  => AVG_RESULT_AXIS_TDATA,
                                        AVG_RESULT_AXIS_TLAST  => AVG_RESULT_AXIS_TLAST,
                                        AVG_RESULT_AXIS_TREADY => AVG_RESULT_AXIS_TREADY );

	stimulus: process
	begin
		aresetn <= '0';
		wait for aclk_period*10;
		aresetn <= '1';
		wait for aclk_period*10;
		start_sm <= '1';
		wait for aclk_period*10;
		start_sm <= '0';
		wait;
	end process;

    signal_input_inst: process(aclk, aresetn)  
        variable my_counter  : integer range 0 to 16383 :=0;   
		variable decimation_counter  : integer range 0 to 16383 :=0; 

        type signalDataFile is file of integer;
        file signal_data_file: signalDataFile;
        variable signal_data: integer;     
        variable signal_data_file_status: file_open_status;
    begin 
        if (aresetn = '0') then
			-- config
			AVG_LEVEL	<= std_logic_vector(to_unsigned(1, AVG_LEVEL'length));
			--AVG_LEVEL	<= std_logic_vector(to_unsigned(2, AVG_LEVEL'length));
			--AVG_LEVEL	<= std_logic_vector(to_unsigned(4, AVG_LEVEL'length));
			--AVG_LEVEL	<= std_logic_vector(to_unsigned(8, AVG_LEVEL'length));                                                                       
			--AVG_LEVEL	<= std_logic_vector(to_unsigned(16, AVG_LEVEL'length));
			--AVG_LEVEL	<= std_logic_vector(to_unsigned(32, AVG_LEVEL'length));
			--AVG_LEVEL	<= std_logic_vector(to_unsigned(64, AVG_LEVEL'length));
			DATA_WINDOW <= std_logic_vector(to_unsigned(2049, DATA_WINDOW'length));																		  
			-- SIGNAL INPUT AXIS
			SIGNAL_INPUT_AXIS_TDATA  <= (others => '0');
			SIGNAL_INPUT_AXIS_TLAST  <= '0';
			SIGNAL_INPUT_AXIS_TVALID <= '0';
			-- AVG RESULT AXIS
			AVG_RESULT_AXIS_TREADY <= '1';																  
			-- decimation
			decimation_level <= std_logic_vector(to_unsigned(1, DATA_WINDOW'length));
			--decimation_level <= std_logic_vector(to_unsigned(4, DATA_WINDOW'length));		
			load_signal_counter <= (others => '0');
			--noise_en <= '0';	
			noise_en <= '1';															  
			-- state																		  	
            state_signal_sm <= idle;  
        elsif (aclk'event and aclk = '1') then
            case state_signal_sm is  
                when idle =>         
                    if(start_sm = '1') then
                        my_counter := 0;
                        state_signal_sm <= open_file;        
                    end if;
                when open_file => 
                    file_open(signal_data_file_status, signal_data_file, "signal_input.dat", read_mode);	
                    load_signal_counter <= (others => '0');	     
                    SIGNAL_INPUT_AXIS_TDATA  <= std_logic_vector(to_signed(-1024, SIGNAL_INPUT_AXIS_TDATA'length));		                                                                	  
                    state_signal_sm <= load_signal;  
                when load_signal =>
                    if(SIGNAL_INPUT_AXIS_TREADY = '1') then
                		if(my_counter <= to_integer(unsigned(DATA_WINDOW))-1) then
--                     		if not endfile(signal_data_file) then
--                        		read (signal_data_file, signal_data);
--                                SIGNAL_INPUT_AXIS_TDATA<=std_logic_vector(to_signed(signal_data, SIGNAL_INPUT_AXIS_TDATA'length)
--																		  + signed(noise_tdata));
--                            end if;
                            SIGNAL_INPUT_AXIS_TDATA <= std_logic_vector(signed(SIGNAL_INPUT_AXIS_TDATA)+1);
                            if not endfile(signal_data_file) then
                                read (signal_data_file, signal_data);
                            end if;
							SIGNAL_INPUT_AXIS_TVALID <= '1';		
							if(my_counter = to_integer(unsigned(DATA_WINDOW))-1) then
								SIGNAL_INPUT_AXIS_TLAST <= '1';
							end if;
							if(unsigned(decimation_level) > 1) then	
								decimation_counter := 0;	
								state_signal_sm <= load_signal_wait;															
							end if;
							load_signal_counter <= load_signal_counter + 1;
							my_counter := my_counter + 1; 
						else
							SIGNAL_INPUT_AXIS_TVALID <= '0';
							SIGNAL_INPUT_AXIS_TLAST <= '0';													
							my_counter  := 0;
							state_signal_sm    <= close_file;	
                        end if;      
                    end if;																		
				when load_signal_wait =>
					SIGNAL_INPUT_AXIS_TVALID <= '0';
					SIGNAL_INPUT_AXIS_TLAST <= '0';																
					if(decimation_counter >= unsigned(decimation_level)-2) then
						state_signal_sm <= load_signal; 																
					else
						decimation_counter := decimation_counter + 1;																
					end if;																																
                when close_file =>
                    if(my_counter >= 32) then
                        my_counter  := 0;
                        file_close(signal_data_file);
                        state_signal_sm <= open_file;
                    else
                        my_counter := my_counter + 1;
                    end if;   
                when others =>
                    null;                   
            end case;                                                                                            
        end if;
    end process; 
																					
    noise_input: process(aclk, aresetn)  
        variable my_counter  : integer range 0 to 16383 :=0;   

        type noiseDataFile is file of integer;
        file noise_data_file: noiseDataFile;
        variable noise_data: integer;     
        variable noise_data_file_status: file_open_status;
    begin 
        if (aresetn = '0') then																  
			-- noise
			noise_tdata <= std_logic_vector(to_unsigned(0, noise_tdata'length));		
			noise_window <= std_logic_vector(to_unsigned(120000, noise_window'length));			 										   
			-- state																		  	
            state_noise_sm <= idle;  
        elsif (aclk'event and aclk = '1') then
            case state_noise_sm is  
                when idle =>         
                    if(start_sm = '1' and noise_en = '1') then
                        my_counter := 0;
                        state_noise_sm <= open_file;        
                    end if;
                when open_file => 
                    file_open(noise_data_file_status, noise_data_file, "noise_input.dat", read_mode);																  
                    state_noise_sm <= load_noise;                                                                         
                when load_noise =>
                	if(my_counter <= to_integer(unsigned(noise_window))-1) then
                 		if not endfile(noise_data_file) then
                       		read (noise_data_file, noise_data);
                            noise_tdata<=std_logic_vector(to_signed(noise_data, noise_tdata'length));
                        end if;
                        if not endfile(noise_data_file) then
                            read (noise_data_file, noise_data);
                        end if;
                        my_counter := my_counter + 1; 
					else													
						my_counter  := 0;
						state_noise_sm    <= close_file;	    
                    end if;																																																	
                when close_file =>
                    file_close(noise_data_file);
                    state_noise_sm <= open_file;   
                when others =>
                    null;                   
            end case;                                                                                            
        end if;
    end process; 																		

end;