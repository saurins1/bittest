----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 01/17/2017 10:07:23 AM
-- Design Name: 
-- Module Name: dio - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity dio is
	port (
	    -- Sync
	    aclk 	: in STD_LOGIC;
        aresetn	: in STD_LOGIC;
        
--		A  : in STD_LOGIC;
--        B  : in STD_LOGIC;
--        S  : in STD_LOGIC;
--        O  : out STD_LOGIC     
        
		A  : in STD_LOGIC_VECTOR(5 downto 0);
        B  : in STD_LOGIC_VECTOR(5 downto 0);
        S  : in STD_LOGIC;
        O  : out STD_LOGIC_VECTOR(5 downto 0)       
--        O  : out STD_LOGIC_VECTOR(11 downto 0)  
          
	);
end dio;

architecture arch_imp of dio is
	
	--signal O_i STD_LOGIC_VECTOR(5 downto 0);
	       
begin
			
--	O <= A when S = '0' else B;	

    --O <= std_logic_vector(unsigned(A) + unsigned(B));
    --O <= std_logic_vector(unsigned(A) * unsigned(B));

	my_process: process(aclk)
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                --O <= '0';
                O <= (others => '0');
            else
                if(S = '0') then
                    O <= A; 
                else
                    O <= B;
                end if;     
--                O <= std_logic_vector(unsigned(A) * unsigned(B));                                                 
            end if;
        end if;
    end process;
                     																				   					     					      									 	   			                 
end arch_imp;
