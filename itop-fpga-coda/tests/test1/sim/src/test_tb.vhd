library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity dio_tb is
  	generic (
  		REGISTER_W8     : INTEGER := 8; 
  		FILTER_LEVEL    : INTEGER := 4
  	);
end;

architecture bench of dio_tb is

  component dio
  	generic (
  		REGISTER_W8     : INTEGER := 8; 
  		FILTER_LEVEL    : INTEGER := 4
  	);
  	port (
  	    aclk 	: in STD_LOGIC;
        aresetn	: in STD_LOGIC;
  		IO_INPUTS_I 	: in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);	
  		IO_INPUTS_O 	: out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
  		IO_OUTPUT_I 	: in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);	
  		IO_OUTPUT_O 	: out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0)            
  	);
  end component;

  signal aclk: std_logic:= '0';
  signal aresetn: std_logic:= '0';

  signal IO_INPUTS_I: STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0):= (others => '0'); 
  signal IO_INPUTS_O: STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);

  signal IO_OUTPUT_I: STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0):= (others => '0'); 
  signal IO_OUTPUT_O: STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);

  constant aclk_period: time := 10 ns;

begin
	
    update_IO_inst: process(aclk, aresetn)
		variable my_counter  : integer range 0 to 15383 :=0; 
    begin 
		if (rising_edge(aclk)) then
            if (aresetn = '0') then
   				IO_INPUTS_I 	<= (others => '0');
				IO_OUTPUT_I		<= (others => '0');
            else
				if(my_counter >= 65) then
					IO_INPUTS_I <= not IO_INPUTS_I;
					IO_OUTPUT_I <= not IO_OUTPUT_I;
					my_counter := 0;
				else
					my_counter := my_counter + 1;	
				end if;
            end if;
        end if;
    end process;
	
	stimulus: process
	begin
		aresetn <= '0';
		wait for aclk_period*10;
		aresetn <= '1';
		wait;
	end process;
	
	-- aclk process 
    aclk_process :process
    begin
        aclk <= not aclk;
        wait for aclk_period/2;
        aclk <= not aclk;
        wait for aclk_period/2;
    end process;	

  -- Insert values for generic parameters !!
  uut: dio generic map ( REGISTER_W8  => REGISTER_W8,
                         FILTER_LEVEL => FILTER_LEVEL )
              port map ( aclk         => aclk,
                         aresetn      => aresetn,
                         IO_INPUTS_I  => IO_INPUTS_I,
                         IO_INPUTS_O  => IO_INPUTS_O,
                         IO_OUTPUT_I  => IO_OUTPUT_I,
                         IO_OUTPUT_O  => IO_OUTPUT_O );


end;