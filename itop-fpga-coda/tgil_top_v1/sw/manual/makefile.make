WORKDIR = `pwd`

# ADD CROSS COMPILE
CC= /opt/pkg/petalinux-v2016.1-final/tools/linux-i386/gcc-arm-linux-gnueabi/bin/arm-linux-gnueabihf-gcc 
CXX = /opt/pkg/petalinux-v2016.1-final/tools/linux-i386/gcc-arm-linux-gnueabi/bin/arm-linux-gnueabihf-g++
AR = /opt/pkg/petalinux-v2016.1-final/tools/linux-i386/gcc-arm-linux-gnueabi/bin/arm-linux-gnueabihf-gcc-ar
LD = /opt/pkg/petalinux-v2016.1-final/tools/linux-i386/gcc-arm-linux-gnueabi/bin/arm-linux-gnueabihf-g++
WINDRES = windres

INC = 
CFLAGS = -Wall
RESINC = 
LIBDIR = 
LIB = 
LDFLAGS = 

INC_RELEASE = $(INC) -I../arm_service_test/src
CFLAGS_RELEASE = $(CFLAGS) -O2 -fPIC -std=c++11 -DHAVE_PTHREAD -DZMQ_USE_EPOLL
RESINC_RELEASE = $(RESINC)
RCFLAGS_RELEASE = $(RCFLAGS)
LIBDIR_RELEASE = $(LIBDIR)
LIB_RELEASE = $(LIB)
LDFLAGS_RELEASE = $(LDFLAGS) -s -pthread -O2 -fPIC
OBJDIR_RELEASE = obj/Release
DEP_RELEASE = 
OUT_RELEASE = bin/Release/arm_service_test

OBJ_RELEASE = \
$(OBJDIR_RELEASE)/arm_service_test/itscontroller.o \
$(OBJDIR_RELEASE)/arm_service_test/dmacontroller.o \
$(OBJDIR_RELEASE)/arm_service_test/fpgacontroller.o \
$(OBJDIR_RELEASE)/arm_service_test/fpgaboard.o \
$(OBJDIR_RELEASE)/arm_service_test/main.o

all: release

clean: clean_release clean_objets
	
before_release: 
	test -d bin/Release || mkdir -p bin/Release
	test -d $(OBJDIR_RELEASE) || mkdir -p $(OBJDIR_RELEASE)
	test -d $(OBJDIR_RELEASE)/arm_service_test || mkdir -p $(OBJDIR_RELEASE)/arm_service_test

after_release: 

release: before_release out_release after_release clean_objets

out_release: before_release $(OBJ_RELEASE) $(DEP_RELEASE)
	$(LD) $(LIBDIR_RELEASE) -o $(OUT_RELEASE) $(OBJ_RELEASE)  $(LDFLAGS_RELEASE) $(LIB_RELEASE)

$(OBJDIR_RELEASE)/arm_service_test/itscontroller.o: ../arm_service_test/src/itscontroller.cpp
	$(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c ../arm_service_test/src/itscontroller.cpp -o $(OBJDIR_RELEASE)/arm_service_test/itscontroller.o
	
$(OBJDIR_RELEASE)/arm_service_test/dmacontroller.o: ../arm_service_test/src/dmacontroller.cpp
	$(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c ../arm_service_test/src/dmacontroller.cpp -o $(OBJDIR_RELEASE)/arm_service_test/dmacontroller.o
	
$(OBJDIR_RELEASE)/arm_service_test/fpgacontroller.o: ../arm_service_test/src/fpgacontroller.cpp
	$(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c ../arm_service_test/src/fpgacontroller.cpp -o $(OBJDIR_RELEASE)/arm_service_test/fpgacontroller.o
	
$(OBJDIR_RELEASE)/arm_service_test/fpgaboard.o: ../arm_service_test/src/fpgaboard.cpp
	$(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c ../arm_service_test/src/fpgaboard.cpp -o $(OBJDIR_RELEASE)/arm_service_test/fpgaboard.o
	
$(OBJDIR_RELEASE)/arm_service_test/main.o: ../arm_service_test/src/main.cc
	$(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c ../arm_service_test/src/main.cc -o $(OBJDIR_RELEASE)/arm_service_test/main.o
		
clean_release: 
	rm -f $(OBJ_RELEASE) $(OUT_RELEASE)
	rm -rf bin/Release
	rm -rf $(OBJDIR_RELEASE)

clean_objets:
	rm -rf $(OBJDIR_RELEASE)

.PHONY: before_release after_release clean_release
