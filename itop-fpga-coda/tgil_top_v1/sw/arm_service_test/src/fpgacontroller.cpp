#include "fpgacontroller.h"


// Constructor
FpgaController::FpgaController(){
    this->user_buffer_rx = new uint32_t[ITS_MEM_LENGTH+1];
    this->user_buffer_tx = new uint32_t[ITS_MEM_LENGTH+1];
    this->itsController = new ItsController();
    this->registers_len = ITS_MEM_LENGTH;
}

// Destructor
FpgaController::~FpgaController(){
    delete [] this->user_buffer_rx;
    delete [] this->user_buffer_tx;
    delete this->itsController;
}

/* UPLOAD OPERATIONS */

// FPGA set bits 11-31 the DMA lenght transfer
bool FpgaController::fpgaSetUploadDMALen(uint32_t len){
    //std::cout << "fpgaController, fpgaSetUploadDMALen" << std::endl;
	// 21 bits max = 2097151
	// 524288 bytes fpga limitation
	if(len > 524288)
		return false;
	// Read control register
    uint32_t control_reg_rx = this->itsController->readRegister(this->user_buffer_rx, DMA_UPLOAD_CONTROL_REGISTER);  
    //std::cout << "fpgaController, fpgaSetUploadDMALen control_reg_rx=0x" << std::hex << control_reg_rx << std::endl;
    //std::cout << "fpgaController, fpgaSetUploadDMALen control_reg_rx=" << control_reg_rx << std::endl;
    // shift 11 bits the len value to create mask
    //std::cout << "fpgaController, fpgaSetUploadDMALen len=" << len << std::endl;
    len = len << DMA_UPLOAD_CONTROL_DATA_SIZE_BIT_INIT;
    //std::cout << "fpgaController, fpgaSetUploadDMALen len=0x" << std::hex << len << std::endl;
    control_reg_rx &= 0x000007FF;
    //std::cout << "fpgaController, fpgaSetUploadDMALen control_reg_rx=0x" << std::hex << control_reg_rx << std::endl;
    control_reg_rx |=len;
    //std::cout << "fpgaController, fpgaSetUploadDMALen control_reg_rx=0x" << std::hex << control_reg_rx << std::endl;
    // Apply mask
    this->user_buffer_tx[1] = control_reg_rx;
    this->itsController->writeRegister(this->user_buffer_tx, DMA_UPLOAD_CONTROL_REGISTER);   
    //std::cout << "fpgaController, fpgaSetUploadDMALen OK" << std::endl;
    return true;
}

// FPGA set bits 11-31 the DMA lenght transfer
bool FpgaController::fpgaClearUploadDMALen(){
	//std::cout << "fpgaController, fpgaClearUploadDMALen" << std::endl;
	// Read control register
    uint32_t control_reg_rx = this->itsController->readRegister(this->user_buffer_rx, DMA_UPLOAD_CONTROL_REGISTER);
    //std::cout << "fpgaController, fpgaClearUploadDMALen control_reg_rx=0x" << std::hex << control_reg_rx << std::endl;   
    control_reg_rx &= 0x000007FF;
    //std::cout << "fpgaController, fpgaClearUploadDMALen control_reg_rx=0x" << std::hex << control_reg_rx << std::endl; 
    this->user_buffer_tx[1] = control_reg_rx;
    this->itsController->writeRegister(this->user_buffer_tx, DMA_UPLOAD_CONTROL_REGISTER); 
    return true;  
}

// Read DMA Length from Status register
uint32_t FpgaController::fpgaReadUploadDmaLen(){
    //std::cout << "fpgaController, fpgaReadUploadDmaLen" << std::endl;
	// Read control register
    uint32_t control_reg_rx = this->itsController->readRegister(this->user_buffer_rx, DMA_UPLOAD_STATUS_REGISTER); 
    //std::cout << "fpgaController, fpgaReadUploadDmaLen control_reg_rx=0x" << std::hex << control_reg_rx << std::endl;
    // shift 11 bits the len value to create mask
    uint32_t len = control_reg_rx >> DMA_UPLOAD_STATUS_DATA_SIZE_BIT_INIT;
    //std::cout << "fpgaController, fpgaReadUploadDmaLen len=0x" << std::hex << len << std::endl;
    //std::cout << "fpgaController, fpgaReadUploadDmaLen len=" << len << std::endl;
    len &= 0x001FFFFF;
    //std::cout << "fpgaController, fpgaReadUploadDmaLen len=" << len << std::endl;
    //std::cout << "fpgaController, fpgaReadUploadDmaLen OK" << std::endl;
    return len;
}

// FPGA Start ascan upload request
bool FpgaController::fpgaStartUploadAscan(){
	//std::cout << "fpgaController, fpgaStartUploadAscan" << std::endl;
    return this->bitEnableHS(DMA_UPLOAD_CONTROL_REGISTER, DMA_UPLOAD_CONTROL_ASCAN_UPLOAD_REQUEST_BIT, DMA_UPLOAD_STATUS_REGISTER, DMA_UPLOAD_STATUS_ASCAN_UPLOAD_REQUEST_BIT);
}

// FPGA Stop ascan upload request
bool FpgaController::fpgaStopUploadAscan(){
	//std::cout << "fpgaController, fpgaStopUploadAscan" << std::endl;
    return this->bitDisableHS(DMA_UPLOAD_CONTROL_REGISTER, DMA_UPLOAD_CONTROL_ASCAN_UPLOAD_REQUEST_BIT, DMA_UPLOAD_STATUS_REGISTER, DMA_UPLOAD_STATUS_ASCAN_UPLOAD_REQUEST_BIT);
}

// FPGA Start cscan upload request
bool FpgaController::fpgaStartUploadCscan(){
	//std::cout << "fpgaController, fpgaStartUploadCscan" << std::endl;
    return this->bitEnableHS(DMA_UPLOAD_CONTROL_REGISTER, DMA_UPLOAD_CONTROL_CSCAN_UPLOAD_REQUEST_BIT, DMA_UPLOAD_STATUS_REGISTER, DMA_UPLOAD_STATUS_CSCAN_UPLOAD_REQUEST_BIT);
}

// FPGA Stop cscan upload request
bool FpgaController::fpgaStopUploadCscan(){
	//std::cout << "fpgaController, fpgaStopUploadCscan" << std::endl;
    return this->bitDisableHS(DMA_UPLOAD_CONTROL_REGISTER, DMA_UPLOAD_CONTROL_CSCAN_UPLOAD_REQUEST_BIT, DMA_UPLOAD_STATUS_REGISTER, DMA_UPLOAD_STATUS_CSCAN_UPLOAD_REQUEST_BIT);
}

// FPGA Start read config
bool FpgaController::fpgaStartUploadConfig(){
	//std::cout << "fpgaController, fpgaStartUploadConfig" << std::endl;
    return this->bitEnableHS(DMA_UPLOAD_CONTROL_REGISTER, DMA_UPLOAD_CONTROL_CONFIG_UPLOAD_REQUEST_BIT, DMA_UPLOAD_STATUS_REGISTER, DMA_UPLOAD_STATUS_CONFIG_UPLOAD_REQUEST_BIT);
}

// FPGA stop read config
bool FpgaController::fpgaStopUploadConfig(){
	//std::cout << "fpgaController, fpgaStopUploadConfig" << std::endl;
    return this->bitDisableHS(DMA_UPLOAD_CONTROL_REGISTER, DMA_UPLOAD_CONTROL_CONFIG_UPLOAD_REQUEST_BIT, DMA_UPLOAD_STATUS_REGISTER, DMA_UPLOAD_STATUS_CONFIG_UPLOAD_REQUEST_BIT);
}

// FPGA Start read capabilities
bool FpgaController::fpgaStartUploadCapabilities(){
	//std::cout << "fpgaController, fpgaStartUploadCapabilities" << std::endl;
    return this->bitEnableHS(DMA_UPLOAD_CONTROL_REGISTER, DMA_UPLOAD_CONTROL_CAPABILITY_UPLOAD_REQUEST_BIT, DMA_UPLOAD_STATUS_REGISTER, DMA_UPLOAD_STATUS_CAPABILITY_UPLOAD_REQUEST_BIT);
}

// FPGA stop read capabilities
bool FpgaController::fpgaStopUploadCapabilities(){
	//std::cout << "fpgaController, fpgaStopUploadCapabilities" << std::endl;
    return this->bitDisableHS(DMA_UPLOAD_CONTROL_REGISTER, DMA_UPLOAD_CONTROL_CAPABILITY_UPLOAD_REQUEST_BIT, DMA_UPLOAD_STATUS_REGISTER, DMA_UPLOAD_STATUS_CAPABILITY_UPLOAD_REQUEST_BIT);
}

// FPGA set ready read config
bool FpgaController::fpgaSetUploadConfigReady(){
	//std::cout << "fpgaController, fpgaSetUploadConfigReady" << std::endl;
    return this->bitEnableHS(DMA_UPLOAD_CONTROL_REGISTER, DMA_UPLOAD_CONTROL_CONFIG_READY, DMA_UPLOAD_STATUS_REGISTER, DMA_UPLOAD_STATUS_CONFIG_READY);
}

// FPGA clear ready read config
bool FpgaController::fpgaClearUploadConfigReady(){
	//std::cout << "fpgaController, fpgaClearUploadConfigReady" << std::endl;
    return this->bitDisableHS(DMA_UPLOAD_CONTROL_REGISTER, DMA_UPLOAD_CONTROL_CONFIG_READY, DMA_UPLOAD_STATUS_REGISTER, DMA_UPLOAD_STATUS_CONFIG_READY);
}

// FPGA set ready read capabilities
bool FpgaController::fpgaSetUploadCapabilitiesReady(){
    //std::cout << "fpgaController, fpgaSetUploadCapabilitiesReady" << std::endl;
    return this->bitEnableHS(DMA_UPLOAD_CONTROL_REGISTER, DMA_UPLOAD_CONTROL_CAPABILITY_READY, DMA_UPLOAD_STATUS_REGISTER, DMA_UPLOAD_STATUS_CAPABILITY_READY);
}

// FPGA clear ready read capabilities
bool FpgaController::fpgaClearUploadCapabilitiesReady(){
    //std::cout << "fpgaController, fpgaClearUploadCapabilitiesReady" << std::endl;
    return this->bitDisableHS(DMA_UPLOAD_CONTROL_REGISTER, DMA_UPLOAD_CONTROL_CAPABILITY_READY, DMA_UPLOAD_STATUS_REGISTER, DMA_UPLOAD_STATUS_CAPABILITY_READY);
}

// FPGA Set Ascan availability
bool FpgaController::fpgaSetAscanAvailability(){
	//std::cout << "fpgaController, fpgaSetAscanAvailability" << std::endl;
    return this->bitEnable(DMA_UPLOAD_CONTROL_REGISTER, DMA_UPLOAD_CONTROL_ASCAN_AVAILABILITY_BIT);
}

// FPGA Set Cscan availability
bool FpgaController::fpgaSetCscanAvailability(){
	//std::cout << "fpgaController, fpgaSetCscanAvailability" << std::endl;
    return this->bitEnable(DMA_UPLOAD_CONTROL_REGISTER, DMA_UPLOAD_CONTROL_CSCAN_AVAILABILITY_BIT);
}

// FPGA Clear Ascan availability
bool FpgaController::fpgaClearAscanAvailability(){
	//std::cout << "fpgaController, fpgaClearAscanAvailability" << std::endl;
    return this->bitDisable(DMA_UPLOAD_CONTROL_REGISTER, DMA_UPLOAD_CONTROL_ASCAN_AVAILABILITY_BIT);
}

// FPGA Clear Cscan availability
bool FpgaController::fpgaClearCscanAvailability(){
	//std::cout << "fpgaController, fpgaClearCscanAvailability" << std::endl;
    return this->bitDisable(DMA_UPLOAD_CONTROL_REGISTER, DMA_UPLOAD_CONTROL_CSCAN_AVAILABILITY_BIT);
}

// FPGA READ Ascan availability
bool FpgaController::fpgaReadStatusAscanAvailability(){
	//std::cout << "fpgaController, fpgaReadStatusAscanAvailability" << std::endl;
    // Read control register
    uint32_t reg_rx = this->itsController->readRegister(this->user_buffer_rx, DMA_UPLOAD_STATUS_REGISTER); 
    //std::cout << "fpgaController, fpgaReadStatusAscanAvailability reg_rx=0x" << std::hex << reg_rx << std::endl; 
    reg_rx &= (1<<DMA_UPLOAD_STATUS_ASCAN_AVAILABILITY_BIT);
    //std::cout << "fpgaController, fpgaReadStatusAscanAvailability reg_rx=0x" << std::hex << reg_rx << std::endl;
    if(reg_rx)
		return true;
	return false;
}

// FPGA rEAD Cscan availability
bool FpgaController::fpgaReadStatusCscanAvailability(){
	//std::cout << "fpgaController, fpgaReadStatusCscanAvailability" << std::endl;
    // Read control register
    uint32_t reg_rx = this->itsController->readRegister(this->user_buffer_rx, DMA_UPLOAD_STATUS_REGISTER); 
    //std::cout << "fpgaController, fpgaReadStatusCscanAvailability reg_rx=0x" << std::hex << reg_rx << std::endl; 
    reg_rx &= (1<<DMA_UPLOAD_STATUS_CSCAN_AVAILABILITY_BIT);
    //std::cout << "fpgaController, fpgaReadStatusCscanAvailability reg_rx=0x" << std::hex << reg_rx << std::endl;
    if(reg_rx)
		return true;
	return false;
}

// FPGA read scan ready
bool FpgaController::scanReady(){
    //std::cout << "fpgaController, scanReady" << std::endl;
	// Read control register
    uint32_t reg_rx = this->itsController->readRegister(this->user_buffer_rx, DMA_UPLOAD_STATUS_REGISTER);  
    //std::cout << "fpgaController, scanReady reg_rx=0x" << std::hex << reg_rx << std::endl;
    reg_rx &= (1<<DMA_UPLOAD_STATUS_SCAN_READY);
    //std::cout << "fpgaController, scanReady reg_rx=0x" << std::hex << reg_rx << std::endl;
    if(reg_rx)
		return true;
	return false;	
}

/* DOWNLOAD OPERATIONS */

// FPGA set bits 11-31 the DMA lenght transfer
bool FpgaController::fpgaSetDownloadDMALen(uint32_t len){
	//std::cout << "fpgaController, fpgaSetDownloadDMALen" << std::endl;
	// 21 bits max = 2097151
	// 524288 bytes fpga limitation
	if(len > 524288)
		return false;
	// Read control register
    uint32_t control_reg_rx = this->itsController->readRegister(this->user_buffer_rx, DMA_DOWNLOAD_CONTROL_REGISTER); 
    //std::cout << "fpgaController, fpgaSetDownloadDMALen control_reg_rx=" << control_reg_rx << std::endl;   
    // shift 11 bits the len value to create mask
    //std::cout << "fpgaController, fpgaSetDownloadDMALen len=" << len << std::endl;  
    len = len << DMA_DOWNLOAD_CONTROL_DATA_SIZE_BIT_INIT;
    //std::cout << "fpgaController, fpgaSetDownloadDMALen len=" << len << std::endl;  
    control_reg_rx &= 0x000007FF;
    //std::cout << "fpgaController, fpgaSetDownloadDMALen control_reg_rx=" << control_reg_rx << std::endl;
    control_reg_rx |=len;
    //std::cout << "fpgaController, fpgaSetDownloadDMALen control_reg_rx=" << control_reg_rx << std::endl;
    // Apply mask
    this->user_buffer_tx[1] = control_reg_rx;
    this->itsController->writeRegister(this->user_buffer_tx, DMA_DOWNLOAD_CONTROL_REGISTER);   
    return true;
}

// FPGA set bits 11-31 the DMA lenght transfer
bool FpgaController::fpgaClearDownloadDMALen(){
	//std::cout << "fpgaController, fpgaClearDownloadDMALen" << std::endl;
	// Read control register
    uint32_t control_reg_rx = this->itsController->readRegister(this->user_buffer_rx, DMA_DOWNLOAD_CONTROL_REGISTER);  
    //std::cout << "fpgaController, fpgaClearDownloadDMALen control_reg_rx=" << control_reg_rx << std::endl; 
    control_reg_rx &= 0x000007FF;
    //std::cout << "fpgaController, fpgaClearDownloadDMALen control_reg_rx=" << control_reg_rx << std::endl; 
    this->user_buffer_tx[1] = control_reg_rx;
    this->itsController->writeRegister(this->user_buffer_tx, DMA_DOWNLOAD_CONTROL_REGISTER); 
    return true;  
}

// Read DMA Length register
uint32_t FpgaController::fpgaReadDownloadDmaLen(){
	//std::cout << "fpgaController, fpgaReadDownloadDmaLen" << std::endl;
	// Read control register
    uint32_t control_reg_rx = this->itsController->readRegister(this->user_buffer_rx, DMA_DOWNLOAD_STATUS_REGISTER);  
    //std::cout << "fpgaController, fpgaReadDownloadDmaLen control_reg_rx=" << control_reg_rx << std::endl; 
    // shift 11 bits the len value to create mask
    uint32_t len = control_reg_rx >> DMA_UPLOAD_STATUS_DATA_SIZE_BIT_INIT;
    //std::cout << "fpgaController, fpgaReadDownloadDmaLen len=" << len << std::endl; 
    len &= 0x001FFFFF;
    //std::cout << "fpgaController, fpgaReadDownloadDmaLen len=" << len << std::endl; 
    return len;
}

// FPGA Start config download request
bool FpgaController::fpgaStartDownloadConfig(){
	//std::cout << "fpgaController, fpgaStartDownloadConfig" << std::endl;
    return this->bitEnableHS(DMA_DOWNLOAD_CONTROL_REGISTER, DMA_DOWNLOAD_CONTROL_CONFIG_DOWNLOAD_REQUEST_BIT, DMA_DOWNLOAD_STATUS_REGISTER, DMA_DOWNLOAD_STATUS_CONFIG_DOWNLOAD_REQUEST_BIT);
}

// FPGA Stop config download request
bool FpgaController::fpgaStopDownloadConfig(){
	//std::cout << "fpgaController, fpgaStopDownloadConfig" << std::endl;
    return this->bitDisableHS(DMA_DOWNLOAD_CONTROL_REGISTER, DMA_DOWNLOAD_CONTROL_CONFIG_DOWNLOAD_REQUEST_BIT, DMA_DOWNLOAD_STATUS_REGISTER, DMA_DOWNLOAD_STATUS_CONFIG_DOWNLOAD_REQUEST_BIT);
}

// FPGA Start config download request
bool FpgaController::fpgaStartDownloadAscanReplay(){
	//std::cout << "fpgaController, fpgaStartDownloadAscanReplay" << std::endl;
    return this->bitEnableHS(DMA_DOWNLOAD_CONTROL_REGISTER, DMA_DOWNLOAD_CONTROL_ASCAN_REPLAY_DOWNLOAD_REQUEST_BIT, DMA_DOWNLOAD_STATUS_REGISTER, DMA_DOWNLOAD_STATUS_ASCAN_REPLAY_DOWNLOAD_REQUEST_BIT);
}

// FPGA Stop config download request
bool FpgaController::fpgaStopDownloadAscanReplay(){
	//std::cout << "fpgaController, fpgaStopDownloadAscanReplay" << std::endl;
    return this->bitDisableHS(DMA_DOWNLOAD_CONTROL_REGISTER, DMA_DOWNLOAD_CONTROL_ASCAN_REPLAY_DOWNLOAD_REQUEST_BIT, DMA_DOWNLOAD_STATUS_REGISTER, DMA_DOWNLOAD_STATUS_ASCAN_REPLAY_DOWNLOAD_REQUEST_BIT);
}

// FPGA Start config download request
bool FpgaController::fpgaStartDownloadDacCurve(){
	//std::cout << "fpgaController, fpgaStartDownloadDacCurve" << std::endl;
    return this->bitEnableHS(DMA_DOWNLOAD_CONTROL_REGISTER, DMA_DOWNLOAD_CONTROL_DAC_CURVE_DOWNLOAD_REQUEST_BIT, DMA_DOWNLOAD_STATUS_REGISTER, DMA_DOWNLOAD_STATUS_DAC_CURVE_DOWNLOAD_REQUEST_BIT);
}

// FPGA Stop config download request
bool FpgaController::fpgaStopDownloadDacCurve(){
	//std::cout << "fpgaController, fpgaStopDownloadDacCurve" << std::endl;
    return this->bitDisableHS(DMA_DOWNLOAD_CONTROL_REGISTER, DMA_DOWNLOAD_CONTROL_DAC_CURVE_DOWNLOAD_REQUEST_BIT, DMA_DOWNLOAD_STATUS_REGISTER, DMA_DOWNLOAD_STATUS_DAC_CURVE_DOWNLOAD_REQUEST_BIT);
}

/* CONTROL/STATUS OPERATIONS */

// Start Inspection
bool FpgaController::fpgaStartInspection(){
    std::cout << "fpgaController::fpgaStartInspection" << std::endl;
    bool rc = this->bitEnableHS(FPGA_CONTROL_START_INSPECTION_REGISTER, FPGA_CONTROL_START_INSPECTION_BIT, FPGA_STATUS_START_INSPECTION_REGISTER, FPGA_STATUS_START_INSPECTION_BIT);
    if(rc){
        std::cout << "fpgaController, fpgaStartInspection OK" << std::endl;
    }else{
        std::cout << "fpgaController, fpgaStartInspection NOK" << std::endl;
    }
    return rc;
}

// Stop Inspection
bool FpgaController::fpgaStopInspection(){
    std::cout << "fpgaController::fpgaStopInspection" << std::endl;
    bool rc = this->bitDisableHS(FPGA_CONTROL_START_INSPECTION_REGISTER, FPGA_CONTROL_START_INSPECTION_BIT, FPGA_STATUS_START_INSPECTION_REGISTER, FPGA_STATUS_START_INSPECTION_BIT);
    if(rc){
        std::cout << "fpgaController, fpgaStopInspection OK" << std::endl;
    }else{
        std::cout << "fpgaController, fpgaStopInspection NOK" << std::endl;
    }
    return rc;
}

// Set SCAN mode
bool FpgaController::fpgaSetScanMode(uint32_t mode){
    //std::cout << "fpgaController, fpgaSetScanMode mode=" << mode << std::endl;
	if(mode > 15)
		return false;
    this->user_buffer_tx[1] = mode; 
    this->itsController->writeRegister(this->user_buffer_tx, FPGA_CONTROL_SCAN_MODE_REGISTER);  
    //uint32_t control_scan_mode = this->fpgaReadRegisterValue(FPGA_CONTROL_SCAN_MODE_REGISTER);
    //std::cout << "fpgaController, fpgaSetScanMode read control_scan_mode=" << control_scan_mode << std::endl;
    uint32_t status_scan_mode = this->fpgaGetScanMode();
    //std::cout << "fpgaController, fpgaSetScanMode read status_scan_mode=" << status_scan_mode << std::endl;
    if(status_scan_mode != mode){
        //std::cout << "fpgaController, fpgaSetScanMode NOK" << std::endl;
		return false;
	} 
    //std::cout << "fpgaController, fpgaSetScanMode OK" << std::endl;
    return true;
}

// FPGA get SCAN mode
uint32_t FpgaController::fpgaGetScanMode(){
	//std::cout << "fpgaController, fpgaGetScanMode" << std::endl;
	uint32_t scan_mode = this->fpgaReadRegisterValue(FPGA_STATUS_SCAN_MODE_REGISTER);
	//std::cout << "fpgaController, fpgaGetScanMode scan_mode=" << scan_mode << std::endl;
	return scan_mode;
}

// FPGA set CSCAN mode
bool FpgaController::fpgaSetCscanMode(uint32_t mode){	
    //std::cout << "fpgaController, fpgaSetCscanMode mode=" << mode << std::endl;
	if(mode > 15)
		return false;
    this->user_buffer_tx[1] = mode; 
    this->itsController->writeRegister(this->user_buffer_tx, FPGA_CONTROL_CSCAN_MODE_REGISTER);  
    //uint32_t control_cscan_mode = this->fpgaReadRegisterValue(FPGA_CONTROL_CSCAN_MODE_REGISTER);
    //std::cout << "fpgaController, fpgaSetCscanMode read control_cscan_mode=" << control_cscan_mode << std::endl;
    uint32_t status_cscan_mode = this->fpgaGetCscanMode();
    //std::cout << "fpgaController, fpgaSetCscanMode read status_cscan_mode=" << status_cscan_mode << std::endl;
    if(status_cscan_mode != mode){
		return false;
	} 
    return true;
}

// FPGA get CSCAN mode
uint32_t FpgaController::fpgaGetCscanMode(){
    //std::cout << "fpgaController, fpgaGetCscanMode" << std::endl;
	uint32_t cscan_mode = this->fpgaReadRegisterValue(FPGA_STATUS_CSCAN_MODE_REGISTER);
    //std::cout << "fpgaController, fpgaGetCscanMode cscan_mode=" << cscan_mode << std::endl;
	return cscan_mode;
}

// FPGA trigger by SW
bool FpgaController::fpgaTriggerSw(uint32_t channel){
    std::cout << "fpgaController, fpgaTriggerSw channel=" << channel << std::endl;

    this->user_buffer_tx[1] = 0;
    this->itsController->writeRegister(this->user_buffer_tx, FPGA_CONTROL_TRIGGER_SW_REGISTER);

    std::cout << "fpgaController, fpgaTriggerSw clear 1 OK" << channel << std::endl;

    while(this->fpgaReadRegisterValue(FPGA_STATUS_TRIGGER_SW_REGISTER));

    this->user_buffer_tx[1] = channel;
    this->itsController->writeRegister(this->user_buffer_tx, FPGA_CONTROL_TRIGGER_SW_REGISTER);

    while(this->fpgaReadRegisterValue(FPGA_STATUS_TRIGGER_SW_REGISTER) != channel);

    std::cout << "fpgaController, fpgaTriggerSw trigger OK" << channel << std::endl;

    this->user_buffer_tx[1] = 0;
    this->itsController->writeRegister(this->user_buffer_tx, FPGA_CONTROL_TRIGGER_SW_REGISTER);

    while(this->fpgaReadRegisterValue(FPGA_STATUS_TRIGGER_SW_REGISTER));

    std::cout << "fpgaController, fpgaTriggerSw clear 2 OK" << channel << std::endl;

    return true;
}

// Enable FPGA
bool FpgaController::fpgaEnable(){
    std::cout << "fpgaController, fpgaEnable" << std::endl;
    bool rc = this->bitEnableHS(FPGA_CONTROL_ENABLE_REGISTER, FPGA_CONTROL_ENABLE_BIT, FPGA_STATUS_ENABLE_REGISTER, FPGA_STATUS_ENABLE_BIT);
    if(rc){
        std::cout << "fpgaController, fpgaEnable OK" << std::endl;
    }else{
        std::cout << "fpgaController, fpgaEnable NOK" << std::endl;
    }
    return rc;
}

// Disable FPGA
bool FpgaController::fpgaDisable(){
    //std::cout << "fpgaController, fpgaDisable" << std::endl;
    bool rc = this->bitDisableHS(FPGA_CONTROL_ENABLE_REGISTER, FPGA_CONTROL_ENABLE_BIT, FPGA_STATUS_ENABLE_REGISTER, FPGA_STATUS_ENABLE_BIT);
    if(rc){
        //std::cout << "fpgaController, fpgaDisable OK" << std::endl;
    }else{
        //std::cout << "fpgaController, fpgaDisable NOK" << std::endl;
    }
    return rc;
}

// Reset FPGA
bool FpgaController::fpgaReset(){
    //std::cout << "fpgaController, fpgaReset" << std::endl;
    // Enable reset
    if(this->bitEnableHS(FPGA_CONTROL_RESET_REGISTER, FPGA_CONTROL_RESET_BIT, FPGA_STATUS_RESET_REGISTER, FPGA_STATUS_RESET_BIT)){
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
        bool rc = this->bitDisableHS(FPGA_CONTROL_RESET_REGISTER, FPGA_CONTROL_RESET_BIT, FPGA_STATUS_RESET_REGISTER, FPGA_STATUS_RESET_BIT);
        if(rc){
            //std::cout << "fpgaController, fpgaReset OK" << std::endl;
        }else{
            //std::cout << "fpgaController, fpgaReset NOK" << std::endl;
        }
        return rc;
    }
    //std::cout << "fpgaController, fpgaReset NOK" << std::endl;
    return false;
}

// Set heart beat
bool FpgaController::fpgaSetHB(){
	//std::cout << "fpgaController, fpgaSetHB" << std::endl;
    return this->bitEnableHS(FPGA_CONTROL_HEART_BEAT_REGISTER, FPGA_CONTROL_HEART_BEAT_BIT, FPGA_STATUS_HEART_BEAT_REGISTER, FPGA_STATUS_HEART_BEAT_BIT);
}

// UnSet heart beat
bool FpgaController::fpgaClearHB(){
	//std::cout << "fpgaController, fpgaClearHB" << std::endl;
    return this->bitDisableHS(FPGA_CONTROL_HEART_BEAT_REGISTER, FPGA_CONTROL_HEART_BEAT_BIT, FPGA_STATUS_HEART_BEAT_REGISTER, FPGA_STATUS_HEART_BEAT_BIT);
}

// Write value in one register
bool FpgaController::fpgaSetRegisterValue(uint32_t reg, uint32_t value){
	//std::cout << "fpgaController, fpgaSetRegisterValue" << std::endl;
    this->user_buffer_tx[1] = value;
    //std::cout << "fpgaController, fpgaSetRegisterValue value=" << value << std::endl;
    this->itsController->writeRegister(this->user_buffer_tx, reg);
    return true;
}

// Clear register value
bool FpgaController::fpgaClearRegisterValue(uint32_t reg){
	//std::cout << "fpgaController, fpgaClearRegisterValue" << std::endl;
    this->user_buffer_tx[1] = 0;
    this->itsController->writeRegister(this->user_buffer_tx, reg);
    return true;
}

// read register value
uint32_t FpgaController::fpgaReadRegisterValue(uint32_t reg){	
	//std::cout << "fpgaController, fpgaReadRegisterValue" << std::endl;
    // Read register
    uint32_t reg_rx = this->itsController->readRegister(this->user_buffer_rx, reg); 
    //std::cout << "fpgaController, fpgaReadRegisterValue reg_rx=" << reg_rx << std::endl; 
    return reg_rx;
}

// Enable bit control status registers
bool FpgaController::bitEnable(uint32_t control_reg, uint32_t control_bit_pos){
	//std::cout << "fpgaController, bitEnable" << std::endl;
	// Read control register
    uint32_t control_reg_rx = this->itsController->readRegister(this->user_buffer_rx, control_reg);
    // Enable bit
    control_reg_rx |= (1<<control_bit_pos);
    //std::cout << "fpgaController, bitEnable control_reg_rx=0x" << std::hex << control_reg_rx << std::endl; 
    this->user_buffer_tx[1] = control_reg_rx;
    this->itsController->writeRegister(this->user_buffer_tx, control_reg);  
    //Read control register
    if(control_reg_rx != this->itsController->readRegister(this->user_buffer_rx, control_reg))
		return false;

	return true;
}

// Enable bit with ACK control status registers
bool FpgaController::bitEnableHS(uint32_t control_reg, uint32_t control_bit_pos, uint32_t status_reg, uint32_t status_bit_pos){
    //std::cout << "fpgaController, bitEnableHS" << std::endl;
	// Read control register
    uint32_t control_reg_rx = this->itsController->readRegister(this->user_buffer_rx, control_reg);
    //std::cout << "fpgaController, bitEnableHS control_reg_rx=0x" << std::hex << control_reg_rx << std::endl;
    // Enable bit
    control_reg_rx |= (1<<control_bit_pos);
    //std::cout << "fpgaController, bitEnableHS control_reg_rx=0x" << std::hex << control_reg_rx << std::endl;
    this->user_buffer_tx[1] = control_reg_rx;
    this->itsController->writeRegister(this->user_buffer_tx, control_reg);
    
    // Read control register
    if(control_reg_rx != this->itsController->readRegister(this->user_buffer_rx, control_reg)){
        //std::cout << "fpgaController, bitEnableHS value readed=0x" << std::hex << this->itsController->readRegister(this->user_buffer_rx, control_reg) << std::endl;
        //std::cout << "fpgaController, bitEnableHS write not success" << std::endl;
		return false;
	}
	
	uint32_t status_reg_rx = 0;
	uint32_t status_reg_rx_mask = 0;
	status_reg_rx_mask |= (1<<status_bit_pos);
    //std::cout << "fpgaController, bitEnableHS status_reg_rx_mask=0x" << std::hex << status_reg_rx_mask << std::endl;
	auto start = std::chrono::steady_clock::now();
	do{
		// Read status register
		status_reg_rx = this->itsController->readRegister(this->user_buffer_rx, status_reg);
		//std::cout << "fpgaController, bitEnableHS status_reg_rx=0x" << std::hex << status_reg_rx << std::endl; 
		// Mask status_reg_rx
		status_reg_rx &= status_reg_rx_mask;
		//std::cout << "fpgaController, bitEnableHS status_reg_rx after mask=0x" << std::hex << status_reg_rx << std::endl; 
        if(!status_reg_rx){
            auto end = std::chrono::steady_clock::now();
            auto diff = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
            if(std::chrono::duration_cast<std::chrono::microseconds>(diff).count() > MAX_ACCEPTABLE_ACK_DELAY){
                //std::cout << "fpgaController, bitEnableHS MAX_ACCEPTABLE_ACK_DELAY" << std::endl;
                return false;
            }
            std::this_thread::sleep_for(std::chrono::microseconds(1));
        }
	}while(!status_reg_rx);
    //std::cout << "fpgaController, bitEnableHS to_return=" << to_return << std::endl;

    return true;
}

// Disable bit control status registers
bool FpgaController::bitDisable(uint32_t control_reg, uint32_t control_bit_pos){
	//std::cout << "fpgaController, bitDisable" << std::endl;
	// Read control register
    uint32_t control_reg_rx = this->itsController->readRegister(this->user_buffer_rx, control_reg);
    // Disable bit
    control_reg_rx &= ~(1<<control_bit_pos);
    //std::cout << "fpgaController, bitDisable control_reg_rx=0x" << std::hex << control_reg_rx << std::endl; 
    this->user_buffer_tx[1] = control_reg_rx;
    this->itsController->writeRegister(this->user_buffer_tx, control_reg);
    
    // Read control register
    if(control_reg_rx != this->itsController->readRegister(this->user_buffer_rx, control_reg))
		return false;
	
	return true;
}

// Disable bit with ACK control status registers
bool FpgaController::bitDisableHS(uint32_t control_reg, uint32_t control_bit_pos, uint32_t status_reg, uint32_t status_bit_pos){
    //std::cout << "fpgaController, bitDisableHS" << std::endl;
	// Read control register
    uint32_t control_reg_rx = this->itsController->readRegister(this->user_buffer_rx, control_reg);
    //std::cout << "fpgaController, bitDisableHS control_reg_rx=0x" << std::hex << control_reg_rx << std::endl;
    // Disable bit
    control_reg_rx &= ~(1<<control_bit_pos);
    //std::cout << "fpgaController, bitDisableHS control_reg_rx=0x" << std::hex << control_reg_rx << std::endl;
    this->user_buffer_tx[1] = control_reg_rx;
    this->itsController->writeRegister(this->user_buffer_tx, control_reg);
    
    //Read control register
    if(control_reg_rx != this->itsController->readRegister(this->user_buffer_rx, control_reg)){
        //std::cout << "fpgaController, bitDisableHS write not success" << std::endl;
		return false;
	}
	
	uint32_t status_reg_rx = 0;
	uint32_t status_reg_rx_mask = 0;
	status_reg_rx_mask &= (1<<status_bit_pos);
	auto start = std::chrono::steady_clock::now();
	do{
		// Read status register
		status_reg_rx = this->itsController->readRegister(this->user_buffer_rx, status_reg);
		// Mask status_reg_rx
		status_reg_rx &= status_reg_rx_mask;
        if(status_reg_rx){
            auto end = std::chrono::steady_clock::now();
            auto diff = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
            if(std::chrono::duration_cast<std::chrono::microseconds>(diff).count() > MAX_ACCEPTABLE_ACK_DELAY){
                //std::cout << "fpgaController, bitEnableHS MAX_ACCEPTABLE_ACK_DELAY" << std::endl;
                return false;
            }
            std::this_thread::sleep_for(std::chrono::microseconds(1));
        }
	}while(status_reg_rx);
    //std::cout << "fpgaController, bitDisableHS to_return=" << to_return << std::endl;
	
    return true;
}

// Debug

bool FpgaController::fpgaAdcResetOn(){
    std::cout << "fpgaController, fpgaAdcReset" << std::endl;

    this->user_buffer_tx[1] = 0;
    this->itsController->writeRegister(this->user_buffer_tx, ADC_DEBUG_REGISTER);

    std::this_thread::sleep_for(std::chrono::microseconds(10));

    this->user_buffer_tx[1] = 1;
    this->itsController->writeRegister(this->user_buffer_tx, ADC_DEBUG_REGISTER);

    std::this_thread::sleep_for(std::chrono::microseconds(10));

    //this->user_buffer_tx[1] = 0;
    //this->itsController->writeRegister(this->user_buffer_tx, ADC_DEBUG_REGISTER);

    std::this_thread::sleep_for(std::chrono::microseconds(10));

    return true;
}

bool FpgaController::fpgaAdcResetOff(){
    std::cout << "fpgaController, fpgaAdcReset" << std::endl;

    this->user_buffer_tx[1] = 0;
    this->itsController->writeRegister(this->user_buffer_tx, ADC_DEBUG_REGISTER);

    std::this_thread::sleep_for(std::chrono::microseconds(10));

    //this->user_buffer_tx[1] = 1;
    //this->itsController->writeRegister(this->user_buffer_tx, ADC_DEBUG_REGISTER);

    //std::this_thread::sleep_for(std::chrono::microseconds(10));

    //this->user_buffer_tx[1] = 0;
    //this->itsController->writeRegister(this->user_buffer_tx, ADC_DEBUG_REGISTER);

    std::this_thread::sleep_for(std::chrono::microseconds(10));

    return true;
}

bool FpgaController::fpgaAdcPatternOn(){
    std::cout << "fpgaController, fpgaAdcPatternOn" << std::endl;

    uint32_t reg;

    reg = this->itsController->readRegister(this->user_buffer_rx, ADC_CONFIG_CURRENT);
    std::cout << "ADC_CONFIG_CURRENT : " << std::bitset<32>(reg) << std::endl;
    reg |= 0x04000000;
    //reg &= 0xFFFFF9FF;
    //reg |= 0x00000400;
    //reg |= 0x00000600;

    //reg &= 0x1FFFFFFF;
    //reg |= 0x20000000;
    //reg |= 0x40000000;
    //reg |= 0x80000000;
    this->user_buffer_tx[1] = reg;
    this->itsController->writeRegister(this->user_buffer_tx, ADC_CONFIG_EX);
    this->user_buffer_tx[1] = 1;
    this->itsController->writeRegister(this->user_buffer_tx, ADC_CONFIG_EX_VALID);

    while(!this->fpgaReadRegisterValue(ADC_CONFIG_REQUEST));

    this->user_buffer_tx[1] = 0;
    this->itsController->writeRegister(this->user_buffer_tx, ADC_CONFIG_EX_VALID);

    this->user_buffer_tx[1] = 1;
    this->itsController->writeRegister(this->user_buffer_tx, ADC_CONFIG_REQUEST_ACK);

    std::this_thread::sleep_for(std::chrono::microseconds(1));

    this->user_buffer_tx[1] = 0;
    this->itsController->writeRegister(this->user_buffer_tx, ADC_CONFIG_REQUEST_ACK);

    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    std::cout << "ADC_CONFIG_CURRENT : " << std::bitset<32>(reg) << std::endl;

    return true;
}

bool FpgaController::fpgaAdcPatternOff(){
    std::cout << "fpgaController, fpgaAdcPatternOff" << std::endl;

    uint32_t reg;

    reg = this->itsController->readRegister(this->user_buffer_rx, ADC_CONFIG_CURRENT);
    std::cout << "ADC_CONFIG_CURRENT : " << std::bitset<32>(reg) << std::endl;
    reg &= 0xFBFFFFFF;
    this->user_buffer_tx[1] = reg;
    this->itsController->writeRegister(this->user_buffer_tx, ADC_CONFIG_EX);
    this->user_buffer_tx[1] = 1;
    this->itsController->writeRegister(this->user_buffer_tx, ADC_CONFIG_EX_VALID);

    while(!this->fpgaReadRegisterValue(ADC_CONFIG_REQUEST));

    this->user_buffer_tx[1] = 0;
    this->itsController->writeRegister(this->user_buffer_tx, ADC_CONFIG_EX_VALID);

    this->user_buffer_tx[1] = 1;
    this->itsController->writeRegister(this->user_buffer_tx, ADC_CONFIG_REQUEST_ACK);

    std::this_thread::sleep_for(std::chrono::microseconds(1));

    this->user_buffer_tx[1] = 0;
    this->itsController->writeRegister(this->user_buffer_tx, ADC_CONFIG_REQUEST_ACK);

    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    std::cout << "ADC_CONFIG_CURRENT : " << std::bitset<32>(reg) << std::endl;

    return true;
}

bool FpgaController::fpgaAdcTrigger(){
	std::cout << "fpgaController, fpgaAdcTrigger" << std::endl;

    this->user_buffer_tx[1] = 0;
    this->itsController->writeRegister(this->user_buffer_tx, ADC_DEBUG_REGISTER);

    std::this_thread::sleep_for(std::chrono::microseconds(10));

    this->user_buffer_tx[1] = 2;
    this->itsController->writeRegister(this->user_buffer_tx, ADC_DEBUG_REGISTER);

    std::this_thread::sleep_for(std::chrono::microseconds(10));

    this->user_buffer_tx[1] = 0;
    this->itsController->writeRegister(this->user_buffer_tx, ADC_DEBUG_REGISTER);

    std::this_thread::sleep_for(std::chrono::microseconds(10));

    return true;
}

void FpgaController::debugFPGARegisters(){

	std::this_thread::sleep_for(std::chrono::milliseconds(20));

    uint32_t reg;

    reg = this->itsController->readRegister(this->user_buffer_rx, CONFIGURATION_SIZE_REGISTER);
    std::cout << "fpgaController, debugFPGARegisters CONFIGURATION_SIZE_REGISTER=" << reg << std::endl;

    reg = this->itsController->readRegister(this->user_buffer_rx, CONFIGURATION_ON_REGISTER);
    std::cout << "fpgaController, debugFPGARegisters CONFIGURATION_ON_REGISTER=" << reg << std::endl;

    reg = this->itsController->readRegister(this->user_buffer_rx, CH0_RECEIVER_SAMPLING_FREQUENCY_REGISTER);
    std::cout << "fpgaController, debugFPGARegisters CH0_RECEIVER_SAMPLING_FREQUENCY_REGISTER="  << reg << std::endl;

    reg = this->itsController->readRegister(this->user_buffer_rx, CH0_RECEIVER_DATA_WINDOW_REGISTER);
    std::cout << "fpgaController, debugFPGARegisters CH0_RECEIVER_DATA_WINDOW_REGISTER="  << reg << std::endl;

    reg = this->itsController->readRegister(this->user_buffer_rx, CH0_RECEIVER_DELAY_REGISTER);
    std::cout << "fpgaController, debugFPGARegisters CH0_RECEIVER_DELAY_REGISTER="  << reg << std::endl;

    reg = this->itsController->readRegister(this->user_buffer_rx, CH0_RECEIVER_ANALOG_GAIN_REGISTER);
    std::cout << "fpgaController, debugFPGARegisters CH0_RECEIVER_ANALOG_GAIN_REGISTER="  << reg << std::endl;

    reg = this->itsController->readRegister(this->user_buffer_rx, CH0_RECEIVER_DIGITAL_GAIN_REGISTER);
    std::cout << "fpgaController, debugFPGARegisters CH0_RECEIVER_DIGITAL_GAIN_REGISTER="  << reg << std::endl;

    reg = this->itsController->readRegister(this->user_buffer_rx, CH0_RECEIVER_EXTERNAL_MULTIPLEXER_EN_REGISTER);
    std::cout << "fpgaController, debugFPGARegisters CH0_RECEIVER_EXTERNAL_MULTIPLEXER_EN_REGISTER="  << reg << std::endl;

    reg = this->itsController->readRegister(this->user_buffer_rx, CH0_TRANSMITTER_VOLTAGE_REGISTER);
    std::cout << "fpgaController, debugFPGARegisters CH0_TRANSMITTER_VOLTAGE_REGISTER=" << reg << std::endl;

    reg = this->itsController->readRegister(this->user_buffer_rx, CH0_TRANSMITTER_BURST_FREQUENCY_REGISTER);
    std::cout << "fpgaController, debugFPGARegisters CH0_TRANSMITTER_BURST_FREQUENCY_REGISTER=" << reg << std::endl;

    reg = this->itsController->readRegister(this->user_buffer_rx, CH0_TRANSMITTER_N_CYCLES_REGISTER);
    std::cout << "fpgaController, debugFPGARegisters CH0_TRANSMITTER_N_CYCLES_REGISTER=" << reg << std::endl;

    reg = this->itsController->readRegister(this->user_buffer_rx, CH0_TRANSMITTER_DELAY_REGISTER);
    std::cout << "fpgaController, debugFPGARegisters CH0_TRANSMITTER_DELAY_REGISTER=" << reg << std::endl;

    reg = this->itsController->readRegister(this->user_buffer_rx, CH0_TRANSMITTER_DIRECTIONAL_PHASING_REGISTER);
    std::cout << "fpgaController, debugFPGARegisters CH0_TRANSMITTER_DIRECTIONAL_PHASING_REGISTER=" << reg << std::endl;

    reg = this->itsController->readRegister(this->user_buffer_rx, CH0_MAGNET_MODE_REGISTER);
    std::cout << "fpgaController, debugFPGARegisters CH0_MAGNET_MODE_REGISTER=" << reg << std::endl;

    reg = this->itsController->readRegister(this->user_buffer_rx, CH0_MAGNET_PULSE_WIDTH_REGISTER);
    std::cout << "fpgaController, debugFPGARegisters CH0_MAGNET_PULSE_WIDTH_REGISTER=" << reg << std::endl;

    reg = this->itsController->readRegister(this->user_buffer_rx, CH0_MAGNET_INITIAL_DELAY_REGISTER);
    std::cout << "fpgaController, debugFPGARegisters CH0_MAGNET_INITIAL_DELAY_REGISTER=" << reg << std::endl;

    reg = this->itsController->readRegister(this->user_buffer_rx, CH0_DSP_ANALOG_FILTER_REGISTER);
    std::cout << "fpgaController, debugFPGARegisters CH0_DSP_ANALOG_FILTER_REGISTER=" << reg << std::endl;

    reg = this->itsController->readRegister(this->user_buffer_rx, CONFIGURATION_HEADER_SIZE);
    std::cout << "fpgaController, debugFPGARegisters CONFIGURATION_HEADER_SIZE=" << reg << std::endl;

    reg = this->itsController->readRegister(this->user_buffer_rx, CONFIGURATION_CHANNELS);
    std::cout << "fpgaController, debugFPGARegisters CONFIGURATION_CHANNELS=" << reg << std::endl;

    reg = this->itsController->readRegister(this->user_buffer_rx, CONFIGURATION_CHANNEL0_OFFSET);
    std::cout << "fpgaController, debugFPGARegisters CONFIGURATION_CHANNEL0_OFFSET=" << reg << std::endl;

    reg = this->itsController->readRegister(this->user_buffer_rx, CONFIGURATION_ENCODER_OFFSET);
    std::cout << "fpgaController, debugFPGARegisters CONFIGURATION_ENCODER_OFFSET=" << reg << std::endl;

    reg = this->itsController->readRegister(this->user_buffer_rx, CONFIGURATION_DIGITALIO_OFFSET);
    std::cout << "fpgaController, debugFPGARegisters CONFIGURATION_DIGITALIO_OFFSET=" << reg << std::endl;

    reg = this->itsController->readRegister(this->user_buffer_rx, CONFIGURATION_CHANNEL0_SIZE);
    std::cout << "fpgaController, debugFPGARegisters CONFIGURATION_CHANNEL0_SIZE=" << reg << std::endl;

    reg = this->itsController->readRegister(this->user_buffer_rx, CONFIGURATION_TRIGGER_TYPE);
    std::cout << "fpgaController, debugFPGARegisters CONFIGURATION_TRIGGER_TYPE=" << reg << std::endl;

    reg = this->itsController->readRegister(this->user_buffer_rx, CH0_SIGNAL_INPUT_AXIS_TREADY);
    std::cout << "fpgaController, debugFPGARegisters CH0_SIGNAL_INPUT_AXIS_TREADY=" << reg << std::endl;

    reg = this->itsController->readRegister(this->user_buffer_rx, CONFIGURATION_CHANNEL0_ENABLED);
    std::cout << "fpgaController, debugFPGARegisters CONFIGURATION_CHANNEL0_ENABLED=" << reg << std::endl;

    reg = this->itsController->readRegister(this->user_buffer_rx, CONFIGURATION_CHANNEL0_ON);
    std::cout << "fpgaController, debugFPGARegisters CONFIGURATION_CHANNEL0_ON=" << reg << std::endl;

    reg = this->itsController->readRegister(this->user_buffer_rx, FPGA_STATUS_START_INSPECTION_REGISTER);
    std::cout << "fpgaController, debugFPGARegisters FPGA_STATUS_START_INSPECTION_REGISTER=" << reg << std::endl;

    std::cout << std::endl;

    reg = this->itsController->readRegister(this->user_buffer_rx, CH0_CHANNEL_TRIGGER_TYPE);
    std::cout << "fpgaController, debugFPGARegisters CH0_CHANNEL_TRIGGER_TYPE=" << reg << std::endl;

    reg = this->itsController->readRegister(this->user_buffer_rx, CH0_STATE_A);
    std::cout << "fpgaController, debugFPGARegisters CH0_STATE_A=" << reg << std::endl;

    reg = this->itsController->readRegister(this->user_buffer_rx, CH0_STATE_B);
    std::cout << "fpgaController, debugFPGARegisters CH0_STATE_B=" << reg << std::endl;

    reg = this->itsController->readRegister(this->user_buffer_rx, CH0_STATE_C);
    std::cout << "fpgaController, debugFPGARegisters CH0_STATE_C=" << reg << std::endl;

    std::cout << std::endl;

    reg = this->itsController->readRegister(this->user_buffer_rx, MAIN_DMA_STATE);
    std::cout << "fpgaController, debugFPGARegisters MAIN_DMA_STATE=" << reg << std::endl;

    reg = this->itsController->readRegister(this->user_buffer_rx, ADC_WRITE_COUNTER);
    std::cout << "fpgaController, debugFPGARegisters ADC_WRITE_COUNTER=" << reg << std::endl;

    reg = this->itsController->readRegister(this->user_buffer_rx, ADC_READ_COUNTER);
    std::cout << "fpgaController, debugFPGARegisters ADC_READ_COUNTER=" << reg << std::endl;

}


