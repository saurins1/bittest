#ifndef DEFSARM
#define DEFSARM

#include <string>

typedef struct{

    bool FGPA_AVAILABLE;

    std::string PORT0;
    std::string PORT1;

    uint32_t TIMER_ARM_SERVICE;
    uint32_t TIMEOUT_HEARTBEAT;
    uint32_t TIMER_ACQ_SIM_ASCAN;   // 1milli -> 1000Hz, 10 milli -> 100 Hz, 20 milli -> 50 Hz, 100milli -> 10 Hz
    uint32_t TIMER_ACQ_SIM_CSCAN;    // 100 us -> 10 Khz

    std::string LOG_FILE;
    std::string BITFILE;  //Bitfile location and name in the fpga board

    std::string SYSTEM_TYPE;

} ARMSetupStruct;



#endif // DEFSARM

