#ifndef FPGACONTROLLER_H
#define FPGACONTROLLER_H

#ifndef _WIN32
#include <sys/ioctl.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <fcntl.h>
#include <iostream>
#include <unistd.h>
#include <cstring>
#include <time.h>
#include <chrono>
#include <thread>
#include <bitset>

#include "itscontroller.h"
#include "defsarm.h"
#include "fpgadefinitions.h"

class FpgaController
{
    public:

        FpgaController();
        virtual ~FpgaController();
        
        /* UPLOAD OPERATIONS */
		bool fpgaSetUploadDMALen(uint32_t len);
		bool fpgaClearUploadDMALen();
		uint32_t fpgaReadUploadDmaLen();
		bool fpgaStartUploadAscan();
		bool fpgaStopUploadAscan();
		bool fpgaStartUploadCscan();
		bool fpgaStopUploadCscan();
		bool fpgaStartUploadConfig();
		bool fpgaStopUploadConfig();
		bool fpgaStartUploadCapabilities();
		bool fpgaStopUploadCapabilities();
		bool fpgaSetUploadConfigReady();
        bool fpgaClearUploadConfigReady();
        bool fpgaSetUploadCapabilitiesReady();
        bool fpgaClearUploadCapabilitiesReady();
		bool fpgaSetAscanAvailability();
		bool fpgaSetCscanAvailability();
		bool fpgaClearAscanAvailability();
		bool fpgaClearCscanAvailability();
		bool fpgaReadStatusAscanAvailability();
		bool fpgaReadStatusCscanAvailability();
		bool scanReady();
		
		/* DOWNLOAD OPERATIONS */
		bool fpgaSetDownloadDMALen(uint32_t len);
		bool fpgaClearDownloadDMALen();
		uint32_t fpgaReadDownloadDmaLen();
		bool fpgaStartDownloadConfig();
		bool fpgaStopDownloadConfig();
		bool fpgaStartDownloadAscanReplay();
		bool fpgaStopDownloadAscanReplay();
		bool fpgaStartDownloadDacCurve();
		bool fpgaStopDownloadDacCurve();

		/* CONTROL/STATUS OPERATIONS */
		bool fpgaStartInspection();
		bool fpgaStopInspection();
		bool fpgaSetScanMode(uint32_t mode);
		uint32_t fpgaGetScanMode();
		bool fpgaSetCscanMode(uint32_t mode);
		uint32_t fpgaGetCscanMode();
        bool fpgaTriggerSw(uint32_t channel);
		bool fpgaEnable();
		bool fpgaDisable();
		bool fpgaReset();
		bool fpgaSetHB();
		bool fpgaClearHB();
		   
		/* FPGA ON FLY PARAMETER CHANGE REGISTER */
		bool onFlyParameterChange(uint32_t *bufferAddress, uint32_t *bufferData, uint32_t len);

        /* DEBUG */
		bool fpgaAdcResetOn();
		bool fpgaAdcResetOff();
		bool fpgaAdcPatternOn();
		bool fpgaAdcPatternOff();
		bool fpgaAdcTrigger();
        void debugFPGARegisters();

    protected:

        uint32_t *user_buffer_tx = nullptr;         // buffer tx to writte in control FPGA registers
        uint32_t *user_buffer_rx = nullptr;         // buffer rx to read from control and status FPGA registers
        ItsController *itsController = nullptr;     // ITOP Top System controller
        uint32_t registers_len;                     // ITS register lenght

        /* CONTROL/STATUS OPERATIONS */
		bool fpgaSetRegisterValue(uint32_t reg, uint32_t value);
		bool fpgaClearRegisterValue(uint32_t reg);
		uint32_t fpgaReadRegisterValue(uint32_t reg);
		bool bitEnable(uint32_t control_reg, uint32_t control_bit_pos);
		bool bitEnableHS(uint32_t control_reg, uint32_t control_bit_pos, uint32_t status_reg, uint32_t status_bit_pos);
		bool bitDisable(uint32_t control_reg, uint32_t control_bit_pos);
		bool bitDisableHS(uint32_t control_reg, uint32_t control_bit_pos, uint32_t status_reg, uint32_t status_bit_pos);

    private:
};

#endif // FPGACONTROLLER_H
