/*
 * Empty C++ Application
 */

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <cstring>
#include <iostream>
#include <sstream>
#include <fstream>
#include <unistd.h>
#include <pthread.h>
#include <chrono>
#include <thread>
#include <vector>
#include <queue>
#include "fpgaboard.h"
#include "json.hpp"

using namespace nlohmann;

FpgaBoard *fpgaBoard = nullptr;

// struct Main Config
struct MainConfig {
	std::vector<int32_t> configuration;
};

MainConfig mainConfig;
int32_t *myconfig = nullptr;

json json_config;
std::string json_config_name;

bool adc_reset = false;
bool adc_pattern = false;

void defaultConfig();
void mainMenu();
void operationModeMenu();
void operationMode();
void configurationMenu();
void writeConfiguration();

void writeConfigurationSampleFrequency();
void writeConfigurationDataWindow();
void writeConfigurationReceiverDelay();
void writeConfigurationAnalogGain();
void writeConfigurationDigitalGain();
void writeConfigurationBurstFrequency();
void writeConfigurationNumberCycles();
void writeConfigurationPrf();
void writeConfigurationTransmitterDelay();
void writeConfigurationDeltaDelay();
void writeConfigurationDeltaAdd();
void writeConfigurationAverage();
void writeConfigurationCoincidence();
void writeConfigurationTriggerType();
void writeConfigurationIo();

int main()
{
	fpgaBoard = new FpgaBoard();

	json_config_name.assign("configuration_tgil.json");

	// Read capabilities
	std::this_thread::sleep_for(std::chrono::milliseconds(50));
	fpgaBoard->ReadInfo();

	// Default config
	defaultConfig();

	int option = 1;
	while(option){
		mainMenu();
		scanf ("%d",&option);
		switch(option){
			case 0:

				break;
			case 1:
				fpgaBoard->StartAcquisition();
				break;
			case 2:
				fpgaBoard->StopAcquisition();
				break;
			case 3:
				fpgaBoard->ReadInfo();
				break;
			case 4:
				fpgaBoard->Reset();
				break;
			case 5:
				operationMode();
				break;
			case 6:
				writeConfiguration();
				break;
			case 7:
				fpgaBoard->ReadConfiguration();
				break;
			case 8:
				fpgaBoard->TriggerSw(1);
				break;
			case 9:
				if(!adc_reset){
					fpgaBoard->AdcResetOn();
					adc_reset = true;
				}else{
					fpgaBoard->AdcResetOff();
					adc_reset = false;
				}
				break;
			case 10:
				if(!adc_pattern){
					fpgaBoard->AdcPatternOn();
					adc_pattern = true;
				}else{
					fpgaBoard->AdcPatternOff();
					adc_pattern = false;
				}
				break;
			case 11:
				fpgaBoard->AdcTrigger();
				break;
		}
	}

	if(fpgaBoard != nullptr){
		std::cout << "delete fpgaBoard" << std::endl;
		delete fpgaBoard;
		fpgaBoard = nullptr;
	}

    if( mainConfig.configuration.size() > 0 ){
    	mainConfig.configuration.clear();
    }

	return 0;
}

void defaultConfig(){
	std::cout << "Main, defaultConfig" << std::endl;

    // read a JSON file
    std::ifstream i(json_config_name);
    i >> json_config;
    i.close();

    mainConfig.configuration = std::move(json_config["configuration"].get<std::vector<int32_t>>());
    myconfig = (int32_t *) &mainConfig.configuration[0];

    /*std::cout << std::endl << "CONFIGURATION = [" << std::endl;
    for(uint32_t i=0;i<mainConfig.configuration.size();i++){
    	std::cout << " " << myconfig[i] << " "<< std::endl;
    }
    std::cout << std::endl << "]"<< std::endl;*/

    // Write config
    std::this_thread::sleep_for(std::chrono::milliseconds(50));
    fpgaBoard->WriteConfiguration(mainConfig.configuration);
}


void mainMenu(){
	std::cout << std::endl;
	std::cout << "---------- SELECT OPTION ----------" << std::endl;
	std::cout << "1-Start Inspection" << std::endl;
	std::cout << "2-Stop Inspection" << std::endl;
	std::cout << "3-Read Capabilities" << std::endl;
	std::cout << "4-Reset" << std::endl;
	std::cout << "5-Configure Operation Mode" << std::endl;
	std::cout << "6-Write Configuration" << std::endl;
	std::cout << "7-Read Configuration" << std::endl;
	std::cout << "8-Trigger SW " << std::endl;
	std::cout << "9-ADC Reset On/Off" << std::endl;
	std::cout << "10-ADC Pattern On/Off" << std::endl;
	std::cout << "11-ADC trigger" << std::endl;
	std::cout << "0-Exit " << std::endl << std::endl;
}

void operationModeMenu(){
	std::cout << std::endl;
	std::cout << "---------- OPERATION MODE ----------" << std::endl;
	std::cout << "1-MODE_ASCAN" << std::endl;
	std::cout << "2-MODE_IDLE" << std::endl;
	std::cout << "3-MODE_MRUT" << std::endl;
	std::cout << "0-Exit" << std::endl;
}

void operationMode(){
	int option = 1;
	while(option){
		operationModeMenu();
		scanf ("%d",&option);
		switch(option){
			case 1:
				fpgaBoard->OperationMode(MODE_ASCAN);
				option = 0;
				break;
			case 2:
				fpgaBoard->OperationMode(MODE_IDLE);
				option = 0;
				break;
			case 3:
				fpgaBoard->OperationMode(MODE_MRUT);
				option = 0;
				break;
		}
	}
}

void configurationMenu(){
	std::cout << std::endl;
	std::cout << "---------- WRITE CONFIGURATION ----------" << std::endl;
	std::cout << std::endl;
	std::cout << "1-Write Configuration" << std::endl;
	std::cout << std::endl;
	std::cout << "-- RECEIVER --" << std::endl;
	std::cout << std::endl;
	std::cout << "10-Sampling Frequency [Mhz]" << std::endl;
	std::cout << "11-Data Window [samples]" << std::endl;
	std::cout << "12-Receiver Delay [ns]" << std::endl;
	std::cout << "13-Analog Gain [db]" << std::endl;
	std::cout << "14-Digital Gain [db]" << std::endl;
	std::cout << std::endl;
	std::cout << "-- TRANSMITTER --" << std::endl;
	std::cout << std::endl;
	std::cout << "20-Burst Frequency [Khz]" << std::endl;
	std::cout << "21-N Cycles" << std::endl;
	std::cout << "22-PRF [hz]" << std::endl;
	std::cout << "23-Transmitter delay [ns]" << std::endl;
	std::cout << "24-Delta delay [ns]" << std::endl;
	std::cout << "25-Delta add [ns]" << std::endl;
	std::cout << std::endl;
	std::cout << "-- DSP --" << std::endl;
	std::cout << std::endl;
	std::cout << "30-Average" << std::endl;
	std::cout << "31-Coincidence" << std::endl;
	std::cout << std::endl;
	std::cout << "-- TRIGGER TYPE --" << std::endl;
	std::cout << std::endl;
	std::cout << "40-trigger type" << std::endl;
	std::cout << std::endl;
	std::cout << "-- I/O --" << std::endl;
	std::cout << std::endl;
	std::cout << "50-I/O" << std::endl;
	std::cout << std::endl;
	std::cout << "0-Exit" << std::endl;
}

void writeConfiguration(){
	int option = 1;
	while(option){
		configurationMenu();
		scanf ("%d",&option);
		switch(option){
			case 1:
				fpgaBoard->WriteConfiguration(mainConfig.configuration);
				break;
			case 10:
				writeConfigurationSampleFrequency();
				break;
			case 11:
				writeConfigurationDataWindow();
				break;
			case 12:
				writeConfigurationReceiverDelay();
				break;
			case 13:
				writeConfigurationAnalogGain();
				break;
			case 14:
				writeConfigurationDigitalGain();
				break;
			case 20:
				writeConfigurationBurstFrequency();
				break;
			case 21:
				writeConfigurationNumberCycles();
				break;
			case 22:
				writeConfigurationPrf();
				break;
			case 23:
				writeConfigurationTransmitterDelay();
				break;
			case 24:
				writeConfigurationDeltaDelay();
				break;
			case 25:
				writeConfigurationDeltaAdd();
				break;
			case 30:
				writeConfigurationAverage();
				break;
			case 31:
				writeConfigurationCoincidence();
				break;
			case 40:
				writeConfigurationTriggerType();
				break;
			case 50:
				writeConfigurationIo();
				break;
		}
	}
}

void writeConfigurationSampleFrequency(){
	uint32_t intValue = 0;
	std::cout << "Sample Frequency: ";
	scanf ("%d",&intValue);
	std::cout << std::endl;

	if(intValue >= 100){
		intValue = 100;
	}
	intValue = 100 / intValue;
	std::cout << "intValue " << intValue << std::endl;

	myconfig[CONFIG_SAMPLE_FREQUENCY] = (int32_t)intValue;
	fpgaBoard->WriteConfiguration(mainConfig.configuration);

	json_config["configuration"] = mainConfig.configuration;

    std::ofstream o(json_config_name);
    o << std::setw(4) << json_config << std::endl;
    o.close();
}

void writeConfigurationDataWindow(){
	uint32_t intValue = 0;
	std::cout << "Data Window: ";
	scanf ("%d",&intValue);
	std::cout << std::endl;

	if(intValue >= 40000){
		intValue = 40000;
	}
	std::cout << "intValue " << intValue << std::endl;

	myconfig[CONFIG_DATA_WINDOW] = (int32_t)intValue;
	fpgaBoard->WriteConfiguration(mainConfig.configuration);

	json_config["configuration"] = mainConfig.configuration;

    std::ofstream o(json_config_name);
    o << std::setw(4) << json_config << std::endl;
    o.close();
}

void writeConfigurationReceiverDelay(){
	uint32_t intValue = 0;
	std::cout << "Receiver Delay: ";
	scanf ("%d",&intValue);
	std::cout << std::endl;

	intValue = intValue / 10;
	std::cout << "intValue " << intValue << std::endl;

	myconfig[CONFIG_RECEIVER_DELAY] = (int32_t)intValue;
	fpgaBoard->WriteConfiguration(mainConfig.configuration);

	json_config["json_config"] = mainConfig.configuration;

    std::ofstream o(json_config_name);
    o << std::setw(4) << json_config << std::endl;
    o.close();
}

void writeConfigurationAnalogGain(){
	int32_t intValue = 0;
	float floatValue = 0;
	std::cout << "Analog Gain: ";
	scanf ("%f",&floatValue);
	std::cout << std::endl;

	if(floatValue >= 53){
		floatValue = 53;
	}

	if(floatValue <= 7.5){
		floatValue = 7.5;
	}

	floatValue = floatValue*1023/53;
	std::cout << "floatValue " << floatValue << std::endl;

	intValue = (int32_t)floatValue;
	std::cout << "intValue " << intValue << std::endl;

	myconfig[CONFIG_ANALOG_GAIN] = (int32_t)intValue;
	fpgaBoard->WriteConfiguration(mainConfig.configuration);

	json_config["configuration"] = mainConfig.configuration;

    std::ofstream o(json_config_name);
    o << std::setw(4) << json_config << std::endl;
    o.close();
}

void writeConfigurationDigitalGain(){
	int32_t intValue = 0;
	float floatValue = 0;
	std::cout << "Digital Gain: ";
	scanf ("%f",&floatValue);
	std::cout << std::endl;

	if(floatValue >= 20){
		floatValue = 20;
	}

	if(floatValue <= -20){
		floatValue = -20;
	}

	floatValue = pow(10.0,(floatValue)/20.0);
	floatValue = floatValue *1024*1024;
	std::cout << "floatValue " << floatValue << std::endl;

	intValue = (int32_t)floatValue;
	std::cout << "intValue " << intValue << std::endl;

	myconfig[CONFIG_ANALOG_GAIN] = (int32_t)intValue;
	fpgaBoard->WriteConfiguration(mainConfig.configuration);

	json_config["configuration"] = mainConfig.configuration;

    std::ofstream o(json_config_name);
    o << std::setw(4) << json_config << std::endl;
    o.close();
}

void writeConfigurationBurstFrequency(){
	uint32_t intValue = 0;
	std::cout << "Burst Frequency: ";
	scanf ("%d",&intValue);
	std::cout << std::endl;

	if(intValue >= 8000){
		intValue = 8000;
	}

	if(intValue <= 100){
		intValue = 100;
	}

	double period = 1/((double)intValue*1000);
	period = period/2;
	period = period * pow(10.0, 12.0);

	intValue = (uint32_t) period;
	std::cout << "intValue " << intValue << std::endl;

	myconfig[CONFIG_BURST_FREQUENCY] = (int32_t)intValue;
	fpgaBoard->WriteConfiguration(mainConfig.configuration);

	json_config["configuration"] = mainConfig.configuration;

    std::ofstream o(json_config_name);
    o << std::setw(4) << json_config << std::endl;
    o.close();
}

void writeConfigurationNumberCycles(){
	uint32_t intValue = 0;
	std::cout << "N Cycles: ";
	scanf ("%d",&intValue);
	std::cout << std::endl;

	if(intValue >= 10){
		intValue = 10;
	}
	intValue = intValue *2;
	std::cout << "intValue " << intValue << std::endl;

	myconfig[CONFIG_N_CYCLES] = (int32_t)intValue;
	fpgaBoard->WriteConfiguration(mainConfig.configuration);

	json_config["configuration"] = mainConfig.configuration;

    std::ofstream o(json_config_name);
    o << std::setw(4) << json_config << std::endl;
    o.close();
}

void writeConfigurationPrf(){
	uint32_t intValue = 0;
	std::cout << "PRF: ";
	scanf ("%d",&intValue);
	std::cout << std::endl;

	if(intValue >= 600){
		intValue = 600;
	}

	double cycles = 1/(double)intValue;
	cycles = cycles * pow(10.0,9.0);
	cycles = cycles / 10;
	std::cout << "cycles " << cycles << std::endl;

	intValue = (int32_t)cycles;
	std::cout << "intValue " << intValue << std::endl;

	myconfig[CONFIG_PRF] = (int32_t)intValue;
	fpgaBoard->WriteConfiguration(mainConfig.configuration);

	json_config["configuration"] = mainConfig.configuration;

    std::ofstream o(json_config_name);
    o << std::setw(4) << json_config << std::endl;
    o.close();
}

void writeConfigurationTransmitterDelay(){
	uint32_t intValue = 0;
	std::cout << "Transmitter Delay: ";
	scanf ("%d",&intValue);
	std::cout << std::endl;

	intValue = intValue / 10;
	std::cout << "intValue " << intValue << std::endl;

	myconfig[CONFIG_TRANSMITTER_DELAY] = (int32_t)intValue;
	fpgaBoard->WriteConfiguration(mainConfig.configuration);

	json_config["configuration"] = mainConfig.configuration;

    std::ofstream o(json_config_name);
    o << std::setw(4) << json_config << std::endl;
    o.close();
}

void writeConfigurationDeltaDelay(){
	uint32_t intValue = 0;
	std::cout << "Transmitter Delta Delay: ";
	scanf ("%d",&intValue);
	std::cout << std::endl;

	intValue = intValue / 10;
	std::cout << "intValue " << intValue << std::endl;

	myconfig[CONFIG_DELTA_DELAY] = (int32_t)intValue;
	fpgaBoard->WriteConfiguration(mainConfig.configuration);

	json_config["configuration"] = mainConfig.configuration;

    std::ofstream o(json_config_name);
    o << std::setw(4) << json_config << std::endl;
    o.close();
}

void writeConfigurationDeltaAdd(){
	uint32_t intValue = 0;
	std::cout << "Transmitter Delta Add: ";
	scanf ("%d",&intValue);
	std::cout << std::endl;

	intValue = intValue / 10;
	std::cout << "intValue " << intValue << std::endl;

	myconfig[CONFIG_DELTA_ADD] = (int32_t)intValue;
	fpgaBoard->WriteConfiguration(mainConfig.configuration);

	json_config["configuration"] = mainConfig.configuration;

    std::ofstream o(json_config_name);
    o << std::setw(4) << json_config << std::endl;
    o.close();
}

void writeConfigurationAverage(){
	uint32_t intValue = 0;
	std::cout << "Average: ";
	scanf ("%d",&intValue);
	std::cout << std::endl;

	if(intValue >= 64){
		intValue = 64;
	}
	std::cout << "intValue " << intValue << std::endl;

	myconfig[CONFIG_AVERAGE] = (int32_t)intValue;
	fpgaBoard->WriteConfiguration(mainConfig.configuration);

	json_config["configuration"] = mainConfig.configuration;

    std::ofstream o(json_config_name);
    o << std::setw(4) << json_config << std::endl;
    o.close();
}

void writeConfigurationCoincidence(){
	uint32_t intValue = 0;
	std::cout << "Coincidence: ";
	scanf ("%d",&intValue);
	std::cout << std::endl;

	if(intValue >= 5){
		intValue = 5;
	}
	std::cout << "intValue " << intValue << std::endl;

	myconfig[CONFIG_COINCIDENCE] = (int32_t)intValue;
	fpgaBoard->WriteConfiguration(mainConfig.configuration);

	json_config["configuration"] = mainConfig.configuration;

    std::ofstream o(json_config_name);
    o << std::setw(4) << json_config << std::endl;
    o.close();
}

void writeConfigurationTriggerType(){
	std::cout << std::endl;
	std::cout << "---------- TRIGGER TYPE ----------" << std::endl;
	std::cout << "1-TRIGGER_TYPE_NORMAL" << std::endl;
	std::cout << "2-TRIGGER_TYPE_SW" << std::endl;
	std::cout << "3-TRIGGER_TYPE_ENC" << std::endl;
	std::cout << "4-TRIGGER_TYPE_ROBOT" << std::endl;
	std::cout << "5-TRIGGER_TYPE_ROBOT_ENC" << std::endl;

	uint32_t intValue = 0;
	std::cout << "Trigger Type: ";
	scanf ("%d",&intValue);
	std::cout << std::endl;

	if(intValue > 5){
		intValue = 0;
	}

	if(intValue == 1){
		intValue = 0;
	}else if(intValue == 2){
		intValue = 10;
	}else if(intValue == 3){
		intValue = 20;
	}else if(intValue == 4){
		intValue = 30;
	}else if(intValue == 5){
		intValue = 40;
	}

	std::cout << "intValue " << intValue << std::endl;

	myconfig[CONFIG_TRIGGER_TYPE] = (int32_t)intValue;
	fpgaBoard->WriteConfiguration(mainConfig.configuration);

	json_config["configuration"] = mainConfig.configuration;

    std::ofstream o(json_config_name);
    o << std::setw(4) << json_config << std::endl;
    o.close();
}

void writeConfigurationIo(){
	uint32_t intValue = 0;
	std::cout << "I/O: ";
	scanf ("%d",&intValue);
	std::cout << std::endl;

	std::cout << "intValue " << (int32_t)intValue << std::endl;

	myconfig[CONFIG_IO] = intValue;
	fpgaBoard->WriteConfiguration(mainConfig.configuration);

	json_config["configuration"] = mainConfig.configuration;

    std::ofstream o(json_config_name);
    o << std::setw(4) << json_config << std::endl;
    o.close();
}

