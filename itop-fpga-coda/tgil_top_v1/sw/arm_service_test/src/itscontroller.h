#ifndef ITSCONTROLLER_H
#define ITSCONTROLLER_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <fcntl.h>
#include <iostream>
#include <unistd.h>
#include <cstring>
#include <sstream>

/* FPGA ITS GENERAL*/
#define ITS_MEM_LENGTH	512
#define ITS_CONTROL_REGISTERS_LENGTH	256
#define ITS_CONTROL_REGISTERS_OFFSET	0
#define ITS_STATUS_REGISTERS_LENGTH	256
#define ITS_STATUS_REGISTERS_OFFSET 256


class ItsController
{
    public:

        uint32_t its_len;

        ItsController();
        virtual ~ItsController();

        uint32_t writeAllRegisters(uint32_t *buffer);
        uint32_t readAllRegisters(uint32_t *buffer);
        uint32_t writeRegisters(uint32_t *buffer, uint32_t offset, uint32_t n_data);
        uint32_t readRegisters(uint32_t *buffer, uint32_t offset, uint32_t n_data);
        uint32_t writeRegister(uint32_t *buffer, uint32_t offset);
        uint32_t readRegister(uint32_t *buffer, uint32_t offset);

    protected:

        std::string device;
        int device_file;
        std::stringstream ss;

    private:
};

#endif // ITSCONTROLLER_H
