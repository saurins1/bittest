#include "dmacontroller.h"

DmaController::DmaController()
{
    //ctor
    std::cout << "Init DmaController" << std::endl;
    //std::cout << "DmaController" << std::endl;
    this->device_tx = "/dev/loop_tx";
    this->device_rx = "/dev/loop_rx";

    this->tx_fd = open(device_tx.c_str(), O_WRONLY);
    //std::cout << "DmaController tx_fd" << std::endl;
    this->rx_fd = open(device_rx.c_str(), O_RDONLY);
    //std::cout << "DmaController rx_fd" << std::endl;
    if ( tx_fd < 0 || rx_fd < 0 )
    {
        //std::cout << "Error opening DMA" << std::endl;
        std::cout << "Error opening DMA" << std::endl;
    }
}

DmaController::~DmaController()
{
    //dtor
    close(this->tx_fd);
    close(this->rx_fd);
}

// Send packet to the FPGA
bool DmaController::writePacket(uint8_t *buffer, uint32_t packet_size){
    //std::cout << "Packet size written: " << packet_size << std::endl;
    // packet_size in bytes, packet_size in words = packet_size / 4
	if(write(tx_fd, buffer, packet_size) == packet_size){
		// true if packet_size = packets written
        return true;
    }else{
        return false;
    }
}

// Receive packet to the FPGA
bool DmaController::readPacket(uint8_t *buffer, uint32_t packet_size){
    //std::cout << "Packet size read: " << packet_size << std::endl;
    // packet_size in bytes, packet_size in words = packet_size / 4
    if(read(rx_fd, buffer, packet_size) == packet_size){
		// true if packet_size = packets read
        //std::cout << "readPacket : true" << std::endl;
        return true;
    }else{
        //std::cout << "readPacket : false" << std::endl;
        return false;
    }
}


