#include "itscontroller.h"

// Constructor
ItsController::ItsController()
{
    std::cout << "Init ItsController" << std::endl;
    this->device = "/dev/iTS";
    this->its_len = ITS_MEM_LENGTH;

    //Logger::info(ss << "Opening: ");

    this->device_file = open(this->device.c_str(), O_RDWR);
    if (device_file < 0){
        std::cout << "Error opening" << device << std::endl;
  	}
}

// Destructor
ItsController::~ItsController()
{
    //dtor
    close(this->device_file);
}

// Write all register bank
uint32_t ItsController::writeAllRegisters(uint32_t *buffer){
	// buffer = [offset data0 data1...datan]
	// offset = 0
    buffer[0] = 0;
	return write(this->device_file, buffer, this->its_len);
	// return n data written
}

// Read all register bank
uint32_t ItsController::readAllRegisters(uint32_t *buffer){
	// buffer = [offset]
	// offset = 0
    buffer[0] = 0;
	return read(this->device_file, buffer, this->its_len);
	// return n data read, buffer = [data0 data1...datan]
}

// Write registers
uint32_t ItsController::writeRegisters(uint32_t *buffer, uint32_t offset, uint32_t n_data){
	// buffer = [offset data0 data1...datan
	// offset = offset
    buffer[0] = offset;
	return write(this->device_file, buffer, n_data);
	// return n data written
}

// Read registers
uint32_t ItsController::readRegisters(uint32_t *buffer, uint32_t offset, uint32_t n_data){
	// buffer = [offset]
	// offset = offset
    buffer[0] = offset;
	return read(this->device_file, buffer, n_data);
	// return n data read, buffer = [data0 data1...datan]
}

// Write register
uint32_t ItsController::writeRegister(uint32_t *buffer, uint32_t offset){
	// buffer = [offset data 0]
	// offset = offset
    buffer[0] = offset;
	return write(this->device_file, buffer, 1);
	// return n data written
}

// Read register
uint32_t ItsController::readRegister(uint32_t *buffer, uint32_t offset){
	// buffer = [offset]
	// offset = offset
    buffer[0] = offset;
	read(this->device_file, buffer, 1);
	return buffer[1];
	// return data0, buffer = [data0]
}

