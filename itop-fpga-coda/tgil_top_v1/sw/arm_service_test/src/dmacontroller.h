#ifndef DMACONTROLLER_H
#define DMACONTROLLER_H

#ifndef _WIN32
#include <sys/ioctl.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <fcntl.h>
#include <iostream>
#include <sstream>
#include <unistd.h>

class DmaController
{
    public:

        DmaController();
        virtual ~DmaController();

        bool writePacket(uint8_t *buffer, uint32_t packet_size);
        bool readPacket(uint8_t *buffer, uint32_t packet_size);

    protected:
        std::string device_rx;
        std::string device_tx;
        int tx_fd ;
        int rx_fd  ;

    private:
};

#endif // DMACONTROLLER_H
