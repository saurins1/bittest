#ifndef FPGABOARD_H
#define FPGABOARD_H


#include <iostream>
#include <math.h>
#include <mutex>
#include <thread>
#include <vector>
#include <queue>

#include "defsarm.h"
#include "fpgacontroller.h"
#include "dmacontroller.h"

class FpgaBoard
{

    // struct heart beat
    struct HeartBeatStruct {
        bool HB_value = false;
        bool HB_state = true;
        std::chrono::steady_clock::time_point hbTimeStart = std::chrono::steady_clock::now();
        std::chrono::steady_clock::time_point hbTimeEnd = std::chrono::steady_clock::now();
    };

    // NonAcquisition struct
    struct NonAcquisitionStruct {
        std::chrono::steady_clock::time_point naTimeStart = std::chrono::steady_clock::now();
        std::chrono::steady_clock::time_point naTimeEnd = std::chrono::steady_clock::now();
    };

    // struct acquisition
    struct AcquisitionStruct {
        bool running_acquisition = false;
        bool executeStartAcquisition = false;
        bool startAcquisitionExecuted = false;
        bool startAcquisitionResult = false;
        bool executeStopAcquisition = false;
        bool stopAcquisitionExecuted = false;
        bool stopAcquisitionResult = false;
        uint32_t len_word = 0;
        uint32_t len_dma = 0;
        uint8_t *scan_data_8 = nullptr;
        uint32_t *scan_data_32 = nullptr;
        uint16_t *scan_data_16 = nullptr;
    };

    // struct Configuration
    struct ConfigurationStruct {
        bool executeWriteConfig = false;
        bool writeConfigExecuted = false;
        bool writeConfigResult= false;
        uint32_t len_word = 0;
        uint32_t len_dma = 0;
        uint8_t *config_8 = nullptr;
        uint32_t *config = nullptr;
        uint32_t offset_fpga = 0;
        uint32_t len_config = 0;
        uint32_t offset_config = 0;
    };

    // struct Capabilities
    struct CapabilitiesStruct {
        uint32_t len_word = 0;
        uint32_t len_dma = 0;
        uint32_t actual_len_dma = 0;
        uint8_t *capabilities_data_8 = nullptr;
        int32_t *capabilities_data_32 = nullptr;
    };

    // struct acquisition
    struct OperationModeStruct {
        bool executeSetOperationMode = false;
        bool setOperationModeExecuted = false;
        bool setOperationModeResult= false;
        uint32_t operationModeAux;
    };

    // General ops
    struct GeneralOperationStruct {
        bool executeEnable = false;
        bool enableExecuted = false;
        bool enableResult= false;
        bool executeDisable = false;
        bool disableExecuted = false;
        bool disableResult= false;
        bool executeReset = false;
        bool resetExecuted = false;
        bool resetResult= false;
        bool executeTriggerSw = false;
        bool triggerSwExecuted= false;
        bool triggerSwResult= false;
    };

public:

    FpgaBoard();

    ~FpgaBoard();

    void Initialize();

    bool StartAcquisition();

    bool StopAcquisition();

    bool ReadInfo();

    bool Reset();

    bool OperationMode(uint32_t mode);

    bool WriteConfiguration(std::vector<int32_t> configuration);

    bool ReadConfiguration(PayloadMsgPb *payload);

    void Acquisition_worker_task();

    void Start_acquisition_worker();

    void Stop_acquisition_worker();

    bool TriggerSw(int32_t channel_id);

    bool PopulateCapability();

private:

    // FPGA driver instances
    FpgaController *fpgaController = nullptr;		// FPGA controller (write/read control/status registers of ITS)
    DmaController *dmaController = nullptr;        // DMA controller (write/read packets to/from FPGA through DMA)

    bool EnableFPGA();
    bool DisableFPGA();
    bool verifyOperation(bool *toVerify);

    /**** General Functionalities ****/
    void generalFunctionalities();

    /**** Heart Beat ****/
    void heartBeat();
    HeartBeatStruct heartBeatStruct;

    /**** Acquisition ****/
    bool acquisition();
    void printAscan();
    AcquisitionStruct acquisitionStruct;
    bool checkScanReady();
    bool setStartAcquisition();
    bool setStopAcquisition();
    bool startAcq();
    bool stopAcq();

    /**** Configuration ****/
    bool setWriteConfiguration();
    ConfigurationStruct configurationStruct;
    void printConfiguration();
    bool validateConfiguration();
    std::vector<int32_t>     bufferConfigurationVector;

    /**** Configuration ****/
    CapabilitiesStruct capabilitiesStruct;

    /**** Operation Mode ****/
    bool setOperationMode();
    bool SetScanMode(uint32_t mode);
    OperationModeStruct operationModeStruct;
    uint32_t operationModeLocal;

    GeneralOperationStruct generalOperationStruct;

    /**** Enable FPGA ****/
    bool setEnableFPGA();

    /**** Disable FPGA ****/
    bool setDisableFPGA();

    /**** Reset FPGA ****/
    bool setResetFPGA();

    /**** NonAcquisition ****/
    void NonAcquisition();
    NonAcquisitionStruct nonAcquisitionStruct;
    uint32_t acquisition_count = 0;

    /**** Trigger SW ****/
    bool setTriggerSw();
    uint32_t trigger_sw_channel = 0;

};

#endif // FPGABOARD_H
