/*
 * Empty C++ Application
 */

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <cstring>
#include <iostream>
#include <sstream>
#include <fstream>
#include <unistd.h>
#include <pthread.h>
#include <chrono>
#include <thread>
#include <vector>
#include <queue>
#include "fpgaboard.h"
#include "json.hpp"

using namespace nlohmann;

FpgaBoard *fpgaBoard = nullptr;

// struct Main Config
struct MainConfig {
	std::vector<int32_t> configuration;
};

MainConfig mainConfig;
int32_t *myconfig = nullptr;

json json_config;
std::string json_config_name;

void defaultConfig();
void mainMenu();
void operationModeMenu();
void operationMode();
void configurationMenu();
void writeConfiguration();

void writeConfigurationSampleFrequency();
void writeConfigurationDataWindow();
void writeConfigurationReceiverDelay();
void writeConfigurationAnalogGain();
void writeConfigurationDigitalGain();
void writeConfigurationBurstFrequency();
void writeConfigurationNumberCycles();
void writeConfigurationPrf();
void writeConfigurationTransmitterDelay();
void writeConfigurationDeltaDelay();
void writeConfigurationDeltaAdd();
void writeConfigurationAverage();
void writeConfigurationCoincidence();
void writeConfigurationTriggerType();
void writeConfigurationIo();

int main()
{
	fpgaBoard = new FpgaBoard();

	json_config_name.assign("configuration.json");

	// Read capabilities
	std::this_thread::sleep_for(std::chrono::milliseconds(50));
	fpgaBoard->ReadInfo();

	// Default config
	defaultConfig();
	myconfig = (int32_t *) &mainConfig.configuration[0];

	int option = 1;
	while(option){
		mainMenu();
		scanf ("%d",&option);
		switch(option){
			case 0:

				break;
			case 1:
				fpgaBoard->StartAcquisition();
				break;
			case 2:
				fpgaBoard->StopAcquisition();
				break;
			case 3:
				fpgaBoard->ReadInfo();
				break;
			case 4:
				fpgaBoard->Reset();
				break;
			case 5:
				operationMode();
				break;
			case 6:
				writeConfiguration();
				break;
			case 7:
				fpgaBoard->ReadConfiguration();
				break;
			case 8:
				fpgaBoard->TriggerSw(1);
				break;
		}
	}

	if(fpgaBoard != nullptr){
		std::cout << "delete fpgaBoard" << std::endl;
		delete fpgaBoard;
		fpgaBoard = nullptr;
	}

    if( mainConfig.configuration.size() > 0 ){
    	mainConfig.configuration.clear();
    }

	return 0;
}

void defaultConfig(){
	std::cout << "Main, defaultConfig" << std::endl;

    // read a JSON file
    std::ifstream i(json_config_name);
    i >> json_config;
    i.close();

    mainConfig.configuration = std::move(json_config["json_config"].get<std::vector<int32_t>>());

    // Write config
    std::this_thread::sleep_for(std::chrono::milliseconds(50));
    fpgaBoard->WriteConfiguration(mainConfig.configuration);
}


void mainMenu(){
	std::cout << std::endl;
	std::cout << "---------- SELECT OPTION ----------" << std::endl;
	std::cout << "1-Start Inspection" << std::endl;
	std::cout << "2-Stop Inspection" << std::endl;
	std::cout << "3-Read Capabilities" << std::endl;
	std::cout << "4-Reset" << std::endl;
	std::cout << "5-Configure Operation Mode" << std::endl;
	std::cout << "6-Write Configuration" << std::endl;
	std::cout << "7-Read Configuration" << std::endl;
	std::cout << "8-Trigger SW " << std::endl << std::endl;
	std::cout << "0-Exit " << std::endl << std::endl;
}

void operationModeMenu(){
	std::cout << std::endl;
	std::cout << "---------- OPERATION MODE ----------" << std::endl;
	std::cout << "1-MODE_ASCAN" << std::endl;
	std::cout << "2-MODE_IDLE" << std::endl;
	std::cout << "3-MODE_MRUT" << std::endl;
	std::cout << "0-Exit" << std::endl;
}

void operationMode(){
	int option = 1;
	while(option){
		operationModeMenu();
		scanf ("%d",&option);
		switch(option){
			case 1:
				fpgaBoard->OperationMode(MODE_ASCAN);
				option = 0;
				break;
			case 2:
				fpgaBoard->OperationMode(MODE_IDLE);
				option = 0;
				break;
			case 3:
				fpgaBoard->OperationMode(MODE_MRUT);
				option = 0;
				break;
		}
	}
}

void configurationMenu(){
	std::cout << std::endl;
	std::cout << "---------- WRITE CONFIGURATION ----------" << std::endl;
	std::cout << std::endl;
	std::cout << "1-Write Configuration" << std::endl;
	std::cout << std::endl;
	std::cout << "-- RECEIVER --" << std::endl;
	std::cout << std::endl;
	std::cout << "10-Sampling Frequency [Mhz]" << std::endl;
	std::cout << "11-Data Window [us]" << std::endl;
	std::cout << "12-Receiver Delay [ns]" << std::endl;
	std::cout << "13-Analog Gain [db]" << std::endl;
	std::cout << "14-Digital Gain [db]" << std::endl;
	std::cout << std::endl;
	std::cout << "-- TRANSMITTER --" << std::endl;
	std::cout << std::endl;
	std::cout << "20-Burst Frequency [hz]" << std::endl;
	std::cout << "21-N Cycles" << std::endl;
	std::cout << "22-PRF [hz]" << std::endl;
	std::cout << "23-Transmitter delay [ns]" << std::endl;
	std::cout << "24-Delta delay [ns]" << std::endl;
	std::cout << "25-Delta add [ns]" << std::endl;
	std::cout << std::endl;
	std::cout << "-- DSP --" << std::endl;
	std::cout << std::endl;
	std::cout << "30-Average" << std::endl;
	std::cout << "31-Coincidence" << std::endl;
	std::cout << std::endl;
	std::cout << "-- TRIGGER TYPE --" << std::endl;
	std::cout << std::endl;
	std::cout << "40-trigger type" << std::endl;
	std::cout << std::endl;
	std::cout << "-- I/O --" << std::endl;
	std::cout << std::endl;
	std::cout << "50-I/O" << std::endl;
	std::cout << std::endl;
	std::cout << "0-Exit" << std::endl;
}

void writeConfiguration(){
	int option = 1;
	while(option){
		configurationMenu();
		scanf ("%d",&option);
		switch(option){
			case 1:
				fpgaBoard->WriteConfiguration(mainConfig.configuration);
				break;
			case 10:
				writeConfigurationSampleFrequency();
				break;
			case 11:
				writeConfigurationDataWindow();
				break;
			case 12:
				writeConfigurationReceiverDelay();
				break;
			case 13:
				writeConfigurationAnalogGain();
				break;
			case 14:
				writeConfigurationDigitalGain();
				break;
			case 20:
				writeConfigurationBurstFrequency();
				break;
			case 21:
				writeConfigurationNumberCycles();
				break;
			case 22:
				writeConfigurationPrf();
				break;
			case 23:
				writeConfigurationTransmitterDelay();
				break;
			case 24:
				writeConfigurationDeltaDelay();
				break;
			case 25:
				writeConfigurationDeltaAdd();
				break;
			case 30:
				writeConfigurationAverage();
				break;
			case 31:
				writeConfigurationCoincidence();
				break;
			case 40:
				writeConfigurationTriggerType();
				break;
			case 50:
				writeConfigurationIo();
				break;
		}
	}
}

void writeConfigurationSampleFrequency(){
	uint32_t intValue = 0;
	std::cout << "Sample Frequency: ";
	scanf ("%d",&intValue);
	std::cout << std::endl;

	if(intValue >= 100){
		intValue = 100;
	}
	intValue = 100 / intValue;
	std::cout << "intValue" << intValue << std::endl;

	myconfig[CONFIG_SAMPLE_FREQUENCY] = intValue;
	fpgaBoard->WriteConfiguration(mainConfig.configuration);

	json_config["json_config"] = mainConfig.configuration;

    std::ofstream o(json_config_name);
    o << std::setw(4) << json_config << std::endl;
    o.close();
}

void writeConfigurationDataWindow(){

}

void writeConfigurationReceiverDelay(){

}

void writeConfigurationAnalogGain(){

}

void writeConfigurationDigitalGain(){

}

void writeConfigurationBurstFrequency(){

}

void writeConfigurationNumberCycles(){

}

void writeConfigurationPrf(){

}

void writeConfigurationTransmitterDelay(){

}

void writeConfigurationDeltaDelay(){

}

void writeConfigurationDeltaAdd(){

}

void writeConfigurationAverage(){

}

void writeConfigurationCoincidence(){

}

void writeConfigurationTriggerType(){

}

void writeConfigurationIo(){

}

