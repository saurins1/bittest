----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 01/09/2017 10:07:23 AM
-- Design Name: 
-- Module Name: ADC controller - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity adc_controller is
	generic (
	    REGISTER_W32    : INTEGER := 32;
	    REGISTER_W16    : INTEGER := 16;
	    REGISTER_W14    : INTEGER := 14;
	    REGISTER_W8     : INTEGER := 8;
	    REGISTER_W7     : INTEGER := 7;
	    REGISTER_W4     : INTEGER := 4;
	    CONFIG_DONE_WAIT: INTEGER := 4;
		SAMPLE_FREQ     : INTEGER := 100;
	    N_CONFIG_REGISTERS : INTEGER := 5;
		SPI_WAIT_CYCLES : INTEGER := 128;
		SPI_2X_CLK_DIV  : INTEGER := 100       
	);
	port (
	    -- Sync
	    aclk_100     : in STD_LOGIC;
	    aclk_200     : in STD_LOGIC;
        aresetn      : in STD_LOGIC;
	        
        -- Programing mode seletion
        ADC_PAR_SERn : out STD_LOGIC;
        
        -- encode (type of edge for conversion starts)
        ADC_ENCp     : out STD_LOGIC;
        ADC_ENCn     : out STD_LOGIC;
        
        -- Data output clock
        ADC_CLKOUTp  : in STD_LOGIC;
        ADC_CLKOUTn  : in STD_LOGIC;
        
        -- Overflow
        ADC_OFp      : in STD_LOGIC;
        ADC_OFn      : in STD_LOGIC;
        
        ---- Data in ----
        ADC_DATAp    : in STD_LOGIC_VECTOR(REGISTER_W7-1 downto 0);
        ADC_DATAn    : in STD_LOGIC_VECTOR(REGISTER_W7-1 downto 0);
        
        -- Data acquired
        ADC_DATA        : out STD_LOGIC_VECTOR(REGISTER_W14-1 downto 0);
        
        ---- Extrnal config  ----		
		ADC_CONFIG_EX     		: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		ADC_CONFIG_EX_VALID     : in STD_LOGIC;
		ADC_CONFIG_LOADED 		: out STD_LOGIC;
		ADC_CONFIG_CURRENT     	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		ADC_CONFIG_REQUEST		: out STD_LOGIC;
		ADC_CONFIG_REQUEST_ACK	: in STD_LOGIC;
		
		ADC_CONFIG_EX_DONE		: out STD_LOGIC;
		
		---- SPI ----
		ADC_SPI_CONFIG : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0); -- debug
		
        ADC_CSn      : out STD_LOGIC;
        ADC_SCK      : out STD_LOGIC;
        ADC_SDI      : out STD_LOGIC;
        ADC_SDO      : in STD_LOGIC
        
	);
end adc_controller;

architecture arch_imp of adc_controller is

    ---------------------    ADC CONFIG    --------------------- 	
    
	-- SPI MASTER
	COMPONENT spi_master
      GENERIC (   
          N : positive := 32;
          CPOL : STD_LOGIC := '0';
          CPHA : STD_LOGIC := '0';
          PREFETCH : positive := 2;
          SPI_2X_CLK_DIV : positive := 5);
      PORT (  
          sclk_i : in STD_LOGIC := 'X';
          pclk_i : in STD_LOGIC := 'X';
          rst_i : in STD_LOGIC := 'X';
          spi_ssel_o : out STD_LOGIC; -- csn
          spi_sck_o : out STD_LOGIC; -- sck
          spi_mosi_o : out STD_LOGIC; -- sdi
          spi_miso_i : in STD_LOGIC := 'X'; --sdo
          di_req_o : out STD_LOGIC;
          di_i : in  STD_LOGIC_VECTOR (N-1 downto 0) := (others => 'X');
          wren_i : in STD_LOGIC := 'X';
          wr_ack_o : out STD_LOGIC;
          do_valid_o : out STD_LOGIC;
          do_o : out  STD_LOGIC_VECTOR (N-1 downto 0);
          done_o : out STD_LOGIC
    );                      
   	END COMPONENT;

    signal spi_master_rst_i : STD_LOGIC;
	signal spi_master_wren_i : STD_LOGIC;
	signal spi_master_wr_ack_o : STD_LOGIC;
	signal spi_master_do_valid_o : STD_LOGIC;
	signal spi_master_done_o : STD_LOGIC;
	
	---- SPI ----
	signal ADC_CSn_i   : STD_LOGIC;
	signal ADC_SCK_i   : STD_LOGIC;
	signal ADC_SDI_i   : STD_LOGIC;
	signal ADC_SDO_i   : STD_LOGIC;
	
	signal ADC_PAR_SERn_i   : STD_LOGIC;
	
    -- ADC configuration
	signal adc_config_in    : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal adc_config_out   : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);

	signal adc_config_in_op    : STD_LOGIC;
    signal adc_config_in_addr  : STD_LOGIC_VECTOR(REGISTER_W8-2 downto 0);
    signal adc_config_in_data  : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
	signal adc_config_out_data : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
    
    type adc_config_registers_array is array ( 0 to N_CONFIG_REGISTERS-1 ) of STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
    signal adc_config_registers : adc_config_registers_array;
   
    -- Configuration R0 (reset)
    signal adc_reset : STD_LOGIC;
    signal adc_reset_on : STD_LOGIC;
    
    -- Configuration R1 (power-down)
    signal adc_sleep : STD_LOGIC;   
    signal adc_nap : STD_LOGIC; 
    
    -- Configuration R2 (timing)
    signal adc_clkinv : STD_LOGIC;   
    signal adc_clkphase : STD_LOGIC_VECTOR(1 downto 0); 
    signal adc_dcs : STD_LOGIC;
    
    -- Configuration R3 (output mode)
    signal adc_ilvds : STD_LOGIC_VECTOR(2 downto 0); 
    signal adc_termon : STD_LOGIC; 
    signal adc_outoff : STD_LOGIC; 
    
    -- Configuration R4 (data format)
    signal adc_outtest : STD_LOGIC_VECTOR(2 downto 0); 
    signal adc_abp : STD_LOGIC; 
    signal adc_dteston : STD_LOGIC;
    signal adc_drand : STD_LOGIC;
    signal adc_twoscomp : STD_LOGIC;
        		
    -- Load ADC config state machine
    type states_adc_config_sm is (idle, write_register, write_register_end, write_register_wait, update_register, update_config_end); 
    signal state_adc_config_sm : states_adc_config_sm;
    
    signal adc_update_config_start : STD_LOGIC;
    signal adc_update_config_end : STD_LOGIC;
	signal adc_config_loaded_i : STD_LOGIC;

	signal adc_config_ex_i : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  
    ---------------------    ADC DATA IN    ---------------------
    signal adc_data_diff : STD_LOGIC_VECTOR(REGISTER_W7-1 downto 0);
    signal adc_data_diffp: STD_LOGIC_VECTOR(REGISTER_W7-1 downto 0);
    signal adc_data_diffn: STD_LOGIC_VECTOR(REGISTER_W7-1 downto 0);

	---------------------    ADC DATA    ---------------------
	signal adc_data_i : STD_LOGIC_VECTOR(REGISTER_W14-1 downto 0);

	---------------------    ADC ENC    --------------------- 	
	signal adc_encs_i 	: STD_LOGIC;
	signal adc_encs 	: STD_LOGIC;
	signal adc_enc_en   : STD_LOGIC;
	signal adc_enc_en_i : STD_LOGIC;
	
	---------------------    ADC CLKOUT    ---------------------
	signal adc_clkout_diff : STD_LOGIC;
    signal adc_clkout_s : STD_LOGIC;
    signal adc_clkout_en : STD_LOGIC;
        
begin

	---------------------    ADC ENC    ---------------------
	
	-- Encode Ouput. Generate differential LVDS signal from selected clock source (sample freq) 
    adc_enc_inst  : OBUFDS 
		generic map (
        	IOSTANDARD => "LVDS_25"
        )
       	port map ( 
         	O => ADC_ENCp, 
            OB => ADC_ENCn, 
            I => adc_encs
       );
                      
    ADC_ENC_OODR_inst : ODDR
        generic map(
            DDR_CLK_EDGE    => "OPPOSITE_EDGE", -- "OPPOSITE_EDGE" or "SAME_EDGE"
            INIT            => '0',     	-- Sets initial state of the Q output to '0' or '1'
            SRTYPE          => "SYNC")     	-- Specifies "SYNC" or "ASYNC" set/reset
        port map (
            Q      => adc_encs,     -- 1-bit output data
            C      => adc_encs_i,   -- 1-bit clock input
            --CE     => adc_enc_en,   -- 1-bit clock enable input
            CE     => '1',   -- 1-bit clock enable input
            D1     => '1',          -- 1-bit data input (positive edge)
            D2     => '0',          -- 1-bit data input (negative edge)
            R      => '0',          -- 1-bit reset input
            S      => '0'           -- 1-bit set input
        );                        
        
	adc_encs_i <= aclk_200 when SAMPLE_FREQ = 200 else
				  aclk_100;
		
    adc_enc_en <= adc_config_loaded_i and adc_enc_en_i;
    adc_enc_en_i <= '1';
		   
	
	---------------------    ADC CLKOUT    --------------------- 
	
	ADC_CLKOUT_IBUFGDS_inst : IBUFGDS 
	    generic map (
	       IOSTANDARD => "LVDS_25", 
	       DIFF_TERM => TRUE
	    )
        port map (
            I => ADC_CLKOUTp, 
            IB => ADC_CLKOUTn, 
            O => adc_clkout_diff
        );
        
    ADC_CLKOUT_BUFIO_inst  : BUFIO
        port map ( 
            I => adc_clkout_diff, 
            O=> adc_clkout_s 
        );  
		
    -- Differential ADC_DATA LDVS_25 input
    adc_data_diff_gen: for i in 0 to REGISTER_W7-1 generate
       adc_data_diff_inst : IBUFDS 
            generic map (
                IOSTANDARD => "LVDS_25", 
                DIFF_TERM => TRUE)
            port map (
                I => ADC_DATAp(i), 
                IB => ADC_DATAn(i), 
                O => adc_data_diff(i)
            );
    end generate;
    
	-- Syncronize adc_data_diff with adc_clkout_s
    ADC_ENC_IDDR_gen: for i in 0 to REGISTER_W7-1 generate        
		ADC_ENC_IDDR_inst : IDDR
			generic map(
				DDR_CLK_EDGE    => "OPPOSITE_EDGE", -- "OPPOSITE_EDGE" or "SAME_EDGE"
				INIT_Q1         => '0',         -- Sets initial state of the Q output to '0' or '1'
				INIT_Q2         => '0',         -- Sets initial state of the Q output to '0' or '1'
				SRTYPE          => "SYNC")         -- Specifies "SYNC" or "ASYNC" set/reset
			port map (
				Q1     => adc_data_diffp(i),-- 1-bit output data
				Q2     => adc_data_diffn(i),-- 1-bit clock input
				CE     => adc_clkout_en,	-- 1-bit clock enable input
				D      => adc_data_diff(i), -- 1-bit data input (positive edge)
				C      => adc_clkout_s, 	-- 1-bit reset input
				R      => '0',          	-- 1-bit reset input
				S      => '0'           	-- 1-bit set input
			); 
	end generate;
        
    adc_clkout_en <= '1';
    
	---------------------    ADC DATA    ---------------------
      		
	-- ADC data compose
    adc_data_compose : for i in 0 to REGISTER_W7-1 generate
        process(adc_clkout_s) 
        begin
           if (rising_edge(adc_clkout_s)) then
                if (aresetn = '0') then
                    adc_data_i(i*2) <= '0';
                    adc_data_i(i*2+1) <= '0';
                else
                    -- Even bits
					adc_data_i(i*2) <= adc_data_diffp(i);
					-- Odd bits
					adc_data_i(i*2+1) <= adc_data_diffn(i);
                end if;
           end if;
        end process;
    end generate;
			  
	-- Syncronize ADC_DATA with our clock (aclk_100 or aclk_200)
	process(adc_encs_i) 
    begin
    	if (rising_edge(adc_encs_i)) then
        	if (aresetn = '0') then
            	ADC_DATA <= (others => '0');
        	else
         		ADC_DATA <= adc_data_i;
         	end if;
       	end if;
    end process;
		
	---------------------    ADC CONFIG    --------------------- 			   
	-- Programming Mode Selection Pin, 0 = serial programming	   
	ADC_PAR_SERn_i <= '0';
	
	ADC_PAR_SERn_inst   : OBUF 
        generic map (
            IOSTANDARD=>"LVCMOS25", 
            DRIVE=>8, 
            SLEW=>"SLOW"
        )
        port map ( 
            I => ADC_PAR_SERn_i, 
            O=> ADC_PAR_SERn 
        );
	              								
	-- State machine for ADC configuration 
	-- Load (write & read) all the registers (1 to 5) after adc_update_config_start = '1'
	-- Write reset into ADC (register 0) after SW reset + default values to the registers (1 to 5)
    adc_config_sm: process(aclk_100, aresetn) 
		variable wait_counter : integer range 0 to 16383 :=0;
		variable write_counter : integer range 0 to 16383 :=0;	
		variable read_counter : integer range 0 to 16383 :=0;	
    begin 
        if (rising_edge(aclk_100)) then
            if (aresetn = '0') then
                -- ADC config
				adc_config_loaded_i  <= '0';
				adc_config_in_op   <= '0';
				adc_config_in_addr <= (others => '0');
				adc_config_in_data <= (others => '0');
				-- SPI port control
				spi_master_wren_i  <= '0';
				-- Flags
				adc_update_config_end <= '0';
				ADC_CONFIG_EX_DONE	<= '0';		
				-- state
				adc_reset_on <= '1';
				state_adc_config_sm <= idle;
        	else														 
				case state_adc_config_sm is
					when idle =>
						write_counter:= 0;
                        read_counter := 0;
						adc_config_in_op <= '0'; -- ADC write command
					    if(adc_reset_on = '1') then
					        adc_config_in_addr <= std_logic_vector(to_unsigned(0, adc_config_in_addr'length));
					        adc_config_in_data <= adc_config_registers(0);
                            state_adc_config_sm <= write_register;
					    elsif(adc_update_config_start = '1') then
							adc_config_in_addr <= std_logic_vector(to_unsigned(1, adc_config_in_addr'length));
							adc_config_in_data <= adc_config_registers(1);
							state_adc_config_sm <= write_register;	
                        end if; 
					-- Write order
					when write_register =>
						-- SPI master
			  			spi_master_wren_i <= '1';
						state_adc_config_sm <= write_register_end;	
					-- Wait for WR ack
			  		when write_register_end =>
						if(spi_master_wr_ack_o = '1') then
							spi_master_wren_i <= '0';
							wait_counter := 0;
							state_adc_config_sm <= write_register_wait;	
						end if;		
					-- Wait between SPI write orders
					when write_register_wait =>
				    	if(spi_master_done_o = '1') then							  
                        	if(wait_counter >= SPI_WAIT_CYCLES) then
                            	state_adc_config_sm <= update_register; 
                        	else
                            	wait_counter := wait_counter + 1;
                        	end if;	
                    	end if;	
					-- Update register to write
					when update_register =>
					    if(adc_reset_on = '1') then
					    	if(adc_config_in_op = '0') then
					           	adc_config_in_op <= '1';
					       	else
								-- Reset finished 											   	
					           	if(adc_config_out_data(7) = '0') then
					               	adc_reset_on <= '0';
									adc_config_in_op <= '0';
									-- Load default config															   
									adc_config_in_addr <= std_logic_vector(to_unsigned(1, adc_config_in_addr'length));
									adc_config_in_data <= adc_config_registers(1);
					           	end if;
					       	end if;
							state_adc_config_sm <= write_register;	
					    else
							if(adc_config_in_op = '1') then
								if(adc_config_in_data = adc_config_out_data) then
									read_counter := read_counter + 1;	
								end if;
								if(write_counter >= N_CONFIG_REGISTERS-1) then
									wait_counter := 0;													   
									state_adc_config_sm <= update_config_end;	
								else
									-- Update register to write
									adc_config_in_addr <= std_logic_vector(unsigned(adc_config_in_addr)+1);
									adc_config_in_data <= adc_config_registers(write_counter+1);
									state_adc_config_sm <= write_register;
								end if;
							else
								write_counter := write_counter + 1;
								state_adc_config_sm <= write_register;	
							end if;
							-- switch ADC register order (write to read, read to write)
							adc_config_in_op <= not adc_config_in_op;	
					    end if;
					-- End
					when update_config_end =>
						ADC_CONFIG_EX_DONE	<= '1';
						if(wait_counter >= CONFIG_DONE_WAIT) then															   
							adc_update_config_end <= '1';
							if(read_counter = N_CONFIG_REGISTERS) then
								adc_config_loaded_i <= '1';	
							end if;
							adc_config_loaded_i <= '1';	
							if(adc_update_config_start = '0') then
								adc_update_config_end <= '0';
								ADC_CONFIG_EX_DONE	<= '0';	
								state_adc_config_sm <= idle;															   
							end if;
						else
							wait_counter := wait_counter + 1;															   
						end if;																  
					when others =>
						null;
				end case;
			end if;
         end if;
    end process;
																					   
	-- Registers configuration
    adc_config_registers(0) <= adc_reset & "0000000";
    adc_config_registers(1) <= "0000" & adc_sleep & adc_nap & "00";
    adc_config_registers(2) <= "0000" & adc_clkinv & adc_clkphase & adc_dcs;
    adc_config_registers(3) <= "000" & adc_ilvds & adc_termon & adc_outoff;
    adc_config_registers(4) <= adc_outtest & adc_abp & "0" & adc_dteston & adc_drand & adc_twoscomp;
																					   
	ADC_CONFIG_CURRENT <= adc_config_ex_i;
	adc_config_ex_i <= adc_config_registers(4) & adc_config_registers(3) & adc_config_registers(2) & adc_config_registers(1); 

	-- Word to write by SPi to the ADC = op(read/writen) + register add + register value
	adc_config_in <= adc_config_in_op & adc_config_in_addr & adc_config_in_data;

	-- Register readed from ADC
	adc_config_out_data <= adc_config_out(7 downto 0);
	ADC_SPI_CONFIG <= adc_config_in_data;																					   

	-- ADC_CONFIG_LOADED flag
	ADC_CONFIG_LOADED <= adc_config_loaded_i;																		   
    
    -- Registers parameters																				   
    adc_config_parameters_inst: process(aclk_100, aresetn) 
    begin 
		if (rising_edge(aclk_100)) then
            if (aresetn = '0') then
                -- Default parameters
                -- R0
                adc_reset <= '1';
                -- R1
                adc_sleep <= '0';
                adc_nap   <= '0';
                -- R2
                adc_clkinv   <= '0';  -- normal clkout polarity
                adc_clkphase <= "01"; -- 45degree dely (1/8 T)
                adc_dcs      <= '1';  -- clock duty cycle stabilizer ON
                -- R3
                adc_ilvds <= "000"; -- 3.5ma LVDS Output current
--                adc_ilvds <= "001"; -- 4.0ma LVDS Output current
--                adc_ilvds <= "010"; -- 4.5ma LVDS Output current
--                adc_ilvds <= "011"; -- Not used
--                adc_ilvds <= "100"; -- 3.0ma LVDS Output current
--                adc_ilvds <= "101"; -- 2.5ma LVDS Output current
--                adc_ilvds <= "110"; -- 2.1ma LVDS Output current
--                adc_ilvds <= "111"; -- 1.75ma LVDS Output current
                adc_termon <= '0'; -- Internal termination OFF
                adc_outoff <= '0'; -- LVDS DDR = 0 LVDS Tristate = 1
                -- R4
--                adc_outtest <= "000"; -- Digital Output Test Pattern Bits. All Digital Outputs = 0
--                adc_outtest <= "001"; -- All Digital Outputs = 1 
--                adc_outtest <= "010"; -- Alternating Output Pattern. OF, D13-D0 alternate between 000 0000 0000 0000 and 111 1111 1111 1111
                adc_outtest  <= "100"; -- Checkerboard Output Pattern. OF, D13-D0 alternate between 010 1010 1010 1010 and 101 0101 0101 0101
                adc_abp      <= '0'; -- Alternate Bit Polarity Mode
                adc_dteston  <= '0'; -- Enable the Digital Output Test Patterns
                adc_drand    <= '0'; -- Data Output Randomizer Mode
                adc_twoscomp <= '1'; -- 0 = Offset Binary Data Format, 1 = Two's Complement Data Format                                                                             
            else    
            	if(ADC_CONFIG_REQUEST_ACK = '1') then
					-- R1																		   	
					adc_sleep <= ADC_CONFIG_EX(3);
					adc_nap <= ADC_CONFIG_EX(2);	
					-- R2	
					adc_dcs <= ADC_CONFIG_EX(8); 	
					adc_clkphase <= ADC_CONFIG_EX(10 downto 9);	
					adc_dcs <= ADC_CONFIG_EX(11); 	
					-- R3
					adc_outoff <= ADC_CONFIG_EX(16);
					adc_termon <= ADC_CONFIG_EX(17);	
					adc_ilvds <= ADC_CONFIG_EX(20 downto 18);																	   
					-- R4
					adc_twoscomp <= ADC_CONFIG_EX(24);
					adc_drand <= ADC_CONFIG_EX(25);
					adc_dteston <= ADC_CONFIG_EX(26);
					adc_abp <= ADC_CONFIG_EX(28);
					adc_outtest <= ADC_CONFIG_EX(31 downto 29);																   
				end if;																		   
            end if;
        end if;
    end process;
					
	-- External config control																				   
    adc_config_control_inst: process(aclk_100, aresetn) 
    begin 
        if (rising_edge(aclk_100)) then
            if (aresetn = '0') then
				ADC_CONFIG_REQUEST <= '0';																		   
				adc_update_config_start <= '0';																			   
        	else		
				if(ADC_CONFIG_EX_VALID = '1') then																		   
					if(adc_config_ex_i /= ADC_CONFIG_EX) then
						ADC_CONFIG_REQUEST <= '1';																	   
					end if;
				end if;
				if(ADC_CONFIG_REQUEST_ACK = '1') then
					ADC_CONFIG_REQUEST <= '0';
					adc_update_config_start <= '1';																		   
				end if;		
				if(adc_update_config_end = '1' and ADC_CONFIG_REQUEST_ACK = '0') then
					adc_update_config_start <= '0';																			   
				end if;																			   
			end if;
		end if;
	end process;
			 	
	-- SPI MASTER			 			 
	spi_master_inst: spi_master
      GENERIC MAP(   
          N => REGISTER_W16,
          CPOL => '0',
          CPHA => '0',
          PREFETCH => 3,
          SPI_2X_CLK_DIV => SPI_2X_CLK_DIV
      )
      PORT MAP(  
          sclk_i => aclk_100,
          pclk_i => aclk_100,
          rst_i => spi_master_rst_i,
          spi_ssel_o => ADC_CSn_i,
          spi_sck_o => ADC_SCK_i,
          spi_mosi_o => ADC_SDI_i,
          spi_miso_i => ADC_SDO_i,
          di_req_o => open,
          di_i => adc_config_in,
          wren_i => spi_master_wren_i,
          wr_ack_o => spi_master_wr_ack_o,
          do_valid_o => spi_master_do_valid_o,
          do_o => adc_config_out,
          done_o => spi_master_done_o
      ); 
      
    spi_master_rst_i <= not aresetn;	  
																					   
	ADC_CSn_inst   : OBUF 
		generic map (
			IOSTANDARD=>"LVCMOS25", 
			DRIVE=>8, 
			SLEW=>"SLOW"
		)
		port map ( 
			I => ADC_CSn_i, 
			O=> ADC_CSn 
		);
																					   
	ADC_SCK_inst   : OBUF 
		generic map (
			IOSTANDARD=>"LVCMOS25", 
			DRIVE=>8, 
			SLEW=>"SLOW"
		)
		port map ( 
			I => ADC_SCK_i, 
			O=> ADC_SCK 
		);																		   
      
	ADC_SDI_inst   : OBUF 
		generic map (
			IOSTANDARD=>"LVCMOS25", 
			DRIVE=>8, 
			SLEW=>"SLOW"
		)
		port map ( 
			I => ADC_SDI_i, 
			O=> ADC_SDI 
		); 

	ADC_SDO_inst   : IBUF 
		generic map (
			IOSTANDARD=>"LVCMOS25"
		)
		port map ( 
			I => ADC_SDO, 
			O=> ADC_SDO_i 
		);
																					   					     					      									 	   			                 
end arch_imp;
