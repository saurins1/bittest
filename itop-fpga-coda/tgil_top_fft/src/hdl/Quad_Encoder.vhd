library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Quad_Encoder is
	generic (
        FILTER_IN_LEVEL        : INTEGER := 200
    );
    Port (  aclk            : in STD_LOGIC; 
            areset          : in STD_LOGIC; 
            ENC_CHA         : in STD_LOGIC; 
            ENC_CHB        	: in STD_LOGIC; 
            ENC_SPEED_SEL   : in STD_LOGIC;                         -- Select speed calculation algorithm
            ENC_SPEED_AVG   : in STD_LOGIC_VECTOR (8 downto 0);     -- Average level of speed 
            ENC_DIR        	: out STD_LOGIC_VECTOR (1 downto 0);    -- Direction 00 unknown 01 positive 10 negative 11 error
            ENC_RAW_COUNT 	: out STD_LOGIC_VECTOR (31 downto 0);   -- Distance in pulses (ticks)
            ENC_SPEED       : out STD_LOGIC_VECTOR (31 downto 0)    -- Speed  
    );
end Quad_Encoder;

architecture arch_imp of Quad_Encoder is
   
    signal Enc_CHA_Filt     : std_logic := '0';
    signal Enc_CHB_Filt     : std_logic := '0';

    signal Enc_CHA_d1     : std_logic := '0';
    signal Enc_CHB_d1     : std_logic := '0';
    signal Enc_CHA_d2     : std_logic := '0';
    signal Enc_CHB_d2     : std_logic := '0';
    signal Enc_CHA_d3     : std_logic := '0';
    signal Enc_CHB_d3     : std_logic := '0';
    signal Enc_CHA_d4     : std_logic := '0';
    signal Enc_CHB_d4     : std_logic := '0';
    signal Enc_CHA_d5     : std_logic := '0';
    signal Enc_CHB_d5     : std_logic := '0';
    signal Enc_CHA_d6     : std_logic := '0';
    signal Enc_CHB_d6     : std_logic := '0';
    
    signal Enc_Pos_i        : signed(31 downto 0) := to_signed(0, 32);
    signal Enc_Dir_i        : std_logic_vector(1 downto 0) := "00";
	signal Enc_Dir_i_d1     : std_logic_vector(1 downto 0) := "00";
	signal Enc_Dir_Filt     : std_logic_vector(1 downto 0) := "00";
    
    type states_enc is (s0, s00, s01, s10, s11); 
    signal state_enc : states_enc; 
    
	-- -- Signals Speed
    signal speed_1        : unsigned(31 downto 0) := (others => '0');
    signal speed_1_cnt    : unsigned(31 downto 0) := (others => '0');

    signal speed_2        : unsigned(31 downto 0) := (others => '0');
    signal speed_2_cnt    : unsigned(31 downto 0) := (others => '0');
    signal speed_2_on     : std_logic := '0';
    
    signal Enc_Pos_i_d1 : signed(31 downto 0) := to_signed(0, 32);

    type states_enc_speed is (idle, measure_speed); 
    signal state_enc_speed : states_enc_speed; 

begin
	
	Enc_Dir_Filt <= "00" when ( Enc_Dir_i = "00" and Enc_Dir_i_d1 = "00" ) else
					"01" when ( Enc_Dir_i = "01" and Enc_Dir_i_d1 = "01" ) else
					"10" when ( Enc_Dir_i = "10" and Enc_Dir_i_d1 = "10" ) else
					"11" when ( Enc_Dir_i = "11" and Enc_Dir_i_d1 = "11" ) else
					Enc_Dir_Filt;
    
	ENC_RAW_COUNT  <= std_logic_vector(Enc_Pos_i); 
	ENC_DIR <= Enc_Dir_Filt;
	
--	Enc_CHA_Filt <= '0' when ( Enc_CHA_d3 = '0' and Enc_CHA_d4 = '0' and Enc_CHA_d5 = '0' and Enc_CHA_d6 = '0' ) else
--					'1' when ( Enc_CHA_d3 = '1' and Enc_CHA_d4 = '1' and Enc_CHA_d5 = '1' and Enc_CHA_d6 = '1' ) else
--					Enc_CHA_Filt;
						
    cha_filter: process(aclk)
        variable var_counter : integer range 0 to 100000000 :=0;
        begin 
            if (rising_edge(aclk)) then
                if (areset = '1') then
                    Enc_CHA_Filt <= '0';
                    var_counter:=0;
                else
                    if(ENC_CHA = Enc_CHA_d1) then
                        if(var_counter >= FILTER_IN_LEVEL) then
                            Enc_CHA_Filt <= ENC_CHA;
                        else
                            var_counter:=var_counter+1;       
                        end if;
                    else
                        var_counter:=0;
                    end if;         
                end if;
            end if;
    end process;
    
--	Enc_CHB_Filt <= '0' when ( Enc_CHB_d3 = '0' and Enc_CHB_d4 = '0' and Enc_CHB_d5 = '0' and Enc_CHB_d6 = '0' ) else
--                    '1' when ( Enc_CHB_d3 = '1' and Enc_CHB_d4 = '1' and Enc_CHB_d5 = '1' and Enc_CHB_d6 = '1' ) else
--                    Enc_CHB_Filt;
    
    chb_filter: process(aclk)
        variable var_counter : integer range 0 to 100000000 :=0;
        begin 
            if (rising_edge(aclk)) then
                if (areset = '1') then
                    Enc_CHB_Filt <= '0';
                    var_counter:=0;
                else
                    if(ENC_CHB = Enc_CHB_d1) then
                        if(var_counter >= FILTER_IN_LEVEL) then
                            Enc_CHB_Filt <= ENC_CHB;
                        else
                            var_counter:=var_counter+1;       
                        end if;
                    else
                        var_counter:=0;
                    end if;         
                end if;
            end if;
    end process;

    input_filter: process(aclk)
    begin 
        if (aclk'event and aclk = '1') then
			if (areset = '1') then
				Enc_CHA_d1	<= '0';
				Enc_CHA_d2	<= '0';
				Enc_CHA_d3	<= '0';
				Enc_CHA_d4	<= '0';
				Enc_CHA_d5	<= '0';
				Enc_CHA_d6	<= '0';
				Enc_CHB_d1	<= '0';
				Enc_CHB_d2	<= '0';
				Enc_CHB_d3	<= '0';
				Enc_CHB_d4	<= '0';
				Enc_CHB_d5	<= '0';
				Enc_CHB_d6	<= '0';
			else
				Enc_CHA_d1	<= ENC_CHA;
				Enc_CHA_d2	<= Enc_CHA_d1;
				Enc_CHA_d3	<= Enc_CHA_d2;
				Enc_CHA_d4	<= Enc_CHA_d3;
				Enc_CHA_d5	<= Enc_CHA_d4;
				Enc_CHA_d6	<= Enc_CHA_d5;
				Enc_CHB_d1	<= ENC_CHB;
				Enc_CHB_d2	<= Enc_CHB_d1;
				Enc_CHB_d3	<= Enc_CHB_d2;
				Enc_CHB_d4	<= Enc_CHB_d3;
				Enc_CHB_d5	<= Enc_CHB_d4;
				Enc_CHB_d6	<= Enc_CHB_d5;
			end if;
		end if;
    end process;	
	
    -- Position/Direction calculation SM
    encoder_SM: process(aclk)
    begin 
        if (aclk'event and aclk = '1') then
			if (areset = '1') then
				state_enc <= s0;                    
				Enc_Dir_i <= "00";
				Enc_Dir_i_d1 <= "00";
				Enc_Pos_i <= to_signed(0, 32);
			else
				Enc_Dir_i_d1 <= Enc_Dir_i;
				case state_enc is	
					when s0 =>
						if(Enc_CHA_Filt = '0' and Enc_CHB_Filt = '0') then
							state_enc <= s00;
						elsif(Enc_CHA_Filt = '0' and Enc_CHB_Filt = '1') then  
							state_enc <= s01;
						elsif(Enc_CHA_Filt = '1' and Enc_CHB_Filt = '0') then       
							state_enc <= s10;
						elsif(Enc_CHA_Filt = '1' and Enc_CHB_Filt = '1') then
							state_enc <= s11;
						end if;                	
					when s00 =>
						if(Enc_CHA_Filt = '1' and Enc_CHB_Filt = '0') then
							state_enc <= s10;
							if ( Enc_Dir_i_d1 = "01" ) then
								Enc_Pos_i <= Enc_Pos_i + 1;
							end if;
							ENC_DIR_i <= "01";
						elsif(Enc_CHA_Filt = '0' and Enc_CHB_Filt = '1') then  
							state_enc <= s01;
							if ( Enc_Dir_i_d1 = "10" ) then
								Enc_Pos_i <= Enc_Pos_i - 1;
							end if;
							ENC_DIR_i <= "10";
--						elsif(Enc_CHA_Filt = '1' and Enc_CHB_Filt = '1') then
--							state_enc <= s11;
--							ENC_DIR_i <= "11";
						end if;
					when s01 =>
						if(Enc_CHA_Filt = '0' and Enc_CHB_Filt = '0') then
							state_enc <= s00;
							if ( Enc_Dir_i_d1 = "01" ) then
								Enc_Pos_i <= Enc_Pos_i + 1;
							end if;
							ENC_DIR_i <= "01";
						elsif(Enc_CHA_Filt = '1' and Enc_CHB_Filt = '1') then  
							state_enc <= s11;
							if ( Enc_Dir_i_d1 = "10" ) then
								Enc_Pos_i <= Enc_Pos_i - 1;
							end if;
							ENC_DIR_i <= "10";
--						elsif(Enc_CHA_Filt = '1' and Enc_CHB_Filt = '0') then
--							state_enc <= s10;
--							ENC_DIR_i <= "11";
						end if;
					when s10 =>
						if(Enc_CHA_Filt = '1' and Enc_CHB_Filt = '1') then
							state_enc <= s11;
							if ( Enc_Dir_i_d1 = "01" ) then
								Enc_Pos_i <= Enc_Pos_i + 1;
							end if;
							ENC_DIR_i <= "01";
						elsif(Enc_CHA_Filt = '0' and Enc_CHB_Filt = '0') then  
							state_enc <= s00;
							if ( Enc_Dir_i_d1 = "10" ) then
								Enc_Pos_i <= Enc_Pos_i - 1;
							end if;
							ENC_DIR_i <= "10";
--						elsif(Enc_CHA_Filt = '0' and Enc_CHB_Filt = '1') then
--							state_enc <= s01;
--							ENC_DIR_i <= "11";
						end if;           
					when s11 =>
						if(Enc_CHA_Filt = '1' and Enc_CHB_Filt = '0') then
							state_enc <= s10;
							Enc_Pos_i <= Enc_Pos_i + 1;                   
							ENC_DIR_i <= "01";
						elsif(Enc_CHA_Filt = '0' and Enc_CHB_Filt = '1') then  
							state_enc <= s01;
							Enc_Pos_i <= Enc_Pos_i - 1;                   
							ENC_DIR_i <= "10";
--						elsif(Enc_CHA_Filt = '0' and Enc_CHB_Filt = '0') then
--							state_enc <= s00;
--							ENC_DIR_i <= "11";
						end if;            
				end case;
			end if;
		end if;
    end process;
    
    ---- SPEED ----
              
    -- Speed SM
    encoder_speed_1_SM: process(aclk)
    begin 
        if (rising_edge(aclk)) then
            if (areset = '1') then
                state_enc_speed <= idle;                      
                speed_1 <= (others => '0');
                Enc_Pos_i_d1 <= (others => '0');
                speed_1_cnt <= (others => '0');  
            else
                Enc_Pos_i_d1 <= Enc_Pos_i; 
                case state_enc_speed is
                    when idle =>
                        if(Enc_Pos_i_d1 /= Enc_Pos_i) then
                            speed_1_cnt <= (others => '0');
                            state_enc_speed <= measure_speed;    
                        end if;               
                    when measure_speed =>    
                        speed_1_cnt <= speed_1_cnt+1;                       
                        if(ENC_DIR_i /= Enc_Dir_i_d1) then
                            state_enc_speed <= idle;
                        elsif(speed_1_cnt = "11111111111111111111111111111111") then
                            speed_1_cnt <= (others => '0');
                            speed_1 <= (others => '0');
                            state_enc_speed <= idle;
                        else
                            if(Enc_Pos_i_d1 /= Enc_Pos_i) then
                                speed_1 <= speed_1_cnt+1;  
                                speed_1_cnt <= (others => '0');
                            end if;
                        end if;                           
                    when others =>  
                        null;
                end case;
            end if;
        end if;
    end process;
            
    -- Calculate speend in function of encoder pulses
    encoder_speed_2: process(aclk)
    begin 
        if (rising_edge(aclk)) then
            if (areset = '1') then
                speed_2_on     <= '0';
                speed_2        <= (others => '0');
                speed_2_cnt    <= (others => '0');
            else            
                if(ENC_CHA = '1' and enc_cha_d1 = '0') then
                    if(speed_2_on = '0') then
                        speed_2_on <= '1';
                    else
                        speed_2    <= speed_2_cnt+1;
                    end if;
                    speed_2_cnt    <= (others => '0');    
                else
                    speed_2_cnt <= speed_2_cnt + 1;
                end if;
               end if;
        end if;
    end process;
    
    -- Select encoder speed
    encoder_speed: process(aclk)
    begin 
        if (rising_edge(aclk)) then
            if (areset = '1') then
                ENC_SPEED <= (others => '0');
            else            
                if(ENC_SPEED_SEL = '1') then
                    ENC_SPEED <= std_logic_vector(speed_2);   
                else
                    ENC_SPEED <= std_logic_vector(speed_1); 
                end if;
            end if;
        end if;
    end process;
  
end arch_imp;
