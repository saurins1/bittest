----------------------------------------------------------------------------------
-- Company: Innerspec Technologies
-- Engineers: Wei Jiang
-- 
-- Design Name: 
-- Module Name: DIO_CONTROL - DIO_CONTROL_arch
-- Project Name: 
-- Target Devices: zynq 7z045
-- Tool versions: 
-- Description: Wei Jiang - Control Digital Outputs and Inputs
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity DIO_CONTROL is
	port (	
			CLOCK				: in std_logic;
			RESET				: in std_logic;

			--DIO SPI port pins  ( 3 MCP23S17 Devices)
			DIO_SCLK 			: out STD_LOGIC;
			DIO_CS_N 			: out STD_LOGIC;
			DIO_SDO 			: in STD_LOGIC;
			DIO_SDI 			: out STD_LOGIC;
			DIO_INT_N 			: in STD_LOGIC;
			
			--DIO interface
			GPIO_Update         : in  STD_LOGIC;
			GPI_READ_DATA		: out std_logic_vector(15 downto 0);
			
			GPO_WRITE_DATA		: in std_logic_vector(15 downto 0);
			
			ENC_SELECT			: in std_logic_vector(1 downto 0);
			CH_SELECT			: in std_logic_vector(3 downto 0);
			ENC_DIFF_ALARM		: out std_logic_vector(1 downto 0)
			
			);
end DIO_CONTROL;


architecture DIO_CONTROL_arch of DIO_CONTROL is

  type Dio_StateEnum_t is (Dio_Reset_c, 
						   Dio_SendIocon_c,
						   Dio_WaitIoconACK_c,
						   Dio_WaitIoconDone_c,
						   Dio_SendIodir001_c,
						   Dio_WaitIodir001ACK_c,
						   Dio_WaitIodir001Done_c,
						   Dio_SendIodir010_c,
						   Dio_WaitIodir010ACK_c,
						   Dio_WaitIodir010Done_c,
						   Dio_Idle_c,
						   Dio_PollInput000_c,
						   Dio_WaitInput000ACK_c,
						   Dio_WaitInput000Done_c,
						   Dio_Pollinput010_c,
						   Dio_WaitInput010ACK_c,
						   Dio_WaitInput010Done_c,
						   Dio_SendOutput001_c,
						   Dio_WaitOutput001ACK_c,
						   Dio_WaitOutput001Done_c,
						   Dio_SendOutput010_c,
						   Dio_WaitOutput010ACK_c,
						   Dio_WaitOutput010Done_c);
						   
  signal Dio_State_s 	:	Dio_StateEnum_t;
						   
  component spi_master is
      Generic (   
          N : positive := 32;                                             -- 32bit serial word length is default
          CPOL : std_logic := '0';                                        -- SPI mode selection (mode 0 default)
          CPHA : std_logic := '0';                                        -- CPOL = clock polarity, CPHA = clock phase.
          PREFETCH : positive := 2;                                       -- prefetch lookahead cycles
          SPI_2X_CLK_DIV : positive := 5);                                -- for a 100MHz sclk_i, yields a 10MHz SCK
      Port (  
          sclk_i : in std_logic := 'X';                                   -- high-speed serial interface system clock
          pclk_i : in std_logic := 'X';                                   -- high-speed parallel interface system clock
          rst_i : in std_logic := 'X';                                    -- reset core
          ---- serial interface ----
          spi_ssel_o : out std_logic;                                     -- spi bus slave select line
          spi_sck_o : out std_logic;                                      -- spi bus sck
          spi_mosi_o : out std_logic;                                     -- spi bus mosi output
          spi_miso_i : in std_logic := 'X';                               -- spi bus spi_miso_i input
          ---- parallel interface ----
          di_req_o : out std_logic;                                       -- preload lookahead data request line
          di_i : in  std_logic_vector (N-1 downto 0) := (others => 'X');  -- parallel data in (clocked on rising spi_clk after last bit)
          wren_i : in std_logic := 'X';                                   -- user data write enable, starts transmission when interface is idle
          wr_ack_o : out std_logic;                                       -- write acknowledge
          do_valid_o : out std_logic;                                     -- do_o data valid signal, valid during one spi_clk rising edge.
          do_o : out  std_logic_vector (N-1 downto 0);                    -- parallel output (clocked on rising spi_clk after last bit)
          done_o : out std_logic										  -- handshake added to break continuous tx/rx
          --- debug ports: can be removed or left unconnected for the application circuit ---
--          sck_ena_o : out std_logic;                                      -- debug: internal sck enable signal
--          sck_ena_ce_o : out std_logic;                                   -- debug: internal sck clock enable signal
--          do_transfer_o : out std_logic;                                  -- debug: internal transfer driver
--          wren_o : out std_logic;                                         -- debug: internal state of the wren_i pulse stretcher
--          rx_bit_reg_o : out std_logic;                                   -- debug: internal rx bit
--          state_dbg_o : out std_logic_vector (3 downto 0);                -- debug: internal state register
--          core_clk_o : out std_logic;
--          core_n_clk_o : out std_logic;
--          core_ce_o : out std_logic;
--          core_n_ce_o : out std_logic
--          sh_reg_dbg_o : out std_logic_vector (N-1 downto 0)              -- debug: internal shift register
      );                      
  end component spi_master;

  SIGNAL dio_wren_i				: STD_LOGIC;
  SIGNAL dio_do_valid_o			: STD_LOGIC;
  SIGNAL dio_di_req_o			: STD_LOGIC;
  SIGNAL dio_wr_ack_o			: STD_LOGIC;
  SIGNAL dio_done_o			    : STD_LOGIC;

  SIGNAL dio_di_i,dio_do_o		: STD_LOGIC_VECTOR(31 DOWNTO 0);
  
  SIGNAL GPIO_Updated1		    : STD_LOGIC := '0';
  
  constant Iocon_Data			: STD_LOGIC_VECTOR(31 DOWNTO 0):= x"400A0C0C";	--set IOCON.HAEN and IOCON.ODR
  constant Iodir001_Data		: STD_LOGIC_VECTOR(31 DOWNTO 0):= x"42000000";	--set chip_001 as outputs
  constant Iodir010_Data		: STD_LOGIC_VECTOR(31 DOWNTO 0):= x"44000CFC";	--set chip_010 portA 2 in 6 out, port B 2 out(for REVB)  
  constant Input000_Data		: STD_LOGIC_VECTOR(31 DOWNTO 0):= x"41120000";	-- read chip_000 Inputs
  constant Input010_Data		: STD_LOGIC_VECTOR(31 DOWNTO 0):= x"45120000";	-- read chip_010 Inputs (2bit)
  constant Output001_Data		: STD_LOGIC_VECTOR(15 DOWNTO 0):= x"4212";		-- write chip_001 Outputs
  constant Output010_Data		: STD_LOGIC_VECTOR(15 DOWNTO 0):= x"4412";		-- write chip_010 Outputs (6bit)
  
begin
	  
	FSM_Dio_l : process(CLOCK)
	begin
		if rising_edge(CLOCK) then
			
			GPIO_Updated1 <= GPIO_Update; 
			if ( RESET = '1' ) then
				dio_wren_i <= '0';
				Dio_State_s <= Dio_Reset_c;
			else
				case Dio_State_s is
				
					when Dio_Reset_c =>
						dio_wren_i <= '0';
						Dio_State_s <= Dio_SendIocon_c;
						
					when Dio_SendIocon_c =>
						dio_wren_i <= '1';
						dio_di_i <= Iocon_Data;
						Dio_State_s <= Dio_WaitIoconACK_c;
						
					when Dio_WaitIoconACK_c =>
						dio_wren_i <='0';
						if (dio_wr_ack_o = '1') then
							Dio_State_s <= Dio_WaitIoconDone_c;
						else
							Dio_State_s <= Dio_WaitIoconACK_c;
						end if;
					
					when Dio_WaitIoconDone_c => 
						dio_wren_i <='0';
						if (dio_done_o = '1') then
							Dio_State_s <= Dio_SendIodir001_c;
						else
							Dio_State_s <= Dio_WaitIoconDone_c;
						end if;
					
					when Dio_SendIodir001_c =>
						dio_wren_i <= '1';
						dio_di_i <= Iodir001_Data;
						Dio_State_s <= Dio_WaitIodir001ACK_c;
						
					when Dio_WaitIodir001ACK_c =>
						dio_wren_i <='0';
						if (dio_wr_ack_o = '1') then
							Dio_State_s <= Dio_WaitIodir001Done_c;
						else
							Dio_State_s <= Dio_WaitIodir001ACK_c;
						end if;
					
					when Dio_WaitIodir001Done_c =>
						dio_wren_i <='0';
						if (dio_done_o = '1') then
							Dio_State_s <= Dio_SendIodir010_c;
						else
							Dio_State_s <= Dio_WaitIodir001Done_c;
						end if;
						
					when Dio_SendIodir010_c =>
						dio_wren_i <= '1';
						dio_di_i <= Iodir010_Data;
						Dio_State_s <= Dio_WaitIodir010ACK_c;
						
					when Dio_WaitIodir010ACK_c =>
						dio_wren_i <='0';
						if (dio_wr_ack_o = '1') then
							Dio_State_s <= Dio_WaitIodir010Done_c;
						else
							Dio_State_s <= Dio_WaitIodir010ACK_c;
						end if;

					when Dio_WaitIodir010Done_c =>
						dio_wren_i <='0';
						if (dio_done_o = '1') then
							Dio_State_s <= Dio_Idle_c;
						else
							Dio_State_s <= Dio_WaitIodir010Done_c;
						end if;
						
					when Dio_Idle_c =>
						dio_wren_i <='0';
						if (  (GPIO_Update = '1') and (GPIO_Updated1 = '0') ) then
							Dio_State_s <= Dio_PollInput000_c;
						else
							Dio_State_s <= Dio_Idle_c;
						end if;
						
					when Dio_PollInput000_c =>
						dio_wren_i <= '1';
						dio_di_i <= Input000_Data;
						Dio_State_s <= Dio_WaitInput000ACK_c;
						
					when Dio_WaitInput000ACK_c => 
						dio_wren_i <='0';
						if (dio_wr_ack_o = '1') then
							Dio_State_s <= Dio_WaitInput000Done_c;
						else
							Dio_State_s <= Dio_WaitInput000ACK_c;
						end if;					

					when Dio_WaitInput000Done_c =>
						dio_wren_i <='0';
						if (dio_done_o = '1') then
							GPI_READ_DATA <= dio_do_o (7 downto 0) & dio_do_o (15 downto 8);
							Dio_State_s <= Dio_PollInput010_c;
						else
							Dio_State_s <= Dio_WaitInput000Done_c;
						end if;						

					when Dio_PollInput010_c =>
						dio_wren_i <= '1';
						dio_di_i <= Input010_Data;
						Dio_State_s <= Dio_WaitInput010ACK_c;
						
					when Dio_WaitInput010ACK_c => 
						dio_wren_i <='0';
						if (dio_wr_ack_o = '1') then
							Dio_State_s <= Dio_WaitInput010Done_c;
						else
							Dio_State_s <= Dio_WaitInput010ACK_c;
						end if;					

					when Dio_WaitInput010Done_c =>
						dio_wren_i <='0';
						if (dio_done_o = '1') then
							ENC_DIFF_ALARM <= dio_do_o (11 downto 10);
							Dio_State_s <= Dio_SendOutput001_c;
						else
							Dio_State_s <= Dio_WaitInput010Done_c;
						end if;						
					
					when Dio_SendOutput001_c =>
						dio_wren_i <= '1';
						dio_di_i <= Output001_Data & GPO_WRITE_DATA(7 downto 0) & GPO_WRITE_DATA(15 downto 8);
						Dio_State_s <= Dio_WaitOutput001ACK_c;
						
					when Dio_WaitOutput001ACK_c =>
						dio_wren_i <='0';
						if (dio_wr_ack_o = '1') then
							Dio_State_s <= Dio_WaitOutput001Done_c;
						else
							Dio_State_s <= Dio_WaitOutput001ACK_c;
						end if;

					when Dio_WaitOutput001Done_c =>
						dio_wren_i <='0';
						if (dio_done_o = '1') then
							Dio_State_s <= Dio_SendOutput010_c;
						else
							Dio_State_s <= Dio_WaitOutput001Done_c;
						end if;

					when Dio_SendOutput010_c =>
						dio_wren_i <= '1';
						dio_di_i <= Output010_Data & CH_SELECT & "00" & ENC_SELECT & "00000011";	--enable TWR_IO_CNTL(REVB)
						Dio_State_s <= Dio_WaitOutput010ACK_c;
						
					when Dio_WaitOutput010ACK_c =>
						dio_wren_i <='0';
						if (dio_wr_ack_o = '1') then
							Dio_State_s <= Dio_WaitOutput010Done_c;
						else
							Dio_State_s <= Dio_WaitOutput010ACK_c;
						end if;

					when Dio_WaitOutput010Done_c =>
						dio_wren_i <='0';
						if (dio_done_o = '1') then
							Dio_State_s <= Dio_Idle_c;
						else
							Dio_State_s <= Dio_WaitOutput010Done_c;
						end if;
					
					when others =>
						Dio_State_s <= Dio_Idle_c;
				end case;
			end if;
		end if;
	end process;
  
      Inst_spi_master_dio: spi_master
        generic map (N => 32, CPOL => '0', CPHA => '0', PREFETCH => 3, SPI_2X_CLK_DIV => 5)
       port map( 
            sclk_i => CLOCK,                      -- system clock is used for serial and parallel ports
            pclk_i => CLOCK,
            rst_i => RESET,
            spi_ssel_o => DIO_CS_N,
            spi_sck_o => DIO_SCLK,
            spi_mosi_o => DIO_SDI,
            spi_miso_i => DIO_SDO,
            di_req_o => open, --dio_di_req_o,
            di_i => dio_di_i,
            wren_i => dio_wren_i,
            wr_ack_o => dio_wr_ack_o,
            do_valid_o => open, --dio_do_valid_o,
            do_o => dio_do_o,
            done_o => dio_done_o
        );   


end DIO_CONTROL_arch;