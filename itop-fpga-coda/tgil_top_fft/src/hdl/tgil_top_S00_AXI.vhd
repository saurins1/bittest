library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tgil_top_S00_AXI is
	generic (
	    REGISTER_W32    : INTEGER := 32;
	    REGISTER_W16    : INTEGER := 16;
	    REGISTER_W8     : INTEGER := 8;
		REGISTER_W6     : INTEGER := 6;
		REGISTER_W4     : INTEGER := 4;
		REGISTER_W2     : INTEGER := 2;
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_ADDR_WIDTH	: integer	:= 11
	);
	port (
		-- Users to add ports here
        -- CONTROL REGISTERS
        DMA_UPLOAD_CONTROL      : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);   
        DMA_DOWNLOAD_CONTROL    : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0); 
        START_INSPECTION_CONTROL: out std_logic; 
        SCAN_MODE_CONTROL       : out std_logic_vector(REGISTER_W32-1 downto 0);  
        CSCAN_MODE_CONTROL      : out std_logic_vector(REGISTER_W32-1 downto 0);  
        TRIGGER_CONTROL         : out std_logic_vector(REGISTER_W32-1 downto 0);
        RESET_CONTROL           : out std_logic;
        ENABLE_CONTROL          : out std_logic;
        CLK_ENABLE_CONTROL      : out std_logic;
        CHANNEL_CONTROL         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        RESET_ENCODERS_CONTROL  : out std_logic;
        HARDWARE_CONTROL        : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        -- STATUS REGISTERS
        DMA_UPLOAD_STATUS       : in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);   
        DMA_DOWNLOAD_STATUS     : in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0); 
        START_INSPECTION_STATUS : in std_logic; 
        SCAN_MODE_STATUS        : in std_logic_vector(REGISTER_W32-1 downto 0);  
        CSCAN_MODE_STATUS       : in std_logic_vector(REGISTER_W32-1 downto 0);  
        TRIGGER_STATUS          : in std_logic_vector(REGISTER_W32-1 downto 0);
        RESET_STATUS            : in std_logic;
        ENABLE_STATUS           : in std_logic;
        CLK_ENABLE_STATUS       : in std_logic;
        CHANNEL_STATUS          : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        RESET_ENCODERS_STATUS   : in std_logic;
        TEMPERATURE_STATUS      : in std_logic_vector(REGISTER_W32-1 downto 0);
        PULSER_STATUS           : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        DIGITAL_IO_STATUS       : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        VOLTAGE_STATUS          : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        
        -- AVG
        DSP_AVG_LEVEL    : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        DSP_AVG_COUNTER  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        DSP_AVG_TYPE     : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
               
        ADC_DEBUG : out std_logic_vector(REGISTER_W32-1 downto 0); 
        
        -- DEBUG
        CONFIGURATION_SIZE                      : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        CONFIGURATION_ON                        : in STD_LOGIC;
        MAIN_DMA_STATE                          : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        
        -- Config output receiver
        CH0_RECEIVER_SAMPLING_FREQUENCY         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        CH0_RECEIVER_DATA_WINDOW                : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        CH0_RECEIVER_DELAY                      : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        CH0_RECEIVER_ANALOG_GAIN                : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        CH0_RECEIVER_DIGITAL_GAIN               : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        CH0_RECEIVER_EXTERNAL_MULTIPLEXER_EN    : in STD_LOGIC;
                
        -- Config output transmitter
        CH0_TRANSMITTER_VOLTAGE                 : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        CH0_TRANSMITTER_BURST_FREQUENCY         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        CH0_TRANSMITTER_N_CYCLES                : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        CH0_TRANSMITTER_DELAY                   : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        CH0_TRANSMITTER_DIRECTIONAL_PHASING     : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
            
        -- Config output magnet
        CH0_MAGNET_MODE                         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        CH0_MAGNET_PULSE_WIDTH                  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        CH0_MAGNET_INITIAL_DELAY                : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            
        -- Config analog filters
        CH0_DSP_ANALOG_FILTER                   : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);   
        
        -- Debug
        CH0_CHANNEL_TRIGGER_TYPE    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        CH0_STATE_A                 : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        CH0_STATE_B                 : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        CH0_STATE_C                 : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);   
             
        -- Config parameters
        CONFIGURATION_HEADER_SIZE      : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        CONFIGURATION_CHANNELS         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        CONFIGURATION_CHANNEL0_OFFSET  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        CONFIGURATION_ENCODER_OFFSET   : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        CONFIGURATION_DIGITALIO_OFFSET : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        CONFIGURATION_CHANNEL0_SIZE    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        CONFIGURATION_TRIGGER_TYPE     : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        CH0_SIGNAL_INPUT_AXIS_TREADY   : in STD_LOGIC;
        CONFIGURATION_CHANNEL0_ENABLED : in STD_LOGIC;
        CONFIGURATION_CHANNEL0_ON      : in STD_LOGIC;
        
        CH0_DSP_COINC_LEVEL                     : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        CH0_DSP_AVG_LEVEL                       : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        
        ENCODER1_COUNT                  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        ENCODER2_COUNT                  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        ENCODER_DEBUG                   : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        
        ADC_WRITE_COUNTER : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);  
        ADC_READ_COUNTER  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        
        ADC_CONFIG_EX     		: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        ADC_CONFIG_EX_VALID     : out STD_LOGIC;
        ADC_CONFIG_LOADED       : in STD_LOGIC;
        ADC_CONFIG_CURRENT      : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        ADC_CONFIG_REQUEST      : in STD_LOGIC;
        ADC_CONFIG_REQUEST_ACK  : out STD_LOGIC;
        
        CC_CONFIG_ON_ACK        : in STD_LOGIC;
        CC_CONFIG_ON            : out STD_LOGIC;        
        CC_CONFIG_ADDR          : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        CC_CONFIG_DATA          : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0); 
      
		-- User ports ends
		-- Do not modify the ports beyond this line

		-- Global Clock Signal
		S_AXI_ACLK	: in std_logic;
		-- Global Reset Signal. This Signal is Active LOW
		S_AXI_ARESETN	: in std_logic;
		-- Write address (issued by master, acceped by Slave)
		S_AXI_AWADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		-- Write channel Protection type. This signal indicates the
    		-- privilege and security level of the transaction, and whether
    		-- the transaction is a data access or an instruction access.
		S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
		-- Write address valid. This signal indicates that the master signaling
    		-- valid write address and control information.
		S_AXI_AWVALID	: in std_logic;
		-- Write address ready. This signal indicates that the slave is ready
    		-- to accept an address and associated control signals.
		S_AXI_AWREADY	: out std_logic;
		-- Write data (issued by master, acceped by Slave) 
		S_AXI_WDATA	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		-- Write strobes. This signal indicates which byte lanes hold
    		-- valid data. There is one write strobe bit for each eight
    		-- bits of the write data bus.    
		S_AXI_WSTRB	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		-- Write valid. This signal indicates that valid write
    		-- data and strobes are available.
		S_AXI_WVALID	: in std_logic;
		-- Write ready. This signal indicates that the slave
    		-- can accept the write data.
		S_AXI_WREADY	: out std_logic;
		-- Write response. This signal indicates the status
    		-- of the write transaction.
		S_AXI_BRESP	: out std_logic_vector(1 downto 0);
		-- Write response valid. This signal indicates that the channel
    		-- is signaling a valid write response.
		S_AXI_BVALID	: out std_logic;
		-- Response ready. This signal indicates that the master
    		-- can accept a write response.
		S_AXI_BREADY	: in std_logic;
		-- Read address (issued by master, acceped by Slave)
		S_AXI_ARADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		-- Protection type. This signal indicates the privilege
    		-- and security level of the transaction, and whether the
    		-- transaction is a data access or an instruction access.
		S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
		-- Read address valid. This signal indicates that the channel
    		-- is signaling valid read address and control information.
		S_AXI_ARVALID	: in std_logic;
		-- Read address ready. This signal indicates that the slave is
    		-- ready to accept an address and associated control signals.
		S_AXI_ARREADY	: out std_logic;
		-- Read data (issued by slave)
		S_AXI_RDATA	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		-- Read response. This signal indicates the status of the
    		-- read transfer.
		S_AXI_RRESP	: out std_logic_vector(1 downto 0);
		-- Read valid. This signal indicates that the channel is
    		-- signaling the required read data.
		S_AXI_RVALID	: out std_logic;
		-- Read ready. This signal indicates that the master can
    		-- accept the read data and response information.
		S_AXI_RREADY	: in std_logic
	);
end tgil_top_S00_AXI;

architecture arch_imp of tgil_top_S00_AXI is

    -- USER SIGNALS
    
    signal heart_beat_control : std_logic;
    signal heart_beat_status  : std_logic;

	-- AXI4LITE signals
	signal axi_awaddr	: std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
	signal axi_awready	: std_logic;
	signal axi_wready	: std_logic;
	signal axi_bresp	: std_logic_vector(1 downto 0);
	signal axi_bvalid	: std_logic;
	signal axi_araddr	: std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
	signal axi_arready	: std_logic;
	signal axi_rdata	: std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal axi_rresp	: std_logic_vector(1 downto 0);
	signal axi_rvalid	: std_logic;

	-- Example-specific design signals
	-- local parameter for addressing 32 bit / 64 bit C_S_AXI_DATA_WIDTH
	-- ADDR_LSB is used for addressing 32/64 bit registers/memories
	-- ADDR_LSB = 2 for 32 bits (n downto 2)
	-- ADDR_LSB = 3 for 64 bits (n downto 3)
	constant ADDR_LSB  : integer := (C_S_AXI_DATA_WIDTH/32)+ 1;
	constant OPT_MEM_ADDR_BITS : integer := 8;
	------------------------------------------------
	---- Signals for user logic register space example
	--------------------------------------------------
	---- Number of Slave Registers 512
	signal slv_reg0	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg1	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg2	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg3	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg4	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg5	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg6	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg7	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg8	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg9	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg10	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg11	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg12	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg13	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg14	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg15	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	
	signal slv_reg16	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg17	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg18	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg27	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg28	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg29	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg30	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg31	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	
	signal slv_reg256	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg257	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg258	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg259	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg260	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg261	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg262	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg263	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg264	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg265	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg266	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg267	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg268	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg269	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg270	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg271	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg272	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg273	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg275	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	
	-- DEBUG reg
    signal slv_reg384	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg385    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg386    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg387    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg388    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg389    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg390    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg391    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg392    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg393    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg394    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg395    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg396    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg397    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg398    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg399    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg400    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg401    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg402    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg403    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg404    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg405    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg406    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg407    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg408    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg409    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg410    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg411    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg412    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg413    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg414    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg415    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg416    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg417    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg418    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg419    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg420    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg421    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg422    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg423    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg424    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg425    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg426    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg427    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg428    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg429    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg430    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg431    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg432    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg433    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg434    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg435    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg436    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg437    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg438    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg439    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg440    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg441    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg442    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg443    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0); 
    signal slv_reg444    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg445    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg446    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg447    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg448    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg449    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg450    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg451    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg452    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg453    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg454    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg455    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal slv_reg456    :std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
      
	signal slv_reg_rden	: std_logic;
	signal slv_reg_wren	: std_logic;
	signal reg_data_out	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal byte_index	: integer;

begin
	-- I/O Connections assignments

	S_AXI_AWREADY	<= axi_awready;
	S_AXI_WREADY	<= axi_wready;
	S_AXI_BRESP	<= axi_bresp;
	S_AXI_BVALID	<= axi_bvalid;
	S_AXI_ARREADY	<= axi_arready;
	S_AXI_RDATA	<= axi_rdata;
	S_AXI_RRESP	<= axi_rresp;
	S_AXI_RVALID	<= axi_rvalid;
	-- Implement axi_awready generation
	-- axi_awready is asserted for one S_AXI_ACLK clock cycle when both
	-- S_AXI_AWVALID and S_AXI_WVALID are asserted. axi_awready is
	-- de-asserted when reset is low.

	process (S_AXI_ACLK)
	begin
	  if rising_edge(S_AXI_ACLK) then 
	    if S_AXI_ARESETN = '0' then
	      axi_awready <= '0';
	    else
	      if (axi_awready = '0' and S_AXI_AWVALID = '1' and S_AXI_WVALID = '1') then
	        -- slave is ready to accept write address when
	        -- there is a valid write address and write data
	        -- on the write address and data bus. This design 
	        -- expects no outstanding transactions. 
	        axi_awready <= '1';
	      else
	        axi_awready <= '0';
	      end if;
	    end if;
	  end if;
	end process;

	-- Implement axi_awaddr latching
	-- This process is used to latch the address when both 
	-- S_AXI_AWVALID and S_AXI_WVALID are valid. 

	process (S_AXI_ACLK)
	begin
	  if rising_edge(S_AXI_ACLK) then 
	    if S_AXI_ARESETN = '0' then
	      axi_awaddr <= (others => '0');
	    else
	      if (axi_awready = '0' and S_AXI_AWVALID = '1' and S_AXI_WVALID = '1') then
	        -- Write Address latching
	        axi_awaddr <= S_AXI_AWADDR;
	      end if;
	    end if;
	  end if;                   
	end process; 

	-- Implement axi_wready generation
	-- axi_wready is asserted for one S_AXI_ACLK clock cycle when both
	-- S_AXI_AWVALID and S_AXI_WVALID are asserted. axi_wready is 
	-- de-asserted when reset is low. 

	process (S_AXI_ACLK)
	begin
	  if rising_edge(S_AXI_ACLK) then 
	    if S_AXI_ARESETN = '0' then
	      axi_wready <= '0';
	    else
	      if (axi_wready = '0' and S_AXI_WVALID = '1' and S_AXI_AWVALID = '1') then
	          -- slave is ready to accept write data when 
	          -- there is a valid write address and write data
	          -- on the write address and data bus. This design 
	          -- expects no outstanding transactions.           
	          axi_wready <= '1';
	      else
	        axi_wready <= '0';
	      end if;
	    end if;
	  end if;
	end process; 

	-- Implement memory mapped register select and write logic generation
	-- The write data is accepted and written to memory mapped registers when
	-- axi_awready, S_AXI_WVALID, axi_wready and S_AXI_WVALID are asserted. Write strobes are used to
	-- select byte enables of slave registers while writing.
	-- These registers are cleared when reset (active low) is applied.
	-- Slave register write enable is asserted when valid address and data are available
	-- and the slave is ready to accept the write address and write data.
	slv_reg_wren <= axi_wready and S_AXI_WVALID and axi_awready and S_AXI_AWVALID ;

	process (S_AXI_ACLK)
	variable loc_addr :std_logic_vector(OPT_MEM_ADDR_BITS downto 0); 
	begin
	  if rising_edge(S_AXI_ACLK) then 
	    if S_AXI_ARESETN = '0' then
	      -- CONTROL
	      slv_reg0 <= (others => '0');
	      slv_reg1 <= (others => '0');
	      slv_reg2 <= (others => '0');
	      slv_reg3 <= (others => '0');
	      slv_reg4 <= (others => '0');
	      slv_reg5 <= (others => '0');
	      slv_reg6 <= (others => '0');
	      slv_reg7 <= (others => '0');
	      slv_reg8 <= (others => '0');
	      slv_reg9 <= (others => '0');
	      slv_reg10 <= (others => '0');
	      slv_reg11 <= (others => '0');
	      slv_reg12 <= (others => '0');
	      slv_reg13 <= (others => '0');
	      slv_reg14 <= (others => '0');
	      slv_reg15 <= (others => '0');
	      slv_reg16 <= (others => '0');
	      slv_reg17 <= (others => '0');
	      slv_reg18 <= (others => '0');
	      slv_reg27 <= (others => '0');
	      slv_reg28 <= (others => '0');
	      slv_reg29 <= (others => '0');
	      slv_reg30 <= (others => '0');
	    else
	      loc_addr := axi_awaddr(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB);
	      if (slv_reg_wren = '1') then
	        case loc_addr is
	          when b"000000000" =>
	            for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
	              if ( S_AXI_WSTRB(byte_index) = '1' ) then
	                -- Respective byte enables are asserted as per write strobes                   
	                -- slave registor 0
	                slv_reg0(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
	              end if;
	            end loop;
	          when b"000000001" =>
	            for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
	              if ( S_AXI_WSTRB(byte_index) = '1' ) then
	                -- Respective byte enables are asserted as per write strobes                   
	                -- slave registor 1
	                slv_reg1(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
	              end if;
	            end loop;
	          when b"000000010" =>
	            for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
	              if ( S_AXI_WSTRB(byte_index) = '1' ) then
	                -- Respective byte enables are asserted as per write strobes                   
	                -- slave registor 2
	                slv_reg2(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
	              end if;
	            end loop;
	          when b"000000011" =>
	            for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
	              if ( S_AXI_WSTRB(byte_index) = '1' ) then
	                -- Respective byte enables are asserted as per write strobes                   
	                -- slave registor 3
	                slv_reg3(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
	              end if;
	            end loop;
	          when b"000000100" =>
	            for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
	              if ( S_AXI_WSTRB(byte_index) = '1' ) then
	                -- Respective byte enables are asserted as per write strobes                   
	                -- slave registor 4
	                slv_reg4(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
	              end if;
	            end loop;
	          when b"000000101" =>
	            for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
	              if ( S_AXI_WSTRB(byte_index) = '1' ) then
	                -- Respective byte enables are asserted as per write strobes                   
	                -- slave registor 5
	                slv_reg5(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
	              end if;
	            end loop;
	          when b"000000110" =>
	            for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
	              if ( S_AXI_WSTRB(byte_index) = '1' ) then
	                -- Respective byte enables are asserted as per write strobes                   
	                -- slave registor 6
	                slv_reg6(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
	              end if;
	            end loop;
	          when b"000000111" =>
	            for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
	              if ( S_AXI_WSTRB(byte_index) = '1' ) then
	                -- Respective byte enables are asserted as per write strobes                   
	                -- slave registor 7
	                slv_reg7(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
	              end if;
	            end loop;
	          when b"000001000" =>
	            for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
	              if ( S_AXI_WSTRB(byte_index) = '1' ) then
	                -- Respective byte enables are asserted as per write strobes                   
	                -- slave registor 8
	                slv_reg8(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
	              end if;
	            end loop;
	          when b"000001001" =>
	            for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
	              if ( S_AXI_WSTRB(byte_index) = '1' ) then
	                -- Respective byte enables are asserted as per write strobes                   
	                -- slave registor 9
	                slv_reg9(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
	              end if;
	            end loop;
	          when b"000001010" =>
	            for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
	              if ( S_AXI_WSTRB(byte_index) = '1' ) then
	                -- Respective byte enables are asserted as per write strobes                   
	                -- slave registor 10
	                slv_reg10(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
	              end if;
	            end loop;
	          when b"000001011" =>
	            for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
	              if ( S_AXI_WSTRB(byte_index) = '1' ) then
	                -- Respective byte enables are asserted as per write strobes                   
	                -- slave registor 11
	                slv_reg11(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
	              end if;
	            end loop;
	          when b"000001100" =>
	            for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
	              if ( S_AXI_WSTRB(byte_index) = '1' ) then
	                -- Respective byte enables are asserted as per write strobes                   
	                -- slave registor 12
	                slv_reg12(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
	              end if;
	            end loop;
	          when b"000001101" =>
	            for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
	              if ( S_AXI_WSTRB(byte_index) = '1' ) then
	                -- Respective byte enables are asserted as per write strobes                   
	                -- slave registor 13
	                slv_reg13(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
	              end if;
	            end loop;
	          when b"000001110" =>
	            for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
	              if ( S_AXI_WSTRB(byte_index) = '1' ) then
	                -- Respective byte enables are asserted as per write strobes                   
	                -- slave registor 14
	                slv_reg14(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
	              end if;
	            end loop;
	          when b"000001111" =>
	            for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
	              if ( S_AXI_WSTRB(byte_index) = '1' ) then
	                -- Respective byte enables are asserted as per write strobes                   
	                -- slave registor 15
	                slv_reg15(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
	              end if;
	            end loop;
	          when b"000010000" =>
                for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
                  if ( S_AXI_WSTRB(byte_index) = '1' ) then
                    -- Respective byte enables are asserted as per write strobes                   
                    -- slave registor 15
                    slv_reg16(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
                  end if;
                end loop;
	          when b"000010001" =>
                for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
                  if ( S_AXI_WSTRB(byte_index) = '1' ) then
                    -- Respective byte enables are asserted as per write strobes                   
                    -- slave registor 15
                    slv_reg17(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
                  end if;
                end loop;
	          when b"000010010" =>
                for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
                  if ( S_AXI_WSTRB(byte_index) = '1' ) then
                    -- Respective byte enables are asserted as per write strobes                   
                    -- slave registor 15
                    slv_reg18(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
                  end if;
                end loop;
	          when b"000011011" =>
                for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
                  if ( S_AXI_WSTRB(byte_index) = '1' ) then
                    -- Respective byte enables are asserted as per write strobes                   
                    -- slave registor 15
                    slv_reg27(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
                  end if;
                end loop;
	          when b"000011100" =>
                for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
                  if ( S_AXI_WSTRB(byte_index) = '1' ) then
                    -- Respective byte enables are asserted as per write strobes                   
                    -- slave registor 15
                    slv_reg28(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
                  end if;
                end loop; 
	          when b"000011101" =>
                  for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
                    if ( S_AXI_WSTRB(byte_index) = '1' ) then
                      -- Respective byte enables are asserted as per write strobes                   
                      -- slave registor 15
                      slv_reg29(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
                    end if;
                  end loop;
	          when b"000011110" =>
                  for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
                    if ( S_AXI_WSTRB(byte_index) = '1' ) then
                      -- Respective byte enables are asserted as per write strobes                   
                      -- slave registor 15
                      slv_reg30(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
                    end if;
                  end loop;  
	          when others =>
	            -- CONTROL
	            slv_reg0 <= slv_reg0;
	            slv_reg1 <= slv_reg1;
	            slv_reg2 <= slv_reg2;
	            slv_reg3 <= slv_reg3;
	            slv_reg4 <= slv_reg4;
	            slv_reg5 <= slv_reg5;
	            slv_reg6 <= slv_reg6;
	            slv_reg7 <= slv_reg7;
	            slv_reg8 <= slv_reg8;
	            slv_reg9 <= slv_reg9;
	            slv_reg10 <= slv_reg10;
	            slv_reg11 <= slv_reg11;
	            slv_reg12 <= slv_reg12;
	            slv_reg13 <= slv_reg13;
	            slv_reg14 <= slv_reg14;
	            slv_reg15 <= slv_reg15;  
	            slv_reg16 <= slv_reg16; 
	            slv_reg17 <= slv_reg17; 
	            slv_reg18 <= slv_reg18;        
	            slv_reg27 <= slv_reg27;
	            slv_reg28 <= slv_reg28;
	            slv_reg29 <= slv_reg29;
	            slv_reg30 <= slv_reg30;
	        end case;
	      end if;
	    end if;
	  end if;                   
	end process; 
		
	-- UPDATE CONTROL SIGNALS
	process (S_AXI_ACLK)
        begin
          if rising_edge(S_AXI_ACLK) then 
            if S_AXI_ARESETN = '0' then
                -- CONTROL
                DMA_UPLOAD_CONTROL <= (others => '0');
                DMA_DOWNLOAD_CONTROL <= (others => '0');
                START_INSPECTION_CONTROL <= '0';
                SCAN_MODE_CONTROL <= (others => '0');
                CSCAN_MODE_CONTROL <= (others => '0');
                TRIGGER_CONTROL <= (others => '0');
                RESET_CONTROL <= '0';
                ENABLE_CONTROL <= '0';
                CLK_ENABLE_CONTROL <= '0';
                RESET_ENCODERS_CONTROL <= '0';
                heart_beat_control <= '0';
                CHANNEL_CONTROL <= (others => '0');
                HARDWARE_CONTROL <= (others => '0');
                ADC_DEBUG <= (others => '0');
                ADC_CONFIG_EX <= (others => '0');
                ADC_CONFIG_EX_VALID <= '0';
                ADC_CONFIG_REQUEST_ACK <= '0';
                CC_CONFIG_ON <= '0';
                CC_CONFIG_ADDR <= (others => '0');
                CC_CONFIG_DATA <= (others => '0');
            else
                -- STATUS
                DMA_UPLOAD_CONTROL <= slv_reg0;
                DMA_DOWNLOAD_CONTROL <= slv_reg1;
                START_INSPECTION_CONTROL <= slv_reg2(0);
                SCAN_MODE_CONTROL <= slv_reg3;
                CSCAN_MODE_CONTROL <= slv_reg4;
                TRIGGER_CONTROL <= slv_reg5;
                RESET_CONTROL <= slv_reg6(0);
                ENABLE_CONTROL <= slv_reg7(0);
                CLK_ENABLE_CONTROL <= slv_reg7(1);
                CHANNEL_CONTROL <= slv_reg8;
                heart_beat_control <= slv_reg9(0);
                RESET_ENCODERS_CONTROL <= slv_reg10(0);
                HARDWARE_CONTROL <= slv_reg11;
                ADC_DEBUG <= slv_reg15;
                ADC_CONFIG_EX <= slv_reg16;
                ADC_CONFIG_EX_VALID <= slv_reg17(0);
                ADC_CONFIG_REQUEST_ACK <= slv_reg18(0);
                CC_CONFIG_ON <= slv_reg27(0);
                CC_CONFIG_ADDR <= slv_reg29(15 downto 0);
                CC_CONFIG_DATA <= slv_reg30(15 downto 0);
            end if;
          end if;                   
        end process; 
	
    process (S_AXI_ACLK)
    begin
      if rising_edge(S_AXI_ACLK) then 
        if S_AXI_ARESETN = '0' then
	        -- STATUS
            slv_reg256 <= (others => '0');
            slv_reg257 <= (others => '0');
            slv_reg258 <= (others => '0');
            slv_reg259 <= (others => '0');
            slv_reg260 <= (others => '0');
            slv_reg261 <= (others => '0');
            slv_reg262 <= (others => '0');
            slv_reg263 <= (others => '0');
            slv_reg264 <= (others => '0');
            slv_reg265 <= (others => '0');
            slv_reg266 <= (others => '0');
            slv_reg267 <= (others => '0');
            slv_reg268 <= (others => '0');
            slv_reg269 <= (others => '0');
            slv_reg270 <= (others => '0');
            slv_reg271 <= (others => '0');
            slv_reg272 <= (others => '0');
            slv_reg273 <= (others => '0');
            slv_reg275 <= (others => '0');
        else
	        -- STATUS
            slv_reg256 <= DMA_UPLOAD_STATUS;
            slv_reg257 <= DMA_DOWNLOAD_STATUS;
            slv_reg258 <= "0000000000000000000000000000000" & START_INSPECTION_STATUS;
            slv_reg259 <= SCAN_MODE_STATUS;
            slv_reg260 <= CSCAN_MODE_STATUS;
            slv_reg261 <= TRIGGER_STATUS;
            slv_reg262 <= "0000000000000000000000000000000" & RESET_STATUS;
            slv_reg263 <= "000000000000000000000000000000" & CLK_ENABLE_STATUS & ENABLE_STATUS;
            slv_reg264 <= CHANNEL_STATUS;
            slv_reg265 <= "0000000000000000000000000000000" & heart_beat_status;
            slv_reg266 <= "0000000000000000000000000000000" & RESET_ENCODERS_STATUS;   
            slv_reg267 <= TEMPERATURE_STATUS;
            slv_reg268 <= PULSER_STATUS;
            slv_reg269 <= "0000000000000000" & DSP_AVG_LEVEL;
            slv_reg270 <= "0000000000000000" & DSP_AVG_COUNTER;
            slv_reg271 <= DSP_AVG_TYPE;
            slv_reg272 <= DIGITAL_IO_STATUS;
            slv_reg273 <= VOLTAGE_STATUS;
            slv_reg275 <= "0000000000000000000000000000000" & CC_CONFIG_ON_ACK;
            
            -- DEBUG
            slv_reg384 <= CONFIGURATION_SIZE;
            slv_reg385 <= "0000000000000000000000000000000" & CONFIGURATION_ON;
            
            slv_reg386 <= CH0_RECEIVER_SAMPLING_FREQUENCY;
            slv_reg387 <= CH0_RECEIVER_DATA_WINDOW;
            slv_reg388 <= CH0_RECEIVER_DELAY;
            slv_reg389 <= CH0_RECEIVER_ANALOG_GAIN;
            slv_reg390 <= CH0_RECEIVER_DIGITAL_GAIN;
            slv_reg391 <= "0000000000000000000000000000000" & CH0_RECEIVER_EXTERNAL_MULTIPLEXER_EN;
            
            slv_reg392 <= CH0_TRANSMITTER_VOLTAGE;
            slv_reg393 <= CH0_TRANSMITTER_BURST_FREQUENCY;
            slv_reg394 <= CH0_TRANSMITTER_N_CYCLES;
            slv_reg395 <= CH0_TRANSMITTER_DELAY;
            slv_reg396 <= CH0_TRANSMITTER_DIRECTIONAL_PHASING;
            
            slv_reg397 <= CH0_MAGNET_MODE;
            slv_reg398 <= CH0_MAGNET_PULSE_WIDTH;
            slv_reg399 <= CH0_MAGNET_INITIAL_DELAY;
            
            slv_reg400 <= "0000000000000000000000000000" & CH0_DSP_ANALOG_FILTER;
            
            
            slv_reg416 <= CONFIGURATION_HEADER_SIZE;
            slv_reg417 <= CONFIGURATION_CHANNELS;
            slv_reg418 <= CONFIGURATION_CHANNEL0_OFFSET;
--            slv_reg419 <= CONFIGURATION_CHANNEL1_OFFSET;
            slv_reg420 <= CONFIGURATION_ENCODER_OFFSET;
            slv_reg421 <= CONFIGURATION_DIGITALIO_OFFSET;
            slv_reg422 <= CONFIGURATION_CHANNEL0_SIZE;
--            slv_reg423 <= CONFIGURATION_CHANNEL1_SIZE;
            slv_reg424 <= CONFIGURATION_TRIGGER_TYPE;
            slv_reg425 <= "0000000000000000000000000000000" & CH0_SIGNAL_INPUT_AXIS_TREADY;
--            slv_reg426 <= "0000000000000000000000000000000" & CH1_SIGNAL_INPUT_AXIS_TREADY;
            slv_reg427 <= "0000000000000000000000000000000" & CONFIGURATION_CHANNEL0_ENABLED;
--            slv_reg428 <= "0000000000000000000000000000000" & CONFIGURATION_CHANNEL1_ENABLED;  
            slv_reg429 <= "0000000000000000000000000000000" & CONFIGURATION_CHANNEL0_ON; 
--            slv_reg430 <= "0000000000000000000000000000000" & CONFIGURATION_CHANNEL1_ON;      
            
            slv_reg431 <= CH0_CHANNEL_TRIGGER_TYPE; 
            slv_reg432 <= "0000000000000000" & CH0_STATE_A; 
            slv_reg433 <= "0000000000000000" & CH0_STATE_B; 
            slv_reg434 <= "0000000000000000" & CH0_STATE_C; 
                                 
            slv_reg439 <= "0000000000000000" & MAIN_DMA_STATE; 
            
            slv_reg440 <= "0000000000000000" & CH0_DSP_COINC_LEVEL;
            slv_reg441 <= "0000000000000000" & CH0_DSP_AVG_LEVEL;
            
            slv_reg444 <= ENCODER1_COUNT;
            slv_reg445 <= ENCODER2_COUNT;
            slv_reg446 <= ENCODER_DEBUG;
                     
            slv_reg452 <= "0000000000000000" & ADC_WRITE_COUNTER;
            slv_reg453 <= "0000000000000000" & ADC_READ_COUNTER;
            
            slv_reg454 <= "0000000000000000000000000000000" & ADC_CONFIG_LOADED;
            slv_reg455 <= ADC_CONFIG_CURRENT;
            slv_reg456 <= "0000000000000000000000000000000" & ADC_CONFIG_REQUEST;          
        end if;
      end if;                   
    end process; 

	-- Implement write response logic generation
	-- The write response and response valid signals are asserted by the slave 
	-- when axi_wready, S_AXI_WVALID, axi_wready and S_AXI_WVALID are asserted.  
	-- This marks the acceptance of address and indicates the status of 
	-- write transaction.

	process (S_AXI_ACLK)
	begin
	  if rising_edge(S_AXI_ACLK) then 
	    if S_AXI_ARESETN = '0' then
	      axi_bvalid  <= '0';
	      axi_bresp   <= "00"; --need to work more on the responses
	    else
	      if (axi_awready = '1' and S_AXI_AWVALID = '1' and axi_wready = '1' and S_AXI_WVALID = '1' and axi_bvalid = '0'  ) then
	        axi_bvalid <= '1';
	        axi_bresp  <= "00"; 
	      elsif (S_AXI_BREADY = '1' and axi_bvalid = '1') then   --check if bready is asserted while bvalid is high)
	        axi_bvalid <= '0';                                 -- (there is a possibility that bready is always asserted high)
	      end if;
	    end if;
	  end if;                   
	end process; 

	-- Implement axi_arready generation
	-- axi_arready is asserted for one S_AXI_ACLK clock cycle when
	-- S_AXI_ARVALID is asserted. axi_awready is 
	-- de-asserted when reset (active low) is asserted. 
	-- The read address is also latched when S_AXI_ARVALID is 
	-- asserted. axi_araddr is reset to zero on reset assertion.

	process (S_AXI_ACLK)
	begin
	  if rising_edge(S_AXI_ACLK) then 
	    if S_AXI_ARESETN = '0' then
	      axi_arready <= '0';
	      axi_araddr  <= (others => '1');
	    else
	      if (axi_arready = '0' and S_AXI_ARVALID = '1') then
	        -- indicates that the slave has acceped the valid read address
	        axi_arready <= '1';
	        -- Read Address latching 
	        axi_araddr  <= S_AXI_ARADDR;           
	      else
	        axi_arready <= '0';
	      end if;
	    end if;
	  end if;                   
	end process; 

	-- Implement axi_arvalid generation
	-- axi_rvalid is asserted for one S_AXI_ACLK clock cycle when both 
	-- S_AXI_ARVALID and axi_arready are asserted. The slave registers 
	-- data are available on the axi_rdata bus at this instance. The 
	-- assertion of axi_rvalid marks the validity of read data on the 
	-- bus and axi_rresp indicates the status of read transaction.axi_rvalid 
	-- is deasserted on reset (active low). axi_rresp and axi_rdata are 
	-- cleared to zero on reset (active low).  
	process (S_AXI_ACLK)
	begin
	  if rising_edge(S_AXI_ACLK) then
	    if S_AXI_ARESETN = '0' then
	      axi_rvalid <= '0';
	      axi_rresp  <= "00";
	    else
	      if (axi_arready = '1' and S_AXI_ARVALID = '1' and axi_rvalid = '0') then
	        -- Valid read data is available at the read data bus
	        axi_rvalid <= '1';
	        axi_rresp  <= "00"; -- 'OKAY' response
	      elsif (axi_rvalid = '1' and S_AXI_RREADY = '1') then
	        -- Read data is accepted by the master
	        axi_rvalid <= '0';
	      end if;            
	    end if;
	  end if;
	end process;

	-- Implement memory mapped register select and read logic generation
	-- Slave register read enable is asserted when valid address is available
	-- and the slave is ready to accept the read address.
	slv_reg_rden <= axi_arready and S_AXI_ARVALID and (not axi_rvalid) ;

    process (slv_reg0, slv_reg1, slv_reg2, slv_reg3, slv_reg4, slv_reg5, slv_reg6, slv_reg7, slv_reg8, 
    slv_reg9, slv_reg10, slv_reg11, slv_reg12, slv_reg13, slv_reg14, slv_reg15, slv_reg16, slv_reg17, slv_reg18,
    slv_reg27, slv_reg28, slv_reg29, slv_reg30,
    
    slv_reg256, slv_reg257, slv_reg258, slv_reg259, slv_reg260, slv_reg261, 
    slv_reg262, slv_reg263, slv_reg264, slv_reg265, slv_reg266, slv_reg267, 
    slv_reg268, slv_reg269, slv_reg270, slv_reg271, slv_reg272, slv_reg273, slv_reg275,
    
    slv_reg384, slv_reg385, slv_reg386, slv_reg387, slv_reg388, slv_reg389,
    slv_reg390, slv_reg391, slv_reg392, slv_reg393, slv_reg394, slv_reg395, slv_reg396, slv_reg397, slv_reg398, slv_reg399,
    slv_reg400, slv_reg401, slv_reg402, slv_reg403, slv_reg404, slv_reg405, slv_reg406, slv_reg407, slv_reg408, slv_reg409,
    slv_reg410, slv_reg411, slv_reg412, slv_reg413, 
    
    slv_reg414, slv_reg415, slv_reg416, slv_reg417, slv_reg418, slv_reg419, slv_reg420, slv_reg421, slv_reg422, slv_reg423, 
    slv_reg424, slv_reg425, slv_reg426, slv_reg427, slv_reg428, slv_reg429, slv_reg430, slv_reg431, slv_reg432, slv_reg433,
    slv_reg434, slv_reg435, slv_reg436, slv_reg437, slv_reg438, slv_reg439, slv_reg440, slv_reg441, slv_reg442, slv_reg443,
    slv_reg444, slv_reg445, slv_reg446, slv_reg447, slv_reg448, slv_reg449, slv_reg450, slv_reg451, slv_reg452, slv_reg453,
    slv_reg454, slv_reg455, slv_reg456,
    
    axi_araddr, S_AXI_ARESETN, slv_reg_rden)
	variable loc_addr :std_logic_vector(OPT_MEM_ADDR_BITS downto 0);
	begin
	    -- Address decoding for reading registers
	    loc_addr := axi_araddr(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB);
	    case loc_addr is
	      -- CONTROL
	      when b"000000000" =>
	        reg_data_out <= slv_reg0;
	      when b"000000001" =>
	        reg_data_out <= slv_reg1;
	      when b"000000010" =>
	        reg_data_out <= slv_reg2;
	      when b"000000011" =>
	        reg_data_out <= slv_reg3;
	      when b"000000100" =>
	        reg_data_out <= slv_reg4;
	      when b"000000101" =>
	        reg_data_out <= slv_reg5;
	      when b"000000110" =>
	        reg_data_out <= slv_reg6;
	      when b"000000111" =>
	        reg_data_out <= slv_reg7;
	      when b"000001000" =>
	        reg_data_out <= slv_reg8;
	      when b"000001001" =>
	        reg_data_out <= slv_reg9;
	      when b"000001010" =>
	        reg_data_out <= slv_reg10;
	      when b"000001011" =>
	        reg_data_out <= slv_reg11;
	      when b"000001100" =>
	        reg_data_out <= slv_reg12;
	      when b"000001101" =>
	        reg_data_out <= slv_reg13;
	      when b"000001110" =>
	        reg_data_out <= slv_reg14;
	      when b"000001111" =>
	        reg_data_out <= slv_reg15;
	      when b"000010000" =>
            reg_data_out <= slv_reg16;
	      when b"000010001" =>
            reg_data_out <= slv_reg17;
	      when b"000010010" =>
            reg_data_out <= slv_reg18;
	      when b"000011011" =>
            reg_data_out <= slv_reg27;
	      when b"000011100" =>
            reg_data_out <= slv_reg28;
	      when b"000011101" =>
            reg_data_out <= slv_reg29;
	      when b"000011110" =>
            reg_data_out <= slv_reg30;
	      -- STATUS	    
	      when b"100000000" =>
	        reg_data_out <= slv_reg256;
	      when b"100000001" =>
	        reg_data_out <= slv_reg257;
	      when b"100000010" =>
	        reg_data_out <= slv_reg258;
	      when b"100000011" =>
	        reg_data_out <= slv_reg259;
	      when b"100000100" =>
	        reg_data_out <= slv_reg260;
	      when b"100000101" =>
	        reg_data_out <= slv_reg261;
	      when b"100000110" =>
	        reg_data_out <= slv_reg262;
	      when b"100000111" =>
	        reg_data_out <= slv_reg263;
	      when b"100001000" =>
	        reg_data_out <= slv_reg264;
	      when b"100001001" =>
	        reg_data_out <= slv_reg265;
	      when b"100001010" =>
	        reg_data_out <= slv_reg266;
	      when b"100001011" =>
	        reg_data_out <= slv_reg267;
	      when b"100001100" =>
	        reg_data_out <= slv_reg268;
	      when b"100001101" =>
	        reg_data_out <= slv_reg269;
	      when b"100001110" =>
	        reg_data_out <= slv_reg270;
	      when b"100001111" =>
	        reg_data_out <= slv_reg271;
	      when b"100010000" =>
            reg_data_out <= slv_reg272;
	      when b"100010001" =>
            reg_data_out <= slv_reg273;      
	      when b"100010011" =>
            reg_data_out <= slv_reg275;   
	        
	      -- DEBUG	    
          when b"110000000" =>
            reg_data_out <= slv_reg384;
          when b"110000001" =>
            reg_data_out <= slv_reg385;
          when b"110000010" =>
            reg_data_out <= slv_reg386;
          when b"110000011" =>
            reg_data_out <= slv_reg387;
          when b"110000100" =>
            reg_data_out <= slv_reg388;
          when b"110000101" =>
            reg_data_out <= slv_reg389;
            
	      when b"110000110" =>
            reg_data_out <= slv_reg390;
          when b"110000111" =>
            reg_data_out <= slv_reg391;
          when b"110001000" =>
            reg_data_out <= slv_reg392;
          when b"110001001" =>
            reg_data_out <= slv_reg393;
          when b"110001010" =>
            reg_data_out <= slv_reg394;
          when b"110001011" =>
            reg_data_out <= slv_reg395;
          when b"110001100" =>
            reg_data_out <= slv_reg396;
          when b"110001101" =>
            reg_data_out <= slv_reg397;
          when b"110001110" =>
            reg_data_out <= slv_reg398;
          when b"110001111" =>
            reg_data_out <= slv_reg399;
            
	      when b"110010000" =>
            reg_data_out <= slv_reg400;
          when b"110010001" =>
            reg_data_out <= slv_reg401;
          when b"110010010" =>
            reg_data_out <= slv_reg402;
          when b"110010011" =>
            reg_data_out <= slv_reg403;
          when b"110010100" =>
            reg_data_out <= slv_reg404;
          when b"110010101" =>
            reg_data_out <= slv_reg405;
          when b"110010110" =>
            reg_data_out <= slv_reg406;
          when b"110010111" =>
            reg_data_out <= slv_reg407;
          when b"110011000" =>
            reg_data_out <= slv_reg408;
          when b"110011001" =>
            reg_data_out <= slv_reg409;
            
	      when b"110011010" =>
            reg_data_out <= slv_reg410;
          when b"110011011" =>
            reg_data_out <= slv_reg411;
          when b"110011100" =>
            reg_data_out <= slv_reg412;
          when b"110011101" =>
            reg_data_out <= slv_reg413;
            
          when b"110011110" =>
            reg_data_out <= slv_reg414;              
          when b"110011111" =>
            reg_data_out <= slv_reg415; 
          when b"110100000" =>
            reg_data_out <= slv_reg416;   
          when b"110100001" =>
            reg_data_out <= slv_reg417;
          when b"110100010" =>
            reg_data_out <= slv_reg418;
          when b"110100011" =>
            reg_data_out <= slv_reg419;
          when b"110100100" =>
            reg_data_out <= slv_reg420;
          when b"110100101" =>
            reg_data_out <= slv_reg421;
          when b"110100110" =>
            reg_data_out <= slv_reg422;  
          when b"110100111" =>
            reg_data_out <= slv_reg423; 
          when b"110101000" =>
            reg_data_out <= slv_reg424; 
          when b"110101001" =>
            reg_data_out <= slv_reg425;
          when b"110101010" =>
            reg_data_out <= slv_reg426;  
          when b"110101011" =>
            reg_data_out <= slv_reg427;
          when b"110101100" =>
            reg_data_out <= slv_reg428;  
          when b"110101101" =>
            reg_data_out <= slv_reg429; 
          when b"110101110" =>
            reg_data_out <= slv_reg430; 
          when b"110101111" =>
            reg_data_out <= slv_reg431; 
          when b"110110000" =>
            reg_data_out <= slv_reg432;
          when b"110110001" =>
            reg_data_out <= slv_reg433;  
          when b"110110010" =>
            reg_data_out <= slv_reg434;
          when b"110110011" =>
            reg_data_out <= slv_reg435;  
          when b"110110100" =>
            reg_data_out <= slv_reg436;   
          when b"110110101" =>
            reg_data_out <= slv_reg437; 
          when b"110110110" =>
            reg_data_out <= slv_reg438;
          when b"110110111" =>
            reg_data_out <= slv_reg439;
          when b"110111000" =>
            reg_data_out <= slv_reg440;
          when b"110111001" =>
            reg_data_out <= slv_reg441;    
          when b"110111010" =>
            reg_data_out <= slv_reg442;   
          when b"110111011" =>
            reg_data_out <= slv_reg443;  
          when b"110111100" =>
            reg_data_out <= slv_reg444;
          when b"110111101" =>
            reg_data_out <= slv_reg445; 
          when b"110111110" =>
            reg_data_out <= slv_reg446;         
          when b"110111111" =>
            reg_data_out <= slv_reg447;  
          when b"111000000" =>
            reg_data_out <= slv_reg448;            
          when b"111000001" =>
            reg_data_out <= slv_reg449;                          
          when b"111000010" =>
            reg_data_out <= slv_reg450;                                    
          when b"111000011" =>
            reg_data_out <= slv_reg451;                                        
          when b"111000100" =>
            reg_data_out <= slv_reg452;                                        
          when b"111000101" =>
            reg_data_out <= slv_reg453;  
          when b"111000110" =>
            reg_data_out <= slv_reg454;  
          when b"111000111" =>
            reg_data_out <= slv_reg455;                
          when b"111001000" =>
            reg_data_out <= slv_reg456;                                  
                                                          
	      when others =>
	        reg_data_out  <= (others => '0');
	    end case;
	end process; 

	-- Output register or memory read data
	process( S_AXI_ACLK ) is
	begin
	  if (rising_edge (S_AXI_ACLK)) then
	    if ( S_AXI_ARESETN = '0' ) then
	      axi_rdata  <= (others => '0');
	    else
	      if (slv_reg_rden = '1') then
	        -- When there is a valid read address (S_AXI_ARVALID) with 
	        -- acceptance of read address by the slave (axi_arready), 
	        -- output the read dada 
	        -- Read address mux
	          axi_rdata <= reg_data_out;     -- register read data
	      end if;   
	    end if;
	  end if;
	end process;

	-- Add user logic here
	
    heart_beat_process: process (S_AXI_ACLK)
    begin
      if rising_edge(S_AXI_ACLK) then 
        if S_AXI_ARESETN = '0' then
            heart_beat_status <= '0';       
        else
            heart_beat_status <= heart_beat_control;
        end if;
      end if;                   
    end process; 
    
	-- User logic ends

end arch_imp;
