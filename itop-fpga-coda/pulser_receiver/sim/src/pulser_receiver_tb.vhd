library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity pulser_receiver_tb is
	generic (
  		REGISTER_W32    : INTEGER := 32;
  		REGISTER_W30    : INTEGER := 30;
  		REGISTER_W16    : INTEGER := 16;
  		REGISTER_W14    : INTEGER := 14;
  		REGISTER_W8     : INTEGER := 8;
  		REGISTER_W7     : INTEGER := 7;
  		REGISTER_W4     : INTEGER := 4;
  		REGISTER_W2     : INTEGER := 2;
  		CONFIG_DONE_WAIT: INTEGER := 4;
  		SAMPLE_FREQ     : INTEGER := 100;
  		N_CONFIG_REGISTERS : INTEGER := 5;
  		SPI_WAIT_CYCLES : INTEGER := 128;
  		SPI_2X_CLK_DIV  : INTEGER := 100       
  	);
end;

architecture bench of pulser_receiver_tb is

  component pulser_receiver
  	generic (
  		REGISTER_W32    : INTEGER := 32;
  		REGISTER_W30    : INTEGER := 30;
  		REGISTER_W16    : INTEGER := 16;
  		REGISTER_W14    : INTEGER := 14;
  		REGISTER_W8     : INTEGER := 8;
  		REGISTER_W7     : INTEGER := 7;
  		REGISTER_W4     : INTEGER := 4;
  		REGISTER_W2     : INTEGER := 2;
  		CONFIG_DONE_WAIT: INTEGER := 4;
  		SAMPLE_FREQ     : INTEGER := 100;
  		N_CONFIG_REGISTERS : INTEGER := 5;
  		SPI_WAIT_CYCLES : INTEGER := 128;
  		SPI_2X_CLK_DIV  : INTEGER := 100       
  	);
  	port (
  	    aclk_100 	: in STD_LOGIC;
  		aclk_200 	: in STD_LOGIC;
          aresetn		: in STD_LOGIC;
  		TX_DATA_DELAY			: in std_logic_vector(REGISTER_W32-1 downto 0);
  		RX_SAMPLING_FREQUENCY  	: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		RX_DATA_DELAY			: in std_logic_vector(REGISTER_W32-1 downto 0);
  		RX_DATA_WINDOW			: in std_logic_vector(REGISTER_W32-1 downto 0);
  		HALF_CYCLE_FREQ	: in std_logic_vector(REGISTER_W30-1 downto 0);
  		NO_HALF_CYCLES	: in std_logic_vector(REGISTER_W8-1 downto 0);
  		TXp		: out std_logic;
  		TXn		: out std_logic;
  		ADC_PAR_SERn : out STD_LOGIC;
  		ADC_ENCp     : out STD_LOGIC;
  		ADC_ENCn     : out STD_LOGIC;
  		ADC_CLKOUTp  : in STD_LOGIC;
  		ADC_CLKOUTn  : in STD_LOGIC;
  		ADC_OFp      : in STD_LOGIC;
  		ADC_OFn      : in STD_LOGIC;
  		ADC_DATAp    : in STD_LOGIC_VECTOR(REGISTER_W7-1 downto 0);
  		ADC_DATAn    : in STD_LOGIC_VECTOR(REGISTER_W7-1 downto 0);
  		ADC_CONFIG_EX     		: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		ADC_CONFIG_EX_VALID     : in STD_LOGIC;
  		ADC_CONFIG_LOADED 		: out STD_LOGIC;
  		ADC_CONFIG_CURRENT     	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		ADC_CONFIG_REQUEST		: out STD_LOGIC;
  		ADC_CONFIG_REQUEST_ACK	: in STD_LOGIC;
  		ADC_CONFIG_EX_DONE		: out STD_LOGIC;
  		ADC_SPI_CONFIG : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
  		ADC_CSn      : out STD_LOGIC;
  		ADC_SCK      : out STD_LOGIC;
  		ADC_SDI      : out STD_LOGIC;
  		ADC_SDO      : in STD_LOGIC;
  		CH_SIGNAL_AXIS_TREADY  					: in STD_LOGIC;
  		CH_SIGNAL_AXIS_TDATA	  				: out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  		CH_SIGNAL_AXIS_TLAST	  				: out STD_LOGIC;
  		CH_SIGNAL_AXIS_TVALID  					: out STD_LOGIC          
  	);
  end component;

  signal aclk_100: STD_LOGIC;
  signal aclk_200: STD_LOGIC;
  signal aresetn: STD_LOGIC;
  signal TX_DATA_DELAY: std_logic_vector(REGISTER_W32-1 downto 0);
  signal RX_SAMPLING_FREQUENCY: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal RX_DATA_DELAY: std_logic_vector(REGISTER_W32-1 downto 0);
  signal RX_DATA_WINDOW: std_logic_vector(REGISTER_W32-1 downto 0);
  signal HALF_CYCLE_FREQ: std_logic_vector(REGISTER_W30-1 downto 0);
  signal NO_HALF_CYCLES: std_logic_vector(REGISTER_W8-1 downto 0);
  signal TXp: std_logic;
  signal TXn: std_logic;
  signal ADC_PAR_SERn: STD_LOGIC;
  signal ADC_ENCp: STD_LOGIC;
  signal ADC_ENCn: STD_LOGIC;
  signal ADC_CLKOUTp: STD_LOGIC;
  signal ADC_CLKOUTn: STD_LOGIC;
  signal ADC_OFp: STD_LOGIC;
  signal ADC_OFn: STD_LOGIC;
  signal ADC_DATAp: STD_LOGIC_VECTOR(REGISTER_W7-1 downto 0);
  signal ADC_DATAn: STD_LOGIC_VECTOR(REGISTER_W7-1 downto 0);
  signal ADC_CONFIG_EX: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal ADC_CONFIG_EX_VALID: STD_LOGIC;
  signal ADC_CONFIG_LOADED: STD_LOGIC;
  signal ADC_CONFIG_CURRENT: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal ADC_CONFIG_REQUEST: STD_LOGIC;
  signal ADC_CONFIG_REQUEST_ACK: STD_LOGIC;
  signal ADC_CONFIG_EX_DONE: STD_LOGIC;
  signal ADC_SPI_CONFIG: STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
  signal ADC_CSn: STD_LOGIC;
  signal ADC_SCK: STD_LOGIC;
  signal ADC_SDI: STD_LOGIC;
  signal ADC_SDO: STD_LOGIC;
  signal CH_SIGNAL_AXIS_TREADY: STD_LOGIC;
  signal CH_SIGNAL_AXIS_TDATA: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  signal CH_SIGNAL_AXIS_TLAST: STD_LOGIC;
  signal CH_SIGNAL_AXIS_TVALID: STD_LOGIC ;

begin

  -- Insert values for generic parameters !!
  uut: pulser_receiver generic map ( REGISTER_W32           => REGISTER_W32,
                                     REGISTER_W30           => REGISTER_W30,
                                     REGISTER_W16           => REGISTER_W16,
                                     REGISTER_W14           => REGISTER_W14,
                                     REGISTER_W8            => REGISTER_W8,
                                     REGISTER_W7            => REGISTER_W7,
                                     REGISTER_W4            => REGISTER_W4,
                                     REGISTER_W2            => REGISTER_W2,
                                     CONFIG_DONE_WAIT       => CONFIG_DONE_WAIT,
                                     SAMPLE_FREQ            => SAMPLE_FREQ,
                                     N_CONFIG_REGISTERS     => N_CONFIG_REGISTERS,
                                     SPI_WAIT_CYCLES        => SPI_WAIT_CYCLES,
                                     SPI_2X_CLK_DIV         => SPI_2X_CLK_DIV )
                          port map ( aclk_100               => aclk_100,
                                     aclk_200               => aclk_200,
                                     aresetn                => aresetn,
                                     TX_DATA_DELAY          => TX_DATA_DELAY,
                                     RX_SAMPLING_FREQUENCY  => RX_SAMPLING_FREQUENCY,
                                     RX_DATA_DELAY          => RX_DATA_DELAY,
                                     RX_DATA_WINDOW         => RX_DATA_WINDOW,
                                     HALF_CYCLE_FREQ        => HALF_CYCLE_FREQ,
                                     NO_HALF_CYCLES         => NO_HALF_CYCLES,
                                     TXp                    => TXp,
                                     TXn                    => TXn,
                                     ADC_PAR_SERn           => ADC_PAR_SERn,
                                     ADC_ENCp               => ADC_ENCp,
                                     ADC_ENCn               => ADC_ENCn,
                                     ADC_CLKOUTp            => ADC_CLKOUTp,
                                     ADC_CLKOUTn            => ADC_CLKOUTn,
                                     ADC_OFp                => ADC_OFp,
                                     ADC_OFn                => ADC_OFn,
                                     ADC_DATAp              => ADC_DATAp,
                                     ADC_DATAn              => ADC_DATAn,
                                     ADC_CONFIG_EX          => ADC_CONFIG_EX,
                                     ADC_CONFIG_EX_VALID    => ADC_CONFIG_EX_VALID,
                                     ADC_CONFIG_LOADED      => ADC_CONFIG_LOADED,
                                     ADC_CONFIG_CURRENT     => ADC_CONFIG_CURRENT,
                                     ADC_CONFIG_REQUEST     => ADC_CONFIG_REQUEST,
                                     ADC_CONFIG_REQUEST_ACK => ADC_CONFIG_REQUEST_ACK,
                                     ADC_CONFIG_EX_DONE     => ADC_CONFIG_EX_DONE,
                                     ADC_SPI_CONFIG         => ADC_SPI_CONFIG,
                                     ADC_CSn                => ADC_CSn,
                                     ADC_SCK                => ADC_SCK,
                                     ADC_SDI                => ADC_SDI,
                                     ADC_SDO                => ADC_SDO,
                                     CH_SIGNAL_AXIS_TREADY  => CH_SIGNAL_AXIS_TREADY,
                                     CH_SIGNAL_AXIS_TDATA   => CH_SIGNAL_AXIS_TDATA,
                                     CH_SIGNAL_AXIS_TLAST   => CH_SIGNAL_AXIS_TLAST,
                                     CH_SIGNAL_AXIS_TVALID  => CH_SIGNAL_AXIS_TVALID );

  stimulus: process
  begin
  
    -- Put initialisation code here


    -- Put test bench stimulus code here

    wait;
  end process;


end;