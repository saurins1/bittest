library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity gates_processor_tb is
	generic (
  		REGISTER_W64  : INTEGER    := 64;
  		REGISTER_W48  : INTEGER    := 48;
        REGISTER_W32  : INTEGER    := 32;
        REGISTER_W16  : INTEGER    := 16;
        REGISTER_W8   : INTEGER    := 8;
        REGISTER_W4   : INTEGER    := 4;
        REGISTER_W2   : INTEGER    := 2
  	);
end;

architecture bench of gates_processor_tb is

  component gates_processor
  	generic (
  		REGISTER_W64  : INTEGER    := 64;
  		REGISTER_W48  : INTEGER    := 48;
        REGISTER_W32  : INTEGER    := 32;
        REGISTER_W16  : INTEGER    := 16;
        REGISTER_W8   : INTEGER    := 8;
        REGISTER_W4   : INTEGER    := 4;
        REGISTER_W2   : INTEGER    := 2
  	);
  	port (
  	      aclk           : in STD_LOGIC;
          aresetn        : in STD_LOGIC;
		
  	      RX_DELAY       : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  	      TX_DELAY       : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
		
  		  GATE1_EN                          : in STD_LOGIC;  
  		  GATE1_START                       : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  		  GATE1_WIDTH                       : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);	
          GATE1_AMP_THRESHOLD               : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
       	  GATE1_AMP_THRESHOLD_ALARM_EN      : in STD_LOGIC; 
          GATE1_AMP_THRESHOLD_ALARM_CROSSING: in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
          GATE1_TOF_ALGORITHM               : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
          GATE1_TOF_AVG                     : in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
          GATE1_TOF_MIN_THICKNESS           : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
          GATE1_TOF_ALARM_EN                : in STD_LOGIC; 
          GATE1_TOF_ALARM_TYPE              : in STD_LOGIC;
          GATE1_TOF_ALARM_CROSSING          : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
          GATE1_TOF_ALARM_MIN_VALUE         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
          GATE1_TOF_ALARM_MAX_VALUE         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
          GATE1_TOF                         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
          GATE1_TOF_AVERAGED                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
          GATE1_AMP_ALARM                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
          GATE1_TOF_ALARM                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
          GATE1_MAX                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
          GATE1_MAX_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
          GATE1_MIN                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
          GATE1_MIN_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		
          GATE2_EN                          : in STD_LOGIC;  
          GATE2_START                       : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
          GATE2_WIDTH                       : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);    
          GATE2_AMP_THRESHOLD               : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
          GATE2_AMP_THRESHOLD_ALARM_EN      : in STD_LOGIC; 
          GATE2_AMP_THRESHOLD_ALARM_CROSSING: in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
          GATE2_TOF_ALGORITHM               : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
          GATE2_TOF_AVG                     : in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
          GATE2_TOF_MIN_THICKNESS           : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
          GATE2_TOF_ALARM_EN                : in STD_LOGIC; 
          GATE2_TOF_ALARM_TYPE              : in STD_LOGIC;
          GATE2_TOF_ALARM_CROSSING          : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
          GATE2_TOF_ALARM_MIN_VALUE         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
          GATE2_TOF_ALARM_MAX_VALUE         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
          GATE2_TOF                         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
          GATE2_TOF_AVERAGED                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
          GATE2_AMP_ALARM                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
          GATE2_TOF_ALARM                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
          GATE2_MAX                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
          GATE2_MAX_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
          GATE2_MIN                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
          GATE2_MIN_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
          GATE2_THR                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
          GATE2_THR_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
          GATE2_FFT_EN                      : in STD_LOGIC;
          GATE2_CC_EN                       : in STD_LOGIC;
          GATE2_FFT_WIDTH                   : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
          GATE2_FFT_MAX_VALUE               : out STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
          GATE2_FFT_MAX_SAMPLE              : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
          GATE2_CC_MAX_VALUE                : out STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
          GATE2_CC_MAX_SAMPLE               : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
          GATE2_FFT_CUADRATIC_ERROR         : out STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
          GATE2_CC_CONFIG_ON_ACK            : out STD_LOGIC;
          GATE2_CC_CONFIG_ON                : in STD_LOGIC;        
          GATE2_CC_CONFIG_ADDR              : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
          GATE2_CC_CONFIG_DATA              : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0); 
		
          GATE3_EN                          : in STD_LOGIC;  
          GATE3_START                       : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
          GATE3_WIDTH                       : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);    
          GATE3_AMP_THRESHOLD               : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
          GATE3_AMP_THRESHOLD_ALARM_EN      : in STD_LOGIC; 
          GATE3_AMP_THRESHOLD_ALARM_CROSSING: in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
          GATE3_TOF_ALGORITHM               : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
          GATE3_TOF_AVG                     : in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
          GATE3_TOF_MIN_THICKNESS           : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
          GATE3_TOF_ALARM_EN                : in STD_LOGIC; 
          GATE3_TOF_ALARM_TYPE              : in STD_LOGIC;
          GATE3_TOF_ALARM_CROSSING          : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
          GATE3_TOF_ALARM_MIN_VALUE         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
          GATE3_TOF_ALARM_MAX_VALUE         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
          GATE3_TOF                         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
          GATE3_TOF_AVERAGED                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
          GATE3_AMP_ALARM                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
          GATE3_TOF_ALARM                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
          GATE3_MAX                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
          GATE3_MAX_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
          GATE3_MIN                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
          GATE3_MIN_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
          GATES_CONFIG : in STD_LOGIC;
  		  GATES_SAMPLE_START: out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
          GATES_SAMPLE_END  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
          GATES_EN          : out STD_LOGIC;
          GATES_END         : out STD_LOGIC;
          READ_GATES_RESULT : in STD_LOGIC;
  		  GATES_AXIS_TREADY : out STD_LOGIC;
  		  GATES_AXIS_TDATA  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  		  GATES_AXIS_TLAST  : in STD_LOGIC;
  		  GATES_AXIS_TVALID : in STD_LOGIC;
          CORRELATION_EN              	: out STD_LOGIC;
          CORRELATION_GATE_SAMPLE_START   : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
          CORRELATION_GATE_SAMPLE_END     : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
          CORRELATION_AXIS_TREADY     	: out STD_LOGIC;
          CORRELATION_AXIS_TDATA      	: in STD_LOGIC_VECTOR(REGISTER_W48-1 downto 0);
          CORRELATION_AXIS_TLAST      	: in STD_LOGIC;
          CORRELATION_AXIS_TVALID     	: in STD_LOGIC
  	);
  end component;

  signal aclk: STD_LOGIC:= '0';
  signal aresetn: STD_LOGIC:= '0';

  signal RX_DELAY: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal TX_DELAY: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

  signal GATE1_EN: STD_LOGIC:= '0';
  signal GATE1_START: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0');
  signal GATE1_WIDTH: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0');
  signal GATE1_AMP_THRESHOLD: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0');
  signal GATE1_AMP_THRESHOLD_ALARM_EN: STD_LOGIC:= '0';
  signal GATE1_AMP_THRESHOLD_ALARM_CROSSING: STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0):= (others => '0');
  signal GATE1_TOF_ALGORITHM: STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0):= (others => '0');
  signal GATE1_TOF_AVG: STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0):= (others => '0');
  signal GATE1_TOF_MIN_THICKNESS: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0');
  signal GATE1_TOF_ALARM_EN: STD_LOGIC:= '0';
  signal GATE1_TOF_ALARM_TYPE: STD_LOGIC:= '0';
  signal GATE1_TOF_ALARM_CROSSING: STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0):= (others => '0');
  signal GATE1_TOF_ALARM_MIN_VALUE: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');
  signal GATE1_TOF_ALARM_MAX_VALUE: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');

  signal GATE1_TOF: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal GATE1_TOF_AVERAGED: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal GATE1_AMP_ALARM: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal GATE1_TOF_ALARM: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal GATE1_MAX: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  signal GATE1_MAX_SAMPLE: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  signal GATE1_MIN: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  signal GATE1_MIN_SAMPLE: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);

  signal GATE2_EN: STD_LOGIC:= '0';
  signal GATE2_START: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0');
  signal GATE2_WIDTH: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0');
  signal GATE2_AMP_THRESHOLD: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0');
  signal GATE2_AMP_THRESHOLD_ALARM_EN: STD_LOGIC:= '0';
  signal GATE2_AMP_THRESHOLD_ALARM_CROSSING: STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0):= (others => '0');
  signal GATE2_TOF_ALGORITHM: STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0):= (others => '0');
  signal GATE2_TOF_AVG: STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0):= (others => '0');
  signal GATE2_TOF_MIN_THICKNESS: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0');
  signal GATE2_TOF_ALARM_EN: STD_LOGIC:= '0';
  signal GATE2_TOF_ALARM_TYPE: STD_LOGIC:= '0';
  signal GATE2_TOF_ALARM_CROSSING: STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0):= (others => '0');
  signal GATE2_TOF_ALARM_MIN_VALUE: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');
  signal GATE2_TOF_ALARM_MAX_VALUE: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');

  signal GATE2_TOF: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal GATE2_TOF_AVERAGED: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal GATE2_AMP_ALARM: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal GATE2_TOF_ALARM: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal GATE2_MAX: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  signal GATE2_MAX_SAMPLE: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  signal GATE2_MIN: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  signal GATE2_MIN_SAMPLE: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  
  signal GATE2_THR                         : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  signal GATE2_THR_SAMPLE                  : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  signal GATE2_FFT_EN                      : STD_LOGIC:= '0';
  signal GATE2_CC_EN                       : STD_LOGIC:= '0';
  signal GATE2_FFT_WIDTH                   : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0');
  signal GATE2_FFT_MAX_VALUE               : STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
  signal GATE2_FFT_MAX_SAMPLE              : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal GATE2_CC_MAX_VALUE                : STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
  signal GATE2_CC_MAX_SAMPLE               : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal GATE2_FFT_CUADRATIC_ERROR         : STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
  signal GATE2_CC_CONFIG_ON_ACK            : STD_LOGIC;
  signal GATE2_CC_CONFIG_ON                : STD_LOGIC:= '0';        
  signal GATE2_CC_CONFIG_ADDR              : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0');
  signal GATE2_CC_CONFIG_DATA              : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0');
  
  signal GATE2_CC_CONFIG_ADDR_first     : std_logic:= '0'; 

  signal GATE3_EN: STD_LOGIC:= '0';
  signal GATE3_START: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0');
  signal GATE3_WIDTH: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0');
  signal GATE3_AMP_THRESHOLD: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0');
  signal GATE3_AMP_THRESHOLD_ALARM_EN: STD_LOGIC:= '0';
  signal GATE3_AMP_THRESHOLD_ALARM_CROSSING: STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0):= (others => '0');
  signal GATE3_TOF_ALGORITHM: STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0):= (others => '0');
  signal GATE3_TOF_AVG: STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0):= (others => '0');
  signal GATE3_TOF_MIN_THICKNESS: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0');
  signal GATE3_TOF_ALARM_EN: STD_LOGIC:= '0';
  signal GATE3_TOF_ALARM_TYPE: STD_LOGIC:= '0';
  signal GATE3_TOF_ALARM_CROSSING: STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0):= (others => '0');
  signal GATE3_TOF_ALARM_MIN_VALUE: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');
  signal GATE3_TOF_ALARM_MAX_VALUE: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');

  signal GATE3_TOF: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal GATE3_TOF_AVERAGED: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal GATE3_AMP_ALARM: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal GATE3_TOF_ALARM: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal GATE3_MAX: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  signal GATE3_MAX_SAMPLE: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  signal GATE3_MIN: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  signal GATE3_MIN_SAMPLE: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);

  signal GATES_CONFIG : STD_LOGIC:='0';
  signal GATES_SAMPLE_START: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  signal GATES_SAMPLE_END: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  signal GATES_EN: STD_LOGIC;
  signal GATES_END: STD_LOGIC;

  signal READ_GATES_RESULT: STD_LOGIC:= '0';

  signal GATES_AXIS_TREADY: STD_LOGIC;
  signal GATES_AXIS_TDATA: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0');
  signal GATES_AXIS_TLAST: STD_LOGIC:= '0';
  signal GATES_AXIS_TVALID: STD_LOGIC:= '0';

  signal CORRELATION_EN: STD_LOGIC;
  signal CORRELATION_GATE_SAMPLE_START: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  signal CORRELATION_GATE_SAMPLE_END: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);

  signal CORRELATION_AXIS_TREADY: STD_LOGIC;
  signal CORRELATION_AXIS_TDATA: STD_LOGIC_VECTOR(REGISTER_W48-1 downto 0):= (others => '0');
  signal CORRELATION_AXIS_TLAST: STD_LOGIC:= '0';
  signal CORRELATION_AXIS_TVALID: STD_LOGIC:= '0';

  constant aclk_period: time := 10 ns;

  type states_sm is (idle, open_files, load_gates, load_correlation, wait_gates, read_result, read_result_end, update_gates_en_1, update_gates_en_2, close_files); 
  signal state_sm : states_sm; 

  signal start_sm: STD_LOGIC:= '0';

 signal correlation_size: UNSIGNED(REGISTER_W32-1 downto 0):= (others => '0');
 signal gates_en_i: STD_LOGIC_VECTOR(2 downto 0):= (others => '0');
 
 type states_signal_sm is (idle, open_file, load_signal, load_signal_wait, close_file); 
 signal state_cc_signal_sm : states_signal_sm; 
 
  signal start_cc_sm: STD_LOGIC:= '0';
 
 signal load_signal_counter: unsigned(REGISTER_W16-1 downto 0):= (others => '0');
 
 signal FFT_WIDTH  : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0');

begin
	
	   -- aclk process 
    aclk_process :process
    begin
        aclk <= not aclk;
        wait for aclk_period/2;
        aclk <= not aclk;
        wait for aclk_period/2;
    end process;

  -- Insert values for generic parameters !!
  uut: gates_processor generic map ( REGISTER_W64                       => REGISTER_W64,
                           REGISTER_W48                       => REGISTER_W48,
                           REGISTER_W32                       => REGISTER_W32,
                           REGISTER_W16                       => REGISTER_W16,
                           REGISTER_W8                        => REGISTER_W8,
                           REGISTER_W4                        => REGISTER_W4,
                           REGISTER_W2                        => REGISTER_W2 )
                port map ( aclk                               => aclk,
                           aresetn                            => aresetn,
                           RX_DELAY                           => RX_DELAY,
                           TX_DELAY                           => TX_DELAY,
                           GATE1_EN                           => GATE1_EN,
                           GATE1_START                        => GATE1_START,
                           GATE1_WIDTH                        => GATE1_WIDTH,
                           GATE1_AMP_THRESHOLD                => GATE1_AMP_THRESHOLD,
                           GATE1_AMP_THRESHOLD_ALARM_EN       => GATE1_AMP_THRESHOLD_ALARM_EN,
                           GATE1_AMP_THRESHOLD_ALARM_CROSSING => GATE1_AMP_THRESHOLD_ALARM_CROSSING,
                           GATE1_TOF_ALGORITHM                => GATE1_TOF_ALGORITHM,
                           GATE1_TOF_AVG                      => GATE1_TOF_AVG,
                           GATE1_TOF_MIN_THICKNESS            => GATE1_TOF_MIN_THICKNESS,
                           GATE1_TOF_ALARM_EN                 => GATE1_TOF_ALARM_EN,
                           GATE1_TOF_ALARM_TYPE               => GATE1_TOF_ALARM_TYPE,
                           GATE1_TOF_ALARM_CROSSING           => GATE1_TOF_ALARM_CROSSING,
                           GATE1_TOF_ALARM_MIN_VALUE          => GATE1_TOF_ALARM_MIN_VALUE,
                           GATE1_TOF_ALARM_MAX_VALUE          => GATE1_TOF_ALARM_MAX_VALUE,
                           GATE1_TOF                          => GATE1_TOF,
                           GATE1_TOF_AVERAGED                 => GATE1_TOF_AVERAGED,
                           GATE1_AMP_ALARM                    => GATE1_AMP_ALARM,
                           GATE1_TOF_ALARM                    => GATE1_TOF_ALARM,
                           GATE1_MAX                          => GATE1_MAX,
                           GATE1_MAX_SAMPLE                   => GATE1_MAX_SAMPLE,
                           GATE1_MIN                          => GATE1_MIN,
                           GATE1_MIN_SAMPLE                   => GATE1_MIN_SAMPLE,
                           GATE2_EN                           => GATE2_EN,
                           GATE2_START                        => GATE2_START,
                           GATE2_WIDTH                        => GATE2_WIDTH,
                           GATE2_AMP_THRESHOLD                => GATE2_AMP_THRESHOLD,
                           GATE2_AMP_THRESHOLD_ALARM_EN       => GATE2_AMP_THRESHOLD_ALARM_EN,
                           GATE2_AMP_THRESHOLD_ALARM_CROSSING => GATE2_AMP_THRESHOLD_ALARM_CROSSING,
                           GATE2_TOF_ALGORITHM                => GATE2_TOF_ALGORITHM,
                           GATE2_TOF_AVG                      => GATE2_TOF_AVG,
                           GATE2_TOF_MIN_THICKNESS            => GATE2_TOF_MIN_THICKNESS,
                           GATE2_TOF_ALARM_EN                 => GATE2_TOF_ALARM_EN,
                           GATE2_TOF_ALARM_TYPE               => GATE2_TOF_ALARM_TYPE,
                           GATE2_TOF_ALARM_CROSSING           => GATE2_TOF_ALARM_CROSSING,
                           GATE2_TOF_ALARM_MIN_VALUE          => GATE2_TOF_ALARM_MIN_VALUE,
                           GATE2_TOF_ALARM_MAX_VALUE          => GATE2_TOF_ALARM_MAX_VALUE,
                           GATE2_TOF                          => GATE2_TOF,
                           GATE2_TOF_AVERAGED                 => GATE2_TOF_AVERAGED,
                           GATE2_AMP_ALARM                    => GATE2_AMP_ALARM,
                           GATE2_TOF_ALARM                    => GATE2_TOF_ALARM,
                           GATE2_MAX                          => GATE2_MAX,
                           GATE2_MAX_SAMPLE                   => GATE2_MAX_SAMPLE,
                           GATE2_MIN                          => GATE2_MIN,
                           GATE2_MIN_SAMPLE                   => GATE2_MIN_SAMPLE,
                           
                           GATE2_THR                          => GATE2_THR,
                           GATE2_THR_SAMPLE                   => GATE2_THR_SAMPLE,
                           GATE2_FFT_EN                       => GATE2_FFT_EN,
                           GATE2_CC_EN                        => GATE2_CC_EN,
                           GATE2_FFT_WIDTH                    => GATE2_FFT_WIDTH,
                           GATE2_FFT_MAX_VALUE                => GATE2_FFT_MAX_VALUE,
                           GATE2_FFT_MAX_SAMPLE               => GATE2_FFT_MAX_SAMPLE,
                           GATE2_CC_MAX_VALUE                 => GATE2_CC_MAX_VALUE,
                           GATE2_CC_MAX_SAMPLE                => GATE2_CC_MAX_SAMPLE,
                           GATE2_FFT_CUADRATIC_ERROR          => GATE2_FFT_CUADRATIC_ERROR,
                           GATE2_CC_CONFIG_ON_ACK             => GATE2_CC_CONFIG_ON_ACK,
                           GATE2_CC_CONFIG_ON                 => GATE2_CC_CONFIG_ON,
                           GATE2_CC_CONFIG_ADDR               => GATE2_CC_CONFIG_ADDR,
                           GATE2_CC_CONFIG_DATA               => GATE2_CC_CONFIG_DATA,
                           
                           GATE3_EN                           => GATE3_EN,
                           GATE3_START                        => GATE3_START,
                           GATE3_WIDTH                        => GATE3_WIDTH,
                           GATE3_AMP_THRESHOLD                => GATE3_AMP_THRESHOLD,
                           GATE3_AMP_THRESHOLD_ALARM_EN       => GATE3_AMP_THRESHOLD_ALARM_EN,
                           GATE3_AMP_THRESHOLD_ALARM_CROSSING => GATE3_AMP_THRESHOLD_ALARM_CROSSING,
                           GATE3_TOF_ALGORITHM                => GATE3_TOF_ALGORITHM,
                           GATE3_TOF_AVG                      => GATE3_TOF_AVG,
                           GATE3_TOF_MIN_THICKNESS            => GATE3_TOF_MIN_THICKNESS,
                           GATE3_TOF_ALARM_EN                 => GATE3_TOF_ALARM_EN,
                           GATE3_TOF_ALARM_TYPE               => GATE3_TOF_ALARM_TYPE,
                           GATE3_TOF_ALARM_CROSSING           => GATE3_TOF_ALARM_CROSSING,
                           GATE3_TOF_ALARM_MIN_VALUE          => GATE3_TOF_ALARM_MIN_VALUE,
                           GATE3_TOF_ALARM_MAX_VALUE          => GATE3_TOF_ALARM_MAX_VALUE,
                           GATE3_TOF                          => GATE3_TOF,
                           GATE3_TOF_AVERAGED                 => GATE3_TOF_AVERAGED,
                           GATE3_AMP_ALARM                    => GATE3_AMP_ALARM,
                           GATE3_TOF_ALARM                    => GATE3_TOF_ALARM,
                           GATE3_MAX                          => GATE3_MAX,
                           GATE3_MAX_SAMPLE                   => GATE3_MAX_SAMPLE,
                           GATE3_MIN                          => GATE3_MIN,
                           GATE3_MIN_SAMPLE                   => GATE3_MIN_SAMPLE,
                           GATES_CONFIG                       => GATES_CONFIG,
                           GATES_SAMPLE_START                 => GATES_SAMPLE_START,
                           GATES_SAMPLE_END                   => GATES_SAMPLE_END,
                           GATES_EN                           => GATES_EN,
                           GATES_END                          => GATES_END,
                           READ_GATES_RESULT                  => READ_GATES_RESULT,
                           GATES_AXIS_TREADY                  => GATES_AXIS_TREADY,
                           GATES_AXIS_TDATA                   => GATES_AXIS_TDATA,
                           GATES_AXIS_TLAST                   => GATES_AXIS_TLAST,
                           GATES_AXIS_TVALID                  => GATES_AXIS_TVALID,
                           CORRELATION_EN                     => CORRELATION_EN,
                           CORRELATION_GATE_SAMPLE_START      => CORRELATION_GATE_SAMPLE_START,
                           CORRELATION_GATE_SAMPLE_END        => CORRELATION_GATE_SAMPLE_END,
                           CORRELATION_AXIS_TREADY            => CORRELATION_AXIS_TREADY,
                           CORRELATION_AXIS_TDATA             => CORRELATION_AXIS_TDATA,
                           CORRELATION_AXIS_TLAST             => CORRELATION_AXIS_TLAST,
                           CORRELATION_AXIS_TVALID            => CORRELATION_AXIS_TVALID );

	  stimulus: process
	  begin
		aresetn <= '0';
		start_cc_sm <= '0';
		GATES_CONFIG <= '0';
		wait for aclk_period*10;
		aresetn <= '1';
		wait for aclk_period*10;
		GATES_CONFIG <= '1';
		wait for aclk_period*10;
		GATES_CONFIG <= '0';
		wait for aclk_period*1000;
		start_cc_sm <= '1';
		wait for aclk_period*10;
		start_cc_sm <= '0';
		wait for aclk_period*1000000;
		start_sm <= '1';
		wait for aclk_period*10;
		start_sm <= '0';
		wait;
	  end process;

	correlation_size <= unsigned(CORRELATION_GATE_SAMPLE_END) - unsigned(CORRELATION_GATE_SAMPLE_START) + 1;

    process_inst: process(aclk, aresetn)  
        variable my_counter  : integer range 0 to 16383 :=0;
        
        type signalDataFile is file of integer;
        file signal_data_file: signalDataFile;
        variable signal_data: integer;     
        variable signal_data_file_status: file_open_status;

		type signalCorrelationFile is file of integer;
        file signal_correlation_file: signalCorrelationFile;
        variable signal_correlation: integer;     
        variable signal_correlation_file_status: file_open_status; 

    begin 
        if (aresetn = '0') then
			-- RX DELAY
			RX_DELAY <= (others => '0');
			TX_DELAY <= (others => '0');

			-- GATE 1
			GATE1_EN                          <= '1';
			GATE1_START                       <= std_logic_vector(to_unsigned(400, GATE1_START'length));
			GATE1_WIDTH                       <= std_logic_vector(to_unsigned(800, GATE1_WIDTH'length));
			GATE1_AMP_THRESHOLD               <= std_logic_vector(to_unsigned(600, GATE1_AMP_THRESHOLD'length));
			GATE1_AMP_THRESHOLD_ALARM_EN      <= '1';
			GATE1_AMP_THRESHOLD_ALARM_CROSSING<= std_logic_vector(to_unsigned(1, GATE1_AMP_THRESHOLD_ALARM_CROSSING'length));
			GATE1_TOF_ALGORITHM               <= std_logic_vector(to_unsigned(1, GATE1_TOF_ALGORITHM'length));
			GATE1_TOF_AVG                     <= std_logic_vector(to_unsigned(0, GATE1_TOF_AVG'length));
			GATE1_TOF_MIN_THICKNESS           <= std_logic_vector(to_unsigned(5000, GATE1_TOF_MIN_THICKNESS'length));
			GATE1_TOF_ALARM_EN                <= '0';
			GATE1_TOF_ALARM_TYPE              <= '1';
			GATE1_TOF_ALARM_CROSSING          <= std_logic_vector(to_unsigned(1, GATE1_TOF_ALARM_CROSSING'length));
			GATE1_TOF_ALARM_MIN_VALUE         <= std_logic_vector(to_unsigned(400*1024, GATE1_TOF_ALARM_MIN_VALUE'length));
			GATE1_TOF_ALARM_MAX_VALUE         <= std_logic_vector(to_unsigned(500*1024, GATE1_TOF_ALARM_MAX_VALUE'length));
																			  																		  
			-- GATE 2
			GATE2_EN                          <= '1';
			GATE2_START                       <= std_logic_vector(to_unsigned(300, GATE2_START'length));
			GATE2_WIDTH                       <= std_logic_vector(to_unsigned(9300, GATE2_WIDTH'length));
			GATE2_AMP_THRESHOLD               <= std_logic_vector(to_unsigned(600, GATE2_AMP_THRESHOLD'length));
			GATE2_AMP_THRESHOLD_ALARM_EN      <= '1';
			GATE2_AMP_THRESHOLD_ALARM_CROSSING<= std_logic_vector(to_unsigned(2, GATE2_AMP_THRESHOLD_ALARM_CROSSING'length));
			GATE2_TOF_ALGORITHM               <= std_logic_vector(to_unsigned(3, GATE2_TOF_ALGORITHM'length)); -- peak peak
			--GATE2_TOF_ALGORITHM               <= std_logic_vector(to_unsigned(2, GATE2_TOF_ALGORITHM'length));
			GATE2_TOF_AVG                     <= std_logic_vector(to_unsigned(0, GATE2_TOF_AVG'length));
			GATE2_TOF_MIN_THICKNESS           <= std_logic_vector(to_unsigned(5000, GATE2_TOF_MIN_THICKNESS'length));
			GATE2_TOF_ALARM_EN                <= '0';
			GATE2_TOF_ALARM_TYPE              <= '1';
			GATE2_TOF_ALARM_CROSSING          <= std_logic_vector(to_unsigned(1, GATE2_TOF_ALARM_CROSSING'length));
			GATE2_TOF_ALARM_MIN_VALUE         <= std_logic_vector(to_unsigned(400*1024, GATE2_TOF_ALARM_MIN_VALUE'length));
			GATE2_TOF_ALARM_MAX_VALUE         <= std_logic_vector(to_unsigned(500*1024, GATE2_TOF_ALARM_MAX_VALUE'length));

			-- GATE 3
			GATE3_EN                          <= '1';
			GATE3_START                       <= std_logic_vector(to_unsigned(8000, GATE2_START'length));
			GATE3_WIDTH                       <= std_logic_vector(to_unsigned(1500, GATE2_WIDTH'length));
			GATE3_AMP_THRESHOLD               <= std_logic_vector(to_unsigned(600, GATE2_AMP_THRESHOLD'length));
			GATE3_AMP_THRESHOLD_ALARM_EN      <= '1';
			GATE3_AMP_THRESHOLD_ALARM_CROSSING<= std_logic_vector(to_unsigned(1, GATE2_AMP_THRESHOLD_ALARM_CROSSING'length));
			GATE3_TOF_ALGORITHM               <= std_logic_vector(to_unsigned(2, GATE2_TOF_ALGORITHM'length));
			GATE3_TOF_AVG                     <= std_logic_vector(to_unsigned(0, GATE2_TOF_AVG'length));
			GATE3_TOF_MIN_THICKNESS           <= std_logic_vector(to_unsigned(5000, GATE2_TOF_MIN_THICKNESS'length));
			GATE3_TOF_ALARM_EN                <= '0';
			GATE3_TOF_ALARM_TYPE              <= '1';
			GATE3_TOF_ALARM_CROSSING          <= std_logic_vector(to_unsigned(2, GATE2_TOF_ALARM_CROSSING'length));
			GATE3_TOF_ALARM_MIN_VALUE         <= std_logic_vector(to_unsigned(400*1024, GATE2_TOF_ALARM_MIN_VALUE'length));
			GATE3_TOF_ALARM_MAX_VALUE         <= std_logic_vector(to_unsigned(500*1024, GATE2_TOF_ALARM_MAX_VALUE'length));
																			  
			-- GATES AXIS
			GATES_AXIS_TDATA <= (others => '0');
			GATES_AXIS_TLAST <= '0';
			GATES_AXIS_TVALID <= '0';

			-- GATE_CORRELATION AXIS
			CORRELATION_AXIS_TDATA  <= (others => '0');
			CORRELATION_AXIS_TLAST  <= '0';
			CORRELATION_AXIS_TVALID <= '0';
																			  
			-- READ_GATES
			READ_GATES_RESULT <= '0';
			
			-- GATES EN
			gates_en_i <= (others => '0');
			
			GATES_CONFIG <= '0';

            state_sm <= idle;  
        elsif (aclk'event and aclk = '1') then
            case state_sm is  
                when idle =>         
                    if(start_sm = '1') then
                        my_counter := 0;
                        state_sm <= open_files;        
                    end if;
                when open_files => 
                    file_open(signal_data_file_status, signal_data_file, "ascan.dat", read_mode);
					file_open(signal_correlation_file_status, signal_correlation_file, "correlation.dat", read_mode);																	  
                    state_sm <= load_gates;                                                                         
                when load_gates =>
                    if(GATES_EN = '1') then
                        if(GATES_AXIS_TREADY = '1') then
                            if(my_counter <= to_integer(unsigned(GATES_SAMPLE_END))) then
                                if not endfile(signal_data_file) then
                                    read (signal_data_file, signal_data);
                                    GATES_AXIS_TDATA<=std_logic_vector(to_signed(signal_data, GATES_AXIS_TDATA'length));
                                end if;
                                if not endfile(signal_data_file) then
                                    read (signal_data_file, signal_data);
                                end if;
                                if(my_counter >= to_integer(unsigned(GATES_SAMPLE_START))) then
                                    GATES_AXIS_TVALID <= '1';
                                    if(my_counter >= to_integer(unsigned(GATES_SAMPLE_END))) then
                                        GATES_AXIS_TLAST <= '1';
                                        my_counter  := 0;												
                                        state_sm    <= wait_gates;	
                                    end if;
                                end if;
                                my_counter := my_counter + 1;           
                            end if;  
                        end if;
                    else
                        state_sm   <= update_gates_en_1;      
                    end if;
                when wait_gates =>
                    GATES_AXIS_TVALID <= '0';
                    GATES_AXIS_TLAST <= '0';
                    if(CORRELATION_EN = '1') then
             			state_sm    <= load_correlation;
              		else
                		state_sm    <= read_result; 
                    end if;
                when load_correlation =>																		 
                    if(CORRELATION_AXIS_TREADY = '1') then
                        if(my_counter <= correlation_size) then
                            if not endfile(signal_correlation_file) then
                                read (signal_correlation_file, signal_correlation);
                                CORRELATION_AXIS_TDATA<=std_logic_vector(to_signed(signal_correlation, CORRELATION_AXIS_TDATA'length));
                            end if;
                            if not endfile(signal_correlation_file) then
                                read (signal_correlation_file, signal_correlation);
                            end if;
							CORRELATION_AXIS_TVALID <= '1';
							if(my_counter >= correlation_size-1) then
								CORRELATION_AXIS_TLAST <= '1';
								my_counter  := 0;												
								state_sm    <= read_result;	
							end if;
                            my_counter := my_counter + 1;           
                        end if;  
                    end if;
                when read_result => 
					CORRELATION_AXIS_TVALID <= '0';
                    CORRELATION_AXIS_TLAST <= '0';															   
                    if(GATES_EN = '1') then
						if(GATES_END = '1') then
							READ_GATES_RESULT <= '1';	
							state_sm    <= read_result_end;															   		
						end if;
					else
						state_sm    <= close_files;																   
                    end if; 
				when read_result_end => 
					if(GATES_END = '0') then
						READ_GATES_RESULT <= '0';
						state_sm   <= update_gates_en_1;																   
					end if;		
			    when update_gates_en_1 => 
			    	gates_en_i <= std_logic_vector(unsigned(gates_en_i)+1);	
			    	state_sm   <= update_gates_en_2;	
			    when update_gates_en_2 =>
			        GATE1_EN <= gates_en_i(0);
			        GATE2_EN <= gates_en_i(1);
			        GATE3_EN <= gates_en_i(2);	
			        state_sm   <= close_files;											   
                when close_files =>
                    if(my_counter >= 128) then
                        file_close(signal_correlation_file);  
                        file_close(signal_data_file);
                        my_counter := 0;
                        state_sm <= open_files;   
                    else
                        my_counter := my_counter + 1;
                    end if;
                when others =>
                    null;                   
            end case;                                                                                            
        end if;
   end process; 
   
    ref_signal_input_inst: process(aclk, aresetn)  
       variable my_counter  : integer range 0 to 10000000 :=0;   
       
       type signalDataFile is file of integer;
       file signal_data_file: signalDataFile;
       variable signal_data: integer;     
       variable signal_data_file_status: file_open_status;
   begin 
       if (aresetn = '0') then
           GATE2_CC_CONFIG_ADDR <= std_logic_vector(to_unsigned(0, GATE2_CC_CONFIG_ADDR'length));    
           GATE2_CC_CONFIG_DATA <= std_logic_vector(to_unsigned(0, GATE2_CC_CONFIG_DATA'length));    
           GATE2_CC_CONFIG_ON <= '0';                
           GATE2_CC_CONFIG_ADDR_first <= '1';  
           FFT_WIDTH <= std_logic_vector(to_unsigned(0, FFT_WIDTH'length));                                                                                                                                                       
           -- state                                                                              
           state_cc_signal_sm <= idle;  
       elsif (aclk'event and aclk = '1') then
           case state_cc_signal_sm is  
               when idle =>        
                   GATE2_CC_CONFIG_ON <= '0';  
                   GATE2_CC_CONFIG_ADDR <= std_logic_vector(to_unsigned(0, GATE2_CC_CONFIG_ADDR'length)); 
                   GATE2_CC_CONFIG_ADDR_first <= '1';  
                   FFT_WIDTH <= std_logic_vector(to_unsigned(1024, FFT_WIDTH'length));
                   if(start_cc_sm = '1') then
                       my_counter := 0;
                       load_signal_counter <= (others => '0');    
                       state_cc_signal_sm <= open_file;        
                   end if; 
               when open_file => 
                   file_open(signal_data_file_status, signal_data_file, "wave2.dat", read_mode);
                   my_counter := 0; 
                   state_cc_signal_sm <= load_signal;      
               when load_signal =>
                   GATE2_CC_CONFIG_ON <= '1'; 
                   if(GATE2_CC_CONFIG_ON_ACK = '1') then
                       -- tdata
                       if not endfile(signal_data_file) then
                           read (signal_data_file, signal_data);
                           GATE2_CC_CONFIG_DATA<=std_logic_vector(to_signed(signal_data, GATE2_CC_CONFIG_DATA'length));
                       end if;  
                       if not endfile(signal_data_file) then
                           read (signal_data_file, signal_data);
                       end if;    
                       -- tlast                                              
                       if(my_counter >= unsigned(FFT_WIDTH)-1) then
                           my_counter := 0;                                           
                           state_cc_signal_sm <= load_signal_wait;                                              
                       else
                           -- counter                                          
                           my_counter := my_counter + 1;                                          
                       end if; 
                       if(GATE2_CC_CONFIG_ADDR_first = '1') then
                           GATE2_CC_CONFIG_ADDR_first <= '0'; 
                           GATE2_CC_CONFIG_ADDR <= std_logic_vector(to_unsigned(0, GATE2_CC_CONFIG_ADDR'length));   
                       else
                           GATE2_CC_CONFIG_ADDR <= std_logic_vector(unsigned(GATE2_CC_CONFIG_ADDR)+1); 
                       end if;
                   end if;                                                                        
               when load_signal_wait =>                                                  
                   if(my_counter >= 16) then
                       my_counter := 0; 
                       state_cc_signal_sm <= close_file;                                                    
                   else
                       my_counter := my_counter + 1;                                          
                   end if;                                                                                                                                
               when close_file =>
                   GATE2_CC_CONFIG_ON <= '0'; 
                   if(my_counter >= 1000000) then                                               
                       file_close(signal_data_file);
                       --state_cc_signal_sm <= idle;                                                   
                   else
                       my_counter := my_counter + 1;
                   end if;     
               when others =>
                   null;                   
           end case;                                                                                            
       end if;
   end process;

end;