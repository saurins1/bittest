library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity gates_tb is
	generic (
		-- Users to add parameters here
    GATE_RANGE                      : integer    := 32;
    GATE_ALARM_CROSSING             : integer    := 2;
    GATE_TOF_ALGORITHM_RANGE        : integer    := 4;
    GATE_TOF_AVG_RANGE              : integer    := 8;
    GATE_TOF_RANGE                  : integer    := 32;
    GATE_AXIS_GATE_RANGE_48         : integer  := 48;
    GATE_AXIS_GATE_RANGE_16         : integer   := 16;
    -- User parameters ends
    -- Do not modify the parameters beyond this line

    -- Parameters of Axi Slave Bus Interface GATE_AXIS
    C_GATE_AXIS_TDATA_WIDTH    : integer    := 16
);
end; 

architecture bench of gates_tb is

  component gates
  	port (
  	    BEGIN_GATE                        : in std_logic;  
  	    END_GATE                          : out std_logic;  
  	    
  		GATE1_EN                          : in std_logic;  
  		GATE1_START                       : in std_logic_vector(GATE_RANGE-1 downto 0);
  		GATE1_WIDTH                       : in std_logic_vector(GATE_RANGE-1 downto 0);	
          GATE1_AMP_THRESHOLD               : in std_logic_vector(GATE_RANGE-1 downto 0);
          GATE1_AMP_THRESHOLD_ALARM_EN      : in std_logic; 
          GATE1_AMP_THRESHOLD_ALARM_CROSSING: in std_logic_vector(GATE_ALARM_CROSSING-1 downto 0);
          GATE1_TOF_ALGORITHM               : in std_logic_vector(GATE_TOF_ALGORITHM_RANGE-1 downto 0);
          GATE1_TOF_AVG                     : in std_logic_vector(GATE_TOF_AVG_RANGE-1 downto 0);
          GATE1_TOF_MIN_THICKNESS           : in std_logic_vector(GATE_TOF_RANGE-1 downto 0);
          GATE1_TOF_ALARM_EN                : in std_logic; 
          GATE1_TOF_ALARM_TYPE              : in std_logic;
          GATE1_TOF_ALARM_CROSSING          : in std_logic_vector(GATE_ALARM_CROSSING-1 downto 0);
          GATE1_TOF_ALARM_MIN_VALUE         : in std_logic_vector(GATE_TOF_RANGE-1 downto 0);
          GATE1_TOF_ALARM_MAX_VALUE         : in std_logic_vector(GATE_TOF_RANGE-1 downto 0);
          GATE1_TRACK_GATE_3                : in std_logic; 
          GATE1_NORMALIZE_TO_AMP_GATE_1     : in std_logic; 
          GATE1_TOF                         : out std_logic_vector(GATE_TOF_RANGE-1 downto 0);
          GATE1_AMP_ALARM                   : out std_logic; 
          GATE1_TOF_ALARM                   : out std_logic; 
          GATE1_MAX                         : out std_logic_vector(GATE_AXIS_GATE_RANGE_16-1 downto 0);
          GATE1_MAX_SAMPLE                  : out std_logic_vector(GATE_RANGE-1 downto 0);
          GATE1_MIN                         : out std_logic_vector(GATE_AXIS_GATE_RANGE_16-1 downto 0);
          GATE1_MIN_SAMPLE                  : out std_logic_vector(GATE_RANGE-1 downto 0);
          
          GATE2_EN                          : in std_logic;  
          GATE2_START                       : in std_logic_vector(GATE_RANGE-1 downto 0);
          GATE2_WIDTH                       : in std_logic_vector(GATE_RANGE-1 downto 0);    
          GATE2_AMP_THRESHOLD               : in std_logic_vector(GATE_RANGE-1 downto 0);
          GATE2_AMP_THRESHOLD_ALARM_EN      : in std_logic; 
          GATE2_AMP_THRESHOLD_ALARM_CROSSING: in std_logic_vector(GATE_ALARM_CROSSING-1 downto 0);
          GATE2_TOF_ALGORITHM               : in std_logic_vector(GATE_TOF_ALGORITHM_RANGE-1 downto 0);
          GATE2_TOF_AVG                     : in std_logic_vector(GATE_TOF_AVG_RANGE-1 downto 0);
          GATE2_TOF_MIN_THICKNESS           : in std_logic_vector(GATE_TOF_RANGE-1 downto 0);
          GATE2_TOF_ALARM_EN                : in std_logic; 
          GATE2_TOF_ALARM_TYPE              : in std_logic;
          GATE2_TOF_ALARM_CROSSING          : in std_logic_vector(GATE_ALARM_CROSSING-1 downto 0);
          GATE2_TOF_ALARM_MIN_VALUE         : in std_logic_vector(GATE_TOF_RANGE-1 downto 0);
          GATE2_TOF_ALARM_MAX_VALUE         : in std_logic_vector(GATE_TOF_RANGE-1 downto 0);
          GATE2_TRACK_GATE_3                : in std_logic; 
          GATE2_NORMALIZE_TO_AMP_GATE_1     : in std_logic; 
          GATE2_TOF                         : out std_logic_vector(GATE_TOF_RANGE-1 downto 0);
          GATE2_AMP_ALARM                   : out std_logic; 
          GATE2_TOF_ALARM                   : out std_logic; 
          GATE2_MAX                         : out std_logic_vector(GATE_AXIS_GATE_RANGE_16-1 downto 0);
          GATE2_MAX_SAMPLE                  : out std_logic_vector(GATE_RANGE-1 downto 0);
          GATE2_MIN                         : out std_logic_vector(GATE_AXIS_GATE_RANGE_16-1 downto 0);
          GATE2_MIN_SAMPLE                  : out std_logic_vector(GATE_RANGE-1 downto 0);
          
          GATE3_EN                          : in std_logic;  
          GATE3_START                       : in std_logic_vector(GATE_RANGE-1 downto 0);
          GATE3_WIDTH                       : in std_logic_vector(GATE_RANGE-1 downto 0);    
          GATE3_AMP_THRESHOLD               : in std_logic_vector(GATE_RANGE-1 downto 0);
          GATE3_AMP_THRESHOLD_ALARM_EN      : in std_logic; 
          GATE3_AMP_THRESHOLD_ALARM_CROSSING: in std_logic_vector(GATE_ALARM_CROSSING-1 downto 0);
          GATE3_TOF_ALGORITHM               : in std_logic_vector(GATE_TOF_ALGORITHM_RANGE-1 downto 0);
          GATE3_TOF_AVG                     : in std_logic_vector(GATE_TOF_AVG_RANGE-1 downto 0);
          GATE3_TOF_MIN_THICKNESS           : in std_logic_vector(GATE_TOF_RANGE-1 downto 0);
          GATE3_TOF_ALARM_EN                : in std_logic; 
          GATE3_TOF_ALARM_TYPE              : in std_logic;
          GATE3_TOF_ALARM_CROSSING          : in std_logic_vector(GATE_ALARM_CROSSING-1 downto 0);
          GATE3_TOF_ALARM_MIN_VALUE         : in std_logic_vector(GATE_TOF_RANGE-1 downto 0);
          GATE3_TOF_ALARM_MAX_VALUE         : in std_logic_vector(GATE_TOF_RANGE-1 downto 0);
          GATE3_TRACK_GATE_3                : in std_logic; 
          GATE3_NORMALIZE_TO_AMP_GATE_1     : in std_logic; 
          GATE3_TOF                         : out std_logic_vector(GATE_TOF_RANGE-1 downto 0);
          GATE3_AMP_ALARM                   : out std_logic; 
          GATE3_TOF_ALARM                   : out std_logic; 
          GATE3_MAX                         : out std_logic_vector(GATE_AXIS_GATE_RANGE_16-1 downto 0);
          GATE3_MAX_SAMPLE                  : out std_logic_vector(GATE_RANGE-1 downto 0);
          GATE3_MIN                         : out std_logic_vector(GATE_AXIS_GATE_RANGE_16-1 downto 0);
          GATE3_MIN_SAMPLE                  : out std_logic_vector(GATE_RANGE-1 downto 0);
          
		  -- Ports of Axi Slave Bus Interface GATE1_AXIS
          gate_axis_aclk      : in std_logic;
          gate_axis_aresetn : in std_logic;
          gate_axis_tready  : out std_logic;
          gate_axis_tdata      : in std_logic_vector(C_GATE_AXIS_TDATA_WIDTH-1 downto 0);
          gate_axis_tstrb      : in std_logic_vector((C_GATE_AXIS_TDATA_WIDTH/8)-1 downto 0);
          gate_axis_tlast      : in std_logic;
          gate_axis_tvalid  : in std_logic
  	);
  end component;

  signal BEGIN_GATE: std_logic := '0';
  signal END_GATE: std_logic;
  
  signal GATE1_EN: std_logic := '0';
  signal GATE1_START: std_logic_vector(GATE_RANGE-1 downto 0) := (others => '0');
  signal GATE1_WIDTH: std_logic_vector(GATE_RANGE-1 downto 0) := (others => '0');
  signal GATE1_AMP_THRESHOLD: std_logic_vector(GATE_RANGE-1 downto 0) := (others => '0');
  signal GATE1_AMP_THRESHOLD_ALARM_EN: std_logic := '0';
  signal GATE1_AMP_THRESHOLD_ALARM_CROSSING: std_logic_vector(GATE_ALARM_CROSSING-1 downto 0) := (others => '0');
  signal GATE1_TOF_ALGORITHM: std_logic_vector(GATE_TOF_ALGORITHM_RANGE-1 downto 0) := (others => '0');
  signal GATE1_TOF_AVG: std_logic_vector(GATE_TOF_AVG_RANGE-1 downto 0);
  signal GATE1_TOF_MIN_THICKNESS: std_logic_vector(GATE_TOF_RANGE-1 downto 0) := (others => '0');
  signal GATE1_TOF_ALARM_EN: std_logic := '0';
  signal GATE1_TOF_ALARM_TYPE: std_logic := '0';
  signal GATE1_TOF_ALARM_CROSSING : std_logic_vector(GATE_ALARM_CROSSING-1 downto 0) := (others => '0');
  signal GATE1_TOF_ALARM_MIN_VALUE: std_logic_vector(GATE_TOF_RANGE-1 downto 0) := (others => '0');
  signal GATE1_TOF_ALARM_MAX_VALUE: std_logic_vector(GATE_TOF_RANGE-1 downto 0) := (others => '0');
  signal GATE1_TRACK_GATE_3: std_logic := '0';
  signal GATE1_NORMALIZE_TO_AMP_GATE_1: std_logic := '0';
  signal GATE1_TOF: std_logic_vector(GATE_TOF_RANGE-1 downto 0);
  signal GATE1_AMP_ALARM: std_logic;
  signal GATE1_TOF_ALARM: std_logic;
  signal GATE1_MAX                         : std_logic_vector(GATE_AXIS_GATE_RANGE_16-1 downto 0);
  signal GATE1_MAX_SAMPLE                  : std_logic_vector(GATE_RANGE-1 downto 0);
  signal GATE1_MIN                         : std_logic_vector(GATE_AXIS_GATE_RANGE_16-1 downto 0);
  signal GATE1_MIN_SAMPLE                  : std_logic_vector(GATE_RANGE-1 downto 0);
  
  signal GATE2_EN: std_logic := '0';
  signal GATE2_START: std_logic_vector(GATE_RANGE-1 downto 0) := (others => '0');
  signal GATE2_WIDTH: std_logic_vector(GATE_RANGE-1 downto 0) := (others => '0');
  signal GATE2_AMP_THRESHOLD: std_logic_vector(GATE_RANGE-1 downto 0) := (others => '0');
  signal GATE2_AMP_THRESHOLD_ALARM_EN: std_logic := '0';
  signal GATE2_AMP_THRESHOLD_ALARM_CROSSING: std_logic_vector(GATE_ALARM_CROSSING-1 downto 0) := (others => '0');
  signal GATE2_TOF_ALGORITHM: std_logic_vector(GATE_TOF_ALGORITHM_RANGE-1 downto 0) := (others => '0');
  signal GATE2_TOF_AVG: std_logic_vector(GATE_TOF_AVG_RANGE-1 downto 0) := (others => '0');
  signal GATE2_TOF_MIN_THICKNESS: std_logic_vector(GATE_TOF_RANGE-1 downto 0) := (others => '0');
  signal GATE2_TOF_ALARM_EN: std_logic := '0';
  signal GATE2_TOF_ALARM_TYPE: std_logic := '0';
  signal GATE2_TOF_ALARM_CROSSING : std_logic_vector(GATE_ALARM_CROSSING-1 downto 0) := (others => '0');
  signal GATE2_TOF_ALARM_MIN_VALUE: std_logic_vector(GATE_TOF_RANGE-1 downto 0) := (others => '0');
  signal GATE2_TOF_ALARM_MAX_VALUE: std_logic_vector(GATE_TOF_RANGE-1 downto 0) := (others => '0');
  signal GATE2_TRACK_GATE_3: std_logic := '0';
  signal GATE2_NORMALIZE_TO_AMP_GATE_1: std_logic := '0';
  signal GATE2_TOF: std_logic_vector(GATE_TOF_RANGE-1 downto 0);
  signal GATE2_AMP_ALARM: std_logic;
  signal GATE2_TOF_ALARM: std_logic;
  signal GATE2_MAX                         : std_logic_vector(GATE_AXIS_GATE_RANGE_16-1 downto 0);
  signal GATE2_MAX_SAMPLE                  : std_logic_vector(GATE_RANGE-1 downto 0);
  signal GATE2_MIN                         : std_logic_vector(GATE_AXIS_GATE_RANGE_16-1 downto 0);
  signal GATE2_MIN_SAMPLE                  : std_logic_vector(GATE_RANGE-1 downto 0);
  
  signal GATE3_EN: std_logic := '0';
  signal GATE3_START: std_logic_vector(GATE_RANGE-1 downto 0) := (others => '0');
  signal GATE3_WIDTH: std_logic_vector(GATE_RANGE-1 downto 0) := (others => '0');
  signal GATE3_AMP_THRESHOLD: std_logic_vector(GATE_RANGE-1 downto 0) := (others => '0');
  signal GATE3_AMP_THRESHOLD_ALARM_EN: std_logic := '0';
  signal GATE3_AMP_THRESHOLD_ALARM_CROSSING: std_logic_vector(GATE_ALARM_CROSSING-1 downto 0) := (others => '0');
  signal GATE3_TOF_ALGORITHM: std_logic_vector(GATE_TOF_ALGORITHM_RANGE-1 downto 0) := (others => '0');
  signal GATE3_TOF_AVG: std_logic_vector(GATE_TOF_AVG_RANGE-1 downto 0) := (others => '0');
  signal GATE3_TOF_MIN_THICKNESS: std_logic_vector(GATE_TOF_RANGE-1 downto 0) := (others => '0');
  signal GATE3_TOF_ALARM_EN: std_logic := '0';
  signal GATE3_TOF_ALARM_TYPE: std_logic := '0';
  signal GATE3_TOF_ALARM_CROSSING : std_logic_vector(GATE_ALARM_CROSSING-1 downto 0) := (others => '0');
  signal GATE3_TOF_ALARM_MIN_VALUE: std_logic_vector(GATE_TOF_RANGE-1 downto 0) := (others => '0');
  signal GATE3_TOF_ALARM_MAX_VALUE: std_logic_vector(GATE_TOF_RANGE-1 downto 0) := (others => '0');
  signal GATE3_TRACK_GATE_3: std_logic := '0';
  signal GATE3_NORMALIZE_TO_AMP_GATE_1: std_logic := '0';
  signal GATE3_TOF: std_logic_vector(GATE_TOF_RANGE-1 downto 0);
  signal GATE3_AMP_ALARM: std_logic;
  signal GATE3_TOF_ALARM: std_logic;
  signal GATE3_MAX                         : std_logic_vector(GATE_AXIS_GATE_RANGE_16-1 downto 0);
  signal GATE3_MAX_SAMPLE                  : std_logic_vector(GATE_RANGE-1 downto 0);
  signal GATE3_MIN                         : std_logic_vector(GATE_AXIS_GATE_RANGE_16-1 downto 0);
  signal GATE3_MIN_SAMPLE                  : std_logic_vector(GATE_RANGE-1 downto 0);

  signal gate_axis_tready: std_logic;
  signal gate_axis_tdata: std_logic_vector(C_GATE_AXIS_TDATA_WIDTH-1 downto 0) := (others => '0');
  signal gate_axis_tstrb: std_logic_vector((C_GATE_AXIS_TDATA_WIDTH/8)-1 downto 0) := (others => '0');
  signal gate_axis_tlast: std_logic := '0';
  signal gate_axis_tvalid: std_logic := '0';

  constant CLOCK_period : time := 10 ns;
  
	  -- STATE MACHINE STATES
    type states_load is (iddle, load_input, load_end); 
    signal state_load : states_load; 
    signal load_counter : unsigned(GATE_RANGE-1 downto 0) := (others => '0'); 

    signal aclk     : std_logic := '0';
    signal aresetn  : std_logic := '0';
    
    signal begin_load : std_logic := '0';
    
    signal GATE_WIDTH: std_logic_vector(GATE_RANGE-1 downto 0);

begin

  -- Insert values for generic parameters !!
  uut: gates
                          port map ( BEGIN_GATE                         => BEGIN_GATE,
                                     END_GATE                           => END_GATE,
                                     GATE1_EN                           => GATE1_EN,
                                     GATE1_START                        => GATE1_START,
                                     GATE1_WIDTH                        => GATE1_WIDTH,
                                     GATE1_AMP_THRESHOLD                => GATE1_AMP_THRESHOLD,
                                     GATE1_AMP_THRESHOLD_ALARM_EN       => GATE1_AMP_THRESHOLD_ALARM_EN,
                                     GATE1_AMP_THRESHOLD_ALARM_CROSSING => GATE1_AMP_THRESHOLD_ALARM_CROSSING,
                                     GATE1_TOF_ALGORITHM                => GATE1_TOF_ALGORITHM,
                                     GATE1_TOF_AVG                      => GATE1_TOF_AVG,
                                     GATE1_TOF_MIN_THICKNESS            => GATE1_TOF_MIN_THICKNESS,
                                     GATE1_TOF_ALARM_EN                 => GATE1_TOF_ALARM_EN,
                                     GATE1_TOF_ALARM_TYPE               => GATE1_TOF_ALARM_TYPE,
                                     GATE1_TOF_ALARM_CROSSING           => GATE1_TOF_ALARM_CROSSING,
                                     GATE1_TOF_ALARM_MIN_VALUE          => GATE1_TOF_ALARM_MIN_VALUE,
                                     GATE1_TOF_ALARM_MAX_VALUE          => GATE1_TOF_ALARM_MAX_VALUE,
                                     GATE1_TRACK_GATE_3                 => GATE1_TRACK_GATE_3,
                                     GATE1_NORMALIZE_TO_AMP_GATE_1      => GATE1_NORMALIZE_TO_AMP_GATE_1,
                                     GATE1_TOF                          => GATE1_TOF,
                                     GATE1_AMP_ALARM                    => GATE1_AMP_ALARM,
                                     GATE1_TOF_ALARM                    => GATE1_TOF_ALARM,
                                     GATE1_MAX                          => GATE1_MAX,
                                     GATE1_MAX_SAMPLE                   => GATE1_MAX_SAMPLE,
                                     GATE1_MIN                          => GATE1_MIN,
                                     GATE1_MIN_SAMPLE                   => GATE1_MIN_SAMPLE,
                                     GATE2_EN                           => GATE2_EN,
                                     GATE2_START                        => GATE2_START,
                                     GATE2_WIDTH                        => GATE2_WIDTH,
                                     GATE2_AMP_THRESHOLD                => GATE2_AMP_THRESHOLD,
                                     GATE2_AMP_THRESHOLD_ALARM_EN       => GATE2_AMP_THRESHOLD_ALARM_EN,
                                     GATE2_AMP_THRESHOLD_ALARM_CROSSING => GATE2_AMP_THRESHOLD_ALARM_CROSSING,
                                     GATE2_TOF_ALGORITHM                => GATE2_TOF_ALGORITHM,
                                     GATE2_TOF_AVG                      => GATE2_TOF_AVG,
                                     GATE2_TOF_MIN_THICKNESS            => GATE2_TOF_MIN_THICKNESS,
                                     GATE2_TOF_ALARM_EN                 => GATE2_TOF_ALARM_EN,
                                     GATE2_TOF_ALARM_TYPE               => GATE2_TOF_ALARM_TYPE,
                                     GATE2_TOF_ALARM_CROSSING           => GATE2_TOF_ALARM_CROSSING,
                                     GATE2_TOF_ALARM_MIN_VALUE          => GATE2_TOF_ALARM_MIN_VALUE,
                                     GATE2_TOF_ALARM_MAX_VALUE          => GATE2_TOF_ALARM_MAX_VALUE,
                                     GATE2_TRACK_GATE_3                 => GATE2_TRACK_GATE_3,
                                     GATE2_NORMALIZE_TO_AMP_GATE_1      => GATE2_NORMALIZE_TO_AMP_GATE_1,
                                     GATE2_TOF                          => GATE2_TOF,
                                     GATE2_AMP_ALARM                    => GATE2_AMP_ALARM,
                                     GATE2_TOF_ALARM                    => GATE2_TOF_ALARM,
                                     GATE2_MAX                          => GATE2_MAX,
                                     GATE2_MAX_SAMPLE                   => GATE2_MAX_SAMPLE,
                                     GATE2_MIN                          => GATE2_MIN,
                                     GATE2_MIN_SAMPLE                   => GATE2_MIN_SAMPLE,
                                     GATE3_EN                           => GATE3_EN,
                                     GATE3_START                        => GATE3_START,
                                     GATE3_WIDTH                        => GATE3_WIDTH,
                                     GATE3_AMP_THRESHOLD                => GATE3_AMP_THRESHOLD,
                                     GATE3_AMP_THRESHOLD_ALARM_EN       => GATE3_AMP_THRESHOLD_ALARM_EN,
                                     GATE3_AMP_THRESHOLD_ALARM_CROSSING => GATE3_AMP_THRESHOLD_ALARM_CROSSING,
                                     GATE3_TOF_ALGORITHM                => GATE3_TOF_ALGORITHM,
                                     GATE3_TOF_AVG                      => GATE3_TOF_AVG,
                                     GATE3_TOF_MIN_THICKNESS            => GATE3_TOF_MIN_THICKNESS,
                                     GATE3_TOF_ALARM_EN                 => GATE3_TOF_ALARM_EN,
                                     GATE3_TOF_ALARM_TYPE               => GATE3_TOF_ALARM_TYPE,
                                     GATE3_TOF_ALARM_CROSSING           => GATE3_TOF_ALARM_CROSSING,
                                     GATE3_TOF_ALARM_MIN_VALUE          => GATE3_TOF_ALARM_MIN_VALUE,
                                     GATE3_TOF_ALARM_MAX_VALUE          => GATE3_TOF_ALARM_MAX_VALUE,
                                     GATE3_TRACK_GATE_3                 => GATE3_TRACK_GATE_3,
                                     GATE3_NORMALIZE_TO_AMP_GATE_1      => GATE3_NORMALIZE_TO_AMP_GATE_1,
                                     GATE3_TOF                          => GATE3_TOF,
                                     GATE3_AMP_ALARM                    => GATE3_AMP_ALARM,
                                     GATE3_TOF_ALARM                    => GATE3_TOF_ALARM,
                                     GATE3_MAX                          => GATE3_MAX,
                                     GATE3_MAX_SAMPLE                   => GATE3_MAX_SAMPLE,
                                     GATE3_MIN                          => GATE3_MIN,
                                     GATE3_MIN_SAMPLE                   => GATE3_MIN_SAMPLE,
                                     gate_axis_aclk                    => aclk,
                                     gate_axis_aresetn                 => aresetn,
                                     gate_axis_tready                  => gate_axis_tready,
                                     gate_axis_tdata                   => gate_axis_tdata,
                                     gate_axis_tstrb                   => gate_axis_tstrb,
                                     gate_axis_tlast                   => gate_axis_tlast,
                                     gate_axis_tvalid                  => gate_axis_tvalid );

  stimulus: process
  begin
  
    aresetn <= '0';
    begin_load <= '0';
  
    -- Put initialisation code here
    GATE_WIDTH 		                 <= "00000000000000000010011001001000"; --9800
    
    GATE1_EN                          <= '1';
  	GATE1_START                       <= "00000000000000000000000100101100"; --300
  	GATE1_WIDTH                       <= "00000000000000000000001001011000"; --600
    GATE1_AMP_THRESHOLD               <= "00000000000000000000001001011000"; --600
    GATE1_AMP_THRESHOLD_ALARM_EN      <= '1';
    GATE1_AMP_THRESHOLD_ALARM_CROSSING<= "01";
    GATE1_TOF_ALGORITHM               <= "0001";
    GATE1_TOF_AVG                     <= "00000000";
    GATE1_TOF_MIN_THICKNESS           <= "00000000000000000001001110001000"; --5000
    GATE1_TOF_ALARM_EN                <= '0';
    GATE1_TOF_ALARM_TYPE              <= '1';
    GATE1_TOF_ALARM_CROSSING          <= "01";
    GATE1_TOF_ALARM_MIN_VALUE         <= "00000000000001100100000000000000"; --400*1024
    GATE1_TOF_ALARM_MAX_VALUE         <= "00000000000001111101000000000000"; --500*1024
    GATE1_TRACK_GATE_3                <= '1';
    GATE1_NORMALIZE_TO_AMP_GATE_1     <= '1'; 
    
    GATE2_EN                          <= '1';
    GATE2_START                       <= "00000000000000000000000100101100"; --300
    GATE2_WIDTH                       <= "00000000000000000000001001011000"; --600
    GATE2_AMP_THRESHOLD               <= "00000000000000000000001001011000"; --600
    GATE2_AMP_THRESHOLD_ALARM_EN      <= '0';
    GATE2_AMP_THRESHOLD_ALARM_CROSSING<= "10";
    GATE2_TOF_ALGORITHM               <= "0010";
    GATE2_TOF_AVG                     <= "00000000";
    GATE2_TOF_MIN_THICKNESS           <= "00000000000000000001001110001000"; --5000
    GATE2_TOF_ALARM_EN                <= '1';
    GATE2_TOF_ALARM_TYPE              <= '0';
    GATE2_TOF_ALARM_CROSSING          <= "01";
    GATE2_TOF_ALARM_MIN_VALUE         <= "00000000000001100100000000000000"; --400*1024
    GATE2_TOF_ALARM_MAX_VALUE         <= "00000000000001111101000000000000"; --500*1024
    GATE2_TRACK_GATE_3                <= '1';
    GATE2_NORMALIZE_TO_AMP_GATE_1     <= '1'; 
    
    GATE3_EN                          <= '1';
    GATE3_START                       <= "00000000000000000000000100101100"; --300
    GATE3_WIDTH                       <= "00000000000000000000001001011000"; --600
    GATE3_AMP_THRESHOLD               <= "00000000000000000000001001011000"; --600
    GATE3_AMP_THRESHOLD_ALARM_EN      <= '0';
    GATE3_AMP_THRESHOLD_ALARM_CROSSING<= "01";
    GATE3_TOF_ALGORITHM               <= "0011";
    GATE3_TOF_AVG                     <= "00000000";
    GATE3_TOF_MIN_THICKNESS           <= "00000000000000000001001110001000"; --5000
    GATE3_TOF_ALARM_EN                <= '0';
    GATE3_TOF_ALARM_TYPE              <= '0';
    GATE3_TOF_ALARM_CROSSING          <= "10";
    GATE3_TOF_ALARM_MIN_VALUE         <= "00000000000001100100000000000000"; --400*1024
    GATE3_TOF_ALARM_MAX_VALUE         <= "00000000000001111101000000000000"; --500*1024
    GATE3_TRACK_GATE_3                <= '1';
    GATE3_NORMALIZE_TO_AMP_GATE_1     <= '1'; 
    
    wait for CLOCK_period*10;	
    
    aresetn <= '1';
    wait for CLOCK_period*10;	
    
    begin_load <= '1';
    wait for CLOCK_period*10;
    begin_load <= '0';
    wait for CLOCK_period*10;
    
    wait;
  end process;

   -- Clock process 
   CLOCK_process :process
   begin
		aclk <= '0';
		wait for CLOCK_period/2;
		aclk <= '1';
		wait for CLOCK_period/2;
   end process;
   
  --PERFORM LOAD GATE DATA
  load_inst: process(aclk, aresetn)                    
      type dataFile is file of integer;
      file data_file: dataFile is in "ascan.dat";
      variable data_in_file: integer;                        
  begin 
      if (aresetn = '0') then
          gate_axis_tlast  <= '0';
          gate_axis_tvalid <= '0';
          gate_axis_tdata  <= (others => '0'); 
          BEGIN_GATE <= '0';          
          state_load <= iddle;  
      elsif (aclk'event and aclk = '1') then
          case state_load is  
              when iddle =>                        
                    if(begin_load = '1') then
                        BEGIN_GATE <= '1';
                        state_load <= load_input;
                        load_counter <= (others => '0'); 
                    end if;
              when load_input =>     
                    if( gate_axis_tready = '1') then
                        -- data from file
                        if not endfile(data_file) then
                            read (data_file, data_in_file);
                            gate_axis_tdata<=std_logic_vector(to_signed(data_in_file, gate_axis_tdata'length));
                        end if;
                        if not endfile(data_file) then
                            read (data_file, data_in_file);
                        end if; 
                        load_counter <= load_counter + 1;
                        gate_axis_tvalid <= '1';
                        if(load_counter = 1999) then
                            gate_axis_tlast <= '1';
                        else
                            gate_axis_tlast <= '0';
                        end if;
                        if(load_counter >= 2000) then
                            gate_axis_tlast <= '0';
                            gate_axis_tvalid <= '0';
                            state_load <= load_end; 
                        end if;  
                    end if;                              
                when load_end =>            
                    if(END_GATE = '1' and begin_load = '0') then
                        BEGIN_GATE <= '0';
                        state_load <= iddle; 
                    end if;
                when others =>
                    null;                   
            end case;                                                                                            
        end if;
    end process; 
    
end;
