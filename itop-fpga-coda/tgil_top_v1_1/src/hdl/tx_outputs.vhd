----------------------------------------------------------------------------------
-- Company: Innerspec Technologies
-- Engineers: Swapna Muralidhar
--				  Zubin Kumar	
--				Wei Jiang
-- 
-- Design Name: 
-- Module Name: TX_OUTPUTS - Behavioral 
-- Project Name:
-- Target Devices: Virtex 5 xc5vsx50t-1ff1136
-- Tool versions: 
-- Description: Swapna Muralidhar - Generate TTL outputs for TX signal from 200 MHz clock
--					Zubin Kumar - Reduced dead time between positive and negative cycles
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Revision 0.02 - Rewrite to get rid of divider, wei jiang
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_SIGNED.ALL;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity TX_OUTPUTS is
	port (CLOCK					: in std_logic;
			RESET					: in std_logic;
			TX_ENABLE			: in std_logic;
			START_CALC			: in std_logic;
			START_TX				: in std_logic;
			HALF_CYCLE_FREQ	: in std_logic_vector(29 downto 0);
			NO_HALF_CYCLES		: in std_logic_vector(7 downto 0);
			START_CYCLE			: in std_logic_vector(1 downto 0);
			-- Outputs
			TX_PLUS				: out std_logic;
			TX_MINUS				: out std_logic
			);
end TX_OUTPUTS;

architecture Behavioral of TX_OUTPUTS is

signal HALF_CYCLE_PEROID_PS : std_logic_vector(29 downto 0) := (others => '0');
signal FPGA_TXCLK_PERIOD_PS : std_logic_vector(29 downto 0) := (others => '0');
signal HALF_CYCLE_DEADTIME_PS: std_logic_vector(29 downto 0) := (others => '0');

signal START_CALCd1 : std_logic := '0';
signal START_CALCd2 : std_logic := '0';

type STATES is (IDLE_STATE, PLUS_STATE, MINUS_STATE, END_STATE, 
					DEAD_TIME_STATE1, DEAD_TIME_STATE2);
signal SM_STATE : STATES;

signal COUNT_DOWN_TIMER : std_logic_vector(29 downto 0) := (others => '0');
signal COUNT_CYCLES : std_logic_vector(7 downto 0) := (others => '0');

signal TX_PLUS1_MINUS0_i : std_logic := '0';

begin

	HALF_CYCLE_PEROID_PS <= HALF_CYCLE_FREQ;

	FPGA_TXCLK_PERIOD_PS <= "000000000000000010011100010000";	-- 10,000 (10 ns x 1000) 100 MHz clock
	HALF_CYCLE_DEADTIME_PS <= "000000000000000100111000100000";	-- 30,000 ( 20 ns total deadtime) 

	process (RESET, CLOCK)
	begin
		if (RESET = '1') then
			SM_STATE <= IDLE_STATE;
		elsif (rising_edge(CLOCK)) then
			case SM_STATE is
				when IDLE_STATE =>
				
					COUNT_CYCLES <= (others => '0');
					TX_PLUS <= '0';
					TX_MINUS <= '0';
					if (TX_ENABLE = '1' and START_TX = '1') then
						if (START_CYCLE = "01") then
							-- 1 = start from negative
							SM_STATE <= MINUS_STATE;
						else
							SM_STATE <= PLUS_STATE;
						end if;
						COUNT_DOWN_TIMER <= HALF_CYCLE_PEROID_PS;
					else
						SM_STATE <= IDLE_STATE;
					end if;
					
				when PLUS_STATE =>
				
					TX_PLUS1_MINUS0_i <= '1';
					TX_PLUS <= '1';
					TX_MINUS <= '0';
					COUNT_DOWN_TIMER <= COUNT_DOWN_TIMER - FPGA_TXCLK_PERIOD_PS;
					if ( COUNT_DOWN_TIMER > HALF_CYCLE_DEADTIME_PS ) then
						SM_STATE <= PLUS_STATE;
					else
						SM_STATE <= DEAD_TIME_STATE1;
					end if;
					
				when DEAD_TIME_STATE1 =>

					TX_PLUS <= '0';
					TX_MINUS <= '0';
					COUNT_DOWN_TIMER <= COUNT_DOWN_TIMER - FPGA_TXCLK_PERIOD_PS;
					if ( COUNT_DOWN_TIMER > '0' & FPGA_TXCLK_PERIOD_PS(29 downto 1) ) then
						SM_STATE <= DEAD_TIME_STATE1;
					else
						SM_STATE <= DEAD_TIME_STATE2;
						COUNT_CYCLES <= COUNT_CYCLES + 1;
					end if;
					
				when DEAD_TIME_STATE2 =>
				
					TX_PLUS <= '0';
					TX_MINUS <= '0';
					COUNT_DOWN_TIMER <= COUNT_DOWN_TIMER + HALF_CYCLE_PEROID_PS;
					if (COUNT_CYCLES < NO_HALF_CYCLES) then
						if (TX_PLUS1_MINUS0_i = '1') then
							SM_STATE <= MINUS_STATE;
						else
							SM_STATE <= PLUS_STATE;
						end if;
					else
						SM_STATE <= END_STATE;
					end if;
					
				when MINUS_STATE =>
				
					TX_PLUS1_MINUS0_i <= '0';
					TX_PLUS <= '0';
					TX_MINUS <= '1';
					COUNT_DOWN_TIMER <= COUNT_DOWN_TIMER - FPGA_TXCLK_PERIOD_PS;
					if ( COUNT_DOWN_TIMER > HALF_CYCLE_DEADTIME_PS ) then
						SM_STATE <= MINUS_STATE;
					else
						SM_STATE <= DEAD_TIME_STATE1;
					end if;
					
				when END_STATE =>
			
					TX_PLUS <= '0';
					TX_MINUS <= '0';
					if (START_TX = '1') then
						SM_STATE <= END_STATE;
					else
						SM_STATE <= IDLE_STATE;
					end if;
					
				when others =>
				
					TX_PLUS <= '0';
					TX_MINUS <= '0';				
					SM_STATE <= IDLE_STATE;
					
			end case;
		end if;
	end process;	
	

-----------------------------------------------------------------------------
end Behavioral;

