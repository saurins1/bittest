library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Channel_Receiver is
	port (
		-- axis sync
		aclk                    				: in STD_LOGIC; 
		aresetn                 				: in STD_LOGIC; 

		RX_DATA_VALID							: in std_logic;
		RX_DATA_WINDOW_ON						: in std_logic;
		RX_DATA_WINDOW							: in std_logic_vector(31 downto 0);

		-- Receiver data from digitizer units
		CH_ADC_DATA_IN							: in STD_LOGIC_VECTOR(15 downto 0);

		-- Receiver data stream output to other units
		CH_SIGNAL_AXIS_TREADY  					: in STD_LOGIC;
		CH_SIGNAL_AXIS_TDATA	  				: out STD_LOGIC_VECTOR(15 downto 0);
		CH_SIGNAL_AXIS_TLAST	  				: out STD_LOGIC;
		CH_SIGNAL_AXIS_TVALID  					: out STD_LOGIC
		);
end Channel_Receiver;

architecture Behavioral of Channel_Receiver is

signal Count_Data_Window : std_logic_vector(31 downto 0);
signal RX_DATA_WINDOW_M1 : std_logic_vector(31 downto 0);
	
begin
	
	RX_DATA_WINDOW_M1 <= std_logic_vector(unsigned(RX_DATA_WINDOW) - 1);
	
	process (aclk)
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				Count_Data_Window <= (others => '0');
				CH_SIGNAL_AXIS_TDATA <= (others => '0');		
				CH_SIGNAL_AXIS_TLAST <= '0';
				CH_SIGNAL_AXIS_TVALID <= '0';
        	else
				if(CH_SIGNAL_AXIS_TREADY = '1' and RX_DATA_WINDOW_ON = '1') then
					if(RX_DATA_VALID = '1') then
						CH_SIGNAL_AXIS_TVALID <= '1';
						if(Count_Data_Window = RX_DATA_WINDOW_M1 ) then
							CH_SIGNAL_AXIS_TLAST <= '1';	
						end if;
						CH_SIGNAL_AXIS_TDATA <= CH_ADC_DATA_IN;	
						Count_Data_Window <= std_logic_vector(unsigned(Count_Data_Window) + 1);
					else
						CH_SIGNAL_AXIS_TLAST <= '0';
						CH_SIGNAL_AXIS_TVALID <= '0';
					end if;
				else
					Count_Data_Window <= (others => '0');
					CH_SIGNAL_AXIS_TLAST <= '0';
					CH_SIGNAL_AXIS_TVALID <= '0';
				end if;
			end if;
		end if;
	end process;	

end Behavioral;
			