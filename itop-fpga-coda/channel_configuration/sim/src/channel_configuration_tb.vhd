library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity channel_configuration_tb is
  	generic (
    CHANNEL_ID      : INTEGER := 0;
    RECEIVER_SIZE   : INTEGER := 5;
    TRANSMITTER_SIZE: INTEGER := 6;
    MAGNET_SIZE     : INTEGER := 3;
    GATES_SIZE      : INTEGER := 42;
    DSP_SIZE        : INTEGER := 11;
    REGISTER_W32    : INTEGER := 32;
    REGISTER_W16    : INTEGER := 16;
    REGISTER_W8     : INTEGER := 8;
    REGISTER_W4     : INTEGER := 4
);
end;

architecture bench of channel_configuration_tb is

  component channel_configuration
  	generic (
  		CHANNEL_ID		: INTEGER := 0;
  		RECEIVER_SIZE	: INTEGER := 5;
  		TRANSMITTER_SIZE: INTEGER := 6;
  		MAGNET_SIZE		: INTEGER := 3;
  		GATES_SIZE		: INTEGER := 42;
  		DSP_SIZE		: INTEGER := 11;
        REGISTER_W32    : INTEGER := 32;
        REGISTER_W16    : INTEGER := 16;
  		REGISTER_W8     : INTEGER := 8;
  		REGISTER_W4     : INTEGER := 4
  	);
  	port (
  	    aclk         : in STD_LOGIC;
          aresetn      : in STD_LOGIC;
  		CONFIG_UPDATE_RECEIVER  	: in STD_LOGIC;
  		CONFIG_NEW_RECEIVER   		: out STD_LOGIC;
  		CONFIG_UPDATE_TRANSMITTER  	: in STD_LOGIC;
  		CONFIG_NEW_TRANSMITTER  	: out STD_LOGIC;
  		CONFIG_UPDATE_MAGNET 	 	: in STD_LOGIC;
  		CONFIG_NEW_MAGNET  			: out STD_LOGIC;
  		CONFIG_UPDATE_DSP_1  	: in STD_LOGIC;
  		CONFIG_NEW_DSP_1  		: out STD_LOGIC;
  		CONFIG_UPDATE_DSP_2  	: in STD_LOGIC;
  		CONFIG_NEW_DSP_2  		: out STD_LOGIC;
  		CONFIG_UPDATE_FIR  	 	: in STD_LOGIC;
  		CONFIG_NEW_FIR  		: out STD_LOGIC;
  		CONFIG_UPDATE_GATES  	: in STD_LOGIC;
  		CONFIG_NEW_GATES  		: out STD_LOGIC;
  		CONFIG_UPDATE_DAC  	 	: in STD_LOGIC;
  		CONFIG_NEW_DAC  		: out STD_LOGIC;
  		THERE_IS_CONFIGURATION   : out STD_LOGIC;
  		CHANNEL_EN   : out STD_LOGIC;
  		CONFIG_INPUT_LOAD	  	  : in STD_LOGIC;
  		CONFIG_INPUT_AXIS_TREADY  : out STD_LOGIC;
  		CONFIG_INPUT_AXIS_TDATA	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		CONFIG_INPUT_AXIS_TLAST	  : in STD_LOGIC;
  		CONFIG_INPUT_AXIS_TVALID  : in STD_LOGIC;
  		RECEIVER_SAMPLING_FREQUENCY         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        RECEIVER_DATA_WINDOW                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        RECEIVER_DELAY                      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        RECEIVER_GAIN                       : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        RECEIVER_EXTERNAL_MULTIPLEXER_EN    : out STD_LOGIC;
  		TRANSMITTER_VOLTAGE                 : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        TRANSMITTER_BURST_FREQUENCY         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        TRANSMITTER_N_CYCLES                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        TRANSMITTER_PRF                     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_DELAY                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        TRANSMITTER_DIRECTIONAL_PHASING     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		MAGNET_MODE                         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        MAGNET_PULSE_WIDTH                  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        MAGNET_INITIAL_DELAY                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		DSP_AVERAGE                         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        DSP_NOISE_REDUCTION_FILTER          : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        DSP_BANDPASS_EN  					: out STD_LOGIC;
  		DSP_BANDPASS_N_TAPS  				: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
  		DSP_BANDPASS_L  					: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        DSP_GAUSSIAN_EN  					: out STD_LOGIC;
  		DSP_GAUSSIAN_N_TAPS  				: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
  		DSP_GAUSSIAN_L  					: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
  		DSP_ENVELOPE_EN  					: out STD_LOGIC;
  		DSP_ENVELOPE_N_TAPS  				: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
  		DSP_ENVELOPE_L  					: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		DSP_ANALOG_FILTER  					: out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
  		GATE1_EN                          : out STD_LOGIC;  
  		GATE1_START                       : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		GATE1_WIDTH                       : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);	
        GATE1_AMP_THRESHOLD               : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		GATE1_TOF_ALGORITHM               : out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
  		GATE1_TOF_AVG                     : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
        GATE1_AMP_THRESHOLD_ALARM_EN      : out STD_LOGIC; 
        GATE1_AMP_THRESHOLD_ALARM_CROSSING: out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE1_TOF_ALARM_EN                : out STD_LOGIC; 
        GATE1_TOF_ALARM_TYPE              : out STD_LOGIC;
        GATE1_TOF_ALARM_CROSSING          : out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE1_TOF_ALARM_MIN_VALUE         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE1_TOF_ALARM_MAX_VALUE         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		GATE1_TOF_MIN_THICKNESS           : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		GATE2_EN                          : out STD_LOGIC;  
  		GATE2_START                       : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		GATE2_WIDTH                       : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);	
        GATE2_AMP_THRESHOLD               : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		GATE2_TOF_ALGORITHM               : out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
  		GATE2_TOF_AVG                     : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
        GATE2_AMP_THRESHOLD_ALARM_EN      : out STD_LOGIC; 
        GATE2_AMP_THRESHOLD_ALARM_CROSSING: out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE2_TOF_ALARM_EN                : out STD_LOGIC; 
        GATE2_TOF_ALARM_TYPE              : out STD_LOGIC;
        GATE2_TOF_ALARM_CROSSING          : out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE2_TOF_ALARM_MIN_VALUE         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE2_TOF_ALARM_MAX_VALUE         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		GATE2_TOF_MIN_THICKNESS           : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		GATE3_EN                          : out STD_LOGIC;  
  		GATE3_START                       : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		GATE3_WIDTH                       : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);	
        GATE3_AMP_THRESHOLD               : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		GATE3_TOF_ALGORITHM               : out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
  		GATE3_TOF_AVG                     : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
        GATE3_AMP_THRESHOLD_ALARM_EN      : out STD_LOGIC; 
        GATE3_AMP_THRESHOLD_ALARM_CROSSING: out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE3_TOF_ALARM_EN                : out STD_LOGIC; 
        GATE3_TOF_ALARM_TYPE              : out STD_LOGIC;
        GATE3_TOF_ALARM_CROSSING          : out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE3_TOF_ALARM_MIN_VALUE         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE3_TOF_ALARM_MAX_VALUE         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		GATE3_TOF_MIN_THICKNESS           : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		CONFIG_FIR_AXIS_TREADY    : in STD_LOGIC;
  		CONFIG_FIR_AXIS_TDATA	  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  		CONFIG_FIR_AXIS_TLAST	  : out STD_LOGIC;
  		CONFIG_FIR_AXIS_TVALID    : out STD_LOGIC;
  		CONFIG_DAC_AXIS_TREADY    : in STD_LOGIC;
  		CONFIG_DAC_AXIS_TDATA	  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		CONFIG_DAC_AXIS_TLAST	  : out STD_LOGIC;
  		CONFIG_DAC_AXIS_TVALID    : out STD_LOGIC
  	);
  end component;

  signal aclk: STD_LOGIC:= '0';
  signal aresetn: STD_LOGIC:= '0';

  signal CONFIG_UPDATE_RECEIVER: STD_LOGIC:= '0';
  signal CONFIG_NEW_RECEIVER: STD_LOGIC;
  signal CONFIG_UPDATE_TRANSMITTER: STD_LOGIC:= '0';
  signal CONFIG_NEW_TRANSMITTER: STD_LOGIC;
  signal CONFIG_UPDATE_MAGNET: STD_LOGIC:= '0';
  signal CONFIG_NEW_MAGNET: STD_LOGIC;
  signal CONFIG_UPDATE_DSP_1: STD_LOGIC:= '0';
  signal CONFIG_NEW_DSP_1: STD_LOGIC;
  signal CONFIG_UPDATE_DSP_2: STD_LOGIC:= '0';
  signal CONFIG_NEW_DSP_2: STD_LOGIC;
  signal CONFIG_UPDATE_FIR: STD_LOGIC:= '0';
  signal CONFIG_NEW_FIR: STD_LOGIC;
  signal CONFIG_UPDATE_GATES: STD_LOGIC:= '0';
  signal CONFIG_NEW_GATES: STD_LOGIC;
  signal CONFIG_UPDATE_DAC: STD_LOGIC:= '0';
  signal CONFIG_NEW_DAC: STD_LOGIC;
  signal THERE_IS_CONFIGURATION : STD_LOGIC;
  signal CHANNEL_EN: STD_LOGIC;
  signal CONFIG_INPUT_LOAD: STD_LOGIC:= '0';
  signal CONFIG_INPUT_AXIS_TREADY: STD_LOGIC;
  signal CONFIG_INPUT_AXIS_TDATA: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');
  signal CONFIG_INPUT_AXIS_TLAST: STD_LOGIC:= '0';
  signal CONFIG_INPUT_AXIS_TVALID: STD_LOGIC:= '0';
  signal RECEIVER_SAMPLING_FREQUENCY: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal RECEIVER_DATA_WINDOW: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal RECEIVER_DELAY: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal RECEIVER_GAIN: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal RECEIVER_EXTERNAL_MULTIPLEXER_EN: STD_LOGIC;
  signal TRANSMITTER_VOLTAGE: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal TRANSMITTER_BURST_FREQUENCY: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal TRANSMITTER_N_CYCLES: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal TRANSMITTER_PRF: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal TRANSMITTER_DELAY: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal TRANSMITTER_DIRECTIONAL_PHASING: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal MAGNET_MODE: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal MAGNET_PULSE_WIDTH: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal MAGNET_INITIAL_DELAY: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal DSP_AVERAGE: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal DSP_NOISE_REDUCTION_FILTER: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal DSP_BANDPASS_EN: STD_LOGIC;
  signal DSP_BANDPASS_N_TAPS: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal DSP_BANDPASS_L: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal DSP_GAUSSIAN_EN: STD_LOGIC;
  signal DSP_GAUSSIAN_N_TAPS: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal DSP_GAUSSIAN_L: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal DSP_ENVELOPE_EN: STD_LOGIC;
  signal DSP_ENVELOPE_N_TAPS: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal DSP_ENVELOPE_L: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal DSP_ANALOG_FILTER : STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
  signal GATE1_EN: STD_LOGIC;
  signal GATE1_START: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal GATE1_WIDTH: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal GATE1_AMP_THRESHOLD: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal GATE1_TOF_ALGORITHM: STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
  signal GATE1_TOF_AVG: STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
  signal GATE1_AMP_THRESHOLD_ALARM_EN: STD_LOGIC;
  signal GATE1_AMP_THRESHOLD_ALARM_CROSSING: STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
  signal GATE1_TOF_ALARM_EN: STD_LOGIC;
  signal GATE1_TOF_ALARM_TYPE: STD_LOGIC;
  signal GATE1_TOF_ALARM_CROSSING: STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
  signal GATE1_TOF_ALARM_MIN_VALUE: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal GATE1_TOF_ALARM_MAX_VALUE: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal GATE1_TOF_MIN_THICKNESS: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal GATE2_EN: STD_LOGIC;
  signal GATE2_START: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal GATE2_WIDTH: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal GATE2_AMP_THRESHOLD: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal GATE2_TOF_ALGORITHM: STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
  signal GATE2_TOF_AVG: STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
  signal GATE2_AMP_THRESHOLD_ALARM_EN: STD_LOGIC;
  signal GATE2_AMP_THRESHOLD_ALARM_CROSSING: STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
  signal GATE2_TOF_ALARM_EN: STD_LOGIC;
  signal GATE2_TOF_ALARM_TYPE: STD_LOGIC;
  signal GATE2_TOF_ALARM_CROSSING: STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
  signal GATE2_TOF_ALARM_MIN_VALUE: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal GATE2_TOF_ALARM_MAX_VALUE: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal GATE2_TOF_MIN_THICKNESS: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal GATE3_EN: STD_LOGIC;
  signal GATE3_START: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal GATE3_WIDTH: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal GATE3_AMP_THRESHOLD: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal GATE3_TOF_ALGORITHM: STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
  signal GATE3_TOF_AVG: STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
  signal GATE3_AMP_THRESHOLD_ALARM_EN: STD_LOGIC;
  signal GATE3_AMP_THRESHOLD_ALARM_CROSSING: STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
  signal GATE3_TOF_ALARM_EN: STD_LOGIC;
  signal GATE3_TOF_ALARM_TYPE: STD_LOGIC;
  signal GATE3_TOF_ALARM_CROSSING: STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
  signal GATE3_TOF_ALARM_MIN_VALUE: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal GATE3_TOF_ALARM_MAX_VALUE: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal GATE3_TOF_MIN_THICKNESS: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal CONFIG_FIR_AXIS_TREADY: STD_LOGIC:= '0';
  signal CONFIG_FIR_AXIS_TDATA: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  signal CONFIG_FIR_AXIS_TLAST: STD_LOGIC;
  signal CONFIG_FIR_AXIS_TVALID: STD_LOGIC;
  signal CONFIG_DAC_AXIS_TREADY: STD_LOGIC:= '0';
  signal CONFIG_DAC_AXIS_TDATA: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal CONFIG_DAC_AXIS_TLAST: STD_LOGIC;
  signal CONFIG_DAC_AXIS_TVALID: STD_LOGIC ;

  constant aclk_period: time := 10 ns;
  signal start_sm: STD_LOGIC:= '0';

  type states_config_sm is (idle, open_file, load_config, load_config_wait, read_receiver, read_transmitter,
							read_magnet, read_dsp_1, read_dsp_2, read_fir, read_gates, read_dac, close_file); 
  signal state_config_sm : states_config_sm; 

  signal channel_configuration_size: UNSIGNED(REGISTER_W32-1 downto 0):= (others => '0');
  signal channel_configuration_size_flag: STD_LOGIC:= '0';
  
  signal config_counter: UNSIGNED(REGISTER_W32-1 downto 0):= (others => '0');
  signal fir_counter: UNSIGNED(REGISTER_W32-1 downto 0):= (others => '0');
  signal dac_counter: UNSIGNED(REGISTER_W32-1 downto 0):= (others => '0');

begin
	
	-- aclk process 
    aclk_process :process
    begin
        aclk <= not aclk;
        wait for aclk_period/2;
        aclk <= not aclk;
        wait for aclk_period/2;
    end process;

  -- Insert values for generic parameters !!
  uut: channel_configuration generic map ( CHANNEL_ID                         => CHANNEL_ID,
                                           RECEIVER_SIZE                      => RECEIVER_SIZE,
                                           TRANSMITTER_SIZE                   => TRANSMITTER_SIZE,
                                           MAGNET_SIZE                        => MAGNET_SIZE,
                                           GATES_SIZE                         => GATES_SIZE,
                                           DSP_SIZE                           => DSP_SIZE,
                                           REGISTER_W32                       => REGISTER_W32,
                                           REGISTER_W16                       => REGISTER_W16,
                                           REGISTER_W8                        => REGISTER_W8,
                                           REGISTER_W4                        => REGISTER_W4 )
                                port map ( aclk                               => aclk,
                                           aresetn                            => aresetn,
                                           CONFIG_UPDATE_RECEIVER             => CONFIG_UPDATE_RECEIVER,
                                           CONFIG_NEW_RECEIVER                => CONFIG_NEW_RECEIVER,
                                           CONFIG_UPDATE_TRANSMITTER          => CONFIG_UPDATE_TRANSMITTER,
                                           CONFIG_NEW_TRANSMITTER             => CONFIG_NEW_TRANSMITTER,
                                           CONFIG_UPDATE_MAGNET               => CONFIG_UPDATE_MAGNET,
                                           CONFIG_NEW_MAGNET                  => CONFIG_NEW_MAGNET,
                                           CONFIG_UPDATE_DSP_1                => CONFIG_UPDATE_DSP_1,
                                           CONFIG_NEW_DSP_1                   => CONFIG_NEW_DSP_1,
                                           CONFIG_UPDATE_DSP_2                => CONFIG_UPDATE_DSP_2,
                                           CONFIG_NEW_DSP_2                   => CONFIG_NEW_DSP_2,
                                           CONFIG_UPDATE_FIR                  => CONFIG_UPDATE_FIR,
                                           CONFIG_NEW_FIR                     => CONFIG_NEW_FIR,
                                           CONFIG_UPDATE_GATES                => CONFIG_UPDATE_GATES,
                                           CONFIG_NEW_GATES                   => CONFIG_NEW_GATES,
                                           CONFIG_UPDATE_DAC                  => CONFIG_UPDATE_DAC,
                                           CONFIG_NEW_DAC                     => CONFIG_NEW_DAC,
                                           CHANNEL_EN                         => CHANNEL_EN,
                                           THERE_IS_CONFIGURATION             => THERE_IS_CONFIGURATION,
                                           CONFIG_INPUT_LOAD                  => CONFIG_INPUT_LOAD,
                                           CONFIG_INPUT_AXIS_TREADY           => CONFIG_INPUT_AXIS_TREADY,
                                           CONFIG_INPUT_AXIS_TDATA            => CONFIG_INPUT_AXIS_TDATA,
                                           CONFIG_INPUT_AXIS_TLAST            => CONFIG_INPUT_AXIS_TLAST,
                                           CONFIG_INPUT_AXIS_TVALID           => CONFIG_INPUT_AXIS_TVALID,
                                           RECEIVER_SAMPLING_FREQUENCY        => RECEIVER_SAMPLING_FREQUENCY,
                                           RECEIVER_DATA_WINDOW               => RECEIVER_DATA_WINDOW,
                                           RECEIVER_DELAY                     => RECEIVER_DELAY,
                                           RECEIVER_GAIN                      => RECEIVER_GAIN,
                                           RECEIVER_EXTERNAL_MULTIPLEXER_EN   => RECEIVER_EXTERNAL_MULTIPLEXER_EN,
                                           TRANSMITTER_VOLTAGE                => TRANSMITTER_VOLTAGE,
                                           TRANSMITTER_BURST_FREQUENCY        => TRANSMITTER_BURST_FREQUENCY,
                                           TRANSMITTER_N_CYCLES               => TRANSMITTER_N_CYCLES,
                                           TRANSMITTER_PRF                    => TRANSMITTER_PRF,
                                           TRANSMITTER_DELAY                  => TRANSMITTER_DELAY,
                                           TRANSMITTER_DIRECTIONAL_PHASING    => TRANSMITTER_DIRECTIONAL_PHASING,
                                           MAGNET_MODE                        => MAGNET_MODE,
                                           MAGNET_PULSE_WIDTH                 => MAGNET_PULSE_WIDTH,
                                           MAGNET_INITIAL_DELAY               => MAGNET_INITIAL_DELAY,
                                           DSP_AVERAGE                        => DSP_AVERAGE,
                                           DSP_NOISE_REDUCTION_FILTER         => DSP_NOISE_REDUCTION_FILTER,
                                           DSP_BANDPASS_EN                    => DSP_BANDPASS_EN,
                                           DSP_BANDPASS_N_TAPS                => DSP_BANDPASS_N_TAPS,
                                           DSP_BANDPASS_L                     => DSP_BANDPASS_L,
                                           DSP_GAUSSIAN_EN                    => DSP_GAUSSIAN_EN,
                                           DSP_GAUSSIAN_N_TAPS                => DSP_GAUSSIAN_N_TAPS,
                                           DSP_GAUSSIAN_L                     => DSP_GAUSSIAN_L,
                                           DSP_ENVELOPE_EN                    => DSP_ENVELOPE_EN,
                                           DSP_ENVELOPE_N_TAPS                => DSP_ENVELOPE_N_TAPS,
                                           DSP_ENVELOPE_L                     => DSP_ENVELOPE_L,
                                           DSP_ANALOG_FILTER                  => DSP_ANALOG_FILTER,
                                           GATE1_EN                           => GATE1_EN,
                                           GATE1_START                        => GATE1_START,
                                           GATE1_WIDTH                        => GATE1_WIDTH,
                                           GATE1_AMP_THRESHOLD                => GATE1_AMP_THRESHOLD,
                                           GATE1_TOF_ALGORITHM                => GATE1_TOF_ALGORITHM,
                                           GATE1_TOF_AVG                      => GATE1_TOF_AVG,
                                           GATE1_AMP_THRESHOLD_ALARM_EN       => GATE1_AMP_THRESHOLD_ALARM_EN,
                                           GATE1_AMP_THRESHOLD_ALARM_CROSSING => GATE1_AMP_THRESHOLD_ALARM_CROSSING,
                                           GATE1_TOF_ALARM_EN                 => GATE1_TOF_ALARM_EN,
                                           GATE1_TOF_ALARM_TYPE               => GATE1_TOF_ALARM_TYPE,
                                           GATE1_TOF_ALARM_CROSSING           => GATE1_TOF_ALARM_CROSSING,
                                           GATE1_TOF_ALARM_MIN_VALUE          => GATE1_TOF_ALARM_MIN_VALUE,
                                           GATE1_TOF_ALARM_MAX_VALUE          => GATE1_TOF_ALARM_MAX_VALUE,
                                           GATE1_TOF_MIN_THICKNESS            => GATE1_TOF_MIN_THICKNESS,
                                           GATE2_EN                           => GATE2_EN,
                                           GATE2_START                        => GATE2_START,
                                           GATE2_WIDTH                        => GATE2_WIDTH,
                                           GATE2_AMP_THRESHOLD                => GATE2_AMP_THRESHOLD,
                                           GATE2_TOF_ALGORITHM                => GATE2_TOF_ALGORITHM,
                                           GATE2_TOF_AVG                      => GATE2_TOF_AVG,
                                           GATE2_AMP_THRESHOLD_ALARM_EN       => GATE2_AMP_THRESHOLD_ALARM_EN,
                                           GATE2_AMP_THRESHOLD_ALARM_CROSSING => GATE2_AMP_THRESHOLD_ALARM_CROSSING,
                                           GATE2_TOF_ALARM_EN                 => GATE2_TOF_ALARM_EN,
                                           GATE2_TOF_ALARM_TYPE               => GATE2_TOF_ALARM_TYPE,
                                           GATE2_TOF_ALARM_CROSSING           => GATE2_TOF_ALARM_CROSSING,
                                           GATE2_TOF_ALARM_MIN_VALUE          => GATE2_TOF_ALARM_MIN_VALUE,
                                           GATE2_TOF_ALARM_MAX_VALUE          => GATE2_TOF_ALARM_MAX_VALUE,
                                           GATE2_TOF_MIN_THICKNESS            => GATE2_TOF_MIN_THICKNESS,
                                           GATE3_EN                           => GATE3_EN,
                                           GATE3_START                        => GATE3_START,
                                           GATE3_WIDTH                        => GATE3_WIDTH,
                                           GATE3_AMP_THRESHOLD                => GATE3_AMP_THRESHOLD,
                                           GATE3_TOF_ALGORITHM                => GATE3_TOF_ALGORITHM,
                                           GATE3_TOF_AVG                      => GATE3_TOF_AVG,
                                           GATE3_AMP_THRESHOLD_ALARM_EN       => GATE3_AMP_THRESHOLD_ALARM_EN,
                                           GATE3_AMP_THRESHOLD_ALARM_CROSSING => GATE3_AMP_THRESHOLD_ALARM_CROSSING,
                                           GATE3_TOF_ALARM_EN                 => GATE3_TOF_ALARM_EN,
                                           GATE3_TOF_ALARM_TYPE               => GATE3_TOF_ALARM_TYPE,
                                           GATE3_TOF_ALARM_CROSSING           => GATE3_TOF_ALARM_CROSSING,
                                           GATE3_TOF_ALARM_MIN_VALUE          => GATE3_TOF_ALARM_MIN_VALUE,
                                           GATE3_TOF_ALARM_MAX_VALUE          => GATE3_TOF_ALARM_MAX_VALUE,
                                           GATE3_TOF_MIN_THICKNESS            => GATE3_TOF_MIN_THICKNESS,
                                           CONFIG_FIR_AXIS_TREADY             => CONFIG_FIR_AXIS_TREADY,
                                           CONFIG_FIR_AXIS_TDATA              => CONFIG_FIR_AXIS_TDATA,
                                           CONFIG_FIR_AXIS_TLAST              => CONFIG_FIR_AXIS_TLAST,
                                           CONFIG_FIR_AXIS_TVALID             => CONFIG_FIR_AXIS_TVALID,
                                           CONFIG_DAC_AXIS_TREADY             => CONFIG_DAC_AXIS_TREADY,
                                           CONFIG_DAC_AXIS_TDATA              => CONFIG_DAC_AXIS_TDATA,
                                           CONFIG_DAC_AXIS_TLAST              => CONFIG_DAC_AXIS_TLAST,
                                           CONFIG_DAC_AXIS_TVALID             => CONFIG_DAC_AXIS_TVALID );

	stimulus: process
	begin
		aresetn <= '0';
		wait for aclk_period*10;
		aresetn <= '1';
		wait for aclk_period*10;
		start_sm <= '1';
		wait for aclk_period*10;
		start_sm <= '0';
		wait;
	end process;

    configuration_input_inst: process(aclk, aresetn)  
        variable my_counter  : integer range 0 to 16383 :=0;    

        type configDataFile is file of integer;
        file config_data_file: configDataFile;
        variable config_data: integer;     
        variable config_data_file_status: file_open_status;
    begin 
        if (aresetn = '0') then
			-- config_input
			CONFIG_INPUT_LOAD <= '0';
			CONFIG_INPUT_AXIS_TDATA <= (others => '0');
			CONFIG_INPUT_AXIS_TLAST <= '0';
			CONFIG_INPUT_AXIS_TVALID <= '0';
			-- config update
			CONFIG_UPDATE_RECEIVER <= '0';
			CONFIG_UPDATE_TRANSMITTER <= '0';
			CONFIG_UPDATE_MAGNET <= '0';
			CONFIG_UPDATE_DSP_1 <= '0';
			CONFIG_UPDATE_DSP_2 <= '0';
			CONFIG_UPDATE_FIR <= '0';
			CONFIG_UPDATE_GATES <= '0';
			CONFIG_UPDATE_DAC <= '0';
			-- tready fir and dac
			CONFIG_FIR_AXIS_TREADY <= '1';
			CONFIG_DAC_AXIS_TREADY <= '1';
			channel_configuration_size_flag <= '1';
			fir_counter <= (others => '0');
			dac_counter <= (others => '0');
			-- state																		  	
            state_config_sm <= idle;  
        elsif (aclk'event and aclk = '1') then
            if(CONFIG_FIR_AXIS_TVALID = '1') then
                fir_counter <= fir_counter + 1;
            else
                fir_counter <= (others => '0');
            end if;
            if(CONFIG_DAC_AXIS_TVALID = '1') then
                dac_counter <= dac_counter + 1;
            else
                dac_counter <= (others => '0');
            end if;
            case state_config_sm is  
                when idle =>         
                    if(start_sm = '1') then
                        my_counter := 0;
                        state_config_sm <= open_file;        
                    end if;
                when open_file => 
                    file_open(config_data_file_status, config_data_file, "configuration.dat", read_mode);
                    channel_configuration_size <= (others => '1');
                    channel_configuration_size_flag <= '1';
                    config_counter <= (others => '0');
                   	my_counter := 0;                                                                       	  
                    state_config_sm <= load_config;  
                when load_config =>
					CONFIG_INPUT_LOAD <= '1';
                    if(CONFIG_INPUT_AXIS_TREADY = '1') then
						-- tdata
                     	if not endfile(config_data_file) then
                        	read (config_data_file, config_data);
                            CONFIG_INPUT_AXIS_TDATA<=std_logic_vector(to_signed(config_data, CONFIG_INPUT_AXIS_TDATA'length));
                        end if;
						-- tvalid														
						if(my_counter >= 6) then													
							CONFIG_INPUT_AXIS_TVALID <= '1';
						end if;
						if(my_counter = 6) then													
							channel_configuration_size <= to_unsigned(config_data, channel_configuration_size'length);
							channel_configuration_size_flag <= '0';
						end if;	
						if not endfile(config_data_file) then
                            read (config_data_file, config_data);
                        end if;	
						-- tlast											  
						--if(channel_configuration_size_flag = '0' and my_counter >= channel_configuration_size+6) then
						if(channel_configuration_size_flag = '0' and my_counter >= 1617+6-1) then
							CONFIG_INPUT_AXIS_TLAST <= '1';
							my_counter := 0; 										  
							state_config_sm <= load_config_wait;											  
						else
							-- counter										  
							my_counter := my_counter + 1;										  
						end if;
						config_counter <= config_counter + 1;     
                    end if;																		
				when load_config_wait =>										  
					CONFIG_INPUT_LOAD <= '0';
					CONFIG_INPUT_AXIS_TDATA <= (others => '0');
					CONFIG_INPUT_AXIS_TLAST <= '0';
					CONFIG_INPUT_AXIS_TVALID <= '0';													  
					if(my_counter >= 16) then
						if(CONFIG_NEW_RECEIVER = '1') then
							state_config_sm <= read_receiver;
						elsif(CONFIG_NEW_TRANSMITTER = '1') then	
							state_config_sm <= read_transmitter;
						elsif(CONFIG_NEW_MAGNET = '1') then	
							state_config_sm <= read_magnet;
						elsif(CONFIG_NEW_DSP_1 = '1') then	
							state_config_sm <= read_dsp_1;
						elsif(CONFIG_NEW_DSP_2 = '1') then	
							state_config_sm <= read_dsp_2;
						elsif(CONFIG_NEW_FIR = '1') then	
							state_config_sm <= read_fir;
						elsif(CONFIG_NEW_GATES = '1') then	
							state_config_sm <= read_gates;	
						elsif(CONFIG_NEW_DAC = '1') then	
							state_config_sm <= read_dac;
						else
							my_counter := 0;											  	
							state_config_sm <= close_file;											  
						end if;											  
					else
						my_counter := my_counter + 1;										  
					end if;	
				when read_receiver =>
					if(CONFIG_NEW_RECEIVER = '0') then
						CONFIG_UPDATE_RECEIVER <= '0';	
						state_config_sm <= load_config_wait;											  
					else
						CONFIG_UPDATE_RECEIVER <= '1';												  
					end if;												  												  
				when read_transmitter =>
					if(CONFIG_NEW_TRANSMITTER = '0') then
						CONFIG_UPDATE_TRANSMITTER <= '0';	
						state_config_sm <= load_config_wait;											  
					else
						CONFIG_UPDATE_TRANSMITTER <= '1';												  
					end if;														  
				when read_magnet =>
					if(CONFIG_NEW_MAGNET = '0') then
						CONFIG_UPDATE_MAGNET <= '0';	
						state_config_sm <= load_config_wait;											  
					else
						CONFIG_UPDATE_MAGNET <= '1';												  
					end if;														  
				when read_dsp_1 =>
					if(CONFIG_NEW_DSP_1 = '0') then
						CONFIG_UPDATE_DSP_1 <= '0';	
						state_config_sm <= load_config_wait;											  
					else
						CONFIG_UPDATE_DSP_1 <= '1';												  
					end if;														  
				when read_dsp_2 =>
					if(CONFIG_NEW_DSP_2 = '0') then
						CONFIG_UPDATE_DSP_2 <= '0';	
						state_config_sm <= load_config_wait;											  
					else
						CONFIG_UPDATE_DSP_2 <= '1';												  
					end if;														  
				when read_fir =>
					if(CONFIG_NEW_FIR = '0') then
						CONFIG_UPDATE_FIR <= '0';	
						state_config_sm <= load_config_wait;											  
					else
						CONFIG_UPDATE_FIR <= '1';												  
					end if;														  
				when read_gates =>
					if(CONFIG_NEW_GATES = '0') then
						CONFIG_UPDATE_GATES <= '0';	
						state_config_sm <= load_config_wait;											  
					else
						CONFIG_UPDATE_GATES <= '1';												  
					end if;														  
				when read_dac =>													  
					if(CONFIG_NEW_DAC = '0') then
						CONFIG_UPDATE_DAC <= '0';	
						state_config_sm <= load_config_wait;											  
					else
						CONFIG_UPDATE_DAC <= '1';												  
					end if;														  
                when close_file =>
                    if(my_counter >= 32) then
                        file_close(config_data_file);
                        state_config_sm <= open_file;
                    else
                        my_counter := my_counter + 1;
                    end if;   
                when others =>
                    null;                   
            end case;                                                                                            
        end if;
    end process; 

end;