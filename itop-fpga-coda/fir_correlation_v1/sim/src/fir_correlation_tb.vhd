library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity fir_correlation_tb is
	generic (
    REGISTER_W48        : INTEGER    := 48;
    REGISTER_W32        : INTEGER    := 32;
    REGISTER_W16        : INTEGER    := 16;
    N_TAPS              : INTEGER    := 64;
    N_SETS_TAP          : INTEGER    := 3;
    OVERSAMPLING        : INTEGER    := 2;
    WAIT_CYCLES         : INTEGER    := 4
    );
end;

architecture bench of fir_correlation_tb is
 
component fir_correlation is
	generic (
	    REGISTER_W48        : INTEGER    := 48;
        REGISTER_W32        : INTEGER    := 32;
        REGISTER_W16        : INTEGER    := 16;
        N_TAPS              : INTEGER    := 512;
        N_SETS_TAP          : INTEGER    := 3;
        OVERSAMPLING        : INTEGER    := 2;
        WAIT_CYCLES         : INTEGER    := 4
    );
    port (
        aclk         : in std_logic;
        aresetn      : in std_logic;
        
        NO_READ_FIR_RESULT          : in STD_LOGIC;
        READ_FIR_RESULT             : in STD_LOGIC;
        READ_DEC_RESULT             : in STD_LOGIC;
        READ_FIR_RESULT_TYPE        : in STD_LOGIC;
        DATA_WINDOW                 : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        SAMPLE_FREQ                 : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        DATA_WINDOW_RESULT          : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        SAMPLE_FREQ_RESULT	        : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        DATA_WINDOW_HALF_RESULT     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        
        BAND_PASS_FILTER_EN         : in STD_LOGIC;
        BAND_PASS_FILTER_L_DIVIDER  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GAUSSIAN_FILTER_EN          : in STD_LOGIC;
        GAUSSIAN_FILTER_L_DIVIDER   : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        ENVELOPE_FILTER_EN          : in STD_LOGIC;
        ENVELOPE_FILTER_L_DIVIDER   : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        
        FIR_END                     : out STD_LOGIC;
                     
        FIR_TAPS_AXIS_TREADY        : out STD_LOGIC;
        FIR_TAPS_AXIS_TDATA         : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        FIR_TAPS_AXIS_TLAST         : in STD_LOGIC;
        FIR_TAPS_AXIS_TVALID        : in STD_LOGIC;
     
        SIGNAL_INPUT_AXIS_TREADY    : out STD_LOGIC;
        SIGNAL_INPUT_AXIS_TDATA     : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        SIGNAL_INPUT_AXIS_TLAST     : in STD_LOGIC;
        SIGNAL_INPUT_AXIS_TVALID    : in STD_LOGIC;
        
        GATES_EN                    : in STD_LOGIC;
        GATES_SAMPLE_START          : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATES_SAMPLE_END            : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        
        GATES_AXIS_TVALID           : out STD_LOGIC;
        GATES_AXIS_TDATA            : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATES_AXIS_TLAST            : out STD_LOGIC;
        GATES_AXIS_TREADY           : in STD_LOGIC;
        
        CORRELATION_EN              : in STD_LOGIC;
        CORRELATION_GATE_SAMPLE_START      : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        CORRELATION_GATE_SAMPLE_END : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        
        CORRELATION_AXIS_TVALID     : out STD_LOGIC;
        CORRELATION_AXIS_TDATA      : out STD_LOGIC_VECTOR(REGISTER_W48-1 downto 0);
        CORRELATION_AXIS_TLAST      : out STD_LOGIC;
        CORRELATION_AXIS_TREADY     : in STD_LOGIC;
        
		-- FIR Result for decimation
        DEC_AXIS_TVALID      : out STD_LOGIC;
        DEC_AXIS_TDATA       : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        DEC_AXIS_TLAST       : out STD_LOGIC;
        DEC_AXIS_TREADY      : in STD_LOGIC;

        -- FIR Result
        FIR_RESULT_AXIS_TVALID      : out STD_LOGIC;
        FIR_RESULT_AXIS_TDATA       : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        FIR_RESULT_AXIS_TLAST       : out STD_LOGIC;
        FIR_RESULT_AXIS_TREADY      : in STD_LOGIC
    );
  end component;

  signal aclk: std_logic:= '0';
  signal aresetn: std_logic:= '0';
  
  constant aclk_period: time := 10 ns;
  
  signal NO_READ_FIR_RESULT: std_logic:= '0';
  signal READ_FIR_RESULT: std_logic:= '0';
  signal READ_DEC_RESULT: std_logic:= '0';
  signal READ_FIR_RESULT_TYPE:  std_logic:= '0';
  signal DATA_WINDOW: std_logic_vector(REGISTER_W32-1 downto 0):= (others => '0');
  signal SAMPLE_FREQ: std_logic_vector(REGISTER_W32-1 downto 0):= (others => '0');
  signal DATA_WINDOW_RESULT: std_logic_vector(REGISTER_W32-1 downto 0);
  signal SAMPLE_FREQ_RESULT: std_logic_vector(REGISTER_W32-1 downto 0):= (others => '0');
  signal DATA_WINDOW_HALF_RESULT: std_logic_vector(REGISTER_W32-1 downto 0);
  signal BAND_PASS_FILTER_EN: std_logic:= '0';
  signal BAND_PASS_FILTER_L_DIVIDER: std_logic_vector(REGISTER_W32-1 downto 0):= (others => '0');
  signal GAUSSIAN_FILTER_EN: std_logic:= '0';
  signal GAUSSIAN_FILTER_L_DIVIDER: std_logic_vector(REGISTER_W32-1 downto 0):= (others => '0');
  signal ENVELOPE_FILTER_EN: std_logic:= '0';
  signal ENVELOPE_FILTER_L_DIVIDER: std_logic_vector(REGISTER_W32-1 downto 0):= (others => '0');
  signal FIR_END: std_logic;
  signal fir_taps_axis_tready: std_logic;
  signal fir_taps_axis_tdata: std_logic_vector(REGISTER_W16-1 downto 0):= (others => '0');
  signal fir_taps_axis_tlast: std_logic:= '0';
  signal fir_taps_axis_tvalid: std_logic:= '0';
  signal signal_input_axis_tready: std_logic;
  signal signal_input_axis_tdata: std_logic_vector(REGISTER_W16-1 downto 0):= (others => '0');
  signal signal_input_axis_tlast: std_logic:= '0';
  signal signal_input_axis_tvalid: std_logic:= '0';
  signal CORRELATION_EN : std_logic := '0'; 
  signal GATES_EN : std_logic := '0';
  signal GATES_SAMPLE_START: std_logic_vector(REGISTER_W16-1 downto 0):= (others => '0');
  signal GATES_SAMPLE_END: std_logic_vector(REGISTER_W16-1 downto 0):= (others => '0');
  signal gates_axis_tvalid: std_logic;
  signal gates_axis_tdata: std_logic_vector(REGISTER_W16-1 downto 0);
  signal gates_axis_tlast: std_logic;
  signal gates_axis_tready: std_logic:= '0';
  signal CORRELATION_GATE_SAMPLE_START: std_logic_vector(REGISTER_W16-1 downto 0):= (others => '0');
  signal CORRELATION_GATE_SAMPLE_END: std_logic_vector(REGISTER_W16-1 downto 0):= (others => '0');
  signal correlation_axis_tvalid: std_logic;
  signal correlation_axis_tdata: std_logic_vector(REGISTER_W48-1 downto 0);
  signal correlation_axis_tlast: std_logic;
  signal correlation_axis_tready: std_logic:= '0';
  signal DEC_AXIS_TVALID: std_logic;
  signal DEC_AXIS_TDATA: std_logic_vector(REGISTER_W16-1 downto 0);
  signal DEC_AXIS_TLAST: std_logic;
  signal DEC_AXIS_TREADY: std_logic:= '0';
  signal fir_result_axis_tvalid: std_logic;
  signal fir_result_axis_tdata: std_logic_vector(REGISTER_W32-1 downto 0);
  signal fir_result_axis_tlast: std_logic;
  signal fir_result_axis_tready: std_logic:= '0';
  
  type states_sm is (idle, open_files, load_taps, load_signal, wait_gates, wait_corr, check_fir_end, read_result, close_files); 
  signal state_sm : states_sm; 
  
  signal start_sm: std_logic:= '0';
  signal corr_on: std_logic:= '0';
  signal gates_en_i: std_logic:= '0';
  
  signal fir_result_axis_tdata_a: std_logic_vector(REGISTER_W16-1 downto 0);
  signal fir_result_axis_tdata_b: std_logic_vector(REGISTER_W16-1 downto 0);
  
  signal fir_result_axis_tdata_counter: unsigned(REGISTER_W32-1 downto 0);

begin

    fir_result_axis_tdata_a <= fir_result_axis_tdata(REGISTER_W32-1 downto REGISTER_W16);
    fir_result_axis_tdata_b <= fir_result_axis_tdata(REGISTER_W16-1 downto 0);

   -- aclk process 
    aclk_process :process
    begin
        aclk <= not aclk;
        wait for aclk_period/2;
        aclk <= not aclk;
        wait for aclk_period/2;
    end process;
    
  -- Insert values for generic parameters !!
  uut: fir_correlation generic map ( REGISTER_W48             => REGISTER_W48,
                                REGISTER_W32        => REGISTER_W32,
                                N_TAPS                     => N_TAPS,
                                N_SETS_TAP                 => N_SETS_TAP,
                                OVERSAMPLING               => OVERSAMPLING,
                                WAIT_CYCLES                => WAIT_CYCLES)
                     port map ( aclk                       => aclk,
                                aresetn                    => aresetn,
                                NO_READ_FIR_RESULT         => NO_READ_FIR_RESULT,
                                READ_FIR_RESULT            => READ_FIR_RESULT,
                                READ_DEC_RESULT            => READ_DEC_RESULT,
                                READ_FIR_RESULT_TYPE       => READ_FIR_RESULT_TYPE,
                                DATA_WINDOW                => DATA_WINDOW,
                                SAMPLE_FREQ                => SAMPLE_FREQ,
                                DATA_WINDOW_RESULT         => DATA_WINDOW_RESULT,
                                SAMPLE_FREQ_RESULT         => SAMPLE_FREQ_RESULT,
                                DATA_WINDOW_HALF_RESULT    => DATA_WINDOW_HALF_RESULT,
                                BAND_PASS_FILTER_EN        => BAND_PASS_FILTER_EN,
                                BAND_PASS_FILTER_L_DIVIDER => BAND_PASS_FILTER_L_DIVIDER,
                                GAUSSIAN_FILTER_EN         => GAUSSIAN_FILTER_EN,
                                GAUSSIAN_FILTER_L_DIVIDER  => GAUSSIAN_FILTER_L_DIVIDER,
                                ENVELOPE_FILTER_EN         => ENVELOPE_FILTER_EN,
                                ENVELOPE_FILTER_L_DIVIDER  => ENVELOPE_FILTER_L_DIVIDER,
                                FIR_END                    => FIR_END,
                                fir_taps_axis_tready       => fir_taps_axis_tready,
                                fir_taps_axis_tdata        => fir_taps_axis_tdata,
                                fir_taps_axis_tlast        => fir_taps_axis_tlast,
                                fir_taps_axis_tvalid       => fir_taps_axis_tvalid,
                                signal_input_axis_tready   => signal_input_axis_tready,
                                signal_input_axis_tdata    => signal_input_axis_tdata,
                                signal_input_axis_tlast    => signal_input_axis_tlast,
                                signal_input_axis_tvalid   => signal_input_axis_tvalid,
                                GATES_EN                   => GATES_EN,
                                GATES_SAMPLE_START                => GATES_SAMPLE_START,
                                GATES_SAMPLE_END                  => GATES_SAMPLE_END,
                                gates_axis_tvalid          => gates_axis_tvalid,
                                gates_axis_tdata           => gates_axis_tdata,
                                gates_axis_tlast           => gates_axis_tlast,
                                gates_axis_tready          => gates_axis_tready,
                                CORRELATION_EN             => CORRELATION_EN,
                                CORRELATION_GATE_SAMPLE_START                 => CORRELATION_GATE_SAMPLE_START,
                                CORRELATION_GATE_SAMPLE_END                   => CORRELATION_GATE_SAMPLE_END,
                                correlation_axis_tvalid    => correlation_axis_tvalid,
                                correlation_axis_tdata     => correlation_axis_tdata,
                                correlation_axis_tlast     => correlation_axis_tlast,
                                correlation_axis_tready    => correlation_axis_tready,                            
                                DEC_AXIS_TVALID      => DEC_AXIS_TVALID,
                                DEC_AXIS_TDATA       => DEC_AXIS_TDATA,
                                DEC_AXIS_TLAST       => DEC_AXIS_TLAST,
                                DEC_AXIS_TREADY      => DEC_AXIS_TREADY,
                                fir_result_axis_tvalid     => fir_result_axis_tvalid,
                                fir_result_axis_tdata      => fir_result_axis_tdata,
                                fir_result_axis_tlast      => fir_result_axis_tlast,
                                fir_result_axis_tready     => fir_result_axis_tready );

  stimulus: process
  begin
    aresetn <= '0';
    wait for aclk_period*10;
    aresetn <= '1';
    wait for aclk_period*10;
    start_sm <= '1';
    wait for aclk_period*10;
    start_sm <= '0';
    wait;
  end process;
  
  CORRELATION_EN <= corr_on;
  GATES_EN <= gates_en_i;
  
    process_inst: process(aclk, aresetn)  
        variable my_counter  : integer range 0 to 16383 :=0;
        
        type tapsDataFile is file of integer;
        file taps_data_file: tapsDataFile;
        variable taps_fir: integer;
        variable taps_data_file_status: file_open_status;
        
        type signalDataFile is file of integer;
        file signal_data_file: tapsDataFile;
        variable signal_fir: integer;     
        variable signal_data_file_status: file_open_status;                   
    begin 
        if (aresetn = '0') then
            DATA_WINDOW <= (others => '0');
            BAND_PASS_FILTER_EN <= '0';
            BAND_PASS_FILTER_L_DIVIDER <= (others => '0');
            GAUSSIAN_FILTER_EN <= '0';
            GAUSSIAN_FILTER_L_DIVIDER <= (others => '0');
            ENVELOPE_FILTER_EN <= '0';
            ENVELOPE_FILTER_L_DIVIDER <= (others => '0');
            fir_taps_axis_tdata <= (others => '0');
            fir_taps_axis_tvalid <= '0';
            fir_taps_axis_tlast <= '0';
            signal_input_axis_tdata <= (others => '0');
            signal_input_axis_tvalid <= '0';
            signal_input_axis_tlast <= '0';
            gates_axis_tready   <= '0';
            fir_result_axis_tready <= '0';
            gates_en_i <= '1';
            GATES_SAMPLE_START <= (others => '0');
            GATES_SAMPLE_END <= (others => '0');
            CORRELATION_GATE_SAMPLE_START <= (others => '0');
            CORRELATION_GATE_SAMPLE_END <= (others => '0');
            READ_FIR_RESULT <= '0';
            READ_DEC_RESULT <= '0';
            corr_on <= '0';
            correlation_axis_tready <= '0';
            READ_FIR_RESULT_TYPE <= '0';
            DEC_AXIS_TREADY <= '1';
            state_sm <= idle;  
        elsif (aclk'event and aclk = '1') then
            if(fir_result_axis_tvalid = '1') then
                fir_result_axis_tdata_counter <= fir_result_axis_tdata_counter + 1;
            else
                fir_result_axis_tdata_counter <= (others => '0');
            end if;
            case state_sm is  
                when idle =>              
                    --NO_READ_RESULT <= '1';   
                    DATA_WINDOW <= std_logic_vector(to_unsigned(1024, DATA_WINDOW'length));
                    --DATA_WINDOW <= std_logic_vector(to_unsigned(1023, DATA_WINDOW'length));
                    --DATA_WINDOW <= std_logic_vector(to_unsigned(32, DATA_WINDOW'length));
                    BAND_PASS_FILTER_EN <= '1';
                    --BAND_PASS_FILTER_EN <= '0';
                    GAUSSIAN_FILTER_EN  <= '1';
                    --GAUSSIAN_FILTER_EN  <= '0';
                    --ENVELOPE_FILTER_EN  <= '1';
                    ENVELOPE_FILTER_EN  <= '0';
                    BAND_PASS_FILTER_L_DIVIDER  <= std_logic_vector(to_unsigned(3, BAND_PASS_FILTER_L_DIVIDER'length));
                    GAUSSIAN_FILTER_L_DIVIDER   <= std_logic_vector(to_unsigned(3, BAND_PASS_FILTER_L_DIVIDER'length));
                    ENVELOPE_FILTER_L_DIVIDER   <= std_logic_vector(to_unsigned(3, BAND_PASS_FILTER_L_DIVIDER'length));
                    gates_axis_tready   <= '1';
                    fir_result_axis_tready <= '1';
                    correlation_axis_tready <= '1';
                    gates_en_i <= '1';
                    --gates_en_i <= '0';
                    --GATES_SAMPLE_START <= std_logic_vector(to_unsigned(3, GATES_SAMPLE_START'length));
                    GATES_SAMPLE_START <= std_logic_vector(to_unsigned(256, GATES_SAMPLE_START'length));
                    --GATES_SAMPLE_END   <= std_logic_vector(to_unsigned(29, GATES_SAMPLE_END'length));
                    GATES_SAMPLE_END   <= std_logic_vector(to_unsigned(511, GATES_SAMPLE_END'length));
                    corr_on <= '1';
                    --corr_on <= '0';
                    --GATE_START <= std_logic_vector(to_unsigned(8, GATE_START'length));
                    CORRELATION_GATE_SAMPLE_START <= std_logic_vector(to_unsigned(256, CORRELATION_GATE_SAMPLE_START'length));
                    CORRELATION_GATE_SAMPLE_END   <= std_logic_vector(to_unsigned(516, CORRELATION_GATE_SAMPLE_END'length));
                    READ_FIR_RESULT_TYPE <= '1';
                    --READ_FIR_RESULT_TYPE <= '0';
                    DEC_AXIS_TREADY <= '1';
                    if(start_sm = '1') then
                        fir_taps_axis_tdata <= (others => '0');
                        my_counter := 0;
                        state_sm <= open_files;        
                    end if;
                when open_files =>
                    file_open(taps_data_file_status, taps_data_file, "taps_fir.dat", read_mode);
                    file_open(signal_data_file_status, signal_data_file, "signal_fir.dat", read_mode);
                    state_sm <= load_taps; 
                when load_taps => 
                    if(fir_taps_axis_tready = '1') then
                        if(my_counter <= (n_taps*n_sets_tap)-1) then
                            fir_taps_axis_tvalid <= '1';
                            if not endfile(taps_data_file) then
                                read (taps_data_file, taps_fir);
                                fir_taps_axis_tdata<=std_logic_vector(to_signed(taps_fir, fir_taps_axis_tdata'length));
                            end if;
                            if not endfile(taps_data_file) then
                                read (taps_data_file, taps_fir);
                            end if;
                            if(my_counter >= (n_taps*n_sets_tap)-1) then
                                fir_taps_axis_tlast <= '1';
                            end if;
                            my_counter := my_counter + 1;
                        else
                            fir_taps_axis_tvalid    <= '0';
                            fir_taps_axis_tlast     <= '0';
                            my_counter              := 0;
                            state_sm                <= load_signal;
                        end if;
                    end if;                                                                           
                when load_signal =>
                    if(signal_input_axis_tready = '1') then
                        if(my_counter <= (to_integer(unsigned(DATA_WINDOW))-1)) then
                            signal_input_axis_tvalid <= '1';
                            if not endfile(signal_data_file) then
                                read (signal_data_file, signal_fir);
                                signal_input_axis_tdata<=std_logic_vector(to_signed(signal_fir, signal_input_axis_tdata'length));
                            end if;
                            if not endfile(signal_data_file) then
                                read (signal_data_file, signal_fir);
                            end if;
                            if(my_counter >= (to_integer(unsigned(DATA_WINDOW))-1)) then
                                signal_input_axis_tlast <= '1';
                                my_counter  := 0;
                                state_sm    <= wait_gates;
                            end if;
                            my_counter := my_counter + 1;
                        else
                            signal_input_axis_tvalid    <= '0';
                            signal_input_axis_tlast     <= '0';
                            if(FIR_END = '1') then
                                my_counter  := 0;
                                state_sm    <= wait_gates;
                            end if;             
                        end if;  
                    end if;
                    if(FIR_END = '1') then
                        my_counter  := 0;
                        state_sm    <= wait_gates;
                    end if; 
                when wait_gates =>
                    signal_input_axis_tvalid <= '0';
                    signal_input_axis_tlast <= '0';
                    if(gates_en_i = '1') then
                        if(gates_axis_tlast = '1') then
                            if(corr_on = '1') then
                                state_sm    <= wait_corr;
                            else
                                state_sm    <= check_fir_end;
                            end if;    
                        end if;
                    else
                        state_sm    <= check_fir_end;
                    end if;
                when wait_corr =>
                    if(corr_on = '1') then
                        if(correlation_axis_tlast = '1') then
                            state_sm <= check_fir_end;
                        end if;
                    else
                        state_sm <= check_fir_end;
                    end if;
                when check_fir_end =>
                    if(FIR_END = '1') then
                        --NO_READ_FIR_RESULT <= '1';
                        state_sm <= read_result;
                    end if;
                when read_result =>
                    if(NO_READ_FIR_RESULT = '0') then
                        if(fir_result_axis_tlast = '1' or DEC_AXIS_TLAST = '1') then
                            READ_FIR_RESULT <= '0';
                            READ_DEC_RESULT <= '0';
                            state_sm    <= close_files;
                        else
                            if(READ_FIR_RESULT_TYPE = '0') then
                                READ_FIR_RESULT <= '1';
                            else
                                READ_DEC_RESULT <= '1';
                            end if;
                        end if; 
                    else
                        state_sm    <= close_files;
                    end if;
                when close_files =>
                    NO_READ_FIR_RESULT <= '0';
                    if(my_counter >= 128) then
                        file_close(taps_data_file);  
                        file_close(signal_data_file);
                        my_counter := 0;
                        state_sm <= open_files;   
                    else
                        my_counter := my_counter + 1;
                    end if;
                when others =>
                    null;                   
            end case;                                                                                            
        end if;
   end process; 
   
end;
