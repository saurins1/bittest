----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 09/27/2017 10:07:23 AM
-- Design Name: 
-- Module Name: average filter light - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity average_filter is
	generic (
        REGISTER_W32    : INTEGER := 32;
        REGISTER_W16    : INTEGER := 16;
        REGISTER_W14    : INTEGER := 14;
        TRIGGER_WAIT    : INTEGER := 32;
        MIN_AVG_LEVEL   : INTEGER := 2
	);
	port (
	    -- Sync
	    aclk         : in STD_LOGIC;
        aresetn      : in STD_LOGIC;
	
	    -- Config
	    AVG_LEVEL	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	    DATA_WINDOW	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	    SAMPLE_FREQ	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	    DATA_WINDOW_RESULT	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	    SAMPLE_FREQ_RESULT	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		AVG_WINDOW  	  : out STD_LOGIC;

		-- Signal input
		SIGNAL_INPUT_AXIS_TREADY  : out STD_LOGIC;
		SIGNAL_INPUT_AXIS_TDATA	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		SIGNAL_INPUT_AXIS_TLAST	  : in STD_LOGIC;
		SIGNAL_INPUT_AXIS_TVALID  : in STD_LOGIC;

		-- Average result
		AVG_RESULT_AXIS_TVALID    : out STD_LOGIC;
		AVG_RESULT_AXIS_TDATA	  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		AVG_RESULT_AXIS_TLAST	  : out STD_LOGIC;
		AVG_RESULT_AXIS_TREADY    : in STD_LOGIC
	);
end average_filter;

architecture arch_imp of average_filter is

    -- Delayed signal input
    type signal_input_axis_tdata_array is array ( 0 to 7 ) of STD_LOGIC_VECTOR(13 downto 0);
    signal signal_input_axis_tdata_i    : signal_input_axis_tdata_array;
    signal signal_input_axis_tlast_i    : STD_LOGIC_VECTOR(7 downto 0);
    signal signal_input_axis_tvalid_i   : STD_LOGIC_VECTOR(7 downto 0);

    -- Delayed average result
    signal avg_result_axis_tlast_i    : STD_LOGIC_VECTOR(3 downto 0);
    signal avg_result_axis_tvalid_i   : STD_LOGIC_VECTOR(3 downto 0);
  
    -- BRAM
    COMPONENT average_filter_BRAM 
      PORT (
        clka : IN STD_LOGIC;
        ena : IN STD_LOGIC;
        wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addra : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        dina : IN STD_LOGIC_VECTOR(20 DOWNTO 0);
        douta : OUT STD_LOGIC_VECTOR(20 DOWNTO 0);
        clkb : IN STD_LOGIC;
        enb : IN STD_LOGIC;
        web : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addrb : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        dinb : IN STD_LOGIC_VECTOR(20 DOWNTO 0);
        doutb : OUT STD_LOGIC_VECTOR(20 DOWNTO 0)
      );
    END COMPONENT;
    
    -- average BRAM port a
    signal average_bram_addra   : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal average_bram_dina    : STD_LOGIC_VECTOR ( 20 downto 0 );
    signal average_bram_douta   : STD_LOGIC_VECTOR ( 20 downto 0 );
    signal average_bram_ena     : STD_LOGIC;
    signal average_bram_wea     : STD_LOGIC_VECTOR ( 0 to 0 );

	signal average_bram_addra_first : STD_LOGIC;
    
    -- average BRAM port b
    signal average_bram_addrb   : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal average_bram_dinb    : STD_LOGIC_VECTOR ( 20 downto 0 );
    signal average_bram_doutb   : STD_LOGIC_VECTOR ( 20 downto 0 );
    signal average_bram_enb     : STD_LOGIC;
    signal average_bram_web     : STD_LOGIC_VECTOR ( 0 to 0 );

	signal average_bram_addrb_first : STD_LOGIC;
   
    -- Average state machine
    type states_general_sm is (idle, first_frame, next_frame, check_frame, read_result); 
    signal state_general_sm : states_general_sm;

    -- BRAM porta state machine
	type states_bram_porta_sm is (idle, first_frame, next_frame); 
    signal state_bram_porta_sm : states_bram_porta_sm;

    -- BRAM portb state machine
	type states_bram_portb_sm is (idle, read_result, next_frame); 
    signal state_bram_portb_sm : states_bram_portb_sm;
   
    -- handshake flags between state machines 
    signal first_frame_flag  : STD_LOGIC;
    signal first_frame_end   : STD_LOGIC;

	signal next_frame_flag  : STD_LOGIC;
    signal next_frame_end   : STD_LOGIC;

	signal read_result_flag  : STD_LOGIC;
    signal read_result_end   : STD_LOGIC;
    
    signal signal_input_axis_tready_i1 : STD_LOGIC;
    signal signal_input_axis_tready_i2 : STD_LOGIC;
    signal signal_input_axis_tready_i3 : STD_LOGIC;

	signal input_output_bypass : STD_LOGIC;
	
	signal avg_l_divider   : integer range 0 to 16 :=0;
	signal avg_level_i     : integer range 0 to 16384 :=0;
	
	signal avg_result_axis_tdata_i : std_logic_vector(REGISTER_W14-1 downto 0);

begin
	
	-- Tready in													
	SIGNAL_INPUT_AXIS_TREADY <= signal_input_axis_tready_i1 and signal_input_axis_tready_i2 and signal_input_axis_tready_i3;
    
    -- Divider factor for avg result	
    avg_l_divider <= 0 when (unsigned(AVG_LEVEL) < 2) else
                     1 when ((unsigned(AVG_LEVEL) >= 2) and (unsigned(AVG_LEVEL) < 4)) else
                     2 when ((unsigned(AVG_LEVEL) >= 4) and (unsigned(AVG_LEVEL) < 8)) else
                     3 when ((unsigned(AVG_LEVEL) >= 8) and (unsigned(AVG_LEVEL) < 16)) else
                     4 when ((unsigned(AVG_LEVEL) >= 16) and (unsigned(AVG_LEVEL) < 32)) else
                     5 when ((unsigned(AVG_LEVEL) >= 32) and (unsigned(AVG_LEVEL) < 64)) else
                     6 when ((unsigned(AVG_LEVEL) >= 64) and (unsigned(AVG_LEVEL) < 128)) else
                     7 when ((unsigned(AVG_LEVEL) >= 128)) else
                     0;	
        
	-- Updated avg level
    avg_level_i <= 0 when (unsigned(AVG_LEVEL) < 2) else
                   2 when ((unsigned(AVG_LEVEL) >= 2) and (unsigned(AVG_LEVEL) < 4)) else
                   4 when ((unsigned(AVG_LEVEL) >= 4) and (unsigned(AVG_LEVEL) < 8)) else
                   8 when ((unsigned(AVG_LEVEL) >= 8) and (unsigned(AVG_LEVEL) < 16)) else
                   16 when ((unsigned(AVG_LEVEL) >= 16) and (unsigned(AVG_LEVEL) < 32)) else
                   32 when ((unsigned(AVG_LEVEL) >= 32) and (unsigned(AVG_LEVEL) < 64)) else
                   64 when ((unsigned(AVG_LEVEL) >= 64) and (unsigned(AVG_LEVEL) < 128)) else
                   128 when ((unsigned(AVG_LEVEL) >= 128)) else
                   0;   
			
	AVG_RESULT_AXIS_TDATA <= std_logic_vector(resize(signed(avg_result_axis_tdata_i), AVG_RESULT_AXIS_TDATA'length));			
					      						
	-- Average result
    avg_result_process: process(aclk, aresetn) 
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				AVG_RESULT_AXIS_TVALID <= '0';
				AVG_RESULT_AXIS_TLAST  <= '0';
				avg_result_axis_tdata_i  <= (others => '0');												
        	else
				if(input_output_bypass = '1') then
					AVG_RESULT_AXIS_TVALID <= SIGNAL_INPUT_AXIS_TVALID;				
					AVG_RESULT_AXIS_TLAST  <= SIGNAL_INPUT_AXIS_TLAST;	
					avg_result_axis_tdata_i  <= std_logic_vector(resize(signed(SIGNAL_INPUT_AXIS_TDATA), avg_result_axis_tdata_i'length));											
				else
					AVG_RESULT_AXIS_TVALID <= avg_result_axis_tvalid_i(2);
					AVG_RESULT_AXIS_TLAST  <= avg_result_axis_tlast_i(2);
					avg_result_axis_tdata_i  <= average_bram_doutb(REGISTER_W16 + avg_l_divider -3 downto avg_l_divider);									
				end if;												
			end if;
		end if;
	end process;

    -- Average SM
    avg_inst: process(aclk, aresetn) 
        variable var_counter_wait  : integer range 0 to 65536 :=0;
        variable var_counter_avg : integer range 0 to 65536 :=0; 
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				signal_input_axis_tready_i1 <= '0';
				first_frame_flag         <= '0';
				next_frame_flag			 <= '0';
				read_result_flag		 <= '0';
				input_output_bypass		 <= '0';	
				DATA_WINDOW_RESULT		 <= (others => '0');
				SAMPLE_FREQ_RESULT	     <= (others => '0');
				AVG_WINDOW <= '0';																
				state_general_sm <= idle;
        	else
				case state_general_sm is        
					when idle =>
						signal_input_axis_tready_i1 <= '1';
						var_counter_wait := 0;
						var_counter_avg:= 0;														
						if(avg_level_i >= MIN_AVG_LEVEL) then
							input_output_bypass	<= '0';								
						  	signal_input_axis_tready_i1 <= '1';
							if(SIGNAL_INPUT_AXIS_TVALID = '1') then
								AVG_WINDOW <= '1';										
								state_general_sm <= first_frame;	
							end if;
						else											
							input_output_bypass	<= '1';										
							signal_input_axis_tready_i1 <= AVG_RESULT_AXIS_TREADY;
							if(SIGNAL_INPUT_AXIS_TVALID = '1') then												
								AVG_WINDOW <= '1';
							else
								AVG_WINDOW <= '0';											
							end if;											
						end if;
						if(SIGNAL_INPUT_AXIS_TVALID = '1') then
						    DATA_WINDOW_RESULT <= DATA_WINDOW;
						    SAMPLE_FREQ_RESULT <= SAMPLE_FREQ;
						end if;
					when first_frame =>
						if(first_frame_end = '1') then
							signal_input_axis_tready_i1 <= '0';
							first_frame_flag  <= '0';
							var_counter_avg := var_counter_avg + 1; 
							state_general_sm <= check_frame;
						 else
							first_frame_flag  <= '1';
						end if;
					when next_frame =>
						if(next_frame_end = '1') then
							signal_input_axis_tready_i1 <= '0';	
							next_frame_flag  <= '0';
							var_counter_avg := var_counter_avg + 1;
							state_general_sm <= check_frame;
						else
							signal_input_axis_tready_i1 <= '1';
							next_frame_flag <= '1';	
						end if;
					when check_frame =>
						if(var_counter_wait >= TRIGGER_WAIT) then
							var_counter_wait := 0;		
							if(avg_level_i < MIN_AVG_LEVEL) then
								state_general_sm  <= read_result;
							else
								if(var_counter_avg < avg_level_i) then
									state_general_sm  <= next_frame;
								else
									state_general_sm  <= read_result;	
								end if;
							end if;		
						else
							var_counter_wait := var_counter_wait + 1;
							signal_input_axis_tready_i1 <= '0';	
						end if;						
					when read_result =>
						AVG_WINDOW <= '0';													
						if(read_result_end = '1') then
							read_result_flag  <= '0';
							state_general_sm  <= idle;
						 else
							read_result_flag  <= '1';
						end if;
					when others =>
						null;
				end case;
			end if;
         end if;
    end process;
    	 
	-- Write first frame
	-- Write avg result
    signal_BRAM_porta: process(aclk) 
		variable var_counter_result : integer range 0 to 65536 :=0;
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                average_bram_ena   	<= '1';
                average_bram_wea   	<= (others => '0');
                average_bram_addra 	<= (others => '0');
                average_bram_dina  	<= (others => '0');
				average_bram_addra_first <= '0';
                first_frame_end			<= '0';
				next_frame_end			<= '0';
				signal_input_axis_tready_i2 <= '0';
                -- State
                state_bram_porta_sm    	<= idle;
            else     
                case state_bram_porta_sm is        
                    when idle =>
                        average_bram_wea <= (others => '0');
						var_counter_result 	 := 0;
						average_bram_addra_first <= '1';
						signal_input_axis_tready_i2 <= '1';
                        if(first_frame_flag = '1') then
							state_bram_porta_sm <= first_frame;
						elsif(next_frame_flag = '1') then
							state_bram_porta_sm <= next_frame;							
                        end if;
                    when first_frame =>
						if(first_frame_flag = '1') then
							if(signal_input_axis_tvalid_i(3) = '1') then
								if(var_counter_result >= unsigned(DATA_WINDOW)) then
									-- end
									first_frame_end   <= '1';
									signal_input_axis_tready_i2 <= '0';
									var_counter_result := to_integer(unsigned(unsigned(DATA_WINDOW)));	
								else
									-- write
									average_bram_wea   <= (others => '1');
									-- addr
									if(average_bram_addra_first = '1') then
										average_bram_addra_first <= '0';
										average_bram_addra 	<= (others => '0');	
									else
										average_bram_addra <= std_logic_vector(unsigned(average_bram_addra)+1);
									end if;
									-- din
									average_bram_dina  <= std_logic_vector(resize(signed(signal_input_axis_tdata_i(3)), average_bram_dina'length));	
									-- counter
									if(signal_input_axis_tlast_i(3) = '1') then
										first_frame_end   <= '1';
										signal_input_axis_tready_i2 <= '0';
										var_counter_result := to_integer(unsigned(unsigned(DATA_WINDOW)));
									else
										var_counter_result := var_counter_result + 1;
									end if;
								end if;
							end if;
						else						    
							first_frame_end   	<= '0';
							state_bram_porta_sm <= idle;
						end if;
					when next_frame =>
						if(next_frame_flag = '1') then
							if(signal_input_axis_tvalid_i(4) = '1') then
								if(var_counter_result >= unsigned(DATA_WINDOW)) then
									-- end
									next_frame_end <= '1';
									signal_input_axis_tready_i2 <= '0';
									var_counter_result := to_integer(unsigned(unsigned(DATA_WINDOW)));
								else
									-- write
									average_bram_wea <= (others => '1');
									-- addr
									if(average_bram_addra_first = '1') then
										average_bram_addra_first <= '0';
										average_bram_addra 	<= (others => '0');
									else
										average_bram_addra <= std_logic_vector(unsigned(average_bram_addra)+1);	
									end if;
									-- din
									average_bram_dina  <= std_logic_vector(
									                      resize(signed(signal_input_axis_tdata_i(4)), average_bram_dina'length) +
									                      signed(average_bram_doutb)
									                       );
									-- counter															  	
									if(signal_input_axis_tlast_i(4) = '1') then
                                        next_frame_end   <= '1';
                                        signal_input_axis_tready_i2 <= '0';
                                        var_counter_result := to_integer(unsigned(unsigned(DATA_WINDOW)));
                                    else
                                        var_counter_result := var_counter_result + 1;
                                    end if;	
								end if;
							end if;
						else
							next_frame_end <= '0';																		
							state_bram_porta_sm <= idle;	
						end if;
                      
                    when others => 
                        null;
                end case;                                 
            end if;
        end if;
    end process;
			
	-- Read result
	-- Read previous frame for filter
    signal_BRAM_portb: process(aclk) 
		variable var_counter_result : integer range 0 to 65536 :=0; 
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				-- Bram
                average_bram_enb   	<= '1';
                average_bram_web   	<= (others => '0');
                average_bram_addrb 	<= (others => '0');
                average_bram_dinb  	<= (others => '0');
				average_bram_addrb_first <= '0';
                read_result_end			<= '0';
                signal_input_axis_tready_i3 <= '0';
				-- Average result					
                avg_result_axis_tlast_i(0)    <= '0';
                avg_result_axis_tvalid_i(0)	<= '0';
                -- State
                state_bram_portb_sm    	<= idle;
            else     
                case state_bram_portb_sm is        
                    when idle =>
						average_bram_addrb_first <= '1';
						var_counter_result := 0;
						signal_input_axis_tready_i3 <= '1';
                        if(read_result_flag = '1') then
							state_bram_portb_sm <= read_result;
						elsif(next_frame_flag = '1') then
							state_bram_portb_sm <= next_frame;	
                        end if;
                    when read_result =>
                        signal_input_axis_tready_i3 <= '0';
						if(read_result_flag = '1') then
							if(var_counter_result >= unsigned(DATA_WINDOW)) then
								-- end
								avg_result_axis_tlast_i(0)    <= '0';
                				avg_result_axis_tvalid_i(0)	<= '0';
								read_result_end <= '1';
							else
							    if(AVG_RESULT_AXIS_TREADY = '1') then
                                    -- addr
                                    if(average_bram_addrb_first = '1') then
                                        average_bram_addrb_first <= '0';
                                        average_bram_addrb 	<= (others => '0');
                                    else
                                        average_bram_addrb <= std_logic_vector(unsigned(average_bram_addrb)+1);	
                                    end if;
                                    -- tvalid																	 
                                    avg_result_axis_tvalid_i(0) <= '1';
                                    -- tlast
                                    if(var_counter_result = unsigned(DATA_WINDOW)-1) then
                                        avg_result_axis_tlast_i(0) <= '1';	
                                    end if;
                                    -- counter
                                    var_counter_result := var_counter_result + 1;
                                else
                                    avg_result_axis_tvalid_i(0) <= '0';
                                end if;
							end if;
						else
							read_result_end   	<= '0';
							state_bram_portb_sm <= idle;
						end if;
					when next_frame =>
						if(next_frame_flag = '1') then
							if(signal_input_axis_tvalid_i(1) = '1') then
								if(var_counter_result >= unsigned(DATA_WINDOW)) then
									-- end
									signal_input_axis_tready_i3 <= '0';
									var_counter_result := to_integer(unsigned(unsigned(DATA_WINDOW)));																	  
								else
									-- addr
									if(average_bram_addrb_first = '1') then
										average_bram_addrb_first <= '0';
										average_bram_addrb 	<= (others => '0');
									else
										average_bram_addrb <= std_logic_vector(unsigned(average_bram_addrb)+1);	
									end if;
									-- counter
									if(signal_input_axis_tlast_i(4) = '1') then
                                        signal_input_axis_tready_i3 <= '0';
                                        var_counter_result := to_integer(unsigned(unsigned(DATA_WINDOW)));
                                    else
                                        var_counter_result := var_counter_result + 1;
                                    end if;    																	  
								end if;
							end if;
						else
							state_bram_portb_sm <= idle;	
						end if;
                      
                    when others => 
                        null;
                end case;                                 
            end if;
        end if;
    end process;
    
    signal_input_axis_tdata_i(0) <= std_logic_vector(resize(signed(SIGNAL_INPUT_AXIS_TDATA), signal_input_axis_tdata_i(0)'length));
    signal_input_axis_tlast_i(0) <= SIGNAL_INPUT_AXIS_TLAST;
    signal_input_axis_tvalid_i(0) <= SIGNAL_INPUT_AXIS_TVALID;
       
    -- Delay signal input     
    delay_signal_input : for i in 1 to 7 generate
        process(aclk) 
        begin
           if (rising_edge(aclk)) then
                if (aresetn = '0') then
                    signal_input_axis_tdata_i(i)    <= (others => '0');
                    signal_input_axis_tlast_i(i)    <= '0';
                    signal_input_axis_tvalid_i(i)   <= '0';
                else
                    signal_input_axis_tdata_i(i) <= signal_input_axis_tdata_i(i-1);
                    signal_input_axis_tlast_i(i) <= signal_input_axis_tlast_i(i-1);
                    signal_input_axis_tvalid_i(i) <= signal_input_axis_tvalid_i(i-1);
                end if;
           end if;
        end process;
    end generate;
			    
    -- Delay average result     
    delay_avg_result : for i in 1 to 3 generate
        process(aclk) 
        begin
           if (rising_edge(aclk)) then
                if (aresetn = '0') then
                    avg_result_axis_tlast_i(i)    <= '0';
                    avg_result_axis_tvalid_i(i)   <= '0';
                else
                    avg_result_axis_tlast_i(i) <= avg_result_axis_tlast_i(i-1);
                    avg_result_axis_tvalid_i(i) <= avg_result_axis_tvalid_i(i-1);
                end if;
           end if;
        end process;
    end generate;		   			  
      
    -- BRAM
    average_filter_BRAM_inst : average_filter_BRAM
      PORT MAP (
        clka => aclk,
        ena => average_bram_ena,
        wea => average_bram_wea,
        addra => average_bram_addra,
        dina => average_bram_dina,
        douta => average_bram_douta,
        clkb => aclk,
        enb => average_bram_enb,
        web => average_bram_web,
        addrb => average_bram_addrb,
        dinb => average_bram_dinb,
        doutb => average_bram_doutb
      );
      
end arch_imp;
