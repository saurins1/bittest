clear all;close all;

% SIGNAL 
data_window = 1025;
signal_input = zeros(1,data_window);

first_value = -1025;
for n = 1:data_window*2
  signal_input(n) = first_value + n;
end

figure(1);
plot(signal_input);

file_signal_input = fopen('signal_input.dat','w');
fwrite(file_signal_input,signal_input,'int64');
fclose(file_signal_input);

% NOISE
noise_window = 131072;
noise_value_max=15;
noise_value_min=-15;
noise_input=round(noise_value_min+rand(1,noise_window)*(noise_value_max-noise_value_min));

figure(2);
plot(noise_input);

file_noise_input= fopen('noise_input.dat','w');
fwrite(file_noise_input,noise_input,'int64');
fclose(file_noise_input);