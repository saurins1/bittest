----------------------------------------------------------------------------------
-- Company: Innerspec Technologies
-- Engineers: Wei Jiang
-- 
-- Design Name: 
-- Module Name: BURST_SEQUENCE - BURST_SEQUENCE_arch 
-- Project Name: ITOP Rollamte
-- Target Devices: 
-- Tool versions: 
-- Description: Generate Data_window for RX
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Revision 0.02 - DAC Delay add, need pass parameters
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity burst_sequence is
	generic (
	    REGISTER_W32    : INTEGER := 32;
	    REGISTER_W8     : INTEGER := 8   
	);
	port (	
		-- Sync
		aclk		: in STD_LOGIC;
		aresetn		: in STD_LOGIC;
		
		-- Inputs
		CH_TRIG					: in STD_LOGIC;
		TX_DATA_DELAY			: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);		--delays in 100MHz samples
		TX_DELTA_DELAY			: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);		--delays in 100MHz samples
        TX_DELTA_ADD            : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);     --delays in 100MHz samples
		RX_SAMPLING_FREQUENCY  	: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  	-- 100 MHz divider for sample freq
		RX_DATA_DELAY			: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);		--delays in 100MHz samples
		RX_DATA_WINDOW			: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);		--data window in sample freq samples
		SIGNAL_COINC_WINDOW  	: in STD_LOGIC;
			
		-- Outputs
		CH_ACTIVE				: out STD_LOGIC;
		TX_TRIGGER				: out STD_LOGIC;
		RX_DATA_VALID			: out STD_LOGIC;
		RX_DATA_WINDOW_ON		: out STD_LOGIC
	);
end burst_sequence;

architecture BURST_SEQUENCE_arch of burst_sequence is

type burst_seq_states is (rst_state, idle_state, tx_delay, delta_delay, delta_add,
		init_data_delay_state, data_window_state, data_window_dcm_state);
signal burst_sm_state : burst_seq_states := rst_state;

signal rx_freq_i : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0) := "00000001";
signal count_freq : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
signal count_data_window : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0) := (others => '0');
signal count_data_delay : UNSIGNED(REGISTER_W32-1 downto 0) := (others => '0');
signal count_delta_delay : UNSIGNED(REGISTER_W32-1 downto 0) := (others => '0');
signal count_delta_add : UNSIGNED(REGISTER_W32-1 downto 0) := (others => '0');
signal count_coincidence : UNSIGNED(REGISTER_W32-1 downto 0) := (others => '0');
--signal DAC_Gain_Delay		: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0) := STD_LOGIC_VECTOR(to_unsigned(50,32));

signal ch_trig_d1 : STD_LOGIC := '0';

signal delta_delay_flag : STD_LOGIC := '0';
signal delta_add_flag : STD_LOGIC := '0';
signal tx_delay_flag : STD_LOGIC := '0';

signal TX_DATA_DELAY_i	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);		--delays in 100MHz samples
signal TX_DELTA_DELAY_i	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);		--delays in 100MHz samples
signal TX_DELTA_ADD_i   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);        --delays in 100MHz samples

begin

	rx_freq_i <= RX_SAMPLING_FREQUENCY(REGISTER_W8-1 downto 0);
	
	process (aclk)
	begin
		if (rising_edge(aclk)) then
			if (aresetn = '0' ) then
				count_freq <= "00000001";
			else
				if ( count_freq < rx_freq_i ) then
					count_freq <= count_freq + 1;
				else
					count_freq <= "00000001";
				end if;
			end if;	
		end if;
	end process;	
	
	process (aclk)
	begin
		if (rising_edge(aclk)) then
			ch_trig_d1 <= CH_TRIG;
		
			case burst_sm_state is
				when rst_state =>
					if (aresetn = '0') then
						burst_sm_state <= rst_state;
					else
						burst_sm_state <= idle_state;
					end if;

					count_data_window <= (others => '0');
					count_data_delay <= (others => '0');
					count_coincidence <= (others => '0');
					CH_ACTIVE <= '0';
					RX_DATA_VALID <= '0';
					RX_DATA_WINDOW_ON <= '0';
					TX_TRIGGER <= '0';
					
				when idle_state =>
					if (CH_TRIG = '1' and ch_trig_d1 = '0') then
					    if(unsigned(TX_DATA_DELAY) > 0 and unsigned(count_coincidence) = 0) then
					        burst_sm_state <= tx_delay;
					    else
							if(unsigned(count_coincidence) > 0) then
								if(unsigned(TX_DELTA_DELAY) > 0) then
									burst_sm_state <= delta_delay;	
								elsif(unsigned(TX_DELTA_DELAY) > 0 and unsigned(count_coincidence) > 1) then
									burst_sm_state <= delta_add;
								else
									burst_sm_state <= init_data_delay_state;	
								end if;	
							else
					        	burst_sm_state <= init_data_delay_state;
							end if;
					    end if;
					else
						burst_sm_state <= idle_state;
					end if;
				
					if(SIGNAL_COINC_WINDOW = '0') then       
						count_coincidence <= (others => '0');
					end if;

					count_data_window <= (others => '0');
					count_data_delay <= (others => '0');
					count_delta_delay <= (others => '0');
					count_delta_add <= (others => '0');
					TX_DATA_DELAY_i <= TX_DATA_DELAY;
                    TX_DELTA_DELAY_i <= TX_DELTA_DELAY;
                    TX_DELTA_ADD_i <= TX_DELTA_ADD; 
					CH_ACTIVE <= '0';
					RX_DATA_VALID <= '0';
					RX_DATA_WINDOW_ON <= '0';
					TX_TRIGGER <= '0';
					
				when tx_delay =>
				    if (count_data_delay >= (unsigned(TX_DATA_DELAY_i)-1)) then
						tx_delay_flag <= '0';
						count_data_delay <= (others => '0');
                        burst_sm_state <= init_data_delay_state;
                    else
						tx_delay_flag <= '1';
                        count_data_delay <= count_data_delay + 1;
                    end if;
						
				when delta_delay =>
				    if (count_delta_delay >= (unsigned(TX_DELTA_DELAY_i)-1)) then
						delta_delay_flag <= '0';
						if(unsigned(TX_DELTA_ADD_i) > 0 and unsigned(count_coincidence) > 1) then
							count_delta_delay <= (others => '0');
							burst_sm_state <= delta_add;	
						else
							burst_sm_state <= init_data_delay_state;	
						end if;
                    else
						delta_delay_flag <= '1';
                        count_delta_delay <= count_delta_delay + 1;
                    end if;
						
				when delta_add =>
				    if (count_delta_delay >= unsigned(TX_DELTA_ADD_i)) then
						count_delta_delay <= (others => '0');
						if(count_delta_add >= (unsigned(count_coincidence)-2)) then
							delta_add_flag <= '0';
							burst_sm_state <= init_data_delay_state;		
						else
							count_delta_add <= count_delta_add + 1;	
						end if;
                    else
						delta_add_flag <= '1';
                        count_delta_delay <= count_delta_delay + 1;
                    end if;
										
				when init_data_delay_state =>
					count_data_delay <= count_data_delay + 1;
					CH_ACTIVE <= '1';
					RX_DATA_VALID <= '0';
					RX_DATA_WINDOW_ON <= '0';
					if ( count_data_delay < unsigned(RX_DATA_DELAY) ) then
						burst_sm_state <= init_data_delay_state;
					else
						count_data_window <= (others => '0');
						if ( rx_freq_i = 0 or rx_freq_i = 1 ) then
							burst_sm_state <= data_window_state;
						else
							burst_sm_state <= data_window_dcm_state;
						end if;
					end if;
					
					TX_TRIGGER <= '1';
								
				when data_window_state =>
					count_data_delay <= count_data_delay + 1;
					count_data_window <= count_data_window + 1;
					if (count_data_window < RX_DATA_WINDOW) then
						CH_ACTIVE <= '1';
						RX_DATA_VALID <= '1';
						RX_DATA_WINDOW_ON <= '1';
						burst_sm_state <= data_window_state;
					else
						CH_ACTIVE <= '0';
						RX_DATA_VALID <= '0';
						RX_DATA_WINDOW_ON <= '0';
						if(SIGNAL_COINC_WINDOW = '1') then
							count_coincidence <= count_coincidence + 1;
						end if;
						burst_sm_state <= idle_state;
					end if;

					TX_TRIGGER <= '1';

				when data_window_dcm_state =>
					count_data_delay <= count_data_delay + 1;
					if( count_freq = rx_freq_i and count_data_window < RX_DATA_WINDOW) then
						RX_DATA_VALID <= '1';
						count_data_window <= count_data_window + 1;
					else
						RX_DATA_VALID <= '0';
					end if;
					
					if (count_data_window < RX_DATA_WINDOW) then
						CH_ACTIVE <= '1';
						RX_DATA_WINDOW_ON <= '1';
						burst_sm_state <= data_window_dcm_state;
					else
						CH_ACTIVE <= '0';
						RX_DATA_WINDOW_ON <= '0';
						if(SIGNAL_COINC_WINDOW = '1') then
                            count_coincidence <= count_coincidence + 1;
                        end if;
						burst_sm_state <= idle_state;
					end if;

					TX_TRIGGER <= '1';					
	
				when others =>
					burst_sm_state <= idle_state;
					
			end case;
		end if;
	end process;
-----------------------------------------------------------------------------
end BURST_SEQUENCE_arch;

