----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 01/09/2017 10:07:23 AM
-- Design Name: 
-- Module Name: Pulser - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity pulser is
	generic (
	    REGISTER_W32    : INTEGER := 32;
		REGISTER_W30    : INTEGER := 30;
	    REGISTER_W8     : INTEGER := 8;
	    REGISTER_W2     : INTEGER := 2    
	);
	port (
	    -- Sync
	    aclk 	: in STD_LOGIC;
        aresetn	: in STD_LOGIC;
		
		-- Burst sequence
		CH_TRIG					: in STD_LOGIC;
		TX_DATA_DELAY			: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);		--delays in 100MHz samples
		TX_DELTA_DELAY			: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);		--delays in 100MHz samples
		TX_DELTA_ADD			: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);		--delays in 100MHz samples
		RX_SAMPLING_FREQUENCY  	: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  	-- 100 MHz divider for sample freq
		RX_DATA_DELAY			: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);		--delays in 100MHz samples
		RX_DATA_WINDOW			: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);		--data window in sample freq samples
		SIGNAL_COINC_WINDOW  	: in STD_LOGIC;
		
		RX_DATA_VALID			: out STD_LOGIC;
		RX_DATA_WINDOW_ON		: out STD_LOGIC;
		
		-- Tx Outputs
		HALF_CYCLE_FREQ	: in STD_LOGIC_VECTOR(REGISTER_W30-1 downto 0);
		NO_HALF_CYCLES	: in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
		START_CYCLE		: in STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
		
		TXp		: out STD_LOGIC;
		TXn		: out STD_LOGIC              
	);
end pulser;

architecture arch_imp of pulser is
	
	---- BURST EQUENCE ----
	component burst_sequence is
		generic (
			REGISTER_W32    : INTEGER := 32;
			REGISTER_W8     : INTEGER := 8   
		);
		port (	
			-- Sync
			aclk		: in STD_LOGIC;
			aresetn		: in STD_LOGIC;

			-- Inputs
			CH_TRIG					: in STD_LOGIC;
			TX_DATA_DELAY			: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);		--delays in 100MHz samples
			TX_DELTA_DELAY			: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);		--delays in 100MHz samples
            TX_DELTA_ADD            : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);        --delays in 100MHz samples
			RX_SAMPLING_FREQUENCY  	: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  	-- 100 MHz divider for sample freq
			RX_DATA_DELAY			: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);		--delays in 100MHz samples
			RX_DATA_WINDOW			: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);		--data window in sample freq samples
			SIGNAL_COINC_WINDOW  	: in STD_LOGIC;

			-- Outputs
			CH_ACTIVE				: out STD_LOGIC;
			TX_TRIGGER				: out STD_LOGIC;
			RX_DATA_VALID			: out STD_LOGIC;
			RX_DATA_WINDOW_ON		: out STD_LOGIC
		);
	end component burst_sequence;
	
	signal tx_trigger : STD_LOGIC := '0';
	
	---- TX OUTPUTS ----
	component tx_outputs is
		generic (
			REGISTER_W30    : INTEGER := 30;
			REGISTER_W8     : INTEGER := 8;
			REGISTER_W2     : INTEGER := 2 
		);
		port (
			-- Sync
			aclk		: in STD_LOGIC;
			aresetn		: in STD_LOGIC;

			-- Inputs
			TX_ENABLE		: in STD_LOGIC;
			START_CALC		: in STD_LOGIC;
			START_TX		: in STD_LOGIC;
			HALF_CYCLE_FREQ	: in STD_LOGIC_VECTOR(REGISTER_W30-1 downto 0);
			NO_HALF_CYCLES	: in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
			START_CYCLE		: in STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);

			-- Outputs
			TX_PLUS			: out STD_LOGIC;
			TX_MINUS		: out STD_LOGIC
		);
	end component tx_outputs;
        
begin

	---- BURST EQUENCE ----
	burst_sequence_inst: burst_sequence
		generic map(
			REGISTER_W32 	=> REGISTER_W32,
			REGISTER_W8 	=> REGISTER_W8
		)
		port map(	
			-- Sync
			aclk		=> aclk,
			aresetn		=> aresetn,

			-- Inputs
			CH_TRIG					=> CH_TRIG,
			TX_DATA_DELAY			=> TX_DATA_DELAY,
			TX_DELTA_DELAY          => TX_DELTA_DELAY,
			TX_DELTA_ADD            => TX_DELTA_ADD,
			RX_SAMPLING_FREQUENCY  	=> RX_SAMPLING_FREQUENCY,
			RX_DATA_DELAY			=> RX_DATA_DELAY,
			RX_DATA_WINDOW			=> RX_DATA_WINDOW,
			SIGNAL_COINC_WINDOW     => SIGNAL_COINC_WINDOW,

			-- Outputs
			CH_ACTIVE				=> open,
			TX_TRIGGER				=> tx_trigger,
			RX_DATA_VALID			=> RX_DATA_VALID,
			RX_DATA_WINDOW_ON		=> RX_DATA_WINDOW_ON 
		);
		
	---- TX OUTPUTS ----
	tx_outputs_inst: tx_outputs
		generic map(
			REGISTER_W30 	=> REGISTER_W30,
			REGISTER_W8 	=> REGISTER_W8,
			REGISTER_W2 	=> REGISTER_W2
		)
		port map(
			-- Sync
			aclk		=> aclk,
			aresetn		=> aresetn,

			-- Inputs
			TX_ENABLE		=> '1',
			START_CALC		=> '1',
			START_TX		=> tx_trigger,
			HALF_CYCLE_FREQ	=> HALF_CYCLE_FREQ,
			NO_HALF_CYCLES	=> NO_HALF_CYCLES,
			START_CYCLE		=> START_CYCLE,

			-- Outputs
			TX_PLUS			=> TXp,
			TX_MINUS		=> TXn
		);
																					   					     					      									 	   			                 
end arch_imp;
