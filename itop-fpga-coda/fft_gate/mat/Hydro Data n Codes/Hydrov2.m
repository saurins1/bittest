%% Cleaning

clear all
close all
clc
%% Define Data

   path='.\L-BUT\'; 
   TestSample ={'Sample1OK', 'Sample2OK', 'Sample3OK', 'Sample32NOK', 'Sample34NOK', 'Sample36NOK'}; 
   fs=100;
   freq=2; %MHz
   pbh_gate_start= 6;
   pbh_gate_lenght= 17;
   echo1windowStart=pbh_gate_start*fs;
   echo1windowEnd=(pbh_gate_start+pbh_gate_lenght)*fs;
  

%%




for ii=1:6
   inputFile=[ path TestSample{ii} '.csv'];
   if exist(inputFile)
    display([inputFile ' loaded' ])
    RawData1=importdata(inputFile);
   end
end

%Eliminate first AVG
RawData1(1:32,:)=[];
RawGated1=RawData1(:,echo1windowStart:echo1windowEnd);
RawGated1=RawGated1-mean(mean(RawGated1));

maxAScan=max(max(RawGated1));
RawGated1=100*RawGated1/maxAScan;

[numAcq, scanLength]=size(RawGated1);

figure
  plot(RawGated1(:,:)')
  title([' Raw Data fs: ' num2str(fs) 'MHz' ]);
  xlabel('us')    
  grid
%%
[AmplitudeStrip,PosMaxStrip]=max(RawGated1');

figure
   plot(AmplitudeStrip);
   grid
   
%%





























%%RawGated1
RawData1=RawData1-max(max(RawData1(:,echo1windowStart:echo1windowEnd)'));


[numAcq, scanLength]=size(RawData1);
axisxx=linspace(1,scanLength,scanLength);
axisxxUs=axisxx/fs;

figure
   %subplot(2,1,1)
   %plot(RawData');
   plot(axisxxUs(echo1windowStart:echo1windowEnd),RawData1(:,echo1windowStart:echo1windowEnd)');
   title([' Raw Data fs: ' num2str(fs) 'MHz' ]);
   xlabel('us')    
   grid
%% Amplitude strip

[AmplitudeStrip,PosMaxStrip]=max(RawData1(:,echo1windowStart:echo1windowEnd)');

figure
   plot(AmplitudeStrip);
   grid
   
   
   
   
   
   
   
%%
emptycounter=1;
empty=0;
for ii=start_index:end_index
   inputFile=[ path num2str(ii,'%08d') '.txt'];
   if exist (inputFile)
      display([inputFile ' loaded' ])
      RawData1=importdata(inputFile);
      
      %figure;plot(aux');
      
      row_size=length(RawData1)/column_size; %number of AScans on the dataset
      AScans = zeros(row_size,column_size);

      for jj=1:row_size
       AScans(jj,:)= RawData1(1:column_size);
       RawData1(1:column_size)=[];
      end
      
      %figure;plot(AScans');
      
       SampleData(ii-start_index+1,:)= mean(AScans,1);
       SampleDataGated(ii-start_index+1,:)=SampleData(ii-start_index+1,gateStart:gateEnd);
%        figure;
%        subplot(2,1,1)
%           plot(SampleData(ii,:));
%        subplot(2,1,2)
%           plot(SampleDataGated(ii,:));
   else
      display([inputFile ' does not exist' ])
      empty(emptycounter)=ii-start_index+1;
      emptycounter=emptycounter+1;
   end
end

if  (empty)
    SampleData(empty,:)=[];
    SampleDataGated(empty,:)=[];
end

       figure;
       subplot(2,1,1)
          plot(SampleData');
          grid
       subplot(2,1,2)
          plot(SampleDataGated');
          grid
          
MaxDataGated=max(max(max(SampleDataGated,1)));

%% RF data

%Display Gated info separately

figure
for ii = 1:NumSamples/2        
    subplot(NumSamples/2,2,2*ii-1)
        plot(SampleDataGated(2*ii-1,:)','color',[(((2*ii-1)-1)/(NumSamples-1)) 0 (1-(((2*ii-1)-1)/(NumSamples-1)))]);
            title(SampleLabels(2*ii-1));
            grid
            axis([1 (gateEnd-gateStart+1) -1.1*MaxDataGated 1.1*MaxDataGated])
            
        subplot(NumSamples/2,2,2*ii)
            plot(SampleDataGated(2*ii,:)','color',[((2*ii-1)/(NumSamples-1)) 0 (1-((2*ii-1)/(NumSamples-1)))]);
            title([ SampleLabels(2*ii)]);
            grid
            axis([1 (gateEnd-gateStart+1) -1.1*MaxDataGated 1.1*MaxDataGated])
end     
suptitle('Gated RF data')
% figure
% for ii = 1:NumSamples   
%       
%     subplot(NumSamples,1,ii)
%         plot(SampleDataGated(ii,:)','color',[((ii-1)/(NumSamples-1)) 0 (1-((ii-1)/(NumSamples-1)))]);
%             title(['Sample:  ' num2str(ii)])
%             grid
%             axis([1 (gateEnd-gateStart+1) -1.1*MaxDataGated 1.1*MaxDataGated])
% 
% end

%1. MaxAmplitude
[maxSample, posSample]=max(SampleDataGated,[],2);

figure
  plot(maxSample)
  grid
  title([TestSample ': RF Max. Amplitude'  ])
%   strValues = strtrim(cellstr(num2str([maxSample(:)],'%.0f')));
%   text(1:NumSamples,maxSample,strValues,'VerticalAlignment','bottom');
  xlabel('Sample')
  ylabel('Amplitude')
  set(gca, 'xticklabel', SampleLabels)
  
  
%2. Position of the maximum
figure
  plot(posSample)
  grid
  title([TestSample ': RF Position of Max.'  ])
%   strValues = strtrim(cellstr(num2str([posSample(:)],'%.0f')));
%   text(1:NumSamples,posSample,strValues,'VerticalAlignment','bottom');
  xlabel('Sample')
  ylabel('Pos')
  set(gca, 'xticklabel', SampleLabels)
 

%Setting the sourronding of the reference peak 
Reference=SampleDataGated(1,:);
[maxRef,maxRefPos]= max(Reference);
period=round(fs/freq);
surronding= round(0.5*period); % 50% of the period
pos_start =maxRefPos-surronding;
pos_end =maxRefPos+surronding;


 for ii = 1:NumSamples
     %Phase delay
     [maxZeroPhase(ii),ZeroPhase(ii)]= max(SampleDataGated(ii,pos_start:pos_end));
     PhaseDelayRF(ii)=ZeroPhase(ii)+pos_start-1-maxRefPos;
     PhaseDelayRF(ii)= PhaseDelayRF(ii)*360/period;
     
     %Group Velocity delay
     GroupVelocityDelayRF(ii)=sum(SampleDataGated(ii,:).*(gateStart:gateEnd))/sum(SampleDataGated(ii,:));
     
 end
 
 %3. RF phase delay
 figure
  plot(PhaseDelayRF)
  grid
  title([ TestSample ': Phase Delay (RF method)'  ])
  xlabel('Sample')
  xlabel('Angle [�]')
  ylim([-180 180])
  set(gca, 'xticklabel', SampleLabels) 
 
%3. RF Group Velocity Delay
  figure
      plot(GroupVelocityDelayRF/fs)
      grid
      title([ TestSample ': Group Velocity delay (RF method)'  ])
      xlabel('Sample')
      ylabel('Time [us]')
      set(gca, 'xticklabel', SampleLabels) 

      
%4. RF Skewness

DataSkewRF=skewness(SampleDataGated,[],2);

figure
  plot(DataSkewRF)
  grid
  title([TestSample ': Skewness RF'  ])
  xlabel('Sample')
  set(gca, 'xticklabel', SampleLabels)
  
  
%5. RF Kurtosis

DataKurtRF=kurtosis(SampleDataGated,[],2);

figure
  plot(DataKurtRF)
  grid
  title([TestSample ': Kurtosis in RF'  ])
  xlabel('Sample')
  set(gca, 'xticklabel', SampleLabels)

%% ACF
  
for ii = 1:NumSamples
    ACF(ii,:)=xcorr(SampleDataGated(ii,:),SampleDataGated(ii,:));
end

MaxDataACF=max(max(max(ACF,1)));

figure
for ii = 1:NumSamples/2        
    subplot(NumSamples/2,2,2*ii-1)
        plot(ACF(2*ii-1,:)','color',[(((2*ii-1)-1)/(NumSamples-1)) 0 (1-(((2*ii-1)-1)/(NumSamples-1)))]);
            title([ SampleLabels(2*ii-1) ]);
            grid
            axis([1 2*(gateEnd-gateStart+1)-1 -1.1*MaxDataACF 1.1*MaxDataACF])
            
        subplot(NumSamples/2,2,2*ii)
            plot(ACF(2*ii,:)','color',[((2*ii-1)/(NumSamples-1)) 0 (1-((2*ii-1)/(NumSamples-1)))]);
            title([SampleLabels(2*ii) ]);
            grid
            axis([1 2*(gateEnd-gateStart+1)-1 -1.1*MaxDataACF 1.1*MaxDataACF])
end
suptitle('ACF')

%1. Energy 
[Energy, ~]=max(ACF,[],2);
Energy=Energy/(gateEnd-gateStart+1);
%Energy=20*log10(Energy);

figure
  plot(Energy)
  grid
  title([TestSample ': Energy'  ])
  xlabel('Sample')
  ylabel('Energy')
  set(gca, 'xticklabel', SampleLabels)
  
%2. ACF Skewness

DataSkewACF=skewness(ACF,[],2);

figure
  plot(DataSkewACF)
  grid
  title([TestSample ': Skewness RF'  ])
  xlabel('Sample')
  set(gca, 'xticklabel', SampleLabels)
  
  
%3. ACF Kurtosis

DataKurtACF=kurtosis(ACF,[],2);

figure
  plot(DataKurtACF)
  grid
  title([TestSample ': Kurtosis in RF'  ])
  xlabel('Sample')
  set(gca, 'xticklabel', SampleLabels)

  
 ReferenceACF=ACF(1,:);

for ii = 1:NumSamples
     
    %MSE Error 
     ACFErr(ii)=immse(ACF(ii,:),ReferenceACF);
     
     %xcorr coef
     cc=corrcoef(ACF(ii,:),ReferenceACF);
     xcorrcoeffACF(ii)=cc(1,2);
     
     %Singular similarity Index Module
     ssimValueACF(ii,:)=ssim(ACF(ii,:),ReferenceACF);
     
     %dtw
     dist_dtwACF(ii) = dtw(ACF(ii,:),ReferenceACF); 
end

%MSE ERROR 
figure
  plot(ACFErr)
  grid
  title([TestSample ': MSE error on ACF'  ])
  xlabel('Sample')
  set(gca, 'xticklabel', SampleLabels)
  
%XcorrCoefficient
figure
  plot(xcorrcoeffACF)
  grid
  title([TestSample ': Cross Correlation Coeficient  ACF'  ])
  xlabel('Sample')
  set(gca, 'xticklabel', SampleLabels)

%Singular similarity Index module
figure
  plot(abs(ssimValueACF))
  grid
  title([TestSample ': SSIM ACF'  ])
  xlabel('Sample')
  set(gca, 'xticklabel', SampleLabels)

%Dynamic Time Wrapping
figure
  plot(dist_dtwACF)
  grid
  title([TestSample ': Dynamic Time Wrapping ACF'  ])
  xlabel('Sample')
  set(gca, 'xticklabel', SampleLabels)
  
%% Frequential Content
 
N=length(SampleDataGated(1,:))*5;
detailinic=floor((freq*0.5)*N/fs);
detailfin=floor((freq*2)*N/fs);
 
for ii = 1:NumSamples
    
    x1=SampleDataGated(ii,:);
    axisfft=((linspace(1,N,N)/N)*fs);
    densEsp = fft(x1,N);
    pot = densEsp.* conj(densEsp);
    potencia(ii,:)=pot;
    [ampFFT(ii),indexFFT(ii)]=max(potencia(ii,1:floor(N/2)));
    %[ampFreqPeak,FreqPeak]= findpeaks(-diffprofile,'SortStr','descend','NPeaks',2);
end

MaxampFFT=max(max(max(ampFFT,1)));



figure
for ii = 1:NumSamples/2        
    subplot(NumSamples/2,2,2*ii-1)
        plot(axisfft(detailinic:detailfin),potencia(2*ii-1,detailinic:detailfin)','color',[(((2*ii-1)-1)/(NumSamples-1)) 0 (1-(((2*ii-1)-1)/(NumSamples-1)))]);
            title([ SampleLabels(2*ii-1)]);
            grid
            axis([1 axisfft(detailfin-detailinic+1) 0 MaxampFFT])
            xlabel('MHz')
            
        subplot(NumSamples/2,2,2*ii)
            plot(axisfft(detailinic:detailfin),potencia(2*ii,detailinic:detailfin)','color',[((2*ii-1)/(NumSamples-1)) 0 (1-((2*ii-1)/(NumSamples-1)))]);
            title([ SampleLabels(2*ii)]);
            grid
            axis([1 axisfft(detailfin-detailinic+1) 0 MaxampFFT])
            xlabel('MHz')         
end  
suptitle('Spectral Density')

figure
  plot(axisfft(indexFFT))
  grid
  title([TestSample ': Freq Max'  ])
  xlabel('Sample')
  ylabel('MHz')
  set(gca, 'xticklabel', SampleLabels)
  

FreqReference=potencia(1,:);


for ii = 1:NumSamples

    FreqErr(ii)=immse(potencia(ii,:),FreqReference);

end 

figure
  plot(FreqErr)
  grid
  title([TestSample ': Freq MSE'  ])
  xlabel('Sample')
  set(gca, 'xticklabel', SampleLabels)


  
%% CrossCorrelation

Reference=SampleDataGated(1,:);

for ii = 1:NumSamples

     XcorrSamples(ii,:)=xcorr(SampleDataGated(ii,:),Reference,'coeff');
     %Group Velocity delay
     GroupVelocityDelayXcorr(ii)=sum(XcorrSamples(ii,:).*(1:length(XcorrSamples(1,:))))/sum(XcorrSamples(ii,:));
    
     %xcorr coef
     cc=corrcoef(SampleDataGated(ii,:),Reference);
     xcorrcoeff(ii)=cc(1,2);
     
     %Singular similarity Index Module
     ssimValue(ii,:)=ssim(SampleDataGated(ii,:),Reference);
     
     %dtw
     dist_dtw(ii) = dtw(SampleDataGated(ii,:),Reference); 
end

MaxDataXcorr=max(max(max(XcorrSamples,1)));

figure
for ii = 1:NumSamples/2        
    subplot(NumSamples/2,2,2*ii-1)
        plot(XcorrSamples(2*ii-1,:)','color',[(((2*ii-1)-1)/(NumSamples-1)) 0 (1-(((2*ii-1)-1)/(NumSamples-1)))]);
            title([ SampleLabels(2*ii-1) ]);
            grid
            axis([1 2*(gateEnd-gateStart+1)-1 -1.1*MaxDataXcorr 1.1*MaxDataXcorr])
            
        subplot(NumSamples/2,2,2*ii)
            plot(XcorrSamples(2*ii,:)','color',[((2*ii-1)/(NumSamples-1)) 0 (1-((2*ii-1)/(NumSamples-1)))]);
            title([ SampleLabels(2*ii) ]);
            grid
            axis([1 2*(gateEnd-gateStart+1)-1 -1.1*MaxDataXcorr 1.1*MaxDataXcorr])
end
suptitle('Cross Correlation')


%1.Max Xcorr
[maxSampleXcorr, posSampleXcorr]=max(XcorrSamples,[],2);

lengthXcorrhalf=length(Reference);
posSampleXcorr=(posSampleXcorr-lengthXcorrhalf)*360/period;
figure
  plot(maxSampleXcorr)
  grid
  title([TestSample ': Cross Correlation Amplitude'  ])
  xlabel('Sample')
  ylabel('Amplitude Xcorr')
  set(gca, 'xticklabel', SampleLabels)
  
 %4. Position of the Xcorr maximum
figure
  plot(posSampleXcorr)
  grid
  title([TestSample ': Phase Delay (Xcorr)'  ])
  xlabel('Sample')
  ylabel('Angle [�]')
  set(gca, 'xticklabel', SampleLabels)
  
%XcorrCoefficient
figure
  plot(abs(xcorrcoeff))
  grid
  title([TestSample ': Cross Correlation Coeficient'  ])
  xlabel('Sample')
  set(gca, 'xticklabel', SampleLabels)

%Singular similarity Index module
figure
  plot(abs(ssimValue))
  grid
  title([TestSample ': SSIM'  ])
  xlabel('Sample')
  set(gca, 'xticklabel', SampleLabels)

%Dynamic Time Wrapping
figure
  plot(dist_dtw)
  grid
  title([TestSample ': Dynamic Time Wrapping'  ])
  xlabel('Sample')
  set(gca, 'xticklabel', SampleLabels)
  


% Group velocity delay
figure
   plot(GroupVelocityDelayXcorr/fs)
   grid
   title([ TestSample ': Group Velocity Delay (Xcorr method)'  ])
   xlabel('Sample')
   ylabel('Time [us]')
   set(gca, 'xticklabel', SampleLabels) 