
%% Cleaning

clear all
close all
clc
%% Define Data

   %path='.\S-BUT\'; freq=6;%MHz
   path='.\L-BUT\'; freq=2;%MHz
   %path='.\S-SPIRAL\'; freq=4.5;%MHz %The worst
   TestSample ={'Sample1OK', 'Sample2OK', 'Sample3OK', 'Sample32NOK', 'Sample34NOK', 'Sample36NOK'};
   sampleLength=600;
   Colors = [0 0 1; 0/255 139/255 139/255; 173/255 216/255 230/255; 1 0 0; 178/255 34/255 34/255; 255/255 192/255 203/255  ] ;
   fs=100;
   pbh_gate_start= 6;
   pbh_gate_lenght= 17;
   echo1windowStart=pbh_gate_start*fs;
   echo1windowEnd=(pbh_gate_start+pbh_gate_lenght)*fs;
  

%% Loading RF data

numSamples = 6;
maxAScan=zeros(1,numSamples);
maxAScan1=zeros(1,numSamples);
numAcq=zeros(1,numSamples);
scanLength=zeros(1,numSamples);

RawData = cell(numSamples,1);
RawGated= cell(numSamples,1);

AmplitudeStrip = cell(numSamples,1);
TOFStrip= cell(numSamples,1);

for ii=1:numSamples
   inputFile=[ path TestSample{ii} '.csv'];
   inputTof=[ path TestSample{ii} 'TOF.csv'];
   if exist(inputFile)
     display([inputFile ' loaded' ])
     RawData{ii}=importdata(inputFile);
     %Eliminate first AVG
     RawData{ii}(1:32,:)=[];
     RawData{ii}(1001:end,:)=[];
     
     RawGated{ii}=RawData{ii}(:,echo1windowStart:echo1windowEnd);
     RawGated{ii}=RawGated{ii}-mean(mean(RawGated{ii}));
     RawData{ii}=RawData{ii}-mean(mean(RawData{ii}));
     [numAcq(ii), scanLength(ii)]=size(RawGated{ii});
     [numAcq1(ii), scanLength1(ii)]=size(RawData{ii});
     maxAScan(ii)=max(max(RawGated{ii}));
     maxAScan1(ii)=max(max(RawData{ii}));
   end
   if exist(inputTof)
       display([inputTof ' loaded' ])
       TOFStrip{ii}=importdata(inputTof);
       %Eliminate first AVG
       TOFStrip{ii}(1:32)=[];
       TOFStrip{ii}(1001:end)=[];
   end
end

maxTotalAScan=max(max(maxAScan));
axisxx=linspace(1,scanLength(1),scanLength(1));
axisxxUs=axisxx/fs;
maxTotalAScan1=max(max(maxAScan1));
axisxx1=linspace(1,scanLength1(1),scanLength1(1));
axisxxUs1=axisxx1/fs;



SkewData=cell(numSamples,1);
KurtData=cell(numSamples,1);

for ii=1:numSamples
    RawGated{ii}=100*RawGated{ii}/maxTotalAScan;
    RawData{ii}=100*RawData{ii}/maxTotalAScan1;
    AmplitudeStrip{ii}=max(RawGated{ii}');
    SkewData{ii}=skewness(RawGated{ii},[],2);
    KurtData{ii}=kurtosis(RawGated{ii},[],2);
end

figure
for ii = 1:numSamples/2        
    subplot(numSamples/2,2,2*ii-1)
        plot(axisxxUs,RawGated{2*ii-1}');
            title(TestSample(2*ii-1));
            axis([axisxxUs(1) axisxxUs(end) -110 110]);
            grid
              
        subplot(numSamples/2,2,2*ii)
            plot(axisxxUs,RawGated{2*ii,:}');
            title([ TestSample(2*ii)]);
            axis([axisxxUs(1) axisxxUs(end) -110 110]);
            grid
           
end     
suptitle('Gated RF data')


figure
for ii = 1:numSamples/2        
    subplot(numSamples/2,2,2*ii-1)
        plot(axisxxUs1,RawData{2*ii-1}');
            title(TestSample(2*ii-1));
            axis([axisxxUs1(1) axisxxUs1(end) -110 110]);
            grid
              
        subplot(numSamples/2,2,2*ii)
            plot(axisxxUs1,RawData{2*ii,:}');
            title([ TestSample(2*ii)]);
            axis([axisxxUs1(1) axisxxUs1(end) -110 110]);
            grid
           
end     
suptitle('RF data')


figure
   for ii=1:numSamples
     plot(SkewData {ii},'color', Colors(ii,:));
     hold on
   end
   suptitle('Skewness ')
   grid
   %axis([1 numAcq(ii) 1 110]);
   legend (TestSample)
   ylabel('skew')
   xlabel('mm')

 figure
   for ii=1:numSamples
     plot(KurtData{ii},'color', Colors(ii,:));
     hold on
   end
   suptitle('Kurtosis ')
   grid
   %axis([1 numAcq(ii) 1 110]);
   legend (TestSample)
   ylabel('kurtosis')
   xlabel('mm')   
   
%% Getting max amplidude
figure

for ii=1:numSamples/2
  
   subplot(numSamples/2,2,2*ii-1)
      plot(AmplitudeStrip{2*ii-1}','color', Colors(2*ii-1,:));
      title(TestSample(2*ii-1));
      axis([1 numAcq(ii) 1 110]);
      grid
              
   subplot(numSamples/2,2,2*ii)
      plot(AmplitudeStrip{2*ii}','color', Colors(2*ii,:));
      title([ TestSample(2*ii)]);
      axis([1 numAcq(ii) 1 110]);
      grid
end
suptitle('Amplitude Strips')


figure
   for ii=1:numSamples
     axismm=(1:sampleLength/numAcq(ii):sampleLength+sampleLength/numAcq(ii));
     plot(axismm,AmplitudeStrip {ii},'color', Colors(ii,:));
     hold on
   end
   title('Amplitude Strips')
   grid
   axis([1 axismm(end) 1 110]);
   legend (TestSample)
   ylabel('Max amplitude ')
   xlabel('mm')
   
%% Displaying recorded TOF

if exist(inputTof)
    figure
       for ii=1:numSamples
         axismm=(1:sampleLength/numAcq(ii):sampleLength+sampleLength/numAcq(ii));
         plot(axismm,TOFStrip {ii},'color', Colors(ii,:));
         hold on
       end
       title('ToF Strips')
       grid
       limsx=get(gca,'XLim');
       set(gca,'Xlim',[1 axismm(end)]);
      % legend (TestSample)
       ylabel(['\mus'])
       xlabel('mm')
end 
   
%%   CrossCorrelation with a reference

Reference=mean(RawGated{1})/100;
%Referece=RawGated{1}(827,:)/100;
figure
plot(axisxxUs,Reference)
grid
title('Reference')

Xcorr = cell(numSamples,1);
AmplitudeXcorr = cell(numSamples,1);
PosMaxXcorr = cell(numSamples,1);

for ii=1:numSamples
    for jj=1:numAcq(ii)
       Xcorr{ii}(jj,:)=xcorr(RawGated{ii}(jj,:)/100,Reference);
    end
end


figure
for ii = 1:numSamples/2        
    subplot(numSamples/2,2,2*ii-1)
        plot(Xcorr{2*ii-1}');
            title(TestSample(2*ii-1));
            axis([1 (2*scanLength(1)-1) -110 110]);
            grid
              
        subplot(numSamples/2,2,2*ii)
            plot(Xcorr{2*ii,:}');
            title( TestSample(2*ii));
            axis([1 (2*scanLength(1)-1) -110 110]);
            grid
           
end     
suptitle('Comparison with a Reference')

SkewXcorr=cell(numSamples,1);
KurtXcorr=cell(numSamples,1);

for ii=1:numSamples
   
    [AmplitudeXcorr{ii},PosMaxXcorr{ii}]=max(Xcorr{ii}');
    SkewXcorr{ii}=skewness(Xcorr{ii},[],2);
    KurtXcorr{ii}=kurtosis(Xcorr{ii},[],2);
end



figure
   for ii=1:numSamples
     axismm=(1:sampleLength/numAcq(ii):sampleLength+sampleLength/numAcq(ii));
     plot(axismm,AmplitudeXcorr {ii},'color', Colors(ii,:));
     hold on
   end
   title('Amplitude related to Reference Strip')
   grid
   %axis([1 numAcq(ii) 1 110]);
   axis([1 axismm(end) 1 110]);
   %legend (TestSample)
   ylabel('Max amplitude')
   xlabel('mm')

 figure
   for ii=1:numSamples
     plot(SkewXcorr {ii},'color', Colors(ii,:));
     hold on
   end
   suptitle('Skewness of the Comparison')
   grid
   %axis([1 numAcq(ii) 1 110]);
   legend (TestSample)
   ylabel('skew')
   xlabel('mm')

 figure
   for ii=1:numSamples
     plot(KurtXcorr{ii},'color', Colors(ii,:));
     hold on
   end
   suptitle('Kurtosis of the Comparison')
   grid
   %axis([1 numAcq(ii) 1 110]);
   legend (TestSample)
   ylabel('kurtosis')
   xlabel('mm')   
   
   
 figure
   for ii=1:numSamples
     
     plot((PosMaxXcorr {ii}-scanLength(1)),'color', Colors(ii,:));
     hold on
   end
   suptitle('Time deviation Strips')
   grid
   %axis([1 numAcq(ii) 1 110]);
   legend (TestSample)
   ylabel('deviation')
   xlabel('mm')
   
%% Frequential Content

FreqContent = cell(numSamples,1);
ampFFT = cell(numSamples,1);
indexFFT= cell(numSamples,1);

N=scanLength(1);
detailinic=max(floor((freq-4)*N/fs),floor((0.1*N/fs)));
detailfin=floor((freq+4)*N/fs);

axisfft=((linspace(1,N,N)/N)*fs);


for ii=1:numSamples
    for jj=1:numAcq(ii)
       x1=RawGated{ii}(jj,:)/100;
       densEsp = fft(x1,N);
       pot = densEsp.* conj(densEsp);
       FreqContent{ii}(jj,:)=pot/scanLength(1);
       [ampFFT{ii}(jj),indexFFT{ii}(jj)]=max(FreqContent{ii}(jj,:));
    end 
    aux(ii)=max(ampFFT{ii});
end

MaxampFFT=max(aux)*1.1;%For visualization only

figure
for ii = 1:numSamples/2        
    subplot(numSamples/2,2,2*ii-1)
       
        plot(axisfft(detailinic:detailfin),FreqContent{2*ii-1}(:,detailinic:detailfin)');
            title([ TestSample(2*ii-1)]);
            grid
            axis([0.5 axisfft(detailfin) 0 MaxampFFT])
            xlabel('MHz')
            
        subplot(numSamples/2,2,2*ii)
            plot(axisfft(detailinic:detailfin),FreqContent{2*ii}(:,detailinic:detailfin)');
            title([ TestSample(2*ii)]);
            grid
            axis([axisfft(detailinic) axisfft(detailfin) 0 MaxampFFT])
            xlabel('MHz')         
end  
suptitle('Power Spectral Density')

%Calculating mean squared error with a reference

densEspRef = fft(Reference',N);
potRef = densEspRef.* conj(densEspRef);
FreqReference=potRef/scanLength(1);

figure
   plot(axisfft(detailinic:detailfin),FreqReference(detailinic:detailfin));
   title('Freq Reference PSD');
   grid
   axis([axisfft(detailinic) axisfft(detailfin) 0 MaxampFFT])
   xlabel('MHz')

FreqErr= cell(numSamples,1);
   
for ii=1:numSamples
    %FreqErr{ii}=zeros(numAcq(ii));
    for jj=1:numAcq(ii)
     FreqErr{ii}(jj)=immse(FreqContent{ii}(jj,:),FreqReference');
    end
end

figure
   for ii=1:numSamples
     axismm=(1:sampleLength/numAcq(ii):sampleLength+sampleLength/numAcq(ii));
     plot(axismm,FreqErr{ii},'color', Colors(ii,:));
     hold on
   end
   title('Frequential Content error')
   grid
   %axis([1 numAcq(ii) 1 110]);
   %legend (TestSample)
   ylabel('Error')
   xlabel('mm')
   limsx=get(gca,'XLim');
   set(gca,'Xlim',[1 axismm(end)]);

   
   

%%
