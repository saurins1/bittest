clear all;close all;

fname = 'ascan_2500_1.json'; 
fid = fopen(fname); 
raw = fread(fid,inf); 
str = char(raw'); 
fclose(fid); 
json_struct = jsondecode(str);
ascan_2500_1 = json_struct.ascan;

file_data = fopen('ascan_2500_1.dat','w');
fwrite(file_data,ascan_2500_1,'int64');
fclose(file_data);

fname = 'ascan_2500_2.json'; 
fid = fopen(fname); 
raw = fread(fid,inf); 
str = char(raw'); 
fclose(fid); 
json_struct = jsondecode(str);
ascan_2500_2 = json_struct.ascan;

file_data = fopen('ascan_2500_2.dat','w');
fwrite(file_data,ascan_2500_2,'int64');
fclose(file_data);

fname = 'ascan_2500_3.json'; 
fid = fopen(fname); 
raw = fread(fid,inf); 
str = char(raw'); 
fclose(fid); 
json_struct = jsondecode(str);
ascan_2500_3 = json_struct.ascan;

file_data = fopen('ascan_2500_3.dat','w');
fwrite(file_data,ascan_2500_3,'int64');
fclose(file_data);

fname = 'ascan_3500_1.json'; 
fid = fopen(fname); 
raw = fread(fid,inf); 
str = char(raw'); 
fclose(fid); 
json_struct = jsondecode(str);
ascan_3500_1 = json_struct.ascan;

file_data = fopen('ascan_3500_1.dat','w');
fwrite(file_data,ascan_3500_1,'int64');
fclose(file_data);

fname = 'ascan_3500_2.json'; 
fid = fopen(fname); 
raw = fread(fid,inf); 
str = char(raw'); 
fclose(fid); 
json_struct = jsondecode(str);
ascan_3500_2 = json_struct.ascan;

file_data = fopen('ascan_32500_2.dat','w');
fwrite(file_data,ascan_3500_2,'int64');
fclose(file_data);

fname = 'ascan_3500_3.json'; 
fid = fopen(fname); 
raw = fread(fid,inf); 
str = char(raw'); 
fclose(fid); 
json_struct = jsondecode(str);
ascan_3500_3 = json_struct.ascan;

file_data = fopen('ascan_3500_3.dat','w');
fwrite(file_data,ascan_3500_3,'int64');
fclose(file_data);

% Ascans
ascans = [ascan_2500_1'; ascan_2500_2'; ascan_2500_3'; ascan_3500_1'; ascan_3500_2'; ascan_3500_3'];

ascans_size = size(ascans);
n_ascans = ascans_size(1);
n_figure = 0;
% for i = 1:n_ascans
%     figure(i);
%     plot(ascans(i,:));
%     n_figure = i;
% end

csvwrite('coherence_ascans.csv',ascans);

% Gate configuration
reference_index = 1;
gate_index = 4;

gate_start = 500;
gate_width = 1500;

sigA = ascans(reference_index,gate_start:gate_start+gate_width-1)';
sigB = ascans(gate_index,gate_start:gate_start+gate_width-1)';

g = floor(length(sigA)/4.5);
p = ceil(log2(g));
nfft = max (256, 2^p)
noverlap = fix(0.5.*g);
w = hamming (g);
%w = 1;
U = w'*w;
%U = 1;

fs=100;

sigA = [sigA' zeros(1,length(sigA))]';
sigB = [sigB' zeros(1,length(sigB))]';

coordinates = [];

for n =1:1
    freq1 = 0:fs/nfft:fs/2;
    
    start_window = 1; 
    end_window = 333
    segA1 = sigA(start_window:end_window,:);
    segB1 = sigB(start_window:end_window,:);
    A1 = fft (segA1,nfft);%A1=A1(1:nfft/2+1);
    B1 = fft (segB1,nfft);%B1=B1(1:nfft/2+1);
    psdAA1 = (1/(g)).*(A1.*conj(A1));
    psdBB1 = (1/(g)).*abs(B1.*conj(B1));
    psdAB1 = (1/(g)).*abs(A1.*conj(B1));
    Cab1 = abs(psdAB1).^2./(psdAA1.*psdBB1);
%     figure
%     plot(psdAA1)
%     figure
%     plot(psdBB1,'r')
%     figure
%     plot(psdAB1,'g')
    segA1 = sigA(start_window:end_window,:).*w;
    segB1 = sigB(start_window:end_window,:).*w;
    psd_AA1 = (fft(segA1,nfft).*conj(fft(segA1,nfft)))/U;
    psd_BB1 = (fft(segB1,nfft).*conj(fft(segB1,nfft)))/U;
    psd_AB1 = (fft(segA1,nfft).*conj(fft(segB1,nfft)))/U;
    %Cab1 = abs(psd_AB1).^2./(psd_AA1.*psd_BB1);
    coordinate = [start_window end_window];
    coordinates = [coordinates;coordinate];
    
    start_window = 167; 
    end_window = 499;
    segA2 = sigA(start_window:end_window,:).*w;
    segB2 = sigB(start_window:end_window,:).*w;
    psd_AA2 = (fft(segA2,nfft).*conj(fft(segA2,nfft)))/U;
    psd_BB2 = (fft(segB2,nfft).*conj(fft(segB2,nfft)))/U;
    psd_AB2 = (fft(segA2,nfft).*conj(fft(segB2,nfft)))/U;
    %Cab2 = abs(psd_AB2).^2./(psd_AA2.*psd_BB2);
    coordinate = [start_window end_window];
    coordinates = [coordinates;coordinate];
    
    start_window = 333; 
    end_window = 665;
    segA3 = sigA(start_window:end_window,:).*w;
    segB3 = sigB(start_window:end_window,:).*w;
    psd_AA3 = (fft(segA3,nfft).*conj(fft(segA3,nfft)))/U;
    psd_BB3 = (fft(segB3,nfft).*conj(fft(segB3,nfft)))/U;
    psd_AB3 = (fft(segA3,nfft).*conj(fft(segB3,nfft)))/U;
    %Cab3 = abs(psd_AB3).^2./(psd_AA3.*psd_BB3);
    coordinate = [start_window end_window];
    coordinates = [coordinates;coordinate];
    
    start_window = 499; 
    end_window = 831;
    segA4 = sigA(start_window:end_window,:).*w;
    segB4 = sigB(start_window:end_window,:).*w;
    psd_AA4 = (fft(segA4,nfft).*conj(fft(segA4,nfft)))/U;
    psd_BB4 = (fft(segB4,nfft).*conj(fft(segB4,nfft)))/U;
    psd_AB4 = (fft(segA4,nfft).*conj(fft(segB4,nfft)))/U;
    %Cab4 = abs(psd_AB4).^2./(psd_AA4.*psd_BB4);
    coordinate = [start_window end_window];
    coordinates = [coordinates;coordinate];
    
    start_window = 665; 
    end_window = 997;
    segA5 = sigA(start_window:end_window,:).*w;
    segB5 = sigB(start_window:end_window,:).*w;
    psd_AA5 = (fft(segA5,nfft).*conj(fft(segA5,nfft)))/U;
    psd_BB5 = (fft(segB5,nfft).*conj(fft(segB5,nfft)))/U;
    psd_AB5 = (fft(segA5,nfft).*conj(fft(segB5,nfft)))/U;
   %Cab5 = abs(psd_AB5).^2./(psd_AA5.*psd_BB5);
    coordinate = [start_window end_window];
    coordinates = [coordinates;coordinate];
   
    start_window = 831; 
    end_window = 1163;
    segA6 = sigA(start_window:end_window,:).*w;
    segB6 = sigB(start_window:end_window,:).*w;
    psd_AA6 = (fft(segA6,nfft).*conj(fft(segA6,nfft)))/U;
    psd_BB6 = (fft(segB6,nfft).*conj(fft(segB6,nfft)))/U;
    psd_AB6 = (fft(segA6,nfft).*conj(fft(segB6,nfft)))/U;
    %Cab6 = abs(psd_AB6).^2./(psd_AA6.*psd_BB6);
    coordinate = [start_window end_window];
    coordinates = [coordinates;coordinate];
    
    start_window = 997; 
    end_window = 1329;
    segA7 = sigA(start_window:end_window,:).*w;
    segB7 = sigB(start_window:end_window,:).*w;
    psd_AA7 = (fft(segA7,nfft).*conj(fft(segA7,nfft)))/U;
    psd_BB7 = (fft(segB7,nfft).*conj(fft(segB7,nfft)))/U;
    psd_AB7 = (fft(segA7,nfft).*conj(fft(segB7,nfft)))/U;
    %Cab7 = abs(psd_AB7).^2./(psd_AA7.*psd_BB7);
    coordinate = [start_window end_window];
    coordinates = [coordinates;coordinate];
    
    start_window = 1163; 
    end_window = 1495;
    segA8 = sigA(start_window:end_window,:).*w;
    segB8 = sigB(start_window:end_window,:).*w;
    psd_AA8 = (fft(segA8,nfft).*conj(fft(segA8,nfft)))/U;
    psd_BB8 = (fft(segB8,nfft).*conj(fft(segB8,nfft)))/U;
    psd_AB8 = (fft(segA8,nfft).*conj(fft(segB8,nfft)))/U;
    %Cab8 = abs(psd_AB8).^2./(psd_AA8.*psd_BB8);
    coordinate = [start_window end_window];
    coordinates = [coordinates;coordinate];
    
    start_window = 1329; 
    end_window = 1661;
    segA9 = sigA(start_window:end_window,:).*w;
    segB9 = sigB(start_window:end_window,:).*w;
    psd_AA9 = (fft(segA9,nfft).*conj(fft(segA9,nfft)))/U;
    psd_BB9 = (fft(segB9,nfft).*conj(fft(segB9,nfft)))/U;
    psd_AB9 = (fft(segA9,nfft).*conj(fft(segB9,nfft)))/U;
    %Cab9 = abs(psd_AB9).^2./(psd_AA9.*psd_BB9);
    coordinate = [start_window end_window];
    coordinates = [coordinates;coordinate];
    
    start_window = 1495; 
    end_window = 1827;
    segA10 = sigA(start_window:end_window,:).*w;
    segB10 = sigB(start_window:end_window,:).*w;
    psd_AA10 = (fft(segA10,nfft).*conj(fft(segA10,nfft)))/U;
    psd_BB10 = (fft(segB10,nfft).*conj(fft(segB10,nfft)))/U;
    psd_AB10 = (fft(segA10,nfft).*conj(fft(segB10,nfft)))/U;
    %Cab10 = abs(psd_AB10).^2./(psd_AA10.*psd_BB10);
    coordinate = [start_window end_window];
    coordinates = [coordinates;coordinate];
    
end

% psdAA = (psd_AA1 + psd_AA2 +psd_AA3 +psd_AA4 +psd_AA5 +psd_AA6 +psd_AA7 +psd_AA8)./8;
% psdBB = (psd_BB1 + psd_BB2 +psd_BB3 +psd_BB4 +psd_BB5 +psd_BB6 +psd_BB7 +psd_BB8)./8;
% psdAB = (psd_AB1 + psd_AB2 +psd_AB3 +psd_AB4 +psd_AB5 +psd_AB6 +psd_AB7 +psd_AB8)./8;

ref_windows = [segA1';segA2';segA3';segA4';segA5';segA6';segA7';segA8';segA9';segA10'];
gate_windows = [segB1';segB2';segB3';segB4';segB5';segB6';segB7';segB8';segB9';segB10'];

psdAA = (psd_AA1 + psd_AA2 +psd_AA3 +psd_AA4 +psd_AA5 +psd_AA6 +psd_AA7 +psd_AA8 +psd_AA9 +psd_AA10)./10;
psdBB = (psd_BB1 + psd_BB2 +psd_BB3 +psd_BB4 +psd_BB5 +psd_BB6 +psd_BB7 +psd_BB8 +psd_BB9 +psd_BB10)./10;
psdAB = (psd_AB1 + psd_AB2 +psd_AB3 +psd_AB4 +psd_AB5 +psd_AB6 +psd_AB7 +psd_AB8 +psd_AB9 +psd_AB10)./10;

Cab = (abs(psdAB).^2)./(psdAA.*psdBB);
ff = psdfreqvec('npts',nfft,'Fs',fs*1E6);
select = 1:nfft/2+1;    %EVEN
Cab_scaled = Cab(select,:);
ww = ff(select);
figure
plot(ww,Cab_scaled)
title('Cab - Frequency Coherence Using FFT')




