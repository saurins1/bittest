clear all;close all;

fname = 'ascan_2500_1.json'; 
fid = fopen(fname); 
raw = fread(fid,inf); 
str = char(raw'); 
fclose(fid); 
json_struct = jsondecode(str);
ascan_2500_1 = json_struct.ascan;

file_data = fopen('ascan_2500_1.dat','w');
fwrite(file_data,ascan_2500_1,'int64');
fclose(file_data);

fname = 'ascan_2500_2.json'; 
fid = fopen(fname); 
raw = fread(fid,inf); 
str = char(raw'); 
fclose(fid); 
json_struct = jsondecode(str);
ascan_2500_2 = json_struct.ascan;

file_data = fopen('ascan_2500_2.dat','w');
fwrite(file_data,ascan_2500_2,'int64');
fclose(file_data);

fname = 'ascan_2500_3.json'; 
fid = fopen(fname); 
raw = fread(fid,inf); 
str = char(raw'); 
fclose(fid); 
json_struct = jsondecode(str);
ascan_2500_3 = json_struct.ascan;

file_data = fopen('ascan_2500_3.dat','w');
fwrite(file_data,ascan_2500_3,'int64');
fclose(file_data);

fname = 'ascan_3500_1.json'; 
fid = fopen(fname); 
raw = fread(fid,inf); 
str = char(raw'); 
fclose(fid); 
json_struct = jsondecode(str);
ascan_3500_1 = json_struct.ascan;

file_data = fopen('ascan_3500_1.dat','w');
fwrite(file_data,ascan_3500_1,'int64');
fclose(file_data);

fname = 'ascan_3500_2.json'; 
fid = fopen(fname); 
raw = fread(fid,inf); 
str = char(raw'); 
fclose(fid); 
json_struct = jsondecode(str);
ascan_3500_2 = json_struct.ascan;

file_data = fopen('ascan_32500_2.dat','w');
fwrite(file_data,ascan_3500_2,'int64');
fclose(file_data);

fname = 'ascan_3500_3.json'; 
fid = fopen(fname); 
raw = fread(fid,inf); 
str = char(raw'); 
fclose(fid); 
json_struct = jsondecode(str);
ascan_3500_3 = json_struct.ascan;

file_data = fopen('ascan_3500_3.dat','w');
fwrite(file_data,ascan_3500_3,'int64');
fclose(file_data);

% Ascans
ascans = [ascan_2500_1'; ascan_2500_2'; ascan_2500_3'; ascan_3500_1'; ascan_3500_2'; ascan_3500_3'];

ascans_size = size(ascans)
n_ascans = ascans_size(1)
n_figure = 0;
% for i = 1:n_ascans
%     figure(i);
%     plot(ascans(i,:));
%     n_figure = i;
% end

csvwrite('coherence_ascans.csv',ascans)

% Gate configuration
gate_start = 500;
gate_width = 1500;
fft_segment = floor(gate_width/4.5);
overlaping = floor(fft_segment/2);

% FFT features
fft_width = 256;
fft_nfft = 8;
if fft_segment > 256
    fft_width = 512;  
    fft_nfft = 9;
end

% Reference/Gate selection
reference_index = 1;
gate_index = 4;

reference = ascans(reference_index,gate_start:gate_start+gate_width-1);
gate = ascans(gate_index,gate_start:gate_start+gate_width-1);

n_figure = n_figure + 1;
figure(n_figure);
plot(reference);

n_figure = n_figure + 1;
figure(n_figure);
plot(gate);

fft_reference = fft(reference);
reference_psd = abs(fft_reference).^2;
fft_gate = fft(gate);
gate_psd = abs(fft_gate).^2;

n_figure = n_figure + 1;
figure(n_figure);
plot(reference_psd);

n_figure = n_figure + 1;
figure(n_figure);
plot(gate_psd);

% FFT Windowing
ref_fft = []
gate_fft = []
ref_windows = []
gate_windows = []
start_window = 1;
end_window = 1;
n_windows = 0;

while(start_window < gate_width)
    
    if((gate_width-start_window) >= fft_segment)
        end_window = start_window+fft_segment-1;
    else
        end_window = gate_width;     
    end
       
    ref_fft_win = floor(fft(reference(start_window:end_window), fft_width)); 
    ref_fft_win = ref_fft_win(1:length(ref_fft_win)/2);
    gate_fft_win = floor(fft(gate(start_window:end_window), fft_width));
    gate_fft_win = gate_fft_win(1:length(gate_fft_win)/2);
    
    ref_fft = [ref_fft;ref_fft_win];
    gate_fft = [gate_fft;gate_fft_win];
    
    ref_window = reference(start_window:end_window);
    length(ref_window)
    if(length(ref_window) < fft_width)
        ref_window = [ref_window zeros(1,fft_width-length(ref_window))];
    end
    ref_windows = [ref_windows;ref_window];
    
    gate_window = gate(start_window:end_window);
    length(gate_window)
    if(length(gate_window) < fft_width)
        gate_window = [gate_window zeros(1,fft_width-length(gate_window))];
    end
    gate_windows = [gate_windows;gate_window];    
    
    start_window = start_window + overlaping;
    n_windows = n_windows + 1;
    
%     ref_window = [reference(start_window:start_window+fft_segment-1) zeros(1,fft_width-fft_segment)];
%     ref_fft_win_2 = floor(fft(ref_window, fft_width)); 
%     gate_window = [gate(start_window:start_window+fft_segment-1) zeros(1,fft_width-fft_segment)];
%     gate_fft_win_2 = floor(fft(gate_window, fft_width));     
end

ref_fft_im = imag(ref_fft);
ref_fft_re = real(ref_fft);
ax = ref_fft_re;
bx = ref_fft_im;

gate_fft_im = imag(gate_fft);
gate_fft_re = real(gate_fft);
ay = gate_fft_re;
by = gate_fft_im;

axax = ax .* ax;
ayay = ay .* ay;
bxbx = bx .* bx;
byby = by .* by;
axay = ax .* ay;
axby = ax .* by;
bxay = bx .* ay;
bxby = bx .* by;

pxx = axax+bxbx;
pyy = ayay+byby;

pxy_re = axay+bxby;
pxy_im = bxay-axby;
apxy = pxy_re .* pxy_re;
bpxy = pxy_im .* pxy_im;
pxy = apxy + bpxy;

n_figure = n_figure + 1;
figure(n_figure);
plot(pxx');

n_figure = n_figure + 1;
figure(n_figure);
plot(pyy');

divider = pxy;
divisor = pxx.*pyy;
coherence = divider./divisor;

n_figure = n_figure + 1;
figure(n_figure);
plot(coherence');

pxx_2 = ref_fft.*conj(ref_fft);
pyy_2 = gate_fft.*conj(gate_fft);
pxy_2 = (abs(ref_fft.*conj(gate_fft))).^2;
divider_2 = pxy_2;
divisor_2 = pxx_2.*pyy_2;
coherence_2 = divider_2./divisor_2;

pxx_3 = fft_reference.*conj(fft_reference);
pyy_3 = fft_gate.*conj(fft_gate);
pxy_3 = (abs(fft_reference.*conj(fft_gate))).^2;
divider_3 = pxy_3;
divisor_3 = pxx_3.*pyy_3;
coherence_3 = divider_3./divisor_3;



