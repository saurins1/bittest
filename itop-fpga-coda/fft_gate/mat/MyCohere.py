# -*- coding: utf-8 -*-
"""
Created on Fri Jan 31 15:13:00 2020

@author: sali
"""

from scipy import signal
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from matplotlib.ticker import NullFormatter
import math

df = pd.read_excel("myfile.xlsx",header=None)
#df.as_matrix()
a1=df.values
#print("a1 size: ", a1.size)
print("Total Rows: ", a1[:,0].size)
print("Input Array: ", a1.shape)
# a1 is the data file with total 6 rows. Each row is an ascan
# First 3 rows are a-scans from good samples
# Last 3 rows are a-scans from bad samples
nRows = a1[:,0].size
count = 1
while count <= nRows:
    row = count -1
    x=a1[0,:]
    y=a1[row,:]
    print("Input Size: ", x.size)
    
    # Setting up gate of 15 us starting  7us
    xx=x[700:2200]
    yy=y[700:2200]
    print("Gate Size: ", xx.size)
    # Sampling Frequency is 100 MHz
    fs=100
    #**************Calculating Frequence Coherence using Welch Method**********
    
    # Dividing the input vector in 8 segments with 50% overlap
    # Ensure nfft is greater than segment length
    
    segment = int(xx.size/4.5)
    overlap = int(segment/2)
    win = signal.hann(segment)
    m = math.ceil(math.log2(segment))
    n = int(math.pow(2, m))
    print("Segment Size: ",segment)
    print("Overlapping: ",overlap)
    print("NFFT: ",n)
    
    f, Cxy = signal.coherence(xx, yy,fs,win,nperseg=segment,noverlap=overlap,nfft=n)
    
    plt.figure()
    #plt.semilogy(f, Cxy)
    plt.subplot(221)
    plt.plot(f, Cxy)
    plt.xlabel('frequency [Hz]')
    plt.ylabel('Coherence')
    plt.grid(True)
    plt.axis([0, 50, 0, 1.1])
    #plt.show()
    
    #****************Culculate Average Error for frequency <=10 MHz **********
    
    nn = Cxy.size
    nn5 = int(nn/5)
    print("Cxy Size: ", Cxy.size)
    print("Cxy/5 Size: ",nn5)
    array=np.array(Cxy[0:nn5])
    #print(array)
    sum = np.sum(array[0:nn5],axis=0).tolist()/(nn5)
    print("Analyzing Coherence Average Error for frequencies <= 10 MHz")
    print("Error : ","%0.2f" % (sum))
    #*************************************************************************
    # perform cross-correlation for all data points
    Nx = len(xx)
    yunbiased = yy-np.mean(yy)
    xunbiased = xx-np.mean(xx)
    xnorm = np.sum(xunbiased**2); print("xnorm :", "%0.2f" % (xnorm))
    ynorm = np.sum(yunbiased**2); #print("ynorm :", ynorm)
    xycor = np.correlate(xunbiased,yunbiased,mode='full')/xnorm
    xycorr = xycor[math.ceil((len(xycor)-1)/2):]
    print("*** Get the maximum element from Cross Corr array***")
    maxElement = np.amax(xycorr)
    print("Max element from Cross-Corr : ","%0.4f" % (maxElement))
 
    print("*** Get the indices of maximum element from Cross Corr array***")
     # Get the position indices of maximum element in numpy array
    result = np.where(xycorr == np.amax(xycorr))
    TimeLag = result[0]*(1/fs)
    print('Cross Corr Lag for Max Value in microseconds  :', TimeLag[0])
    print("*** Max Time Lag would be as per the gate width***")
    print("*** The Max Gate Width is Fixed to 20us max at 100MHz Sampling Rate***")
    plt.subplot(222)
    plt.plot(xycorr)
    plt.xlabel('Lags or Samples')
    plt.ylabel('XCORR')
    plt.grid(True)
    plt.ylim([-1,1])
    
    #*************************PSD plotted on Linear y-axis ********************
    
    f, Pxx_den = signal.welch(xx, fs,window='hann',nperseg=segment,noverlap=overlap,nfft=n)
    
    plt.subplot(223)
    plt.plot(f, Pxx_den)
    #plt.ylim([0.5e-3, 1])
    plt.xlabel('frequency [Hz]')
    plt.ylabel('PSD [V**2/Hz]')
    #plt.show()
    
    f, Pyy_den = signal.welch(yy, fs,window='hann',nperseg=segment,noverlap=overlap,nfft=n)
    plt.plot(f, Pyy_den)
    #plt.ylim([0.5e-3, 1])
    plt.xlabel('frequency [Hz]')
    plt.ylabel('PSD [V**2/Hz]')
    plt.grid(True)
    #f, Pxx_den = signal.welch(xx, fs,window='hann',nperseg=segment,noverlap=overlap,nfft=n)
    
    #*********************PSD plotted on Semilog y-axis ***********************
    plt.subplot(224)
    plt.semilogy(f, Pxx_den)
    #plt.ylim([0.5e-3, 1])
    plt.xlabel('frequency [Hz]')
    plt.ylabel('PSD Semilog Scale')
    plt.grid(True)
    #plt.show()
    
    
    plt.semilogy(f, Pyy_den)
    #plt.ylim([0.5e-3, 1])
    plt.xlabel('frequency [Hz]')
    plt.ylabel('PSD Semilog Scale')
    plt.grid(True)
    
    plt.gca().yaxis.set_minor_formatter(NullFormatter())
    plt.subplots_adjust(top=0.92, bottom=0.08, left=0.10, right=0.95, hspace=0.35,
                        wspace=0.35)
    plt.show()
    count += 1
    
