library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity fft_gate_tb is
    generic (
        REGISTER_W64        : INTEGER := 64;
        REGISTER_W48        : INTEGER := 48;
        REGISTER_W32        : INTEGER := 32;
        REGISTER_W24        : INTEGER := 24;
        REGISTER_W16        : INTEGER := 16;
        REGISTER_W8         : INTEGER := 8;
        FFT_MAX             : INTEGER := 4192;
        BRAM_ADDR_WIDTH     : INTEGER := 12;
        BRAM_DATA_WIDTH     : INTEGER := 32
    );	
end;

architecture bench of fft_gate_tb is

    component fft_gate is
        generic (
            REGISTER_W64        : INTEGER := 64;
            REGISTER_W48        : INTEGER := 48;
            REGISTER_W32        : INTEGER := 32;
            REGISTER_W24        : INTEGER := 24;
            REGISTER_W16        : INTEGER := 16;
            REGISTER_W8         : INTEGER := 8;
            FFT_MAX             : INTEGER := 4192;
            BRAM_ADDR_WIDTH     : INTEGER := 12;
            BRAM_DATA_WIDTH     : INTEGER := 32
        );
        port (
            -- Sync
            aclk         : in STD_LOGIC;
            aresetn      : in STD_LOGIC;
        
            -- Control
            FFT_TEST     : in STD_LOGIC;
            FFT_EN       : in STD_LOGIC;  
            CC_EN        : in STD_LOGIC; 
            
            FFT_WIDTH	 : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	        CO_FFT_SEGMENT : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
            CO_FFT_NFFT : in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
            
            RECEIVER_SAMPLING_FREQUENCY  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    
            FFT_MAX_VALUE  : out STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
            FFT_MAX_SAMPLE : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
            CC_MAX_VALUE  : out STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
            CC_MAX_SAMPLE : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            FFT_GATE_END  : out STD_LOGIC;  
            FFT_GATE_END_ACK  : in STD_LOGIC;  
            FFT_COHERENCE   : out STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
                
            -- Signal input
            FFT_INPUT_AXIS_TREADY  : out STD_LOGIC;
            FFT_INPUT_AXIS_TDATA   : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
            FFT_INPUT_AXIS_TLAST   : in STD_LOGIC;
            FFT_INPUT_AXIS_TVALID  : in STD_LOGIC;
            
            CC_CONFIG_ON_ACK : out STD_LOGIC;
            CC_CONFIG_ON     : in STD_LOGIC;	    
            CC_CONFIG_ADDR   : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
            CC_CONFIG_DATA   : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
            
            FFT_STATUS_AXIS_TDATA  : out STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
            FFT_STATUS_AXIS_TUSER  : out STD_LOGIC_VECTOR(REGISTER_W24-1 downto 0);
            FFT_STATUS_AXIS_TLAST  : out STD_LOGIC;
            FFT_STATUS_AXIS_TVALID : out STD_LOGIC
        );
    end component fft_gate;

  signal aclk: std_logic:= '0';
  signal aresetn: std_logic:= '0';

  signal FFT_TEST : std_logic:= '0';
  signal FFT_EN : std_logic:= '0';
  signal CC_EN  : std_logic:= '0';
  
  signal FFT_WIDTH  : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0');
  signal CO_FFT_SEGMENT  : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0');
  signal CO_FFT_NFFT  : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0):= (others => '0');
  
  signal RECEIVER_SAMPLING_FREQUENCY  : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0');
  
  signal FFT_MAX_VALUE  : STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
  signal FFT_MAX_SAMPLE : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal FFT_GATE_END        : STD_LOGIC;
  
  signal CC_MAX_VALUE  : STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
  signal CC_MAX_SAMPLE : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal FFT_GATE_END_ACK        : STD_LOGIC;
  
  signal FFT_COHERENCE   : STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
  
  signal FFT_INPUT_AXIS_TREADY  : STD_LOGIC;
  signal FFT_INPUT_AXIS_TDATA   : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0');
  signal FFT_INPUT_AXIS_TLAST   : std_logic:= '0';
  signal FFT_INPUT_AXIS_TVALID  : std_logic:= '0';
  
  signal CC_CONFIG_ON_ACK : STD_LOGIC;
  signal CC_CONFIG_ON     : std_logic:= '0';
  signal CC_CONFIG_ADDR   : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0');
  signal CC_CONFIG_DATA   : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0');
  
  signal CC_CONFIG_ADDR_first     : std_logic:= '0';
  
  signal FFT_STATUS_AXIS_TDATA  : STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
  signal FFT_STATUS_AXIS_TUSER  : STD_LOGIC_VECTOR(REGISTER_W24-1 downto 0);
  signal FFT_STATUS_AXIS_TLAST  : STD_LOGIC;
  signal FFT_STATUS_AXIS_TVALID : STD_LOGIC;
 
  constant aclk_period: time := 10 ns;

  type states_signal_sm is (idle, open_file, load_signal, load_signal_wait, close_file); 
  signal state_signal_sm : states_signal_sm; 
  signal state_cc_signal_sm : states_signal_sm; 
  
  signal start_sm: STD_LOGIC:= '0';
  signal start_cc_sm: STD_LOGIC:= '0';

  signal decimation_level: std_logic_vector(REGISTER_W32-1 downto 0):= (others => '0');
  signal load_signal_counter: unsigned(REGISTER_W16-1 downto 0):= (others => '0');
  
  type states_fftresult_sm is (idle, save_result, read_result); 
  signal state_fftresult_sm : states_fftresult_sm; 
  
  type fft_axis_tdata_array is array ( 0 to 8192 ) of std_logic_vector(REGISTER_W64-1 downto 0);
  signal fft_tdata_array : fft_axis_tdata_array;
  
  signal fft_result_size : UNSIGNED(REGISTER_W32-1 downto 0);
  signal fft_result_cnt : UNSIGNED(REGISTER_W32-1 downto 0);
  signal fft_result_id : UNSIGNED(REGISTER_W32-1 downto 0);
  
  signal FFT_RESULT_AXIS_TDATA  : STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
  signal FFT_RESULT_AXIS_TDATA_RE  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal FFT_RESULT_AXIS_TDATA_RE_i1  : SIGNED(REGISTER_W64-1 downto 0);
  signal FFT_RESULT_AXIS_TDATA_RE_i2  : SIGNED(REGISTER_W64-1 downto 0);
  signal FFT_RESULT_AXIS_TDATA_IM  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal FFT_RESULT_AXIS_TLAST  : STD_LOGIC;
  signal FFT_RESULT_AXIS_TVALID : STD_LOGIC;
  
  signal FFT_RESULT_AXIS_TDATA_MOD  : integer range 0 to 1000000000 :=0;
  
  signal gate_init : UNSIGNED(REGISTER_W32-1 downto 0);
  signal gate_width : UNSIGNED(REGISTER_W32-1 downto 0);
  
  signal reference_init : UNSIGNED(REGISTER_W32-1 downto 0);
  signal reference_width : UNSIGNED(REGISTER_W32-1 downto 0);
  
    type fft_result_array is array ( 0 to 1024 ) of std_logic_vector(31 downto 0);
    signal fft_result_re : fft_result_array;
    signal fft_result_im : fft_result_array;
    
  signal CC_MAX_VALUE_1 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal CC_MAX_VALUE_2 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

begin

    CC_MAX_VALUE_1 <= CC_MAX_VALUE(31 downto 0);
    CC_MAX_VALUE_2 <= CC_MAX_VALUE(63 downto 32);

    FFT_RESULT_AXIS_TDATA_RE_i1 <= resize(signed(FFT_RESULT_AXIS_TDATA_RE), FFT_RESULT_AXIS_TDATA_RE_i1'length);
    FFT_RESULT_AXIS_TDATA_RE_i2 <= shift_left(FFT_RESULT_AXIS_TDATA_RE_i1, 14);
    
    FFT_RESULT_AXIS_TDATA_MOD <= to_integer(unsigned(FFT_RESULT_AXIS_TDATA_RE))*to_integer(unsigned(FFT_RESULT_AXIS_TDATA_RE))+
                                 to_integer(unsigned(FFT_RESULT_AXIS_TDATA_IM))*to_integer(unsigned(FFT_RESULT_AXIS_TDATA_IM));
	
	-- aclk process 
    aclk_process :process
    begin
        aclk <= not aclk;
        wait for aclk_period/2;
        aclk <= not aclk;
        wait for aclk_period/2;
    end process;

	stimulus: process
	begin
		aresetn <= '0';
		start_cc_sm <= '0';
		start_sm <= '0';
		wait for aclk_period*10;
		aresetn <= '1';
		wait for aclk_period*10;
		start_cc_sm <= '1';
		wait for aclk_period*50000;
		start_sm <= '1';
		wait;
	end process;
	
    gate_init <= (to_unsigned(500, gate_init'length));
    gate_width <= (to_unsigned(1500, gate_width'length));
    
    reference_init <= (to_unsigned(500, gate_init'length));
    reference_width <= (to_unsigned(1500, gate_width'length));
	
    signal_input_inst: process(aclk, aresetn)  
        variable my_counter  : integer range 0 to 10000000 :=0;   
        variable my_counter_2  : integer range 0 to 10000000 :=0; 
        
        type signalDataFile is file of integer;
        file signal_data_file: signalDataFile;
        variable signal_data: integer;     
        variable signal_data_file_status: file_open_status;
    begin 
        if (aresetn = '0') then
			FFT_EN <= '0';
			FFT_TEST <= '0';
			
			FFT_WIDTH <= std_logic_vector(to_unsigned(0, FFT_WIDTH'length));	
			CO_FFT_SEGMENT <= std_logic_vector(to_unsigned(0, CO_FFT_SEGMENT'length));	
			CO_FFT_NFFT <= std_logic_vector(to_unsigned(0, CO_FFT_NFFT'length));	
			
			RECEIVER_SAMPLING_FREQUENCY <= std_logic_vector(to_unsigned(0, RECEIVER_SAMPLING_FREQUENCY'length));	
			
			FFT_INPUT_AXIS_TDATA <= std_logic_vector(to_unsigned(0, FFT_INPUT_AXIS_TDATA'length));	
			FFT_INPUT_AXIS_TLAST <= '0';
			FFT_INPUT_AXIS_TVALID <= '0';
																								  														  
			-- state																		  	
            state_signal_sm <= idle;  
        elsif (aclk'event and aclk = '1') then
            case state_signal_sm is  
                when idle =>     
                    --FFT_TEST <= '1';
			        FFT_EN <= '1';   
                    --FFT_WIDTH <= std_logic_vector(to_unsigned(1024, FFT_WIDTH'length));	
                    FFT_WIDTH <= std_logic_vector(to_unsigned(1500, FFT_WIDTH'length));	
                    --FFT_WIDTH <= std_logic_vector(to_unsigned(2048, FFT_WIDTH'length));	
                    CO_FFT_SEGMENT <= std_logic_vector(to_unsigned(333, CO_FFT_SEGMENT'length));
                    CO_FFT_NFFT <= std_logic_vector(to_unsigned(8, CO_FFT_NFFT'length));
                    RECEIVER_SAMPLING_FREQUENCY <= std_logic_vector(to_unsigned(1, RECEIVER_SAMPLING_FREQUENCY'length));	
                    if(start_sm = '1') then
                        my_counter := 0;
                        my_counter_2 := 0;
                        load_signal_counter <= (others => '0');	
                        state_signal_sm <= open_file;        
                    end if; 
                when open_file => 
                    --file_open(signal_data_file_status, signal_data_file, "wave2.dat", read_mode);
                    file_open(signal_data_file_status, signal_data_file, "ascan_3500_1.dat", read_mode);
                    my_counter := 0; 
                    my_counter_2 := 0;
                    state_signal_sm <= load_signal;      
                when load_signal =>
                    if(FFT_INPUT_AXIS_TREADY = '1') then
                        -- tdata
                        if not endfile(signal_data_file) then
                            read (signal_data_file, signal_data);
                            FFT_INPUT_AXIS_TDATA<=std_logic_vector(to_signed(signal_data, FFT_INPUT_AXIS_TDATA'length));
                        end if;
                        -- tvalid       
                        if(my_counter >= reference_init-1) then                                                                                                 
                            FFT_INPUT_AXIS_TVALID <= '1';   
                        end if; 
                        if not endfile(signal_data_file) then
                            read (signal_data_file, signal_data);
                        end if;    
                        -- tlast                                              
                        if(my_counter_2 >= unsigned(FFT_WIDTH)-1) then
                            FFT_INPUT_AXIS_TLAST <= '1';
                            my_counter := 0;        
                            my_counter_2 := 0;                                     
                            state_signal_sm <= load_signal_wait;                                              
                        else
                            -- counter                                           
                            my_counter := my_counter + 1; 
                            if( my_counter >= reference_init) then
                                my_counter_2 := my_counter_2 + 1;  
                            end if;                                                           
                        end if;  
                    else
                       FFT_INPUT_AXIS_TVALID <= '0';
                       FFT_INPUT_AXIS_TLAST <= '0';
                    end if;    																	
				when load_signal_wait =>
					FFT_INPUT_AXIS_TDATA <= (others => '0');
                    FFT_INPUT_AXIS_TLAST <= '0';
                    FFT_INPUT_AXIS_TVALID <= '0';                                                      
                    if(my_counter >= 16) then
                        my_counter := 0; 
                        state_signal_sm <= close_file;                                                    
                    else
                        my_counter := my_counter + 1;                                          
                    end if;    																															
                when close_file =>
                    if(my_counter >= 100000) then                                               
                        file_close(signal_data_file);
                        state_signal_sm <= idle;                                                   
                    else
                        my_counter := my_counter + 1;
                    end if;     
                when others =>
                    null;                   
            end case;                                                                                            
        end if;
    end process;
    
    ref_signal_input_inst: process(aclk, aresetn)  
        variable my_counter  : integer range 0 to 10000000 :=0;   
        variable my_counter_2  : integer range 0 to 10000000 :=0; 
        
        type signalDataFile is file of integer;
        file signal_data_file: signalDataFile;
        variable signal_data: integer;     
        variable signal_data_file_status: file_open_status;
    begin 
        if (aresetn = '0') then
            CC_EN <= '0';
            
            CC_CONFIG_ADDR <= std_logic_vector(to_unsigned(0, CC_CONFIG_ADDR'length));    
            CC_CONFIG_DATA <= std_logic_vector(to_unsigned(0, CC_CONFIG_DATA'length));    
            CC_CONFIG_ON <= '0';        
            
            CC_CONFIG_ADDR_first <= '1';        
                                                                                                                                                    
            -- state                                                                              
            state_cc_signal_sm <= idle;  
        elsif (aclk'event and aclk = '1') then
            case state_cc_signal_sm is  
                when idle =>     
                    CC_EN <= '1';    
                    CC_CONFIG_ON <= '0';  
                    CC_CONFIG_ADDR <= std_logic_vector(to_unsigned(0, CC_CONFIG_ADDR'length)); 
                    CC_CONFIG_ADDR_first <= '1';  
                    if(start_cc_sm = '1') then
                        my_counter := 0;
                        my_counter_2 := 0;
                        load_signal_counter <= (others => '0');    
                        state_cc_signal_sm <= open_file;        
                    end if; 
                when open_file => 
                    --file_open(signal_data_file_status, signal_data_file, "wave1.dat", read_mode);
                    file_open(signal_data_file_status, signal_data_file, "ascan_2500_1.dat", read_mode);
                    my_counter := 0; 
                    my_counter_2 := 0;
                    state_cc_signal_sm <= load_signal;      
                when load_signal =>
                    CC_CONFIG_ON <= '1'; 
                    if(CC_CONFIG_ON_ACK = '1') then
                        -- tdata
                        if not endfile(signal_data_file) then
                            read (signal_data_file, signal_data);
                            CC_CONFIG_DATA<=std_logic_vector(to_signed(signal_data, CC_CONFIG_DATA'length));
                        end if;  
                        if not endfile(signal_data_file) then
                            read (signal_data_file, signal_data);
                        end if;    
                        CC_CONFIG_ADDR <= std_logic_vector(to_unsigned(my_counter_2, CC_CONFIG_ADDR'length));   
                        -- tlast                                              
                        if(my_counter_2 >= unsigned(FFT_WIDTH)-1) then
                            my_counter := 0;  
                            my_counter_2 := 0;                                          
                            state_cc_signal_sm <= load_signal_wait;                                              
                        else
                            -- counter                                          
                            my_counter := my_counter + 1;    
                            if( my_counter >= reference_init) then
                                my_counter_2 := my_counter_2 + 1;  
                            end if;                                     
                        end if; 
--                        if(CC_CONFIG_ADDR_first = '1') then
--                            CC_CONFIG_ADDR_first <= '0'; 
--                            CC_CONFIG_ADDR <= std_logic_vector(to_unsigned(0, CC_CONFIG_ADDR'length));   
--                        else
--                            CC_CONFIG_ADDR <= std_logic_vector(unsigned(CC_CONFIG_ADDR)+1); 
--                        end if;
                    end if;                                                                        
                when load_signal_wait =>                                                  
                    if(my_counter >= 16) then
                        my_counter := 0; 
                        state_cc_signal_sm <= close_file;                                                    
                    else
                        my_counter := my_counter + 1;                                          
                    end if;                                                                                                                                
                when close_file =>
                    CC_CONFIG_ON <= '0'; 
                    if(my_counter >= 100000) then                                               
                        file_close(signal_data_file);
                        state_cc_signal_sm <= idle;                                                   
                    else
                        my_counter := my_counter + 1;
                    end if;     
                when others =>
                    null;                   
            end case;                                                                                            
        end if;
    end process;
    
    FFT_RESULT_AXIS_TDATA_RE <= FFT_RESULT_AXIS_TDATA(31 downto 0);
    FFT_RESULT_AXIS_TDATA_IM <= FFT_RESULT_AXIS_TDATA(63 downto 32);
    
    fft_data_inst: process(aclk, aresetn)  
        variable my_counter  : integer range 0 to 10000000 :=0; 
    begin 
        if (aresetn = '0') then
            fft_tdata_array <= (others => (others => '0'));  
            fft_result_size <= (others => '0');   
            fft_result_cnt <= (others => '0');   
            FFT_RESULT_AXIS_TDATA <= (others => '0');                 
            FFT_RESULT_AXIS_TLAST <= '0';
            FFT_RESULT_AXIS_TVALID <= '0';   
            fft_result_id <= (others => '0');  
            fft_result_re <= (others => (others => '0'));             
            fft_result_im <= (others => (others => '0'));                                            
            state_fftresult_sm <= idle;  
        elsif (aclk'event and aclk = '1') then
            if(unsigned(FFT_STATUS_AXIS_TUSER) < 1024) then
                fft_result_re(to_integer(unsigned(FFT_STATUS_AXIS_TUSER))) <= FFT_STATUS_AXIS_TDATA(31 downto 0);
                fft_result_im(to_integer(unsigned(FFT_STATUS_AXIS_TUSER))) <= FFT_STATUS_AXIS_TDATA(63 downto 32);
            end if;
            case state_fftresult_sm is  
                when idle =>       
                    FFT_RESULT_AXIS_TDATA <= (others => '0');                      
                    FFT_RESULT_AXIS_TLAST <= '0';
                    FFT_RESULT_AXIS_TVALID <= '0'; 
                    fft_result_cnt <= (others => '0');              
                    my_counter := 0;   
                    --if(fft_result_id              
                    if(FFT_STATUS_AXIS_TVALID = '1') then
                        fft_tdata_array(to_integer(unsigned(FFT_STATUS_AXIS_TUSER))) <= FFT_STATUS_AXIS_TDATA;
                        fft_result_size <= fft_result_size + 1;
                        state_fftresult_sm <= save_result; 
                    else
                        fft_result_size <= (others => '0');   
                    end if;
                when save_result => 
                    if(FFT_STATUS_AXIS_TVALID = '1') then
                        fft_result_size <= fft_result_size + 1;
                        fft_tdata_array(to_integer(unsigned(FFT_STATUS_AXIS_TUSER))) <= FFT_STATUS_AXIS_TDATA;
                        if(FFT_STATUS_AXIS_TLAST = '1') then                          
                            state_fftresult_sm <= read_result; 
                        end if;
                    end if;
                when read_result =>
                    if(fft_result_id >= 2) then
                        if(fft_result_cnt >= fft_result_size) then
                            FFT_RESULT_AXIS_TVALID <= '0'; 
                            FFT_RESULT_AXIS_TLAST <= '0';
                            fft_result_id <= fft_result_id + 1;
                            state_fftresult_sm <= idle; 
                        else
                            FFT_RESULT_AXIS_TVALID <= '1';
                            if(fft_result_cnt >= fft_result_size-1) then
                                FFT_RESULT_AXIS_TLAST <= '1';
                            end if;
                            FFT_RESULT_AXIS_TDATA <= fft_tdata_array(to_integer(fft_result_cnt));
                            fft_result_cnt <= fft_result_cnt + 1;
                        end if;
                    else
                        fft_result_id <= fft_result_id + 1;
                        state_fftresult_sm <= idle;
                    end if;
                when others =>
                    null;
            end case;
        end if;
    end process;
    
    fft_end: process(aclk, aresetn)  
    begin 
        if (aresetn = '0') then
            FFT_GATE_END_ACK <= '0';   
        elsif (aclk'event and aclk = '1') then
            FFT_GATE_END_ACK <= FFT_GATE_END; 
        end if;
    end process;
    
    
  -- Insert values for generic parameters !!
    uut: fft_gate generic map ( REGISTER_W64        => REGISTER_W64,
                                REGISTER_W32        => REGISTER_W32,
                                REGISTER_W24        => REGISTER_W24,
                                REGISTER_W16        => REGISTER_W16,
                                REGISTER_W8         => REGISTER_W8,
                                FFT_MAX             => FFT_MAX,
                                BRAM_ADDR_WIDTH     => BRAM_ADDR_WIDTH,
                                BRAM_DATA_WIDTH     => BRAM_DATA_WIDTH )
                   port map    ( aclk               => aclk,
                                aresetn             => aresetn,
                                
                                FFT_TEST            => FFT_TEST,
                                FFT_EN              => FFT_EN,
                                CC_EN               => CC_EN,
                                
                                FFT_WIDTH           => FFT_WIDTH,
                                CO_FFT_SEGMENT      => CO_FFT_SEGMENT,
                                CO_FFT_NFFT         => CO_FFT_NFFT,
                                RECEIVER_SAMPLING_FREQUENCY => RECEIVER_SAMPLING_FREQUENCY,
                                          
                                FFT_MAX_VALUE       => FFT_MAX_VALUE,
                                FFT_MAX_SAMPLE      => FFT_MAX_SAMPLE,                                         
                                CC_MAX_VALUE        => CC_MAX_VALUE,
                                CC_MAX_SAMPLE       => CC_MAX_SAMPLE, 
                                FFT_GATE_END        => FFT_GATE_END,
                                FFT_GATE_END_ACK    => FFT_GATE_END_ACK,
                                FFT_COHERENCE       => FFT_COHERENCE,
                                          
                                FFT_INPUT_AXIS_TREADY  => FFT_INPUT_AXIS_TREADY,
                                FFT_INPUT_AXIS_TDATA   => FFT_INPUT_AXIS_TDATA,
                                FFT_INPUT_AXIS_TLAST   => FFT_INPUT_AXIS_TLAST,
                                FFT_INPUT_AXIS_TVALID  => FFT_INPUT_AXIS_TVALID,
                                          
                                CC_CONFIG_ON_ACK     => CC_CONFIG_ON_ACK,
                                CC_CONFIG_ON         => CC_CONFIG_ON,
                                CC_CONFIG_ADDR       => CC_CONFIG_ADDR,
                                CC_CONFIG_DATA       => CC_CONFIG_DATA,
                                
                                FFT_STATUS_AXIS_TDATA     => FFT_STATUS_AXIS_TDATA,
                                FFT_STATUS_AXIS_TUSER     => FFT_STATUS_AXIS_TUSER,
                                FFT_STATUS_AXIS_TLAST     => FFT_STATUS_AXIS_TLAST,
                                FFT_STATUS_AXIS_TVALID    => FFT_STATUS_AXIS_TVALID                          
                               );

    																																					
end;
