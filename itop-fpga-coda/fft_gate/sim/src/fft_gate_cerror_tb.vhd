library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity fft_gate_coherence_tb is
    generic (
	    REGISTER_W64        : INTEGER := 64;
        REGISTER_W32        : INTEGER := 32;
        REGISTER_W24        : INTEGER := 24;
        REGISTER_W16        : INTEGER := 16;
        REGISTER_W8         : INTEGER := 8
    );	
end;

architecture bench of fft_gate_coherence_tb is

    COMPONENT fft_gate_coherence is
        generic (
            REGISTER_W64        : INTEGER := 64;
            REGISTER_W32        : INTEGER := 32;
            REGISTER_W24        : INTEGER := 24;
            REGISTER_W16        : INTEGER := 16;
            REGISTER_W8         : INTEGER := 8
        );
        port (
            -- Sync
            aclk         : in STD_LOGIC;
            aresetn      : in STD_LOGIC;
            
            -- Control
            COHERENCE_FFT_WIDTH : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
            COHERENCE_NFFT      : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
            COHERENCE_FFT_FRAME : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
            COHERENCE_CALC      : in STD_LOGIC;
            COHERENCE_END       : out STD_LOGIC;
            
            -- Signal input
            FFT_INPUT_AXIS_TREADY  : out STD_LOGIC;
            FFT_INPUT_AXIS_TDATA_X : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            FFT_INPUT_AXIS_TDATA_Y : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            FFT_INPUT_AXIS_TLAST   : in STD_LOGIC;
            FFT_INPUT_AXIS_TVALID  : in STD_LOGIC;
            
            -- Result
            COHERENCE              : out STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
            COHERENCE_TVALID       : out STD_LOGIC
        );
    end COMPONENT fft_gate_coherence;
    
  signal aclk: std_logic:= '0';
  signal aresetn: std_logic:= '0';

  signal COHERENCE_FFT_WIDTH  : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0');
  signal COHERENCE_NFFT  : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0');
  signal COHERENCE_FFT_FRAME  : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0');
  signal COHERENCE_CALC: std_logic:= '0';
  signal COHERENCE_END : std_logic;
  
  signal FFT_INPUT_AXIS_TREADY : std_logic;
  signal FFT_INPUT_AXIS_TDATA_X  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');
  signal FFT_INPUT_AXIS_TDATA_Y  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');
  signal FFT_INPUT_AXIS_TLAST : std_logic:= '0';
  signal FFT_INPUT_AXIS_TVALID : std_logic:= '0';
  
  signal COHERENCE : STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
  signal COHERENCE_TVALID  : STD_LOGIC;
  
  constant aclk_period: time := 10 ns;

  type states_signal_sm is (idle, open_file, load_signal, load_signal_wait, load_signal_check, close_file); 
  signal state_signal_sm : states_signal_sm; 
  
  signal start_sm: STD_LOGIC:= '0';
  signal load_signal_counter: unsigned(REGISTER_W16-1 downto 0):= (others => '0');
  
  signal ref_gate  : STD_LOGIC;
   
begin

    -- aclk process 
    aclk_process :process
    begin
        aclk <= not aclk;
        wait for aclk_period/2;
        aclk <= not aclk;
        wait for aclk_period/2;
    end process;

	stimulus: process
	begin
		aresetn <= '0';
		start_sm <= '0';
		wait for aclk_period*10;
		aresetn <= '1';
		wait for aclk_period*10;
		wait for aclk_period*50000;
		start_sm <= '1';
		wait;
	end process;
	 	
    signal_input_inst: process(aclk, aresetn)  
        variable my_counter  : integer range 0 to 10000000 :=0;   
        
        type signalDataFile is file of integer;
        file signal_data_file: signalDataFile;
        variable signal_data: integer;     
        variable signal_data_file_status: file_open_status;
    begin 
        if (aresetn = '0') then		
			COHERENCE_FFT_WIDTH <= std_logic_vector(to_unsigned(0, COHERENCE_FFT_WIDTH'length));		
			COHERENCE_NFFT <= std_logic_vector(to_unsigned(0, COHERENCE_NFFT'length));	
			COHERENCE_FFT_FRAME	<= std_logic_vector(to_unsigned(0, COHERENCE_FFT_FRAME'length));
			COHERENCE_CALC	<= '0';
			FFT_INPUT_AXIS_TDATA_X <= std_logic_vector(to_unsigned(0, FFT_INPUT_AXIS_TDATA_X'length));	
			FFT_INPUT_AXIS_TDATA_Y <= std_logic_vector(to_unsigned(0, FFT_INPUT_AXIS_TDATA_X'length));
			FFT_INPUT_AXIS_TLAST <= '0';
			FFT_INPUT_AXIS_TVALID <= '0';	
			ref_gate <= '0';																			  														  
			-- state																		  	
            state_signal_sm <= idle;  
        elsif (aclk'event and aclk = '1') then
            case state_signal_sm is  
                when idle =>     
                    COHERENCE_FFT_WIDTH <= std_logic_vector(to_unsigned(128, COHERENCE_FFT_WIDTH'length));	
                    COHERENCE_NFFT <= std_logic_vector(to_unsigned(7, COHERENCE_NFFT'length));   
                    COHERENCE_FFT_FRAME	<= std_logic_vector(to_unsigned(0, COHERENCE_FFT_FRAME'length));
                    if(start_sm = '1') then
                        my_counter := 0;
                        load_signal_counter <= (others => '0');	
                        state_signal_sm <= open_file;        
                    end if; 
                when open_file => 
                    file_open(signal_data_file_status, signal_data_file, "wave1.dat", read_mode);
                    my_counter := 0; 
                    state_signal_sm <= load_signal;      
                when load_signal =>
                    if(FFT_INPUT_AXIS_TREADY = '1') then
                        -- tdata
                        if not endfile(signal_data_file) then
                            read (signal_data_file, signal_data);
--                            FFT_INPUT_AXIS_TDATA_X <= std_logic_vector(unsigned(FFT_INPUT_AXIS_TDATA_X)+1);
--                            FFT_INPUT_AXIS_TDATA_Y <= std_logic_vector(unsigned(FFT_INPUT_AXIS_TDATA_Y)+1);
			                FFT_INPUT_AXIS_TDATA_X <= std_logic_vector(to_unsigned(1, FFT_INPUT_AXIS_TDATA_X'length));	
                            FFT_INPUT_AXIS_TDATA_Y <= std_logic_vector(to_unsigned(0, FFT_INPUT_AXIS_TDATA_X'length));
                        end if;
                        -- tvalid                                                                                                        
                        FFT_INPUT_AXIS_TVALID <= '1';    
                        if not endfile(signal_data_file) then
                            read (signal_data_file, signal_data);
                        end if;    
                        -- tlast                                              
                        if(my_counter >= unsigned(COHERENCE_FFT_WIDTH)-1) then
                            FFT_INPUT_AXIS_TLAST <= '1';
                            my_counter := 0;                                           
                            state_signal_sm <= load_signal_wait;                                              
                        else
                            -- counter                                          
                            my_counter := my_counter + 1;                                          
                        end if;  
                    else
                       FFT_INPUT_AXIS_TVALID <= '0';
                       FFT_INPUT_AXIS_TLAST <= '0';
                    end if;    																	
				when load_signal_wait =>
			        FFT_INPUT_AXIS_TDATA_X <= std_logic_vector(to_unsigned(0, FFT_INPUT_AXIS_TDATA_X'length));	
                    FFT_INPUT_AXIS_TDATA_Y <= std_logic_vector(to_unsigned(0, FFT_INPUT_AXIS_TDATA_X'length));
                    FFT_INPUT_AXIS_TLAST <= '0';
                    FFT_INPUT_AXIS_TVALID <= '0';                                                      
                    if(my_counter >= 16) then
                        my_counter := 0;
                        COHERENCE_FFT_FRAME	<= std_logic_vector(unsigned(COHERENCE_FFT_FRAME) + 1); 
                        state_signal_sm <= load_signal_check;                                                    
                    else
                        my_counter := my_counter + 1;                                          
                    end if;    
                when load_signal_check => 
                    if(ref_signal = '0') then
                    
                    else
                    
                    end if;   																															
                when close_file =>
                    if(my_counter >= 10000) then                                               
                        file_close(signal_data_file);
                        state_signal_sm <= idle;                                                   
                    else
                        my_counter := my_counter + 1;
                    end if;     
                when others =>
                    null;                   
            end case;                                                                                            
        end if;
    end process;
    
  -- Insert values for generic parameters !!
    uut: fft_gate_coherence 
                   generic map ( REGISTER_W64       => REGISTER_W64,
                                REGISTER_W32        => REGISTER_W32,
                                REGISTER_W24        => REGISTER_W24,
                                REGISTER_W16        => REGISTER_W16,
                                REGISTER_W8         => REGISTER_W8)
                   port map    ( aclk               => aclk,
                                aresetn             => aresetn,
                                
                                COHERENCE_FFT_WIDTH    => COHERENCE_FFT_WIDTH,
                                COHERENCE_NFFT         => COHERENCE_NFFT,
                                COHERENCE_FFT_FRAME    => COHERENCE_FFT_FRAME,
                                COHERENCE_CALC         => COHERENCE_CALC,
                                COHERENCE_END          => COHERENCE_END,
                                          
                                FFT_INPUT_AXIS_TREADY  => FFT_INPUT_AXIS_TREADY,
                                FFT_INPUT_AXIS_TDATA_X => FFT_INPUT_AXIS_TDATA_X,
                                FFT_INPUT_AXIS_TDATA_Y => FFT_INPUT_AXIS_TDATA_Y,
                                FFT_INPUT_AXIS_TLAST   => FFT_INPUT_AXIS_TLAST,
                                FFT_INPUT_AXIS_TVALID  => FFT_INPUT_AXIS_TVALID,
                                          
                                COHERENCE        => COHERENCE,
                                COHERENCE_TVALID => COHERENCE_TVALID                        
                               );
   																																					
end;
