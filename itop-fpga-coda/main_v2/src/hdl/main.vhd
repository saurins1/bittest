----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/05/2018 10:07:23 AM
-- Design Name: 
-- Module Name: VOLTA_main - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity main is
	generic (
		-- General
		N_CHANNELS		: INTEGER := 1;
		REGISTER_W64    : INTEGER := 64;
		REGISTER_W48    : INTEGER := 48;
	    REGISTER_W32    : INTEGER := 32;
	    REGISTER_W21    : INTEGER := 21;
        REGISTER_W16    : INTEGER := 16;
		REGISTER_W14    : INTEGER := 14;
		REGISTER_W8     : INTEGER := 8;
		REGISTER_W6     : INTEGER := 6;
		REGISTER_W4     : INTEGER := 4;
		REGISTER_W2     : INTEGER := 2;
		
		-- Scan mode
        SCAN_MODE_NORMAL  : INTEGER := 0;
        SCAN_MODE_MRUT    : INTEGER := 10;
        
		-- Trigger type
        TRIGGER_TYPE_NORMAL     : INTEGER := 0;
        TRIGGER_TYPE_SW         : INTEGER := 10;
        TRIGGER_TYPE_ENC        : INTEGER := 20;
        TRIGGER_TYPE_ROBOT      : INTEGER := 30;
        TRIGGER_TYPE_ROBOT_ENC  : INTEGER := 40;
		
		-- external trigger
		DISPOSITION_PASS  : INTEGER := 1;
		DISPOSITION_FAIL  : INTEGER := 2;
		DISPOSITION_NODET : INTEGER := 0;
		
		-- Coinc
        TRIGGER_WAIT    : INTEGER := 32;
        MIN_COINC_LEVEL : INTEGER := 2;
		
		-- Avg
        MIN_AVG_LEVEL       : INTEGER := 2;
        AVG_TYPE_NORMAL     : INTEGER := 0;
        AVG_TYPE_PIPELINE   : INTEGER := 1;
		
		-- Ascan header size
		ASCAN_HEADER_SIZE : INTEGER := 43;
		
		-- FIR CORR
        N_TAPS              : INTEGER    := 512;
        N_SETS_TAP          : INTEGER    := 3;
        OVERSAMPLING        : INTEGER    := 2;
        WAIT_CYCLES         : INTEGER    := 4
	);
port (
    -- sync
    aclk                    : in STD_LOGIC; 
    aresetn                 : in STD_LOGIC; 
    
    -- DEBUG
    CONFIGURATION_SIZE      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CONFIGURATION_ON        : out STD_LOGIC;
    MAIN_DMA_STATE          : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    
    CONFIGURATION_HEADER_SIZE      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CONFIGURATION_CHANNELS         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CONFIGURATION_CHANNEL0_OFFSET  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CONFIGURATION_ENCODER_OFFSET   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CONFIGURATION_DIGITALIO_OFFSET : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CONFIGURATION_CHANNEL0_SIZE    : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CONFIGURATION_TRIGGER_TYPE     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CONFIGURATION_CHANNEL0_ENABLED : out STD_LOGIC;
    CONFIGURATION_CHANNEL0_ON      : out STD_LOGIC;
    
    -- CONTROL REGISTERS
    DMA_UPLOAD_CONTROL      : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);   
    DMA_DOWNLOAD_CONTROL    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    START_INSPECTION_CONTROL: in STD_LOGIC; 
    SCAN_MODE_CONTROL       : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    CSCAN_MODE_CONTROL      : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
    TRIGGER_CONTROL         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    RESET_CONTROL           : in STD_LOGIC;
    ENABLE_CONTROL          : in STD_LOGIC;
    CHANNEL_CONTROL         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    -- STATUS REGISTERS
    DMA_UPLOAD_STATUS       : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);   
    DMA_DOWNLOAD_STATUS     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    START_INSPECTION_STATUS : out STD_LOGIC; 
    SCAN_MODE_STATUS        : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    CSCAN_MODE_STATUS       : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
    TRIGGER_STATUS          : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    RESET_STATUS            : out STD_LOGIC;
    ENABLE_STATUS           : out STD_LOGIC;
    CHANNEL_STATUS          : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    
    -- ENCODER
    ENCODER1_DIR     : in STD_LOGIC_VECTOR (REGISTER_W2-1 downto 0);  
    ENCODER1_COUNT   : in STD_LOGIC_VECTOR (REGISTER_W32-1 downto 0);  
    
    ENCODER2_DIR     : in STD_LOGIC_VECTOR (REGISTER_W2-1 downto 0);  
    ENCODER2_COUNT   : in STD_LOGIC_VECTOR (REGISTER_W32-1 downto 0);  
	
	-- DIO
	IO_INPUTS        : in  STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
	IO_OUTPUTS  	 : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
	
	-- AVG
	DSP_AVG_LEVEL    : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
    DSP_AVG_COUNTER  : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
    DSP_AVG_TYPE     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	
	---------------------------------------------- CH0 ----------------------------------------------
	
	CH0_CHANNEL_TRIGGER_TYPE   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	CH0_STATE_A	  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    CH0_STATE_B   : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    CH0_STATE_C   : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	
	-- Config output receiver
	CH0_RECEIVER_SAMPLING_FREQUENCY         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
    CH0_RECEIVER_DATA_WINDOW                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
    CH0_RECEIVER_DELAY                      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
    CH0_RECEIVER_ANALOG_GAIN                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    CH0_RECEIVER_DIGITAL_GAIN               : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    CH0_RECEIVER_EXTERNAL_MULTIPLEXER_EN    : out STD_LOGIC;
			
	-- Config output transmitter
	CH0_TRANSMITTER_VOLTAGE                 : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
    CH0_TRANSMITTER_BURST_FREQUENCY         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
    CH0_TRANSMITTER_N_CYCLES                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    CH0_TRANSMITTER_N_BURST                 : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    CH0_TRANSMITTER_DELAY                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CH0_TRANSMITTER_DELTA_DELAY             : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CH0_TRANSMITTER_DELTA_ADD               : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CH0_TRANSMITTER_DIRECTIONAL_PHASING     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
		
	-- Config output magnet
	CH0_MAGNET_MODE                         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    CH0_MAGNET_PULSE_WIDTH                  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    CH0_MAGNET_INITIAL_DELAY                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CH0_MAGNET_RAMP_UP_VOLTAGE              : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    CH0_MAGNET_VOLTAGE                      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);   
	
	-- Config analog filters
	CH0_DSP_ANALOG_FILTER  					: out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
	CH0_DSP_COINC_LEVEL                     : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
    CH0_DSP_AVG_LEVEL                       : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
	
	CH0_DSP_BURST_LEVEL                     : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
	CH0_DSP_BURST_COUNTER					: out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
		
	-- Config DAC curves
	CH0_CONFIG_DAC_AXIS_TREADY    : in STD_LOGIC;
	CH0_CONFIG_DAC_AXIS_TDATA	  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	CH0_CONFIG_DAC_AXIS_TLAST	  : out STD_LOGIC;
	CH0_CONFIG_DAC_AXIS_TVALID    : out STD_LOGIC;
			
	-- Signal input
	CH0_SIGNAL_COINC_WINDOW  	  : out STD_LOGIC;
	CH0_SIGNAL_AVG_WINDOW  		  : out STD_LOGIC;
		
	CH0_SIGNAL_INPUT_AXIS_TREADY  : out STD_LOGIC;
	CH0_SIGNAL_INPUT_AXIS_TDATA	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	CH0_SIGNAL_INPUT_AXIS_TLAST	  : in STD_LOGIC;
	CH0_SIGNAL_INPUT_AXIS_TVALID  : in STD_LOGIC;
		
    -- SYSTEM SIGNALS
    SW_RESET                : out STD_LOGIC;
	
    -- DMA INPUT
    DMA_S_axis_tready       : out STD_LOGIC;
    DMA_S_axis_tkeep        : in STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0);
    DMA_S_axis_tdata        : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    DMA_S_axis_tlast        : in STD_LOGIC;
    DMA_S_axis_tvalid       : in STD_LOGIC;
    -- DMA OUTPUT
    DMA_M_axis_tvalid       : out STD_LOGIC;
    DMA_M_axis_tdata        : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    DMA_M_axis_tlast        : out STD_LOGIC;
    DMA_M_axis_tkeep        : out STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0);
    DMA_M_axis_tready       : in STD_LOGIC
    
);
end main;

architecture Behavioral of main is
	
	---------------------------------------------- CHANNEL ----------------------------------------------
	COMPONENT channel IS
	generic (
		-- General
		CHANNEL_ID		: INTEGER := 0;
		REGISTER_W64    : INTEGER := 64;
		REGISTER_W48    : INTEGER := 48;
	    REGISTER_W32    : INTEGER := 32;
	    REGISTER_W21    : INTEGER := 21;
        REGISTER_W16    : INTEGER := 16;
		REGISTER_W14    : INTEGER := 14;
		REGISTER_W8     : INTEGER := 8;
		REGISTER_W4     : INTEGER := 4;
		REGISTER_W2     : INTEGER := 2;
		
		-- Scan mode
        SCAN_MODE_NORMAL  : INTEGER := 0;
        SCAN_MODE_MRUT    : INTEGER := 10;
        
		-- Trigger type
        TRIGGER_TYPE_NORMAL     : INTEGER := 0;
        TRIGGER_TYPE_SW         : INTEGER := 10;
        TRIGGER_TYPE_ENC        : INTEGER := 20;
        TRIGGER_TYPE_ROBOT      : INTEGER := 30;
        TRIGGER_TYPE_ROBOT_ENC  : INTEGER := 40;
		
		-- external trigger
		DISPOSITION_PASS  : INTEGER := 1;
		DISPOSITION_FAIL  : INTEGER := 2;
		DISPOSITION_NODET : INTEGER := 0;
		
		-- Coinc
        TRIGGER_WAIT    : INTEGER := 32;
        MIN_COINC_LEVEL : INTEGER := 2;
		
		-- Avg
        MIN_AVG_LEVEL       : INTEGER := 2;
        AVG_TYPE_NORMAL     : INTEGER := 0;
        AVG_TYPE_PIPELINE   : INTEGER := 1;
		
		-- Ascan header size
		ASCAN_HEADER_SIZE : INTEGER := 43;
		
		-- FIR CORR
        N_TAPS              : INTEGER    := 512;
        N_SETS_TAP          : INTEGER    := 3;
        OVERSAMPLING        : INTEGER    := 2;
        WAIT_CYCLES         : INTEGER    := 4
	);
	port (
	    -- Sync
	    aclk         : in STD_LOGIC;
        aresetn      : in STD_LOGIC;
		
		-- Control	
		CONFIGURE_CONTROL  	  : in STD_LOGIC;
		
		-- Start inspection
		START_INSPECTION_CONTROL  : in STD_LOGIC;
		START_INSPECTION_STATUS   : out STD_LOGIC;
		
		-- Start top inspection
        START_STOP_INSPECTION_CONTROL  : in STD_LOGIC;
        START_STOP_INSPECTION_STATUS   : out STD_LOGIC;
        DISPOSITION		  : out STD_LOGIC; 
        DISPOSITION_BITS  : out STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0); --
		
		-- Scan mode
		SCAN_MODE_CONTROL : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        SCAN_MODE_STATUS  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        
        -- Trigger type
        TRIGGER_TYPE_CONTROL : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRIGGER_TYPE_STATUS  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        
        -- Trigger sw
        TRIGGER_SW_CONTROL : in STD_LOGIC;
        TRIGGER_SW_STATUS  : out STD_LOGIC;
        
        -- Ascan
        READ_ASCAN  	  : in STD_LOGIC;
        ASCAN_READY        : out STD_LOGIC;
        ASCAN_SIZE          : out STD_LOGIC_VECTOR(REGISTER_W21-1 downto 0);
        
        -- Coinc
        READ_COINC_RESULT     : in STD_LOGIC;
        COINC_RESULT_READY  : out STD_LOGIC;
        COINC_RESULT_SIZE   : out STD_LOGIC_VECTOR(REGISTER_W21-1 downto 0);
		
		-- DEBUG
		CHANNEL_ENABLED   : out STD_LOGIC;
		CONFIG_ON         : out STD_LOGIC;
		CHANNEL_STATE_A	  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        CHANNEL_STATE_B   : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        CHANNEL_STATE_C   : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
			
		-- Signal input
		SIGNAL_COINC_WINDOW  	  : out STD_LOGIC;
        SIGNAL_COINC_COUNTER      : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
        SIGNAL_AVG_WINDOW            : out STD_LOGIC;
        SIGNAL_AVG_COUNTER        : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
		
		SIGNAL_INPUT_AXIS_TREADY  : out STD_LOGIC;
		SIGNAL_INPUT_AXIS_TDATA	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		SIGNAL_INPUT_AXIS_TLAST	  : in STD_LOGIC;
		SIGNAL_INPUT_AXIS_TVALID  : in STD_LOGIC;
		
		-- Config
		CONFIG_INPUT_LOAD	  	  : in STD_LOGIC;
		CONFIG_INPUT_AXIS_TREADY  : out STD_LOGIC;
		CONFIG_INPUT_AXIS_TDATA	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		CONFIG_INPUT_AXIS_TLAST	  : in STD_LOGIC;
		CONFIG_INPUT_AXIS_TVALID  : in STD_LOGIC;
		
		-- Avg sample
        AVG_SAMPLE_AXIS_TREADY  : out STD_LOGIC;
        AVG_SAMPLE_AXIS_TDATA   : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        AVG_SAMPLE_AXIS_TLAST   : in STD_LOGIC;
        AVG_SAMPLE_AXIS_TVALID  : in STD_LOGIC;
		
		-- ENCODERS
		ENCODER1_POS  	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		ENCODER1_DIR  	  : in STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
		ENCODER1_RPM  	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		ENCODER1_TRIGGER_COUNTS : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		ENCODER2_POS  	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		ENCODER2_DIR  	  : in STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
		ENCODER2_RPM  	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		ENCODER2_TRIGGER_COUNTS : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		ENCODER3_POS  	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		ENCODER3_DIR  	  : in STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
		ENCODER3_RPM  	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		ENCODER3_TRIGGER_COUNTS : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		-- IO
		IO_INPUTS  	  	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		IO_OUTPUTS  	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		
		-- Config output receiver
		RECEIVER_SAMPLING_FREQUENCY         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        RECEIVER_DATA_WINDOW                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        RECEIVER_DELAY                      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        RECEIVER_ANALOG_GAIN                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  --db*1024
        RECEIVER_DIGITAL_GAIN               : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  --V*1024*1024   
        RECEIVER_EXTERNAL_MULTIPLEXER_EN    : out STD_LOGIC;
		
		-- Config output transmitter
		TRANSMITTER_VOLTAGE                 : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        TRANSMITTER_BURST_FREQUENCY         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        TRANSMITTER_N_CYCLES                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_N_BURST                 : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_DELAY                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_DELTA_DELAY             : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_DELTA_ADD               : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        TRANSMITTER_DIRECTIONAL_PHASING     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        TRANSMITTER_START_CYCLE	            : out STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
		
		-- Config output magnet
		MAGNET_MODE                         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        MAGNET_PULSE_WIDTH                  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        MAGNET_INITIAL_DELAY                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        MAGNET_RAMP_UP_VOLTAGE              : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        MAGNET_VOLTAGE                      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		-- Condig analog filters
		DSP_ANALOG_FILTER  					: out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
		DSP_COINC_LEVEL                     : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
        DSP_AVG_LEVEL                       : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
        DSP_AVG_TYPE                        : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		-- Config DAC curves
		CONFIG_DAC_AXIS_TREADY    : in STD_LOGIC;
		CONFIG_DAC_AXIS_TDATA	  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		CONFIG_DAC_AXIS_TLAST	  : out STD_LOGIC;
		CONFIG_DAC_AXIS_TVALID    : out STD_LOGIC;
					
		-- ASCAN
		ASCAN_AXIS_TREADY  : in STD_LOGIC;
		ASCAN_AXIS_TDATA   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		ASCAN_AXIS_TLAST   : out STD_LOGIC;
		ASCAN_AXIS_TVALID  : out STD_LOGIC;
		ASCAN_AXIS_TKEEP   : out std_logic_vector((REGISTER_W32/8)-1 downto 0);
		
		-- COINCIDENCE RESULT
        COINC_DMA_RESULT_AXIS_TREADY  : in STD_LOGIC;
        COINC_DMA_RESULT_AXIS_TDATA   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        COINC_DMA_RESULT_AXIS_TLAST   : out STD_LOGIC;
        COINC_DMA_RESULT_AXIS_TVALID  : out STD_LOGIC;
        COINC_DMA_RESULT_AXIS_TKEEP   : out std_logic_vector((REGISTER_W32/8)-1 downto 0)
	);
	END COMPONENT channel;

	---------------------------------------------- CH0 ----------------------------------------------	
	
	-- Control
	signal CH0_START_INSPECTION_STATUS  	: STD_LOGIC;
	signal CH0_START_STOP_INSPECTION_CONTROL: STD_LOGIC;
	signal CH0_START_STOP_INSPECTION_STATUS : STD_LOGIC;
	signal CH0_DISPOSITION		  	: STD_LOGIC; 
	signal CH0_DISPOSITION_BITS  	: STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
	signal CH0_SCAN_MODE_STATUS 	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH0_TRIGGER_TYPE_STATUS 	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

	-- Ascan
	signal ch0_read_ascan  	  		: STD_LOGIC;
	signal ch0_ascan_ready 	  	    : STD_LOGIC;
	signal ch0_ascan_size	      	: STD_LOGIC_VECTOR(REGISTER_W21-1 downto 0);
	
	-- Ascan
    signal ch0_read_coinc_result    : STD_LOGIC; 
    signal ch0_coinc_result_ready   : STD_LOGIC; 
    signal ch0_coinc_result_size    : STD_LOGIC_VECTOR(REGISTER_W21-1 downto 0);
				
	-- Config
	signal ch0_config_input_load	  	  	: STD_LOGIC;
	signal ch0_config_input_axis_tready  	: STD_LOGIC;
	signal ch0_config_input_axis_tdata	  	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal ch0_config_input_axis_tlast	  	: STD_LOGIC;
	signal ch0_config_input_axis_tvalid  	: STD_LOGIC;
	
	-- Avg sample
    signal ch0_avg_sample_axis_tready       : STD_LOGIC;
    signal ch0_avg_sample_axis_tdata        : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal ch0_avg_sample_axis_tlast        : STD_LOGIC;
    signal ch0_avg_sample_axis_tvalid       : STD_LOGIC;
		
	-- ENCODERS
	signal CH0_ENCODER1_POS  	  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH0_ENCODER1_DIR  	  : STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
	signal CH0_ENCODER1_RPM  	  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH0_ENCODER1_TRIGGER_COUNTS : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
	signal CH0_ENCODER2_POS  	  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH0_ENCODER2_DIR  	  : STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
	signal CH0_ENCODER2_RPM  	  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH0_ENCODER2_TRIGGER_COUNTS : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
	signal CH0_ENCODER3_POS  	  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH0_ENCODER3_DIR  	  : STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
	signal CH0_ENCODER3_RPM  	  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH0_ENCODER3_TRIGGER_COUNTS : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
							
	-- ASCAN
	signal ch0_ascan_axis_tready  : STD_LOGIC;
	signal ch0_ascan_axis_tdata   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal ch0_ascan_axis_tlast   : STD_LOGIC;
	signal ch0_ascan_axis_tvalid  : STD_LOGIC;
	signal ch0_ascan_axis_tkeep   : STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0);
	
    -- COINCIDENCE RESULT
    signal ch0_coinc_dma_result_axis_tready  : STD_LOGIC;
    signal ch0_coinc_dma_result_axis_tdata   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal ch0_coinc_dma_result_axis_tlast   : STD_LOGIC;
    signal ch0_coinc_dma_result_axis_tvalid  : STD_LOGIC;
    signal ch0_coinc_dma_result_axis_tkeep   : STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0);
	
	
	signal COINC_LEVEL : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
    signal COINC_COUNTER : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);

    signal AVG_LEVEL : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
    signal AVG_COUNTER : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
    signal avg_type : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

	---------------------------------------------- MAIN ----------------------------------------------

	signal configure_control            : std_logic;

    signal enable_status_i              : std_logic;
    signal reset_status_i               : std_logic;
    signal start_inspection_status_i    : std_logic;
    signal scan_mode_status_i           : std_logic_vector(REGISTER_W32-1 downto 0);
    signal cscan_mode_status_i          : std_logic_vector(REGISTER_W32-1 downto 0); 
    signal trigger_status_i          	: std_logic_vector(REGISTER_W32-1 downto 0); 
    
    signal trigger_type_control         : std_logic_vector(REGISTER_W32-1 downto 0);
    signal trigger_type_status_i        : std_logic_vector(REGISTER_W32-1 downto 0); 
    
    -- SW trigger
    signal trigger_sw_status            : std_logic_vector(N_CHANNELS-1 downto 0);
    
    -- dma state machine
    type dma_states is (idle, prepare_write_configuration, write_configuration, write_configuration_ack, 
                              prepare_write_avg_sample, write_avg_sample, write_avg_sample_ack, 
                              prepare_read_configuration_1, prepare_read_configuration_2, read_configuration, read_configuration_end, 
                              prepare_read_capability_1, prepare_read_capability_2, read_capability, read_capability_end,
                              start_send_ascan, send_ascan_ch0, send_ascan_ch1, stop_send_ascan,
                              start_send_coinc_result, send_coinc_result_ch0, stop_send_coinc_result); 
    signal dma_state : dma_states; 
    
    -- UPLOAD
    signal dma_upload_control_ascan_request         : std_logic;
    signal dma_upload_control_cscan_request         : std_logic;
    signal dma_upload_control_config_request        : std_logic;
    signal dma_upload_control_capability_request    : std_logic;
    signal dma_upload_control_coinc_result_request  : std_logic;
    --signal dma_upload_control_ascan_availability: std_logic;
    --signal dma_upload_control_cscan_availability: std_logic;
    signal dma_upload_control_scan_ready            : std_logic;
    signal dma_upload_control_config_ready          : std_logic;
    signal dma_upload_control_capability_ready      : std_logic;
	signal dma_upload_control_coinc_result_ready    : std_logic;
    signal dma_upload_control_data_size             : std_logic_vector(REGISTER_W21-1 downto 0);
        
    signal dma_upload_status_ascan_request          : std_logic;
    signal dma_upload_status_cscan_request          : std_logic;
    signal dma_upload_status_config_request         : std_logic;
    signal dma_upload_status_capability_request     : std_logic;
    signal dma_upload_status_coinc_result_request   : std_logic;
    --signal dma_upload_status_ascan_availability : std_logic;
    --signal dma_upload_status_cscan_availability : std_logic;
    signal dma_upload_status_scan_ready             : std_logic;
    signal dma_upload_status_config_ready           : std_logic;
    signal dma_upload_status_capability_ready       : std_logic;
    signal dma_upload_status_coinc_result_ready     : std_logic;
    signal dma_upload_status_data_size              : std_logic_vector(REGISTER_W21-1 downto 0);
    
	signal channel_selected_for_upload  : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal upload_coinc_result_flag : STD_LOGIC;
       
    -- DOWNLOAD
    signal dma_download_control_config_request      : std_logic;
    signal dma_download_control_ascan_request       : std_logic;
    signal dma_download_control_avg_sample_request  : std_logic;
    signal dma_download_control_data_size           : std_logic_vector(REGISTER_W21-1 downto 0);
        
    signal dma_download_status_config_request       : std_logic;
    signal dma_download_status_ascan_request        : std_logic;
    signal dma_download_status_avg_sample_request   : std_logic;
    signal dma_download_status_data_size            : std_logic_vector(REGISTER_W21-1 downto 0);
    
    signal dma_s_axis_tlast_d1   : std_logic;
            
    signal sw_reset_i                   : std_logic;

	---------------------------------------------- CONFIGURATION ----------------------------------------------
	
    signal config_header_size        : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal config_channels           : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal config_channel0_offset    : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal config_encoder_offset     : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal config_digitalio_offset   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal config_channel0_size      : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

	signal config_trigger_type: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

	signal config_encoder_x_axis_enabled: STD_LOGIC;
	signal config_encoder_x_axis_start: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal config_encoder_x_axis_stopt: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal config_encoder_x_axis_trigger_counts: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

	signal config_encoder_y_axis_enabled: STD_LOGIC;
	signal config_encoder_y_axis_start: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal config_encoder_y_axis_stopt: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal config_encoder_y_axis_trigger_counts: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

	signal config_digitalio: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	
	signal config_counter : UNSIGNED(REGISTER_W32-1 downto 0);
	
    COMPONENT main_configuration IS
        generic (
            REGISTER_W32            : INTEGER := 32;
            BRAM_TREADY_SYNC        : INTEGER := 3
        );
        port (
            -- Sync
            aclk         : in STD_LOGIC;
            aresetn      : in STD_LOGIC;
            
            -- Control
            READ_CONFIGURATION  : in STD_LOGIC;
            CONFIGURATION_READY : out STD_LOGIC;
            CONFIGURATION_SIZE  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            
            -- Input
            CONFIGURATION_IN_AXIS_TREADY  : out STD_LOGIC;
            CONFIGURATION_IN_AXIS_TDATA   : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            CONFIGURATION_IN_AXIS_TLAST   : in STD_LOGIC;
            CONFIGURATION_IN_AXIS_TVALID  : in STD_LOGIC;
            
            -- Output
            CONFIGURATION_OUT_AXIS_TREADY  : in STD_LOGIC;
            CONFIGURATION_OUT_AXIS_TDATA   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            CONFIGURATION_OUT_AXIS_TLAST   : out STD_LOGIC;
            CONFIGURATION_OUT_AXIS_TVALID  : out STD_LOGIC;
            CONFIGURATION_OUT_AXIS_TKEEP   : out STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0)
        );
    END COMPONENT main_configuration;
    
    signal main_configuration_ready    : STD_LOGIC;
    signal main_configuration_size     : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    
    signal main_configuration_in_axis_tready   : STD_LOGIC;
    signal main_configuration_in_axis_tdata    : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal main_configuration_in_axis_tlast    : STD_LOGIC;
    signal main_configuration_in_axis_tvalid   : STD_LOGIC;
    
    signal main_configuration_out_axis_tready  : STD_LOGIC;
    signal main_configuration_out_axis_tdata   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal main_configuration_out_axis_tlast   : STD_LOGIC;
    signal main_configuration_out_axis_tvalid  : STD_LOGIC;
    signal main_configuration_out_axis_tkeep   : STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0);
    
    signal read_configuration_flag : STD_LOGIC;
    signal write_configuration_flag : STD_LOGIC;
      
    -- tkeep
    constant tkeep_all_bytes : STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0):= (others => '1');
    
    ---------------------------------------------- CAPABILITIES ----------------------------------------------
    
    COMPONENT capabilities IS
        generic (
            RAM_ADDR_LENGTH         : INTEGER := 10;
            REGISTER_W32            : INTEGER := 32;
            BRAM_TREADY_SYNC        : INTEGER := 3;
            WAIT_FOR_BRAM           : INTEGER := 16
        );
        port (
            -- Sync
            aclk         : in STD_LOGIC;
            aresetn      : in STD_LOGIC;
            
            -- Control
            READ_CAPABILITIES  : in STD_LOGIC;
            CAPABILITIES_READY : out STD_LOGIC;
            CAPABILITIES_SIZE  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
                            
            -- Capabilities
            CAPABILITIES_AXIS_TREADY  : in STD_LOGIC;
            CAPABILITIES_AXIS_TDATA   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            CAPABILITIES_AXIS_TLAST   : out STD_LOGIC;
            CAPABILITIES_AXIS_TVALID  : out STD_LOGIC;
            CAPABILITIES_AXIS_TKEEP   : out STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0)
        );
    END COMPONENT capabilities;
    
    signal read_capability_flag : STD_LOGIC;
    
    --signal read_capabilities  : STD_LOGIC;
    signal capabilities_ready  : STD_LOGIC;
    signal capabilities_size  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
                    
    signal capabilities_axis_tready  : STD_LOGIC;
    signal capabilities_axis_tdata   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal capabilities_axis_tlast   : STD_LOGIC;
    signal capabilities_axis_tvalid  : STD_LOGIC;
    signal capabilities_axis_tkeep   : STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0);
       
	---------------------------------------------- I/O ----------------------------------------------
	
	signal IO_OUTPUTS_i : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal IO_INPUTS_i  : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);

	---------------------------------------------- SEQUENCER ----------------------------------------------

	component channel_sequencer_1 is
		generic (
			CHANNEL_SEPPARATION	: INTEGER := 163840;
			REGISTER_W32    : INTEGER := 32;
			REGISTER_W16    : INTEGER := 16;
			REGISTER_W8     : INTEGER := 8
		);
		port (
			-- Sync
			aclk         : in STD_LOGIC;
			aresetn      : in STD_LOGIC;

			-- Control
			CHANNEL_SEQUENCER_EN : in STD_LOGIC;
			RECEIVER_DATA_WINDOW : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

			-- Tready
			TREADY_IN  : in STD_LOGIC;
			TREADY_OUT : out STD_LOGIC
		);
	end component channel_sequencer_1;

	signal CH0_SIGNAL_INPUT_AXIS_TREADY_i  : STD_LOGIC;
	signal CH0_RECEIVER_DATA_WINDOW_i      : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
		
begin

    CONFIGURATION_HEADER_SIZE      <= config_header_size;
    CONFIGURATION_CHANNELS         <= config_channels;
    CONFIGURATION_CHANNEL0_OFFSET  <= config_channel0_offset;
    CONFIGURATION_ENCODER_OFFSET   <= config_encoder_offset;
    CONFIGURATION_DIGITALIO_OFFSET <= config_digitalio_offset;
    CONFIGURATION_CHANNEL0_SIZE    <= config_channel0_size;
    CONFIGURATION_TRIGGER_TYPE     <= config_trigger_type;
    
	enable_status_i <= ENABLE_CONTROL; 
    ENABLE_STATUS <= enable_status_i;
    
    reset_status_i  <= RESET_CONTROL;  
    RESET_STATUS <= reset_status_i; 
    
    sw_reset_i <= RESET_CONTROL or (not ENABLE_CONTROL);  
    SW_RESET <= sw_reset_i; 
	     	
    control_process: process(aclk)
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				configure_control  <= '0'; 	   
				cscan_mode_status_i <= (others => '0');
        	else 
				cscan_mode_status_i <= CSCAN_MODE_CONTROL; 	
				if((START_INSPECTION_CONTROL = start_inspection_status_i) and
				   (SCAN_MODE_CONTROL = scan_mode_status_i) and
				   (TRIGGER_TYPE_CONTROL = trigger_type_status_i)) then
--                   (TRIGGER_TYPE_CONTROL = trigger_type_status_i) and
--                   (TRIGGER_CONTROL(N_CHANNELS-1 downto 0) = trigger_sw_status(N_CHANNELS-1 downto 0))) then
					configure_control  <= '0'; 	   
				else
					configure_control  <= '1'; 	   
				end if;
			end if;
        end if;
    end process;
    
    CSCAN_MODE_STATUS <= cscan_mode_status_i; 
    					 
	START_INSPECTION_STATUS <= start_inspection_status_i;
	SCAN_MODE_STATUS <= scan_mode_status_i;		
				   
	start_inspection_status_i <= CH0_START_INSPECTION_STATUS;
	scan_mode_status_i <= CH0_SCAN_MODE_STATUS;

	TRIGGER_TYPE_CONTROL <= config_trigger_type;
	trigger_type_status_i <= CH0_TRIGGER_TYPE_STATUS;			   
			
	TRIGGER_STATUS <= trigger_status_i; 
    trigger_status_i(REGISTER_W32-1 downto N_CHANNELS) <= (others => '0');
    trigger_status_i(N_CHANNELS-1 downto 0) <= trigger_sw_status;
   					
    -- UPLOAD DMA PORT (master) source selection
	DMA_M_axis_tvalid <= capabilities_axis_tvalid when read_capability_flag = '1' else
	                     main_configuration_out_axis_tvalid when read_configuration_flag = '1' else
						 ch0_ascan_axis_tvalid when (unsigned(channel_selected_for_upload) = 1 and upload_coinc_result_flag = '0') else
						 ch0_coinc_dma_result_axis_tvalid when (unsigned(channel_selected_for_upload) = 1 and upload_coinc_result_flag = '1') else 
						 '0';
		
	DMA_M_axis_tdata <= capabilities_axis_tdata when read_capability_flag = '1' else
	                    main_configuration_out_axis_tdata when read_configuration_flag = '1' else
						ch0_ascan_axis_tdata when (unsigned(channel_selected_for_upload) = 1 and upload_coinc_result_flag = '0') else 
						ch0_coinc_dma_result_axis_tdata when (unsigned(channel_selected_for_upload) = 1 and upload_coinc_result_flag = '1') else 
						(others =>'0');
		
	DMA_M_axis_tlast <= capabilities_axis_tlast when read_capability_flag = '1' else
	                    main_configuration_out_axis_tlast when read_configuration_flag = '1' else
						ch0_ascan_axis_tlast when (unsigned(channel_selected_for_upload) = 1 and upload_coinc_result_flag = '0') else
						ch0_coinc_dma_result_axis_tlast when (unsigned(channel_selected_for_upload) = 1 and upload_coinc_result_flag = '1') else 
						'0';
						
	DMA_M_axis_tkeep <= capabilities_axis_tkeep when read_capability_flag = '1' else
	                    main_configuration_out_axis_tkeep when read_configuration_flag = '1' else
                        ch0_ascan_axis_tkeep when (unsigned(channel_selected_for_upload) = 1 and upload_coinc_result_flag = '0') else
                        ch0_coinc_dma_result_axis_tkeep when (unsigned(channel_selected_for_upload) = 1 and upload_coinc_result_flag = '1') else 
                        (others =>'0');
		
	ch0_ascan_axis_tready <= DMA_M_axis_tready when (unsigned(channel_selected_for_upload) = 1 and upload_coinc_result_flag = '0') else
							'0';
							
	ch0_coinc_dma_result_axis_tready <= DMA_M_axis_tready when (unsigned(channel_selected_for_upload) = 1 and upload_coinc_result_flag = '1') else
                            '0';
							
	main_configuration_out_axis_tready  <= DMA_M_axis_tready when read_configuration_flag = '1' else
	                       '0';
	                       
	capabilities_axis_tready  <= DMA_M_axis_tready when read_capability_flag = '1' else
                           '0';
                           
    -- MAIN configuration input enable (DMA_S) when  write_configuration_flag = '1'                                                 
    main_configuration_in_axis_tdata <= DMA_S_axis_tdata when write_configuration_flag = '1' else
                                        (others => '0'); 
                                        
    main_configuration_in_axis_tlast <= DMA_S_axis_tlast when write_configuration_flag = '1' else
                                        '0';          
                                        
    main_configuration_in_axis_tvalid <= DMA_S_axis_tvalid when write_configuration_flag = '1' else
                                        '0';  
                                                          	                       	
    -- DMA   
    dma_process: process(aclk)
		variable var_counter : integer range 0 to 65536 :=0;			
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				-- DOWNLOAD
				dma_download_status_config_request <= '0';
				dma_download_status_ascan_request <= '0';
				dma_download_status_avg_sample_request <= '0';
				dma_s_axis_tlast_d1 <= '0';
				-- UPLOAD
				dma_upload_status_ascan_request <= '0';
				dma_upload_status_cscan_request <= '0';
				dma_upload_status_config_request <= '0';
				dma_upload_status_capability_request <= '0';
				dma_upload_status_coinc_result_request <= '0';
				--dma_upload_status_ascan_availability <= '0';
				--dma_upload_status_cscan_availability <= '0';
				dma_upload_status_scan_ready <= '0';
				dma_upload_status_config_ready <= '0';
				dma_upload_status_capability_ready <= '0';
				dma_upload_status_coinc_result_ready <= '0';
				dma_upload_status_data_size <= (others => '0');
				ch0_read_ascan <= '0';	
				ch0_read_coinc_result <= '0';
				channel_selected_for_upload <= (others => '0');
				upload_coinc_result_flag <= '0';
				-- CONFIG
				ch0_config_input_load <= '0';
				ch0_config_input_axis_tdata <= (others => '0');
				ch0_config_input_axis_tlast <= '0';
				ch0_config_input_axis_tvalid <= '0';

				config_header_size <= (others => '0');
				config_channels <= (others => '0');
				config_channel0_offset <= (others => '0');
				config_encoder_offset <= (others => '0');
				config_digitalio_offset <= (others => '0');
				config_channel0_size <= (others => '0');

				config_trigger_type <= (others => '0');
				config_encoder_x_axis_enabled <= '0';
				config_encoder_x_axis_start <= (others => '0');
				config_encoder_x_axis_stopt <= (others => '0');
				config_encoder_x_axis_trigger_counts <= (others => '0');
				config_encoder_y_axis_enabled <= '0';
				config_encoder_y_axis_start <= (others => '0');
				config_encoder_y_axis_stopt <= (others => '0');
				config_encoder_y_axis_trigger_counts <= (others => '0');

				config_digitalio <= (others => '0');
				
				CONFIGURATION_ON <= '0';
				CONFIGURATION_SIZE <= (others => '0');
				config_counter <= (others => '0');
                
                write_configuration_flag <= '0';
                read_configuration_flag <= '0';
                read_capability_flag <= '0';
                
				-- state
				dma_state <= idle;            
        	else
        	    dma_s_axis_tlast_d1 <= DMA_S_axis_tlast;
				case dma_state is        
					when idle =>
						DMA_S_axis_tready <= '1';
						var_counter := 0;
						if(dma_upload_control_capability_ready = '1') then
						    dma_state <= prepare_read_capability_1;
						elsif(dma_download_control_config_request = '1') then
							dma_state <= prepare_write_configuration;  
					    elsif(dma_upload_control_config_ready = '1') then
					        dma_state <= prepare_read_configuration_1;
					    elsif(dma_upload_control_ascan_request = '1') then
					        if(unsigned(channel_selected_for_upload) = 1) then
					            dma_state <= send_ascan_ch0;
					        end if;
					    elsif(dma_download_control_avg_sample_request = '1') then
					        dma_state <= prepare_write_avg_sample;
					    else            
                            if(ch0_ascan_ready = '1') then
                                dma_state <= start_send_ascan; 
                            else
                                dma_upload_status_scan_ready <= '0'; 
                            end if;
                        end if;
                    -- Handshake
                    when prepare_read_capability_1 =>
                        if(capabilities_ready = '1') then
                            dma_upload_status_capability_ready <= capabilities_ready;
                            dma_upload_status_data_size <= capabilities_size(REGISTER_W21-1 downto 0);
                            dma_state <= prepare_read_capability_2;
                        end if;
                    -- Handshake
                    when prepare_read_capability_2 =>
                        if(dma_upload_control_capability_ready = '0') then
                            dma_upload_status_capability_ready <= '0';
                            dma_state <= read_capability;
                        end if;
                    -- Handshake
                    when read_capability =>
                        if(dma_upload_control_capability_request = '1') then
                            dma_upload_status_capability_request <= '1';
                            read_capability_flag <= '1';
                            dma_state <= read_capability_end;
                        end if;
                    -- Handshake   
                    when read_capability_end =>
                        if(dma_upload_control_capability_request = '0') then
                            dma_upload_status_capability_request <= '0';
                            read_capability_flag <= '0';
                            dma_state <= idle;
                        end if;  
                    -- Handshake 
                    when prepare_read_configuration_1 =>
                        if(main_configuration_ready = '1') then
                            dma_upload_status_config_ready <= main_configuration_ready;
                            dma_upload_status_data_size <= main_configuration_size(REGISTER_W21-1 downto 0);
                            dma_state <= prepare_read_configuration_2;
                        end if;
                    when prepare_read_configuration_2 =>
                        if(dma_upload_control_config_ready = '0') then
                            dma_upload_status_config_ready <= '0';
                            dma_state <= read_configuration;
                        end if;                 
                    -- Handshake
					when read_configuration =>
					    if(dma_upload_control_config_request = '1') then
					       dma_upload_status_config_request <= '1';
					       read_configuration_flag <= '1';
					       dma_state  <= read_configuration_end;
                        end if;		
                    -- Handshake
                    when read_configuration_end => 
                        if(dma_upload_control_config_request = '0') then
                            dma_upload_status_config_request <= '0';
                            read_configuration_flag <= '0';
                            dma_state <= idle;
                        end if;
                    -- Handshake                                       			   					  
					when prepare_write_configuration =>
					    if(ch0_config_input_axis_tready = '1'and main_configuration_in_axis_tready = '1') then
					       	config_header_size <= (others => '1');
                           	config_channels <= (others => '1');
                           	config_channel0_offset <= (others => '1');
                           	config_encoder_offset <= (others => '1');
                           	config_digitalio_offset <= (others => '1');
                           	config_channel0_size <= (others => '1');
							dma_download_status_config_request <= '1';
							config_counter <= (others => '0');
							write_configuration_flag <= '1';
							dma_state <= write_configuration;  
					    end if;
					-- Write config (split by channels & store in main_configuration the original one)
					when write_configuration =>
						if(DMA_S_axis_tvalid = '1' and DMA_S_axis_tkeep = tkeep_all_bytes) then
						    config_counter <= config_counter + 1;
							if(var_counter = 0) then
								config_header_size <= DMA_S_axis_tdata;	
							elsif(var_counter = 1) then
								config_channels <= DMA_S_axis_tdata;	
							elsif(var_counter = 2) then
								config_channel0_offset <= DMA_S_axis_tdata;	
							elsif(var_counter = 3) then
								config_encoder_offset <= DMA_S_axis_tdata;		
							elsif(var_counter = 4) then
								config_digitalio_offset <= DMA_S_axis_tdata;
						    elsif(var_counter = unsigned(config_channel0_offset)) then
						        config_channel0_size <= DMA_S_axis_tdata;
						        ch0_config_input_axis_tdata <= DMA_S_axis_tdata;	
                                ch0_config_input_axis_tvalid <= '1';
                                ch0_config_input_load <= '1';
							elsif(	(var_counter > unsigned(config_channel0_offset)) and 
									(var_counter <= (unsigned(config_channel0_offset)+unsigned(config_channel0_size)-1)) 	)then
								ch0_config_input_axis_tdata <= DMA_S_axis_tdata;	
								ch0_config_input_axis_tvalid <= '1';
								ch0_config_input_load <= '1';
								if(var_counter = (unsigned(config_channel0_offset)+unsigned(config_channel0_size)-1)) then
									ch0_config_input_axis_tlast <= '1';	
								end if;
							elsif(	(var_counter > (unsigned(config_channel0_offset)+unsigned(config_channel0_size)-1)) and
									(var_counter < (unsigned(config_digitalio_offset))) ) then 
						        ch0_config_input_axis_tvalid <= '0';
                                ch0_config_input_axis_tlast  <= '0';
                                ch0_config_input_load        <= '0';
								if(var_counter = unsigned(config_encoder_offset))then
									config_trigger_type	<= DMA_S_axis_tdata;
								elsif(var_counter = unsigned(config_encoder_offset)+1)then
									config_encoder_x_axis_enabled	<= DMA_S_axis_tdata(0);
								elsif(var_counter = unsigned(config_encoder_offset)+2)then
									config_encoder_x_axis_start	<= DMA_S_axis_tdata;
								elsif(var_counter = unsigned(config_encoder_offset)+3)then
									config_encoder_x_axis_stopt	<= DMA_S_axis_tdata;
								elsif(var_counter = unsigned(config_encoder_offset)+4)then
									config_encoder_x_axis_trigger_counts <= DMA_S_axis_tdata;
								elsif(var_counter = unsigned(config_encoder_offset)+4)then
									config_encoder_y_axis_enabled <= DMA_S_axis_tdata(0);
								elsif(var_counter = unsigned(config_encoder_offset)+4)then
									config_encoder_y_axis_start <= DMA_S_axis_tdata;
								elsif(var_counter = unsigned(config_encoder_offset)+4)then
									config_encoder_y_axis_stopt <= DMA_S_axis_tdata;
								elsif(var_counter = unsigned(config_encoder_offset)+4)then
									config_encoder_y_axis_trigger_counts <= DMA_S_axis_tdata;				
								end if;
							elsif(var_counter = (unsigned(config_digitalio_offset))) then 	
								config_digitalio <= DMA_S_axis_tdata;	
							end if;
							var_counter := var_counter + 1;
						else
							ch0_config_input_axis_tvalid <= '0';	
							ch0_config_input_axis_tlast  <= '0';	
						end if;
						if(DMA_S_axis_tlast = '1') then
							dma_state <= write_configuration_ack; 
						end if;
				    -- Handshake
					when write_configuration_ack =>
						ch0_config_input_axis_tvalid <= '0';	
						ch0_config_input_axis_tlast  <= '0';
						if(dma_download_control_config_request = '0') then
							dma_download_status_config_request <= '0';
							CONFIGURATION_ON <= '1';
                            CONFIGURATION_SIZE <= std_logic_vector(config_counter);
                            write_configuration_flag <= '0';
							dma_state <= idle; 
						end if;
					when prepare_write_avg_sample =>
					    if(ch0_avg_sample_axis_tready = '1') then
                            dma_state <= write_avg_sample;  
                        end if;
					when write_avg_sample =>
					    dma_download_status_avg_sample_request <= '1';
					    if(DMA_S_axis_tvalid = '1' and DMA_S_axis_tkeep = tkeep_all_bytes) then
					        ch0_avg_sample_axis_tdata <= DMA_S_axis_tdata;	
                            ch0_avg_sample_axis_tvalid <= DMA_S_axis_tvalid;
                            ch0_avg_sample_axis_tlast  <= DMA_S_axis_tlast;
                        else
                            ch0_avg_sample_axis_tvalid <= '0';	
                            ch0_avg_sample_axis_tlast  <= '0';
                            if(dma_s_axis_tlast_d1 = '1') then
                                dma_state <= write_avg_sample_ack; 
                            end if;
					    end if;
					when write_avg_sample_ack =>	
						if(dma_download_control_avg_sample_request = '0') then
                            dma_download_status_avg_sample_request <= '0';
                            dma_state <= idle; 
                        end if;
				    -- Upload ASCAN
					when start_send_ascan =>
					    dma_upload_status_scan_ready <= '1'; 
						if(ch0_ascan_ready = '1') then
						    dma_upload_status_data_size <= ch0_ascan_size;
						    upload_coinc_result_flag <= '0';
						    channel_selected_for_upload	<= std_logic_vector(to_unsigned(1, channel_selected_for_upload'length));					 
						end if;
						dma_state <= idle;	
				    -- Handshake
					when send_ascan_ch0 => 										
						if(dma_upload_control_ascan_request = '1' and (unsigned(ch0_ascan_size) = unsigned(dma_upload_control_data_size))) then
							dma_upload_status_ascan_request <= '1';
							dma_upload_status_scan_ready <= '0';
							ch0_read_ascan	<= '1';																															
						end if;
						if(ch0_ascan_axis_tlast = '1') then
							dma_state <= stop_send_ascan;																
						end if;																	
					when stop_send_ascan =>
						ch0_read_ascan	<= '0';																		
						if(dma_upload_control_ascan_request = '0') then
							dma_upload_status_ascan_request <= '0';
							if(ch0_coinc_result_ready = '1') then
							    dma_upload_status_coinc_result_ready <= '1';
							    dma_upload_status_data_size <= ch0_coinc_result_size;
							    upload_coinc_result_flag <= '1';
							    dma_state <= start_send_coinc_result;
							else
							    dma_state <= idle; 
							end if; 
						end if;     
					when start_send_coinc_result =>
                        if(dma_upload_control_coinc_result_request = '1') then
                            dma_state <= send_coinc_result_ch0;                   
                        end if;
                    when send_coinc_result_ch0 =>
						if(dma_upload_control_coinc_result_request = '1' and 
						  (unsigned(ch0_coinc_result_size) = unsigned(dma_upload_control_data_size))) then
                            dma_upload_status_coinc_result_request <= '1';
                            dma_upload_status_coinc_result_ready <= '0';
                            ch0_read_coinc_result <= '1';                                                                                                                            
                        end if;
                        if(ch0_coinc_dma_result_axis_tlast = '1') then
                            dma_state <= stop_send_coinc_result;                                                                
                        end if;   
                    when stop_send_coinc_result => 
						ch0_read_coinc_result	<= '0';																		
                        if(dma_upload_control_coinc_result_request = '0') then
                            dma_upload_status_coinc_result_request <= '0';
                            upload_coinc_result_flag <= '0';
                            dma_state <= idle; 
                        end if;
					when others =>
						null;                   
				end case;  
			end if;
        end if;
    end process;
    
    DSP_AVG_LEVEL   <= AVG_LEVEL;
    DSP_AVG_TYPE    <= avg_type;
    DSP_AVG_COUNTER <= AVG_COUNTER;    
    CHANNEL_STATUS  <= (others => '0');
    
    MAIN_DMA_STATE <= std_logic_vector(to_unsigned(0, MAIN_DMA_STATE'length)) when dma_state = idle else
                       std_logic_vector(to_unsigned(1, MAIN_DMA_STATE'length)) when dma_state = prepare_write_configuration else
                       std_logic_vector(to_unsigned(2, MAIN_DMA_STATE'length)) when dma_state = write_configuration else
                       std_logic_vector(to_unsigned(3, MAIN_DMA_STATE'length)) when dma_state = write_configuration_ack else
                       std_logic_vector(to_unsigned(4, MAIN_DMA_STATE'length)) when dma_state = prepare_read_configuration_1 else
                       std_logic_vector(to_unsigned(5, MAIN_DMA_STATE'length)) when dma_state = prepare_read_configuration_2 else
                       std_logic_vector(to_unsigned(6, MAIN_DMA_STATE'length)) when dma_state = read_configuration else
                       std_logic_vector(to_unsigned(7, MAIN_DMA_STATE'length)) when dma_state = read_configuration_end else
                       std_logic_vector(to_unsigned(8, MAIN_DMA_STATE'length)) when dma_state = prepare_read_capability_1 else
                       std_logic_vector(to_unsigned(9, MAIN_DMA_STATE'length)) when dma_state = prepare_read_capability_2 else
                       std_logic_vector(to_unsigned(10, MAIN_DMA_STATE'length)) when dma_state = read_capability else
                       std_logic_vector(to_unsigned(11, MAIN_DMA_STATE'length)) when dma_state = read_capability_end else
                       std_logic_vector(to_unsigned(12, MAIN_DMA_STATE'length)) when dma_state = start_send_ascan else
                       std_logic_vector(to_unsigned(13, MAIN_DMA_STATE'length)) when dma_state = send_ascan_ch0 else
                       std_logic_vector(to_unsigned(14, MAIN_DMA_STATE'length)) when dma_state = send_ascan_ch1 else
                       std_logic_vector(to_unsigned(15, MAIN_DMA_STATE'length));
                       
    
    -- DMA Control signal management
    DMA_signals: process(aclk)
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				-- UPLOAD 
				dma_upload_control_ascan_request        <= '0';    
				dma_upload_control_cscan_request        <= '0';
				dma_upload_control_config_request       <= '0'; 
				dma_upload_control_capability_request   <= '0'; 
				dma_upload_control_coinc_result_request <= '0'; 
				dma_upload_control_scan_ready           <= '0'; 
				dma_upload_control_config_ready         <= '0'; 
				dma_upload_control_capability_ready     <= '0';  
				dma_upload_control_coinc_result_ready	<= '0';  								
				dma_upload_control_data_size            <= (others => '0');  
				-- DOWNLOAD  
				dma_download_control_config_request     <= '0';    
				dma_download_control_ascan_request      <= '0';
				dma_download_control_data_size          <= (others => '0'); 
        	else
				-- UPLOAD 
				dma_upload_control_ascan_request <= DMA_UPLOAD_CONTROL(0);
				dma_upload_control_cscan_request <= DMA_UPLOAD_CONTROL(1);
				dma_upload_control_config_request <= DMA_UPLOAD_CONTROL(2);
				dma_upload_control_capability_request <= DMA_UPLOAD_CONTROL(3);
				dma_upload_control_coinc_result_request <= DMA_UPLOAD_CONTROL(4);
				--dma_upload_control_ascan_availability <= DMA_UPLOAD_CONTROL(4);
				--dma_upload_control_Cscan_availability <= DMA_UPLOAD_CONTROL(5);
				dma_upload_control_scan_ready <= DMA_UPLOAD_CONTROL(6);
				dma_upload_control_config_ready <= DMA_UPLOAD_CONTROL(7);
				dma_upload_control_capability_ready <= DMA_UPLOAD_CONTROL(8);
				dma_upload_control_coinc_result_ready <= DMA_UPLOAD_CONTROL(9);								
				dma_upload_control_data_size <= DMA_UPLOAD_CONTROL(31 downto 11);
				-- DOWNLOAD  
				dma_download_control_config_request <= DMA_DOWNLOAD_CONTROL(0);
				dma_download_control_ascan_request <= DMA_DOWNLOAD_CONTROL(1);
				dma_download_control_avg_sample_request <= DMA_DOWNLOAD_CONTROL(2);
				dma_download_control_data_size <= DMA_DOWNLOAD_CONTROL(31 downto 11);  
			end if;
        end if;
    end process;
    
    dma_download_status_data_size <= dma_download_control_data_size;

    DMA_DOWNLOAD_STATUS <= dma_download_status_data_size & "00000000" & dma_download_status_avg_sample_request & 
        dma_download_status_ascan_request & dma_download_status_config_request;
               
    DMA_UPLOAD_STATUS <= dma_upload_status_data_size & "0" & dma_upload_status_coinc_result_ready & dma_upload_status_capability_ready & 
        dma_upload_status_config_ready & dma_upload_status_scan_ready & "0" & dma_upload_status_coinc_result_request & 
        dma_upload_status_capability_request &  dma_upload_status_config_request & dma_upload_status_cscan_request & dma_upload_status_ascan_request;
                
    ------------------------------------------ MAIN CONFIGURATION ------------------------------------------      
        
    main_configuration_inst: main_configuration
        generic map(
            REGISTER_W32     =>  REGISTER_W32
        )
        port map(
            -- Sync
            aclk         => aclk,
            aresetn      => aresetn,
            
            -- Control
            READ_CONFIGURATION  => read_configuration_flag,
            CONFIGURATION_READY => main_configuration_ready,
            CONFIGURATION_SIZE  => main_configuration_size,
                
            -- Input
            CONFIGURATION_IN_AXIS_TREADY  => main_configuration_in_axis_tready,
            CONFIGURATION_IN_AXIS_TDATA   => main_configuration_in_axis_tdata,
            CONFIGURATION_IN_AXIS_TLAST   => main_configuration_in_axis_tlast,
            CONFIGURATION_IN_AXIS_TVALID  => main_configuration_in_axis_tvalid,
                
            -- Output
            CONFIGURATION_OUT_AXIS_TREADY  => main_configuration_out_axis_tready,
            CONFIGURATION_OUT_AXIS_TDATA   => main_configuration_out_axis_tdata,
            CONFIGURATION_OUT_AXIS_TLAST   => main_configuration_out_axis_tlast,
            CONFIGURATION_OUT_AXIS_TVALID  => main_configuration_out_axis_tvalid,
            CONFIGURATION_OUT_AXIS_TKEEP   => main_configuration_out_axis_tkeep
        );
         
    ------------------------------------------ CAPABILITIES ------------------------------------------
    
    capabilities_inst: capabilities
        generic map (
            REGISTER_W32     =>  REGISTER_W32
        )
        port map(
            -- Sync
            aclk         => aclk,
            aresetn      => aresetn,
                
            -- Control
            READ_CAPABILITIES  => read_capability_flag,
            CAPABILITIES_READY => capabilities_ready,
            CAPABILITIES_SIZE  => capabilities_size,
                                
            -- Capabilities
            CAPABILITIES_AXIS_TREADY  => capabilities_axis_tready,
            CAPABILITIES_AXIS_TDATA   => capabilities_axis_tdata,
            CAPABILITIES_AXIS_TLAST   => capabilities_axis_tlast,
            CAPABILITIES_AXIS_TVALID  => capabilities_axis_tvalid,
            CAPABILITIES_AXIS_TKEEP   => capabilities_axis_tkeep
        );
													
    ------------------------------------------ SEQUENCER ------------------------------------------													
	channel_sequencer_1_inst: channel_sequencer_1
		port map(
			-- Sync
			aclk         => aclk,
			aresetn      => aresetn,

			-- Control
			CHANNEL_SEQUENCER_EN => '1',
			--CHANNEL_SEQUENCER_EN => '0',
			RECEIVER_DATA_WINDOW => CH0_RECEIVER_DATA_WINDOW_i,

			-- Tready
			TREADY_IN  => CH0_SIGNAL_INPUT_AXIS_TREADY_i,
			TREADY_OUT => CH0_SIGNAL_INPUT_AXIS_TREADY
		);
													
	CH0_DSP_COINC_LEVEL <= COINC_LEVEL;
	CH0_DSP_AVG_LEVEL <= AVG_LEVEL;
														
	CH0_DSP_BURST_LEVEL <= COINC_LEVEL;	
	CH0_DSP_BURST_COUNTER <= COINC_COUNTER;	
	
	CH0_RECEIVER_DATA_WINDOW <= CH0_RECEIVER_DATA_WINDOW_i;											
						   
	---------------------------------------------- CH0 ----------------------------------------------
	
	--IO_INPUTS_i <= "00000000" & IO_INPUTS;
	
    inputs_process: process(aclk)
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                IO_INPUTS_i(15 downto 0) <= (others => '0');
            else
                IO_INPUTS_i(7 downto 0) <= config_digitalio(23 downto 16) and IO_INPUTS;                                                                   
            end if;
        end if;
    end process;
													
	CH0_START_STOP_INSPECTION_CONTROL <= IO_INPUTS_i(0);
													
	--IO_OUTPUTS_i(0) <= CH0_INSPECTION_INPROGESS and CH1_INSPECTION_INPROGESS;
    -- CHANNEL 0 and CHANNEL 1 syncronized for inspection in progress
    outputs_process: process(aclk)
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				IO_OUTPUTS_i(15 downto 0) <= (others => '0');
        	else
        	    IO_OUTPUTS_i(15 downto 8) <= config_digitalio(15 downto 8);
        	    IO_OUTPUTS_i(7 downto 1) <= config_digitalio(7 downto 1);
			    IO_OUTPUTS_i(0) <= CH0_START_STOP_INSPECTION_STATUS or config_digitalio(0);																	
			end if;
        end if;
    end process;

	IO_OUTPUTS <= IO_OUTPUTS_i(7 downto 0);
																																																																																	
	CH0_ENCODER1_POS <= ENCODER1_COUNT;
	CH0_ENCODER1_DIR <= ENCODER1_DIR;	
	CH0_ENCODER1_RPM <= ENCODER1_COUNT;
	CH0_ENCODER1_TRIGGER_COUNTS	<= config_encoder_x_axis_trigger_counts;																				
	
	CH0_ENCODER2_POS <= (others => '0');
	CH0_ENCODER2_DIR <= (others => '0');
	CH0_ENCODER2_RPM <= (others => '0');
	CH0_ENCODER2_TRIGGER_COUNTS	<= (others => '0');																				
	
	CH0_ENCODER3_POS <= (others => '0');
	CH0_ENCODER3_DIR <= (others => '0');
	CH0_ENCODER3_RPM <= (others => '0');
	CH0_ENCODER3_TRIGGER_COUNTS	<= (others => '0');		
	
	CH0_CHANNEL_TRIGGER_TYPE <= CH0_TRIGGER_TYPE_STATUS;																		
	
	channel_0_inst: channel
	generic map(
		-- General
		CHANNEL_ID		=> 0,
		REGISTER_W64    => REGISTER_W64,
		REGISTER_W48    => REGISTER_W48,
	    REGISTER_W32    => REGISTER_W32,
	    REGISTER_W21    => REGISTER_W21,
        REGISTER_W16    => REGISTER_W16,
		REGISTER_W14    => REGISTER_W14,
		REGISTER_W8     => REGISTER_W8,
		REGISTER_W4     => REGISTER_W4,
		REGISTER_W2     => REGISTER_W2,

		-- Scan mode
		SCAN_MODE_NORMAL  => SCAN_MODE_NORMAL,
		SCAN_MODE_MRUT    => SCAN_MODE_MRUT,
		
		-- Trigger type
        TRIGGER_TYPE_NORMAL     => TRIGGER_TYPE_NORMAL,
        TRIGGER_TYPE_SW         => TRIGGER_TYPE_SW,
        TRIGGER_TYPE_ENC        => TRIGGER_TYPE_ENC,
		TRIGGER_TYPE_ROBOT      => TRIGGER_TYPE_ROBOT,
        TRIGGER_TYPE_ROBOT_ENC  => TRIGGER_TYPE_ROBOT_ENC,
		
		-- external trigger
		DISPOSITION_PASS  => DISPOSITION_PASS,
		DISPOSITION_FAIL  => DISPOSITION_FAIL,
		DISPOSITION_NODET => DISPOSITION_NODET,
		
		-- Coinc
        TRIGGER_WAIT    => TRIGGER_WAIT,
        MIN_COINC_LEVEL => MIN_COINC_LEVEL,
		
		-- Avg
		MIN_AVG_LEVEL     => MIN_AVG_LEVEL,
		AVG_TYPE_NORMAL   => AVG_TYPE_NORMAL,
        AVG_TYPE_PIPELINE => AVG_TYPE_PIPELINE,
		
		-- Ascan header size
		ASCAN_HEADER_SIZE => ASCAN_HEADER_SIZE,
		
		-- FIR CORR
        N_TAPS              => N_TAPS,
        N_SETS_TAP          => N_SETS_TAP,
        OVERSAMPLING        => OVERSAMPLING,
        WAIT_CYCLES         => WAIT_CYCLES
	)
	port map(
	    -- Sync
	    aclk         => aclk,
        aresetn      => aresetn,
		
		-- Control
		CONFIGURE_CONTROL => configure_control, 
		
		-- Start inspection
		START_INSPECTION_CONTROL => START_INSPECTION_CONTROL, 
		START_INSPECTION_STATUS  => CH0_START_INSPECTION_STATUS, 
		
		-- Start top inspection
		START_STOP_INSPECTION_CONTROL => CH0_START_STOP_INSPECTION_CONTROL, 
		START_STOP_INSPECTION_STATUS  => CH0_START_STOP_INSPECTION_STATUS, 
		DISPOSITION		  => CH0_DISPOSITION,
		DISPOSITION_BITS  => CH0_DISPOSITION_BITS,
		
		-- Scan mode
		SCAN_MODE_CONTROL => SCAN_MODE_CONTROL, 
		SCAN_MODE_STATUS  => CH0_SCAN_MODE_STATUS, 
		
		-- Trigger type
		TRIGGER_TYPE_CONTROL => TRIGGER_TYPE_CONTROL, 
		TRIGGER_TYPE_STATUS  => CH0_TRIGGER_TYPE_STATUS, 
		
		-- Trigger sw
        TRIGGER_SW_CONTROL    => TRIGGER_CONTROL(0),
        TRIGGER_SW_STATUS     => trigger_sw_status(0),
		
		-- Ascan
		READ_ASCAN  	  => ch0_read_ascan,
		ASCAN_READY  	  => ch0_ascan_ready,
		ASCAN_SIZE	      => ch0_ascan_size,
		
		-- Coinc
        READ_COINC_RESULT   => ch0_read_coinc_result,
        COINC_RESULT_READY  => ch0_coinc_result_ready,
        COINC_RESULT_SIZE   => ch0_coinc_result_size,
			
		-- DEBUG
		CHANNEL_ENABLED   => CONFIGURATION_CHANNEL0_ENABLED,
		CONFIG_ON         => CONFIGURATION_CHANNEL0_ON,
		CHANNEL_STATE_A   => CH0_STATE_A, 
		CHANNEL_STATE_B   => CH0_STATE_B, 
		CHANNEL_STATE_C   => CH0_STATE_C, 
			
		-- Signal input
		SIGNAL_COINC_WINDOW  	  => CH0_SIGNAL_COINC_WINDOW, 
		SIGNAL_COINC_COUNTER  	  => COINC_COUNTER, 
		SIGNAL_AVG_WINDOW  	  	  => CH0_SIGNAL_AVG_WINDOW, 
		SIGNAL_AVG_COUNTER  	  => AVG_COUNTER, 
		
		--SIGNAL_INPUT_AXIS_TREADY  => CH0_SIGNAL_INPUT_AXIS_TREADY, 
		SIGNAL_INPUT_AXIS_TREADY  => CH0_SIGNAL_INPUT_AXIS_TREADY_i, 
		SIGNAL_INPUT_AXIS_TDATA	  => CH0_SIGNAL_INPUT_AXIS_TDATA, 
		SIGNAL_INPUT_AXIS_TLAST	  => CH0_SIGNAL_INPUT_AXIS_TLAST, 
		SIGNAL_INPUT_AXIS_TVALID  => CH0_SIGNAL_INPUT_AXIS_TVALID, 
		
		-- Config
		CONFIG_INPUT_LOAD	  	  => ch0_config_input_load, 
		CONFIG_INPUT_AXIS_TREADY  => ch0_config_input_axis_tready, 
		CONFIG_INPUT_AXIS_TDATA	  => ch0_config_input_axis_tdata, 
		CONFIG_INPUT_AXIS_TLAST	  => ch0_config_input_axis_tlast, 
		CONFIG_INPUT_AXIS_TVALID  => ch0_config_input_axis_tvalid, 
		
		-- Avg sample
        AVG_SAMPLE_AXIS_TREADY    => ch0_avg_sample_axis_tready, 
        AVG_SAMPLE_AXIS_TDATA     => ch0_avg_sample_axis_tdata, 
        AVG_SAMPLE_AXIS_TLAST     => ch0_avg_sample_axis_tlast, 
        AVG_SAMPLE_AXIS_TVALID    => ch0_avg_sample_axis_tvalid, 
		
		-- ENCODERS
		ENCODER1_POS  	  => CH0_ENCODER1_POS, 
		ENCODER1_DIR  	  => CH0_ENCODER1_DIR, 
		ENCODER1_RPM  	  => CH0_ENCODER1_RPM, 
		ENCODER1_TRIGGER_COUNTS => CH0_ENCODER1_TRIGGER_COUNTS,
		
		ENCODER2_POS  	  => CH0_ENCODER2_POS,
		ENCODER2_DIR  	  => CH0_ENCODER2_DIR,
		ENCODER2_RPM  	  => CH0_ENCODER2_RPM,
		ENCODER2_TRIGGER_COUNTS => CH0_ENCODER2_TRIGGER_COUNTS,
		
		ENCODER3_POS  	  => CH0_ENCODER3_POS,
		ENCODER3_DIR  	  => CH0_ENCODER3_DIR,
		ENCODER3_RPM  	  => CH0_ENCODER3_RPM,
		ENCODER3_TRIGGER_COUNTS => CH0_ENCODER3_TRIGGER_COUNTS,
		
		-- IO
		IO_INPUTS  	  	  => IO_INPUTS_i,
		IO_OUTPUTS  	  => IO_OUTPUTS_i,
		
		-- Config output receiver
		RECEIVER_SAMPLING_FREQUENCY         => CH0_RECEIVER_SAMPLING_FREQUENCY, 
        RECEIVER_DATA_WINDOW                => CH0_RECEIVER_DATA_WINDOW_i,  
        RECEIVER_DELAY                      => CH0_RECEIVER_DELAY,  
        RECEIVER_ANALOG_GAIN                => CH0_RECEIVER_ANALOG_GAIN,
        RECEIVER_DIGITAL_GAIN               => CH0_RECEIVER_DIGITAL_GAIN,  
        RECEIVER_EXTERNAL_MULTIPLEXER_EN    => CH0_RECEIVER_EXTERNAL_MULTIPLEXER_EN,
		
		-- Config output transmitter
		TRANSMITTER_VOLTAGE                 => CH0_TRANSMITTER_VOLTAGE, 
        TRANSMITTER_BURST_FREQUENCY         => CH0_TRANSMITTER_BURST_FREQUENCY,
        TRANSMITTER_N_CYCLES                => CH0_TRANSMITTER_N_CYCLES,
        TRANSMITTER_N_BURST                 => CH0_TRANSMITTER_N_BURST,
        TRANSMITTER_DELAY                   => CH0_TRANSMITTER_DELAY,
        TRANSMITTER_DELTA_DELAY             => CH0_TRANSMITTER_DELTA_DELAY,
        TRANSMITTER_DELTA_ADD               => CH0_TRANSMITTER_DELTA_ADD ,
        TRANSMITTER_DIRECTIONAL_PHASING     => CH0_TRANSMITTER_DIRECTIONAL_PHASING, 
        TRANSMITTER_START_CYCLE             => open,
		
		-- Config output magnet
		MAGNET_MODE                         => CH0_MAGNET_MODE,
        MAGNET_PULSE_WIDTH                  => CH0_MAGNET_PULSE_WIDTH, 
        MAGNET_INITIAL_DELAY                => CH0_MAGNET_INITIAL_DELAY,
        MAGNET_RAMP_UP_VOLTAGE              => CH0_MAGNET_RAMP_UP_VOLTAGE, 
        MAGNET_VOLTAGE                      => CH0_MAGNET_VOLTAGE,  
		
		-- Config analog filters
		DSP_ANALOG_FILTER  					=> CH0_DSP_ANALOG_FILTER,
		DSP_COINC_LEVEL  					=> COINC_LEVEL,
		DSP_AVG_LEVEL  					    => AVG_LEVEL,
		DSP_AVG_TYPE                        => avg_type,
					
		-- Config DAC curves
		CONFIG_DAC_AXIS_TREADY    => CH0_CONFIG_DAC_AXIS_TREADY,
		CONFIG_DAC_AXIS_TDATA	  => CH0_CONFIG_DAC_AXIS_TDATA,
		CONFIG_DAC_AXIS_TLAST	  => CH0_CONFIG_DAC_AXIS_TLAST,
		CONFIG_DAC_AXIS_TVALID    => CH0_CONFIG_DAC_AXIS_TVALID,
					
		-- ASCAN
		ASCAN_AXIS_TREADY  => ch0_ascan_axis_tready,
		ASCAN_AXIS_TDATA   => ch0_ascan_axis_tdata,
		ASCAN_AXIS_TLAST   => ch0_ascan_axis_tlast,
		ASCAN_AXIS_TVALID  => ch0_ascan_axis_tvalid,
		ASCAN_AXIS_TKEEP   => ch0_ascan_axis_tkeep,
		
		-- COINCIDENCE RESULT
        COINC_DMA_RESULT_AXIS_TREADY  => ch0_coinc_dma_result_axis_tready,
        COINC_DMA_RESULT_AXIS_TDATA   => ch0_coinc_dma_result_axis_tdata,
        COINC_DMA_RESULT_AXIS_TLAST   => ch0_coinc_dma_result_axis_tlast,
        COINC_DMA_RESULT_AXIS_TVALID  => ch0_coinc_dma_result_axis_tvalid,
        COINC_DMA_RESULT_AXIS_TKEEP   => ch0_coinc_dma_result_axis_tkeep
	);
		     																																					       
end Behavioral;