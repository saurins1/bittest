library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity average_filter_tb is
  	generic (
          REGISTER_W32    : INTEGER := 32;
          REGISTER_W16    : INTEGER := 16;
          TRIGGER_WAIT    : INTEGER := 32;
          MIN_AVG_LEVEL : INTEGER := 2
  	);	
end;

architecture bench of average_filter_tb is

  component average_filter
  	generic (
          REGISTER_W32    : INTEGER := 32;
          REGISTER_W16    : INTEGER := 16;
          TRIGGER_WAIT    : INTEGER := 32;
          MIN_AVG_LEVEL : INTEGER := 2
  	);
  	port (
  	    aclk         : in std_logic;
        aresetn      : in std_logic;
  	    AVG_LEVEL	    : in std_logic_vector(REGISTER_W32-1 downto 0);
  	    DATA_WINDOW	    : in std_logic_vector(REGISTER_W32-1 downto 0);
  	    SAMPLE_FREQ	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  	    INSPECTION_ON  	: in STD_LOGIC;
  	    DATA_WINDOW_RESULT	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  	    SAMPLE_FREQ_RESULT	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  	    AVG_WINDOW  	  : out STD_LOGIC;
  	    
		AVG_TYPE	     : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        AVG_RESET        : in STD_LOGIC;
        AVG_RESET_ACK    : out STD_LOGIC;
        AVG_LEVEL_RESULT : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        AVG_SUBSTRACT_ON : out STD_LOGIC;
  	    
  		SIGNAL_INPUT_AXIS_TREADY  : out std_logic;
  		SIGNAL_INPUT_AXIS_TDATA	  : in std_logic_vector(REGISTER_W16-1 downto 0);
  		SIGNAL_INPUT_AXIS_TLAST	  : in std_logic;
  		SIGNAL_INPUT_AXIS_TVALID  : in std_logic;
  		AVG_RESULT_AXIS_TVALID    : out std_logic;
  		AVG_RESULT_AXIS_TDATA	  : out std_logic_vector(REGISTER_W16-1 downto 0);
  		AVG_RESULT_AXIS_TLAST	  : out std_logic;
  		AVG_RESULT_AXIS_TREADY    : in std_logic;
  		
		-- Average sample input
        AVG_SAMPLE_AXIS_TREADY  : out STD_LOGIC;
        AVG_SAMPLE_AXIS_TDATA   : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        AVG_SAMPLE_AXIS_TLAST   : in STD_LOGIC;
        AVG_SAMPLE_AXIS_TVALID  : in STD_LOGIC
          
  	);
  end component;

  signal aclk: std_logic:= '0';
  signal aresetn: std_logic:= '0';

  signal AVG_LEVEL: std_logic_vector(REGISTER_W32-1 downto 0):= (others => '0');
  signal DATA_WINDOW: std_logic_vector(REGISTER_W32-1 downto 0):= (others => '0');
  signal SAMPLE_FREQ: std_logic_vector(REGISTER_W32-1 downto 0):= (others => '0');
  signal INSPECTION_ON: STD_LOGIC:= '0';
  signal DATA_WINDOW_RESULT: std_logic_vector(REGISTER_W32-1 downto 0);
  signal SAMPLE_FREQ_RESULT: std_logic_vector(REGISTER_W32-1 downto 0);
  signal AVG_WINDOW : std_logic;
  
  signal AVG_TYPE	     : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');
  signal AVG_RESET        : STD_LOGIC:= '0';
  signal AVG_RESET_ACK    : STD_LOGIC;
  signal AVG_LEVEL_RESULT : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal AVG_SUBSTRACT_ON : STD_LOGIC;
  
  signal SIGNAL_INPUT_AXIS_TREADY: std_logic;
  signal SIGNAL_INPUT_AXIS_TDATA: std_logic_vector(REGISTER_W16-1 downto 0):= (others => '0');
  signal SIGNAL_INPUT_AXIS_TLAST: std_logic:= '0';
  signal SIGNAL_INPUT_AXIS_TVALID: std_logic:= '0';
  signal AVG_RESULT_AXIS_TVALID: std_logic;
  signal AVG_RESULT_AXIS_TDATA: std_logic_vector(REGISTER_W16-1 downto 0);
  signal AVG_RESULT_AXIS_TLAST: std_logic;
  signal AVG_RESULT_AXIS_TREADY: std_logic:= '0';
  
  signal AVG_SAMPLE_AXIS_TREADY  : STD_LOGIC;
  signal AVG_SAMPLE_AXIS_TDATA   : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0');
  signal AVG_SAMPLE_AXIS_TLAST   : STD_LOGIC:= '0';
  signal AVG_SAMPLE_AXIS_TVALID  : STD_LOGIC:= '0';

  constant aclk_period: time := 10 ns;

  type states_signal_sm is (idle, open_file, load_signal, load_signal_wait, close_file); 
  signal state_signal_sm : states_signal_sm; 
  
  type states_avg_sample_sm is (idle, open_file, load_signal, load_signal_wait, close_file); 
  signal state_avg_sample_sm : states_avg_sample_sm; 

  signal start_sm: STD_LOGIC:= '0';

  signal decimation_level: std_logic_vector(REGISTER_W32-1 downto 0):= (others => '0');
  signal load_signal_counter: unsigned(REGISTER_W16-1 downto 0):= (others => '0');
  signal load_avg_sample_counter: unsigned(REGISTER_W16-1 downto 0):= (others => '0');

begin
	
	-- aclk process 
    aclk_process :process
    begin
        aclk <= not aclk;
        wait for aclk_period/2;
        aclk <= not aclk;
        wait for aclk_period/2;
    end process;

  -- Insert values for generic parameters !!
  uut: average_filter generic map ( REGISTER_W32             => REGISTER_W32,
                                        REGISTER_W16             => REGISTER_W16,
                                        TRIGGER_WAIT             => TRIGGER_WAIT,
                                        MIN_AVG_LEVEL          => MIN_AVG_LEVEL )
                             port map ( aclk                     => aclk,
                                        aresetn                  => aresetn,
                                        AVG_LEVEL              => AVG_LEVEL,
                                        DATA_WINDOW              => DATA_WINDOW,
                                        SAMPLE_FREQ              => SAMPLE_FREQ,
                                        INSPECTION_ON            => INSPECTION_ON,
                                        DATA_WINDOW_RESULT       => DATA_WINDOW_RESULT,
                                        SAMPLE_FREQ_RESULT       => SAMPLE_FREQ_RESULT,
                                        AVG_WINDOW               => AVG_WINDOW,
                                        AVG_TYPE                 => AVG_TYPE, 
                                        AVG_RESET                => AVG_RESET,
                                        AVG_RESET_ACK            => AVG_RESET_ACK,
                                        AVG_LEVEL_RESULT         => AVG_LEVEL_RESULT,
                                        AVG_SUBSTRACT_ON         => AVG_SUBSTRACT_ON,
                                        SIGNAL_INPUT_AXIS_TREADY => SIGNAL_INPUT_AXIS_TREADY,
                                        SIGNAL_INPUT_AXIS_TDATA  => SIGNAL_INPUT_AXIS_TDATA,
                                        SIGNAL_INPUT_AXIS_TLAST  => SIGNAL_INPUT_AXIS_TLAST,
                                        SIGNAL_INPUT_AXIS_TVALID => SIGNAL_INPUT_AXIS_TVALID,
                                        AVG_RESULT_AXIS_TVALID => AVG_RESULT_AXIS_TVALID,
                                        AVG_RESULT_AXIS_TDATA  => AVG_RESULT_AXIS_TDATA,
                                        AVG_RESULT_AXIS_TLAST  => AVG_RESULT_AXIS_TLAST,
                                        AVG_RESULT_AXIS_TREADY => AVG_RESULT_AXIS_TREADY,
                                        AVG_SAMPLE_AXIS_TREADY => AVG_SAMPLE_AXIS_TREADY,
                                        AVG_SAMPLE_AXIS_TDATA  => AVG_SAMPLE_AXIS_TDATA,
                                        AVG_SAMPLE_AXIS_TLAST  => AVG_SAMPLE_AXIS_TLAST,
                                        AVG_SAMPLE_AXIS_TVALID => AVG_SAMPLE_AXIS_TVALID                                         
                                        );

	stimulus: process
	begin
		aresetn <= '0';
		wait for aclk_period*10;
		aresetn <= '1';
		wait for aclk_period*10;
		start_sm <= '1';
		wait;
	end process;
	
	-- Average level
    avg_level_process: process(aclk) 
        variable my_counter  : integer range 0 to 256000 :=0; 
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                AVG_LEVEL	<= std_logic_vector(to_unsigned(0, AVG_LEVEL'length));
                my_counter := 0;                                                
            else
                if(my_counter >= 250000) then
                    my_counter := 0;
                    if(unsigned(AVG_LEVEL) < 32) then
                        AVG_LEVEL	<= std_logic_vector(unsigned(AVG_LEVEL)+1);
                    end if;
                else
                    my_counter := my_counter + 1;
                end if;                                              
            end if;
        end if;
    end process;
	
    signal_input_inst: process(aclk, aresetn)  
        variable my_counter  : integer range 0 to 16383 :=0;   
		variable decimation_counter  : integer range 0 to 16383 :=0; 
    begin 
        if (aresetn = '0') then
			-- config
			--AVG_TYPE	<= std_logic_vector(to_unsigned(0, AVG_TYPE'length));
			AVG_TYPE	<= std_logic_vector(to_unsigned(1, AVG_TYPE'length));
			
			--AVG_LEVEL	<= std_logic_vector(to_unsigned(1, AVG_LEVEL'length));
			--AVG_LEVEL	<= std_logic_vector(to_unsigned(2, AVG_LEVEL'length));
			--AVG_LEVEL	<= std_logic_vector(to_unsigned(4, AVG_LEVEL'length));
			--AVG_LEVEL	<= std_logic_vector(to_unsigned(8, AVG_LEVEL'length));                                                                       
			--AVG_LEVEL	<= std_logic_vector(to_unsigned(16, AVG_LEVEL'length));
			--AVG_LEVEL	<= std_logic_vector(to_unsigned(32, AVG_LEVEL'length));
			--AVG_LEVEL	<= std_logic_vector(to_unsigned(64, AVG_LEVEL'length));
			DATA_WINDOW <= std_logic_vector(to_unsigned(1024, DATA_WINDOW'length));		
			
			INSPECTION_ON <= '1';
																			  
			-- SIGNAL INPUT AXIS
			SIGNAL_INPUT_AXIS_TDATA  <= (others => '0');
			SIGNAL_INPUT_AXIS_TLAST  <= '0';
			SIGNAL_INPUT_AXIS_TVALID <= '0';
			-- AVG RESULT AXIS
			AVG_RESULT_AXIS_TREADY <= '1';																  
			-- decimation
			decimation_level <= std_logic_vector(to_unsigned(1, DATA_WINDOW'length));
			--decimation_level <= std_logic_vector(to_unsigned(4, DATA_WINDOW'length));		
			load_signal_counter <= (others => '0');															  
			-- state																		  	
            state_signal_sm <= idle;  
        elsif (aclk'event and aclk = '1') then
            case state_signal_sm is  
                when idle =>         
                    if(start_sm = '1') then
                        my_counter := 0;
                        load_signal_counter <= (others => '0');	
                        state_signal_sm <= load_signal;        
                    end if; 
                when load_signal =>
                    if(SIGNAL_INPUT_AXIS_TREADY = '1') then
                		if(my_counter <= to_integer(unsigned(DATA_WINDOW))-1) then
                            SIGNAL_INPUT_AXIS_TDATA<=std_logic_vector(load_signal_counter);
							SIGNAL_INPUT_AXIS_TVALID <= '1';		
							if(my_counter = to_integer(unsigned(DATA_WINDOW))-1) then
								SIGNAL_INPUT_AXIS_TLAST <= '1';
							end if;
							if(unsigned(decimation_level) > 1) then	
								decimation_counter := 0;	
								state_signal_sm <= load_signal_wait;															
							end if;
							load_signal_counter <= load_signal_counter + 1;
							my_counter := my_counter + 1; 
						else
							SIGNAL_INPUT_AXIS_TVALID <= '0';
							SIGNAL_INPUT_AXIS_TLAST <= '0';													
							my_counter  := 0;
							state_signal_sm    <= close_file;	
                        end if;      
                    end if;																		
				when load_signal_wait =>
					SIGNAL_INPUT_AXIS_TVALID <= '0';
					SIGNAL_INPUT_AXIS_TLAST <= '0';																
					if(decimation_counter >= unsigned(decimation_level)-2) then
						state_signal_sm <= load_signal; 																
					else
						decimation_counter := decimation_counter + 1;																
					end if;																																
                when close_file =>
                    if(my_counter >= 32) then
                        my_counter  := 0;
                        state_signal_sm <= idle;
                    else
                        my_counter := my_counter + 1;
                    end if;   
                when others =>
                    null;                   
            end case;                                                                                            
        end if;
    end process;
    
    avg_avg_sample_input_inst: process(aclk, aresetn)  
        variable my_counter  : integer range 0 to 16383 :=0;   
        variable decimation_counter  : integer range 0 to 16383 :=0; 
    begin 
        if (aresetn = '0') then                                                                       
            -- SIGNAL INPUT AXIS
            AVG_SAMPLE_AXIS_TDATA  <= (others => '0');
            AVG_SAMPLE_AXIS_TLAST  <= '0';
            AVG_SAMPLE_AXIS_TVALID <= '0';                                                          
            -- state                                                                              
            state_avg_sample_sm <= idle;  
        elsif (aclk'event and aclk = '1') then
            case state_avg_sample_sm is  
                when idle =>         
                    if(start_sm = '1') then
                        my_counter := 0;
                        load_avg_sample_counter <= (others => '0');    
                        state_avg_sample_sm <= load_signal;        
                    end if; 
                when load_signal =>
                    if(AVG_SAMPLE_AXIS_TREADY = '1') then
                        if(my_counter <= to_integer(unsigned(DATA_WINDOW))-1) then
                            AVG_SAMPLE_AXIS_TDATA<=std_logic_vector(load_avg_sample_counter);
                            AVG_SAMPLE_AXIS_TVALID <= '1';        
                            if(my_counter = to_integer(unsigned(DATA_WINDOW))-1) then
                                AVG_SAMPLE_AXIS_TLAST <= '1';
                            end if;
                            if(unsigned(decimation_level) > 1) then    
                                decimation_counter := 0;    
                                state_avg_sample_sm <= load_signal_wait;                                                            
                            end if;
                            load_avg_sample_counter <= load_avg_sample_counter + 1;
                            my_counter := my_counter + 1; 
                        else
                            AVG_SAMPLE_AXIS_TVALID <= '0';
                            AVG_SAMPLE_AXIS_TLAST <= '0';                                                    
                            my_counter  := 0;
                            state_avg_sample_sm    <= close_file;    
                        end if;      
                    end if;                                                                        
                when load_signal_wait =>
                    AVG_SAMPLE_AXIS_TVALID <= '0';
                    AVG_SAMPLE_AXIS_TLAST <= '0';                                                                
                    if(decimation_counter >= unsigned(decimation_level)-2) then
                        state_avg_sample_sm <= load_signal;                                                                 
                    else
                        decimation_counter := decimation_counter + 1;                                                                
                    end if;                                                                                                                                
                when close_file =>
                    if(my_counter >= 32) then
                        my_counter  := 0;
                        state_avg_sample_sm <= idle;
                    else
                        my_counter := my_counter + 1;
                    end if;   
                when others =>
                    null;                   
            end case;                                                                                            
        end if;
    end process; 
																																					
end;