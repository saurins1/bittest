-- spartan3 gain filter download (DAC also download from here)
--	gain parameters ( -23dB to 73dB hardware limit )
-- -23db		41							0.04v
-- -20db		71 "00 0100 0111"		0.07v
-- 0db		276 "01 0001 0100"	0.27v
-- 20db		480 "01 1110 0000"	0.47v
-- 40db		684 "10 1010 1100"	0.67v
-- 73db		1021						1.00v
-- 1db		30.64/3 					0.01v
-- db to DAC code : db * 10 + 276. (accurate at 0db, +/-0.5db@-23db/25db -1.5db@73db)
--
-- filter parameters
-- "000"		0.05MHz to 25MHz
-- "001"		0.0625MHz to 0.3125MHz
-- "010"		0.125MHz to 0.625MHz
-- "011"		0.25MHz to 1.25MHz
-- "100"		0.5MHz to 2.5MHz
-- "101"		1MHz to 5MHz
-- "110"		2MHz to 10MHz
-- "111"		4MHz to 20MHz

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity spartan3_gain_filt_dac is
	port (
			CLOCK					: in std_logic;
			RESET					: in std_logic;

			CH0_RECEIVER_GAIN       : in STD_LOGIC_VECTOR(31 downto 0);  
			CH1_RECEIVER_GAIN       : in STD_LOGIC_VECTOR(31 downto 0);  
			CH0_DSP_ANALOG_FILTER  	: in STD_LOGIC_VECTOR(3 downto 0);
			CH1_DSP_ANALOG_FILTER  	: in STD_LOGIC_VECTOR(3 downto 0);
			
			spartan3_ver			: out STD_LOGIC_VECTOR(31 DOWNTO 0);
			
			RCVR_FPGA_SCLK			: out STD_LOGIC;
			RCVR_FPGA_CS			: out STD_LOGIC;
			RCVR_FPGA_SDO			: in STD_LOGIC;
			RCVR_FPGA_SDI			: out STD_LOGIC
		);
end spartan3_gain_filt_dac;

architecture spartan3_gain_filt_dac_arch of spartan3_gain_filt_dac is

  type Spartan3_StateEnum_t is (
						   Spartan3_Init_c,
						   Spartan3_Readid_c,
						   Spartan3_WaitReadidACK_c,
						   Spartan3_WaitReadiddone_c,
						   Spartan3_Readver_c,
						   Spartan3_WaitReadverACK_c,
						   Spartan3_WaitReadverdone_c,
						   Spartan3_ReadLoopback_c,
						   Spartan3_WaitReadLoopbackACK_c,
						   Spartan3_WaitReadLoopbackdone_c,
						   Spartan3_Idle_c,
						   Spartan3_Send_CH0_Gain_Filter_c,
						   Spartan3_Send_CH1_Gain_Filter_c,
						   Spartan3_ConvGain1_c,
						   Spartan3_ConvGain2_c,
						   Spartan3_ConvGain3_c,
						   Spartan3_SendCurrentFrame_c,
						   Spartan3_WaitCurrentFrameACK_c,
						   Spartan3_SendGainFilt_c,
						   Spartan3_WaitGainFiltACK_c
						   );
						   
  signal Spartan3_State_s 	:	Spartan3_StateEnum_t;
  
	signal m_Spartan3_ver	: STD_LOGIC_VECTOR(31 DOWNTO 0) := (others => '1');		-- "FFFFFFFF" no valid Spartan3 firmware
	signal Spartan3_valid	: std_logic := '0';										-- "1" Spartan3 has valid firmware

	signal CH0_Gain_Filter_Reg		: STD_LOGIC_VECTOR(35 DOWNTO 0) := (others => '0');
	signal CH1_Gain_Filter_Reg		: STD_LOGIC_VECTOR(35 DOWNTO 0) := (others => '0');
	
	signal Conv_Gain				: signed(19 DOWNTO 0);	
	
	signal Channel_Gain				: signed(31 DOWNTO 0);
	signal Channel_Filter			: STD_LOGIC_VECTOR(2 DOWNTO 0);
	signal Channel_Address			: STD_LOGIC_VECTOR(13 DOWNTO 0);
	
	signal Current_Frame			: STD_LOGIC_VECTOR(7 DOWNTO 0) := "00000001";
	signal Current_Frame_Reg		: STD_LOGIC_VECTOR(7 DOWNTO 0) := (others => '0');
	
	constant POR_Gain0db			: STD_LOGIC_VECTOR(9 DOWNTO 0) := "0100010100";		--0db POR gain
	constant POR_filt				: STD_LOGIC_VECTOR(2 DOWNTO 0) := "000";			--25MHz (bandwidth) POR filter	
	constant Max_Gain				: signed(31 DOWNTO 0) := to_signed(73*1024, 32);		--Max Gain +73dB
	constant Min_Gain				: signed(31 DOWNTO 0) := to_signed( (-23)*1024, 32);	--Min Gain -23dB
	--
	
    component spi_master_spartan3 is
      Generic (   
          N : positive := 32;                                             -- 32bit serial word length is default
          CPOL : std_logic := '0';                                        -- SPI mode selection (mode 0 default)
          CPHA : std_logic := '0';                                        -- CPOL = clock polarity, CPHA = clock phase.
          PREFETCH : positive := 2;                                       -- prefetch lookahead cycles
          SPI_2X_CLK_DIV : positive := 5);                                -- for a 100MHz sclk_i, yields a 10MHz SCK
      Port (  
          sclk_i : in std_logic := 'X';                                   -- high-speed serial interface system clock
          pclk_i : in std_logic := 'X';                                   -- high-speed parallel interface system clock
          rst_i : in std_logic := 'X';                                    -- reset core
          ---- serial interface ----
          spi_ssel_o : out std_logic;                                     -- spi bus slave select line
          spi_sck_o : out std_logic;                                      -- spi bus sck
          spi_mosi_o : out std_logic;                                     -- spi bus mosi output
          spi_miso_i : in std_logic := 'X';                               -- spi bus spi_miso_i input
          ---- parallel interface ----
          di_req_o : out std_logic;                                       -- preload lookahead data request line
          di_i : in  std_logic_vector (N-1 downto 0) := (others => 'X');  -- parallel data in (clocked on rising spi_clk after last bit)
          wren_i : in std_logic := 'X';                                   -- user data write enable, starts transmission when interface is idle
          wr_ack_o : out std_logic;                                       -- write acknowledge
          do_valid_o : out std_logic;                                     -- do_o data valid signal, valid during one spi_clk rising edge.
          do_o : out  std_logic_vector (N-1 downto 0)                     -- parallel output (clocked on rising spi_clk after last bit)
          --- debug ports: can be removed or left unconnected for the application circuit ---
--          sck_ena_o : out std_logic;                                      -- debug: internal sck enable signal
--          sck_ena_ce_o : out std_logic;                                   -- debug: internal sck clock enable signal
--          do_transfer_o : out std_logic;                                  -- debug: internal transfer driver
--          wren_o : out std_logic;                                         -- debug: internal state of the wren_i pulse stretcher
--          rx_bit_reg_o : out std_logic;                                   -- debug: internal rx bit
--          state_dbg_o : out std_logic_vector (3 downto 0);                -- debug: internal state register
--          core_clk_o : out std_logic;
--          core_n_clk_o : out std_logic;
--          core_ce_o : out std_logic;
--          core_n_ce_o : out std_logic
--          sh_reg_dbg_o : out std_logic_vector (N-1 downto 0)              -- debug: internal shift register
      );                      
  end component spi_master_spartan3;
  
  SIGNAL m_wren_i				: STD_LOGIC;
  SIGNAL m_do_valid_o           : STD_LOGIC;
  SIGNAL m_di_req_o             : STD_LOGIC;
  SIGNAL m_wr_ack_o             : STD_LOGIC;
  SIGNAL m_wr_ack_o_d1          : STD_LOGIC;
  
--  SIGNAL m_do_transfer_o        : STD_LOGIC;
--  SIGNAL m_wren_o               : STD_LOGIC;
--  SIGNAL m_rx_bit_reg_o         : STD_LOGIC;
--  SIGNAL m_core_clk_o           : STD_LOGIC;
--  SIGNAL m_core_n_clk_o         : STD_LOGIC;
--  SIGNAL m_state_dbg_o          : STD_LOGIC_VECTOR(3 DOWNTO 0);
  
  SIGNAL m_di_i,m_do_o          : STD_LOGIC_VECTOR(31 DOWNTO 0);

 
begin
	
	spartan3_ver <= m_Spartan3_ver when Spartan3_valid = '1' else
					x"FFFFFFFF";
	
	process(CLOCK)
	begin 
		if rising_edge(CLOCK) then
			if ( RESET = '1' ) then
				m_wr_ack_o_d1 <= '0';
				m_wren_i <= '0';
				Spartan3_valid <= '0';
				Spartan3_State_s <= Spartan3_Init_c;
			else
				m_wr_ack_o_d1 <= m_wr_ack_o;
				case Spartan3_State_s is 
					
					when Spartan3_Init_c =>
						Spartan3_State_s <= Spartan3_Readid_c;
					
					when Spartan3_Readid_c =>
						Spartan3_State_s <= Spartan3_WaitReadidACK_c;
						m_wren_i <= '1';
						m_di_i <= '1' & '0' & std_logic_vector(to_unsigned(9,14)) & "00000000" & "00000000";
					
					when Spartan3_WaitReadidACK_c =>
						m_wren_i <= '0';
						if (m_wr_ack_o = '1' and m_wr_ack_o_d1 = '0') then
							Spartan3_State_s <= Spartan3_WaitReadiddone_c;
						else
							Spartan3_State_s <= Spartan3_WaitReadidACK_c;
						end if;	

					when Spartan3_WaitReadiddone_c =>
						if (m_do_valid_o = '1' ) then
							Spartan3_State_s <= Spartan3_Readver_c;
						else
							Spartan3_State_s <= Spartan3_WaitReadiddone_c;
						end if;					
						
					when Spartan3_Readver_c =>
						Spartan3_State_s <= Spartan3_WaitReadverACK_c;
						m_wren_i <= '1';
						m_di_i <= '1' & '0' & std_logic_vector(to_unsigned(11,14)) & "00000000" & "00000000";
					
					when Spartan3_WaitReadverACK_c =>
						m_wren_i <= '0';
						if (m_wr_ack_o = '1' and m_wr_ack_o_d1 = '0') then
							Spartan3_State_s <= Spartan3_WaitReadverdone_c;
						else
							Spartan3_State_s <= Spartan3_WaitReadverACK_c;
						end if;							
					
					when Spartan3_WaitReadverdone_c =>
						if (m_do_valid_o = '1' ) then
							if ( m_do_o = x"a55a5aa5") then
								Spartan3_valid <= '1';
							end if;
							Spartan3_State_s <= Spartan3_ReadLoopback_c;
						else
							Spartan3_State_s <= Spartan3_WaitReadverdone_c;
						end if;					

					when Spartan3_ReadLoopback_c =>
						Spartan3_State_s <= Spartan3_WaitReadLoopbackACK_c;
						m_wren_i <= '1';
						m_di_i <= '1' & '0' & std_logic_vector(to_unsigned(10,14)) & "00000000" & "00000000";
					
					when Spartan3_WaitReadLoopbackACK_c =>
						m_wren_i <= '0';
						if (m_wr_ack_o = '1' and m_wr_ack_o_d1 = '0') then
							Spartan3_State_s <= Spartan3_WaitReadLoopbackdone_c;
						else
							Spartan3_State_s <= Spartan3_WaitReadLoopbackACK_c;
						end if;							
					
					when Spartan3_WaitReadLoopbackdone_c =>
						if (m_do_valid_o = '1' ) then
							m_Spartan3_ver <= m_do_o;
							Spartan3_State_s <= Spartan3_Idle_c;
						else
							Spartan3_State_s <= Spartan3_WaitReadLoopbackdone_c;
						end if;											
					
					when Spartan3_Idle_c =>	
						m_wren_i <= '0';
						if ( (CH0_DSP_ANALOG_FILTER & CH0_RECEIVER_GAIN) /= CH0_Gain_Filter_Reg ) then
							CH0_Gain_Filter_Reg <= CH0_DSP_ANALOG_FILTER & CH0_RECEIVER_GAIN;
							Spartan3_State_s <= Spartan3_Send_CH0_Gain_Filter_c;
						elsif ( (CH1_DSP_ANALOG_FILTER&CH1_RECEIVER_GAIN) /= CH1_Gain_Filter_Reg ) then
							CH1_Gain_Filter_Reg <= CH1_DSP_ANALOG_FILTER & CH1_RECEIVER_GAIN;
							Spartan3_State_s <= Spartan3_Send_CH1_Gain_Filter_c;
						elsif ( Current_Frame /= Current_Frame_Reg ) then
							Spartan3_State_s <= Spartan3_SendCurrentFrame_c;
						else
							Spartan3_State_s <= Spartan3_Idle_c;
						end if;

					when Spartan3_Send_CH0_Gain_Filter_c => 
						Spartan3_State_s <= Spartan3_ConvGain1_c;
						Channel_Address <=  STD_LOGIC_VECTOR(to_unsigned(101, 14));
						Channel_Gain <=  signed(CH0_RECEIVER_GAIN);
						Channel_Filter <= CH0_DSP_ANALOG_FILTER(2 downto 0);

					when Spartan3_Send_CH1_Gain_Filter_c => 
						Spartan3_State_s <= Spartan3_ConvGain1_c;
						Channel_Address <=  STD_LOGIC_VECTOR(to_unsigned(201, 14));
						Channel_Gain <=  signed(CH1_RECEIVER_GAIN);
						Channel_Filter <= CH1_DSP_ANALOG_FILTER(2 downto 0);
						
					when Spartan3_ConvGain1_c =>
						Spartan3_State_s <= Spartan3_ConvGain2_c;
						if ( Channel_Gain > Max_Gain ) then
							Channel_Gain <= Max_Gain;
						elsif (  Channel_Gain < Min_Gain ) then
							Channel_Gain <= Min_Gain;
						else
							Channel_Gain <= Channel_Gain;
						end if;
							
					when Spartan3_ConvGain2_c =>
						Spartan3_State_s <= Spartan3_ConvGain3_c;
						Conv_Gain <= (Channel_Gain (18 downto 0) & '0') + (Channel_Gain(16 downto 0)&"000");	-- dB * 10
					
					when Spartan3_ConvGain3_c =>
						Spartan3_State_s <= Spartan3_SendGainFilt_c;
						Conv_Gain <= Conv_Gain + to_signed(276*1024, 20);
					
					when Spartan3_SendGainFilt_c =>
						Spartan3_State_s <= Spartan3_WaitGainFiltACK_c;
						m_wren_i <= '1';
						m_di_i <= '0' & '0' & Channel_Address & "000" & Channel_Filter & Std_logic_vector( Conv_Gain(19 downto 10) );
					
					when Spartan3_WaitGainFiltACK_c =>
						m_wren_i <= '0';
						if (m_wr_ack_o = '1' and m_wr_ack_o_d1 = '0') then
							Spartan3_State_s <= Spartan3_Idle_c;
						else
							Spartan3_State_s <= Spartan3_WaitGainFiltACK_c;
						end if;					
					
					when Spartan3_SendCurrentFrame_c =>
						Spartan3_State_s <= Spartan3_WaitCurrentFrameACK_c;
						m_wren_i <= '1';
						m_di_i <= '0' & '0' & std_logic_vector(to_unsigned(6,14)) & "00000000" & Current_Frame;
						
					when Spartan3_WaitCurrentFrameACK_c =>
						m_wren_i <= '0';
						if (m_wr_ack_o = '1' and m_wr_ack_o_d1 = '0') then
							Current_Frame_Reg <= Current_Frame;
							Spartan3_State_s <= Spartan3_Idle_c;
						else
							Spartan3_State_s <= Spartan3_WaitCurrentFrameACK_c;
						end if;						

				end case;
			end if;
		end if;
	end process; 
 
     Inst_spi_master_spartan3: spi_master_spartan3
        generic map (N => 32, CPOL => '0', CPHA => '0', PREFETCH => 3, SPI_2X_CLK_DIV => 1)
        port map( 
            sclk_i => CLOCK,                      -- system clock is used for serial and parallel ports
            pclk_i => CLOCK,
            rst_i => RESET,
            spi_ssel_o => RCVR_FPGA_CS,
            spi_sck_o => RCVR_FPGA_SCLK,
            spi_mosi_o => RCVR_FPGA_SDI,
            spi_miso_i => RCVR_FPGA_SDO,
            di_req_o => m_di_req_o,
            di_i => m_di_i,
            wren_i => m_wren_i,
            wr_ack_o => m_wr_ack_o,
            do_valid_o => m_do_valid_o,
            do_o => m_do_o
            ----- debug -----
--            do_transfer_o => m_do_transfer_o,
--            wren_o => m_wren_o,
--            rx_bit_reg_o => m_rx_bit_reg_o,
--            state_dbg_o => m_state_dbg_o,
--            core_clk_o => m_core_clk_o,
--            core_n_clk_o => m_core_n_clk_o
--            sh_reg_dbg_o => m_sh_reg_dbg_o
        );   
  
end spartan3_gain_filt_dac_arch;