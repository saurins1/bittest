clear all;close all;

% ASCAN
fname = 'tgil_ascan.json'; 
fid = fopen(fname); 
raw = fread(fid,inf); 
str = char(raw'); 
fclose(fid); 
json_struct = jsondecode(str);

ascan = json_struct.ascan;
figure(1)
plot(ascan);

file_ascan = fopen('ascan.dat','w');
fwrite(file_ascan,ascan,'int64');
fclose(file_ascan);

% CONFIG 
config = csvread('tgil_configuration.csv',1,2);
config = [0 config'];

file_config = fopen('configuration.dat','w');
fwrite(file_config,config,'int64');
fclose(file_config);