library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity digital_amplifier_tb is
	generic (
  		REGISTER_W48        : INTEGER    := 48;
        REGISTER_W32        : INTEGER    := 32;
        REGISTER_W16        : INTEGER    := 16;
  		DIVIDER_FACTOR      : INTEGER    := 10;
  		MULTIPLIER_DELAY    : INTEGER    := 4
	);
end;

architecture bench of digital_amplifier_tb is

  	component digital_amplifier
		generic (
  			REGISTER_W48        : INTEGER    := 48;
        	REGISTER_W32        : INTEGER    := 32;
        	REGISTER_W16        : INTEGER    := 16;
  			DIVIDER_FACTOR      : INTEGER    := 10;
  			MULTIPLIER_DELAY    : INTEGER    := 4
      	);
      	port (
			aclk         : in std_logic;
			aresetn      : in std_logic;
			DIGITAL_AMPLIFIER_GAIN		: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			SIGNAL_INPUT_AXIS_TREADY 	: out STD_LOGIC;
			SIGNAL_INPUT_AXIS_TDATA 	: in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
			SIGNAL_INPUT_AXIS_TLAST		: in STD_LOGIC;
			SIGNAL_INPUT_AXIS_TVALID  	: in STD_LOGIC;
			SIGNAL_OUTPUT_AXIS_TREADY 	: in STD_LOGIC;
			SIGNAL_OUTPUT_AXIS_TDATA	: out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
			SIGNAL_OUTPUT_AXIS_TLAST	: out STD_LOGIC;
			SIGNAL_OUTPUT_AXIS_TVALID   : out STD_LOGIC		
      	);
  end component;

  signal aclk: std_logic:= '0';
  signal aresetn: std_logic:= '0';

  signal DIGITAL_AMPLIFIER_GAIN: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');

  signal SIGNAL_INPUT_AXIS_TREADY: STD_LOGIC;
  signal SIGNAL_INPUT_AXIS_TDATA: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0');
  signal SIGNAL_INPUT_AXIS_TLAST: STD_LOGIC:= '0';
  signal SIGNAL_INPUT_AXIS_TVALID: STD_LOGIC:= '0';

  signal SIGNAL_OUTPUT_AXIS_TREADY: STD_LOGIC:= '0';
  signal SIGNAL_OUTPUT_AXIS_TDATA: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  signal SIGNAL_OUTPUT_AXIS_TLAST: STD_LOGIC;
  signal SIGNAL_OUTPUT_AXIS_TVALID: STD_LOGIC ;

  signal DATA_WINDOW: std_logic_vector(REGISTER_W32-1 downto 0):= (others => '0');

  constant aclk_period: time := 10 ns;

  type states_signal_sm is (idle, load_signal, load_signal_wait, load_signal_end); 
  signal state_signal_sm : states_signal_sm; 

  signal start_sm: STD_LOGIC:= '0';

  signal decimation_level: std_logic_vector(REGISTER_W32-1 downto 0):= (others => '0');

begin

    signal_input_inst: process(aclk, aresetn)  
        variable my_counter  : integer range 0 to 16383 :=0;
		variable signal_counter  : integer range 0 to 16383 :=0;  
		variable decimation_counter  : integer range 0 to 16383 :=0; 
    begin 
        if (aresetn = '0') then
			-- config
			DIGITAL_AMPLIFIER_GAIN	<= std_logic_vector(to_unsigned(2048, DIGITAL_AMPLIFIER_GAIN'length));  
			DATA_WINDOW	<= std_logic_vector(to_unsigned(1024, DIGITAL_AMPLIFIER_GAIN'length));  															
			-- SIGNAL INPUT AXIS
			SIGNAL_INPUT_AXIS_TDATA  <= (others => '0');
			SIGNAL_INPUT_AXIS_TLAST  <= '0';
			SIGNAL_INPUT_AXIS_TVALID <= '0';
														
			SIGNAL_OUTPUT_AXIS_TREADY <= '0';													
			-- decimation
			--decimation_level <= std_logic_vector(to_unsigned(1, decimation_level'length));
			decimation_level <= std_logic_vector(to_unsigned(4, decimation_level'length));																  
			-- state																		  	
            state_signal_sm <= idle;  
        elsif (aclk'event and aclk = '1') then
            case state_signal_sm is  
                when idle =>         
                    if(start_sm = '1') then
                        my_counter := 0;
						signal_counter := 1;									 
						SIGNAL_OUTPUT_AXIS_TREADY <= '1';													 
                        state_signal_sm <= load_signal;        
                    end if;
                when load_signal =>
                    if(SIGNAL_INPUT_AXIS_TREADY = '1') then
                		if(my_counter <= to_integer(unsigned(DATA_WINDOW))-1) then
							-- tdata										 
							SIGNAL_INPUT_AXIS_TDATA <= std_logic_vector(to_unsigned(signal_counter, SIGNAL_INPUT_AXIS_TDATA'length)); 
							if(signal_counter >= 8) then
								signal_counter := 1;								 
							else
								signal_counter := signal_counter +1;								 
							end if;	
							-- tvalid														
							SIGNAL_INPUT_AXIS_TVALID <= '1';	
							-- tlast													
							if(my_counter = to_integer(unsigned(DATA_WINDOW))-1) then
								SIGNAL_INPUT_AXIS_TLAST <= '1';
							end if;
							-- decimation															
							if(unsigned(decimation_level) > 1) then	
								decimation_counter := 0;	
								state_signal_sm <= load_signal_wait;															
							end if;
							my_counter := my_counter + 1; 
						else
							SIGNAL_INPUT_AXIS_TVALID <= '0';
							SIGNAL_INPUT_AXIS_TLAST <= '0';													
							my_counter  := 0;
							state_signal_sm    <= load_signal_end;	
                        end if;      
                    end if;																		
				when load_signal_wait =>
					SIGNAL_INPUT_AXIS_TVALID <= '0';
					SIGNAL_INPUT_AXIS_TLAST <= '0';																
					if(decimation_counter >= unsigned(decimation_level)-2) then
						state_signal_sm <= load_signal; 																
					else
						decimation_counter := decimation_counter + 1;																
					end if;																																
                when load_signal_end =>
					SIGNAL_OUTPUT_AXIS_TREADY <= '0';																	
                    if(my_counter >= 1024) then
                        my_counter  := 0;
                        state_signal_sm <= idle;
                    else
                        my_counter := my_counter + 1;
                    end if;   
                when others =>
                    null;                   
            end case;                                                                                            
        end if;
    end process; 

	stimulus: process
	begin
		aresetn <= '0';
		wait for aclk_period*10;
		aresetn <= '1';
		wait for aclk_period*10;
		start_sm <= '1';
		wait for aclk_period*10;
		--start_sm <= '0';
		wait;
	end process;

	-- aclk process 
    aclk_process :process
    begin
        aclk <= not aclk;
        wait for aclk_period/2;
        aclk <= not aclk;
        wait for aclk_period/2;
    end process;
			
  -- Insert values for generic parameters !!
  uut: digital_amplifier generic map ( REGISTER_W48           => REGISTER_W48,
                                       REGISTER_W32           => REGISTER_W32,
                                       REGISTER_W16           => REGISTER_W16,
                                       DIVIDER_FACTOR         => DIVIDER_FACTOR,
                                       MULTIPLIER_DELAY       => MULTIPLIER_DELAY )
                            port map ( aclk                   => aclk,
                                       aresetn                => aresetn,
                                       DIGITAL_AMPLIFIER_GAIN => DIGITAL_AMPLIFIER_GAIN,
                                       SIGNAL_INPUT_AXIS_TREADY  => SIGNAL_INPUT_AXIS_TREADY,
                                       SIGNAL_INPUT_AXIS_TDATA   => SIGNAL_INPUT_AXIS_TDATA,
                                       SIGNAL_INPUT_AXIS_TLAST   => SIGNAL_INPUT_AXIS_TLAST,
                                       SIGNAL_INPUT_AXIS_TVALID  => SIGNAL_INPUT_AXIS_TVALID,
                                       SIGNAL_OUTPUT_AXIS_TREADY => SIGNAL_OUTPUT_AXIS_TREADY,
                                       SIGNAL_OUTPUT_AXIS_TDATA  => SIGNAL_OUTPUT_AXIS_TDATA,
                                       SIGNAL_OUTPUT_AXIS_TLAST  => SIGNAL_OUTPUT_AXIS_TLAST,
                                       SIGNAL_OUTPUT_AXIS_TVALID => SIGNAL_OUTPUT_AXIS_TVALID );

end;