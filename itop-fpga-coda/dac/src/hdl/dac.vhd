----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 01/16/2017 10:07:23 AM
-- Design Name: 
-- Module Name: dac - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity dac is
	generic (
	    REGISTER_W32    : INTEGER := 32;
		REGISTER_W10    : INTEGER := 10
	);
	port (
	    -- Sync
	    aclk_1 	: in STD_LOGIC;
	    aclk_2 	: in STD_LOGIC;
        aresetn	: in STD_LOGIC;
		
		-- Input
		RECEIVER_ANALOG_GAIN	: in STD_LOGIC_VECTOR(REGISTER_W10-1 downto 0);	
		START_INSPECTION		: in STD_LOGIC;
		DATA_WINDOW_ON		    : in STD_LOGIC;
		
		-- Config DAC curves
        CONFIG_DAC_AXIS_TREADY   : out STD_LOGIC;
        CONFIG_DAC_AXIS_TDATA    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        CONFIG_DAC_AXIS_TLAST    : in STD_LOGIC;
        CONFIG_DAC_AXIS_TVALID   : in STD_LOGIC;
		
		-- Output
		DAC_GAIN  	: out STD_LOGIC_VECTOR(REGISTER_W10-1 downto 0);  		
		DAC_CLK   	: out STD_LOGIC; 
		DAC_SLEEP 	: out STD_LOGIC             
	);
end dac;

architecture arch_imp of dac is
	
	signal receiver_analog_gain_i : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	
	type tgc_array is array ( 0 to 131 ) of STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);     
    signal tgc_array_config : tgc_array;
    signal tgc_on : STD_LOGIC; 
    
    signal sample_counter : UNSIGNED(REGISTER_W32-1 downto 0);
    signal target_time : UNSIGNED(REGISTER_W32-1 downto 0);
    signal target_gain : SIGNED(REGISTER_W32-1 downto 0);
    signal dac_start  : STD_LOGIC; 
    signal dac_update  : STD_LOGIC; 
    signal actual_taps : UNSIGNED(REGISTER_W32-1 downto 0);
    signal taps_counter : UNSIGNED(REGISTER_W32-1 downto 0);
	       
begin
	
	-- CLK
	DAC_CLK <= aclk_2;
	
	-- SLEEP
	--DAC_SLEEP <= not START_INSPECTION;
	DAC_SLEEP <= '0';

	-- GAIN	
	--DAC_GAIN <= receiver_analog_gain_i(REGISTER_W10-1 downto 0) when tgc_on = '0' else
	 --           receiver_analog_gain_i(REGISTER_W10-1+16 downto 16);
	            
    --  Gain																			   
    update_gain_inst: process(aclk_2, aresetn)
    begin 
        if (rising_edge(aclk_2)) then
            if (aresetn = '0') then
                DAC_GAIN <= (others => '0'); 
            else
                if(tgc_on = '0') then
                    DAC_GAIN <= receiver_analog_gain_i(REGISTER_W10-1 downto 0);
                else
                    DAC_GAIN <= receiver_analog_gain_i(REGISTER_W10-1+16 downto 16);
                end if; 
            end if;
        end if;
    end process;

	-- DAC																			   
    dac_inst: process(aclk_1, aresetn)
		variable tgc_index  : integer range 0 to 131071 :=0; 
    begin 
		if (rising_edge(aclk_1)) then
            if (aresetn = '0') then
   				receiver_analog_gain_i 	<= (others => '0'); 
   				sample_counter <= (others => '0'); 
   				tgc_index := 0;
  
   				taps_counter<= (others => '0');            
   				target_time <= (others => '0');
   				target_gain <= (others => '0');			
   				dac_start <= '1';
   				dac_update <= '0';
            else
                if(tgc_on = '1') then
                    --receiver_analog_gain_i <= RECEIVER_ANALOG_GAIN;
                    if(DATA_WINDOW_ON = '1') then
                        if(sample_counter >= target_time) then                           
                            if(taps_counter <= actual_taps) then
                                dac_update <= '1';
                                if(dac_start = '1') then 
                                    dac_start <= '0';
                                else
                                end if;
                                target_time <= sample_counter + unsigned(tgc_array_config(tgc_index));                           
                                target_gain <= signed(tgc_array_config(tgc_index+1));
                                tgc_index := tgc_index + 2;
                                taps_counter <= taps_counter + 1;
                            else
                                dac_update <= '0';
                            end if;
                        else
                            dac_update <= '0';
                        end if;
                        if(dac_start = '0') then
                            receiver_analog_gain_i <= std_logic_vector(signed(receiver_analog_gain_i) + target_gain);
                        else
                            receiver_analog_gain_i <= std_logic_vector(target_gain);
                        end if; 
                        sample_counter <= sample_counter + 1;
                    else
                        receiver_analog_gain_i <= tgc_array_config(3);
                        sample_counter <= (others => '0');
                        tgc_index := 4;
                        taps_counter<= (others => '0');                    
                        target_time <= unsigned(tgc_array_config(2));
                        target_gain <= signed(tgc_array_config(3));
                        dac_start <= '1';
                        dac_update <= '0';
                    end if;
                else
                    receiver_analog_gain_i <= std_logic_vector(resize(unsigned(RECEIVER_ANALOG_GAIN), receiver_analog_gain_i'length)); 
                    sample_counter <= (others => '0');
                    tgc_index := 4;
                    taps_counter<= (others => '0');                 
   				    target_time <= (others => '0');
                    target_gain <= (others => '0');            
                    dac_start <= '1';
                    dac_update <= '0';
                end if;
            end if;
        end if;
    end process;
    
	-- Update gain																			   
    store_tfc_inst: process(aclk_1, aresetn)
        variable my_counter  : integer range 0 to 65535 :=0; 
    begin 
        if (rising_edge(aclk_1)) then
            if (aresetn = '0') then
                tgc_on <= '0';
                CONFIG_DAC_AXIS_TREADY <= '0';
                actual_taps <= (others => '0');
                tgc_array_config <= (others => (others => '0'));
                my_counter := 0;
            else
                CONFIG_DAC_AXIS_TREADY <= '1';  
                if(unsigned(tgc_array_config(0)) > 0) then
                    if(CONFIG_DAC_AXIS_TVALID = '0') then
                        tgc_on <= '1';
                        actual_taps <= unsigned(tgc_array_config(0));
                    end if;
                else
                    tgc_on <= '0';
                end if;
                if(CONFIG_DAC_AXIS_TVALID = '1') then
                    tgc_array_config(my_counter) <= CONFIG_DAC_AXIS_TDATA;
                    if(CONFIG_DAC_AXIS_TLAST = '1') then
                        my_counter := 0;
                    else
                        my_counter := my_counter + 1;
                    end if;
                else
                    my_counter := 0;
                end if;
            end if;
        end if;
    end process;
																					   					     					      									 	   			                 
end arch_imp;
