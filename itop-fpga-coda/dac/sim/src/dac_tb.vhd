library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity dac_tb is
	generic (
  	    REGISTER_W32    : INTEGER := 32;
  		REGISTER_W10    : INTEGER := 10; 
  		WAIT_CYCLES     : INTEGER := 128
  	);
end;

architecture bench of dac_tb is

    component dac is
        generic (
            REGISTER_W32    : INTEGER := 32;
            REGISTER_W10    : INTEGER := 10
        );
        port (
            -- Sync
            aclk_1 	: in STD_LOGIC;
            aclk_2 	: in STD_LOGIC;
            aresetn	: in STD_LOGIC;
            
            -- Input
            RECEIVER_ANALOG_GAIN	: in STD_LOGIC_VECTOR(REGISTER_W10-1 downto 0);	
            START_INSPECTION		: in STD_LOGIC;
            DATA_WINDOW_ON		    : in STD_LOGIC;
            
            -- Config DAC curves
            CONFIG_DAC_AXIS_TREADY   : out STD_LOGIC;
            CONFIG_DAC_AXIS_TDATA    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            CONFIG_DAC_AXIS_TLAST    : in STD_LOGIC;
            CONFIG_DAC_AXIS_TVALID   : in STD_LOGIC;
            
            -- Output
            DAC_GAIN  	: out STD_LOGIC_VECTOR(REGISTER_W10-1 downto 0);  		
            DAC_CLK   	: out STD_LOGIC; 
            DAC_SLEEP 	: out STD_LOGIC             
        );
    end component dac;

  signal aclk_1: std_logic:= '0';
  signal aclk_2: std_logic:= '0';
  signal aresetn: std_logic:= '0';

  signal RECEIVER_ANALOG_GAIN: STD_LOGIC_VECTOR(REGISTER_W10-1 downto 0) := (others => '0');
  signal START_INSPECTION: STD_LOGIC := '0';
  signal DATA_WINDOW_ON: STD_LOGIC := '0';
  
            -- Config DAC curves
  signal CONFIG_DAC_AXIS_TREADY   : STD_LOGIC;
  signal CONFIG_DAC_AXIS_TDATA    : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0) := (others => '0');
  signal CONFIG_DAC_AXIS_TLAST    : STD_LOGIC;
  signal CONFIG_DAC_AXIS_TVALID   : STD_LOGIC;

  signal DAC_GAIN: STD_LOGIC_VECTOR(REGISTER_W10-1 downto 0);
  signal DAC_CLK: STD_LOGIC;
  signal DAC_SLEEP: STD_LOGIC ;

  constant aclk_period_1: time := 10 ns;
  constant aclk_period_2: time := 20 ns;
  
  type tgc_array is array ( 0 to 131 ) of STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);     
  signal tgc_array_config : tgc_array := (others => (others => '0'));
  
  signal dac_size: std_logic_vector(REGISTER_W32-1 downto 0):= (others => '0');
  
  type states_dac_sm is (idle, load_dac, load_dac_end); 
  signal state_dac_sm : states_dac_sm; 
  signal start_sm: STD_LOGIC:= '0';

begin

    dac_input_inst: process(aclk_1, aresetn)  
        variable my_counter  : integer range 0 to 16383 :=0; 
		variable decimation_counter  : integer range 0 to 16383 :=0; 
    begin 
        if (aresetn = '0') then
			-- config
			RECEIVER_ANALOG_GAIN	<= std_logic_vector(to_unsigned(64, RECEIVER_ANALOG_GAIN'length));  
			CONFIG_DAC_AXIS_TDATA  <= (others => '0');
			CONFIG_DAC_AXIS_TLAST  <= '0';
			CONFIG_DAC_AXIS_TVALID <= '0';
			START_INSPECTION <= '0';																										  
			-- state																		  	
            state_dac_sm <= idle;  
        elsif (aclk_1'event and aclk_1 = '1') then
            case state_dac_sm is  
                when idle =>         
                    if(start_sm = '1') then
                        my_counter := 0;								 												 
                        state_dac_sm <= load_dac;        
                    end if;
                when load_dac =>
                    if(CONFIG_DAC_AXIS_TREADY = '1') then
                		if(my_counter <= to_integer(unsigned(dac_size))-1) then
							-- tdata										 
							CONFIG_DAC_AXIS_TDATA <= tgc_array_config(my_counter); 
							-- tvalid														
							CONFIG_DAC_AXIS_TVALID <= '1';	
							-- tlast													
							if(my_counter = to_integer(unsigned(dac_size))-1) then
								CONFIG_DAC_AXIS_TLAST <= '1';
							end if;
							my_counter := my_counter + 1; 
						else
							CONFIG_DAC_AXIS_TVALID <= '0';
							CONFIG_DAC_AXIS_TLAST <= '0';													
							my_counter  := 0;
							state_dac_sm    <= load_dac_end;	
                        end if;      
                    end if;																																																	
                when load_dac_end =>
                    START_INSPECTION <= '1';																
                    if(my_counter >= 1024) then
                        my_counter  := 0;
                        state_dac_sm <= idle;
                    else
                        my_counter := my_counter + 1;
                    end if;   
                when others =>
                    null;                   
            end case;                                                                                            
        end if;
    end process; 
	
    update_data_window_inst: process(aclk_1, aresetn)
		variable my_counter  : integer range 0 to 1000000 :=0; 
    begin 
		if (rising_edge(aclk_1)) then
            if (aresetn = '0') then
				DATA_WINDOW_ON <= '0';
            else
				if(my_counter >= 16384) then
					DATA_WINDOW_ON <= not DATA_WINDOW_ON;
					my_counter := 0;
				else
					my_counter := my_counter + 1;	
				end if;
            end if;
        end if;
    end process;
	
	stimulus: process
    begin
        aresetn <= '0';
        wait for aclk_period_1*10;
        aresetn <= '1';
        wait for aclk_period_1*10;
        start_sm <= '1';
        wait for aclk_period_1*10;
        --start_sm <= '0';
        wait;
    end process;
	
	-- aclk process 
    aclk1_process :process
    begin
        aclk_1 <= not aclk_1;
        wait for aclk_period_1/2;
        aclk_1 <= not aclk_1;
        wait for aclk_period_1/2;
    end process;
    
	-- aclk process 
    aclk2_process :process
    begin
        aclk_2 <= not aclk_2;
        wait for aclk_period_2/2;
        aclk_2 <= not aclk_2;
        wait for aclk_period_2/2;
    end process;

  -- Insert values for generic parameters !!
  uut: dac generic map ( REGISTER_W32           => REGISTER_W32,
                         REGISTER_W10           => REGISTER_W10)
              port map ( aclk_1                 => aclk_1,
                         aclk_2                 => aclk_2,
                         aresetn                => aresetn,
                         RECEIVER_ANALOG_GAIN   => RECEIVER_ANALOG_GAIN,
                         START_INSPECTION       => START_INSPECTION,
                         DATA_WINDOW_ON         => DATA_WINDOW_ON,
                         CONFIG_DAC_AXIS_TREADY => CONFIG_DAC_AXIS_TREADY,
                         CONFIG_DAC_AXIS_TDATA  => CONFIG_DAC_AXIS_TDATA,
                         CONFIG_DAC_AXIS_TLAST  => CONFIG_DAC_AXIS_TLAST,
                         CONFIG_DAC_AXIS_TVALID => CONFIG_DAC_AXIS_TVALID,
                         DAC_GAIN               => DAC_GAIN,
                         DAC_CLK                => DAC_CLK,
                         DAC_SLEEP              => DAC_SLEEP );
                         
  tgc_array_config(0) <= std_logic_vector(to_unsigned(30, tgc_array_config(0)'length)); 
  tgc_array_config(1) <= std_logic_vector(to_unsigned(0, tgc_array_config(0)'length)); 
  tgc_array_config(2) <= std_logic_vector(to_unsigned(0, tgc_array_config(0)'length)); 
  tgc_array_config(3) <= std_logic_vector(to_unsigned(100*65536, tgc_array_config(0)'length)); 
  tgc_array_config(4) <= std_logic_vector(to_unsigned(0, tgc_array_config(0)'length)); 
  tgc_array_config(5) <= std_logic_vector(to_unsigned(0, tgc_array_config(0)'length)); 
  tgc_array_config(6) <= std_logic_vector(to_unsigned(100, tgc_array_config(0)'length)); 
  tgc_array_config(7) <= std_logic_vector(to_unsigned(1*65536, tgc_array_config(0)'length)); 
  tgc_array_config(8) <= std_logic_vector(to_unsigned(100, tgc_array_config(0)'length)); 
  tgc_array_config(9) <= std_logic_vector(to_unsigned(1*65536, tgc_array_config(0)'length)); 
  tgc_array_config(10) <= std_logic_vector(to_unsigned(100, tgc_array_config(0)'length)); 
  tgc_array_config(11) <= std_logic_vector(to_unsigned(1*65536, tgc_array_config(0)'length)); 
  tgc_array_config(12) <= std_logic_vector(to_unsigned(100, tgc_array_config(0)'length)); 
  tgc_array_config(13) <= std_logic_vector(to_signed(1*65536, tgc_array_config(0)'length)); 
  tgc_array_config(14) <= std_logic_vector(to_unsigned(100, tgc_array_config(0)'length)); 
  tgc_array_config(15) <= std_logic_vector(to_unsigned(1*65536, tgc_array_config(0)'length)); 
  tgc_array_config(16) <= std_logic_vector(to_unsigned(100, tgc_array_config(0)'length)); 
  tgc_array_config(17) <= std_logic_vector(to_unsigned(1*65536, tgc_array_config(0)'length)); 
  tgc_array_config(18) <= std_logic_vector(to_unsigned(100, tgc_array_config(0)'length)); 
  tgc_array_config(19) <= std_logic_vector(to_unsigned(1*65536, tgc_array_config(0)'length)); 
  tgc_array_config(20) <= std_logic_vector(to_unsigned(100, tgc_array_config(0)'length)); 
  tgc_array_config(21) <= std_logic_vector(to_unsigned(1*65536, tgc_array_config(0)'length)); 
  tgc_array_config(22) <= std_logic_vector(to_unsigned(100, tgc_array_config(0)'length)); 
  tgc_array_config(23) <= std_logic_vector(to_signed(1*65536, tgc_array_config(0)'length)); 
  tgc_array_config(24) <= std_logic_vector(to_unsigned(100, tgc_array_config(0)'length)); 
  tgc_array_config(25) <= std_logic_vector(to_signed(-65536, tgc_array_config(0)'length)); 
  tgc_array_config(26) <= std_logic_vector(to_unsigned(100, tgc_array_config(0)'length)); 
  tgc_array_config(27) <= std_logic_vector(to_signed(-65536, tgc_array_config(0)'length)); 
  tgc_array_config(28) <= std_logic_vector(to_unsigned(100, tgc_array_config(0)'length)); 
  tgc_array_config(29) <= std_logic_vector(to_signed(-65536, tgc_array_config(0)'length)); 
  tgc_array_config(30) <= std_logic_vector(to_unsigned(100, tgc_array_config(0)'length)); 
  tgc_array_config(31) <= std_logic_vector(to_signed(-65536, tgc_array_config(0)'length)); 
  tgc_array_config(32) <= std_logic_vector(to_unsigned(100, tgc_array_config(0)'length)); 
  tgc_array_config(33) <= std_logic_vector(to_signed(-65536, tgc_array_config(0)'length)); 
  tgc_array_config(34) <= std_logic_vector(to_unsigned(100, tgc_array_config(0)'length)); 
  tgc_array_config(35) <= std_logic_vector(to_signed(-65536, tgc_array_config(0)'length)); 
  tgc_array_config(36) <= std_logic_vector(to_unsigned(100, tgc_array_config(0)'length)); 
  tgc_array_config(37) <= std_logic_vector(to_signed(-65536, tgc_array_config(0)'length));  
  tgc_array_config(38) <= std_logic_vector(to_unsigned(100, tgc_array_config(0)'length)); 
  tgc_array_config(39) <= std_logic_vector(to_signed(-65536, tgc_array_config(0)'length)); 
  tgc_array_config(40) <= std_logic_vector(to_unsigned(100, tgc_array_config(0)'length)); 
  tgc_array_config(41) <= std_logic_vector(to_signed(-65536, tgc_array_config(0)'length)); 
  tgc_array_config(42) <= std_logic_vector(to_unsigned(100, tgc_array_config(0)'length)); 
  tgc_array_config(43) <= std_logic_vector(to_signed(-65536, tgc_array_config(0)'length)); 
  tgc_array_config(44) <= std_logic_vector(to_unsigned(200, tgc_array_config(0)'length)); 
  tgc_array_config(45) <= std_logic_vector(to_unsigned(2*65536, tgc_array_config(0)'length)); 
  tgc_array_config(46) <= std_logic_vector(to_unsigned(300, tgc_array_config(0)'length)); 
  tgc_array_config(47) <= std_logic_vector(to_unsigned(2*65536, tgc_array_config(0)'length)); 
  tgc_array_config(48) <= std_logic_vector(to_unsigned(400, tgc_array_config(0)'length)); 
  tgc_array_config(49) <= std_logic_vector(to_unsigned(2*65536, tgc_array_config(0)'length)); 
  tgc_array_config(50) <= std_logic_vector(to_unsigned(500, tgc_array_config(0)'length)); 
  tgc_array_config(51) <= std_logic_vector(to_unsigned(2*65536, tgc_array_config(0)'length)); 
  tgc_array_config(52) <= std_logic_vector(to_unsigned(400, tgc_array_config(0)'length)); 
  tgc_array_config(53) <= std_logic_vector(to_unsigned(2*65536, tgc_array_config(0)'length)); 
  tgc_array_config(54) <= std_logic_vector(to_unsigned(300, tgc_array_config(0)'length)); 
  tgc_array_config(55) <= std_logic_vector(to_unsigned(2*65536, tgc_array_config(0)'length)); 
  tgc_array_config(56) <= std_logic_vector(to_unsigned(200, tgc_array_config(0)'length)); 
  tgc_array_config(57) <= std_logic_vector(to_unsigned(2*65536, tgc_array_config(0)'length)); 
  tgc_array_config(58) <= std_logic_vector(to_unsigned(100, tgc_array_config(0)'length)); 
  tgc_array_config(59) <= std_logic_vector(to_unsigned(2*65536, tgc_array_config(0)'length)); 
  tgc_array_config(60) <= std_logic_vector(to_unsigned(200, tgc_array_config(0)'length)); 
  tgc_array_config(61) <= std_logic_vector(to_unsigned(2*65536, tgc_array_config(0)'length)); 
  tgc_array_config(62) <= std_logic_vector(to_unsigned(300, tgc_array_config(0)'length)); 
  tgc_array_config(63) <= std_logic_vector(to_unsigned(2*65536, tgc_array_config(0)'length)); 
  
  
  
  dac_size <= std_logic_vector(to_unsigned(64, tgc_array_config(0)'length)); 
  

end;