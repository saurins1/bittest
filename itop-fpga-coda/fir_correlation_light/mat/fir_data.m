clear all;close all;

%signal_size = 1024;
%tap_size = 64;
signal_size = 1024;
tap_size = 64;
tap_sets = 3;

signal_fir = zeros(1,signal_size);

taps_fir = zeros(1,tap_size*tap_sets);

signal_dir = 0;
signal_amplitude = 10;
signal_value = 0;

% Create signal
for n = 1:signal_size
    counter_dir = 0;
    if(signal_dir == 0)
        signal_fir(n) = signal_value + 1;
        signal_value  = signal_value + 1;
        if(signal_value >= signal_amplitude)
            signal_dir = 1;
        end
    else
        signal_fir(n) = signal_value - 1;
        signal_value  = signal_value - 1;
        if(abs(signal_value) >= signal_amplitude)
            signal_dir = 0;
        end
    end
end

file_signal_fir = fopen('signal_fir.dat','w');
fwrite(file_signal_fir,signal_fir,'int64');
fclose(file_signal_fir);
figure(1);
plot(signal_fir);

n_aux = 0;
tap_dir = 0;
tap_size_aux = 8;
taps_fir_aux = zeros(1,tap_size_aux);
% Create taps
for n = 1:tap_size_aux
    if(tap_dir == 0)
        if(n_aux >= tap_size_aux/2)
           tap_dir = 1; 
        else
           n_aux = n_aux + 1;
        end
    else
        if(n_aux <= 1)
            tap_dir = 0;
        else
            n_aux = n_aux - 1;
        end
    end
    taps_fir_aux(n) = n_aux;
end

% taps_fir = [taps_fir_aux -taps_fir_aux -taps_fir_aux taps_fir_aux];
% taps_fir = [-taps_fir_aux taps_fir_aux taps_fir_aux -taps_fir_aux];
taps_fir = [taps_fir_aux -taps_fir_aux -taps_fir_aux taps_fir_aux taps_fir_aux -taps_fir_aux -taps_fir_aux taps_fir_aux];
taps_fir = [taps_fir taps_fir taps_fir];
figure(2);
plot(taps_fir);
file_taps_fir = fopen('taps_fir.dat','w');
fwrite(file_taps_fir,taps_fir,'int64');
fclose(file_taps_fir);

% FIR oversampling 1

signal_fir_1 = [zeros(1,tap_size/2-1) signal_fir zeros(1,tap_size/2)];
fir_result_1 = zeros(1,signal_size);

for n = 1:signal_size
   %fir_result(n) = signal_fir_x(n:n+tap_size-1) * taps_fir(1:tap_size)';
   fir_result_1(n) = dot(signal_fir_1(n:n+tap_size-1),taps_fir(1:tap_size));
end
fir_result_1 = (fir_result_1/8);
fir_result_1 = floor(fir_result_1);

figure(3);
plot(fir_result_1);

signal_fir_2 = [zeros(1,tap_size/2-1) fir_result_1 zeros(1,tap_size/2)];
fir_result_2 = zeros(1,signal_size);

for n = 1:signal_size
   %fir_result(n) = signal_fir_x(n:n+tap_size-1) * taps_fir(1:tap_size)';
   fir_result_2(n) = dot(signal_fir_2(n:n+tap_size-1),taps_fir(1:tap_size));
end
fir_result_2 = (fir_result_2/8);
fir_result_2 = floor(fir_result_2);
figure(4);
plot(fir_result_2);

signal_fir_3 = [zeros(1,tap_size/2-1) abs(fir_result_2) zeros(1,tap_size/2)];
fir_result_3 = zeros(1,signal_size);

for n = 1:signal_size
   %fir_result(n) = signal_fir_x(n:n+tap_size-1) * taps_fir(1:tap_size)';
   fir_result_3(n) = dot(signal_fir_3(n:n+tap_size-1),taps_fir(1:tap_size));
end
fir_result_3 =(fir_result_3/8);
fir_result_3 =floor(fir_result_3);
figure(5);
plot(fir_result_3);

% CORRELATION

correlation_start = 257;
correlation_end = 517;
correlation_size = correlation_end - correlation_start + 1;

correlation_result_1x = xcorr(fir_result_1(correlation_start:correlation_end));
correlation_result_1 = correlation_result_1x(correlation_size:2*correlation_size-1);

correlation_result_2x = xcorr(fir_result_2(correlation_start:correlation_end));
correlation_result_2 = correlation_result_2x(correlation_size:2*correlation_size-1);

correlation_result_3x = xcorr(fir_result_3(correlation_start:correlation_end));
correlation_result_3 = correlation_result_3x(correlation_size:2*correlation_size-1);

figure(6);
plot(correlation_result_3);

% for n = 1:correlation_size
%     correlation_result_3(n) = fir_result_1(correlation_start:correlation_size+1-n)*fir_result_1(correlation_start+n-1:correlation_end)';
% end



