----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/10/2017 11:40:52 PM
-- Design Name: 
-- Module Name: dsp_test_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity dsp_test_tb is
--  Port ( );
end dsp_test_tb;

architecture Behavioral of dsp_test_tb is

    COMPONENT dsp_test
      PORT (
        CLK : IN STD_LOGIC;
        CE : IN STD_LOGIC;
        SCLR : IN STD_LOGIC;
        A : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
        B : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        P : OUT STD_LOGIC_VECTOR(47 DOWNTO 0)
      );
    END COMPONENT;
    
    signal aclk: std_logic:= '0';
    signal aresetn: std_logic:= '0';
    signal CE: std_logic:= '0';
    signal SCLR: std_logic:= '0';
    signal A: std_logic_vector(13 downto 0):= (others => '0');
    signal B: std_logic_vector(15 downto 0):= (others => '0');
    signal P: std_logic_vector(47 downto 0);
    
    constant aclk_period: time := 10 ns;
    
    signal signal_on: std_logic:= '0';
    signal ce_off: std_logic:= '0';
    signal sclr_on: std_logic:= '0';
    
begin

   -- aclk process 
    aclk_process :process
    begin
        aclk <= not aclk;
        wait for aclk_period/2;
        aclk <= not aclk;
        wait for aclk_period/2;
    end process;

    -- Insert values for generic parameters !!
    uut: dsp_test port map (    CLK  => aclk,
                                CE   => CE,
                                SCLR => SCLR,
                                A    => A,
                                B    => B,
                                P    => P);
    
    stimulus: process
    begin
        aresetn <= '0';
        signal_on <= '0';
        ce_off <= '0';
        sclr_on <= '0';
        wait for aclk_period*10;
        aresetn <= '1';
        wait for aclk_period*10;
        
        signal_on <= '1';
        wait for aclk_period*10.5;
        ce_off <= '1';
        wait for aclk_period*10.5;
        ce_off <= '0';
        wait for aclk_period*10;
        sclr_on <= '1';
        wait for aclk_period*10.5;
        sclr_on <= '0';
        wait for aclk_period*10.5;
        signal_on <= '0';
        wait for aclk_period*10;
        sclr_on <= '1';
        wait for aclk_period;
        sclr_on <= '0';
        
        signal_on <= '1';
        wait for aclk_period*10;
        ce_off <= '1';
        wait for aclk_period*1;
        ce_off <= '0';
        wait for aclk_period*10;
        sclr_on <= '1';
        wait for aclk_period*1;
        sclr_on <= '0';
        wait for aclk_period*10;
        signal_on <= '0';
        ce_off <= '1';
        sclr_on <= '1';
        
        wait;
    end process;  
    

    signal_process: process(aclk, aresetn)                         
    begin 
        if (aresetn = '0') then
            A <= (others => '0');
            B <= (others => '0');
        elsif (aclk'event and aclk = '1') then
            if(signal_on = '1') then
                A<=std_logic_vector(unsigned(A)+1);
                B<=std_logic_vector(unsigned(B)+1);
            else
                A <= (others => '0');
                B <= (others => '0');
            end if;
        end if;
    end process;
    
    CE_process: process(aclk, aresetn)                         
    begin 
        if (aresetn = '0') then
            CE <= '1';
        elsif (aclk'event and aclk = '1') then
            if(ce_off = '1') then
                CE <= '0';
            else
                CE <= '1';
            end if;
        end if;
    end process;
    
    SCLR_process: process(aclk, aresetn)                         
    begin 
        if (aresetn = '0') then
            SCLR <= '0';
        elsif (aclk'event and aclk = '1') then
            if(sclr_on = '1') then
                SCLR <= '1';
            else
                SCLR <= '0';
            end if;
        end if;
    end process;
    
end Behavioral;
