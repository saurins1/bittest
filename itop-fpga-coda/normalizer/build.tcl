#
# Vivado (TM) v2016.4 (64-bit)
#
# build.tcl: Tcl script for re-creating project 'average_filter'
#

################################################################################
# define names
################################################################################

set project_name normalizer

################################################################################
# define paths
################################################################################

# Set the reference directory for source file relative paths (by default the value is script directory path)
set origin_dir 		"."

set common_repo 	"itop-fpga-common"

set path_sim 		$origin_dir/sim
set path_src 		$origin_dir/src/hdl
set path_src_common 	$origin_dir/../../$common_repo/normalizer/src/hdl
set path_ip 		$origin_dir/src/ip

# Set the directory path for the original project from where this script was exported
set orig_proj_dir "[file normalize "$origin_dir/$project_name"]"

################################################################################
# setup an project
################################################################################

# Create project
create_project $project_name $origin_dir/$project_name -part xc7z020clg400-1 -force 

# Set the directory path for the new project
set proj_dir [get_property directory [current_project]]

# Set project properties
set obj [get_projects $project_name]
set_property "board_part" "em.avnet.com:microzed_7020:part0:1.1" $obj
set_property "default_lib" "xil_defaultlib" $obj
set_property "ip_cache_permissions" "read write" $obj
set_property "sim.ip.auto_export_scripts" "1" $obj
set_property "simulator_language" "Mixed" $obj
set_property "target_language" "VHDL" $obj
set_property "xpm_libraries" "XPM_CDC XPM_MEMORY" $obj

################################################################################
# setup constraints
################################################################################

# Create 'constrs_1' fileset (if not found)
if {[string equal [get_filesets -quiet constrs_1] ""]} {
  create_fileset -constrset constrs_1
}

#add_files -fileset constrs_1      $path_constrs/thickness_gate_constraints.xdc

################################################################################
# setup simulation
################################################################################

# Create 'sim_1' fileset (if not found)
if {[string equal [get_filesets -quiet sim_1] ""]} {
  create_fileset -simset sim_1
}

# Set 'sim_1' fileset object
set obj [get_filesets sim_1]
set files [list \
 "[file normalize "$path_sim/src/normalizer_tb.vhd"]"\
 "[file normalize "$path_sim/simulation_files/signal_input.dat"]"\
 "[file normalize "$path_sim/simulation_files/normalizer_tb_behav.wcfg"]"\
]
add_files -norecurse -fileset $obj $files

# Set 'sim_1' fileset file properties for remote files
set file "$path_sim/src/normalizer_tb.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sim_1] [list "*$file"]]
set_property "file_type" "VHDL" $file_obj

set file "$path_sim/simulation_files/signal_input.dat"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sim_1] [list "*$file"]]
set_property "file_type" "Data Files" $file_obj

# Set 'sim_1' fileset file properties for local files
# None

# Set 'sim_1' fileset properties
set obj [get_filesets sim_1]
set_property "top" "average_filter_tb" $obj
set_property "transport_int_delay" "0" $obj
set_property "transport_path_delay" "0" $obj
set_property "xelab.nosort" "1" $obj
set_property "xelab.unifast" "" $obj
set_property "xsim.view" "$path_sim/simulation_files/normalizer_tb_behav.wcfg \
$path_sim/simulation_files/normalizer_tb_behav.wcfg" $obj

################################################################################
# setup sources
################################################################################

# Create 'sources_1' fileset (if not found)
if {[string equal [get_filesets -quiet sources_1] ""]} {
  create_fileset -srcset sources_1
}

add_files	$path_src/normalizer.vhd

# Update the compile order
update_compile_order -fileset sources_1
update_compile_order -fileset sim_1

puts "INFO: Project created:${project_name}"

# Delete temp vivado files
file delete {*}[glob vivado*]

