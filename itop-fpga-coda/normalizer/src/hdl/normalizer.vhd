----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 05/01/2018 10:07:23 AM
-- Design Name: 
-- Module Name: average filter light - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity normalizer is

	port (
	    -- Sync
	    aclk         : in STD_LOGIC;
        aresetn      : in STD_LOGIC;
	
	    -- Control
	    DATA_IN	       : in STD_LOGIC_VECTOR(127 downto 0);    
		DATA_IN_VALID  : in STD_LOGIC;
		
		NORMALIZER_RESULT	 : out STD_LOGIC_VECTOR(7 downto 0)
	);
end normalizer;

architecture arch_imp of normalizer is

begin

    div_norm_loop_process : process(aclk) 
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                NORMALIZER_RESULT <= std_logic_vector(to_unsigned(0, NORMALIZER_RESULT'length));	
            else
                if(DATA_IN_VALID = '1') then
                    NORMALIZER_RESULT <= std_logic_vector(to_unsigned(0, NORMALIZER_RESULT'length));
                    for I in 8 to 127 loop
                        if(DATA_IN(I) = '1') then
                            NORMALIZER_RESULT <= std_logic_vector(to_unsigned(I-7, NORMALIZER_RESULT'length));
                        end if;
                    end loop;
                end if;
            end if;
        end if;
    end process;
	
--    div_norm_process : process(aclk) 
--    begin
--        if (rising_edge(aclk)) then
--            if (aresetn = '0') then
--                NORMALIZER_RESULT <= std_logic_vector(to_unsigned(0, NORMALIZER_RESULT'length));	
--            else
--                if(DATA_IN_VALID = '1') then
--                    if(DATA_IN(127) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(120, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(126) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(119, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(125) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(118, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(124) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(117, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(123) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(116, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(122) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(115, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(121) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(114, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(120) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(113, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(119) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(112, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(118) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(111, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(117) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(110, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(116) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(109, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(115) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(108, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(114) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(107, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(113) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(106, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(112) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(105, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(111) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(104, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(110) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(103, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(109) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(102, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(108) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(101, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(107) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(100, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(106) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(99, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(105) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(98, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(104) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(97, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(103) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(96, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(102) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(95, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(101) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(94, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(100) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(93, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(99) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(92, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(98) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(91, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(97) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(90, NORMALIZER_RESULT'length)); 
--                    elsif(DATA_IN(96) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(89, NORMALIZER_RESULT'length)); 
--                    elsif(DATA_IN(95) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(88, NORMALIZER_RESULT'length)); 
--                    elsif(DATA_IN(94) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(87, NORMALIZER_RESULT'length)); 
--                    elsif(DATA_IN(93) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(86, NORMALIZER_RESULT'length)); 
--                    elsif(DATA_IN(92) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(85, NORMALIZER_RESULT'length)); 
--                    elsif(DATA_IN(91) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(84, NORMALIZER_RESULT'length)); 
--                    elsif(DATA_IN(90) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(83, NORMALIZER_RESULT'length)); 
--                    elsif(DATA_IN(89) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(82, NORMALIZER_RESULT'length)); 
--                    elsif(DATA_IN(88) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(81, NORMALIZER_RESULT'length)); 
--                    elsif(DATA_IN(87) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(80, NORMALIZER_RESULT'length)); 
--                    elsif(DATA_IN(86) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(79, NORMALIZER_RESULT'length)); 
--                    elsif(DATA_IN(85) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(78, NORMALIZER_RESULT'length)); 
--                    elsif(DATA_IN(84) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(77, NORMALIZER_RESULT'length)); 
--                    elsif(DATA_IN(83) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(76, NORMALIZER_RESULT'length)); 
--                    elsif(DATA_IN(82) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(75, NORMALIZER_RESULT'length)); 
--                    elsif(DATA_IN(81) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(74, NORMALIZER_RESULT'length)); 
--                    elsif(DATA_IN(80) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(73, NORMALIZER_RESULT'length)); 
--                    elsif(DATA_IN(79) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(72, NORMALIZER_RESULT'length)); 
--                    elsif(DATA_IN(78) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(71, NORMALIZER_RESULT'length)); 
--                    elsif(DATA_IN(77) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(70, NORMALIZER_RESULT'length)); 
--                    elsif(DATA_IN(76) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(69, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(75) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(68, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(74) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(67, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(73) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(66, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(72) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(65, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(71) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(64, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(70) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(63, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(69) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(62, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(68) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(61, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(67) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(60, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(66) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(59, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(65) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(58, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(64) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(57, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(63) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(56, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(62) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(55, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(61) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(54, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(60) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(53, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(59) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(52, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(58) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(51, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(57) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(50, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(56) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(49, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(55) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(48, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(54) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(47, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(53) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(46, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(52) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(45, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(51) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(44, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(50) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(43, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(49) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(42, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(48) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(41, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(47) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(40, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(46) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(39, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(45) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(38, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(44) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(37, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(43) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(36, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(42) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(35, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(41) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(34, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(40) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(33, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(39) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(32, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(38) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(31, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(37) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(30, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(36) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(29, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(35) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(28, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(34) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(27, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(33) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(26, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(32) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(25, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(31) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(24, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(30) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(23, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(29) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(22, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(28) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(21, NORMALIZER_RESULT'length));
--                    elsif(DATA_IN(27) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(20, NORMALIZER_RESULT'length));	
--                    elsif(DATA_IN(26) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(19, NORMALIZER_RESULT'length));	
--                    elsif(DATA_IN(25) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(18, NORMALIZER_RESULT'length));	
--                    elsif(DATA_IN(24) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(17, NORMALIZER_RESULT'length));	
--                    elsif(DATA_IN(23) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(16, NORMALIZER_RESULT'length));	
--                    elsif(DATA_IN(22) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(15, NORMALIZER_RESULT'length));	
--                    elsif(DATA_IN(21) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(14, NORMALIZER_RESULT'length));	
--                    elsif(DATA_IN(20) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(13, NORMALIZER_RESULT'length));	
--                    elsif(DATA_IN(19) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(12, NORMALIZER_RESULT'length));	
--                    elsif(DATA_IN(18) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(11, NORMALIZER_RESULT'length));	 
--                    elsif(DATA_IN(17) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(10, NORMALIZER_RESULT'length));	
--                    elsif(DATA_IN(16) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(9, NORMALIZER_RESULT'length));	 
--                    elsif(DATA_IN(15) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(8, NORMALIZER_RESULT'length));	
--                    elsif(DATA_IN(14) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(7, NORMALIZER_RESULT'length));	
--                    elsif(DATA_IN(13) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(6, NORMALIZER_RESULT'length));	
--                    elsif(DATA_IN(12) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(5, NORMALIZER_RESULT'length));	
--                    elsif(DATA_IN(11) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(4, NORMALIZER_RESULT'length));	
--                    elsif(DATA_IN(10) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(3, NORMALIZER_RESULT'length));	
--                    elsif(DATA_IN(9) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(2, NORMALIZER_RESULT'length));	
--                    elsif(DATA_IN(8) = '1') then
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(1, NORMALIZER_RESULT'length));	
--                    else
--                        NORMALIZER_RESULT <= std_logic_vector(to_unsigned(0, NORMALIZER_RESULT'length));	
--                    end if;                  
--                else
--                    NORMALIZER_RESULT <= std_logic_vector(to_unsigned(0, NORMALIZER_RESULT'length));	     
--                end if;   
--            end if;
--        end if;
--    end process;
      
end arch_imp;
