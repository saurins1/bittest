library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity channel_tb is
	generic (
  		CHANNEL_ID		: INTEGER := 0;
  		REGISTER_W64    : INTEGER := 64;
  		REGISTER_W48    : INTEGER := 48;
  	    REGISTER_W32    : INTEGER := 32;
  	    REGISTER_W21    : INTEGER := 21;
        REGISTER_W16    : INTEGER := 16;
  		REGISTER_W14    : INTEGER := 14;
  		REGISTER_W8     : INTEGER := 8;
  		REGISTER_W4     : INTEGER := 4;
  		REGISTER_W2     : INTEGER := 2;
  		DISPOSITION_PASS: INTEGER := 1;
  		DISPOSITION_FAIL: INTEGER := 2;
  		DISPOSITION_NODET : INTEGER := 0;
        TRIGGER_WAIT    : INTEGER := 32;
        ASCAN_HEADER_SIZE : INTEGER := 43;
        MIN_COINC_LEVEL : INTEGER := 2;
  		MIN_AVG_LEVEL   : INTEGER := 2;
        N_TAPS              : INTEGER    := 512;
        N_SETS_TAP          : INTEGER    := 3;
        OVERSAMPLING        : INTEGER    := 2;
        WAIT_CYCLES         : INTEGER    := 4
  	);
end;

architecture bench of channel_tb is

  component channel
  	generic (
  		CHANNEL_ID		: INTEGER := 0;
  		REGISTER_W64    : INTEGER := 64;
  		REGISTER_W48    : INTEGER := 48;
  	    REGISTER_W32    : INTEGER := 32;
  	    REGISTER_W21    : INTEGER := 21;
        REGISTER_W16    : INTEGER := 16;
  		REGISTER_W14    : INTEGER := 14;
  		REGISTER_W8     : INTEGER := 8;
  		REGISTER_W4     : INTEGER := 4;
  		REGISTER_W2     : INTEGER := 2;
  		DISPOSITION_PASS: INTEGER := 1;
  		DISPOSITION_FAIL: INTEGER := 2;
  		DISPOSITION_NODET : INTEGER := 0;
        TRIGGER_WAIT    : INTEGER := 32;
        ASCAN_HEADER_SIZE : INTEGER := 43;
        MIN_COINC_LEVEL : INTEGER := 2;
  		MIN_AVG_LEVEL   : INTEGER := 2;
        N_TAPS              : INTEGER    := 512;
        N_SETS_TAP          : INTEGER    := 3;
        OVERSAMPLING        : INTEGER    := 2;
        WAIT_CYCLES         : INTEGER    := 4
  	);
  	port (
  	    aclk         : in STD_LOGIC;
        aresetn      : in STD_LOGIC;
  		START_INSPECTION  : in STD_LOGIC;
  		TRIGGER_TYPE  	  : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
  		READ_ASCAN  	  : in STD_LOGIC;
  		ASCAN_READY  	  : out STD_LOGIC;
  		ASCAN_SIZE	      : out STD_LOGIC_VECTOR(REGISTER_W21-1 downto 0);
  				-- DEBUG
          CHANNEL_ENABLED   : out STD_LOGIC;
          CONFIG_ON        : out STD_LOGIC;
  		START_STOP_INSPECTION  : in STD_LOGIC;
  		INSPECTION_INPROGESS   : out STD_LOGIC;
  		DISPOSITION		  : out STD_LOGIC;
  		DISPOSITION_BITS  : out STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
  		SIGNAL_COINC_WINDOW  	  : out STD_LOGIC;
  		SIGNAL_AVG_WINDOW  		  : out STD_LOGIC;
  		SIGNAL_INPUT_AXIS_TREADY  : out STD_LOGIC;
  		SIGNAL_INPUT_AXIS_TDATA	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  		SIGNAL_INPUT_AXIS_TLAST	  : in STD_LOGIC;
  		SIGNAL_INPUT_AXIS_TVALID  : in STD_LOGIC;
  		CONFIG_INPUT_LOAD	  	  : in STD_LOGIC;
  		CONFIG_INPUT_AXIS_TREADY  : out STD_LOGIC;
  		CONFIG_INPUT_AXIS_TDATA	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		CONFIG_INPUT_AXIS_TLAST	  : in STD_LOGIC;
  		CONFIG_INPUT_AXIS_TVALID  : in STD_LOGIC;
  		ENCODER1_POS  	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		ENCODER1_DIR  	  : in STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
  		ENCODER1_RPM  	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		ENCODER1_TRIGGER_COUNTS : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		ENCODER2_POS  	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		ENCODER2_DIR  	  : in STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
  		ENCODER2_RPM  	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		ENCODER2_TRIGGER_COUNTS : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		ENCODER3_POS  	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		ENCODER3_DIR  	  : in STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
  		ENCODER3_RPM  	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		ENCODER3_TRIGGER_COUNTS : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		IO_INPUTS  	  	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  		IO_OUTPUTS  	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  		RECEIVER_SAMPLING_FREQUENCY         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        RECEIVER_DATA_WINDOW                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        RECEIVER_DELAY                      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        RECEIVER_GAIN                       : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        RECEIVER_EXTERNAL_MULTIPLEXER_EN    : out STD_LOGIC;
  		TRANSMITTER_VOLTAGE                 : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        TRANSMITTER_BURST_FREQUENCY         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        TRANSMITTER_N_CYCLES                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        TRANSMITTER_DELAY                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        TRANSMITTER_DIRECTIONAL_PHASING     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		MAGNET_MODE                         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        MAGNET_PULSE_WIDTH                  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        MAGNET_INITIAL_DELAY                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		DSP_ANALOG_FILTER  					: out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
  		CONFIG_DAC_AXIS_TREADY    : in STD_LOGIC;
  		CONFIG_DAC_AXIS_TDATA	  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		CONFIG_DAC_AXIS_TLAST	  : out STD_LOGIC;
  		CONFIG_DAC_AXIS_TVALID    : out STD_LOGIC;
  		ASCAN_AXIS_TREADY  : in STD_LOGIC;
  		ASCAN_AXIS_TDATA   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		ASCAN_AXIS_TLAST   : out STD_LOGIC;
  		ASCAN_AXIS_TVALID  : out STD_LOGIC;
  		ASCAN_AXIS_TKEEP   : out std_logic_vector((REGISTER_W32/8)-1 downto 0)
  	);
  end component;

  signal aclk: STD_LOGIC:= '0';
  signal aresetn: STD_LOGIC:= '0';

  signal START_INSPECTION: STD_LOGIC:= '0';
  signal TRIGGER_TYPE: STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0):= (others => '0');
  signal READ_ASCAN: STD_LOGIC:= '0';
  signal ASCAN_READY: STD_LOGIC;
  signal ASCAN_SIZE: STD_LOGIC_VECTOR(REGISTER_W21-1 downto 0);
  signal CHANNEL_ENABLED : STD_LOGIC;                                    
  signal CONFIG_ON : STD_LOGIC;

  signal START_STOP_INSPECTION: STD_LOGIC:= '0';
  signal INSPECTION_INPROGESS: STD_LOGIC;
  signal DISPOSITION: STD_LOGIC;
  signal DISPOSITION_BITS: STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);

  signal SIGNAL_COINC_WINDOW: STD_LOGIC;
  signal SIGNAL_AVG_WINDOW: STD_LOGIC;

  signal SIGNAL_INPUT_AXIS_TREADY: STD_LOGIC;
  signal SIGNAL_INPUT_AXIS_TDATA: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0');
  signal SIGNAL_INPUT_AXIS_TLAST: STD_LOGIC:= '0';
  signal SIGNAL_INPUT_AXIS_TVALID: STD_LOGIC:= '0';

  signal CONFIG_INPUT_LOAD: STD_LOGIC:= '0';
  signal CONFIG_INPUT_AXIS_TREADY: STD_LOGIC;
  signal CONFIG_INPUT_AXIS_TDATA: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');
  signal CONFIG_INPUT_AXIS_TLAST: STD_LOGIC:= '0';
  signal CONFIG_INPUT_AXIS_TVALID: STD_LOGIC:= '0';

  signal ENCODER1_POS: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');
  signal ENCODER1_DIR: STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0):= (others => '0');
  signal ENCODER1_RPM: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');
  signal ENCODER1_TRIGGER_COUNTS: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');
  signal ENCODER2_POS: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');
  signal ENCODER2_DIR: STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0):= (others => '0');
  signal ENCODER2_RPM: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');
  signal ENCODER2_TRIGGER_COUNTS: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');
  signal ENCODER3_POS: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');
  signal ENCODER3_DIR: STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0):= (others => '0');
  signal ENCODER3_RPM: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');
  signal ENCODER3_TRIGGER_COUNTS: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');

  signal IO_INPUTS: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0');
  signal IO_OUTPUTS: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0');

  signal RECEIVER_SAMPLING_FREQUENCY: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal RECEIVER_DATA_WINDOW: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal RECEIVER_DELAY: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal RECEIVER_GAIN: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal RECEIVER_EXTERNAL_MULTIPLEXER_EN: STD_LOGIC;

  signal TRANSMITTER_VOLTAGE: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal TRANSMITTER_BURST_FREQUENCY: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal TRANSMITTER_N_CYCLES: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal TRANSMITTER_DELAY: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal TRANSMITTER_DIRECTIONAL_PHASING: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

  signal MAGNET_MODE: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal MAGNET_PULSE_WIDTH: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal MAGNET_INITIAL_DELAY: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

  signal DSP_ANALOG_FILTER: STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);

  signal CONFIG_DAC_AXIS_TREADY: STD_LOGIC:= '0';
  signal CONFIG_DAC_AXIS_TDATA: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal CONFIG_DAC_AXIS_TLAST: STD_LOGIC;
  signal CONFIG_DAC_AXIS_TVALID: STD_LOGIC;

  signal ASCAN_AXIS_TREADY: STD_LOGIC:= '0';
  signal ASCAN_AXIS_TDATA: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal ASCAN_AXIS_TLAST: STD_LOGIC;
  signal ASCAN_AXIS_TVALID: STD_LOGIC;
  signal ASCAN_AXIS_TKEEP: std_logic_vector((REGISTER_W32/8)-1 downto 0);

  constant aclk_period: time := 10 ns;
  
  type states_config_sm is (idle, open_file, load_config_header, load_config_channel0, load_config_wait, close_file); 
  signal state_config_sm : states_config_sm; 

  signal load_config_flag: STD_LOGIC:= '0'; 

  signal header_size        : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');
  signal channels           : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');
  signal channel0_offset    : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');
  signal channel1_offset    : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');
  signal encoder_offset     : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');
  signal digitalio_offset   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');
  signal channel0_size      : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');
  signal channel1_size      : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');
  signal channel0_configuration_size_flag: STD_LOGIC:= '0';

  type states_signal_sm is (idle, open_file, load_signal, load_signal_wait, close_file); 
  signal state_signal_sm : states_signal_sm; 

  signal load_signal_flag: STD_LOGIC:= '0';

  type states_general_sm is (idle, initial_config, start_inspection_on, get_ascan_1, get_ascan_2); 
  signal state_general_sm : states_general_sm; 
  
  signal start_general_sm: STD_LOGIC:= '0';
  
  signal ascan_counter: UNSIGNED(REGISTER_W32-1 downto 0);
    
begin
	
	-- aclk process 
    aclk_process :process
    begin
        aclk <= not aclk;
        wait for aclk_period/2;
        aclk <= not aclk;
        wait for aclk_period/2;
    end process;

  -- Insert values for generic parameters !!
  uut: channel generic map ( CHANNEL_ID                       => CHANNEL_ID,
                             REGISTER_W64                     => REGISTER_W64,
                             REGISTER_W48                     => REGISTER_W48,
                             REGISTER_W32                     => REGISTER_W32,
                             REGISTER_W21                     => REGISTER_W21,
                             REGISTER_W16                     => REGISTER_W16,
                             REGISTER_W14                     => REGISTER_W14,
                             REGISTER_W8                      => REGISTER_W8,
                             REGISTER_W4                      => REGISTER_W4,
                             REGISTER_W2                      => REGISTER_W2,
                             DISPOSITION_PASS                 => DISPOSITION_PASS,
                             DISPOSITION_FAIL                 => DISPOSITION_FAIL,
                             DISPOSITION_NODET                => DISPOSITION_NODET,
                             TRIGGER_WAIT                     => TRIGGER_WAIT,
                             ASCAN_HEADER_SIZE                => ASCAN_HEADER_SIZE,
                             MIN_COINC_LEVEL                  => MIN_COINC_LEVEL,
                             MIN_AVG_LEVEL                    => MIN_AVG_LEVEL,
                             N_TAPS                           => N_TAPS,
                             N_SETS_TAP                       => N_SETS_TAP,
                             OVERSAMPLING                     => OVERSAMPLING,
                             WAIT_CYCLES                      => WAIT_CYCLES )
                  port map ( aclk                             => aclk,
                             aresetn                          => aresetn,
                             START_INSPECTION                 => START_INSPECTION,
                             TRIGGER_TYPE                     => TRIGGER_TYPE,
                             READ_ASCAN                       => READ_ASCAN,
                             ASCAN_READY                      => ASCAN_READY,
                             ASCAN_SIZE                       => ASCAN_SIZE,
                             CHANNEL_ENABLED                  => CHANNEL_ENABLED,
                             CONFIG_ON                        => CONFIG_ON,
                             START_STOP_INSPECTION            => START_STOP_INSPECTION,
                             INSPECTION_INPROGESS             => INSPECTION_INPROGESS,
                             DISPOSITION                      => DISPOSITION,
                             DISPOSITION_BITS                 => DISPOSITION_BITS,
                             SIGNAL_COINC_WINDOW              => SIGNAL_COINC_WINDOW,
                             SIGNAL_AVG_WINDOW                => SIGNAL_AVG_WINDOW,
                             SIGNAL_INPUT_AXIS_TREADY         => SIGNAL_INPUT_AXIS_TREADY,
                             SIGNAL_INPUT_AXIS_TDATA          => SIGNAL_INPUT_AXIS_TDATA,
                             SIGNAL_INPUT_AXIS_TLAST          => SIGNAL_INPUT_AXIS_TLAST,
                             SIGNAL_INPUT_AXIS_TVALID         => SIGNAL_INPUT_AXIS_TVALID,
                             CONFIG_INPUT_LOAD                => CONFIG_INPUT_LOAD,
                             CONFIG_INPUT_AXIS_TREADY         => CONFIG_INPUT_AXIS_TREADY,
                             CONFIG_INPUT_AXIS_TDATA          => CONFIG_INPUT_AXIS_TDATA,
                             CONFIG_INPUT_AXIS_TLAST          => CONFIG_INPUT_AXIS_TLAST,
                             CONFIG_INPUT_AXIS_TVALID         => CONFIG_INPUT_AXIS_TVALID,
                             ENCODER1_POS                     => ENCODER1_POS,
                             ENCODER1_DIR                     => ENCODER1_DIR,
                             ENCODER1_RPM                     => ENCODER1_RPM,
                             ENCODER1_TRIGGER_COUNTS          => ENCODER1_TRIGGER_COUNTS,
                             ENCODER2_POS                     => ENCODER2_POS,
                             ENCODER2_DIR                     => ENCODER2_DIR,
                             ENCODER2_RPM                     => ENCODER2_RPM,
                             ENCODER2_TRIGGER_COUNTS          => ENCODER2_TRIGGER_COUNTS,
                             ENCODER3_POS                     => ENCODER3_POS,
                             ENCODER3_DIR                     => ENCODER3_DIR,
                             ENCODER3_RPM                     => ENCODER3_RPM,
                             ENCODER3_TRIGGER_COUNTS          => ENCODER3_TRIGGER_COUNTS,
                             IO_INPUTS                        => IO_INPUTS,
                             IO_OUTPUTS                       => IO_OUTPUTS,
                             RECEIVER_SAMPLING_FREQUENCY      => RECEIVER_SAMPLING_FREQUENCY,
                             RECEIVER_DATA_WINDOW             => RECEIVER_DATA_WINDOW,
                             RECEIVER_DELAY                   => RECEIVER_DELAY,
                             RECEIVER_GAIN                    => RECEIVER_GAIN,
                             RECEIVER_EXTERNAL_MULTIPLEXER_EN => RECEIVER_EXTERNAL_MULTIPLEXER_EN,
                             TRANSMITTER_VOLTAGE              => TRANSMITTER_VOLTAGE,
                             TRANSMITTER_BURST_FREQUENCY      => TRANSMITTER_BURST_FREQUENCY,
                             TRANSMITTER_N_CYCLES             => TRANSMITTER_N_CYCLES,
                             TRANSMITTER_DELAY                => TRANSMITTER_DELAY,
                             TRANSMITTER_DIRECTIONAL_PHASING  => TRANSMITTER_DIRECTIONAL_PHASING,
                             MAGNET_MODE                      => MAGNET_MODE,
                             MAGNET_PULSE_WIDTH               => MAGNET_PULSE_WIDTH,
                             MAGNET_INITIAL_DELAY             => MAGNET_INITIAL_DELAY,
                             DSP_ANALOG_FILTER                => DSP_ANALOG_FILTER,
                             CONFIG_DAC_AXIS_TREADY           => CONFIG_DAC_AXIS_TREADY,
                             CONFIG_DAC_AXIS_TDATA            => CONFIG_DAC_AXIS_TDATA,
                             CONFIG_DAC_AXIS_TLAST            => CONFIG_DAC_AXIS_TLAST,
                             CONFIG_DAC_AXIS_TVALID           => CONFIG_DAC_AXIS_TVALID,
                             ASCAN_AXIS_TREADY                => ASCAN_AXIS_TREADY,
                             ASCAN_AXIS_TDATA                 => ASCAN_AXIS_TDATA,
                             ASCAN_AXIS_TLAST                 => ASCAN_AXIS_TLAST,
                             ASCAN_AXIS_TVALID                => ASCAN_AXIS_TVALID,
                             ASCAN_AXIS_TKEEP                 => ASCAN_AXIS_TKEEP );

	stimulus: process
	begin
		aresetn <= '0';
		wait for aclk_period*10;
		aresetn <= '1';
		wait for aclk_period*10;
		start_general_sm <= '1';
		wait for aclk_period*10;
		start_general_sm <= '0';
		wait;
	end process;
	
    trigger_inst: process(aclk, aresetn) 
        variable var_counter : integer range 0 to 65536 :=0;
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                ENCODER1_POS <= (others => '0');  
                var_counter := 0;  
            else
                if(UNSIGNED(TRIGGER_TYPE) /= 0) then
                    if(var_counter >= 2) then
                        ENCODER1_POS <= std_logic_vector(signed(ENCODER1_POS)-1); 
                        --ENCODER1_POS <= std_logic_vector(signed(ENCODER1_POS)+1); 
                        var_counter := 0;
                    else    
                        var_counter := var_counter + 1;
                    end if;  
                else
                    ENCODER1_POS <= (others => '0');  
                    var_counter := 0;  
                end if;
            end if;
        end if;
    end process;
    
    ascan_counter_inst: process(aclk, aresetn) 
        variable var_counter : integer range 0 to 65536 :=0;
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                ascan_counter <= (others => '0'); 
            else
                if(ASCAN_AXIS_TVALID = '1') then 
                    ascan_counter <= ascan_counter + 1;
                else
                    ascan_counter <= (others => '0');
                end if;
            end if;
        end if;
    end process;
    
	-- General SM
    general_sm_inst: process(aclk, aresetn)  
        variable my_counter  : integer range 0 to 16383 :=0;    
    begin 
        if (aresetn = '0') then
			-- config
			START_INSPECTION <= '0';
			TRIGGER_TYPE <= (others => '0');
			READ_ASCAN <= '0';
			START_STOP_INSPECTION <= '0';
			CONFIG_DAC_AXIS_TREADY <= '1';
			ASCAN_AXIS_TREADY <= '1';
			-- state																		  	
            state_general_sm <= idle;  
        elsif (aclk'event and aclk = '1') then
            case state_general_sm is  
                when idle =>     
					--TRIGGER_TYPE <= std_logic_vector(to_unsigned(0, TRIGGER_TYPE'length));	
					TRIGGER_TYPE <= std_logic_vector(to_unsigned(1, TRIGGER_TYPE'length));
					ENCODER1_TRIGGER_COUNTS	<= std_logic_vector(to_unsigned(500, ENCODER1_TRIGGER_COUNTS'length)); -- 500000
					READ_ASCAN <= '0';												 
                    if(start_general_sm = '1') then
                        my_counter := 0;
                        state_general_sm <= initial_config;        
                    end if;
                when initial_config => 
                    load_config_flag <= '1';
					if(CONFIG_INPUT_AXIS_TLAST ='1') then
						state_general_sm <= start_inspection_on;													 
					end if;												 
                when start_inspection_on =>
                    START_INSPECTION <= '1';
					START_STOP_INSPECTION <= '1';
					load_config_flag <= '0';															 
					if(my_counter >= 16) then
						load_signal_flag <= '1';
						state_general_sm <= get_ascan_1;										 
					else
						my_counter := my_counter + 1;											 
					end if;												 
				when get_ascan_1 =>										  
					if(ASCAN_READY = '1') then
						READ_ASCAN <= '1';	
						state_general_sm <= get_ascan_2;											 
					end if;												 
                when get_ascan_2 =>
					if(ASCAN_READY = '1') then
						READ_ASCAN <= '0';											 
					end if;													 
					if(ASCAN_AXIS_TLAST = '1') then
						READ_ASCAN <= '0';
						START_STOP_INSPECTION <= '0';
						--state_general_sm <= get_ascan_1;		
						state_general_sm <= start_inspection_on;									 
					end if;		
                when others =>
                    null;                   
            end case;                                                                                            
        end if;
    end process; 	

	-- Configuration SM
    configuration_input_inst: process(aclk, aresetn)  
        variable my_counter  : integer range 0 to 16383 :=0;    

        type configDataFile is file of integer;
        file config_data_file: configDataFile;
        variable config_data: integer;     
        variable config_data_file_status: file_open_status;
    begin 
        if (aresetn = '0') then
			-- config_input
			CONFIG_INPUT_LOAD <= '0';
			CONFIG_INPUT_AXIS_TDATA <= (others => '0');
			CONFIG_INPUT_AXIS_TLAST <= '0';
			CONFIG_INPUT_AXIS_TVALID <= '0';
			-- config params
			header_size <= (others => '0');
 			channels <= (others => '0');
			channel0_offset <= (others => '0');
			channel1_offset <= (others => '0');
			encoder_offset <= (others => '0');
			digitalio_offset <= (others => '0');
			channel0_size <= (others => '0');
			channel1_offset <= (others => '0');
			channel0_configuration_size_flag <= '1';
			-- state																		  	
            state_config_sm <= idle;  
        elsif (aclk'event and aclk = '1') then
            case state_config_sm is  
                when idle =>         
                    if(load_config_flag = '1') then
                        my_counter := 0;
                        state_config_sm <= open_file;        
                    end if;
                when open_file => 
                    file_open(config_data_file_status, config_data_file, "configuration.dat", read_mode);
                   	my_counter := 0; 
				    channel0_configuration_size_flag <= '1';
                    state_config_sm <= load_config_header;  
                when load_config_header =>
					CONFIG_INPUT_LOAD <= '1';
                    if(CONFIG_INPUT_AXIS_TREADY = '1') then
						-- tdata
                     	if not endfile(config_data_file) then
                        	read (config_data_file, config_data);
							if(my_counter = 0) then
								header_size<=std_logic_vector(to_signed(config_data, header_size'length));	
							elsif(my_counter = 1) then
								channels<=std_logic_vector(to_signed(config_data, channels'length));	
							elsif(my_counter = 2) then
								channel0_offset<=std_logic_vector(to_signed(config_data, channel0_offset'length));	
							elsif(my_counter = 3) then
								channel1_offset<=std_logic_vector(to_signed(config_data, channel1_offset'length));
							elsif(my_counter = 4) then
								encoder_offset<=std_logic_vector(to_signed(config_data, encoder_offset'length));	
							elsif(my_counter = 5) then
								digitalio_offset<=std_logic_vector(to_signed(config_data, digitalio_offset'length));												   
							end if;
                            CONFIG_INPUT_AXIS_TDATA<=std_logic_vector(to_signed(config_data, CONFIG_INPUT_AXIS_TDATA'length));
                        end if;
						if not endfile(config_data_file) then
                            read (config_data_file, config_data);
                        end if;											
						if(my_counter >= 5) then
							my_counter := 0; 										  
							state_config_sm <= load_config_channel0;											  
						else
							-- counter										  
							my_counter := my_counter + 1;										  
						end if;   
                    end if;			
																				
                when load_config_channel0 =>
					CONFIG_INPUT_LOAD <= '1';
                    if(CONFIG_INPUT_AXIS_TREADY = '1') then
						-- tdata
                     	if not endfile(config_data_file) then
                        	read (config_data_file, config_data);
                            CONFIG_INPUT_AXIS_TDATA<=std_logic_vector(to_signed(config_data, CONFIG_INPUT_AXIS_TDATA'length));
                        end if;
						-- tvalid																										
						CONFIG_INPUT_AXIS_TVALID <= '1';
						if(my_counter = 0) then													
							channel0_size <= std_logic_vector(to_signed(config_data, CONFIG_INPUT_AXIS_TDATA'length));
							channel0_configuration_size_flag <= '0';
						end if;	
						if not endfile(config_data_file) then
                            read (config_data_file, config_data);
                        end if;	
						-- tlast											  
						--if(channel0_configuration_size_flag = '0' and my_counter >= channel_configuration_size+6) then
						if(channel0_configuration_size_flag = '0' and my_counter >= unsigned(channel0_size)-1) then
							CONFIG_INPUT_AXIS_TLAST <= '1';
							my_counter := 0; 										  
							state_config_sm <= load_config_wait;											  
						else
							-- counter										  
							my_counter := my_counter + 1;										  
						end if;  
                    end if;																																																												
				when load_config_wait =>										  
					CONFIG_INPUT_LOAD <= '0';
					CONFIG_INPUT_AXIS_TDATA <= (others => '0');
					CONFIG_INPUT_AXIS_TLAST <= '0';
					CONFIG_INPUT_AXIS_TVALID <= '0';													  
					if(my_counter >= 16) then
						my_counter := 0; 
						state_config_sm <= close_file;													
					else
						my_counter := my_counter + 1;										  
					end if;														  
                when close_file =>
                    if(my_counter >= 32) then
						if(load_config_flag = '0') then													
							file_close(config_data_file);
							state_config_sm <= idle;
						end if;														
                    else
                        my_counter := my_counter + 1;
                    end if;   
                when others =>
                    null;                   
            end case;                                                                                            
        end if;
    end process; 
																																				
	-- Signal SM
    signal_input_inst: process(aclk, aresetn)  
        variable my_counter  : integer range 0 to 16383 :=0;    

        type signalDataFile is file of integer;
        file signal_data_file: signalDataFile;
        variable signal_data: integer;     
        variable signal_data_file_status: file_open_status;
    begin 
        if (aresetn = '0') then
			-- signal input
			SIGNAL_INPUT_AXIS_TDATA <= (others => '0');
			SIGNAL_INPUT_AXIS_TLAST <= '0';
			SIGNAL_INPUT_AXIS_TVALID <= '0';
			-- state																		  	
            state_signal_sm <= idle;  
        elsif (aclk'event and aclk = '1') then
            case state_signal_sm is  
                when idle =>         
                    if(load_signal_flag = '1') then
                        my_counter := 0;
                        state_signal_sm <= open_file;        
                    end if;
                when open_file => 
                    file_open(signal_data_file_status, signal_data_file, "ascan.dat", read_mode);
                   	my_counter := 0; 
                    state_signal_sm <= load_signal;  													
                when load_signal =>
                    if(SIGNAL_INPUT_AXIS_TREADY = '1') then
						-- tdata
                     	if not endfile(signal_data_file) then
                        	read (signal_data_file, signal_data);
                            SIGNAL_INPUT_AXIS_TDATA<=std_logic_vector(to_signed(signal_data, SIGNAL_INPUT_AXIS_TDATA'length));
                        end if;
						-- tvalid																										
						SIGNAL_INPUT_AXIS_TVALID <= '1';	
						if not endfile(signal_data_file) then
                            read (signal_data_file, signal_data);
                        end if;	
						-- tlast											  
						if(my_counter >= 9799) then
							SIGNAL_INPUT_AXIS_TLAST <= '1';
							my_counter := 0; 										  
							state_signal_sm <= load_signal_wait;											  
						else
							-- counter										  
							my_counter := my_counter + 1;										  
						end if;  
                    end if;																																																												
				when load_signal_wait =>										  
					SIGNAL_INPUT_AXIS_TDATA <= (others => '0');
					SIGNAL_INPUT_AXIS_TLAST <= '0';
					SIGNAL_INPUT_AXIS_TVALID <= '0';													  
					if(my_counter >= 16) then
						my_counter := 0; 
						state_signal_sm <= close_file;													
					else
						my_counter := my_counter + 1;										  
					end if;														  
                when close_file =>
                    if(my_counter >= 32) then
						--if(load_config_flag = '0') then													
							file_close(signal_data_file);
							state_signal_sm <= open_file;
						--end if;														
                    else
                        my_counter := my_counter + 1;
                    end if;   
                when others =>
                    null;                   
            end case;                                                                                            
        end if;
    end process; 																

end;