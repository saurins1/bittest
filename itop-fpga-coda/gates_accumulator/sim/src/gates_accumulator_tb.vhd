library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity decimator_filter_tb is
	generic (
    REGISTER_W32        : INTEGER := 32;
    REGISTER_W16        : INTEGER := 16;
    REGISTER_W14        : INTEGER := 14;
    BRAM_ADDR_WIDTH     : INTEGER := 11;
    BRAM_DATA_WIDTH     : INTEGER := 16;
    WAIT_CYCLES            : INTEGER := 32;
    N_INPUT_DELAY       : INTEGER := 8;
    N_OUTPUT_DELAY      : INTEGER := 4;
    DEC_TYPE_NONE        : INTEGER := 0;
    DEC_TYPE_SIMPLE     : INTEGER := 1;
    DEC_TYPE_MINMAX     : INTEGER := 2;
    DEC_TYPE_ALOK         : INTEGER := 3
);
end;

architecture bench of decimator_filter_tb is

    COMPONENT decimator_filter is
	generic (
        REGISTER_W32        : INTEGER := 32;
        REGISTER_W16        : INTEGER := 16;
        REGISTER_W14        : INTEGER := 14;
        BRAM_ADDR_WIDTH     : INTEGER := 13;
        BRAM_DATA_WIDTH     : INTEGER := 16;
		WAIT_CYCLES        	: INTEGER := 32;
		N_INPUT_DELAY       : INTEGER := 8;
		N_OUTPUT_DELAY      : INTEGER := 4;
		DEC_TYPE_NONE    	: INTEGER := 0;
        DEC_TYPE_SIMPLE     : INTEGER := 1;
        DEC_TYPE_MINMAX     : INTEGER := 2;
		DEC_TYPE_ALOK     	: INTEGER := 3
	);
	port (
	    -- Sync
	    aclk         : in STD_LOGIC;
        aresetn      : in STD_LOGIC;
	
	    -- Control
        NO_READ_DEC_RESULT  : in STD_LOGIC;
        READ_DEC_RESULT     : in STD_LOGIC;
        DEC_END             : out STD_LOGIC;
        DEC_EN              : out STD_LOGIC;
	    
	    DEC_TYPE	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);    
	    DEC_LEVEL	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	    DEC_WINDOW	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	    DATA_WINDOW	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
	    DEC_TYPE_RESULT	    : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);    
	    DEC_LEVEL_RESULT	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	    DEC_WINDOW_RESULT	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        DEC_WINDOW_HALF_RESULT	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);	
        	
		-- Signal input
		SIGNAL_INPUT_AXIS_TREADY  : out STD_LOGIC;
		SIGNAL_INPUT_AXIS_TDATA	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		SIGNAL_INPUT_AXIS_TLAST	  : in STD_LOGIC;
		SIGNAL_INPUT_AXIS_TVALID  : in STD_LOGIC;

		-- Decimation result
		DEC_RESULT_AXIS_TVALID    : out STD_LOGIC;
		DEC_RESULT_AXIS_TDATA	  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		DEC_RESULT_AXIS_TLAST	  : out STD_LOGIC;
		DEC_RESULT_AXIS_TREADY    : in STD_LOGIC
	);
    end COMPONENT decimator_filter;   

  signal aclk: std_logic:= '0';
  signal aresetn: std_logic:= '0';
  
  signal NO_READ_DEC_RESULT  : STD_LOGIC:= '0';
  signal READ_DEC_RESULT  : STD_LOGIC:= '0';
  signal DEC_END  : STD_LOGIC;
  signal DEC_EN  : STD_LOGIC;

  signal DEC_TYPE: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0'); 
  signal DEC_LEVEL: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0'); 
  signal DEC_WINDOW: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0'); 
  signal DATA_WINDOW: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0'); 
  
  signal DEC_TYPE_RESULT: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
  signal DEC_LEVEL_RESULT: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
  signal DEC_WINDOW_RESULT: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
  signal DEC_WINDOW_HALF_RESULT: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
  
  signal SIGNAL_INPUT_AXIS_TREADY  : STD_LOGIC;
  signal SIGNAL_INPUT_AXIS_TDATA   : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0');
  signal SIGNAL_INPUT_AXIS_TLAST   : STD_LOGIC:= '0';
  signal SIGNAL_INPUT_AXIS_TVALID  : STD_LOGIC:= '0';
  
  signal DEC_RESULT_AXIS_TVALID    : STD_LOGIC;
  signal DEC_RESULT_AXIS_TDATA     : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal DEC_RESULT_AXIS_TLAST     : STD_LOGIC;
  signal DEC_RESULT_AXIS_TREADY    : STD_LOGIC:= '0';
  
  signal DEC_RESULT_AXIS_TLAST_d1     : STD_LOGIC;

  constant aclk_period: time := 10 ns;
  
  type states_sm is (idle, open_files, load_signal, check_decimation_end, read_result, read_result_end, show_result, close_files); 
  signal state_sm : states_sm; 
  
  signal start_sm: std_logic:= '0';
  
  type decimation_result_array is array ( 0 to 16383 ) of STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  signal decimation_result_array_i    : decimation_result_array;
  
  signal decimation_result_x: std_logic_vector(REGISTER_W16-1 downto 0);
  signal decimation_result_y: std_logic_vector(REGISTER_W16-1 downto 0);
  signal decimation_result_on: std_logic:= '0';
  
  signal data_window_result: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');
  
begin
		
	-- aclk process 
    aclk_process :process
    begin
        aclk <= not aclk;
        wait for aclk_period/2;
        aclk <= not aclk;
        wait for aclk_period/2;
    end process;
    
    stimulus: process
    begin
      aresetn <= '0';
      wait for aclk_period*10;
      aresetn <= '1';
      wait for aclk_period*10;
      start_sm <= '1';
      wait for aclk_period*10;
      start_sm <= '0';
      wait;
    end process;
          
    process_inst: process(aclk, aresetn)  
        variable my_counter  : integer range 0 to 65535 :=0;    
           
        type signalDataFile is file of integer;
        file signal_data_file: signalDataFile;
        variable signal_dec: integer;     
        variable signal_data_file_status: file_open_status;                   
    begin 
        if (aresetn = '0') then
            NO_READ_DEC_RESULT <= '0';     
            READ_DEC_RESULT <= '0';  
            DEC_TYPE <= (others => '0');
            DEC_LEVEL <= (others => '0');
            DEC_WINDOW <= (others => '0');
            DATA_WINDOW <= (others => '0');
            SIGNAL_INPUT_AXIS_TDATA <= (others => '0');
            SIGNAL_INPUT_AXIS_TLAST <= '0';
            SIGNAL_INPUT_AXIS_TVALID <= '0';
            DEC_RESULT_AXIS_TREADY <= '0';   
            decimation_result_x <= (others => '0');
            decimation_result_y <= (others => '0');
            state_sm <= idle;  
        elsif (aclk'event and aclk = '1') then
            case state_sm is  
                when idle =>     
--                    DEC_TYPE <= std_logic_vector(to_unsigned(DEC_TYPE_NONE, DEC_TYPE'length));
--                    DEC_TYPE <= std_logic_vector(to_unsigned(DEC_TYPE_SIMPLE, DEC_TYPE'length));
                    DEC_TYPE <= std_logic_vector(to_unsigned(DEC_TYPE_MINMAX, DEC_TYPE'length));
--                    DEC_TYPE <= std_logic_vector(to_unsigned(DEC_TYPE_ALOK, DEC_TYPE'length));
                    
--                    DEC_LEVEL <= std_logic_vector(to_unsigned(4, DEC_LEVEL'length));
                    DEC_LEVEL <= std_logic_vector(to_unsigned(8, DEC_LEVEL'length));
--                    DEC_LEVEL <= std_logic_vector(to_unsigned(16, DEC_LEVEL'length));
--                    DEC_LEVEL <= std_logic_vector(to_unsigned(32, DEC_LEVEL'length));
                    
                    DEC_WINDOW <= std_logic_vector(to_unsigned(10000, DEC_WINDOW'length));
                    DATA_WINDOW <= std_logic_vector(to_unsigned(10000, DEC_WINDOW'length));

                    DEC_RESULT_AXIS_TREADY <= '0';                   
                    if(start_sm = '1') then
                        my_counter := 0;
                        state_sm <= open_files;        
                    end if;
                when open_files =>
                    file_open(signal_data_file_status, signal_data_file, "signal_dec.dat", read_mode);
                    state_sm <= load_signal;                                                                          
                when load_signal =>
                    if(SIGNAL_INPUT_AXIS_TREADY = '1') then
                        if(my_counter <= (to_integer(unsigned(DATA_WINDOW))-1)) then
                            SIGNAL_INPUT_AXIS_TVALID <= '1';
                            if not endfile(signal_data_file) then
                                read (signal_data_file, signal_dec);
                                SIGNAL_INPUT_AXIS_TDATA<=std_logic_vector(to_signed(signal_dec, SIGNAL_INPUT_AXIS_TDATA'length));
                            end if;
                            if not endfile(signal_data_file) then
                                read (signal_data_file, signal_dec);
                            end if;
                            my_counter := my_counter + 1;
                            if(my_counter >= (to_integer(unsigned(DATA_WINDOW))-1)) then
                                SIGNAL_INPUT_AXIS_TLAST <= '1';
                                my_counter  := 0;
                                state_sm    <= check_decimation_end;
                            end if;                         
                        else
                            SIGNAL_INPUT_AXIS_TVALID    <= '0';
                            SIGNAL_INPUT_AXIS_TLAST     <= '0';
                            my_counter  := 0;
                            state_sm    <= check_decimation_end;           
                        end if;  
                    end if;
                    if(DEC_RESULT_AXIS_TLAST = '1') then
                        SIGNAL_INPUT_AXIS_TVALID    <= '0';
                        SIGNAL_INPUT_AXIS_TLAST     <= '0';
                        my_counter  := 0;
                        state_sm    <= read_result;
                    end if; 
                when check_decimation_end =>
                    SIGNAL_INPUT_AXIS_TVALID    <= '0';
                    SIGNAL_INPUT_AXIS_TLAST     <= '0';
                    state_sm <= read_result;
                when read_result =>
                    if(DEC_END = '1') then
                        READ_DEC_RESULT <= '1';
                        DEC_RESULT_AXIS_TREADY <= '1';  
                        state_sm <= read_result_end;
                    else
                        READ_DEC_RESULT <= '0';
                    end if;   
                when read_result_end =>
                    if(DEC_RESULT_AXIS_TLAST = '1') then
                        READ_DEC_RESULT <= '0';
                        DEC_RESULT_AXIS_TREADY <= '0';  
                        state_sm <= show_result;
                    end if; 
                when show_result =>
                    if(my_counter >= unsigned(data_window_result)) then
                        my_counter := 0;
                        decimation_result_on <= '0';
                        state_sm    <= close_files;  
                    else
                        decimation_result_on <= '1';
                        if(unsigned(DEC_TYPE_RESULT) = DEC_TYPE_SIMPLE) then
                            decimation_result_y <= decimation_result_array_i(my_counter);
                            my_counter := my_counter + 1;
                        elsif(unsigned(DEC_TYPE_RESULT) = DEC_TYPE_MINMAX) then
                            decimation_result_x <= decimation_result_array_i(my_counter);
                            decimation_result_y <= decimation_result_array_i(my_counter+1);
                            my_counter := my_counter + 2;
                        end if;
                    end if;    
                when close_files =>
                    if(my_counter >= 128) then
                        file_close(signal_data_file);
                        my_counter := 0;
                        state_sm <= open_files;   
                    else
                        my_counter := my_counter + 1;
                    end if;
                when others =>
                    null;                   
            end case;                                                                                            
        end if;
   end process; 
   
   store_inst: process(aclk, aresetn)  
      variable my_counter  : integer range 0 to 16383 :=0;                        
   begin 
       if (aresetn = '0') then
           DEC_RESULT_AXIS_TLAST_d1 <= '0';
           my_counter := 0;
       elsif (aclk'event and aclk = '1') then
           DEC_RESULT_AXIS_TLAST_d1 <= DEC_RESULT_AXIS_TLAST;
           if(DEC_RESULT_AXIS_TVALID = '1') then
--               decimation_result_array_i(my_counter) <= DEC_RESULT_AXIS_TDATA(REGISTER_W32-1 downto REGISTER_W16);
               decimation_result_array_i(my_counter) <= DEC_RESULT_AXIS_TDATA(REGISTER_W16-1 downto 0);
--               decimation_result_array_i(my_counter+1) <= DEC_RESULT_AXIS_TDATA(REGISTER_W16-1 downto 0);
               decimation_result_array_i(my_counter+1) <= DEC_RESULT_AXIS_TDATA(REGISTER_W32-1 downto REGISTER_W16);
               my_counter := my_counter + 2;
               data_window_result <= std_logic_vector(to_unsigned(my_counter, data_window_result'length));
           else
               if(DEC_RESULT_AXIS_TLAST_d1 = '1') then
                   my_counter := 0; 
               end if;
           end if;                                                                                         
       end if;
   end process; 
   
  
    uut: decimator_filter 
        generic map(
            REGISTER_W32        => REGISTER_W32,
            REGISTER_W16        => REGISTER_W16,
            REGISTER_W14        => REGISTER_W14,
            BRAM_ADDR_WIDTH     => BRAM_ADDR_WIDTH,
            BRAM_DATA_WIDTH     => BRAM_DATA_WIDTH,
            WAIT_CYCLES         => WAIT_CYCLES,
            N_INPUT_DELAY       => N_INPUT_DELAY,
            N_OUTPUT_DELAY      => N_OUTPUT_DELAY,
            DEC_TYPE_NONE       => DEC_TYPE_NONE,
            DEC_TYPE_SIMPLE     => DEC_TYPE_SIMPLE,
            DEC_TYPE_MINMAX     => DEC_TYPE_MINMAX,
            DEC_TYPE_ALOK       => DEC_TYPE_ALOK
        )
        port map(
            -- Sync
            aclk         => aclk,
            aresetn      => aresetn,
        
            -- Control
            NO_READ_DEC_RESULT => NO_READ_DEC_RESULT,   
            READ_DEC_RESULT => READ_DEC_RESULT,
            DEC_END         => DEC_END,
            DEC_EN          => DEC_EN,
            
            DEC_TYPE        => DEC_TYPE,   
            DEC_LEVEL       => DEC_LEVEL,
            DEC_WINDOW      => DEC_WINDOW,
            DATA_WINDOW     => DATA_WINDOW,
            
            DEC_TYPE_RESULT     => DEC_TYPE_RESULT,
            DEC_LEVEL_RESULT    => DEC_LEVEL_RESULT,
            DEC_WINDOW_RESULT   => DEC_WINDOW_RESULT,
            DEC_WINDOW_HALF_RESULT  => DEC_WINDOW_HALF_RESULT,
            
            -- Signal input
            SIGNAL_INPUT_AXIS_TREADY  => SIGNAL_INPUT_AXIS_TREADY,
            SIGNAL_INPUT_AXIS_TDATA   => SIGNAL_INPUT_AXIS_TDATA,
            SIGNAL_INPUT_AXIS_TLAST   => SIGNAL_INPUT_AXIS_TLAST,
            SIGNAL_INPUT_AXIS_TVALID  => SIGNAL_INPUT_AXIS_TVALID,
    
            -- Average result
            DEC_RESULT_AXIS_TVALID     => DEC_RESULT_AXIS_TVALID,
            DEC_RESULT_AXIS_TDATA      => DEC_RESULT_AXIS_TDATA,
            DEC_RESULT_AXIS_TLAST      => DEC_RESULT_AXIS_TLAST,
            DEC_RESULT_AXIS_TREADY     => DEC_RESULT_AXIS_TREADY
        );
              
end;