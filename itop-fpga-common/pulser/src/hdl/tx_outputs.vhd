----------------------------------------------------------------------------------
-- Company: Innerspec Technologies
-- Engineers: Swapna Muralidhar
--				  Zubin Kumar	
--				Wei Jiang
-- 
-- Design Name: 
-- Module Name: TX_OUTPUTS - Behavioral 
-- Project Name:
-- Target Devices: Virtex 5 xc5vsx50t-1ff1136
-- Tool versions: 
-- Description: Swapna Muralidhar - Generate TTL outputs for TX signal from 200 MHz clock
--					Zubin Kumar - Reduced dead time between positive and negative cycles
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Revision 0.02 - Rewrite to get rid of divider, wei jiang
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_SIGNED.ALL;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity tx_outputs is
	generic (
		REGISTER_W30    : INTEGER := 30;
		REGISTER_W8     : INTEGER := 8;
		REGISTER_W2		: INTEGER := 2;
		FREQ_PULSER_GEN : INTEGER := 100
	);
	port (
		-- Sync
		aclk		: in STD_LOGIC;
		aclk_2      : in STD_LOGIC;
		aresetn		: in STD_LOGIC;
		
		-- Inputs
		TX_ENABLE		: in STD_LOGIC;
		START_CALC		: in STD_LOGIC;
		START_TX		: in STD_LOGIC;
		HALF_CYCLE_FREQ	: in STD_LOGIC_VECTOR(REGISTER_W30-1 downto 0);
		NO_HALF_CYCLES	: in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
		START_CYCLE		: in STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
		
		-- Outputs
		TX_PLUS			: out STD_LOGIC;
		TX_MINUS		: out STD_LOGIC
	);
end tx_outputs;

architecture Behavioral of tx_outputs is

signal half_cycle_peroid_ps : STD_LOGIC_VECTOR(REGISTER_W30-1 downto 0) := (others => '0');
signal fpga_txclk_period_ps : STD_LOGIC_VECTOR(REGISTER_W30-1 downto 0) := (others => '0');
signal half_cycle_deadtime_ps: STD_LOGIC_VECTOR(REGISTER_W30-1 downto 0) := (others => '0');
signal extra_txclk_period_ps : STD_LOGIC_VECTOR(REGISTER_W30-1 downto 0) := (others => '0');

signal half_cycle_deadtime_ps1: STD_LOGIC_VECTOR(REGISTER_W30-1 downto 0) := (others => '0');
signal half_cycle_deadtime_ps2: STD_LOGIC_VECTOR(REGISTER_W30-1 downto 0) := (others => '0');

type states is (idle_state, first_dead_time_state, plus_state, dead_time_state, minus_state, end_state);
signal sm_state : states;

signal count_down_timer : STD_LOGIC_VECTOR(REGISTER_W30-1 downto 0) := (others => '0');
signal count_cycles : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0) := (others => '0');

signal tx_plus1_minus0_i : STD_LOGIC := '0';

signal TX_ENABLE_i1		    : STD_LOGIC;
signal START_TX_i1		    : STD_LOGIC;
signal HALF_CYCLE_FREQ_i1	: STD_LOGIC_VECTOR(REGISTER_W30-1 downto 0);
signal NO_HALF_CYCLES_i1	: STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
signal START_CYCLE_i1		: STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);

signal TX_ENABLE_i2		    : STD_LOGIC;
signal START_TX_i2		    : STD_LOGIC;
signal HALF_CYCLE_FREQ_i2	: STD_LOGIC_VECTOR(REGISTER_W30-1 downto 0);
signal NO_HALF_CYCLES_i2	: STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
signal START_CYCLE_i2		: STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);

begin

	half_cycle_peroid_ps <= HALF_CYCLE_FREQ_i2;

    fpga_txclk_period_ps <= "000000000000000010011100010000" when FREQ_PULSER_GEN = 100 else	-- 10ns clock
                            "000000000000000001001110001000" when FREQ_PULSER_GEN = 200 else	-- 5ns clock
                            "000000000000000000100111000100" when FREQ_PULSER_GEN = 400 else	-- 2.5ns clock
                            "000000000000000010011100010000"; -- 10ns clock by default
    --fpga_txclk_period_ps <= "000000000000000000100111000100";	-- 2.5ns clock
    --fpga_txclk_period_ps <= "000000000000000001001110001000";	-- 5ns clock
	--fpga_txclk_period_ps <= "000000000000000010011100010000";	-- 10,000 (10 ns x 1000) 100 MHz clock
	--half_cycle_deadtime_ps <= "000000000000000100111000100000";	-- 30,000 ( 20 ns total deadtime) 
	--half_cycle_deadtime_ps <= "000000000000000111010100110000";	-- 30,000 ( 20 ns total deadtime) 
	
--	half_cycle_deadtime_ps1 <= "000000000000000100111000100000";	-- 20,000  
--	half_cycle_deadtime_ps2 <= "000000000000000111010100110000";	-- 30,000  
	
	half_cycle_deadtime_ps1 <= "000000000000000110000110101000";	-- 25,000  
    half_cycle_deadtime_ps2 <= "000000000000000110000110101000";    -- 25,000  
	
	
	
	half_cycle_deadtime_ps <= std_logic_vector(unsigned(half_cycle_deadtime_ps1) + unsigned(half_cycle_deadtime_ps2));
	
	
	process (aclk_2)
	--process (aclk)
	begin
		if (rising_edge(aclk_2)) then
		--if (rising_edge(aclk)) then
			if (aresetn = '0') then
			    TX_PLUS <= '0';
                TX_MINUS <= '0';
				sm_state <= idle_state;
			else
				case sm_state is
					when idle_state =>
					    
						count_cycles <= (others => '0');
						TX_PLUS <= '0';
						TX_MINUS <= '0';
						extra_txclk_period_ps <= fpga_txclk_period_ps+fpga_txclk_period_ps+fpga_txclk_period_ps+fpga_txclk_period_ps;
						if (TX_ENABLE_i2 = '1' and START_TX_i2 = '1') then
                            count_down_timer <= fpga_txclk_period_ps; 
                            --count_down_timer <= half_cycle_deadtime_ps1 - fpga_txclk_period_ps; 
                            sm_state <= first_dead_time_state; 
                            if (START_CYCLE_i2 = "01") then
                                tx_plus1_minus0_i <= '1';
                            else
                                tx_plus1_minus0_i <= '0';
                            end if;
						else
							sm_state <= idle_state;
						end if;
						
				    when first_dead_time_state =>
				    
						TX_PLUS <= '0';
                        TX_MINUS <= '0';
                                            
                        if ( count_down_timer >= fpga_txclk_period_ps ) then
						    count_down_timer <= count_down_timer - fpga_txclk_period_ps;
                            sm_state <= dead_time_state;
                        else
							if (START_CYCLE_i2 = "01") then
                                -- 1 = start from negative
                                sm_state <= minus_state;
                            else
                                sm_state <= plus_state;
                            end if;
                            if(NO_HALF_CYCLES > 2) then
                                if(half_cycle_peroid_ps > half_cycle_deadtime_ps) then
                                    count_down_timer <= half_cycle_peroid_ps - half_cycle_deadtime_ps;         
                                else
                                    count_down_timer <= fpga_txclk_period_ps;    
                                end if;
                            else
                                if(half_cycle_peroid_ps > half_cycle_deadtime_ps) then
                                    count_down_timer <= half_cycle_peroid_ps - half_cycle_deadtime_ps;         
                                else
                                    count_down_timer <= fpga_txclk_period_ps;    
                                end if;
                            end if;
                        end if;    

					when plus_state =>

						tx_plus1_minus0_i <= '1';
						TX_PLUS <= '1';
						TX_MINUS <= '0';		
									
						if ( count_down_timer > fpga_txclk_period_ps ) then
						    count_down_timer <= count_down_timer - fpga_txclk_period_ps;
							sm_state <= plus_state;
						else
						    count_down_timer <= half_cycle_deadtime_ps - fpga_txclk_period_ps;
						    count_cycles <= count_cycles + 1;
						    if(extra_txclk_period_ps >= fpga_txclk_period_ps) then
						        extra_txclk_period_ps <= extra_txclk_period_ps - fpga_txclk_period_ps; 
						    end if;
							sm_state <= dead_time_state;
						end if;

					when dead_time_state =>

						TX_PLUS <= '0';
						TX_MINUS <= '0';
												
						if ( count_down_timer >= fpga_txclk_period_ps ) then
						  count_down_timer <= count_down_timer - fpga_txclk_period_ps;
						  sm_state <= dead_time_state;
						else
                            if (count_cycles < NO_HALF_CYCLES_i2) then
                                if(NO_HALF_CYCLES > 2) then
                                    if(half_cycle_peroid_ps > half_cycle_deadtime_ps) then
                                        count_down_timer <= half_cycle_peroid_ps - half_cycle_deadtime_ps;         
                                    else
                                        count_down_timer <= fpga_txclk_period_ps;    
                                    end if;
                                else
                                     if(half_cycle_peroid_ps > half_cycle_deadtime_ps) then
                                        count_down_timer <= half_cycle_peroid_ps - half_cycle_deadtime_ps;         
                                    else
                                        count_down_timer <= fpga_txclk_period_ps;    
                                    end if;
                                end if;
                                if (tx_plus1_minus0_i = '1') then
                                    sm_state <= minus_state;
                                else
                                    sm_state <= plus_state;
                                end if;
                            else
                                sm_state <= end_state;
                            end if;
					    end if;	
					    
					when minus_state =>

						tx_plus1_minus0_i <= '0';
						TX_PLUS <= '0';
						TX_MINUS <= '1';
						
						if ( count_down_timer > fpga_txclk_period_ps ) then
						    count_down_timer <= count_down_timer - fpga_txclk_period_ps;
							sm_state <= minus_state;
						else
						    count_down_timer <= half_cycle_deadtime_ps - fpga_txclk_period_ps;
						    count_cycles <= count_cycles + 1;
						    if(extra_txclk_period_ps >= fpga_txclk_period_ps) then
                                extra_txclk_period_ps <= extra_txclk_period_ps - fpga_txclk_period_ps; 
                            end if;
							sm_state <= dead_time_state;
						end if;

					when end_state =>

						TX_PLUS <= '0';
						TX_MINUS <= '0';
						if (START_TX_i2 = '1') then
							sm_state <= end_state;
						else
							sm_state <= idle_state;
						end if;

					when others =>

						TX_PLUS <= '0';
						TX_MINUS <= '0';				
						sm_state <= idle_state;

				end case;
			end if;
		end if;
	end process;	
		
	process (aclk_2)
    begin
        if (rising_edge(aclk_2)) then
            if (aresetn = '0') then
                TX_ENABLE_i1 <= '0';
                START_TX_i1 <= '0';
                HALF_CYCLE_FREQ_i1 <= (others => '0');
                NO_HALF_CYCLES_i1 <= (others => '0');
                START_CYCLE_i1 <= (others => '0');
                TX_ENABLE_i2 <= '0';
                START_TX_i2 <= '0';
                HALF_CYCLE_FREQ_i2 <= (others => '0');
                NO_HALF_CYCLES_i2 <= (others => '0');
                START_CYCLE_i2 <= (others => '0');
            else
                TX_ENABLE_i1 <= TX_ENABLE;
                START_TX_i1 <= START_TX;
                HALF_CYCLE_FREQ_i1 <= HALF_CYCLE_FREQ;
                NO_HALF_CYCLES_i1 <= NO_HALF_CYCLES;
                START_CYCLE_i1 <= START_CYCLE;
                TX_ENABLE_i2 <= TX_ENABLE_i1;
                START_TX_i2 <= START_TX_i1;
                HALF_CYCLE_FREQ_i2 <= HALF_CYCLE_FREQ_i1;
                NO_HALF_CYCLES_i2 <= NO_HALF_CYCLES_i1;
                START_CYCLE_i2 <= START_CYCLE_i1;
            end if;
        end if;
    end process;
    
    
-----------------------------------------------------------------------------
end Behavioral;

