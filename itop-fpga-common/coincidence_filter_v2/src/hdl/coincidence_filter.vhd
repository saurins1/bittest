----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 09/27/2017 10:07:23 AM
-- Design Name: 
-- Module Name: coincidence filter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity coincidence_filter is
	generic (
        REGISTER_W32    : INTEGER := 32;
        REGISTER_W21    : INTEGER := 21;
        REGISTER_W16    : INTEGER := 16;
        REGISTER_W8     : INTEGER := 8;
        BRAM_TREADY_SYNC: INTEGER := 2;
        TRIGGER_WAIT    : INTEGER := 32;
        MIN_COINC_LEVEL : INTEGER := 2;
        MIN_AVG_LEVEL   : INTEGER := 2
	);
	port (
	    -- Sync
	    aclk         : in STD_LOGIC;
        aresetn      : in STD_LOGIC;
	
	    -- Config
	    COINC_LEVEL	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	    DATA_WINDOW	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	    SAMPLE_FREQ	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		DATA_WINDOW_RESULT	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		SAMPLE_FREQ_RESULT	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		COINC_COUNTER     : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
		COINC_WINDOW  	  : out STD_LOGIC;

		-- Signal input
		SIGNAL_INPUT_AXIS_TREADY  : out STD_LOGIC;
		SIGNAL_INPUT_AXIS_TDATA	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		SIGNAL_INPUT_AXIS_TLAST	  : in STD_LOGIC;
		SIGNAL_INPUT_AXIS_TVALID  : in STD_LOGIC;

		-- Coincidence result
		COINC_RESULT_AXIS_TVALID  : out STD_LOGIC;
		COINC_RESULT_AXIS_TDATA	  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		COINC_RESULT_AXIS_TLAST	  : out STD_LOGIC;
		COINC_RESULT_AXIS_TREADY  : in STD_LOGIC;
		
		-- Coincidence DMA result
		AVG_TYPE	    		: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		AVG_LEVEL	            : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	    AVG_COUNTER     		: in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
	    AVG_SUBSTRACT_ON 		: in STD_LOGIC;
		
        READ_COINC_RESULT 		: in STD_LOGIC;
		NO_READ_COINC_RESULT 	: in STD_LOGIC;
        COINC_RESULT_READY  	: out STD_LOGIC;
        COINC_RESULT_SIZE   	: out STD_LOGIC_VECTOR(REGISTER_W21-1 downto 0);
		
		COINC_DMA_RESULT_AXIS_TVALID  : out STD_LOGIC;
		COINC_DMA_RESULT_AXIS_TDATA	  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		COINC_DMA_RESULT_AXIS_TLAST	  : out STD_LOGIC;
		COINC_DMA_RESULT_AXIS_TREADY  : in STD_LOGIC;
		COINC_DMA_RESULT_AXIS_TKEEP   : out STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0)
	);
end coincidence_filter;

architecture arch_imp of coincidence_filter is

    -- Delayed signal input
    type signal_input_axis_tdata_array is array ( 0 to 7 ) of STD_LOGIC_VECTOR(13 downto 0);
    signal signal_input_axis_tdata_i    : signal_input_axis_tdata_array;
    signal signal_input_axis_tlast_i    : STD_LOGIC_VECTOR(7 downto 0);
    signal signal_input_axis_tvalid_i   : STD_LOGIC_VECTOR(7 downto 0);

    -- Delayed coincidence result
    signal coinc_result_axis_tlast_i    : STD_LOGIC_VECTOR(3 downto 0);
    signal coinc_result_axis_tvalid_i   : STD_LOGIC_VECTOR(3 downto 0);

    -- BRAM
    COMPONENT coincidence_filter_BRAM
      PORT (
        clka : IN STD_LOGIC;
        ena : IN STD_LOGIC;
        wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addra : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        dina : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
        douta : OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
        clkb : IN STD_LOGIC;
        enb : IN STD_LOGIC;
        web : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addrb : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        dinb : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
        doutb : OUT STD_LOGIC_VECTOR(13 DOWNTO 0)
      );
    END COMPONENT;

    -- coincidence BRAM port a
    signal coincidence_bram_addra   : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal coincidence_bram_dina    : STD_LOGIC_VECTOR ( 13 downto 0 );
    signal coincidence_bram_douta   : STD_LOGIC_VECTOR ( 13 downto 0 );
    signal coincidence_bram_ena     : STD_LOGIC;
    signal coincidence_bram_wea     : STD_LOGIC_VECTOR ( 0 to 0 );

	signal coincidence_bram_addra_first : STD_LOGIC;
    
    -- coincidence BRAM port b
    signal coincidence_bram_addrb   : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal coincidence_bram_dinb    : STD_LOGIC_VECTOR ( 13 downto 0 );
    signal coincidence_bram_doutb   : STD_LOGIC_VECTOR ( 13 downto 0 );
    signal coincidence_bram_enb     : STD_LOGIC;
    signal coincidence_bram_web     : STD_LOGIC_VECTOR ( 0 to 0 );

	signal coincidence_bram_doutb_i   : STD_LOGIC_VECTOR ( 13 downto 0 );

	signal coincidence_bram_addrb_first : STD_LOGIC;
   
    -- Coincidence state machine
    type states_general_sm is (idle, by_pass, first_frame, next_frame, check_frame, read_result, wait_for_read_result_dma, read_result_dma); 
    signal state_general_sm : states_general_sm;

    -- BRAM porta state machine
	type states_bram_porta_sm is (idle, first_frame, next_frame); 
    signal state_bram_porta_sm : states_bram_porta_sm;

    -- BRAM portb state machine
	type states_bram_portb_sm is (idle, read_result, next_frame, read_result_dma); 
    signal state_bram_portb_sm : states_bram_portb_sm;
   
    -- handshake flags between state machines 
    signal first_frame_flag  : STD_LOGIC;
    signal first_frame_end   : STD_LOGIC;

	signal next_frame_flag  : STD_LOGIC;
    signal next_frame_end   : STD_LOGIC;

	signal read_result_flag  : STD_LOGIC;
    signal read_result_end   : STD_LOGIC;

	signal read_dma_result_flag  : STD_LOGIC;
    signal read_dma_result_end   : STD_LOGIC;
	signal read_dma_result_first : STD_LOGIC;
    
    signal signal_input_axis_tready_i1 : STD_LOGIC;
    signal signal_input_axis_tready_i2 : STD_LOGIC;
    signal signal_input_axis_tready_i3 : STD_LOGIC;

	signal input_output_bypass : STD_LOGIC;
	signal sign_change : STD_LOGIC;
	
	signal coinc_counter_i  : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
	signal data_window_result_i	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

    -- Upload coincidence result
    type states_coinc_dma_result_sm is (idle, read_coincidence_start, upload_coincidence_data_flank_down, 
        upload_coincidence_data_flank_up, check_tlast, upload_coincidence_end); 

    signal state_coinc_dma_result_sm : states_coinc_dma_result_sm;

	signal coinc_dma_result_axis_tdata_1   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal coinc_dma_result_axis_tdata_2   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal tready_flank_up_flag        	   : STD_LOGIC;
	signal coinc_dma_result_axis_tready_i1 : STD_LOGIC;
	signal wait_for_tlast_flag         	   : STD_LOGIC;

begin
	
	-- Tready in													
	SIGNAL_INPUT_AXIS_TREADY <= signal_input_axis_tready_i1 and signal_input_axis_tready_i2 and signal_input_axis_tready_i3;

	--COINC_RESULT_AXIS_TVALID <= coinc_result_axis_tvalid_i(2);
	--COINC_RESULT_AXIS_TLAST  <= coinc_result_axis_tlast_i(2);
	--COINC_RESULT_AXIS_TDATA  <= std_logic_vector(resize(signed(coincidence_bram_doutb), COINC_RESULT_AXIS_TDATA'length));						
	-- Coincidence result
    coinc_result_process: process(aclk, aresetn) 
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				COINC_RESULT_AXIS_TVALID <= '0';
				COINC_RESULT_AXIS_TLAST  <= '0';
				COINC_RESULT_AXIS_TDATA  <= (others => '0');												
        	else
				if(input_output_bypass = '1') then
					COINC_RESULT_AXIS_TVALID <= SIGNAL_INPUT_AXIS_TVALID;				
					COINC_RESULT_AXIS_TLAST  <= SIGNAL_INPUT_AXIS_TLAST;	
					COINC_RESULT_AXIS_TDATA  <= SIGNAL_INPUT_AXIS_TDATA;											
				else
					COINC_RESULT_AXIS_TVALID <= coinc_result_axis_tvalid_i(2);
					COINC_RESULT_AXIS_TLAST  <= coinc_result_axis_tlast_i(2);
					COINC_RESULT_AXIS_TDATA  <= std_logic_vector(resize(signed(coincidence_bram_doutb), COINC_RESULT_AXIS_TDATA'length));										
				end if;												
			end if;
		end if;
	end process;
																		
	DATA_WINDOW_RESULT <= data_window_result_i;

    -- Coincidence SM
    coinc_inst: process(aclk) 
        variable var_counter_wait  : integer range 0 to 65536 :=0;
        variable var_counter_coinc : integer range 0 to 65536 :=0; 
        variable var_counter_sample: integer range 0 to 65536 :=0;
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				signal_input_axis_tready_i1 <= '0';
				first_frame_flag        <= '0';
				next_frame_flag			<= '0';
				read_result_flag		<= '0';
				read_dma_result_flag	<= '0';
				COINC_RESULT_READY		<= '0';	
				COINC_RESULT_SIZE		<= (others => '0');													
				input_output_bypass	 	<= '0';
				data_window_result_i    <= (others => '0');	
				SAMPLE_FREQ_RESULT      <= (others => '0');
				COINC_COUNTER           <= (others => '0');
				COINC_WINDOW			<= '0';														
				state_general_sm <= idle;
        	else
				case state_general_sm is        
					when idle =>
						signal_input_axis_tready_i1 <= '1';
						var_counter_wait := 0;
						var_counter_coinc:= 0;
						var_counter_sample := 0;
						data_window_result_i <= DATA_WINDOW;
                        SAMPLE_FREQ_RESULT <= SAMPLE_FREQ;
						if(unsigned(COINC_LEVEL) >= MIN_COINC_LEVEL or unsigned(AVG_TYPE) > 0) then
							input_output_bypass	<= '0';								
						  	signal_input_axis_tready_i1 <= '1';
							if(SIGNAL_INPUT_AXIS_TVALID = '1') then
								COINC_WINDOW <= '1';											
								state_general_sm <= first_frame;	
							end if;
						else														
							input_output_bypass	<= '1';										
							signal_input_axis_tready_i1 <= COINC_RESULT_AXIS_TREADY;
							if(SIGNAL_INPUT_AXIS_TVALID = '1') then
							    COINC_WINDOW <= '1';	
							    state_general_sm <= by_pass;  		
							end if;
						end if;
--						if(SIGNAL_INPUT_AXIS_TVALID = '1') then
--                            data_window_result_i <= DATA_WINDOW;
--                            SAMPLE_FREQ_RESULT <= SAMPLE_FREQ;
--                        end if;
                    when by_pass =>
                        signal_input_axis_tready_i1 <= COINC_RESULT_AXIS_TREADY;                                     
                        if(SIGNAL_INPUT_AXIS_TLAST = '1') then
                            COINC_COUNTER <= (others => '0');
                            COINC_WINDOW <= '0';
                            state_general_sm <= idle;
                        else
					        if(var_counter_sample = 1) then
                                COINC_COUNTER <= std_logic_vector(to_unsigned(1, COINC_COUNTER'length));
                            end if; 
                            var_counter_sample := var_counter_sample + 1;
                        end if;    
					when first_frame =>
						if(first_frame_end = '1') then
							signal_input_axis_tready_i1 <= '0';
							first_frame_flag  <= '0';
							var_counter_coinc := var_counter_coinc + 1; 
							state_general_sm <= check_frame;
						 else
						    COINC_COUNTER <= coinc_counter_i;
							first_frame_flag  <= '1';
						end if;
					when next_frame =>
						if(next_frame_end = '1') then
							signal_input_axis_tready_i1 <= '0';	
							next_frame_flag  <= '0';
							var_counter_coinc := var_counter_coinc + 1;
							state_general_sm <= check_frame;
						else
						    COINC_COUNTER <= coinc_counter_i;
							signal_input_axis_tready_i1 <= '1';
							next_frame_flag <= '1';	
						end if;
					when check_frame =>
						if(var_counter_wait >= TRIGGER_WAIT) then
							var_counter_wait := 0;		
							if(unsigned(COINC_LEVEL) < MIN_COINC_LEVEL) then
								state_general_sm  <= read_result;
							else
								if(var_counter_coinc < unsigned(COINC_LEVEL)) then
									state_general_sm  <= next_frame;
								else
									state_general_sm  <= read_result;	
								end if;
							end if;		
						else
							var_counter_wait := var_counter_wait + 1;
							signal_input_axis_tready_i1 <= '0';	
						end if;						
					when read_result =>
					    COINC_COUNTER <= (others => '0');
                        COINC_WINDOW <= '0';												
						if(read_result_end = '1') then
							read_result_flag  <= '0';
							--if((unsigned(AVG_COUNTER) > 0 or unsigned(AVG_TYPE) > 0) and AVG_SUBSTRACT_ON = '1') then
							if((unsigned(AVG_COUNTER) > 0 or unsigned(AVG_TYPE) > 0) and unsigned(AVG_LEVEL) >= MIN_AVG_LEVEL) then											  
							    state_general_sm  <= wait_for_read_result_dma;
							else
							    state_general_sm  <= idle;
							end if;
						else
							read_result_flag  <= '1';
						end if;
					when wait_for_read_result_dma =>															  
						if((unsigned(AVG_COUNTER) > 0 and unsigned(AVG_TYPE) > 0) and NO_READ_COINC_RESULT = '0' and unsigned(AVG_LEVEL) >= MIN_AVG_LEVEL) then
							if(READ_COINC_RESULT = '1') then
								COINC_RESULT_READY <= '0';												  
								state_general_sm  <= read_result_dma;
							else
								COINC_RESULT_READY <= '1';												  
							end if;
							COINC_RESULT_SIZE  <= data_window_result_i(REGISTER_W21-1 downto 0);
						else
							state_general_sm  <= idle;
						end if;															  														  
					when read_result_dma =>												  
						if(read_dma_result_end = '1') then
							read_dma_result_flag <= '0';
							state_general_sm  <= idle;													  
						else
							read_dma_result_flag <= '1';														  
						end if;
					when others =>
						null;
				end case;
			end if;
         end if;
    end process;
    	 
	-- Write first frame
	-- Write coinc result
    signal_BRAM_porta: process(aclk) 
		variable var_counter_result : integer range 0 to 65536 :=0;
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                coincidence_bram_ena   	<= '1';
                coincidence_bram_wea   	<= (others => '0');
                coincidence_bram_addra 	<= (others => '0');
                coincidence_bram_dina  	<= (others => '0');
				coincidence_bram_addra_first <= '0';
                first_frame_end			<= '0';
				next_frame_end			<= '0';
				signal_input_axis_tready_i2 <= '0';
				sign_change <= '0';
				coinc_counter_i <= (others => '0');
                -- State
                state_bram_porta_sm    	<= idle;
            else     
                case state_bram_porta_sm is        
                    when idle =>
                        coincidence_bram_wea <= (others => '0');
						var_counter_result 	 := 0;
						coincidence_bram_addra_first <= '1';
						signal_input_axis_tready_i2 <= '1';
						if(state_general_sm = read_result or state_general_sm = idle) then
						    coinc_counter_i <= (others => '0'); 
						end if;
                        if(first_frame_flag = '1') then
							state_bram_porta_sm <= first_frame;
						elsif(next_frame_flag = '1') then
							state_bram_porta_sm <= next_frame;							
                        end if;
                    when first_frame =>
						if(first_frame_flag = '1') then
							if(signal_input_axis_tvalid_i(3) = '1') then
								if(var_counter_result >= unsigned(DATA_WINDOW)) then
									-- end
									first_frame_end   <= '1';
									signal_input_axis_tready_i2 <= '0';
									var_counter_result := to_integer(unsigned(unsigned(DATA_WINDOW)));	
								else
									-- write
									coincidence_bram_wea   <= (others => '1');
									-- addr
									if(coincidence_bram_addra_first = '1') then
										coincidence_bram_addra_first <= '0';
										coincidence_bram_addra 	<= (others => '0');	
									else
										coincidence_bram_addra <= std_logic_vector(unsigned(coincidence_bram_addra)+1);
									end if;
									-- din
									coincidence_bram_dina  <= signal_input_axis_tdata_i(3)(13 downto 0); 
									-- counter
									if(signal_input_axis_tlast_i(3) = '1') then
										first_frame_end   <= '1';
										signal_input_axis_tready_i2 <= '0';
										var_counter_result := to_integer(unsigned(unsigned(DATA_WINDOW)));
									else
									    if(var_counter_result = 1) then
									       coinc_counter_i <= std_logic_vector(unsigned(coinc_counter_i) + 1);
									    end if;
										var_counter_result := var_counter_result + 1;
									end if;
								end if;
							end if;
						else						    
							first_frame_end   	<= '0';
							state_bram_porta_sm <= idle;
						end if;
					when next_frame =>
						if(next_frame_flag = '1') then
							if(signal_input_axis_tvalid_i(4) = '1') then
								if(var_counter_result >= unsigned(DATA_WINDOW)) then
									-- end
									next_frame_end <= '1';
									signal_input_axis_tready_i2 <= '0';
									var_counter_result := to_integer(unsigned(unsigned(DATA_WINDOW)));
								else
									-- write
									coincidence_bram_wea <= (others => '1');
									-- addr
									if(coincidence_bram_addra_first = '1') then
										coincidence_bram_addra_first <= '0';
										coincidence_bram_addra 	<= (others => '0');
									else
										coincidence_bram_addra <= std_logic_vector(unsigned(coincidence_bram_addra)+1);	
									end if;
									-- din
									if(coincidence_bram_doutb(13) /= signal_input_axis_tdata_i(4)(13)) then
									    sign_change <= '1';
										coincidence_bram_dina  <= (others => '0');	
									else
									    sign_change <= '0';
										if(coincidence_bram_doutb(13) = '0') then
											if(signed(coincidence_bram_doutb) < signed(signal_input_axis_tdata_i(4))) then
												coincidence_bram_dina  <= coincidence_bram_doutb;		
											else
												coincidence_bram_dina  <= std_logic_vector(resize(signed(signal_input_axis_tdata_i(4)), coincidence_bram_dina'length));	
											end if;
										else
											if(signed(coincidence_bram_doutb) < signed(signal_input_axis_tdata_i(4))) then
												coincidence_bram_dina  <= std_logic_vector(resize(signed(signal_input_axis_tdata_i(4)), coincidence_bram_dina'length));			
											else
												coincidence_bram_dina  <= coincidence_bram_doutb;
											end if;	
										end if;
									end if;
									-- counter															  	
									if(signal_input_axis_tlast_i(4) = '1') then
                                        next_frame_end   <= '1';
                                        signal_input_axis_tready_i2 <= '0';
                                        var_counter_result := to_integer(unsigned(unsigned(DATA_WINDOW)));
                                    else
									    if(var_counter_result = 1) then
                                            coinc_counter_i <= std_logic_vector(unsigned(coinc_counter_i) + 1);
                                        end if;
                                        var_counter_result := var_counter_result + 1;
                                    end if;	
								end if;
							end if;
						else
						    sign_change <= '0';
							next_frame_end <= '0';																		
							state_bram_porta_sm <= idle;	
						end if;
                      
                    when others => 
                        null;
                end case;                                 
            end if;
        end if;
    end process;
			
	-- Read result
	-- Read previous frame for filter
    signal_BRAM_portb: process(aclk) 
		variable var_counter_result : integer range 0 to 65536 :=0; 
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				-- Bram
                coincidence_bram_enb   	<= '1';
                coincidence_bram_web   	<= (others => '0');
                coincidence_bram_addrb 	<= (others => '0');
                coincidence_bram_dinb  	<= (others => '0');
				coincidence_bram_addrb_first <= '0';
                read_result_end			<= '0';
				read_dma_result_first	<= '0';																			  
                signal_input_axis_tready_i3 <= '0';
				-- Coincidence result					
                coinc_result_axis_tlast_i(0)    <= '0';
                coinc_result_axis_tvalid_i(0)	<= '0';																				  
                -- State
                state_bram_portb_sm    	<= idle;
            else     																			  																	  
                case state_bram_portb_sm is        
                    when idle =>
						coincidence_bram_addrb_first <= '1';
						read_dma_result_first <= '1';																		  
						var_counter_result := 0;
						signal_input_axis_tready_i3 <= '1';
                        if(read_result_flag = '1') then
							state_bram_portb_sm <= read_result;
						elsif(next_frame_flag = '1') then
							state_bram_portb_sm <= next_frame;
						elsif(read_dma_result_flag = '1') then																  
							state_bram_portb_sm <= read_result_dma;																			  
                        end if;
                    when read_result =>
                        signal_input_axis_tready_i3 <= '0';
						if(read_result_flag = '1') then
							if(var_counter_result >= unsigned(DATA_WINDOW)) then
								-- end
								coinc_result_axis_tlast_i(0)    <= '0';
                				coinc_result_axis_tvalid_i(0)	<= '0';
								read_result_end <= '1';
							else
							    if(COINC_RESULT_AXIS_TREADY = '1') then
                                    -- addr
                                    if(coincidence_bram_addrb_first = '1') then
                                        coincidence_bram_addrb_first <= '0';
                                        coincidence_bram_addrb 	<= (others => '0');
                                    else
                                        coincidence_bram_addrb <= std_logic_vector(unsigned(coincidence_bram_addrb)+1);	
                                    end if;
                                    -- tvalid																	 
                                    coinc_result_axis_tvalid_i(0) <= '1';
                                    -- tlast
                                    if(var_counter_result = unsigned(DATA_WINDOW)-1) then
                                        coinc_result_axis_tlast_i(0) <= '1';	
                                    end if;
                                    -- counter
                                    var_counter_result := var_counter_result + 1;
                                else
                                    coinc_result_axis_tvalid_i(0) <= '0';
                                end if;      
							end if;
						else
							read_result_end   	<= '0';
							state_bram_portb_sm <= idle;
						end if;
					when next_frame =>
						if(next_frame_flag = '1') then
							if(signal_input_axis_tvalid_i(1) = '1') then
								if(var_counter_result >= unsigned(DATA_WINDOW)) then
									-- end
									signal_input_axis_tready_i3 <= '0';
									var_counter_result := to_integer(unsigned(unsigned(DATA_WINDOW)));																	  
								else
									-- addr
									if(coincidence_bram_addrb_first = '1') then
										coincidence_bram_addrb_first <= '0';
										coincidence_bram_addrb 	<= (others => '0');
									else
										coincidence_bram_addrb <= std_logic_vector(unsigned(coincidence_bram_addrb)+1);	
									end if;
									-- counter
									if(signal_input_axis_tlast_i(4) = '1') then
                                        signal_input_axis_tready_i3 <= '0';
                                        var_counter_result := to_integer(unsigned(unsigned(DATA_WINDOW)));
                                    else
                                        var_counter_result := var_counter_result + 1;
                                    end if;    																	  
								end if;
							end if;
						else
							state_bram_portb_sm <= idle;	
						end if;
					when read_result_dma =>
						if(read_dma_result_flag = '0') then														  
							state_bram_portb_sm <= idle;
						else														  
							if(COINC_DMA_RESULT_AXIS_TREADY = '1') then																	  
								if(read_dma_result_first = '1') then
									read_dma_result_first	<= '0';	
									coincidence_bram_addrb 	<= (others => '0');														  
								else
									coincidence_bram_addrb <= std_logic_vector(unsigned(coincidence_bram_addrb)+1); 															  									  
								end if;
								var_counter_result := var_counter_result + 1;															  																	  
							end if;
						end if;																			  
                    when others => 
                        null;
                end case;                                 
            end if;
        end if;
    end process;
																								  																								  																																										  									  																							 																								  
	-- State machine for upload coincidence result
    upload_coincidence_result_sm: process(aclk, aresetn) 
		variable var_coincidence_counter : integer range 0 to 65536 :=0;
		variable var_wait_counter : integer range 0 to 65536 :=0;																			 
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				read_dma_result_end	<= '0';																					  
				-- Coincidence AXIS														 														 
				COINC_DMA_RESULT_AXIS_TDATA  <= (others => '0');
				COINC_DMA_RESULT_AXIS_TKEEP  <= (others => '0');
				COINC_DMA_RESULT_AXIS_TVALID <= '0';
				COINC_DMA_RESULT_AXIS_TLAST <= '0';
				-- Sync BRAM delay with TREADY evolution		
				tready_flank_up_flag <= '0';
				coinc_dma_result_axis_tdata_1 <= (others => '0');	 
				coinc_dma_result_axis_tdata_2 <= (others => '0');																	 
				coinc_dma_result_axis_tready_i1 <= '0';
				wait_for_tlast_flag         <= '0';
				coincidence_bram_doutb_i <= (others => '0');													 
				-- state
				state_coinc_dma_result_sm <= idle;
        	else
				coinc_dma_result_axis_tready_i1 <= COINC_DMA_RESULT_AXIS_TREADY;
				coincidence_bram_doutb_i        <= coincidence_bram_doutb;															 
				case state_coinc_dma_result_sm is
					when idle =>														 
						if(read_dma_result_flag = '1') then
						    var_wait_counter := 0;		
						    var_coincidence_counter:= 0;
							state_coinc_dma_result_sm <= read_coincidence_start;													 
						end if;	
					when read_coincidence_start =>
                        COINC_DMA_RESULT_AXIS_TKEEP <= (others => '1');
                        if(COINC_DMA_RESULT_AXIS_TREADY = '1') then
                        	if(var_wait_counter >= BRAM_TREADY_SYNC) then
                            	state_coinc_dma_result_sm <= upload_coincidence_data_flank_down;
                            else
                                var_wait_counter := var_wait_counter + 1;
                            end if;	
                        end if;							  												 											 
					when upload_coincidence_data_flank_down =>															 														
                        if(COINC_DMA_RESULT_AXIS_TREADY = '1') then
							COINC_DMA_RESULT_AXIS_TVALID <= '1';													
                            if(tready_flank_up_flag = '1') then
                                COINC_DMA_RESULT_AXIS_TDATA <= coinc_dma_result_axis_tdata_2;
                                tready_flank_up_flag <= '0';
                            else
                                COINC_DMA_RESULT_AXIS_TDATA <= std_logic_vector(resize(signed(coincidence_bram_doutb), COINC_DMA_RESULT_AXIS_TDATA'length));																   
                            end if;
							if(var_coincidence_counter >= to_integer(unsigned(data_window_result_i))-1) then
								COINC_DMA_RESULT_AXIS_TLAST <= '1';
								state_coinc_dma_result_sm <= check_tlast;												
							else
								var_coincidence_counter := var_coincidence_counter + 1;													
							end if;
                        else
                            if(coinc_dma_result_axis_tready_i1 = '1') then
                                coinc_dma_result_axis_tdata_1 <= std_logic_vector(resize(signed(coincidence_bram_doutb), coinc_dma_result_axis_tdata_1'length));
                                var_wait_counter:= 0;
                                state_coinc_dma_result_sm <= upload_coincidence_data_flank_up;
                            end if;
                        end if;																														
					when upload_coincidence_data_flank_up =>																									
                        if(var_wait_counter = 0) then
                            coinc_dma_result_axis_tdata_2 <= std_logic_vector(resize(signed(coincidence_bram_doutb), coinc_dma_result_axis_tdata_2'length));
                        end if;
                        if(COINC_DMA_RESULT_AXIS_TREADY = '1' and coinc_dma_result_axis_tready_i1 = '0') then
							COINC_DMA_RESULT_AXIS_TVALID <= '1';													
                            if(var_wait_counter = 0) then
                                COINC_DMA_RESULT_AXIS_TDATA <= std_logic_vector(resize(signed(coincidence_bram_doutb), COINC_DMA_RESULT_AXIS_TDATA'length));
                            else
                                COINC_DMA_RESULT_AXIS_TDATA <= coinc_dma_result_axis_tdata_1;
                            end if;      
                            tready_flank_up_flag <= '1';
							if(var_coincidence_counter >= to_integer(unsigned(data_window_result_i))-1) then
								COINC_DMA_RESULT_AXIS_TLAST <= '1';
								state_coinc_dma_result_sm <= check_tlast;												
							else
								var_coincidence_counter := var_coincidence_counter + 1;		
								state_coinc_dma_result_sm <= upload_coincidence_data_flank_down;													
							end if;														
                        end if;	
                        if(var_wait_counter < 1) then
                            var_wait_counter := var_wait_counter + 1;
                        end if;	
                    when check_tlast =>
                        if(wait_for_tlast_flag = '0') then
                            if(COINC_DMA_RESULT_AXIS_TREADY = '0' and coinc_dma_result_axis_tready_i1 = '1') then
                                wait_for_tlast_flag <= '1';
                            else
                                COINC_DMA_RESULT_AXIS_TLAST  <= '0';    
                                COINC_DMA_RESULT_AXIS_TVALID <= '0';
                                COINC_DMA_RESULT_AXIS_TKEEP  <= (others => '0');
                                state_coinc_dma_result_sm    <= upload_coincidence_end;
                            end if;  
                        else
                            if(COINC_DMA_RESULT_AXIS_TREADY = '1') then
                                COINC_DMA_RESULT_AXIS_TLAST  <= '0';    
                                COINC_DMA_RESULT_AXIS_TVALID <= '0';
                                COINC_DMA_RESULT_AXIS_TKEEP  <= (others => '0');												   
                                state_coinc_dma_result_sm    <= upload_coincidence_end;
                            end if;
                        end if; 							
					when upload_coincidence_end => 																 
						tready_flank_up_flag <= '0';
						wait_for_tlast_flag <= '0';															   
						if(read_dma_result_flag = '0') then	
							read_dma_result_end	<= '0';															   
							state_coinc_dma_result_sm <= idle;
						else
							read_dma_result_end	<= '1';																	   
						end if;															
					when others =>
						null;
				end case;
			end if;
         end if;
    end process;																					  
																								  																			  																							  																			  																							  																						  																								  
    signal_input_axis_tdata_i(0) <= std_logic_vector(resize(signed(SIGNAL_INPUT_AXIS_TDATA), signal_input_axis_tdata_i(0)'length));
    signal_input_axis_tlast_i(0) <= SIGNAL_INPUT_AXIS_TLAST;
    signal_input_axis_tvalid_i(0) <= SIGNAL_INPUT_AXIS_TVALID;
       
    -- Delay signal input     
    delay_signal_input : for i in 1 to 7 generate
        process(aclk) 
        begin
           if (rising_edge(aclk)) then
                if (aresetn = '0') then
                    signal_input_axis_tdata_i(i)    <= (others => '0');
                    signal_input_axis_tlast_i(i)    <= '0';
                    signal_input_axis_tvalid_i(i)   <= '0';
                else
                    signal_input_axis_tdata_i(i) <= signal_input_axis_tdata_i(i-1);
                    signal_input_axis_tlast_i(i) <= signal_input_axis_tlast_i(i-1);
                    signal_input_axis_tvalid_i(i) <= signal_input_axis_tvalid_i(i-1);
                end if;
           end if;
        end process;
    end generate;
			    
    -- Delay coincidence result     
    delay_coinc_result : for i in 1 to 3 generate
        process(aclk) 
        begin
           if (rising_edge(aclk)) then
                if (aresetn = '0') then
                    coinc_result_axis_tlast_i(i)    <= '0';
                    coinc_result_axis_tvalid_i(i)   <= '0';
                else
                    coinc_result_axis_tlast_i(i) <= coinc_result_axis_tlast_i(i-1);
                    coinc_result_axis_tvalid_i(i) <= coinc_result_axis_tvalid_i(i-1);
                end if;
           end if;
        end process;
    end generate;		   			  
      
    -- BRAM
    coincidence_filter_BRAM_inst : coincidence_filter_BRAM
      PORT MAP (
        clka => aclk,
        ena => coincidence_bram_ena,
        wea => coincidence_bram_wea,
        addra => coincidence_bram_addra,
        dina => coincidence_bram_dina,
        douta => coincidence_bram_douta,
        clkb => aclk,
        enb => coincidence_bram_enb,
        web => coincidence_bram_web,
        addrb => coincidence_bram_addrb,
        dinb => coincidence_bram_dinb,
        doutb => coincidence_bram_doutb
      );
      
end arch_imp;
