----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 05/09/2018 10:07:23 AM
-- Design Name: 
-- Module Name: coincidence filter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity coincidence_filter is
	generic (
        REGISTER_W32    : INTEGER := 32;
        REGISTER_W16    : INTEGER := 16;
        REGISTER_W8     : INTEGER := 8;
        BRAM_ADDR_WIDTH           : INTEGER := 16;
        COINC_TYPE_NORMAL         : INTEGER := 0;
        COINC_TYPE_ACC            : INTEGER := 3;
        TRIGGER_WAIT    : INTEGER := 32;
        MIN_COINC_LEVEL : INTEGER := 2
	);
	port (
	    -- Sync
	    aclk         : in STD_LOGIC;
        aresetn      : in STD_LOGIC;
	
	    -- Config
	    COINC_LEVEL	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	    COINC_RESET     : in STD_LOGIC;
        COINC_RESET_ACK : out STD_LOGIC;
        
        COINC_TYPE	     : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        COINC_ACC  	     : out STD_LOGIC;
	    
		PHASE_SHIFT_LEVEL     : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        PHASE_SHIFT           : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        N_BURST               : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	    	    
	    DATA_WINDOW	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	    SAMPLE_FREQ	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	    
		DATA_WINDOW_RESULT	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		SAMPLE_FREQ_RESULT	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		COINC_TYPE_RESULT	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		COINC_LEVEL_RESULT	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		COINC_COUNTER     : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		COINC_WINDOW  	  : out STD_LOGIC;
		
		FIR_DELAY        : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        FIR_DELAY_ON     : in STD_LOGIC;

		-- Signal input
		SIGNAL_INPUT_AXIS_TREADY  : out STD_LOGIC;
		SIGNAL_INPUT_AXIS_TDATA	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		SIGNAL_INPUT_AXIS_TLAST	  : in STD_LOGIC;
		SIGNAL_INPUT_AXIS_TVALID  : in STD_LOGIC;

		-- Coincidence result
		COINC_RESULT_AXIS_TVALID  : out STD_LOGIC;
		COINC_RESULT_AXIS_TDATA	  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		COINC_RESULT_AXIS_TLAST	  : out STD_LOGIC;
		COINC_RESULT_AXIS_TREADY  : in STD_LOGIC
	);
end coincidence_filter;

architecture arch_imp of coincidence_filter is

    -- Delayed signal input
    type signal_input_axis_tdata_array is array ( 0 to 7 ) of STD_LOGIC_VECTOR(13 downto 0);
    signal signal_input_axis_tdata_i    : signal_input_axis_tdata_array;
    signal signal_input_axis_tlast_i    : STD_LOGIC_VECTOR(7 downto 0);
    signal signal_input_axis_tvalid_i   : STD_LOGIC_VECTOR(7 downto 0);

	signal signal_input_axis_tdata_neg  : STD_LOGIC_VECTOR(13 downto 0);
	signal signal_input_axis_tdata_abs  : STD_LOGIC_VECTOR(13 downto 0);

    -- Delayed coincidence result
    signal coinc_result_axis_tlast_i    : STD_LOGIC_VECTOR(3 downto 0);
    signal coinc_result_axis_tvalid_i   : STD_LOGIC_VECTOR(3 downto 0);
  
    -- BRAM
    COMPONENT coincidence_filter_BRAM
      PORT (
        clka : IN STD_LOGIC;
        ena : IN STD_LOGIC;
        wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addra : IN STD_LOGIC_VECTOR(BRAM_ADDR_WIDTH-1 DOWNTO 0);
        dina : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
        douta : OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
        clkb : IN STD_LOGIC;
        enb : IN STD_LOGIC;
        web : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addrb : IN STD_LOGIC_VECTOR(BRAM_ADDR_WIDTH-1 DOWNTO 0);
        dinb : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
        doutb : OUT STD_LOGIC_VECTOR(13 DOWNTO 0)
      );
    END COMPONENT;
    
    type coincidence_bram_addr is array ( 0 to 3 ) of STD_LOGIC_VECTOR(BRAM_ADDR_WIDTH-1 downto 0);
    type coincidence_bram_d is array ( 0 to 3 ) of STD_LOGIC_VECTOR(13 downto 0);
    type coincidence_bram_we is array ( 0 to 3 ) of STD_LOGIC_VECTOR(0 downto 0);
    
    -- coincidence BRAM port a
    signal coincidence_bram_addra   : STD_LOGIC_VECTOR(BRAM_ADDR_WIDTH-1 downto 0);
    signal coincidence_bram_dina    : STD_LOGIC_VECTOR(13 downto 0);
    signal coincidence_bram_douta   : STD_LOGIC_VECTOR(13 downto 0);
    signal coincidence_bram_ena     : STD_LOGIC;
    signal coincidence_bram_wea     : STD_LOGIC_VECTOR(0 downto 0);

	signal coincidence_bram_addra_first : STD_LOGIC;
    
    -- coincidence BRAM port b
    signal coincidence_bram_addrb   : STD_LOGIC_VECTOR(BRAM_ADDR_WIDTH-1 downto 0);
    signal coincidence_bram_dinb    : STD_LOGIC_VECTOR(13 downto 0);
    signal coincidence_bram_doutb   : STD_LOGIC_VECTOR(13 downto 0);
    signal coincidence_bram_doutb_d1: STD_LOGIC_VECTOR(13 downto 0);
    signal coincidence_bram_enb     : STD_LOGIC;
    signal coincidence_bram_web     : STD_LOGIC_VECTOR(0 downto 0);

	signal coincidence_bram_addrb_first : STD_LOGIC;
	  
    -- Coincidence state machine
    type states_general_sm is (idle, by_pass, first_frame, next_frame, check_frame, check_acc, check_shift, read_result,
                               read_fir_delay, internal_reset); 
    signal state_general_sm : states_general_sm;
    
    -- Data input state machine
    type states_input_sm is (idle, discard_samples, get_samples, padding_samples); 
    signal state_input_sm : states_input_sm; 

    -- BRAM porta state machine
	type states_bram_porta_sm is (idle, first_frame, next_frame); 
    signal state_bram_porta_sm : states_bram_porta_sm;

    -- BRAM portb state machine
	type states_bram_portb_sm is (idle, read_result, read_fir_delay, next_frame); 
    signal state_bram_portb_sm : states_bram_portb_sm;
   
    -- handshake flags between state machines 
    signal first_frame_flag  : STD_LOGIC;
    signal first_frame_end   : STD_LOGIC;

	signal next_frame_flag  : STD_LOGIC;
    signal next_frame_end   : STD_LOGIC;
    signal next_frame_offset: INTEGER range 0 to 128;
    
	signal read_result_flag  : STD_LOGIC;
    signal read_result_end   : STD_LOGIC;
    
    signal signal_input_axis_tready_i  : STD_LOGIC;
    signal signal_input_axis_tready_d1 : STD_LOGIC;
    signal signal_input_axis_tready_d2 : STD_LOGIC;
    signal signal_input_axis_tready_i1 : STD_LOGIC;
    signal signal_input_axis_tready_i2 : STD_LOGIC;
    signal signal_input_axis_tready_i3 : STD_LOGIC;
    signal signal_input_axis_tready_i4 : STD_LOGIC;

	signal input_output_bypass : STD_LOGIC;
	signal sign_change : STD_LOGIC;
			
	signal coinc_counter_i  : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	
	signal coinc_type_result_i	 	 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal coinc_level_result_i	 	 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal data_window_result_i      : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal data_window_read	         : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    
    signal noise_filter_on_i : STD_LOGIC;
    signal advanced_filter_on_i : STD_LOGIC;
    signal phase_shift_level_i  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal phase_shift_i        : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal n_burst_i            : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    
    signal phase_shift_counter  : UNSIGNED(REGISTER_W32-1 downto 0);
    signal phase_shift_threshold: UNSIGNED(REGISTER_W32-1 downto 0);
    
    -- AVG reset
    signal internal_reset_on 		: STD_LOGIC;
    signal internal_reset_on_ack    : STD_LOGIC;    
    signal coinc_level_d1	 	    : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal n_burst_d1	 	        : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal data_window_d1	        : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    signal sample_freq_d1           : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    signal coinc_type_d1	   	    : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
       
    constant all_ones  : STD_LOGIC_VECTOR(13 downto 0) := (others => '1');
    constant all_zeros  : STD_LOGIC_VECTOR(13 downto 0) := (others => '0');
    
	-- FIR delay
    signal fir_delay_on_i      : STD_LOGIC;
    
	type absolute_value_array is array ( 0 to 1 ) of STD_LOGIC_VECTOR(13 downto 0);
    signal coincidence_absolute_value : absolute_value_array;
    
begin
	
	-- Tready in													
	signal_input_axis_tready_i <= signal_input_axis_tready_i1 and signal_input_axis_tready_i2 and signal_input_axis_tready_i3 and signal_input_axis_tready_i4;
	SIGNAL_INPUT_AXIS_TREADY <= signal_input_axis_tready_i and signal_input_axis_tready_d1 and signal_input_axis_tready_d2;
				
	-- Coincidence result
    coinc_result_process: process(aclk, aresetn) 
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				COINC_RESULT_AXIS_TVALID <= '0';
				COINC_RESULT_AXIS_TLAST  <= '0';
				COINC_RESULT_AXIS_TDATA  <= (others => '0');												
        	else
				if(input_output_bypass = '1') then
					COINC_RESULT_AXIS_TVALID <= SIGNAL_INPUT_AXIS_TVALID;				
					COINC_RESULT_AXIS_TLAST  <= SIGNAL_INPUT_AXIS_TLAST;	
					COINC_RESULT_AXIS_TDATA  <= SIGNAL_INPUT_AXIS_TDATA;											
				else
                    COINC_RESULT_AXIS_TVALID <= coinc_result_axis_tvalid_i(2);
                    COINC_RESULT_AXIS_TLAST  <= coinc_result_axis_tlast_i(2);
                    COINC_RESULT_AXIS_TDATA  <= std_logic_vector(resize(signed(coincidence_bram_doutb), COINC_RESULT_AXIS_TDATA'length));  							
				end if;												
			end if;
		end if;
	end process;
		
    -- Internal Reset
    internal_reset_process: process(aclk) 
        variable my_counter : integer range 0 to 8192 :=0;                                                        
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                internal_reset_on   <= '0';
                coinc_level_d1      <= (others => '0');
                n_burst_d1          <= (others => '0');
                data_window_d1      <= (others => '0');    
                sample_freq_d1      <= (others => '0');  
                coinc_type_d1  	    <= (others => '0');	                                                           
            else
                n_burst_d1     <= N_BURST;
                data_window_d1 <= DATA_WINDOW;    
                sample_freq_d1 <= SAMPLE_FREQ;
                coinc_level_d1 <= COINC_LEVEL; 
                coinc_type_d1  <= COINC_TYPE;	                                                                                               
                if(n_burst_d1 /= N_BURST or data_window_d1 /= DATA_WINDOW or 
                   sample_freq_d1 /= SAMPLE_FREQ or coinc_level_d1 /= COINC_LEVEL or 
                   coinc_type_d1 /= COINC_TYPE) then                                
                    internal_reset_on <= '1';    
                else
                    if(internal_reset_on_ack = internal_reset_on) then
                        internal_reset_on <= '0';                                                  
                    end if;
                end if;
            end if;
        end if;
    end process;
	
	COINC_TYPE_RESULT <= coinc_type_result_i;
	DATA_WINDOW_RESULT <= data_window_result_i;
	
    -- Coincidence SM
    coinc_inst: process(aclk) 
        variable var_counter_wait  : integer range 0 to 65536 :=0;
        variable var_counter_coinc : integer range 0 to 65536 :=0; 
        variable var_counter_sample: integer range 0 to 65536 :=0;
        variable var_counter_fir_delay  : integer range 0 to 65536 :=0;
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				signal_input_axis_tready_i1 <= '0';
				first_frame_flag         <= '0';
				next_frame_flag			 <= '0';
				next_frame_offset        <=  0;
				read_result_flag		 <= '0';
				input_output_bypass		 <= '0';	
				data_window_result_i     <= (others => '0');	
				SAMPLE_FREQ_RESULT       <= (others => '0');
				COINC_COUNTER            <= (others => '0');
				COINC_WINDOW			 <= '0';
				noise_filter_on_i        <= '0';
				advanced_filter_on_i     <= '0';
				phase_shift_level_i      <= (others => '0');	
                phase_shift_i            <= (others => '0');
                n_burst_i                <= (others => '0');
                phase_shift_threshold    <= (others => '0');
                coinc_level_result_i     <= (others => '0');
				var_counter_sample       := 0;		
				COINC_RESET_ACK          <= '0';
				internal_reset_on_ack	 <= '0';				
				COINC_LEVEL_RESULT      <= std_logic_vector(to_unsigned(1, COINC_LEVEL_RESULT'length)); 	
				COINC_ACC               <= '0';
				coinc_type_result_i     <= std_logic_vector(to_unsigned(0, COINC_LEVEL_RESULT'length)); 	
				fir_delay_on_i          <= '0';		
				state_general_sm        <= idle;
        	else
				case state_general_sm is        
					when idle =>
						var_counter_wait := 0;
						var_counter_sample := 0;        
						COINC_COUNTER <= (others => '0');              
						if(internal_reset_on = '0' and COINC_RESET = '0' and coinc_level_d1 = COINC_LEVEL and n_burst_d1 = N_BURST and
                          data_window_d1 = DATA_WINDOW and sample_freq_d1 = SAMPLE_FREQ and 
                          n_burst_d1 = N_BURST and coinc_type_d1 = COINC_TYPE) then    
                            data_window_result_i <= DATA_WINDOW;
                            SAMPLE_FREQ_RESULT <= SAMPLE_FREQ;     
                            coinc_type_result_i <= COINC_TYPE;                
                            phase_shift_level_i <= PHASE_SHIFT_LEVEL;
                            phase_shift_i <= PHASE_SHIFT;
                            n_burst_i <= N_BURST; 
                            phase_shift_counter <= (others => '0');
                            phase_shift_threshold <= unsigned(PHASE_SHIFT);  
                            signal_input_axis_tready_i1 <= COINC_RESULT_AXIS_TREADY;                                     
                            if(unsigned(N_BURST) >= MIN_COINC_LEVEL) then
                                if(unsigned(PHASE_SHIFT_LEVEL) > 0) then
                                    advanced_filter_on_i <= '1';                                   
                                else
                                    advanced_filter_on_i <= '0';                                    
                                end if;
                                noise_filter_on_i    <= '0'; 
                                coinc_level_result_i <= N_BURST;
                                COINC_LEVEL_RESULT <= N_BURST;
                                input_output_bypass	<= '0';
                                var_counter_coinc := 0;
                                if(SIGNAL_INPUT_AXIS_TVALID = '1') then
                                    COINC_WINDOW <= '1';
                                    state_general_sm <= first_frame;      
                                end if;
                            elsif(unsigned(COINC_LEVEL) >= MIN_COINC_LEVEL) then
                                advanced_filter_on_i <= '0';
                                noise_filter_on_i    <= '1';
                                coinc_level_result_i <= COINC_LEVEL;
                                COINC_LEVEL_RESULT <= std_logic_vector(to_unsigned(1, COINC_LEVEL_RESULT'length));
                                input_output_bypass	<= '0';
                                if(SIGNAL_INPUT_AXIS_TVALID = '1') then
                                    COINC_WINDOW <= '1';
                                    if(unsigned(coinc_counter_i) = 0) then
                                        state_general_sm <= first_frame;
                                    else                                            
                                        next_frame_offset <= 2; 
                                        state_general_sm <= next_frame;
                                    end if;
                                end if;
                            else
                                advanced_filter_on_i <= '0';
                                noise_filter_on_i    <= '0';
                                coinc_level_result_i <= std_logic_vector(to_unsigned(1, coinc_level_result_i'length));
                                COINC_LEVEL_RESULT <= std_logic_vector(to_unsigned(1, COINC_LEVEL_RESULT'length));
                                input_output_bypass	<= '0';
                                --input_output_bypass	<= '1';	
                                var_counter_coinc := 0;									
                                if(SIGNAL_INPUT_AXIS_TVALID = '1') then                                    
                                    COINC_WINDOW <= '1';
                                    --state_general_sm <= by_pass;    
                                    state_general_sm <= first_frame;                                   
                                end if;
                            end if;  
                        else
                            advanced_filter_on_i <= '0';
                            noise_filter_on_i    <= '0';
                            COINC_WINDOW <= '0';
                            signal_input_axis_tready_i1 <= '0';
                            var_counter_coinc:= 0;
                            state_general_sm <= internal_reset;
                        end if;
                    when by_pass => 
                        signal_input_axis_tready_i1 <= COINC_RESULT_AXIS_TREADY;
					    if(SIGNAL_INPUT_AXIS_TLAST = '1') then
                            var_counter_sample := 0;
                            COINC_COUNTER <= (others => '0');
                            COINC_WINDOW <= '0';
                            state_general_sm <= idle;
                        else
							if(var_counter_sample = 1) then
                                COINC_COUNTER <= std_logic_vector(to_unsigned(1, COINC_COUNTER'length));
                            end if;    
                            var_counter_sample := var_counter_sample + 1;
                        end if;
					when internal_reset => 																
                        internal_reset_on_ack <= internal_reset_on;
                        COINC_RESET_ACK <= COINC_RESET;                                                       
                        if(internal_reset_on = '0' and COINC_RESET = '0') then
                            state_general_sm <= idle;                                                            
                        end if;                    
					when first_frame =>
						if(first_frame_end = '1') then
							signal_input_axis_tready_i1 <= '0';
							first_frame_flag  <= '0';
							var_counter_coinc := var_counter_coinc + 1; 
							state_general_sm <= check_frame;
						 else
						    COINC_COUNTER <= coinc_counter_i;
							first_frame_flag  <= '1';
						end if;
					when next_frame =>
						if(next_frame_end = '1') then
							signal_input_axis_tready_i1 <= '0';	
							next_frame_flag  <= '0';
							var_counter_coinc := var_counter_coinc + 1;
							state_general_sm <= check_frame;
						else
						    COINC_COUNTER <= coinc_counter_i;
							signal_input_axis_tready_i1 <= '1';
							next_frame_flag <= '1';	
							if(SIGNAL_INPUT_AXIS_TVALID = '1') then
                                COINC_WINDOW <= '1';  
                            end if;      
						end if;
					when check_frame =>
						if(var_counter_wait >= TRIGGER_WAIT) then
							var_counter_wait := 0;		
							if(unsigned(coinc_level_result_i) < MIN_COINC_LEVEL) then
								state_general_sm  <= read_result;
							else
								if(var_counter_coinc < unsigned(coinc_level_result_i)) then
								    if(unsigned(N_BURST) >= MIN_COINC_LEVEL) then
								        next_frame_offset <=  0; 
								        state_general_sm  <= next_frame;
								    else
                                        state_general_sm  <= check_acc;
								    end if;
								else
								    if(advanced_filter_on_i = '1') then								       
									   state_general_sm  <= check_shift;
									else
									   state_general_sm  <= read_result;
									end if;	
								end if;
							end if;		
						else
							var_counter_wait := var_counter_wait + 1;
							signal_input_axis_tready_i1 <= '0';	
						end if;	
				    when check_acc =>
                        if(var_counter_wait >= TRIGGER_WAIT) then
                            var_counter_wait := 0;    
                            state_general_sm <= idle;   
                        else
							if(unsigned(coinc_type_result_i) = COINC_TYPE_ACC) then
                                if(var_counter_wait <= 3) then
                                    COINC_ACC             <= '1';
                                else
                                    COINC_ACC             <= '0';
                                end if;
                            end if;
                            var_counter_wait := var_counter_wait + 1;
                        end if;
				    when check_shift =>
				        if(phase_shift_counter >= unsigned(phase_shift_level_i)) then
				            COINC_WINDOW <= '0';
				            state_general_sm  <= read_result;
				        else	
				            COINC_WINDOW <= '0';
				            if(var_counter_wait >= TRIGGER_WAIT) then		        
                                var_counter_coinc := 0;
                                var_counter_wait := 0;
                                phase_shift_counter <= phase_shift_counter + 1;
                                if(phase_shift_counter >= 1) then
                                    phase_shift_threshold <= phase_shift_threshold(REGISTER_W32-2 downto 0) & "0";
                                end if;
                                state_general_sm  <= next_frame;
                            else
                                var_counter_wait := var_counter_wait + 1;
                            end if;
				        end if; 				
					when read_result =>
					    COINC_COUNTER <= (others => '0');
                        COINC_WINDOW <= '0';											
						if(read_result_end = '1') then
							read_result_flag  <= '0';
							var_counter_coinc := 0;						
							if(FIR_DELAY_ON = '1') then                             
                                state_general_sm  <= read_fir_delay;
                            else
                                state_general_sm  <= idle;
                            end if;
						 else
							read_result_flag  <= '1';
						end if;
				    when read_fir_delay =>
                        if(var_counter_wait >= TRIGGER_WAIT) then
                            if(read_result_end = '1') then
                                if(var_counter_fir_delay >= TRIGGER_WAIT) then    
                                    read_result_flag  <= '0';
                                    fir_delay_on_i <= '0';
                                    var_counter_wait := 0;
                                    state_general_sm  <= idle; 
                                else
                                    var_counter_fir_delay:= var_counter_fir_delay + 1;
                                end if;  
                            else
                                fir_delay_on_i <= '1';
                                read_result_flag  <= '1';   
                            end if; 
                        else
                            var_counter_fir_delay := 0;
                            var_counter_wait:= var_counter_wait + 1;
                        end if; 
					when others =>
						null;
				end case;
			end if;
         end if;
    end process;
        
    -- Signal input    
    signal_input_process:process(aclk)
       variable var_counter_phase_shift : integer range 0 to 65536 :=0;
       variable var_next_frame_offset : integer range 0 to 128 :=0;
    begin
       if (rising_edge(aclk)) then
            if (aresetn = '0') then
                signal_input_axis_tdata_i(0)    <= (others => '0');
                signal_input_axis_tlast_i(0)    <= '0';
                signal_input_axis_tvalid_i(0)   <= '0';
				signal_input_axis_tdata_neg 	<= (others => '0');	
				signal_input_axis_tdata_abs	    <= (others => '0');														  
                var_counter_phase_shift := 0;
                signal_input_axis_tready_i4 <= '0';
                signal_input_axis_tready_d1 <= '0';
                signal_input_axis_tready_d2 <= '0';
                var_next_frame_offset := 0;
                state_input_sm <= idle;
            else
                var_next_frame_offset := next_frame_offset;
                signal_input_axis_tready_d1 <= signal_input_axis_tready_i;
                signal_input_axis_tready_d2 <= signal_input_axis_tready_d1;
				signal_input_axis_tdata_neg <= std_logic_vector(-signed(signal_input_axis_tdata_i(2+var_next_frame_offset)));		
				if(signal_input_axis_tdata_i(2)(13) = '0') then
				    signal_input_axis_tdata_abs <= signal_input_axis_tdata_i(2);
				else
				    signal_input_axis_tdata_abs <= std_logic_vector(-signed(signal_input_axis_tdata_i(2)));	
				end if;											  
                case state_input_sm is        
                    when idle =>
                        var_counter_phase_shift := 1;
                        signal_input_axis_tready_i4 <= '1';
                        if(unsigned(phase_shift_counter) > 0 and unsigned(phase_shift_i) > 0) then
                            signal_input_axis_tdata_i(0) <= (others => '0');
                            signal_input_axis_tlast_i(0) <= '0';
                            signal_input_axis_tvalid_i(0)<= '0';
                            if(SIGNAL_INPUT_AXIS_TVALID = '1') then
                                state_input_sm <= discard_samples;
                            end if;
                        else
                            signal_input_axis_tdata_i(0) <= std_logic_vector(resize(signed(SIGNAL_INPUT_AXIS_TDATA), signal_input_axis_tdata_i(0)'length));
                            signal_input_axis_tlast_i(0) <= SIGNAL_INPUT_AXIS_TLAST;
                            signal_input_axis_tvalid_i(0)<= SIGNAL_INPUT_AXIS_TVALID;
                        end if;
                    when discard_samples =>
                        if(SIGNAL_INPUT_AXIS_TVALID = '1') then
                            if(var_counter_phase_shift >= (phase_shift_threshold-1)) then
                                var_counter_phase_shift := 1;   
                                state_input_sm <= get_samples;
                            else
                                var_counter_phase_shift := var_counter_phase_shift + 1; 
                            end if; 
                        end if;
                    when get_samples =>
                        if(phase_shift_counter(0) = '0') then
                            signal_input_axis_tdata_i(0) <= std_logic_vector(resize(signed(SIGNAL_INPUT_AXIS_TDATA), signal_input_axis_tdata_i(0)'length));
                        else
                            signal_input_axis_tdata_i(0) <= std_logic_vector(resize(-signed(SIGNAL_INPUT_AXIS_TDATA), signal_input_axis_tdata_i(0)'length));
                        end if;
                        signal_input_axis_tlast_i(0) <= '0';
                        signal_input_axis_tvalid_i(0)<= SIGNAL_INPUT_AXIS_TVALID;
                        if(SIGNAL_INPUT_AXIS_TLAST = '1') then
                            signal_input_axis_tready_i4 <= '0';
                            state_input_sm <= padding_samples;
                        end if;
                    when padding_samples =>
                        if(var_counter_phase_shift > phase_shift_threshold) then
                            var_counter_phase_shift := 0;
                            signal_input_axis_tvalid_i(0)<= '0';
                            signal_input_axis_tlast_i(0) <= '0';  
                            state_input_sm <= idle;
                        else
                            if(var_counter_phase_shift = phase_shift_threshold) then
                                signal_input_axis_tlast_i(0) <= '1';
                            end if;
                            signal_input_axis_tvalid_i(0)<= '1';
                            signal_input_axis_tdata_i(0) <= (others => '0');
                            var_counter_phase_shift := var_counter_phase_shift + 1; 
                        end if;
					when others =>
                        null;
                end case;
			end if;
        end if;
    end process;
                                              	 
	-- Write first frame
	-- Write coinc result
    signal_BRAM_porta: process(aclk) 
		variable var_counter_result : integer range 0 to 65536 :=0;
		variable var_next_frame_offset : integer range 0 to 128 :=0;
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                coincidence_bram_ena   	<= '1';
                coincidence_bram_wea   	<= (others => '0');
                coincidence_bram_addra 	<= (others => '0');
                coincidence_bram_dina  	<= (others => '0');
				coincidence_bram_addra_first <= '0';
                first_frame_end			<= '0';
				next_frame_end			<= '0';
				signal_input_axis_tready_i2 <= '0';
				sign_change <= '0';
				coinc_counter_i <= (others => '0');		
				var_next_frame_offset := 0;		
				coincidence_bram_doutb_d1 <= (others => '0');	
				coincidence_absolute_value(0)  <= (others => '0');				
				coincidence_absolute_value(1)  <= (others => '0');					
                -- State
                state_bram_porta_sm    	<= idle;
            else     
                coincidence_bram_doutb_d1 <= coincidence_bram_doutb;
                if(coincidence_bram_doutb(13) = '0') then
                    coincidence_absolute_value(0) <= coincidence_bram_doutb;
                else
                    coincidence_absolute_value(0) <= std_logic_vector(-signed(coincidence_bram_doutb)); 
                end if;
                if(signal_input_axis_tdata_i(3+next_frame_offset)(13) = '0') then
                    coincidence_absolute_value(1) <= signal_input_axis_tdata_i(3+next_frame_offset);
                else
                    coincidence_absolute_value(1) <= std_logic_vector(-signed(signal_input_axis_tdata_i(3+next_frame_offset))); 
                end if;
                case state_bram_porta_sm is        
                    when idle =>
                        coincidence_bram_wea <= (others => '0');
						var_counter_result 	 := 0;
						coincidence_bram_addra_first <= '1';
						signal_input_axis_tready_i2 <= '1';					
						if(state_general_sm = read_result or 
						   --state_general_sm = idle or
						   state_general_sm = check_shift or state_general_sm = internal_reset) then
						    coinc_counter_i <= (others => '0'); 
						end if;
                        if(first_frame_flag = '1') then
							state_bram_porta_sm <= first_frame;
						elsif(next_frame_flag = '1') then
						    var_next_frame_offset := next_frame_offset;
							state_bram_porta_sm <= next_frame;	   
					    end if;
                    when first_frame =>
						if(first_frame_flag = '1') then
							if(signal_input_axis_tvalid_i(2) = '1') then
								if(var_counter_result >= unsigned(data_window_result_i)) then
									-- end
									first_frame_end   <= '1';
									signal_input_axis_tready_i2 <= '0';
									var_counter_result := to_integer(unsigned(unsigned(data_window_result_i)));	
								else
									-- write
									coincidence_bram_wea <= (others => '1');
									-- addr
									if(coincidence_bram_addra_first = '1') then
										coincidence_bram_addra_first <= '0';
										coincidence_bram_addra <= (others => '0');	
									else
										coincidence_bram_addra <= std_logic_vector(unsigned(coincidence_bram_addra)+1);
									end if;
									-- din
									coincidence_bram_dina <= signal_input_axis_tdata_i(2)(13 downto 0); 
									-- counter
									if(signal_input_axis_tlast_i(2) = '1') then
										first_frame_end   <= '1';
										signal_input_axis_tready_i2 <= '0';
										var_counter_result := to_integer(unsigned(unsigned(data_window_result_i)));
									else
									    if(var_counter_result = 1) then
									       coinc_counter_i <= std_logic_vector(unsigned(coinc_counter_i) + 1);
									    end if;
										var_counter_result := var_counter_result + 1;
									end if;
								end if;
							end if;
						else						    
							first_frame_end   	<= '0';
							state_bram_porta_sm <= idle;
						end if;																											
					when next_frame =>
						if(next_frame_flag = '1') then
							if(signal_input_axis_tvalid_i(4+var_next_frame_offset) = '1') then
								if(var_counter_result >= unsigned(data_window_result_i)) then
									-- end
									next_frame_end <= '1';
									signal_input_axis_tready_i2 <= '0';
									var_counter_result := to_integer(unsigned(unsigned(data_window_result_i)));
								else
									-- write
									coincidence_bram_wea <= (others => '1');
									-- addr
									if(coincidence_bram_addra_first = '1') then
										coincidence_bram_addra_first <= '0';
										coincidence_bram_addra <= (others => '0');
									else
										coincidence_bram_addra <= std_logic_vector(unsigned(coincidence_bram_addra)+1);	
									end if;
									-- din	
									if(coincidence_bram_doutb_d1(13) /= signal_input_axis_tdata_i(4+var_next_frame_offset)(13) and
									   advanced_filter_on_i = '1') then
									    coincidence_bram_dina  <= (others => '0');
									    sign_change <= '1';
							        else
							            --coincidence_bram_dina  <= signal_input_axis_tdata_i(4+next_frame_offset);	
							            if(signed(coincidence_absolute_value(0)) <= signed(coincidence_absolute_value(1))) then
                                            coincidence_bram_dina  <= coincidence_bram_doutb_d1;        
                                        else
                                            coincidence_bram_dina  <= signal_input_axis_tdata_i(4+next_frame_offset);            
                                        end if;  
							            if(coincidence_bram_doutb_d1(13) /= signal_input_axis_tdata_i(4+var_next_frame_offset)(13)) then
							                sign_change <= '1';
							            else
							                sign_change <= '0';
							            end if;
							        end if;																				
									-- counter															  	
									if(signal_input_axis_tlast_i(4+var_next_frame_offset) = '1') then
                                        next_frame_end   <= '1';
                                        signal_input_axis_tready_i2 <= '0';
                                        var_counter_result := to_integer(unsigned(unsigned(data_window_result_i)));
                                    else
									    if(var_counter_result = 1) then
                                            coinc_counter_i <= std_logic_vector(unsigned(coinc_counter_i) + 1);
                                        end if;
                                        var_counter_result := var_counter_result + 1;
                                    end if;	
								end if;
							end if;
						else
						    sign_change <= '0';
							next_frame_end <= '0';																		
							state_bram_porta_sm <= idle;	
						end if;
                    when others => 
                        null;
                end case;                                 
            end if;
        end if;
    end process;
			
	-- Read result
	-- Read previous frame for filter
    signal_BRAM_portb: process(aclk) 
		variable var_counter_result : integer range 0 to 65536 :=0; 
		variable var_next_frame_offset : integer range 0 to 128 :=0;
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				-- Bram
                coincidence_bram_enb   	<= '1';
                coincidence_bram_web   	<= (others => '0');
                coincidence_bram_addrb 	<= (others => '0');
                coincidence_bram_dinb  	<= (others => '0');
				coincidence_bram_addrb_first <= '0';
                read_result_end			<= '0';
                signal_input_axis_tready_i3 <= '0';
                var_next_frame_offset := 0;
				-- Coincidence result					
                coinc_result_axis_tlast_i(0)    <= '0';
                coinc_result_axis_tvalid_i(0)	<= '0';
                data_window_read    <= (others => '0');
                -- State
                state_bram_portb_sm    	<= idle;
            else     
                case state_bram_portb_sm is        
                    when idle =>
						coincidence_bram_addrb_first <= '1';
						var_counter_result := 0;
						signal_input_axis_tready_i3 <= '1';
                        if(read_result_flag = '1') then
                            if(fir_delay_on_i = '1') then
                                data_window_read <= FIR_DELAY;
                            else
                                data_window_read <= data_window_result_i;
                            end if;
							state_bram_portb_sm <= read_result;
						elsif(next_frame_flag = '1') then
						    var_next_frame_offset := next_frame_offset;
							state_bram_portb_sm <= next_frame;	
					    end if;
                    when read_result =>
                        signal_input_axis_tready_i3 <= '0';
						if(read_result_flag = '1') then
							if(var_counter_result >= unsigned(data_window_read)) then
								-- end
								coinc_result_axis_tlast_i(0)    <= '0';
                				coinc_result_axis_tvalid_i(0)	<= '0';
								read_result_end <= '1';
							else
							    if(COINC_RESULT_AXIS_TREADY = '1') then
                                    -- addr
                                    if(coincidence_bram_addrb_first = '1') then
                                        coincidence_bram_addrb_first <= '0';
                                        coincidence_bram_addrb 	<= (others => '0');
                                    else
                                        coincidence_bram_addrb <= std_logic_vector(unsigned(coincidence_bram_addrb)+1);	
                                    end if;
                                    -- tvalid																	 
                                    coinc_result_axis_tvalid_i(0) <= '1';
                                    -- tlast
                                    if(var_counter_result = unsigned(data_window_read)-1) then
                                        coinc_result_axis_tlast_i(0) <= '1';	
                                    end if;
                                    -- counter
                                    var_counter_result := var_counter_result + 1;
                                else
                                    coinc_result_axis_tvalid_i(0) <= '0';
                                end if;      
							end if;
						else
							read_result_end   	<= '0';
							state_bram_portb_sm <= idle;
						end if;
					when next_frame =>
						if(next_frame_flag = '1') then
							if(signal_input_axis_tvalid_i(0+var_next_frame_offset) = '1') then
								if(var_counter_result >= unsigned(data_window_result_i)) then
									-- end
									signal_input_axis_tready_i3 <= '0';
									var_counter_result := to_integer(unsigned(unsigned(data_window_result_i)));																	  
								else
									-- addr
									if(coincidence_bram_addrb_first = '1') then
										coincidence_bram_addrb_first <= '0';
										coincidence_bram_addrb <= (others => '0');
									else
										coincidence_bram_addrb <= std_logic_vector(unsigned(coincidence_bram_addrb)+1);	
									end if;
									-- counter
									if(signal_input_axis_tlast_i(3+var_next_frame_offset) = '1') then
                                        signal_input_axis_tready_i3 <= '0';
                                        var_counter_result := to_integer(unsigned(unsigned(data_window_result_i)));
                                    else
                                        var_counter_result := var_counter_result + 1;
                                    end if;    																	  
								end if;
							end if;
						else
							state_bram_portb_sm <= idle;	
						end if;                     
                    when others => 
                        null;
                end case;                                 
            end if;
        end if;
    end process;
      
    -- Delay signal input     
    delay_signal_input : for i in 1 to 7 generate
        process(aclk) 
        begin
           if (rising_edge(aclk)) then
                if (aresetn = '0') then
                    signal_input_axis_tdata_i(i)    <= (others => '0');
                    signal_input_axis_tlast_i(i)    <= '0';
                    signal_input_axis_tvalid_i(i)   <= '0';
                else
                    signal_input_axis_tdata_i(i) <= signal_input_axis_tdata_i(i-1);
                    signal_input_axis_tlast_i(i) <= signal_input_axis_tlast_i(i-1);
                    signal_input_axis_tvalid_i(i) <= signal_input_axis_tvalid_i(i-1);
                end if;
           end if;
        end process;
    end generate;
			    
    -- Delay coincidence result     
    delay_coinc_result : for i in 1 to 3 generate
        process(aclk) 
        begin
           if (rising_edge(aclk)) then
                if (aresetn = '0') then
                    coinc_result_axis_tlast_i(i)    <= '0';
                    coinc_result_axis_tvalid_i(i)   <= '0';
                else
                    coinc_result_axis_tlast_i(i) <= coinc_result_axis_tlast_i(i-1);
                    coinc_result_axis_tvalid_i(i) <= coinc_result_axis_tvalid_i(i-1);
                end if;
           end if;
        end process;
    end generate;		   			  
      
    -- BRAM
    coincidence_filter_BRAM_inst : coincidence_filter_BRAM
          PORT MAP (
            clka => aclk,
            ena => coincidence_bram_ena,
            wea => coincidence_bram_wea,
            addra => coincidence_bram_addra,
            dina => coincidence_bram_dina,
            douta => coincidence_bram_douta,
            clkb => aclk,
            enb => coincidence_bram_enb,
            web => coincidence_bram_web,
            addrb => coincidence_bram_addrb,
            dinb => coincidence_bram_dinb,
            doutb => coincidence_bram_doutb
          );
            
end arch_imp;
