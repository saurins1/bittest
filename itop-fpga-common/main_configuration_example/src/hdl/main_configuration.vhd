----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 11/27/2017 13:11:37 AM
-- Design Name: 
-- Module Name: main configuration - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity main_configuration is
	generic (
        REGISTER_W32            : INTEGER := 32;
		BRAM_TREADY_SYNC        : INTEGER := 3
	);
	port (
	    -- Sync
	    aclk         : in STD_LOGIC;
        aresetn      : in STD_LOGIC;
        
        -- Control
        READ_CONFIGURATION  : in STD_LOGIC;
        CONFIGURATION_READY : out STD_LOGIC;
		CONFIGURATION_SIZE  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		-- Input
		CONFIGURATION_IN_AXIS_TREADY  : out STD_LOGIC;
        CONFIGURATION_IN_AXIS_TDATA   : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        CONFIGURATION_IN_AXIS_TLAST   : in STD_LOGIC;
        CONFIGURATION_IN_AXIS_TVALID  : in STD_LOGIC;
		
		-- Output
        CONFIGURATION_OUT_AXIS_TREADY  : in STD_LOGIC;
        CONFIGURATION_OUT_AXIS_TDATA   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        CONFIGURATION_OUT_AXIS_TLAST   : out STD_LOGIC;
        CONFIGURATION_OUT_AXIS_TVALID  : out STD_LOGIC;
        CONFIGURATION_OUT_AXIS_TKEEP   : out STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0)
	);
end main_configuration;

architecture arch_imp of main_configuration is

    -- Aux
    signal configuration_ready_i : STD_LOGIC;
    signal configuration_size_i  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	
	-- BRAM
	COMPONENT main_configuration_BRAM
      PORT (
        clka : IN STD_LOGIC;
        ena : IN STD_LOGIC;
        wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addra : IN STD_LOGIC_VECTOR(12 DOWNTO 0);
        dina : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        douta : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        clkb : IN STD_LOGIC;
        enb : IN STD_LOGIC;
        web : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addrb : IN STD_LOGIC_VECTOR(12 DOWNTO 0);
        dinb : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        doutb : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
       );
    END COMPONENT;

    -- Configuration BRAM port a
    signal configuration_bram_addra   : STD_LOGIC_VECTOR ( 12 downto 0 );
    signal configuration_bram_dina    : STD_LOGIC_VECTOR ( 31 downto 0 );
    signal configuration_bram_douta   : STD_LOGIC_VECTOR ( 31 downto 0 );
    signal configuration_bram_ena     : STD_LOGIC;
    signal configuration_bram_wea     : STD_LOGIC_VECTOR ( 0 to 0 );
        
    -- Configuration BRAM port b
    signal configuration_bram_addrb   : STD_LOGIC_VECTOR ( 12 downto 0 );
    signal configuration_bram_dinb    : STD_LOGIC_VECTOR ( 31 downto 0 );
    signal configuration_bram_doutb   : STD_LOGIC_VECTOR ( 31 downto 0 );
    signal configuration_bram_enb     : STD_LOGIC;
    signal configuration_bram_web     : STD_LOGIC_VECTOR ( 0 to 0 );
    
    signal configuration_bram_addrb_first : STD_LOGIC;
    signal read_configuration_flag : STD_LOGIC;
    
    -- Tready sync
	signal configuration_axis_tdata_1   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal configuration_axis_tdata_2   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal tready_flank_up_flag         : STD_LOGIC;
    signal configuration_counter        : UNSIGNED(REGISTER_W32-1 downto 0);
    signal configuration_out_axis_tready_i1 : STD_LOGIC;
    signal configuration_out_axis_tlast_i   : STD_LOGIC;
    signal wait_for_tlast_flag              : STD_LOGIC;
    
    -- Read configuration state machine
    type states_main_coinfiguration_sm is (idle, write_configuration, write_configuration_end, 
        read_configuration_start, upload_configuration_data_flank_down, upload_configuration_data_flank_up, 
        check_tlast, read_configuration_end); 

    signal state_main_coinfiguration_sm : states_main_coinfiguration_sm;
    
begin
  
    CONFIGURATION_SIZE <= configuration_size_i;
    CONFIGURATION_READY <= configuration_ready_i;
    CONFIGURATION_OUT_AXIS_TLAST <= configuration_out_axis_tlast_i;
  
    -- Configuration SM
    config_inst: process(aclk, aresetn) 
        variable var_counter_config : integer range 0 to 65536 :=0;
        variable var_wait_counter : integer range 0 to 1024 :=0;
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                -- Write BRAM
                configuration_bram_ena   <= '1';
                configuration_bram_wea   <= (others => '0');
                configuration_bram_addra <= (others => '0');
                configuration_bram_dina  <= (others => '0');
                configuration_ready_i    <= '0';
                configuration_size_i     <= (others => '0');
                read_configuration_flag  <= '0';
                CONFIGURATION_IN_AXIS_TREADY <= '0';
                -- Configuration output
                configuration_out_axis_tlast_i  <= '0';
                CONFIGURATION_OUT_AXIS_TVALID   <= '0';
                CONFIGURATION_OUT_AXIS_TDATA    <= (others => '0');
                CONFIGURATION_OUT_AXIS_TKEEP    <= (others => '0');
                configuration_counter           <= (others => '0');
                -- Sync BRAM delay with TREADY evolution		
                tready_flank_up_flag             <= '0';
                configuration_axis_tdata_1       <= (others => '0');     
                configuration_axis_tdata_2       <= (others => '0');                                                                     
                configuration_out_axis_tready_i1 <= '0';
                wait_for_tlast_flag              <= '0';
                state_main_coinfiguration_sm     <= idle;
            else
                configuration_out_axis_tready_i1 <= CONFIGURATION_OUT_AXIS_TREADY;
                case state_main_coinfiguration_sm is  
                    when idle =>
                        var_counter_config := 0; 
                        var_wait_counter := 0;
                        if(CONFIGURATION_IN_AXIS_TVALID = '1') then
                            configuration_bram_wea   <= (others => '1');
                            configuration_bram_addra <= (others => '0');
                            configuration_bram_dina  <= CONFIGURATION_IN_AXIS_TDATA;
                            state_main_coinfiguration_sm <= write_configuration;
                        else
                            if(READ_CONFIGURATION = '1' and configuration_ready_i = '1') then
                                CONFIGURATION_IN_AXIS_TREADY <= '0';     
                                configuration_counter        <= (others => '0'); 
                                state_main_coinfiguration_sm <= read_configuration_start;
                            else
                                CONFIGURATION_IN_AXIS_TREADY <= '1';
                            end if;
                        end if;
                    when write_configuration =>
                        if(CONFIGURATION_IN_AXIS_TVALID = '1') then
                            configuration_bram_addra <= std_logic_vector(unsigned(configuration_bram_addra)+1);
                            configuration_bram_dina  <= CONFIGURATION_IN_AXIS_TDATA;
                            if(CONFIGURATION_IN_AXIS_TLAST = '1') then
                                state_main_coinfiguration_sm <= write_configuration_end;
                            end if;
                        end if;  
                    when write_configuration_end =>
                        if(var_wait_counter >= 2) then
                            if(CONFIGURATION_IN_AXIS_TVALID = '0') then
                                configuration_ready_i        <= '1';
                                configuration_size_i         <= std_logic_vector(resize(unsigned(configuration_bram_addra)+1, CONFIGURATION_SIZE'length));
                                configuration_bram_wea       <= (others => '0');
                                state_main_coinfiguration_sm <= idle;
                            end if;
                        else
                            var_wait_counter := var_wait_counter + 1;
                        end if;
                    when read_configuration_start =>
                        CONFIGURATION_OUT_AXIS_TKEEP <= (others => '1');
                        if(CONFIGURATION_OUT_AXIS_TREADY = '1') then
                            read_configuration_flag <= '1';
                            if(var_wait_counter >= BRAM_TREADY_SYNC) then
                                state_main_coinfiguration_sm <= upload_configuration_data_flank_down;
                            else
                                var_wait_counter := var_wait_counter + 1;
                            end if;
                        end if; 
                    when upload_configuration_data_flank_down =>
                        if(CONFIGURATION_OUT_AXIS_TREADY = '1') then
                            CONFIGURATION_OUT_AXIS_TVALID <= '1';                                                    
                            if(tready_flank_up_flag = '1') then
                                CONFIGURATION_OUT_AXIS_TDATA <= configuration_axis_tdata_2;
                                tready_flank_up_flag <= '0';
                            else
                                CONFIGURATION_OUT_AXIS_TDATA <= configuration_bram_doutb;
                            end if;
                            if(var_counter_config >= to_integer(unsigned(configuration_size_i))-1) then
                                configuration_out_axis_tlast_i <= '1';
                                state_main_coinfiguration_sm <= check_tlast;                                                
                            else
                                var_counter_config := var_counter_config + 1;                                                    
                            end if;
                            configuration_counter <= configuration_counter + 1;
                        else
                            if(configuration_out_axis_tready_i1 = '1') then
                                configuration_axis_tdata_1 <= configuration_bram_doutb;
                                var_wait_counter:= 0;
                                state_main_coinfiguration_sm <= upload_configuration_data_flank_up;
                            end if;
                        end if;  
                    when upload_configuration_data_flank_up =>
                        if(var_wait_counter = 0) then
                            configuration_axis_tdata_2 <= configuration_bram_doutb;
                        end if;
                        if(CONFIGURATION_OUT_AXIS_TREADY = '1' and configuration_out_axis_tready_i1 = '0') then
                            CONFIGURATION_OUT_AXIS_TVALID <= '1';                                                    
                            if(var_wait_counter = 0) then
                                CONFIGURATION_OUT_AXIS_TDATA <= configuration_bram_doutb;
                            else
                                CONFIGURATION_OUT_AXIS_TDATA <= configuration_axis_tdata_1;
                            end if;      
                            tready_flank_up_flag <= '1';
                            if(var_counter_config >= to_integer(unsigned(configuration_size_i))-1) then
                                configuration_out_axis_tlast_i <= '1';
                                state_main_coinfiguration_sm <= check_tlast;                                                
                            else
                                var_counter_config := var_counter_config + 1;        
                                state_main_coinfiguration_sm <= upload_configuration_data_flank_down;                                                    
                            end if;   
                            configuration_counter <= configuration_counter + 1;                                                     
                        end if;    
                        if(var_wait_counter < 1) then
                            var_wait_counter := var_wait_counter + 1;
                        end if;  
                    when check_tlast =>
                        if(wait_for_tlast_flag = '0') then
                            if(CONFIGURATION_OUT_AXIS_TREADY = '0' and configuration_out_axis_tready_i1 = '1') then
                                wait_for_tlast_flag <= '1';
                            else
                                configuration_out_axis_tlast_i  <= '0';    
                                CONFIGURATION_OUT_AXIS_TVALID   <= '0';
                                CONFIGURATION_OUT_AXIS_TKEEP    <= (others => '0');
                                read_configuration_flag         <= '0';
                                tready_flank_up_flag <= '0';
                                state_main_coinfiguration_sm    <= read_configuration_end;
                            end if;  
                        else
                            if(CONFIGURATION_OUT_AXIS_TREADY = '1') then
                                configuration_out_axis_tlast_i  <= '0';    
                                CONFIGURATION_OUT_AXIS_TVALID   <= '0';
                                CONFIGURATION_OUT_AXIS_TKEEP    <= (others => '0');
                                read_configuration_flag         <= '0';
                                tready_flank_up_flag            <= '0';
                                state_main_coinfiguration_sm    <= read_configuration_end;
                            end if;
                        end if;                 
					when read_configuration_end => 
					    wait_for_tlast_flag <= '0';															                                                  
                        if(READ_CONFIGURATION = '0' and read_configuration_flag = '0') then            
                            state_main_coinfiguration_sm <= idle;                                                    
                        end if;                  
                    when others => 
                        null;
				end case;
            end if;
        end if;
    end process;
       
    -- Configuration BRAM read process
    read_bram_configuration_inst: process(aclk, aresetn) 
        variable var_wait_counter : integer range 0 to 127 :=0;
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                configuration_bram_addrb <= (others => '0');
                configuration_bram_dinb <= (others => '0');
                configuration_bram_enb <= '1';
                configuration_bram_web <= (others => '0');
                configuration_bram_addrb_first <= '1';
                var_wait_counter := 0;
            else
                if(read_configuration_flag = '1') then
                    if(CONFIGURATION_OUT_AXIS_TREADY = '1') then
                        if(configuration_bram_addrb_first = '1') then
                            configuration_bram_addrb <= (others => '0');
                            configuration_bram_addrb_first <= '0';
                        else
                            configuration_bram_addrb <= std_logic_vector(unsigned(configuration_bram_addrb)+1);
                        end if;
                    end if;
                else
                    configuration_bram_addrb <= (others => '0');
                    configuration_bram_addrb_first <= '1';
                end if;
            end if;
        end if;    
    end process;
    
    -- Configuration stored
    main_configuration_BRAM_inst : main_configuration_BRAM
      PORT MAP (
        clka => aclk,
        ena => configuration_bram_ena,
        wea => configuration_bram_wea,
        addra => configuration_bram_addra,
        dina => configuration_bram_dina,
        douta => configuration_bram_douta,
        clkb => aclk,
        enb => configuration_bram_enb,
        web => configuration_bram_web,
        addrb => configuration_bram_addrb,
        dinb => configuration_bram_dinb,
        doutb => configuration_bram_doutb
      );
     
end arch_imp;
