library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity main_configuration_tb is
  	generic (
        REGISTER_W32            : INTEGER := 32;
        BRAM_TREADY_SYNC        : INTEGER := 3
    );
end;

architecture bench of main_configuration_tb is

  component main_configuration
  	generic (
        REGISTER_W32            : INTEGER := 32;
  		BRAM_TREADY_SYNC        : INTEGER := 3
  	);
  	port (
  	    aclk         : in STD_LOGIC;
        aresetn      : in STD_LOGIC;
        READ_CONFIGURATION  : in STD_LOGIC;
        CONFIGURATION_READY : out STD_LOGIC;
  		CONFIGURATION_SIZE  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		CONFIGURATION_IN_AXIS_TREADY  : out STD_LOGIC;
        CONFIGURATION_IN_AXIS_TDATA   : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        CONFIGURATION_IN_AXIS_TLAST   : in STD_LOGIC;
        CONFIGURATION_IN_AXIS_TVALID  : in STD_LOGIC;
        CONFIGURATION_OUT_AXIS_TREADY  : in STD_LOGIC;
        CONFIGURATION_OUT_AXIS_TDATA   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        CONFIGURATION_OUT_AXIS_TLAST   : out STD_LOGIC;
        CONFIGURATION_OUT_AXIS_TVALID  : out STD_LOGIC;
        CONFIGURATION_OUT_AXIS_TKEEP   : out STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0)
  	);
  end component;

  signal aclk: STD_LOGIC:= '0';
  signal aresetn: STD_LOGIC:= '0';
  
  signal READ_CONFIGURATION: STD_LOGIC:= '0';
  
  signal CONFIGURATION_READY: STD_LOGIC;
  signal CONFIGURATION_SIZE: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  
  signal CONFIGURATION_IN_AXIS_TREADY: STD_LOGIC;
  signal CONFIGURATION_IN_AXIS_TDATA: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');
  signal CONFIGURATION_IN_AXIS_TLAST: STD_LOGIC:= '0';
  signal CONFIGURATION_IN_AXIS_TVALID: STD_LOGIC:= '0';
  signal CONFIGURATION_OUT_AXIS_TREADY: STD_LOGIC:= '0';
  signal CONFIGURATION_OUT_AXIS_TDATA: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal CONFIGURATION_OUT_AXIS_TLAST: STD_LOGIC;
  signal CONFIGURATION_OUT_AXIS_TVALID: STD_LOGIC;
  signal CONFIGURATION_OUT_AXIS_TKEEP: STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0) ;
  
  signal CONFIGURATION_OUT_AXIS_TLAST_i1: STD_LOGIC;

  constant aclk_period: time := 10 ns;
  signal start_sm: STD_LOGIC:= '0';
  
  type states_config_sm is (idle, open_file, load_config, load_config_wait, read_config, read_config_off, close_file); 
  signal state_config_sm : states_config_sm; 
  
  signal config_counter: UNSIGNED(REGISTER_W32-1 downto 0):= (others => '0');
  
  signal tready_const: UNSIGNED(REGISTER_W32-1 downto 0):= (others => '0');
  signal tready_pwm_on: STD_LOGIC;
  
begin


	stimulus: process
	begin
		aresetn <= '0';
		wait for aclk_period*10;
		aresetn <= '1';
		wait for aclk_period*10;
		start_sm <= '1';
		wait for aclk_period*10;
		start_sm <= '0';
		wait;
	end process;
	
    -- aclk process 
    aclk_process :process
    begin
        aclk <= not aclk;
        wait for aclk_period/2;
        aclk <= not aclk;
        wait for aclk_period/2;
    end process;

  -- Insert values for generic parameters !!
  uut: main_configuration generic map ( 
                                        REGISTER_W32                  => REGISTER_W32,
                                        BRAM_TREADY_SYNC              => BRAM_TREADY_SYNC
                                      )
                             port map ( aclk                          => aclk,
                                        aresetn                       => aresetn,
                                        READ_CONFIGURATION            => READ_CONFIGURATION,
                                        CONFIGURATION_READY           => CONFIGURATION_READY,
                                        CONFIGURATION_SIZE            => CONFIGURATION_SIZE,
                                        CONFIGURATION_IN_AXIS_TREADY  => CONFIGURATION_IN_AXIS_TREADY,
                                        CONFIGURATION_IN_AXIS_TDATA   => CONFIGURATION_IN_AXIS_TDATA,
                                        CONFIGURATION_IN_AXIS_TLAST   => CONFIGURATION_IN_AXIS_TLAST,
                                        CONFIGURATION_IN_AXIS_TVALID  => CONFIGURATION_IN_AXIS_TVALID,
                                        CONFIGURATION_OUT_AXIS_TREADY => CONFIGURATION_OUT_AXIS_TREADY,
                                        CONFIGURATION_OUT_AXIS_TDATA  => CONFIGURATION_OUT_AXIS_TDATA,
                                        CONFIGURATION_OUT_AXIS_TLAST  => CONFIGURATION_OUT_AXIS_TLAST,
                                        CONFIGURATION_OUT_AXIS_TVALID => CONFIGURATION_OUT_AXIS_TVALID,
                                        CONFIGURATION_OUT_AXIS_TKEEP  => CONFIGURATION_OUT_AXIS_TKEEP );

	
	load_config_inst: process(aclk, aresetn)  
        variable my_counter  : integer range 0 to 16383 :=0;   
        variable n_reads  : integer range 0 to 16383 :=0; 
        variable tready_counter  : integer range 0 to 16383 :=0;

        type configDataFile is file of integer;
        file config_data_file: configDataFile;
        variable config_data: integer;     
        variable config_data_file_status: file_open_status;
    begin 
        if (aresetn = '0') then
            READ_CONFIGURATION <= '0';
            CONFIGURATION_IN_AXIS_TDATA <= (others => '0');
            CONFIGURATION_IN_AXIS_TLAST <= '0';
            CONFIGURATION_OUT_AXIS_TLAST_i1 <= '0';
            CONFIGURATION_IN_AXIS_TVALID <= '0';
            CONFIGURATION_OUT_AXIS_TREADY <= '0'; 
            tready_const <= to_unsigned(8, tready_const'length);  
            --tready_pwm_on <= '0';	
            tready_pwm_on <= '1'; 
            config_counter  <= (others => '0');                                                                        
            state_config_sm <= idle;  
        elsif (aclk'event and aclk = '1') then
            CONFIGURATION_OUT_AXIS_TLAST_i1 <= CONFIGURATION_OUT_AXIS_TLAST;
            case state_config_sm is  
                when idle =>   
                    n_reads := 0;   
                    if(start_sm = '1') then
                        my_counter := 0;
                        state_config_sm <= open_file;        
                    end if;
                when open_file => 
                    file_open(config_data_file_status, config_data_file, "configuration.dat", read_mode);
                    config_counter <= (others => '0');
                    my_counter := 0;                                                                             
                    state_config_sm <= load_config;  
                when load_config =>
                    if(CONFIGURATION_IN_AXIS_TREADY = '1') then
                        -- tdata
                        if not endfile(config_data_file) then
                            read (config_data_file, config_data);
                            CONFIGURATION_IN_AXIS_TDATA<=std_logic_vector(to_signed(config_data, CONFIGURATION_IN_AXIS_TDATA'length));
                        end if;
                        if not endfile(config_data_file) then
                            read (config_data_file, config_data);
                        end if;
                        -- tvalid                                                                                                           
                        CONFIGURATION_IN_AXIS_TVALID <= '1';   
                        -- tlast                                              
                        if(my_counter >= 499) then
                            CONFIGURATION_IN_AXIS_TLAST <= '1';
                            my_counter := 0;                                           
                            state_config_sm <= load_config_wait;                                              
                        else
                            -- counter                                          
                            my_counter := my_counter + 1;                                          
                        end if;
                        config_counter <= config_counter + 1;     
                    end if;
                when load_config_wait =>	
                    CONFIGURATION_IN_AXIS_TVALID <= '0'; 
                    CONFIGURATION_IN_AXIS_TLAST <= '0';
                    CONFIGURATION_IN_AXIS_TDATA <= (others => '0');
                    tready_counter := 0;
                    if(my_counter >= 32) then
                        READ_CONFIGURATION <= '1';
                        state_config_sm <= read_config;     
                    else
                        my_counter := my_counter + 1;
                    end if;
                when read_config =>
                    if(tready_pwm_on = '1') then
                        if(tready_counter >= tready_const) then
                            CONFIGURATION_OUT_AXIS_TREADY <= not CONFIGURATION_OUT_AXIS_TREADY;
                            tready_counter := 0;
                        else
                            tready_counter := tready_counter + 1;
                        end if; 
                    else
                        CONFIGURATION_OUT_AXIS_TREADY <= '1';
                    end if;
                    if(CONFIGURATION_OUT_AXIS_TLAST = '1' or (CONFIGURATION_OUT_AXIS_TLAST = '0' and CONFIGURATION_OUT_AXIS_TLAST_i1 = '1')) then
                        READ_CONFIGURATION <= '0';
                        state_config_sm <= read_config_off;
                    end if;
                when read_config_off =>
                    if(CONFIGURATION_OUT_AXIS_TLAST = '0') then 
                        n_reads := n_reads + 1;          
                        my_counter := 0;
                        if(n_reads >= 100) then
                            state_config_sm <= close_file;
                        else
                            state_config_sm <= load_config_wait;
                        end if;
                    else
                        state_config_sm <= read_config;
                    end if;
                when close_file =>
                    if(my_counter >= 32) then
                        file_close(config_data_file);
                        state_config_sm <= idle;
                    else
                        my_counter := my_counter + 1;
                    end if;   
                when others =>
                    null;                   
            end case;                                                                                            
        end if;
    end process; 

end;