----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 09/27/2017 10:07:23 AM
-- Design Name: 
-- Module Name: PF calculator - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity prf_calculator is
	generic (
        REGISTER_W32            : INTEGER := 32;
        REGISTER_W16            : INTEGER := 16;
        AVG_LEVEL_MIN           : INTEGER := 2;
        AVG_LEVEL_MAX           : INTEGER := 32
	);
	port (
	    -- Sync
	    aclk         : in STD_LOGIC;
        aresetn      : in STD_LOGIC;
        
        -- Control
        START_INSPECTION  		: in STD_LOGIC;
		ACQUISITION_PROVIDED  	: in STD_LOGIC;
		TRANSMITTER_PRF   	  	: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		AVG_LEVEL   	  	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		-- Output
        PRF_RESULT   	  		: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0)
	);
end prf_calculator;

architecture arch_imp of prf_calculator is

	-- Divider
    COMPONENT prf_calculator_divider_32_16
      PORT (
        aclk : IN STD_LOGIC;
        aclken : IN STD_LOGIC;
        aresetn : IN STD_LOGIC;
        s_axis_divisor_tvalid : IN STD_LOGIC;
        s_axis_divisor_tlast : IN STD_LOGIC;
        s_axis_divisor_tdata : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        s_axis_dividend_tvalid : IN STD_LOGIC;
        s_axis_dividend_tlast : IN STD_LOGIC;
        s_axis_dividend_tdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        m_axis_dout_tvalid : OUT STD_LOGIC;
        m_axis_dout_tlast : OUT STD_LOGIC;
        m_axis_dout_tdata : OUT STD_LOGIC_VECTOR(47 DOWNTO 0)
      );
    END COMPONENT;
    
    signal s_axis_divisor_tvalid : STD_LOGIC;
    signal s_axis_divisor_tlast : STD_LOGIC;
    signal s_axis_divisor_tdata : STD_LOGIC_VECTOR(15 DOWNTO 0);
    signal s_axis_dividend_tvalid : STD_LOGIC;
    signal s_axis_dividend_tlast : STD_LOGIC;
    signal s_axis_dividend_tdata : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal m_axis_dout_tvalid : STD_LOGIC;
    signal m_axis_dout_tlast : STD_LOGIC;
    signal m_axis_dout_tdata : STD_LOGIC_VECTOR(47 DOWNTO 0);
    
    signal s_axis_divider_valid : STD_LOGIC;
	
	signal prf_adquisiton_counter : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal prf_adquisiton_counter_i : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal prf_adquisiton_counter_part : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal prf_adquisiton         : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal adquisiton_provided_d1 : STD_LOGIC;
    signal prf_result_first       : STD_LOGIC;
    
    signal avg_level_i            : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	 
begin

	-- PRF CALCULATOR
    prf_calculator_process: process(aclk) 
        variable var_prf_counter : integer range 0 to 999999999 :=0;                                                                         
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                var_prf_counter := 0;
                prf_adquisiton_counter <= (others => '0');
                prf_adquisiton_counter_i <= (others => '0');
                prf_adquisiton_counter_part <= (others => '0');
                prf_adquisiton         <= (others => '0');              
                adquisiton_provided_d1 <= '0';
                s_axis_divider_valid   <= '0';
            else
                adquisiton_provided_d1 <= ACQUISITION_PROVIDED;
                prf_adquisiton_counter_part <= std_logic_vector(unsigned(prf_adquisiton_counter_i) srl 3);
                --prf_adquisiton_counter_part <= (others => '0');
                if(START_INSPECTION = '1') then
                    if(var_prf_counter >= 99999999) then
                        var_prf_counter := 0;                                           
                        if(adquisiton_provided_d1 = '0' and ACQUISITION_PROVIDED = '1') then
                            prf_adquisiton <= std_logic_vector(unsigned(prf_adquisiton_counter_i) + 1 + unsigned(prf_adquisiton_counter_part)); 
                        else
                            prf_adquisiton <= std_logic_vector(unsigned(prf_adquisiton_counter_i) + unsigned(prf_adquisiton_counter_part));
                        end if; 
                        prf_adquisiton_counter <= (others => '0'); 
                        s_axis_divider_valid   <= '1';
                    else
                        var_prf_counter := var_prf_counter + 1; 
                        if(adquisiton_provided_d1 = '0' and ACQUISITION_PROVIDED = '1') then
                            prf_adquisiton_counter <= std_logic_vector(unsigned(prf_adquisiton_counter) + 1); 
                            prf_adquisiton_counter_i <= std_logic_vector(unsigned(prf_adquisiton_counter) + 1); 
                        end if;   
                        s_axis_divider_valid   <= '0';                     
                    end if;    
                else
                    var_prf_counter := 0;
                    prf_adquisiton_counter <= (others => '0');   
                    prf_adquisiton         <= (others => '0');                       
                end if;          
            end if;
        end if;
    end process;
       
	-- PRF DIVIDER
    prf_divider_process: process(aclk)                                                                       
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                s_axis_divisor_tdata  <= (others => '0');
                s_axis_dividend_tdata <= (others => '0');
                s_axis_divisor_tvalid <= '0';
                s_axis_divisor_tlast  <= '0';
                s_axis_dividend_tvalid<= '0';
                s_axis_dividend_tlast <= '0';
                PRF_RESULT            <= (others => '0');
                avg_level_i           <= (others => '0');
                prf_result_first      <= '1';
            else
                if(unsigned(prf_adquisiton) > 0) then            
                    s_axis_divisor_tdata  <= prf_adquisiton; 
                else
                    s_axis_divisor_tdata <= std_logic_vector(to_unsigned(1, s_axis_divisor_tdata'length));    
                end if;
                --s_axis_dividend_tdata <= TRANSMITTER_PRF;
                s_axis_dividend_tdata <= std_logic_vector(to_unsigned(100000000, s_axis_dividend_tdata'length));
                s_axis_divisor_tvalid <= s_axis_divider_valid;
                s_axis_divisor_tlast  <= s_axis_divider_valid;
                s_axis_dividend_tvalid<= s_axis_divider_valid;
                s_axis_dividend_tlast <= s_axis_divider_valid;
                if(unsigned(AVG_LEVEL) > AVG_LEVEL_MAX) then
                    avg_level_i <= std_logic_vector(to_unsigned(AVG_LEVEL_MAX, avg_level_i'length));
                else
                    avg_level_i <= AVG_LEVEL;
                end if;
                if(START_INSPECTION = '1') then
                    if(m_axis_dout_tlast = '1') then
                        if(unsigned(m_axis_dout_tdata(47 downto 16)) < unsigned(TRANSMITTER_PRF)) then
                            PRF_RESULT <= TRANSMITTER_PRF; 
                        else
                            PRF_RESULT <= m_axis_dout_tdata(47 downto 16);
                        end if;  
                    else
                        if(prf_result_first = '1') then
                            prf_result_first <= '0';
                            PRF_RESULT <= TRANSMITTER_PRF; 
                        end if;
                    end if;
                else
                    prf_result_first      <= '1';
                    PRF_RESULT <= TRANSMITTER_PRF;  
                end if;
            end if;
        end if;
    end process;
        
    -- Divider
    prf_calculator_divider_32_16_inst : prf_calculator_divider_32_16
      PORT MAP (
        aclk => aclk,
        aclken => '1',
        aresetn => aresetn,
        s_axis_divisor_tvalid => s_axis_divisor_tvalid,
        s_axis_divisor_tlast => s_axis_divisor_tlast,
        s_axis_divisor_tdata => s_axis_divisor_tdata,
        s_axis_dividend_tvalid => s_axis_dividend_tvalid,
        s_axis_dividend_tlast => s_axis_dividend_tlast,
        s_axis_dividend_tdata => s_axis_dividend_tdata,
        m_axis_dout_tvalid => m_axis_dout_tvalid,
        m_axis_dout_tlast => m_axis_dout_tlast,
        m_axis_dout_tdata => m_axis_dout_tdata
      );
	
end arch_imp;
