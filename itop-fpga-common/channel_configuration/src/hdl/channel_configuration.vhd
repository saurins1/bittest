----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 09/27/2017 10:07:23 AM
-- Design Name: 
-- Module Name: channel configuration - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity channel_configuration is
	generic (
		CHANNEL_ID		: INTEGER := 0;
		RECEIVER_SIZE	: INTEGER := 16;
		TRANSMITTER_SIZE: INTEGER := 16;
		MAGNET_SIZE		: INTEGER := 16;
		GATES_SIZE		: INTEGER := 72;
		DSP_SIZE		: INTEGER := 24;
        BRAM_ADDR_WIDTH : INTEGER := 12;
        BRAM_DATA_WIDTH : INTEGER := 32;
        REGISTER_W32    : INTEGER := 32;
        REGISTER_W16    : INTEGER := 16;
		REGISTER_W8     : INTEGER := 8;
		REGISTER_W4     : INTEGER := 4;
		REGISTER_W2     : INTEGER := 2
	);
	port (
	    -- Sync
	    aclk         : in STD_LOGIC;
        aresetn      : in STD_LOGIC;
		
		-- Control
		CONFIG_UPDATE_RECEIVER  	: in STD_LOGIC;
		CONFIG_NEW_RECEIVER   		: out STD_LOGIC;
		
		CONFIG_UPDATE_TRANSMITTER   : in STD_LOGIC;
		CONFIG_NEW_TRANSMITTER  	: out STD_LOGIC;
		
		CONFIG_UPDATE_MAGNET 	 	: in STD_LOGIC;
		CONFIG_NEW_MAGNET  			: out STD_LOGIC;
		
		CONFIG_UPDATE_DSP_1  	: in STD_LOGIC;
		CONFIG_NEW_DSP_1  		: out STD_LOGIC;
		
		CONFIG_UPDATE_DSP_2  	: in STD_LOGIC;
		CONFIG_NEW_DSP_2  		: out STD_LOGIC;
		
		CONFIG_UPDATE_FIR  	 	: in STD_LOGIC;
		CONFIG_NEW_FIR  		: out STD_LOGIC;
		
		CONFIG_UPDATE_GATES  	: in STD_LOGIC;
		CONFIG_NEW_GATES  		: out STD_LOGIC;
		
		CONFIG_UPDATE_DAC  	 	: in STD_LOGIC;
		CONFIG_NEW_DAC  		: out STD_LOGIC;
		
		THERE_IS_CONFIGURATION   : out STD_LOGIC;
		
		-- Channel enable
		CHANNEL_EN   : out STD_LOGIC;
		
		-- Channel visible
        CHANNEL_VIS  : out STD_LOGIC;
		
		-- Channel HW
		CHANNEL_HW	 : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	
		-- Config input
		CONFIG_INPUT_LOAD	      : in STD_LOGIC;
		CONFIG_INPUT_AXIS_TREADY  : out STD_LOGIC;
		CONFIG_INPUT_AXIS_TDATA	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		CONFIG_INPUT_AXIS_TLAST	  : in STD_LOGIC;
		CONFIG_INPUT_AXIS_TVALID  : in STD_LOGIC;
		
		-- Config output receiver
		RECEIVER_SAMPLING_FREQUENCY         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        RECEIVER_DATA_WINDOW                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        RECEIVER_DELAY                      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  -- samples
        RECEIVER_ANALOG_GAIN                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  --db*1024
        RECEIVER_DIGITAL_GAIN               : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  --V*1024*1024
        RECEIVER_EXTERNAL_MULTIPLEXER_EN    : out STD_LOGIC;
		
		-- Config output transmitter
		TRANSMITTER_VOLTAGE                 : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        TRANSMITTER_BURST_FREQUENCY         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        TRANSMITTER_N_CYCLES                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_N_BURST                 : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        TRANSMITTER_PRF                     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_DELAY                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_DELTA_DELAY             : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_DELTA_ADD               : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_DIRECTIONAL_PHASING     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_PHASE_SHIFT             : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_START_CYCLE             : out STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
		
		-- Config output magnet
		MAGNET_MODE                         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        MAGNET_PULSE_WIDTH                  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        MAGNET_INITIAL_DELAY                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        MAGNET_RAMP_UP_VOLTAGE              : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        MAGNET_VOLTAGE                      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		-- Config output DSP
		DSP_AVERAGE                         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
		DSP_AVERAGE_TYPE                    : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        DSP_NOISE_REDUCTION_FILTER          : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        DSP_PHASE_SHIFT                     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        DSP_BANDPASS_EN  					: out STD_LOGIC;
		DSP_BANDPASS_N_TAPS  				: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
		DSP_BANDPASS_L  					: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        DSP_GAUSSIAN_EN  					: out STD_LOGIC;
		DSP_GAUSSIAN_N_TAPS  				: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
		DSP_GAUSSIAN_L  					: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
		DSP_ENVELOPE_EN  					: out STD_LOGIC;
		DSP_ENVELOPE_N_TAPS  				: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
		DSP_ENVELOPE_L  					: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
		DSP_ANALOG_FILTER  					: out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
	    DSP_DEC_TYPE	                    : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);    
        DSP_DEC_LEVEL                       : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        DSP_DEC_WINDOW                      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        DSP_GAUSSIAN_DELAY  				: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        DSP_INVERSION  					    : out STD_LOGIC;
        DSP_RECTIFICATION  					: out STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
        DSP_FIR_DELAY  				        : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        DSP_COMPRESSION_MODE                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        DSP_COMPRESSION_LEVEL               : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        DSP_AVG_COINC_ORDER                 : out STD_LOGIC; 
				
		-- Config output Gates
		GATE1_EN                          : out STD_LOGIC;  
		GATE1_START                       : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		GATE1_WIDTH                       : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);	
        GATE1_AMP_THRESHOLD               : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		GATE1_TOF_ALGORITHM               : out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
		GATE1_TOF_AVG                     : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
		GATE1_TOF_MIN_THICKNESS           : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE1_AMP_THRESHOLD_ALARM_EN      : out STD_LOGIC; 
        GATE1_AMP_THRESHOLD_ALARM_CROSSING: out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE1_TOF_ALARM_EN                : out STD_LOGIC; 
        GATE1_TOF_ALARM_TYPE              : out STD_LOGIC;
        GATE1_TOF_ALARM_CROSSING          : out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE1_TOF_ALARM_MIN_VALUE         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE1_TOF_ALARM_MAX_VALUE         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		GATE2_EN                          : out STD_LOGIC;  
		GATE2_START                       : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		GATE2_WIDTH                       : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);	
        GATE2_AMP_THRESHOLD               : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		GATE2_TOF_ALGORITHM               : out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
		GATE2_TOF_AVG                     : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
		GATE2_TOF_MIN_THICKNESS           : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE2_AMP_THRESHOLD_ALARM_EN      : out STD_LOGIC; 
        GATE2_AMP_THRESHOLD_ALARM_CROSSING: out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE2_TOF_ALARM_EN                : out STD_LOGIC; 
        GATE2_TOF_ALARM_TYPE              : out STD_LOGIC;
        GATE2_TOF_ALARM_CROSSING          : out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE2_TOF_ALARM_MIN_VALUE         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE2_TOF_ALARM_MAX_VALUE         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE2_FFT_EN                      : out STD_LOGIC;
        GATE2_CC_EN                       : out STD_LOGIC;
        GATE2_FFT_SEGMENT                 : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE2_FFT_NFFT                    : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
		
		GATE3_EN                          : out STD_LOGIC;  
		GATE3_START                       : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		GATE3_WIDTH                       : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);	
        GATE3_AMP_THRESHOLD               : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		GATE3_TOF_ALGORITHM               : out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
		GATE3_TOF_AVG                     : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
		GATE3_TOF_MIN_THICKNESS           : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE3_AMP_THRESHOLD_ALARM_EN      : out STD_LOGIC; 
        GATE3_AMP_THRESHOLD_ALARM_CROSSING: out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE3_TOF_ALARM_EN                : out STD_LOGIC; 
        GATE3_TOF_ALARM_TYPE              : out STD_LOGIC;
        GATE3_TOF_ALARM_CROSSING          : out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE3_TOF_ALARM_MIN_VALUE         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE3_TOF_ALARM_MAX_VALUE         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		-- Config output FIR
		CONFIG_FIR_AXIS_TREADY    : in STD_LOGIC;
		CONFIG_FIR_AXIS_TDATA	  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		CONFIG_FIR_AXIS_TLAST	  : out STD_LOGIC;
		CONFIG_FIR_AXIS_TVALID    : out STD_LOGIC;
		
		-- DAC output
		CONFIG_DAC_AXIS_TREADY    : in STD_LOGIC;
		CONFIG_DAC_AXIS_TDATA	  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		CONFIG_DAC_AXIS_TLAST	  : out STD_LOGIC;
		CONFIG_DAC_AXIS_TVALID    : out STD_LOGIC
	);
end channel_configuration;

architecture arch_imp of channel_configuration is
	
	-- channel enable
	signal channel_en_i : std_logic;
	
	-- channel visible
    signal channel_vis_i : std_logic;
	
	-- channel hw
	signal channel_hw_i : std_logic_vector(REGISTER_W32-1 downto 0);

    -- BRAM Configuration
	COMPONENT channel_configuration_BRAM
  	PORT (
		clka : IN STD_LOGIC;
		ena : IN STD_LOGIC;
		wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
		addra : IN STD_LOGIC_VECTOR(BRAM_ADDR_WIDTH-1 DOWNTO 0);
		dina : IN STD_LOGIC_VECTOR(BRAM_DATA_WIDTH-1 DOWNTO 0);
		douta : OUT STD_LOGIC_VECTOR(BRAM_DATA_WIDTH-1 DOWNTO 0);
		clkb : IN STD_LOGIC;
		enb : IN STD_LOGIC;
		web : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
		addrb : IN STD_LOGIC_VECTOR(BRAM_ADDR_WIDTH-1 DOWNTO 0);
		dinb : IN STD_LOGIC_VECTOR(BRAM_DATA_WIDTH-1 DOWNTO 0);
		doutb : OUT STD_LOGIC_VECTOR(BRAM_DATA_WIDTH-1 DOWNTO 0)
  	);
  	END COMPONENT;
    
    -- Configuration BRAM port a
    signal configuration_bram_addra   : STD_LOGIC_VECTOR ( BRAM_ADDR_WIDTH-1 downto 0 );
    signal configuration_bram_dina    : STD_LOGIC_VECTOR ( BRAM_DATA_WIDTH-1 downto 0 );
    signal configuration_bram_douta   : STD_LOGIC_VECTOR ( BRAM_DATA_WIDTH-1 downto 0 );
    signal configuration_bram_ena     : STD_LOGIC;
    signal configuration_bram_wea     : STD_LOGIC_VECTOR ( 0 to 0 );
    
    -- Configuration BRAM port b
    signal configuration_bram_addrb   : STD_LOGIC_VECTOR ( BRAM_ADDR_WIDTH-1 downto 0 );
    signal configuration_bram_dinb    : STD_LOGIC_VECTOR ( BRAM_DATA_WIDTH-1 downto 0 );
    signal configuration_bram_doutb   : STD_LOGIC_VECTOR ( BRAM_DATA_WIDTH-1 downto 0 );
    signal configuration_bram_enb     : STD_LOGIC;
    signal configuration_bram_web     : STD_LOGIC_VECTOR ( 0 to 0 );

	signal configuration_bram_addrb_first : STD_LOGIC;
   
    -- Configuration state machine
    type states_general_sm is (idle, recover_config, write_config, write_config_end, update_receiver, update_transmitter, 
							   	update_magnet, update_dsp_1, update_dsp_2, update_FIR, update_gates, update_DAC); 
    signal state_general_sm : states_general_sm;

	-- Offset of configuration components
	signal configuration_parameters_offset   	: UNSIGNED(REGISTER_W32-1 downto 0);
	signal configuration_parameters_size   		: UNSIGNED(REGISTER_W32-1 downto 0);
	signal configuration_dsp_offset   			: UNSIGNED(REGISTER_W32-1 downto 0);
	signal configuration_fir_band_pass_offset	: UNSIGNED(REGISTER_W32-1 downto 0);
	signal configuration_fir_gaussian_offset   	: UNSIGNED(REGISTER_W32-1 downto 0);
	signal configuration_fir_envelope_offset   	: UNSIGNED(REGISTER_W32-1 downto 0);
	signal configuration_fir_size				: UNSIGNED(REGISTER_W32-1 downto 0);
	signal configuration_dac_curves_offset   	: UNSIGNED(REGISTER_W32-1 downto 0);
	signal configuration_dac_curves_size  		: UNSIGNED(REGISTER_W32-1 downto 0);

	signal update_receiver_flag 	: STD_LOGIC;
	signal update_transmitter_flag 	: STD_LOGIC;
	signal update_magnet_flag 		: STD_LOGIC;
	signal update_dsp_1_flag 		: STD_LOGIC;
	signal update_dsp_2_flag 		: STD_LOGIC;
	signal update_fir_flag 			: STD_LOGIC;
	signal update_gates_flag 		: STD_LOGIC;
	signal update_dac_flag 			: STD_LOGIC;

	signal update_receiver_flag_i 	: STD_LOGIC_VECTOR ( 2 downto 0 );
	signal update_transmitter_flag_i: STD_LOGIC_VECTOR ( 2 downto 0 );
	signal update_magnet_flag_i 	: STD_LOGIC_VECTOR ( 2 downto 0 );
	signal update_dsp_1_flag_i 		: STD_LOGIC_VECTOR ( 2 downto 0 );
	signal update_dsp_2_flag_i 		: STD_LOGIC_VECTOR ( 2 downto 0 );
	signal update_fir_flag_i 		: STD_LOGIC_VECTOR ( 2 downto 0 );
	signal update_gates_flag_i 		: STD_LOGIC_VECTOR ( 2 downto 0 );
	signal update_dac_flag_i 		: STD_LOGIC_VECTOR ( 2 downto 0 );

	signal update_receiver_end 		: STD_LOGIC;
	signal update_transmitter_end 	: STD_LOGIC;
	signal update_magnet_end 		: STD_LOGIC;
	signal update_dsp_1_end			: STD_LOGIC;
	signal update_dsp_2_end 		: STD_LOGIC;
	signal update_FIR_end			: STD_LOGIC;
	signal update_gates_end 		: STD_LOGIC;
	signal update_DAC_end			: STD_LOGIC;

	signal receiver_offset 		: UNSIGNED(REGISTER_W32-1 downto 0);
	signal transmitter_offset 	: UNSIGNED(REGISTER_W32-1 downto 0);
	signal magnet_offset 		: UNSIGNED(REGISTER_W32-1 downto 0);
	signal dsp_1_offset 		: UNSIGNED(REGISTER_W32-1 downto 0);
	signal dsp_2_offset 		: UNSIGNED(REGISTER_W32-1 downto 0);
	signal fir_offset 			: UNSIGNED(REGISTER_W32-1 downto 0);
	signal gates_offset 		: UNSIGNED(REGISTER_W32-1 downto 0);
	signal dac_offset 			: UNSIGNED(REGISTER_W32-1 downto 0);

	signal dsp_bandpass_n_taps_i 	: UNSIGNED(REGISTER_W32-1 downto 0); 
	signal dsp_gaussian_n_taps_i  	: UNSIGNED(REGISTER_W32-1 downto 0); 
	signal dsp_envelope_n_taps_i  	: UNSIGNED(REGISTER_W32-1 downto 0); 

	signal dsp_bandpass_n_taps_offset	: UNSIGNED(REGISTER_W32-1 downto 0); 
	signal dsp_gaussian_n_taps_offset  	: UNSIGNED(REGISTER_W32-1 downto 0); 
	signal dsp_envelope_n_taps_offset  	: UNSIGNED(REGISTER_W32-1 downto 0);
	
	
	signal channel_configuration_size  		: UNSIGNED(REGISTER_W32-1 downto 0);
	signal channel_configuration_size_i  	: UNSIGNED(REGISTER_W32-1 downto 0);
	
	signal there_is_configuration_i			: STD_LOGIC;
	signal there_was_configuration			: STD_LOGIC:= '0';

begin
	
	receiver_offset <= configuration_parameters_offset + 1;
	transmitter_offset <= receiver_offset + RECEIVER_SIZE;
	magnet_offset <= transmitter_offset + TRANSMITTER_SIZE;
	gates_offset <= magnet_offset + MAGNET_SIZE;
	dsp_1_offset <= configuration_dsp_offset;
	--dsp_2_offset <= dsp_1_offset + 2;
	--dsp_2_offset <= dsp_1_offset + 3;
	dsp_2_offset <= dsp_1_offset + 4;
	dac_offset	<= configuration_dac_curves_offset;
	fir_offset <= configuration_fir_band_pass_offset;

	dsp_bandpass_n_taps_offset <= dsp_2_offset + 1 + 1;
	--dsp_bandpass_n_taps_offset <= dsp_2_offset + 1 ;
	dsp_gaussian_n_taps_offset <= dsp_bandpass_n_taps_offset + 3;
	dsp_envelope_n_taps_offset <= dsp_gaussian_n_taps_offset + 3;


	configuration_fir_size <= dsp_bandpass_n_taps_i + dsp_gaussian_n_taps_i + dsp_envelope_n_taps_i;

	CHANNEL_EN <= channel_en_i;                              
	CHANNEL_VIS <= channel_vis_i; 
	CHANNEL_HW <= channel_hw_i;
	
	previous_config: process(aclk) 
    begin 
        if (rising_edge(aclk)) then
            if(there_is_configuration_i = '1') then
                there_was_configuration <= '1';
            end if;
        end if;
    end process;
    
    THERE_IS_CONFIGURATION <= there_is_configuration_i;
					      					
    -- Configuration SM
    config_inst: process(aclk) 
		variable var_counter_config : integer range 0 to 1000000 :=0;
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                -- channel_en_i
                channel_en_i <= '0';
                channel_vis_i<= '0';
                channel_hw_i <= (others => '0');
                dsp_bandpass_n_taps_i <= (others => '0');
                dsp_gaussian_n_taps_i <= (others => '0');
                dsp_envelope_n_taps_i <= (others => '0');
                channel_configuration_size <= (others => '0');
				-- Configuration bram
				configuration_bram_ena   	<= '1';
                configuration_bram_wea   	<= (others => '0');
                configuration_bram_addra 	<= (others => '0');
                configuration_bram_dina  	<= (others => '0');	
				configuration_bram_enb   	<= '1';
                configuration_bram_web   	<= (others => '0'); -- always read mode
                configuration_bram_addrb 	<= (others => '0');
                configuration_bram_dinb 	<= (others => '0');
				configuration_bram_addrb_first <= '0';
				-- Configuration offsets
				configuration_parameters_offset 	<= (others => '0');
				configuration_parameters_size 		<= (others => '0');
				configuration_dsp_offset 			<= (others => '0');
				configuration_fir_band_pass_offset 	<= (others => '0');
				configuration_fir_gaussian_offset 	<= (others => '0');
				configuration_fir_envelope_offset 	<= (others => '0');
				configuration_dac_curves_offset 	<= (others => '0');
				configuration_dac_curves_size		<= (others => '0');
				-- Parms
				CONFIG_NEW_RECEIVER		<= '0';
				CONFIG_NEW_TRANSMITTER 	<= '0';
				CONFIG_NEW_MAGNET 		<= '0';
				CONFIG_NEW_DSP_1 		<= '0';
				CONFIG_NEW_DSP_2 		<= '0';
				CONFIG_NEW_FIR 			<= '0';
				CONFIG_NEW_GATES 		<= '0';
				CONFIG_NEW_DAC 			<= '0';
				-- Configuration flags
				update_receiver_flag 	<= '0';
				update_transmitter_flag <= '0';
				update_magnet_flag 		<= '0';
				update_dsp_1_flag 		<= '0';
				update_dsp_2_flag 		<= '0';
				update_fir_flag 		<= '0';
				update_gates_flag 		<= '0';
				update_dac_flag 		<= '0';
				-- There is configuration flag
				there_is_configuration_i <= '0';
				-- Configuration tready
				CONFIG_INPUT_AXIS_TREADY <= '0';
				state_general_sm <= idle;
        	else
				case state_general_sm is  
					when idle =>
						configuration_bram_addrb_first <= '1';
						configuration_bram_addrb <= (others => '0');
						if(there_is_configuration_i = '0' and there_was_configuration = '1') then
						    CONFIG_INPUT_AXIS_TREADY <= '0';
						    state_general_sm <= recover_config;
						elsif(CONFIG_INPUT_LOAD = '1' and CONFIG_INPUT_AXIS_TVALID = '1') then
							CONFIG_INPUT_AXIS_TREADY <= '1';
							channel_configuration_size_i <= unsigned(CONFIG_INPUT_AXIS_TDATA);
						    var_counter_config := 1;
							state_general_sm <= write_config;
						elsif(CONFIG_UPDATE_RECEIVER = '1') then
							CONFIG_INPUT_AXIS_TREADY <= '0';
							state_general_sm <= update_receiver;
						elsif(CONFIG_UPDATE_TRANSMITTER = '1') then
							CONFIG_INPUT_AXIS_TREADY <= '0';
							state_general_sm <= update_transmitter;
						elsif(CONFIG_UPDATE_MAGNET = '1') then
							CONFIG_INPUT_AXIS_TREADY <= '0';
							state_general_sm <= update_magnet;
						elsif(CONFIG_UPDATE_DSP_1 = '1') then
							CONFIG_INPUT_AXIS_TREADY <= '0';
							state_general_sm <= update_dsp_1;
						elsif(CONFIG_UPDATE_DSP_2 = '1') then
							CONFIG_INPUT_AXIS_TREADY <= '0';
							state_general_sm <= update_dsp_2;
						elsif(CONFIG_UPDATE_FIR = '1') then
							CONFIG_INPUT_AXIS_TREADY <= '0';
							var_counter_config := 0;
							state_general_sm <= update_FIR;
						elsif(CONFIG_UPDATE_GATES = '1') then
							CONFIG_INPUT_AXIS_TREADY <= '0';
							state_general_sm <= update_gates;
						elsif(CONFIG_UPDATE_DAC = '1') then
							CONFIG_INPUT_AXIS_TREADY <= '0';
							var_counter_config := 0;
							state_general_sm <= update_DAC;
						else
							configuration_bram_wea 	 <= (others => '0');
							CONFIG_INPUT_AXIS_TREADY <= '1';	
						end if;
					when write_config =>
						if(CONFIG_INPUT_LOAD = '1') then	
							if(CONFIG_INPUT_AXIS_TVALID = '1') then
								configuration_bram_addra <= std_logic_vector(unsigned(configuration_bram_addra)+1);
								configuration_bram_dina  <= CONFIG_INPUT_AXIS_TDATA;
								if(var_counter_config = 1) then
                                    if(unsigned(CONFIG_INPUT_AXIS_TDATA) /= CHANNEL_ID) then
                                        state_general_sm <= write_config_end;
                                    else
                                        channel_configuration_size  <= channel_configuration_size_i;
                                        configuration_bram_addra <= (others => '0');
                                    end if;
								elsif(var_counter_config = 2) then
								    configuration_bram_wea <= (others => '1');
									channel_en_i <= CONFIG_INPUT_AXIS_TDATA(0);	
							    --Reserved
								elsif(var_counter_config = 3) then
								    channel_hw_i <= CONFIG_INPUT_AXIS_TDATA;
								elsif(var_counter_config = 4) then
								    channel_vis_i<= CONFIG_INPUT_AXIS_TDATA(0);
--								elsif(var_counter_config = 5) then
--								elsif(var_counter_config = 6) then																		
								elsif(var_counter_config = 7) then
									configuration_parameters_offset <= unsigned(CONFIG_INPUT_AXIS_TDATA)-1;
								elsif(var_counter_config = 8) then
									configuration_dsp_offset <= unsigned(CONFIG_INPUT_AXIS_TDATA)-1;
								elsif(var_counter_config = 9) then
									configuration_fir_band_pass_offset <= unsigned(CONFIG_INPUT_AXIS_TDATA)-1;
								elsif(var_counter_config = 10) then
									configuration_fir_gaussian_offset <= unsigned(CONFIG_INPUT_AXIS_TDATA)-1;
								elsif(var_counter_config = 11) then
									configuration_fir_envelope_offset <= unsigned(CONFIG_INPUT_AXIS_TDATA)-1;
								elsif(var_counter_config = 12) then
									configuration_dac_curves_offset <= unsigned(CONFIG_INPUT_AXIS_TDATA)-1;
								elsif(var_counter_config = 13) then
									configuration_parameters_size <= unsigned(CONFIG_INPUT_AXIS_TDATA);
							    --Reserved
--                              elsif(var_counter_config = 14) then
--                              elsif(var_counter_config = 15) then								
								elsif(var_counter_config = dsp_bandpass_n_taps_offset) then
									dsp_bandpass_n_taps_i <= unsigned(CONFIG_INPUT_AXIS_TDATA);
								elsif(var_counter_config = dsp_gaussian_n_taps_offset) then
									dsp_gaussian_n_taps_i <= unsigned(CONFIG_INPUT_AXIS_TDATA);
								elsif(var_counter_config = dsp_envelope_n_taps_offset) then
									dsp_envelope_n_taps_i <= unsigned(CONFIG_INPUT_AXIS_TDATA);
								elsif(var_counter_config = configuration_dac_curves_offset) then
								    if(channel_configuration_size_i > configuration_dac_curves_offset) then
                                        configuration_dac_curves_size <= channel_configuration_size_i - configuration_dac_curves_offset - 1;
                                    else
                                        configuration_dac_curves_size <= to_unsigned(0, configuration_dac_curves_size'length); 
                                    end if;
								end if;	
                                if(CONFIG_INPUT_AXIS_TLAST = '1') then
                                    state_general_sm <= write_config_end;
                                end if;
								var_counter_config := var_counter_config + 1;
							end if;
						else
							state_general_sm <= write_config_end;	
						end if;
					when write_config_end =>
						if(CONFIG_INPUT_LOAD = '0') then
						    CONFIG_NEW_RECEIVER     <= '1';
							CONFIG_NEW_TRANSMITTER 	<= '1';
							CONFIG_NEW_MAGNET 		<= '1';
							CONFIG_NEW_DSP_1 		<= '1';
							CONFIG_NEW_DSP_2 		<= '1';
							CONFIG_NEW_FIR 			<= '1';
							CONFIG_NEW_GATES 		<= '1';
							CONFIG_NEW_DAC 			<= '1';
							there_is_configuration_i  <= '1';
							state_general_sm <= idle;	
						end if;
				    when recover_config =>
						CONFIG_NEW_RECEIVER     <= '1';
                        CONFIG_NEW_TRANSMITTER  <= '1';
                        CONFIG_NEW_MAGNET       <= '1';
                        CONFIG_NEW_DSP_1        <= '1';
                        CONFIG_NEW_DSP_2        <= '1';
                        CONFIG_NEW_FIR          <= '1';
                        CONFIG_NEW_GATES        <= '1';
                        CONFIG_NEW_DAC          <= '1';
                        there_is_configuration_i<= '1';
                        state_general_sm <= idle;    
					when update_receiver =>
						if(CONFIG_UPDATE_RECEIVER = '1') then
							if(update_receiver_end = '1') then
							    CONFIG_NEW_RECEIVER	<= '0';
							else
								if(configuration_bram_addrb_first = '1') then
									configuration_bram_addrb <=	std_logic_vector(receiver_offset(BRAM_ADDR_WIDTH-1 downto 0));
									configuration_bram_addrb_first <= '0';
								else
									configuration_bram_addrb <= std_logic_vector(unsigned(configuration_bram_addrb)+1);	
								end if;
								update_receiver_flag <= '1';
							end if;
						else
						    CONFIG_NEW_RECEIVER	<= '0';	
						    update_receiver_flag <= '0';
							state_general_sm 	<= idle;
						end if;
					when update_transmitter =>
						if(CONFIG_UPDATE_TRANSMITTER = '1') then
							if(update_transmitter_end = '1') then
							    CONFIG_NEW_TRANSMITTER 	<= '0';	
							else
								if(configuration_bram_addrb_first = '1') then
									configuration_bram_addrb <=	std_logic_vector(transmitter_offset(BRAM_ADDR_WIDTH-1 downto 0));
									configuration_bram_addrb_first <= '0';
								else
									configuration_bram_addrb <= std_logic_vector(unsigned(configuration_bram_addrb)+1);	
								end if;
								update_transmitter_flag <= '1';
							end if;
						else	
						    CONFIG_NEW_TRANSMITTER 	<= '0';
							update_transmitter_flag <= '0';
							state_general_sm 		<= idle;
						end if;		
					when update_magnet =>
						if(CONFIG_UPDATE_MAGNET = '1') then
							if(update_magnet_end = '1') then
							    CONFIG_NEW_MAGNET	<= '0';	
							else
								if(configuration_bram_addrb_first = '1') then
									configuration_bram_addrb <=	std_logic_vector(magnet_offset(BRAM_ADDR_WIDTH-1 downto 0));
									configuration_bram_addrb_first <= '0';
								else
									configuration_bram_addrb <= std_logic_vector(unsigned(configuration_bram_addrb)+1);	
								end if;
								update_magnet_flag <= '1';
							end if;
						else	
						    CONFIG_NEW_MAGNET	<= '0';						
							update_magnet_flag <= '0';
							state_general_sm 	<= idle;
						end if;							
					when update_dsp_1 =>
						if(CONFIG_UPDATE_DSP_1 = '1') then
							if(update_dsp_1_end = '1') then
							    CONFIG_NEW_DSP_1 <= '0';	
							else
								if(configuration_bram_addrb_first = '1') then
									configuration_bram_addrb <=	std_logic_vector(dsp_1_offset(BRAM_ADDR_WIDTH-1 downto 0));
									configuration_bram_addrb_first <= '0';
								else
									configuration_bram_addrb <= std_logic_vector(unsigned(configuration_bram_addrb)+1);	
								end if;
								update_dsp_1_flag <= '1';
							end if;
						else	
						    CONFIG_NEW_DSP_1 <= '0';
							update_dsp_1_flag <= '0';
							state_general_sm <= idle;
						end if;		
					when update_dsp_2 =>
						if(CONFIG_UPDATE_DSP_2 = '1') then
							if(update_dsp_2_end = '1') then
							    CONFIG_NEW_DSP_2 <= '0';
							else
								if(configuration_bram_addrb_first = '1') then
									configuration_bram_addrb <=	std_logic_vector(dsp_2_offset(BRAM_ADDR_WIDTH-1 downto 0));
									configuration_bram_addrb_first <= '0';
								else
									configuration_bram_addrb <= std_logic_vector(unsigned(configuration_bram_addrb)+1);	
								end if;
								update_dsp_2_flag <= '1';
							end if;
						else
						    CONFIG_NEW_DSP_2 <= '0';	
							update_dsp_2_flag <= '0';	
							state_general_sm <= idle;
						end if;		
					when update_FIR =>
						if(CONFIG_UPDATE_FIR = '1') then
							if(update_fir_end = '1') then
							    CONFIG_NEW_FIR 	 <= '0';
							else
								if(configuration_bram_addrb_first = '0' or CONFIG_FIR_AXIS_TREADY = '1') then
									if(configuration_bram_addrb_first = '1') then
										configuration_bram_addrb <=	std_logic_vector(fir_offset(BRAM_ADDR_WIDTH-1 downto 0));
										configuration_bram_addrb_first <= '0';
									else
										configuration_bram_addrb <= std_logic_vector(unsigned(configuration_bram_addrb)+1);	
									end if;
									update_fir_flag <= '1';	
							    else
							        if(var_counter_config >= 32768) then
							            CONFIG_NEW_FIR 	 <= '0';
							            update_fir_flag <= '0';
							            state_general_sm <= idle;	
							        else
							            var_counter_config:=var_counter_config+1; 
							        end if;				    
								end if;
							end if;
						else	
						    CONFIG_NEW_FIR 	 <= '0';
							update_fir_flag <= '0';	
							state_general_sm <= idle;
						end if;				
					when update_gates =>
						if(CONFIG_UPDATE_GATES = '1') then
							if(update_gates_end = '1') then
							    CONFIG_NEW_GATES <= '0';	
							else
								if(configuration_bram_addrb_first = '1') then
									configuration_bram_addrb <=	std_logic_vector(gates_offset(BRAM_ADDR_WIDTH-1 downto 0));
									configuration_bram_addrb_first <= '0';
								else
									configuration_bram_addrb <= std_logic_vector(unsigned(configuration_bram_addrb)+1);	
								end if;
								update_gates_flag <= '1';
							end if;
						else
						    CONFIG_NEW_GATES <= '0';	
							update_gates_flag <= '0';
							state_general_sm <= idle;
						end if;			
					when update_DAC =>
						if(CONFIG_UPDATE_DAC= '1') then						    
							if(update_dac_end = '1') then
							    CONFIG_NEW_DAC 	 <= '0';
							else
								if(configuration_bram_addrb_first = '0' or CONFIG_DAC_AXIS_TREADY = '1') then
									if(configuration_bram_addrb_first = '1') then
										configuration_bram_addrb <=	std_logic_vector(dac_offset(BRAM_ADDR_WIDTH-1 downto 0));
										configuration_bram_addrb_first <= '0';
									else
										configuration_bram_addrb <= std_logic_vector(unsigned(configuration_bram_addrb)+1);	
									end if;
									update_dac_flag <= '1';
							    else
							        if(var_counter_config >= 32768) then
							            CONFIG_NEW_DAC 	 <= '0';
                                        update_dac_flag <= '0';
                                        state_general_sm <= idle;
							        else
							            var_counter_config:=var_counter_config+1;
							        end if;
								end if;
							end if;
						else	
						    CONFIG_NEW_DAC 	 <= '0';
							update_dac_flag <= '0';	
							state_general_sm <= idle;
						end if;			
					when others =>
						null;
				end case;
			end if;
         end if;
    end process;
    																																			 			 
	-- Update receiver params
    update_receiver_process: process(aclk)
		variable var_counter_config : integer range 0 to 65536 :=0;
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				RECEIVER_SAMPLING_FREQUENCY <= (others => '0');		
				RECEIVER_DATA_WINDOW 		<= (others => '0');	
				RECEIVER_DELAY 				<= (others => '0');	
				RECEIVER_ANALOG_GAIN 		<= (others => '0');
				RECEIVER_DIGITAL_GAIN	    <= (others => '0');
				RECEIVER_EXTERNAL_MULTIPLEXER_EN <= '0';
				update_receiver_end <= '0';
        	else
				if(update_receiver_flag_i(2) = '1') then
					case var_counter_config is        
						when 0 =>
							RECEIVER_SAMPLING_FREQUENCY <= configuration_bram_doutb;
						when 1 =>
						    if(unsigned(configuration_bram_doutb) >= 1024) then
						        RECEIVER_DATA_WINDOW <= configuration_bram_doutb;
						    else
						        RECEIVER_DATA_WINDOW <= std_logic_vector(to_unsigned(1024, RECEIVER_DATA_WINDOW'length));
						    end if;
						when 2 =>
							RECEIVER_DELAY <= configuration_bram_doutb;
						when 3 =>
							RECEIVER_ANALOG_GAIN <= configuration_bram_doutb;
						when 4 =>
                            RECEIVER_DIGITAL_GAIN <= configuration_bram_doutb;	
						when 5 =>
							RECEIVER_EXTERNAL_MULTIPLEXER_EN <= configuration_bram_doutb(0);
							update_receiver_end <= '1';
						when others =>
							null;
					end case;
					if(update_receiver_end = '0') then
					   var_counter_config := var_counter_config + 1;
				    end if;
				else
					update_receiver_end <= '0';
					var_counter_config := 0;	
				end if;
			end if;
		end if;
	end process;
				
	-- Update transmitter params
    update_transmiter_process: process(aclk)
		variable var_counter_config : integer range 0 to 65536 :=0;
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				TRANSMITTER_VOLTAGE 		<= (others => '0');		
				TRANSMITTER_BURST_FREQUENCY <= (others => '0');	
				TRANSMITTER_N_CYCLES		<= (others => '0');
				TRANSMITTER_N_BURST	        <= (others => '0');
				TRANSMITTER_PRF 			<= (others => '0');	
				TRANSMITTER_DELAY			<= (others => '0');	
				TRANSMITTER_DELTA_DELAY		<= (others => '0');
				TRANSMITTER_DELTA_ADD		<= (others => '0');
				TRANSMITTER_DIRECTIONAL_PHASING	<= (others => '0');
				TRANSMITTER_PHASE_SHIFT     <= (others => '0');
				TRANSMITTER_START_CYCLE     <= (others => '0');
				update_transmitter_end <= '0';
        	else
				if(update_transmitter_flag_i(2) = '1') then
					case var_counter_config is        
						when 0 =>
							TRANSMITTER_VOLTAGE <= configuration_bram_doutb;
						when 1 =>
							TRANSMITTER_BURST_FREQUENCY <= configuration_bram_doutb;
						when 2 =>
							TRANSMITTER_N_CYCLES <= configuration_bram_doutb;
						when 3 => 
						    TRANSMITTER_N_BURST <= configuration_bram_doutb;
						when 4 =>
							TRANSMITTER_PRF <= configuration_bram_doutb;
						when 5 =>
							TRANSMITTER_DELAY <= configuration_bram_doutb;
					    when 6 =>
                            TRANSMITTER_DELTA_DELAY <= configuration_bram_doutb;
                        when 7 =>
                            TRANSMITTER_DELTA_ADD <= configuration_bram_doutb;
						when 8 =>
							TRANSMITTER_DIRECTIONAL_PHASING <= configuration_bram_doutb;
							--update_transmitter_end <= '1';
					    when 9 => 
							TRANSMITTER_PHASE_SHIFT <= configuration_bram_doutb;
                            --update_transmitter_end <= '1';			
                        when 10 => 
                            TRANSMITTER_START_CYCLE	<= configuration_bram_doutb(1 downto 0);
                            update_transmitter_end <= '1';	    
						when others =>
							null;
					end case;
					if(update_transmitter_end = '0') then
                        var_counter_config := var_counter_config + 1;
                    end if;
				else
					update_transmitter_end <= '0';
					var_counter_config := 0;	
				end if;
			end if;
		end if;
	end process;
				
	-- Update magnet params
    update_magnet_process: process(aclk)
		variable var_counter_config : integer range 0 to 65536 :=0;
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				MAGNET_MODE 			<= (others => '0');		
				MAGNET_PULSE_WIDTH 		<= (others => '0');	
				MAGNET_INITIAL_DELAY	<= (others => '0');	
				MAGNET_RAMP_UP_VOLTAGE	<= (others => '0');	
				MAGNET_VOLTAGE	        <= (others => '0');	
				update_magnet_end 	<= '0';
        	else
				if(update_magnet_flag_i(2) = '1') then
					case var_counter_config is        
						when 0 =>
							MAGNET_MODE <= configuration_bram_doutb;
						when 1 =>
							MAGNET_PULSE_WIDTH <= configuration_bram_doutb;
						when 2 =>
							MAGNET_INITIAL_DELAY <= configuration_bram_doutb;
						when 3 =>
                            MAGNET_RAMP_UP_VOLTAGE <= configuration_bram_doutb;
						when 4 =>
                            MAGNET_VOLTAGE <= configuration_bram_doutb;
							update_magnet_end 	<= '1';
						when others =>
							null;
					end case;
					if(update_magnet_end = '0') then
                        var_counter_config := var_counter_config + 1;
                    end if;
				else
					update_magnet_end <= '0';
					var_counter_config := 0;	
				end if;
			end if;
		end if;
	end process;
			
	-- Update DSP_1 params
    update_dsp_1_process: process(aclk)
		variable var_counter_config : integer range 0 to 65536 :=0;
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				DSP_AVERAGE 				<= (others => '0');		
				DSP_AVERAGE_TYPE 		    <= (others => '0');	
				DSP_NOISE_REDUCTION_FILTER 	<= (others => '0');	
				DSP_PHASE_SHIFT             <= (others => '0');	
				DSP_ANALOG_FILTER           <= (others => '0');	
				DSP_INVERSION               <= '0';	
				DSP_COMPRESSION_MODE        <= (others => '0');	
                DSP_COMPRESSION_LEVEL       <= (others => '0');    
                DSP_AVG_COINC_ORDER         <= '0';					
				update_dsp_1_end 	<= '0';
        	else
				if(update_dsp_1_flag_i(2) = '1') then
					case var_counter_config is        
						when 0 =>
						    if(unsigned(configuration_bram_doutb) < 1) then
						        DSP_AVERAGE <= std_logic_vector(to_unsigned(1,DSP_AVERAGE'length));	 
							else
					            DSP_AVERAGE <= configuration_bram_doutb;
					        end if;
						when 1 =>
                            DSP_AVERAGE_TYPE <= configuration_bram_doutb;						
						when 2 =>
						    if(unsigned(configuration_bram_doutb) < 1) then
						        DSP_NOISE_REDUCTION_FILTER <= std_logic_vector(to_unsigned(1,DSP_NOISE_REDUCTION_FILTER'length));	
						    else
						        DSP_NOISE_REDUCTION_FILTER <= configuration_bram_doutb; 
						    end if;
					    when 3 =>
					        DSP_PHASE_SHIFT <= configuration_bram_doutb;
						when 13 =>
						    DSP_ANALOG_FILTER <= std_logic_vector(resize(unsigned(configuration_bram_doutb), DSP_ANALOG_FILTER'length));
					    when 18 => 
                            DSP_INVERSION <= configuration_bram_doutb(0);
                            --update_dsp_1_end  <= '1';
					    when 21 => 
                            DSP_COMPRESSION_MODE <= configuration_bram_doutb;
                        when 22 => 
                            DSP_COMPRESSION_LEVEL <= configuration_bram_doutb;
                            --update_dsp_1_end  <= '1';
                        when 23 => 
                            DSP_AVG_COINC_ORDER <= configuration_bram_doutb(0);
                            update_dsp_1_end  <= '1';
						when others =>
							null;
					end case;
					if(update_dsp_1_end = '0') then
                        var_counter_config := var_counter_config + 1;
                    end if;
				else
					update_dsp_1_end <= '0';
					var_counter_config := 0;	
				end if;
			end if;
		end if;
	end process;
							
	-- Update DSP_2 params
    update_dsp_2_process: process(aclk)
		variable var_counter_config : integer range 0 to 65536 :=0;
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				DSP_BANDPASS_EN <= '0';	
				DSP_BANDPASS_N_TAPS <= (others => '0');	
				DSP_BANDPASS_L <= (others => '0');	
				DSP_GAUSSIAN_EN <= '0';
				DSP_GAUSSIAN_N_TAPS <= (others => '0');	
				DSP_GAUSSIAN_L <= (others => '0');	
				DSP_ENVELOPE_EN <= '0';	
				DSP_ENVELOPE_N_TAPS <= (others => '0');	
				DSP_ENVELOPE_L <= (others => '0');
				DSP_DEC_TYPE <= (others => '0');
				DSP_DEC_LEVEL <= (others => '0');
				DSP_DEC_WINDOW <= (others => '0');	
				DSP_GAUSSIAN_DELAY <= (others => '0');	
				DSP_RECTIFICATION <= (others => '0');	
				DSP_FIR_DELAY <= (others => '0');
				update_dsp_2_end 	<= '0';
        	else
				if(update_dsp_2_flag_i(2) = '1') then
					case var_counter_config is        
						when 0 =>
							DSP_BANDPASS_EN <= configuration_bram_doutb(0);
						when 1 =>
							DSP_BANDPASS_N_TAPS <= configuration_bram_doutb;
						when 2 =>
							DSP_BANDPASS_L <= configuration_bram_doutb;
						when 3 =>
							DSP_GAUSSIAN_EN <= configuration_bram_doutb(0);
						when 4 =>
							DSP_GAUSSIAN_N_TAPS <= configuration_bram_doutb;
						when 5 =>
							DSP_GAUSSIAN_L <= configuration_bram_doutb;
						when 6 =>
							DSP_ENVELOPE_EN <= configuration_bram_doutb(0);
						when 7 =>
							DSP_ENVELOPE_N_TAPS <= configuration_bram_doutb;
						when 8 =>
							DSP_ENVELOPE_L <= configuration_bram_doutb;
						when 10 =>
                            DSP_DEC_TYPE <= configuration_bram_doutb;
						when 11 =>
                            DSP_DEC_LEVEL <= configuration_bram_doutb;
						when 12 =>
                            DSP_DEC_WINDOW <= configuration_bram_doutb;
                        when 13 => 
                            DSP_GAUSSIAN_DELAY <= configuration_bram_doutb;
                            --update_dsp_2_end 	<= '1';
                        when 15 => 
                            DSP_RECTIFICATION <= std_logic_vector(resize(unsigned(configuration_bram_doutb), DSP_RECTIFICATION'length));
                        when 16 => 
                            DSP_FIR_DELAY <= std_logic_vector(resize(unsigned(configuration_bram_doutb), DSP_FIR_DELAY'length));
                            update_dsp_2_end <= '1';
						when others =>
							null;
					end case;
					if(update_dsp_2_end = '0') then
                        var_counter_config := var_counter_config + 1;
                    end if;
				else
					update_dsp_2_end <= '0';
					var_counter_config := 0;	
				end if;
			end if;
		end if;
	end process;
			
	-- Update gates params
    update_gates_process: process(aclk)
		variable var_counter_config : integer range 0 to 65536 :=0;
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				GATE1_EN 							<= '0';		
				GATE1_START 						<= (others => '0');	
				GATE1_WIDTH 						<= (others => '0');	
				GATE1_AMP_THRESHOLD					<= (others => '0');	
				GATE1_TOF_ALGORITHM 				<= (others => '0');	
				GATE1_TOF_AVG						<= (others => '0');	
				GATE1_AMP_THRESHOLD_ALARM_EN		<= '0';
				GATE1_AMP_THRESHOLD_ALARM_CROSSING	<= (others => '0');	
				GATE1_TOF_ALARM_EN					<= '0';
				GATE1_TOF_ALARM_TYPE				<= '0';
				GATE1_TOF_ALARM_CROSSING			<= (others => '0');	
				GATE1_TOF_ALARM_MIN_VALUE			<= (others => '0');	
				GATE1_TOF_ALARM_MAX_VALUE			<= (others => '0');	
				GATE1_TOF_MIN_THICKNESS				<= (others => '0');	
				GATE2_EN 							<= '0';		
				GATE2_START 						<= (others => '0');	
				GATE2_WIDTH 						<= (others => '0');	
				GATE2_AMP_THRESHOLD					<= (others => '0');	
				GATE2_TOF_ALGORITHM 				<= (others => '0');	
				GATE2_TOF_AVG						<= (others => '0');	
				GATE2_AMP_THRESHOLD_ALARM_EN		<= '0';
				GATE2_AMP_THRESHOLD_ALARM_CROSSING	<= (others => '0');	
				GATE2_TOF_ALARM_EN					<= '0';
				GATE2_TOF_ALARM_TYPE				<= '0';
				GATE2_TOF_ALARM_CROSSING			<= (others => '0');	
				GATE2_TOF_ALARM_MIN_VALUE			<= (others => '0');	
				GATE2_TOF_ALARM_MAX_VALUE			<= (others => '0');	
				GATE2_TOF_MIN_THICKNESS				<= (others => '0');	
				GATE2_FFT_EN                        <= '0';	
                GATE2_CC_EN                         <= '0';	
                GATE2_FFT_SEGMENT				    <= (others => '0');	
                GATE2_FFT_NFFT				        <= (others => '0');	
                
				GATE3_EN 							<= '0';		
				GATE3_START 						<= (others => '0');	
				GATE3_WIDTH 						<= (others => '0');	
				GATE3_AMP_THRESHOLD					<= (others => '0');	
				GATE3_TOF_ALGORITHM 				<= (others => '0');	
				GATE3_TOF_AVG						<= (others => '0');	
				GATE3_AMP_THRESHOLD_ALARM_EN		<= '0';
				GATE3_AMP_THRESHOLD_ALARM_CROSSING	<= (others => '0');	
				GATE3_TOF_ALARM_EN					<= '0';
				GATE3_TOF_ALARM_TYPE				<= '0';
				GATE3_TOF_ALARM_CROSSING			<= (others => '0');	
				GATE3_TOF_ALARM_MIN_VALUE			<= (others => '0');	
				GATE3_TOF_ALARM_MAX_VALUE			<= (others => '0');	
				GATE3_TOF_MIN_THICKNESS				<= (others => '0');	
				update_gates_end <= '0';
        	else
				if(update_gates_flag_i(2) = '1') then
					case var_counter_config is        
						when 1 =>
							GATE1_EN <= configuration_bram_doutb(0);
						when 2 =>
							GATE1_START <= std_logic_vector(resize(unsigned(configuration_bram_doutb), GATE1_START'length));
						when 3 =>
							GATE1_WIDTH <= std_logic_vector(resize(unsigned(configuration_bram_doutb), GATE1_WIDTH'length));
						when 4 =>
							GATE1_AMP_THRESHOLD <= std_logic_vector(resize(unsigned(configuration_bram_doutb), GATE1_AMP_THRESHOLD'length));
						when 5 =>
							GATE1_TOF_ALGORITHM <= std_logic_vector(resize(unsigned(configuration_bram_doutb), GATE1_TOF_ALGORITHM'length));													  
						when 6 =>
							GATE1_TOF_AVG <= std_logic_vector(resize(unsigned(configuration_bram_doutb), GATE1_TOF_AVG'length));	
						when 7 =>
                            GATE1_TOF_MIN_THICKNESS <= std_logic_vector(resize(unsigned(configuration_bram_doutb), GATE1_TOF_MIN_THICKNESS'length));													  
						when 8 =>
							GATE1_AMP_THRESHOLD_ALARM_EN <= configuration_bram_doutb(0);													  
						when 9 =>
							GATE1_AMP_THRESHOLD_ALARM_CROSSING <= std_logic_vector(resize(unsigned(configuration_bram_doutb), GATE1_AMP_THRESHOLD_ALARM_CROSSING'length));													  
						when 10 =>
							GATE1_TOF_ALARM_EN <= configuration_bram_doutb(0);													  
						when 11 =>
							GATE1_TOF_ALARM_TYPE <= configuration_bram_doutb(0);													  
						when 12 =>
							GATE1_TOF_ALARM_CROSSING <= std_logic_vector(resize(unsigned(configuration_bram_doutb), GATE1_TOF_ALARM_CROSSING'length));													  
						when 13 =>
							GATE1_TOF_ALARM_MIN_VALUE <= configuration_bram_doutb;													  
						when 14 =>
							GATE1_TOF_ALARM_MAX_VALUE <= configuration_bram_doutb;													  
--						when 15 =>
--						when 16 =>		
--						when 17 =>
--						when 18 =>		
--						when 19 =>
--						when 20 =>		
--						when 21 =>
--						when 22 =>		
--						when 23 =>
--						when 24 =>																	  
						when 25 =>
							GATE2_EN <= configuration_bram_doutb(0);
						when 26 =>
							GATE2_START <= std_logic_vector(resize(unsigned(configuration_bram_doutb), GATE2_START'length));
						when 27 =>
							GATE2_WIDTH <= std_logic_vector(resize(unsigned(configuration_bram_doutb), GATE2_WIDTH'length));
						when 28 =>
							GATE2_AMP_THRESHOLD <= std_logic_vector(resize(unsigned(configuration_bram_doutb), GATE2_AMP_THRESHOLD'length));
						when 29 =>
							GATE2_TOF_ALGORITHM <= std_logic_vector(resize(unsigned(configuration_bram_doutb), GATE2_TOF_ALGORITHM'length));													  
						when 30 =>
							GATE2_TOF_AVG <= std_logic_vector(resize(unsigned(configuration_bram_doutb), GATE2_TOF_AVG'length));	
						when 31 =>
                            GATE2_TOF_MIN_THICKNESS <= std_logic_vector(resize(unsigned(configuration_bram_doutb), GATE2_TOF_MIN_THICKNESS'length));													  
						when 32 =>
							GATE2_AMP_THRESHOLD_ALARM_EN <= configuration_bram_doutb(0);													  
						when 33 =>
							GATE2_AMP_THRESHOLD_ALARM_CROSSING <= std_logic_vector(resize(unsigned(configuration_bram_doutb), GATE2_AMP_THRESHOLD_ALARM_CROSSING'length));													  
						when 34 =>
							GATE2_TOF_ALARM_EN <= configuration_bram_doutb(0);													  
						when 35 =>
							GATE2_TOF_ALARM_TYPE <= configuration_bram_doutb(0);													  
						when 36 =>
							GATE2_TOF_ALARM_CROSSING <= std_logic_vector(resize(unsigned(configuration_bram_doutb), GATE2_TOF_ALARM_CROSSING'length));													  
						when 37 =>
							GATE2_TOF_ALARM_MIN_VALUE <= configuration_bram_doutb;													  
						when 38 =>
							GATE2_TOF_ALARM_MAX_VALUE <= configuration_bram_doutb;													  
						when 39 =>
						    GATE2_FFT_EN <= configuration_bram_doutb(0);
						when 40 =>
						    GATE2_CC_EN <= configuration_bram_doutb(0);						    
						when 41 =>
						    GATE2_FFT_SEGMENT <= std_logic_vector(resize(unsigned(configuration_bram_doutb), GATE2_FFT_SEGMENT'length));
						when 42 =>
						    GATE2_FFT_NFFT <= std_logic_vector(resize(unsigned(configuration_bram_doutb), GATE2_FFT_NFFT'length));
						
--						when 43 =>
--						when 44 =>
--						when 45 =>
--						when 46 =>
--						when 47 =>
--						when 48 =>																				
						when 49 =>
							GATE3_EN <= configuration_bram_doutb(0);
						when 50 =>
							GATE3_START <= std_logic_vector(resize(unsigned(configuration_bram_doutb), GATE3_START'length));
						when 51 =>
							GATE3_WIDTH <= std_logic_vector(resize(unsigned(configuration_bram_doutb), GATE3_WIDTH'length));
						when 52 =>
							GATE3_AMP_THRESHOLD <= std_logic_vector(resize(unsigned(configuration_bram_doutb), GATE3_AMP_THRESHOLD'length));
						when 53 =>
							GATE3_TOF_ALGORITHM <= std_logic_vector(resize(unsigned(configuration_bram_doutb), GATE3_TOF_ALGORITHM'length));													  
						when 54 =>
							GATE3_TOF_AVG <= std_logic_vector(resize(unsigned(configuration_bram_doutb), GATE3_TOF_AVG'length));	
		                when 55 =>
                            GATE3_TOF_MIN_THICKNESS <= std_logic_vector(resize(unsigned(configuration_bram_doutb), GATE3_TOF_MIN_THICKNESS'length));																  
						when 56 =>
							GATE3_AMP_THRESHOLD_ALARM_EN <= configuration_bram_doutb(0);													  
						when 57 =>
							GATE3_AMP_THRESHOLD_ALARM_CROSSING <= std_logic_vector(resize(unsigned(configuration_bram_doutb), GATE3_AMP_THRESHOLD_ALARM_CROSSING'length));													  
						when 58 =>
							GATE3_TOF_ALARM_EN <= configuration_bram_doutb(0);													  
						when 59 =>
							GATE3_TOF_ALARM_TYPE <= configuration_bram_doutb(0);													  
						when 60 =>
							GATE3_TOF_ALARM_CROSSING <= std_logic_vector(resize(unsigned(configuration_bram_doutb), GATE3_TOF_ALARM_CROSSING'length));													  
						when 61 =>
							GATE3_TOF_ALARM_MIN_VALUE <= configuration_bram_doutb;													  
						when 62 =>
							GATE3_TOF_ALARM_MAX_VALUE <= configuration_bram_doutb;													  												
							update_gates_end <= '1';
--						when 63 =>
--                      when 64 =>
--						when 65 =>
--                      when 66 =>
--						when 67 =>
--                      when 68 =>
--						when 69 =>
--                      when 70 =>
--						when 71 =>
--                      when 72 =>
																			  
						when others =>
							null;
					end case;
					
					if(update_gates_end = '0') then
                        var_counter_config := var_counter_config + 1;
                    end if;
				else
					update_gates_end <= '0';
					var_counter_config := 0;	
				end if;
			end if;
		end if;
	end process;
																				
	-- Update fir params
    update_fir_process: process(aclk)
		variable var_counter_config : integer range 0 to 65536 :=0;
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				CONFIG_FIR_AXIS_TDATA <= (others => '0');		
				CONFIG_FIR_AXIS_TLAST <= '0';
				CONFIG_FIR_AXIS_TVALID <= '0';
				update_fir_end <= '0';
        	else
				if(update_fir_flag_i(2) = '1') then
					if(var_counter_config < configuration_fir_size) then
						CONFIG_FIR_AXIS_TVALID <= '1';
						if(var_counter_config = configuration_fir_size-1) then
							CONFIG_FIR_AXIS_TLAST <= '1';	
						end if;
						CONFIG_FIR_AXIS_TDATA <= std_logic_vector(resize(signed(configuration_bram_doutb), CONFIG_FIR_AXIS_TDATA'length));	
						var_counter_config := var_counter_config + 1;
					else
						CONFIG_FIR_AXIS_TLAST <= '0';
						CONFIG_FIR_AXIS_TVALID <= '0';
						update_fir_end <= '1';			
					end if;
				else
					update_fir_end <= '0';
					var_counter_config := 0;	
				end if;
			end if;
		end if;
	end process;
																		 
	-- Update dac params
    update_dac_process: process(aclk)
        variable var_counter_config : integer range 0 to 65536 :=0;
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                CONFIG_DAC_AXIS_TDATA <= (others => '0');        
                CONFIG_DAC_AXIS_TLAST <= '0';
                CONFIG_DAC_AXIS_TVALID <= '0';
                update_dac_end <= '0';
            else
                if(update_dac_flag_i(2) = '1') then
                    if(var_counter_config < configuration_dac_curves_size and configuration_dac_curves_size > 0) then
                        CONFIG_DAC_AXIS_TVALID <= '1';
                        if(var_counter_config = configuration_dac_curves_size-1) then
                            CONFIG_DAC_AXIS_TLAST <= '1';    
                        end if;
                        CONFIG_DAC_AXIS_TDATA <= std_logic_vector(resize(signed(configuration_bram_doutb), CONFIG_DAC_AXIS_TDATA'length));    
                        var_counter_config := var_counter_config + 1;
                    else
                        CONFIG_DAC_AXIS_TLAST <= '0';
                        CONFIG_DAC_AXIS_TVALID <= '0';
                        update_dac_end <= '1';            
                    end if;
                else
                    update_dac_end <= '0';
                    var_counter_config := 0;    
                end if;
            end if;
        end if;
    end process;
	
	update_receiver_flag_i(0) <= update_receiver_flag;
    update_transmitter_flag_i(0) <= update_transmitter_flag;
    update_magnet_flag_i(0) <= update_magnet_flag;
    update_dsp_1_flag_i(0) <= update_dsp_1_flag;
    update_dsp_2_flag_i(0) <= update_dsp_2_flag;
    update_gates_flag_i(0) <= update_gates_flag;
    update_fir_flag_i(0) <= update_fir_flag;
    update_dac_flag_i(0) <= update_dac_flag;													
																																														
	-- Delay update flags    
    delay_update_flags : for i in 1 to 2 generate
        process(aclk) 
        begin
           if (rising_edge(aclk)) then
                if (aresetn = '0') then
                    update_receiver_flag_i(i)   <= '0';
			   		update_transmitter_flag_i(i)   <= '0';
					update_magnet_flag_i(i)   <= '0';
					update_dsp_1_flag_i(i)   <= '0';
					update_dsp_2_flag_i(i)   <= '0';
					update_gates_flag_i(i)   <= '0';
					update_fir_flag_i(i)   <= '0';
					update_dac_flag_i(i)   <= '0';													 	
                else
                    update_receiver_flag_i(i) <= update_receiver_flag_i(i-1);
					update_transmitter_flag_i(i) <= update_transmitter_flag_i(i-1);
					update_magnet_flag_i(i) <= update_magnet_flag_i(i-1);
					update_dsp_1_flag_i(i) <= update_dsp_1_flag_i(i-1);
					update_dsp_2_flag_i(i) <= update_dsp_2_flag_i(i-1);
					update_gates_flag_i(i) <= update_gates_flag_i(i-1);	
					update_fir_flag_i(i) <= update_fir_flag_i(i-1);		
					update_dac_flag_i(i) <= update_dac_flag_i(i-1);														 
                end if;
           end if;
        end process;
    end generate;
				 	   			   
    -- BRAM
    channel_configuration_BRAM_inst : channel_configuration_BRAM
      PORT MAP (
        clka => aclk,
        ena => configuration_bram_ena,
        wea => configuration_bram_wea,
        addra => configuration_bram_addra,
        dina => configuration_bram_dina,
        douta => configuration_bram_douta,
        clkb => aclk,
        enb => configuration_bram_enb,
        web => configuration_bram_web,
        addrb => configuration_bram_addrb,
        dinb => configuration_bram_dinb,
        doutb => configuration_bram_doutb
      );
           
end arch_imp;
