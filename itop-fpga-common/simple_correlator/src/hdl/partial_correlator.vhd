----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/01/2017 06:05:15 AM
-- Design Name: 
-- Module Name: partial_correlator - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_signed.all;
use ieee.numeric_std.all; 

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity partial_correlator is
    generic (
  		n_dsp_usage	        : integer	:= 52
  	);
	port (
	    aclk                : in std_logic;
	    aresetn             : in std_logic;
        -- data input
        DATA_IN_TVALID      : in std_logic;
        DATA_IN_TLAST       : in std_logic;
        DATA_IN_TREADY      : out std_logic;
        DATA_IN_TDATA1      : in std_logic_vector(17 downto 0);
        DATA_IN_TDATA2      : in std_logic_vector(17 downto 0);
        -- data output
        DATA_OUT_TVALID     : out std_logic;
        DATA_OUT_TLAST      : out std_logic;
        DATA_OUT_TREADY     : in std_logic;
        DATA_OUT_TDATA      : out std_logic_vector(47 downto 0)
    );
end partial_correlator;

architecture Behavioral of partial_correlator is

    -- PARTIAL CORRELATOR SM   
    type states_partial_corr is (idle,partial_corr_1, partial_corr_2, partial_corr_3, partial_corr_4); 
    signal state_partial_corr : states_partial_corr; 

    -- GENERAL
    type registered_gate_array is array ( 0 to (n_dsp_usage-1) ) of std_logic_vector(17 downto 0);
    signal registered_gates : registered_gate_array; 
    signal first_gate : std_logic_vector(17 downto 0);  
  
    type result_mult_array is array ( 0 to (n_dsp_usage-1) ) of std_logic_vector(47 downto 0);
    signal result_mult : result_mult_array;
        
    signal wait_for_acc_ready_counter : unsigned(7 downto 0);
    constant wait_for_acc_ready_counter_max : unsigned(7 downto 0):= "00001000";
    
    signal DATA_OUT_TDATA_counter : unsigned(15 downto 0);
    signal N_DSPs                 : unsigned(31 downto 0);
    
--    signal data_in_counter : unsigned(31 downto 0);
--    signal data_in_counter_i1 : unsigned(31 downto 0);
--    signal data_in_counter_i2 : unsigned(31 downto 0);
--    signal data_in_counter_i3 : unsigned(31 downto 0);
--    signal data_in_counter_i4 : unsigned(31 downto 0);
--    signal data_in_counter_i5 : unsigned(31 downto 0);
--    signal data_in_counter_registered : unsigned(31 downto 0);
--    signal data_in_counter_mul : unsigned(31 downto 0);
--    signal data_in_counter_acc : unsigned(31 downto 0);
        
    -- Z REGISTER
    component z_register is
        port (
        aclk    : in std_logic;
        clr     : in std_logic;      
        DATA_IN : in std_logic_vector(17 downto 0);
        DATA_OUT: out std_logic_vector(17 downto 0)
    );
    end component z_register;
             
    signal DATA_IN_TDATA1_i1  : std_logic_vector(17 downto 0);                                             
    signal DATA_IN_TDATA2_i1  : std_logic_vector(17 downto 0);
    signal DATA_IN_TVALID_i1 : std_logic;  
    signal DATA_IN_TLAST_i1  : std_logic;  
    
    signal sclr_z_register  : std_logic; 
    
    -- DSPs 48  
    COMPONENT simple_correlator_DSP
      PORT (
        CLK : IN STD_LOGIC;
        SCLR : IN STD_LOGIC;
        A : IN STD_LOGIC_VECTOR(17 DOWNTO 0);
        B : IN STD_LOGIC_VECTOR(17 DOWNTO 0);
        P : OUT STD_LOGIC_VECTOR(47 DOWNTO 0)
      );
    END COMPONENT;
    
    signal sclr_DSP  : std_logic; 
    
begin

    -- Partial corr SM process
    partial_corr: process(aresetn, aclk)   
    begin 
        if (aresetn = '0') then
            registered_gates(0) <= (others => '0');
            first_gate <= (others => '0');
            sclr_DSP <= '1';
            sclr_z_register <= '1';
            -- general
            wait_for_acc_ready_counter <= (others => '0');
            N_DSPs <= (others => '0');
            --data_in_counter <= (others => '0');
            -- input 
            DATA_IN_TREADY <= '0';
            -- output
            DATA_OUT_TVALID <= '0';
            DATA_OUT_TLAST <= '0';
            DATA_OUT_TDATA <= (others => '0');
            DATA_OUT_TDATA_counter <= (others => '0');
            -- state
            state_partial_corr <= idle;  
        elsif (aclk'event and aclk = '1') then
		    case state_partial_corr is                  
		      when idle =>
		          DATA_IN_TREADY <= '1';
		          if(DATA_IN_TVALID = '1') then
		              sclr_DSP <= '0';
		              sclr_z_register <= '0';
                      N_DSPs <= to_unsigned(n_dsp_usage, N_DSPs'length) - 1;
		              state_partial_corr <= partial_corr_1;
		              --data_in_counter <= (others => '0');
		          end if;
		      when partial_corr_1 =>
		          if(DATA_IN_TVALID_i1 = '1') then
		              sclr_z_register <= '0';
		              --data_in_counter <= data_in_counter + 1;
                      registered_gates(0) <= DATA_IN_TDATA2_i1;
                      first_gate <= DATA_IN_TDATA1_i1;
                      if(DATA_IN_TLAST_i1 = '1') then
                        wait_for_acc_ready_counter <= (others => '0');
                        state_partial_corr <= partial_corr_2;
                      end if;
                  else
                      sclr_DSP <= '1';
                      sclr_z_register <= '1';
                      state_partial_corr <= idle; 
                  end if;
		      when partial_corr_2 =>
		          DATA_IN_TREADY <= '0';
		          sclr_z_register <= '1';
		          registered_gates(0) <= (others => '0');
		          first_gate <= (others => '0');
		          if(DATA_IN_TVALID_i1 = '0') then
		              if(wait_for_acc_ready_counter >= wait_for_acc_ready_counter_max) then
		                  DATA_OUT_TDATA_counter <= (others => '0');
		                  state_partial_corr <= partial_corr_3;
		              else
		                  wait_for_acc_ready_counter <= wait_for_acc_ready_counter + 1;
		              end if;
		          end if;
		      when partial_corr_3 =>
		          if(DATA_OUT_TREADY = '1') then
		              DATA_OUT_TVALID <= '1';
		              DATA_OUT_TDATA <= result_mult(to_integer(DATA_OUT_TDATA_counter));
		              DATA_OUT_TDATA_counter <= DATA_OUT_TDATA_counter + 1;
		              if(DATA_OUT_TDATA_counter >= N_DSPs) then
		                  DATA_OUT_TLAST <= '1'; 
		                  state_partial_corr <= partial_corr_4;   
		              end if;
		          end if;
		      when partial_corr_4 =>
		          DATA_OUT_TVALID <= '0';
		          DATA_OUT_TLAST <= '0';
		          sclr_DSP <= '1';
                  sclr_z_register <= '1';
                  if(DATA_IN_TVALID = '0') then
		              state_partial_corr <= idle;
		          end if;
		      when others =>
		          null;
		    end case;
	   end if;
    end process;

	Z_REGISTERS : for i in 0 to (n_dsp_usage-2) generate
        z_register_inst: z_register
            port map (
            aclk    => aclk,
            clr     => sclr_z_register,      
            DATA_IN => registered_gates(i),
            DATA_OUT=> registered_gates(i+1)
    );
	end generate;
	
	delayed: process(aresetn, aclk)   
    begin 
        if (aresetn = '0') then
            DATA_IN_TLAST_i1 <= '0';
            DATA_IN_TVALID_i1 <= '0';
            DATA_IN_TDATA2_i1 <= (others => '0');
            DATA_IN_TDATA1_i1 <= (others => '0');
            -- data_in_counter for debug    
--            data_in_counter_i1 <= (others => '0');
--            data_in_counter_i2 <= (others => '0');
--            data_in_counter_i3 <= (others => '0');
--            data_in_counter_i4 <= (others => '0');
--            data_in_counter_i5 <= (others => '0');
        elsif (aclk'event and aclk = '1') then
            DATA_IN_TLAST_i1 <= DATA_IN_TLAST;
            DATA_IN_TVALID_i1 <= DATA_IN_TVALID;
            DATA_IN_TDATA2_i1 <= DATA_IN_TDATA2; 
            DATA_IN_TDATA1_i1 <= DATA_IN_TDATA1;    
            -- data_in_counter for debug        
--            data_in_counter_i1 <= data_in_counter;      
--            data_in_counter_i2 <= data_in_counter_i1; 
--            data_in_counter_i3 <= data_in_counter_i2;  
--            data_in_counter_i4 <= data_in_counter_i3; 
--            data_in_counter_i5 <= data_in_counter_i4;     
        end if;
    end process;
    
    -- data_in_counter for debug   
--    data_in_counter_registered <= data_in_counter;
--    data_in_counter_mul <= data_in_counter_i4;
--    data_in_counter_acc <= data_in_counter_i5;
         
	DSP_48s: for i in 0 to (n_dsp_usage-1) generate
        simple_correlator_DSP_inst : simple_correlator_DSP
            PORT MAP (
            CLK => aclk,
            SCLR => sclr_DSP,
            A => first_gate,
            B => registered_gates(i),
            P => result_mult(i)
        );
    end generate;
         
end Behavioral;
