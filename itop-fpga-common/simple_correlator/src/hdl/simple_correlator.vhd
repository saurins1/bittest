library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_signed.all;
use ieee.numeric_std.all; 

entity simple_correlator is
	generic (
		-- Users to add parameters here
        n_dsp_usage   : integer    := 52;
        wait_cycles   : integer    := 8;
		-- User parameters ends
		-- Do not modify the parameters beyond this line


		-- Parameters of Axi Slave Bus Interface S00_AXIS
		C_S00_AXIS_TDATA_WIDTH	: integer	:= 16;

		-- Parameters of Axi Master Bus Interface M00_AXIS
		C_M00_AXIS_TDATA_WIDTH	: integer	:= 48;
		C_M00_AXIS_START_COUNT	: integer	:= 32
	);
	port (
		-- Users to add ports here

		-- User ports ends
		-- Do not modify the ports beyond this line
        GATE_WIDTH	        : in std_logic_vector(31 downto 0);

		-- Ports of Axi Slave Bus Interface S00_AXIS
		s00_axis_aclk	    : in std_logic;
		s00_axis_aresetn	: in std_logic;
		s00_axis_tready	    : out std_logic;
		s00_axis_tdata	    : in std_logic_vector(C_S00_AXIS_TDATA_WIDTH-1 downto 0);
		s00_axis_tstrb	    : in std_logic_vector((C_S00_AXIS_TDATA_WIDTH/8)-1 downto 0);
		s00_axis_tlast	    : in std_logic;
		s00_axis_tvalid	    : in std_logic;

		-- Ports of Axi Master Bus Interface M00_AXIS
		m00_axis_aclk	    : in std_logic;
		m00_axis_aresetn	: in std_logic;
		m00_axis_tvalid	    : out std_logic;
		m00_axis_tdata	    : out std_logic_vector(C_M00_AXIS_TDATA_WIDTH-1 downto 0);
		m00_axis_tstrb	    : out std_logic_vector((C_M00_AXIS_TDATA_WIDTH/8)-1 downto 0);
		m00_axis_tlast	    : out std_logic;
		m00_axis_tready	    : in std_logic
	);
end simple_correlator;

architecture arch_imp of simple_correlator is

    -- General delayed
    signal s00_axis_tvalid_i1   : STD_LOGIC;
    signal s00_axis_tlast_i1    : STD_LOGIC;
    signal s00_axis_tdata_i1    : STD_LOGIC_VECTOR ( C_S00_AXIS_TDATA_WIDTH-1 downto 0 );
    
    signal m00_axis_tvalid_i1	: std_logic;
    signal m00_axis_tdata_i1    : std_logic_vector(C_M00_AXIS_TDATA_WIDTH-1 downto 0);
    signal m00_axis_tvalid_i2	: std_logic;
    signal m00_axis_tdata_i2    : std_logic_vector(C_M00_AXIS_TDATA_WIDTH-1 downto 0);
    
    -- CORRELATION SM   
    type states_corr is (idle,write_corr, write_corr_wait, read_data_window, 
        read_partial_corr, calculate_next_data_window, read_data_window_wait, end_corr); 
    signal state_corr : states_corr; 
    
    signal wait_counter : unsigned ( 14 downto 0 );
    
    signal GATE_WIDTH_i	: unsigned(31 downto 0);

    -- BRAM    
    COMPONENT simple_correlator_BRAM
      PORT (
        clka : IN STD_LOGIC;
        ena : IN STD_LOGIC;
        wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addra : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
        dina : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
        douta : OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
        clkb : IN STD_LOGIC;
        enb : IN STD_LOGIC;
        web : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addrb : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
        dinb : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
        doutb : OUT STD_LOGIC_VECTOR(13 DOWNTO 0)
      );
    END COMPONENT;
    
    -- PORTA BRAM
    signal BRAM_PORTA_addr  : STD_LOGIC_VECTOR ( 31 downto 0 );
    signal BRAM_PORTA_din   : STD_LOGIC_VECTOR ( 13 downto 0 );
    signal BRAM_PORTA_dout  : STD_LOGIC_VECTOR ( 13 downto 0 );
    signal BRAM_PORTA_en    : STD_LOGIC;
    signal BRAM_PORTA_we    : STD_LOGIC_VECTOR ( 0 to 0 );
    signal BRAM_PORTA_addr_counter : unsigned ( 31 downto 0 );
    
    -- PORTB BRAM
    signal BRAM_PORTB_addr  : STD_LOGIC_VECTOR ( 31 downto 0 );
    signal BRAM_PORTB_din   : STD_LOGIC_VECTOR ( 13 downto 0 );
    signal BRAM_PORTB_dout  : STD_LOGIC_VECTOR ( 13 downto 0 );
    signal BRAM_PORTB_en    : STD_LOGIC;
    signal BRAM_PORTB_we    : STD_LOGIC_VECTOR ( 0 to 0 );
    signal BRAM_PORTB_addr_counter : unsigned ( 31 downto 0 );
    
    -- PARTIAL CORRELATOR
    component partial_correlator is
        generic (
            n_dsp_usage    : integer    := 52
        );
        port (
            aclk            : in std_logic;
            aresetn         : in std_logic;
            -- data input
            DATA_IN_TVALID  : in std_logic;
            DATA_IN_TLAST   : in std_logic;
            DATA_IN_TREADY  : out std_logic;
            DATA_IN_TDATA1   : in std_logic_vector(17 downto 0);
            DATA_IN_TDATA2   : in std_logic_vector(17 downto 0);
            -- data output
            DATA_OUT_TVALID : out std_logic;
            DATA_OUT_TLAST  : out std_logic;
            DATA_OUT_TREADY : in std_logic;
            DATA_OUT_TDATA  : out std_logic_vector(47 downto 0)
        );
    end component;
    
    signal PC_N_DSPs          : std_logic_vector(31 downto 0);
    signal PC_DATA_IN_TVALID  : std_logic;
    signal PC_DATA_IN_TLAST   : std_logic;
    signal PC_DATA_IN_TREADY  : std_logic;
    signal PC_DATA_IN_TDATA1  : std_logic_vector(17 downto 0);
    signal PC_DATA_IN_TDATA2  : std_logic_vector(17 downto 0);
    signal PC_DATA_OUT_TVALID : std_logic;
    signal PC_DATA_OUT_TLAST  : std_logic;
    signal PC_DATA_OUT_TREADY : std_logic;
    signal PC_DATA_OUT_TDATA  : std_logic_vector(47 downto 0);
    
    signal PC_DATA_IN_TVALID_i1  : std_logic;
    signal PC_DATA_IN_TVALID_i2  : std_logic;
    signal PC_DATA_IN_TLAST_i1   : std_logic;
    signal PC_DATA_IN_TLAST_i2   : std_logic;
    
    signal read_index_ini  : UNSIGNED ( 31 downto 0 );
    signal read_index_end  : UNSIGNED ( 31 downto 0 );
    signal n_dsp : unsigned ( 31 downto 0 );
    signal n_dsp_max : unsigned ( 31 downto 0 ); -- 52
    
begin

    n_dsp_max <= to_unsigned(n_dsp_usage, n_dsp_max'length);
    
    -- Peak Detector SM
    peak_detector_process: process(s00_axis_aresetn, s00_axis_aclk)   
    begin 
        if (s00_axis_aresetn = '0') then
            -- BRAM
	        BRAM_PORTA_addr <= (others => '0');
            BRAM_PORTA_din <= (others => '0');
            BRAM_PORTA_we <= (others => '0');
            BRAM_PORTA_addr_counter <= (others => '0');
            BRAM_PORTA_en <= '1'; -- always enabled
	        BRAM_PORTB_addr <= (others => '0');
            BRAM_PORTB_din <= (others => '0');
            BRAM_PORTB_we <= (others => '0');
            BRAM_PORTB_addr_counter <= (others => '0');
            BRAM_PORTB_en <= '1'; -- always enabled
            -- AXI
            s00_axis_tready <= '0';
            m00_axis_tlast <= '0';
            m00_axis_tstrb <= (others => '1');
            -- Partial correlator
            n_dsp <= (others => '0');
            read_index_ini <= (others => '0');
            read_index_end <= (others => '0');
            PC_DATA_IN_TVALID <= '0';
            PC_DATA_IN_TLAST <= '0';
            PC_DATA_OUT_TREADY <= '0';
            -- state
            wait_counter <= (others => '0');
            state_corr <= idle;  
        elsif (s00_axis_aclk'event and s00_axis_aclk = '1') then
		    case state_corr is                  
		      when idle =>
		          s00_axis_tready <= '1';
		          PC_DATA_OUT_TREADY <= '1';
		          if(s00_axis_tvalid = '1') then
		              BRAM_PORTA_we <= (others => '1');
		              wait_counter <= (others => '0');
		              GATE_WIDTH_i <= unsigned(GATE_WIDTH);
		              state_corr <= write_corr;  
		          end if;
		      when write_corr =>
                if(s00_axis_tvalid_i1 = '1') then
                    BRAM_PORTA_din <= s00_axis_tdata_i1(13 downto 0);
                    BRAM_PORTA_addr <= std_logic_vector(BRAM_PORTA_addr_counter);
                    BRAM_PORTA_addr_counter <= BRAM_PORTA_addr_counter + 1;
                    if(s00_axis_tlast_i1 = '1' or BRAM_PORTA_addr_counter >=(GATE_WIDTH_i-1)) then
                        read_index_ini <= (others => '0');   
                        if( GATE_WIDTH_i >= n_dsp_max) then
                            n_dsp <= n_dsp_max;  
                        else
                            n_dsp <= GATE_WIDTH_i;
                        end if;
                        state_corr <= write_corr_wait;               
                    end if;
                else
                    BRAM_PORTA_we <= (others => '0');
                    BRAM_PORTA_addr_counter <= (others => '0');
                    BRAM_PORTB_addr_counter <= (others => '0');
                    state_corr <= idle; 
                end if;
              when write_corr_wait =>
                  BRAM_PORTA_addr_counter <= (others => '0');
                  BRAM_PORTB_addr_counter <= (others => '0');
              	  BRAM_PORTA_we <= (others => '0');
                  s00_axis_tready <= '0';
                  if(wait_counter >= to_unsigned(wait_cycles, wait_counter'length)) then
                    read_index_end <= n_dsp;
                    state_corr <= read_data_window;  
                  else
                    wait_counter <= wait_counter + 1;
                  end if;
		      when read_data_window =>
		          if(PC_DATA_IN_TREADY = '1') then
		              PC_DATA_IN_TVALID <= '1';		          
		              BRAM_PORTA_addr <= std_logic_vector(BRAM_PORTA_addr_counter);
		              BRAM_PORTB_addr <= std_logic_vector(BRAM_PORTB_addr_counter);
		              if(BRAM_PORTB_addr_counter >= unsigned(GATE_WIDTH)-1) then
		                  PC_DATA_IN_TLAST <= '1';
		                  state_corr <= read_partial_corr;  
		              else
		                  BRAM_PORTA_addr_counter <= BRAM_PORTA_addr_counter + 1; 
		                  BRAM_PORTB_addr_counter <= BRAM_PORTB_addr_counter + 1;
		              end if;
		          end if;
		      when read_partial_corr =>
                  PC_DATA_IN_TVALID <= '0';	
                  PC_DATA_IN_TLAST <= '0';
                  if(PC_DATA_OUT_TLAST = '1') then
                      read_index_ini <= read_index_ini + n_dsp; 
                      state_corr <= calculate_next_data_window;    
                  end if;
              when calculate_next_data_window =>
                  if(read_index_end >= (GATE_WIDTH_i -1)) then
                      m00_axis_tlast <= '1';
                      state_corr <= end_corr;
                  else
                      if((GATE_WIDTH_i-1) >= (read_index_end + n_dsp_max)) then
                          n_dsp <= n_dsp_max;
                          read_index_end <= read_index_ini + n_dsp_max;
                      else
                          n_dsp <= GATE_WIDTH_i - read_index_end;
                          read_index_end <= read_index_ini + (GATE_WIDTH_i - read_index_end) ;
                      end if;
                      BRAM_PORTA_addr_counter <= (others => '0');
                      BRAM_PORTB_addr_counter <= read_index_end;     
                      state_corr <= read_data_window;
                  end if;
               when end_corr =>
                  m00_axis_tlast <= '0';
                  BRAM_PORTA_addr_counter <= (others => '0');
                  BRAM_PORTB_addr_counter <= (others => '0');
                  state_corr <= idle;
		       when others =>
		          null;
		  end case;
        end if;
    end process;
    
    -- partial correlator inputs
    PC_N_DSPs <= std_logic_vector(n_dsp);
    PC_DATA_IN_TDATA1 <= BRAM_PORTB_dout (13) & BRAM_PORTB_dout (13) & BRAM_PORTB_dout (13) & BRAM_PORTB_dout (13) & BRAM_PORTB_dout;
    PC_DATA_IN_TDATA2 <= BRAM_PORTA_dout (13) & BRAM_PORTA_dout (13) & BRAM_PORTA_dout (13) & BRAM_PORTA_dout (13) & BRAM_PORTA_dout;
    
    -- general outputs
    m00_axis_tdata_i2 <= PC_DATA_OUT_TDATA;
    m00_axis_tvalid_i2 <= PC_DATA_OUT_TVALID;
       
    -- Delayed signals
    Delayed: process(s00_axis_aresetn, s00_axis_aclk)   
    begin 
        if (s00_axis_aresetn = '0') then
            -- stream input
            s00_axis_tvalid_i1 <= '0';
            s00_axis_tlast_i1 <= '0';
            s00_axis_tdata_i1 <= (others => '0');
            -- partial correlator
            PC_DATA_IN_TVALID_i1 <= '0';
            PC_DATA_IN_TVALID_i2 <= '0';
            PC_DATA_IN_TLAST_i1 <= '0';
            PC_DATA_IN_TLAST_i2 <= '0';
            -- stream output
            m00_axis_tvalid_i1 <= '0';
            m00_axis_tdata_i1 <= (others => '0');
            m00_axis_tvalid <= '0';
            m00_axis_tdata <= (others => '0');
        elsif (s00_axis_aclk'event and s00_axis_aclk = '1') then
            -- stream input
            s00_axis_tvalid_i1 <= s00_axis_tvalid;
            s00_axis_tlast_i1 <= s00_axis_tlast;
            s00_axis_tdata_i1 <= s00_axis_tdata; 
            -- partial correlator
            PC_DATA_IN_TVALID_i1 <= PC_DATA_IN_TVALID;
            PC_DATA_IN_TVALID_i2 <= PC_DATA_IN_TVALID_i1;    
            PC_DATA_IN_TLAST_i1 <= PC_DATA_IN_TLAST;
            PC_DATA_IN_TLAST_i2 <= PC_DATA_IN_TLAST_i1; 
            -- stream output
            m00_axis_tvalid_i1 <= m00_axis_tvalid_i2;  
            m00_axis_tvalid <= m00_axis_tvalid_i1;   
            m00_axis_tdata_i1 <= m00_axis_tdata_i2;  
            m00_axis_tdata <= m00_axis_tdata_i1;    
        end if;
    end process;
       
    --BRAM         
    simple_correlator_BRAM_inst : simple_correlator_BRAM
          PORT MAP (
            clka => s00_axis_aclk,
            ena => BRAM_PORTA_en,
            wea => BRAM_PORTA_we,
            addra => BRAM_PORTA_addr(14 downto 0),
            dina => BRAM_PORTA_din,
            douta => BRAM_PORTA_dout,
            clkb => s00_axis_aclk,
            enb => BRAM_PORTB_en,
            web => BRAM_PORTB_we,
            addrb => BRAM_PORTB_addr(14 downto 0),
            dinb => BRAM_PORTB_din,
            doutb => BRAM_PORTB_dout
          );
        
    -- Partial correlator
    partial_correlator_inst: partial_correlator
        generic map ( 
            n_dsp_usage => n_dsp_usage
        )
        port map(
            aclk            => s00_axis_aclk,
            aresetn         => s00_axis_aresetn,
            -- data input
            DATA_IN_TVALID  => PC_DATA_IN_TVALID_i2,
            DATA_IN_TLAST   => PC_DATA_IN_TLAST_i2,
            DATA_IN_TREADY  => PC_DATA_IN_TREADY,
            DATA_IN_TDATA1   => PC_DATA_IN_TDATA1,
            DATA_IN_TDATA2   => PC_DATA_IN_TDATA2,
            -- data output
            DATA_OUT_TVALID => PC_DATA_OUT_TVALID,
            DATA_OUT_TLAST  => PC_DATA_OUT_TLAST,
            DATA_OUT_TREADY => PC_DATA_OUT_TREADY,
            DATA_OUT_TDATA  => PC_DATA_OUT_TDATA
        );
    
end arch_imp;
