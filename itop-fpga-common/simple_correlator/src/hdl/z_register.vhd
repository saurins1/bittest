----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/01/2017 06:27:43 AM
-- Design Name: 
-- Module Name: z_register - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity z_register is
	port (
        aclk    : in std_logic;
        clr     : in std_logic;      
        DATA_IN : in std_logic_vector(17 downto 0);
        DATA_OUT: out std_logic_vector(17 downto 0)
    );
end z_register;

architecture Behavioral of z_register is

begin

    process(aclk)   
    begin 
        if (aclk'event and aclk = '1') then
            if(clr = '1') then
                DATA_OUT <= (others => '0');
            else
                DATA_OUT <= DATA_IN;    
            end if;    
        end if;
    end process;

end Behavioral;
