library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity simple_correlator_tb is
  	generic (
  	n_dsp_usage   : integer    := 52;
    C_S00_AXIS_TDATA_WIDTH    : integer    := 16;
    C_M00_AXIS_TDATA_WIDTH    : integer    := 48;
    C_M00_AXIS_START_COUNT    : integer    := 32
);
end;

architecture bench of simple_correlator_tb is

  component simple_correlator
  	generic (
  	    n_dsp_usage   : integer    := 52;
  		C_S00_AXIS_TDATA_WIDTH	: integer	:= 16;
  		C_M00_AXIS_TDATA_WIDTH	: integer	:= 48;
  		C_M00_AXIS_START_COUNT	: integer	:= 32
  	);
  	port (
          GATE_WIDTH	: in std_logic_vector(31 downto 0);
  		s00_axis_aclk	: in std_logic;
  		s00_axis_aresetn	: in std_logic;
  		s00_axis_tready	: out std_logic;
  		s00_axis_tdata	: in std_logic_vector(C_S00_AXIS_TDATA_WIDTH-1 downto 0);
  		s00_axis_tstrb	: in std_logic_vector((C_S00_AXIS_TDATA_WIDTH/8)-1 downto 0);
  		s00_axis_tlast	: in std_logic;
  		s00_axis_tvalid	: in std_logic;
  		m00_axis_aclk	: in std_logic;
  		m00_axis_aresetn	: in std_logic;
  		m00_axis_tvalid	: out std_logic;
  		m00_axis_tdata	: out std_logic_vector(C_M00_AXIS_TDATA_WIDTH-1 downto 0);
  		m00_axis_tstrb	: out std_logic_vector((C_M00_AXIS_TDATA_WIDTH/8)-1 downto 0);
  		m00_axis_tlast	: out std_logic;
  		m00_axis_tready	: in std_logic
  	);
  end component;

  signal GATE_WIDTH: std_logic_vector(31 downto 0);
  signal s00_axis_tready: std_logic;
  signal s00_axis_tdata: std_logic_vector(C_S00_AXIS_TDATA_WIDTH-1 downto 0):= (others => '0');
  signal s00_axis_tstrb: std_logic_vector((C_S00_AXIS_TDATA_WIDTH/8)-1 downto 0):= (others => '0');
  signal s00_axis_tlast: std_logic:= '0';
  signal s00_axis_tvalid: std_logic:= '0';
  signal m00_axis_tvalid: std_logic;
  signal m00_axis_tdata: std_logic_vector(C_M00_AXIS_TDATA_WIDTH-1 downto 0);
  signal m00_axis_tstrb: std_logic_vector((C_M00_AXIS_TDATA_WIDTH/8)-1 downto 0);
  signal m00_axis_tlast: std_logic;
  signal m00_axis_tready: std_logic := '0';

  signal aclk: std_logic := '0';
  signal aresetn: std_logic := '0';
  
  constant aclk_period: time := 10 ns;
  
  signal begin_load: STD_LOGIC := '0'; 
  
  -- STATE MACHINE STATES
  type states_load is (idle, load_input, load_wait, store_result, show_result, load_end); 
  signal state_load : states_load; 
  signal load_counter : unsigned(17 downto 0) := (others => '0'); 
  
  --- result
  type result_array is array ( 0 to 9999 ) of std_logic_vector(47 downto 0);
  signal result_correlation : result_array;
  
  signal result_correlation_counter     : unsigned(31 downto 0) := (others => '0'); 
  signal result_correlation_counter_end : unsigned(31 downto 0) := (others => '0'); 
  signal result_correlation_tvalid: std_logic:= '0';
  signal result_correlation_tdata: std_logic_vector(47 downto 0);

begin

   -- aclk process 
   aclk_process :process
   begin
    aclk <= not aclk;
    wait for aclk_period/2;
    aclk <= not aclk;
    wait for aclk_period/2;
   end process;

  -- Insert values for generic parameters !!
  uut: simple_correlator generic map ( n_dsp_usage            => 52,
                                            C_S00_AXIS_TDATA_WIDTH => 16,
                                            C_M00_AXIS_TDATA_WIDTH => 48,
                                            C_M00_AXIS_START_COUNT => 32)
                                 port map ( GATE_WIDTH             => GATE_WIDTH,
                                            s00_axis_aclk          => aclk,
                                            s00_axis_aresetn       => aresetn,
                                            s00_axis_tready        => s00_axis_tready,
                                            s00_axis_tdata         => s00_axis_tdata,
                                            s00_axis_tstrb         => s00_axis_tstrb,
                                            s00_axis_tlast         => s00_axis_tlast,
                                            s00_axis_tvalid        => s00_axis_tvalid,
                                            m00_axis_aclk          => aclk,
                                            m00_axis_aresetn       => aresetn,
                                            m00_axis_tvalid        => m00_axis_tvalid,
                                            m00_axis_tdata         => m00_axis_tdata,
                                            m00_axis_tstrb         => m00_axis_tstrb,
                                            m00_axis_tlast         => m00_axis_tlast,
                                            m00_axis_tready        => m00_axis_tready );

  stimulus: process
  begin
    wait for 100 ns;    
    aresetn <= '0';  
    GATE_WIDTH <= "00000000000000000000001111101000"; --1000
    GATE_WIDTH <= "00000000000000000010011001001000"; --9800   
    begin_load <= '0';
    wait for aclk_period*20;
    aresetn <= '1';
    wait for aclk_period*20;
    begin_load <= '1';
    wait for aclk_period*1000;
    wait;
  end process;
  
  load_inst: process(aclk, aresetn)      
    type dataFile is file of integer;
    file data_file: dataFile is in "ascan.dat";
    variable data_in: integer;                                               
  begin 
    if (aresetn = '0') then
        s00_axis_tvalid <= '0';
        s00_axis_tlast <= '0';
        s00_axis_tdata <= (others => '0');
        m00_axis_tready <= '0';
        state_load <= idle;  
        load_counter <= (others => '0');
        result_correlation <= (others => (others => '0'));
        result_correlation_counter <= (others => '0');
        result_correlation_counter_end <= (others => '0');
        result_correlation_tvalid <= '0';
        result_correlation_tdata <= (others => '0');
    elsif (aclk'event and aclk = '1') then
        case state_load is  
           when idle =>   
                load_counter <= (others => '0');  
                result_correlation_counter <= (others => '0');
                result_correlation_counter_end <= (others => '0');
                result_correlation_tvalid <= '0';
                result_correlation_tdata <= (others => '0');
                m00_axis_tready <= '1';  
                if(begin_load = '1') then
                    state_load <= load_input;  
                    load_counter <= load_counter + 1;
                end if;
            when load_input =>
                if(s00_axis_tready = '1') then
                    load_counter <= load_counter + 1;
                    s00_axis_tvalid <= '1';
                    s00_axis_tdata <= std_logic_vector(resize(load_counter, s00_axis_tdata'length));
                    if not endfile(data_file) then
                        read (data_file, data_in);
                        s00_axis_tdata<=std_logic_vector(to_signed(data_in, s00_axis_tdata'length));
                    end if;
                    if not endfile(data_file) then
                        read (data_file, data_in);
                    end if;
                    if(load_counter >= (unsigned(GATE_WIDTH))) then
                        s00_axis_tlast <= '1';
                        state_load <= store_result; 
                    end if;
                end if;   
            when store_result =>   
                s00_axis_tvalid <= '0';
                s00_axis_tlast <= '0';
                if(m00_axis_tvalid = '1') then
                    result_correlation_counter <= result_correlation_counter + 1;
                    result_correlation(to_integer(result_correlation_counter)) <= m00_axis_tdata;
                    if(m00_axis_tlast = '1') then
                        result_correlation_counter_end <= result_correlation_counter;
                        result_correlation_counter <= (others => '0');
                        state_load <= show_result; 
                    end if;
                end if;
            when show_result => 
                if(result_correlation_counter <= result_correlation_counter_end) then
                    result_correlation_tvalid <= '1';
                    result_correlation_tdata <= result_correlation(to_integer(result_correlation_counter));
                    result_correlation_counter <= result_correlation_counter + 1;
                else
                    result_correlation_tvalid <= '0';  
                    state_load <= load_end;
                end if;  
            when load_end =>
                if(begin_load = '0') then
                    state_load <= idle;  
                    load_counter <= (others => '0');
                end if;
            when others =>
                null;                   
        end case;                                                                                            
    end if;
  end process;

end;
