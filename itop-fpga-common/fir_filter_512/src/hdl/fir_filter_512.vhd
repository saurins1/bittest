library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity fir_filter_512 is
	generic (
        REGISTER_WIDTH              : integer := 32;
        COEFF_WIDTH                 : integer := 24;
        N_COEFFICIENTS              : integer := 512;
        COUNTER_WAIT                : integer := 16;
        N                           : integer := 9; --2^9 = 512
        C_S00_AXIS_TDATA_WIDTH      : integer  := 16;
        C_M00_AXIS_TDATA_WIDTH      : integer := 16
	);
	port (
		clk_100                    : in STD_LOGIC;
        clk_200                    : in STD_LOGIC;
        aresetn                    : in STD_LOGIC;
	    FIR_FILTER_EN              : in STD_LOGIC;
	    AUTOMATIC_FREQ_FILTER_EN   : in STD_LOGIC; 
        DATA_WINDOW                : in STD_LOGIC_VECTOR(REGISTER_WIDTH-1 downto 0);
        -- COEFFICIENTS       
        LOAD_CONFIG_FIR            : in std_logic;
        LOAD_CONFIG_FIR_TYPE       : in std_logic;
        CONFIG_INDEX_FIR           : in std_logic_vector(REGISTER_WIDTH-1 downto 0);   
        CONFIG_VALUE_FIR           : in std_logic_vector(REGISTER_WIDTH-1 downto 0); 
        LOADING_COEFFICIENTS       : out STD_LOGIC;
         
		s00_axis_tready	: out std_logic;
		s00_axis_tvalid	: in std_logic;
		s00_axis_tdata	: in std_logic_vector(C_S00_AXIS_TDATA_WIDTH-1 downto 0);
		s00_axis_tlast	: in std_logic;
		
        m00_axis_tready	: in std_logic;
		m00_axis_tvalid	: out std_logic;
		m00_axis_tdata	: out std_logic_vector(C_M00_AXIS_TDATA_WIDTH-1 downto 0);
		m00_axis_tlast	: out std_logic
		
	);
end fir_filter_512;

architecture arch_imp of fir_filter_512 is
      
    signal m00_axis_counter    : UNSIGNED ( 31 downto 0 );
    signal s00_axis_counter    : UNSIGNED ( 31 downto 0 );
    
    signal m00_axis_tvalid_i1  : STD_LOGIC;
    signal m00_axis_tvalid_i2  : STD_LOGIC;
    signal m00_axis_tvalid_i3  : STD_LOGIC;
    signal m00_axis_tlast_i1   : STD_LOGIC;
    signal m00_axis_tlast_i2   : STD_LOGIC;
    signal m00_axis_tlast_i3   : STD_LOGIC;
    
    signal s00_axis_tvalid_i1  : STD_LOGIC;
    signal s00_axis_tvalid_i2  : STD_LOGIC;
    signal s00_axis_tlast_i1   : STD_LOGIC;
    signal s00_axis_tlast_i2   : STD_LOGIC;
    signal s00_axis_tdata_i1   : std_logic_vector(C_S00_AXIS_TDATA_WIDTH-1 downto 0);

    -- Coefficients
    type coefficient_matrix is array(0 to (N_COEFFICIENTS-1)) of std_logic_vector(COEFF_WIDTH-1 downto 0);
    
    signal coefficients_fir_filter: coefficient_matrix;
    signal coefficients_automatic_freq_filter: coefficient_matrix;
    
    signal FIR_FILTER_L_DIV           : STD_LOGIC_VECTOR(REGISTER_WIDTH-1 downto 0); 
    signal AUTOMATIC_FREQ_FILTER_L_DIV: STD_LOGIC_VECTOR(REGISTER_WIDTH-1 downto 0); 
    signal N_COEFFICIENTS_fir_filter  : STD_LOGIC_VECTOR(REGISTER_WIDTH-1 downto 0); 
    signal N_COEFFICIENTS_automatic_freq_filter  : STD_LOGIC_VECTOR(REGISTER_WIDTH-1 downto 0); 
          
    signal BEGIN_LOAD_COEFFICIENTS  : STD_LOGIC;  
    signal END_LOAD_COEFFICIENTS    : STD_LOGIC; 
    signal LOAD_COEFFICIENTS_TYPE_i : STD_LOGIC; 
  
    signal FIR_FILTER_L_DIV_int     : INTEGER range 0 to N_COEFFICIENTS;
    signal FIR_FILTER_L_DIV_i       : UNSIGNED(REGISTER_WIDTH-1 downto 0);
    
    signal AUTOMATIC_FREQ_FILTER_L_DIV_int     : INTEGER range 0 to N_COEFFICIENTS;
    signal AUTOMATIC_FREQ_FILTER_L_DIV_i       : UNSIGNED(REGISTER_WIDTH-1 downto 0);
        
    -- Reload
    type states_reload is (idle, reset_fir, reload_coeff, reload_wait, config, config_end); 
    signal state_reload : states_reload; 
         
    -- Filter input
    type states_filter_input is (idle, config, input, 
        config_fir_filter, config_automatic_freq_filter, 
        input_fir_filter, input_fir_filter_wait, input_fir_filter_wait_2, input_automatic_freq_filter,  input_automatic_freq_filter_wait, read_result, read_result_wait,
        filter_input_end, input_automatic_freq_filter_end); 
        
    signal state_filter_input : states_filter_input; 
    
    signal read_result_counter    : UNSIGNED ( 15 downto 0 );
             
    -- FIR 
    COMPONENT fir_filter_512_FIR
      PORT (
        aresetn : IN STD_LOGIC;
        aclk : IN STD_LOGIC;
        aclken : IN STD_LOGIC;
        s_axis_data_tvalid : IN STD_LOGIC;
        s_axis_data_tready : OUT STD_LOGIC;
        s_axis_data_tlast : IN STD_LOGIC;
        s_axis_data_tdata : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        s_axis_config_tvalid : IN STD_LOGIC;
        s_axis_config_tready : OUT STD_LOGIC;
        s_axis_config_tdata : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        s_axis_reload_tvalid : IN STD_LOGIC;
        s_axis_reload_tready : OUT STD_LOGIC;
        s_axis_reload_tlast : IN STD_LOGIC;
        s_axis_reload_tdata : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
        m_axis_data_tvalid : OUT STD_LOGIC;
        m_axis_data_tready : IN STD_LOGIC;
        m_axis_data_tlast : OUT STD_LOGIC;
        m_axis_data_tdata : OUT STD_LOGIC_VECTOR(47 DOWNTO 0);
        event_s_reload_tlast_missing : OUT STD_LOGIC;
        event_s_reload_tlast_unexpected : OUT STD_LOGIC
      );
    END COMPONENT;

    -- output FIR
    signal FIR_M_AXIS_DATA_tdata    : STD_LOGIC_VECTOR ( 47 downto 0 );
    signal FIR_M_AXIS_DATA_tlast    : STD_LOGIC;
    signal FIR_M_AXIS_DATA_tready   : STD_LOGIC;
    signal FIR_M_AXIS_DATA_tvalid   : STD_LOGIC;
    
    signal FIR_M_AXIS_DATA_tdata_i1    : STD_LOGIC_VECTOR ( 47 downto 0 );
    signal FIR_M_AXIS_DATA_tlast_i1    : STD_LOGIC;
    signal FIR_M_AXIS_DATA_tvalid_i1   : STD_LOGIC;
    signal FIR_M_AXIS_DATA_tvalid_i2   : STD_LOGIC;
    signal FIR_M_AXIS_DATA_tvalid_i3   : STD_LOGIC;
    
    -- config FIR
    signal FIR_S_AXIS_CONFIG_tdata  : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal FIR_S_AXIS_CONFIG_tready : STD_LOGIC;
    signal FIR_S_AXIS_CONFIG_tvalid : STD_LOGIC;
    
    -- input FIR
    signal FIR_S_AXIS_DATA_tdata    : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal FIR_S_AXIS_DATA_tlast    : STD_LOGIC;
    signal FIR_S_AXIS_DATA_tready   : STD_LOGIC;
    signal FIR_S_AXIS_DATA_tvalid   : STD_LOGIC;
    
    signal FIR_S_AXIS_DATA_tlast_i1 : STD_LOGIC;
    signal FIR_S_AXIS_DATA_tvalid_i1: STD_LOGIC;
    signal FIR_S_AXIS_DATA_tlast_i2 : STD_LOGIC;
    signal FIR_S_AXIS_DATA_tlast_i3 : STD_LOGIC;
    signal FIR_S_AXIS_DATA_tvalid_i2: STD_LOGIC;
    signal FIR_S_AXIS_DATA_tvalid_i3: STD_LOGIC;
      
    -- reload
    signal FIR_S_AXIS_RELOAD_tdata  : STD_LOGIC_VECTOR ( COEFF_WIDTH-1 downto 0 );
    signal FIR_S_AXIS_RELOAD_tlast  : STD_LOGIC;
    signal FIR_S_AXIS_RELOAD_tready : STD_LOGIC;
    signal FIR_S_AXIS_RELOAD_tvalid : STD_LOGIC;
    
    signal fir_aclken : STD_LOGIC;
    signal fir_event_s_reload_tlast_missing : STD_LOGIC;
    signal fir_event_s_reload_tlast_unexpected : STD_LOGIC;
    
    COMPONENT fir_filter_512_BRAM
      PORT (
        clka : IN STD_LOGIC;
        ena : IN STD_LOGIC;
        wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addra : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        dina : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
        douta : OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
        clkb : IN STD_LOGIC;
        enb : IN STD_LOGIC;
        web : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addrb : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        dinb : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
        doutb : OUT STD_LOGIC_VECTOR(13 DOWNTO 0)
      );
    END COMPONENT;
    
    signal BRAM_PORTA_addr : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal BRAM_PORTA_addr_counter : UNSIGNED ( 31 downto 0 );
    signal BRAM_PORTA_din : STD_LOGIC_VECTOR ( 13 downto 0 );
    signal BRAM_PORTA_dout : STD_LOGIC_VECTOR ( 13 downto 0 );
    signal BRAM_PORTA_en : STD_LOGIC;
    signal BRAM_PORTA_we : STD_LOGIC_VECTOR ( 0 to 0 );
    
    signal BRAM_PORTB_addr : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal BRAM_PORTB_addr_counter : UNSIGNED ( 31 downto 0 );
    signal BRAM_PORTB_din : STD_LOGIC_VECTOR ( 13 downto 0 );
    signal BRAM_PORTB_dout : STD_LOGIC_VECTOR ( 13 downto 0 );
    signal BRAM_PORTB_en : STD_LOGIC;
    signal BRAM_PORTB_we : STD_LOGIC_VECTOR ( 0 to 0 );
    
    signal first_bram_PORTA : STD_LOGIC; 
    signal first_bram_PORTB : STD_LOGIC; 
    signal read_bram_PORTA : STD_LOGIC;
    signal read_bram_PORTB : STD_LOGIC; 
    signal read_bram_PORTA_end : STD_LOGIC; 
    signal read_bram_PORTB_end : STD_LOGIC; 
    signal clk50_counter : UNSIGNED ( 7 downto 0 );
    signal read_bram_add_padding_PORTA : STD_LOGIC;
    signal bram_cut_padding_PORTB_counter : UNSIGNED ( 15 downto 0 );
    
    type states_BRAM is (idle, write_bram, write_bram_wait, write_bram_wait_end, read_bram, read_bram_wait, read_bram_end); 
    signal state_BRAM_PORTA : states_BRAM;
    signal state_BRAM_PORTB : states_BRAM;
    
    signal FIR_FILTER_flag : STD_LOGIC; 
    
    signal fir_output_counter   : UNSIGNED ( 31 downto 0 );
    signal fir_input_counter    : UNSIGNED ( 31 downto 0 );
    
    signal fir_aresetn : STD_LOGIC; 
    signal fir_aresetn_i : STD_LOGIC;
    
                  
begin

    -- internal fir reset
    fir_aresetn <= aresetn and fir_aresetn_i;

    -- FIR   
    fir_filter_512_FIR_inst : fir_filter_512_FIR
      PORT MAP (
        aresetn => fir_aresetn,
        aclk => clk_200,
        aclken => fir_aclken,
        s_axis_data_tvalid => FIR_S_AXIS_DATA_tvalid,
        s_axis_data_tready => FIR_S_AXIS_DATA_tready,
        s_axis_data_tlast => FIR_S_AXIS_DATA_tlast,
        s_axis_data_tdata => FIR_S_AXIS_DATA_tdata,
        s_axis_config_tvalid => FIR_S_AXIS_CONFIG_tvalid,
        s_axis_config_tready => FIR_S_AXIS_CONFIG_tready,
        s_axis_config_tdata => FIR_S_AXIS_CONFIG_tdata,
        s_axis_reload_tvalid => FIR_S_AXIS_RELOAD_tvalid,
        s_axis_reload_tready => FIR_S_AXIS_RELOAD_tready,
        s_axis_reload_tlast => FIR_S_AXIS_RELOAD_tlast,
        s_axis_reload_tdata => FIR_S_AXIS_RELOAD_tdata,
        m_axis_data_tvalid => FIR_M_AXIS_DATA_tvalid,
        m_axis_data_tready => FIR_M_AXIS_DATA_tready,
        m_axis_data_tlast => FIR_M_AXIS_DATA_tlast,
        m_axis_data_tdata => FIR_M_AXIS_DATA_tdata,
        event_s_reload_tlast_missing => fir_event_s_reload_tlast_missing,
        event_s_reload_tlast_unexpected => fir_event_s_reload_tlast_unexpected
      );
   
    FIR_M_AXIS_DATA_tready <= m00_axis_tready;

	-- Load coefficients into the local matrix
    reload_inst: process(clk_100, aresetn)
    begin 
        if (aresetn = '0') then
            -- Coefficient matrix
            coefficients_fir_filter <= ((others=> (others=>'0')));  
            coefficients_automatic_freq_filter <= ((others=> (others=>'0')));  
            N_COEFFICIENTS_fir_filter <= (others=>'0');     
            N_COEFFICIENTS_automatic_freq_filter <= (others=>'0');                
        elsif (clk_100'event and clk_100 = '1') then
            if(LOAD_CONFIG_FIR = '1') then
                if(LOAD_CONFIG_FIR_TYPE = '0') then
                    if(unsigned(CONFIG_INDEX_FIR) = 0) then
                        N_COEFFICIENTS_fir_filter <= CONFIG_VALUE_FIR;
                    elsif(unsigned(CONFIG_INDEX_FIR) = 1) then
                        FIR_FILTER_L_DIV <= CONFIG_VALUE_FIR;
                        if(unsigned(N_COEFFICIENTS_fir_filter) = 0) then
                            coefficients_fir_filter <= ((others=> (others=>'0')));  
                        end if;
                    else
                        if(unsigned(N_COEFFICIENTS_fir_filter) /= 0) then
                            coefficients_fir_filter(to_integer(unsigned(CONFIG_INDEX_FIR)-2)) <= CONFIG_VALUE_FIR(23 downto 0);                        
                        end if;
                    end if;
                else
                    if(unsigned(CONFIG_INDEX_FIR) = 0) then
                        N_COEFFICIENTS_automatic_freq_filter <= CONFIG_VALUE_FIR;
                    elsif(unsigned(CONFIG_INDEX_FIR) = 1) then
                        AUTOMATIC_FREQ_FILTER_L_DIV <= CONFIG_VALUE_FIR;
                        if(unsigned(N_COEFFICIENTS_automatic_freq_filter) = 0) then
                            coefficients_automatic_freq_filter <= ((others=> (others=>'0')));  
                        end if;
                    else
                        if(unsigned(N_COEFFICIENTS_automatic_freq_filter) /= 0) then
                            coefficients_automatic_freq_filter(to_integer(unsigned(CONFIG_INDEX_FIR)-2)) <= CONFIG_VALUE_FIR(23 downto 0);                       
                        end if;
                    end if;
                end if; 
            end if;                                                                                         
        end if;
    end process;
        
	-- Reload coefficients into FIR and config
    reload_coefficients_inst: process(clk_200, aresetn)
        variable reload_counter : integer range 0 to 2*N_COEFFICIENTS;
        variable reload_counter_wait : integer range 0 to 2*N_COEFFICIENTS;
        variable reset_fir_counter : integer range 0 to 2*N_COEFFICIENTS;
    begin 
        if (aresetn = '0') then
            END_LOAD_COEFFICIENTS <= '0';           
            -- FIR Reload
            reload_counter := 0;
            reload_counter_wait := 0;
            FIR_S_AXIS_RELOAD_tvalid <= '0';
            FIR_S_AXIS_RELOAD_tlast <= '0';
            FIR_S_AXIS_RELOAD_tdata <= (others => '0');
            fir_aresetn_i <= '1';
            -- CONFIG           
            FIR_S_AXIS_CONFIG_tvalid <= '0';
            FIR_S_AXIS_CONFIG_tdata <= (others => '0');
            -- OUTPUT
            LOADING_COEFFICIENTS <= '0';
            -- state
            state_reload <= idle;            
        elsif (clk_200'event and clk_200 = '1') then
            case state_reload is        
                when idle =>
                    if(BEGIN_LOAD_COEFFICIENTS = '1') then
                        LOADING_COEFFICIENTS <= '1';
                        reset_fir_counter := 0;
                        state_reload <= reset_fir;  
                    end if; 
                when reset_fir =>
                    if(reset_fir_counter >= (COUNTER_WAIT)) then
                        fir_aresetn_i <= '1';
                        state_reload <= reload_coeff;  
                    else
                        fir_aresetn_i <= '0';
                        reset_fir_counter := reset_fir_counter + 1;
                    end if;                                
                when reload_coeff =>                   
                    if(FIR_S_AXIS_RELOAD_tready = '1') then                 
                        FIR_S_AXIS_RELOAD_tvalid <= '1';
                        FIR_S_AXIS_RELOAD_tlast <= '0';
                        if(LOAD_COEFFICIENTS_TYPE_i = '0') then
                            FIR_S_AXIS_RELOAD_tdata<=coefficients_fir_filter(reload_counter);  
                        else
                            FIR_S_AXIS_RELOAD_tdata<=coefficients_automatic_freq_filter(reload_counter);  
                        end if;                         
                        if(reload_counter >= (N_COEFFICIENTS-1)) then
                            FIR_S_AXIS_RELOAD_tlast <= '1';  
                            reload_counter := 0; 
                            state_reload <= reload_wait; 
                        else
                             reload_counter := reload_counter + 1; 
                        end if;                                        
                    else
                        FIR_S_AXIS_RELOAD_tvalid <= '0';  
                    end if;                 
                when reload_wait =>
                    FIR_S_AXIS_RELOAD_tvalid <= '0';
                    FIR_S_AXIS_RELOAD_tlast <= '0';                 
                    if(reload_counter_wait >= COUNTER_WAIT) then
                        reload_counter_wait := 0;
                        state_reload <= config; 
                    else
                        reload_counter_wait := reload_counter_wait + 1;
                    end if;
                when config =>             
                    if(FIR_S_AXIS_CONFIG_tready = '1') then
                        FIR_S_AXIS_CONFIG_tvalid <= '1';
                        FIR_S_AXIS_CONFIG_tdata <= (others => '0');
                        state_reload <= config_end; 
                    else
                        FIR_s_axis_config_tvalid <= '0';  
                    end if; 
                when config_end => 
                    FIR_S_AXIS_CONFIG_tvalid <= '0';           
                    if(reload_counter_wait >= COUNTER_WAIT) then
                        END_LOAD_COEFFICIENTS <= '1';
                        if(BEGIN_LOAD_COEFFICIENTS = '0') then
                            END_LOAD_COEFFICIENTS <= '0';
                            LOADING_COEFFICIENTS <= '0';
                            reload_counter_wait := 0;
                            state_reload <= idle; 
                        end if;
                    else
                        reload_counter_wait := reload_counter_wait + 1;
                    end if;
                when others =>
                    null;                   
            end case;                                                                                            
        end if;
    end process;
        
    BRAM_PORTA_addr <= std_logic_vector(BRAM_PORTA_addr_counter(15 downto 0));
    BRAM_PORTB_addr <= std_logic_vector(BRAM_PORTB_addr_counter(15 downto 0));
    
    FIR_FILTER_L_DIV_i <= UNSIGNED(FIR_FILTER_L_DIV);      
    FIR_FILTER_L_DIV_int <= TO_INTEGER(FIR_FILTER_L_DIV_i);  
    
    AUTOMATIC_FREQ_FILTER_L_DIV_i <= UNSIGNED(AUTOMATIC_FREQ_FILTER_L_DIV);      
    AUTOMATIC_FREQ_FILTER_L_DIV_int <= TO_INTEGER(AUTOMATIC_FREQ_FILTER_L_DIV_i); 
    
    -- PortB BRAM is for write output of FIR and read final result
    -- cut padding '0' of N_COEFICIENTS/2 in the beggining of output FIR signal
    control_BRAM_PORTB: process(clk_200, aresetn)
    begin 
        if (aresetn = '0') then
            -- BRAM
            BRAM_PORTB_en <= '1';
            BRAM_PORTB_we <= (others => '0'); 
            BRAM_PORTB_din <= (others => '0');
            BRAM_PORTB_addr_counter <= (others => '0'); 
            read_bram_PORTB_end <= '0';
            bram_cut_padding_PORTB_counter <= (others => '0'); 
            -- M AXIS
            m00_axis_tvalid_i1 <= '0';
            m00_axis_tlast_i1 <= '0'; 
            -- state
            state_BRAM_PORTB <= idle;
        elsif (clk_200'event and clk_200 = '1') then
            case state_BRAM_PORTB is 
                when idle =>
                    BRAM_PORTB_we <= (others => '0'); 
                    BRAM_PORTB_addr_counter <= (others => '0'); 
                    bram_cut_padding_PORTB_counter <= (others => '0');
                    first_bram_PORTB <= '1'; 
                    if(FIR_M_AXIS_DATA_tvalid = '1') then
                        BRAM_PORTB_we <= (others => '1');
                        state_BRAM_PORTB <= write_bram;
                    elsif(read_bram_PORTB = '1') then
                        state_BRAM_PORTB <= read_bram;
                    end if;
                when write_bram =>
                    if(FIR_M_AXIS_DATA_tvalid_i1 = '1') then
                        if(bram_cut_padding_PORTB_counter >= (N_COEFFICIENTS/2)) then
                            if(first_bram_PORTB = '1') then
                                first_bram_PORTB <= '0';
                            else
                                BRAM_PORTB_addr_counter <= BRAM_PORTB_addr_counter + 1;
                            end if;
                            if(FIR_FILTER_flag = '1') then
                                BRAM_PORTB_din <= FIR_M_AXIS_DATA_tdata_i1((C_M00_AXIS_TDATA_WIDTH + FIR_FILTER_L_DIV_int -3) downto FIR_FILTER_L_DIV_int);
                            else
                                BRAM_PORTB_din <= FIR_M_AXIS_DATA_tdata_i1((C_M00_AXIS_TDATA_WIDTH + AUTOMATIC_FREQ_FILTER_L_DIV_int -3) downto AUTOMATIC_FREQ_FILTER_L_DIV_int);
                            end if; 
                            if(FIR_M_AXIS_DATA_tlast_i1 = '1' and BRAM_PORTB_addr_counter >= (unsigned(DATA_WINDOW)-2)) then
                                state_BRAM_PORTB <= write_bram_wait;
                            end if;  
                        else
                            bram_cut_padding_PORTB_counter <= bram_cut_padding_PORTB_counter + 1;
                        end if;           
                    end if;
                when write_bram_wait =>
                    if(FIR_M_AXIS_DATA_tvalid_i3 = '0' and FIR_M_AXIS_DATA_tvalid_i2 = '0') then
                        state_BRAM_PORTB <= idle;
                    end if;      
                when read_bram =>
                    if(m00_axis_tready = '1' and clk_100 = '1') then
                        if(first_bram_PORTB = '1') then
                            first_bram_PORTB <= '0';
                        else       
                            if(BRAM_PORTB_addr_counter <= (unsigned(DATA_WINDOW)-1)) then             
                                BRAM_PORTB_addr_counter <= BRAM_PORTB_addr_counter + 1;
                            end if;
                        end if; 
                        m00_axis_tvalid_i1 <= '1';
                        if(BRAM_PORTB_addr_counter >= (unsigned(DATA_WINDOW)-1)) then
                            m00_axis_tlast_i1 <= '1';
                        end if;
                        if(BRAM_PORTB_addr_counter >= (unsigned(DATA_WINDOW))) then
                            m00_axis_tlast_i1 <= '0';
                            m00_axis_tvalid_i1 <= '0';
                            state_BRAM_PORTB <= read_bram_end;
                        end if;
                    end if;
                when read_bram_end =>
                    read_bram_PORTB_end <= '1';
                    if(read_bram_PORTB = '0') then
                        read_bram_PORTB_end <= '0';
                        state_BRAM_PORTB <= idle;
                    end if;
                when others =>
                    null;
            end case;
        end if;
    end process;
    
    -- PortA BRAM is for write input signal and read FIR result
    -- padding '0' of N_COEFICIENTS/2 in the end of input FIR signal
    control_BRAM_PORTA: process(clk_200, aresetn)
    begin 
        if (aresetn = '0') then
            -- BRAM
            BRAM_PORTA_en <= '1';
            BRAM_PORTA_we <= (others => '0'); 
            BRAM_PORTA_din <= (others => '0');
            BRAM_PORTA_addr_counter <= (others => '0');
            read_bram_PORTA_end <= '0';  
            read_bram_add_padding_PORTA <= '0';  
            -- FIR
            FIR_S_AXIS_DATA_tvalid_i3 <= '0';
            FIR_S_AXIS_DATA_tlast_i3 <= '0';
            -- state
            state_BRAM_PORTA <= idle;
        elsif (clk_200'event and clk_200 = '1') then
            case state_BRAM_PORTA is 
                when idle =>
                    BRAM_PORTA_we <= (others => '0'); 
                    BRAM_PORTA_addr_counter <= (others => '0'); 
                    first_bram_PORTA <= '1'; 
                    read_bram_add_padding_PORTA <= '0';
                    if(s00_axis_tvalid = '1') then
                        BRAM_PORTA_we <= (others => '1');
                        state_BRAM_PORTA <= write_bram;
                    elsif(read_bram_PORTA = '1') then
                        state_BRAM_PORTA <= read_bram;
                    end if;
                when write_bram =>
                    if(s00_axis_tvalid_i1 = '1') then
                        if(first_bram_PORTA = '1') then
                            first_bram_PORTA <= '0';
                        else
                            BRAM_PORTA_addr_counter <= BRAM_PORTA_addr_counter + 1;
                        end if; 
                        BRAM_PORTA_din <= s00_axis_tdata_i1(13 downto 0); 
                        state_BRAM_PORTA <= write_bram_wait;             
                    end if;
                when write_bram_wait =>
                    if(s00_axis_tlast_i2 = '1' and BRAM_PORTA_addr_counter >= (unsigned(DATA_WINDOW)-2)) then
                        state_BRAM_PORTA <= write_bram_wait_end;
                    else
                        state_BRAM_PORTA <= write_bram;
                    end if;
                when write_bram_wait_end =>
                    if(s00_axis_tlast_i2 = '1') then
                        state_BRAM_PORTA <= idle;
                    end if;   
                when read_bram =>
                    if(FIR_S_AXIS_DATA_tready = '1') then
                        if(first_bram_PORTA = '1') then
                            first_bram_PORTA <= '0';
                        else
                            BRAM_PORTA_addr_counter <= BRAM_PORTA_addr_counter + 1;
                        end if; 
                        FIR_S_AXIS_DATA_tvalid_i3 <= '1';
                        if(BRAM_PORTA_addr_counter >= (unsigned(DATA_WINDOW))) then
                            read_bram_add_padding_PORTA <= '1';
                        end if;
                        if(BRAM_PORTA_addr_counter >= (unsigned(DATA_WINDOW)+(N_COEFFICIENTS/2)-2)) then
                            FIR_S_AXIS_DATA_tlast_i3 <= '1';
                        end if;
                        clk50_counter <= (others => '0');
                        state_BRAM_PORTA <= read_bram_wait;
                    end if;
                when read_bram_wait =>
                    FIR_S_AXIS_DATA_tvalid_i3 <= '0';
                    FIR_S_AXIS_DATA_tlast_i3 <= '0';
                    clk50_counter <= clk50_counter + 1;
                    if(clk50_counter >= 2) then
                        if(BRAM_PORTA_addr_counter >= (unsigned(DATA_WINDOW)+(N_COEFFICIENTS/2)-1)) then
                            state_BRAM_PORTA <= read_bram_end;
                        else
                            state_BRAM_PORTA <= read_bram;
                        end if;
                    end if;
                when read_bram_end =>
                    read_bram_PORTA_end <= '1';
                    if(read_bram_PORTA = '0') then
                        read_bram_PORTA_end <= '0';
                        state_BRAM_PORTA <= idle;
                    end if;
                when others =>
                    null;
            end case;
        end if;
    end process;
    
    -- FIR input
    FIR_in_inst: process(clk_200, aresetn)
    begin 
        if (aresetn = '0') then  
            FIR_S_AXIS_DATA_tvalid <= '0';
            FIR_S_AXIS_DATA_tlast <= '0';
            FIR_S_AXIS_DATA_tdata <= (others => '0'); 
            --read_bram_add_padding_PORTA_i1 <= '0';
        elsif (clk_200'event and clk_200 = '1') then
            FIR_S_AXIS_DATA_tvalid <= FIR_S_AXIS_DATA_tvalid_i1;
            FIR_S_AXIS_DATA_tlast <= FIR_S_AXIS_DATA_tlast_i1;
            --read_bram_add_padding_PORTA_i1 <= read_bram_add_padding_PORTA;
            --if(read_bram_add_padding_PORTA_i1 = '1') then
            if(read_bram_add_padding_PORTA = '1') then
                FIR_S_AXIS_DATA_tdata <= (others => '0'); 
            else
                FIR_S_AXIS_DATA_tdata <= BRAM_PORTA_dout(13) & BRAM_PORTA_dout(13) & BRAM_PORTA_dout; 
            end if;
        end if;
    end process; 
     
    --MAIn state machine
    filter_input_inst: process(clk_200, aresetn) 
        variable counter_wait : integer range 0 to 2*N_COEFFICIENTS;
    begin 
        if (aresetn = '0') then
            -- flags
            LOAD_COEFFICIENTS_TYPE_i <= '0';
            BEGIN_LOAD_COEFFICIENTS <= '0';
            -- FIR
            fir_aclken <= '1'; -- fir always enabled
            s00_axis_tready <= '0';
            FIR_FILTER_flag <= '0';
            read_bram_PORTA <= '0';
            read_bram_PORTB <= '0';
            counter_wait := 0;
            -- State
            state_filter_input <= idle;  
        elsif (clk_200'event and clk_200 = '1') then
            case state_filter_input is        
                when idle => 
                    s00_axis_tready <= '1';
                    if(s00_axis_tvalid = '1') then
                        -- Depending Filter enables
                        if(FIR_FILTER_EN = '1') then
                            LOAD_COEFFICIENTS_TYPE_i <= '0';
                            state_filter_input<= config_fir_filter; 
                        elsif(AUTOMATIC_FREQ_FILTER_EN = '1') then  
                            LOAD_COEFFICIENTS_TYPE_i <= '1';
                            state_filter_input<= config_automatic_freq_filter;
                        else
                            read_result_counter <= (others => '0');
                            state_filter_input<= read_result_wait; 
                        end if;  
                    end if;    
                when config_fir_filter =>
                    FIR_FILTER_flag <= '1';
                    -- config FIR with bandpass coefficients
                    if(END_LOAD_COEFFICIENTS = '1') then
                        BEGIN_LOAD_COEFFICIENTS <= '0';
                        state_filter_input<= input_fir_filter; 
                    else
                        BEGIN_LOAD_COEFFICIENTS <= '1';
                    end if;           
                when input_fir_filter =>
                    -- read signal from BRAM 
                    if(read_bram_PORTA_end = '1') then
                        --read_bram_PORTA <= '0';
                        s00_axis_tready <= '0';
                        state_filter_input <= input_fir_filter_wait;
                    else
                        if(s00_axis_tvalid_i2 = '0') then
                            read_bram_PORTA <= '1';
                        end if;
                    end if;
                when input_fir_filter_wait =>   
                    -- wait for FIR M tlast   
                    if(FIR_M_AXIS_DATA_tlast = '1' and fir_output_counter >= (unsigned(DATA_WINDOW)-2)) then
                        read_bram_PORTA <= '0';
                        if(AUTOMATIC_FREQ_FILTER_EN = '1') then
                            counter_wait := 0;
                            LOAD_COEFFICIENTS_TYPE_i <= '1';
                            state_filter_input<= config_automatic_freq_filter;
                        else
                            state_filter_input <= read_result;
                        end if;
                    end if;
                when config_automatic_freq_filter =>
                    FIR_FILTER_flag <= '0';
                    -- config FIR with automatic freq coefficients
                    if(END_LOAD_COEFFICIENTS = '1') then
                        BEGIN_LOAD_COEFFICIENTS <= '0';
                        state_filter_input <= input_automatic_freq_filter; 
                    else
                        BEGIN_LOAD_COEFFICIENTS <= '1';
                    end if;
                when input_automatic_freq_filter => 
                    -- read signal from BRAM 
                    if(read_bram_PORTA_end = '1') then
                        --read_bram_PORTA <= '0';
                        s00_axis_tready <= '0';
                        state_filter_input <= input_automatic_freq_filter_wait;
                    else
                        read_bram_PORTA <= '1';
                    end if;
                when input_automatic_freq_filter_wait =>
                    -- wait for FIR M tlast 
                    if(FIR_M_AXIS_DATA_tlast = '1' and fir_output_counter >= (unsigned(DATA_WINDOW)-2)) then
                        read_bram_PORTA <= '0';
                        state_filter_input <= read_result;
                    end if;
                when read_result_wait => 
                    if(read_result_counter >= 31) then
                        state_filter_input <= read_result;
                    else
                        read_result_counter <= read_result_counter + 1;
                    end if;
                when read_result =>
                    -- read final result - final output
                    if(read_bram_PORTB_end = '1') then
                        read_bram_PORTB <= '0';
                        state_filter_input <= idle;
                    else
                        read_bram_PORTB <= '1';
                    end if;               
                when others =>
                      null;                   
            end case;                                                                                            
        end if;
    end process;
    
    -- BRAM   
      fir_filter_512_BRAM_inst : fir_filter_512_BRAM
        PORT MAP (
          clka => clk_200,
          ena => BRAM_PORTA_en,
          wea => BRAM_PORTA_we,
          addra => BRAM_PORTA_addr,
          dina => BRAM_PORTA_din,
          douta => BRAM_PORTA_dout,
          clkb => clk_200,
          enb => BRAM_PORTB_en,
          web => BRAM_PORTB_we,
          addrb => BRAM_PORTB_addr,
          dinb => BRAM_PORTB_din,
          doutb => BRAM_PORTB_dout
        );
        
    -- final_output
    final_output: process(clk_100, aresetn)
    begin 
        if (aresetn = '0') then
            -- M AXIS
            --m00_axis_tvalid_i2 <= '0';
            --m00_axis_tvalid_i1 <= '0';
            --m00_axis_tvalid <= '0';
            --m00_axis_tlast_i2 <= '0';
            --m00_axis_tlast_i1 <= '0';
            m00_axis_tvalid_i3 <= '0'; 
            m00_axis_tvalid_i2 <= '0'; 
            m00_axis_tlast <= '0';
            m00_axis_tdata <= (others => '0');
        elsif (clk_100'event and clk_100 = '1') then
            -- M AXIS  
            --m00_axis_tvalid_i2 <= m00_axis_tvalid_i3; 
            --m00_axis_tvalid_i1 <= m00_axis_tvalid_i2;
            m00_axis_tvalid_i3 <= m00_axis_tvalid_i1;
            m00_axis_tvalid_i2 <= m00_axis_tvalid_i3;
            --m00_axis_tlast_i2 <= m00_axis_tlast_i3;
            --m00_axis_tlast_i1 <= m00_axis_tlast_i2;
            m00_axis_tlast <= m00_axis_tlast_i1;
            if(m00_axis_tvalid_i1 = '1') then
                m00_axis_tdata <= BRAM_PORTB_dout(13) & BRAM_PORTB_dout(13) & BRAM_PORTB_dout;
            else
                m00_axis_tdata <= (others => '0');
            end if;
        end if;
    end process;
    
    m00_axis_tvalid <= m00_axis_tvalid_i3 and m00_axis_tvalid_i2;
                          
    -- Aux delayed signals
    delay_inst: process(clk_200, aresetn)
    begin 
        if (aresetn = '0') then   
            -- FIR S
            FIR_S_AXIS_DATA_tvalid_i2 <= '0';
            FIR_S_AXIS_DATA_tvalid_i1 <= '0';
            FIR_S_AXIS_DATA_tlast_i2 <= '0';
            FIR_S_AXIS_DATA_tlast_i1 <= '0';    
            -- FIR M
            FIR_M_AXIS_DATA_tvalid_i1 <= '0';
            FIR_M_AXIS_DATA_tvalid_i2 <= '0';
            FIR_M_AXIS_DATA_tvalid_i3 <= '0';
            FIR_M_AXIS_DATA_tlast_i1 <= '0';
            FIR_M_AXIS_DATA_tdata_i1 <= (others => '0');
            -- S AXIS
            s00_axis_tvalid_i1 <= '0';
            s00_axis_tvalid_i2 <= '0';
            s00_axis_tlast_i1 <= '0';
            s00_axis_tlast_i2 <= '0';
            s00_axis_tdata_i1 <= (others => '0');
        elsif (clk_200'event and clk_200 = '1') then
            -- FIR S
            FIR_S_AXIS_DATA_tvalid_i2 <= FIR_S_AXIS_DATA_tvalid_i3;
            FIR_S_AXIS_DATA_tvalid_i1 <= FIR_S_AXIS_DATA_tvalid_i2;
            FIR_S_AXIS_DATA_tlast_i2 <= FIR_S_AXIS_DATA_tlast_i3;
            FIR_S_AXIS_DATA_tlast_i1 <= FIR_S_AXIS_DATA_tlast_i2;     
            -- FIR M
            FIR_M_AXIS_DATA_tvalid_i1 <= FIR_M_AXIS_DATA_tvalid;
            FIR_M_AXIS_DATA_tvalid_i2 <= FIR_M_AXIS_DATA_tvalid_i1;
            FIR_M_AXIS_DATA_tvalid_i3 <= FIR_M_AXIS_DATA_tvalid_i2;
            FIR_M_AXIS_DATA_tlast_i1 <= FIR_M_AXIS_DATA_tlast;
            FIR_M_AXIS_DATA_tdata_i1 <= FIR_M_AXIS_DATA_tdata;
            -- S AXIS
            s00_axis_tvalid_i1 <= s00_axis_tvalid;
            s00_axis_tvalid_i2 <= s00_axis_tvalid_i1;
            s00_axis_tlast_i1 <= s00_axis_tlast;
            s00_axis_tlast_i2 <= s00_axis_tlast_i1;                                          
            s00_axis_tdata_i1 <= s00_axis_tdata; 
        end if;
    end process;
         
    -- output counter
    output_counter_inst: process(clk_100, aresetn)
    begin 
        if (aresetn = '0') then
            m00_axis_counter <= (others => '0');
        elsif (clk_100'event and clk_100 = '1') then
            if(m00_axis_tvalid_i2 = '1') then
                m00_axis_counter <= m00_axis_counter + 1;
            else
                m00_axis_counter <= (others => '0');
            end if;
        end if;
    end process;
    
    -- input counter
    input_counter_inst: process(clk_100, aresetn)
    begin 
        if (aresetn = '0') then
            s00_axis_counter <= (others => '0');
        elsif (clk_100'event and clk_100 = '1') then
            if(s00_axis_tvalid = '1') then
                s00_axis_counter <= s00_axis_counter + 1;
            else
                s00_axis_counter <= (others => '0');
            end if;
        end if;
    end process;
    
    -- fir output counter
    fir_output_counter_inst: process(clk_200, aresetn)
    begin 
        if (aresetn = '0') then
            fir_output_counter <= (others => '0');
        elsif (clk_200'event and clk_200 = '1') then
            if(read_bram_PORTA = '1') then
                if(FIR_M_AXIS_DATA_tvalid = '1') then
                    fir_output_counter <= fir_output_counter + 1;
                end if;
            else
                fir_output_counter <= (others => '0');
            end if;
        end if;
    end process;
    
    -- fir input counter
    fir_input_counter_inst: process(clk_200, aresetn)
    begin 
        if (aresetn = '0') then
            fir_input_counter <= (others => '0');
        elsif (clk_200'event and clk_200 = '1') then
            if(read_bram_PORTA = '1') then
                if(FIR_S_AXIS_DATA_tvalid = '1') then
                    fir_input_counter <= fir_input_counter + 1;
                end if;
            else
                fir_input_counter <= (others => '0');
            end if;
        end if;
    end process;
                   
end arch_imp;
