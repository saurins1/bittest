----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/07/2017 08:09:40 AM
-- Design Name: 
-- Module Name: VOLTA_FIR_FILTER_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity fir_filter_512_tb is
	generic (
    -- Users to add parameters here
    REGISTER_WIDTH              : integer := 32;
    COEFF_WIDTH                 : integer := 24;
    N_COEFFICIENTS              : integer    := 512;
    COUNTER_WAIT                : integer    := 4;
    N                           : integer    := 9; --2^9 = 512
    C_S00_AXIS_TDATA_WIDTH      : integer    := 16;
    C_M00_AXIS_TDATA_WIDTH      : integer    := 16
    );
end fir_filter_512_tb;

architecture Behavioral of fir_filter_512_tb is

    COMPONENT fir_filter_512 is
	generic (
        -- Users to add parameters here
        REGISTER_WIDTH              : integer := 32;
        COEFF_WIDTH                 : integer := 24;
        N_COEFFICIENTS              : integer := 512;
        COUNTER_WAIT                : integer := 4;
        N                           : integer := 9; --2^9 = 512
        C_S00_AXIS_TDATA_WIDTH      : integer  := 16;
        C_M00_AXIS_TDATA_WIDTH      : integer := 16
    );
	port (
		clk_100                    : in STD_LOGIC;
        clk_200                    : in STD_LOGIC;
        aresetn                    : in STD_LOGIC;
		-- Users to add ports here      
        FIR_FILTER_EN              : in STD_LOGIC;
        AUTOMATIC_FREQ_FILTER_EN   : in STD_LOGIC; 
        DATA_WINDOW                : in STD_LOGIC_VECTOR(REGISTER_WIDTH-1 downto 0);
        -- COEFFICIENTS       
        LOAD_CONFIG_FIR            : in std_logic;
        LOAD_CONFIG_FIR_TYPE       : in std_logic;
        CONFIG_INDEX_FIR           : in std_logic_vector(REGISTER_WIDTH-1 downto 0);   
        CONFIG_VALUE_FIR           : in std_logic_vector(REGISTER_WIDTH-1 downto 0); 
        LOADING_COEFFICIENTS       : out STD_LOGIC;
		-- User ports ends
		-- Do not modify the ports beyond this line
		-- Ports of Axi Slave Bus Interface S00_AXIS

		s00_axis_tready	: out std_logic;
		s00_axis_tdata	: in std_logic_vector(C_S00_AXIS_TDATA_WIDTH-1 downto 0);
		s00_axis_tlast	: in std_logic;
		s00_axis_tvalid	: in std_logic;

		-- Ports of Axi Master Bus Interface M00_AXIS
		m00_axis_tvalid	: out std_logic;
		m00_axis_tdata	: out std_logic_vector(C_M00_AXIS_TDATA_WIDTH-1 downto 0);
		m00_axis_tlast	: out std_logic;
		m00_axis_tready	: in std_logic
	);
    end COMPONENT fir_filter_512;
    
    signal FIR_FILTER_EN : STD_LOGIC := '0'; 
    signal AUTOMATIC_FREQ_FILTER_EN : STD_LOGIC := '0'; 
    signal DATA_WINDOW: STD_LOGIC_VECTOR(REGISTER_WIDTH-1 downto 0) := (others => '0'); 
    signal LOAD_CONFIG_FIR: STD_LOGIC := '0'; 
    signal LOAD_CONFIG_FIR_TYPE: STD_LOGIC := '0'; 
    signal CONFIG_VALUE_FIR: STD_LOGIC_VECTOR(REGISTER_WIDTH-1 downto 0) := (others => '0'); 
    signal CONFIG_INDEX_FIR: STD_LOGIC_VECTOR(REGISTER_WIDTH-1 downto 0) := (others => '0'); 
    signal LOADING_COEFFICIENTS: STD_LOGIC;
    signal s00_axis_tready: std_logic;
    signal s00_axis_tdata: std_logic_vector(C_S00_AXIS_TDATA_WIDTH-1 downto 0) := (others => '0'); 
    signal s00_axis_tlast: std_logic := '0'; 
    signal s00_axis_tvalid: std_logic := '0'; 
    signal m00_axis_tvalid: std_logic;
    signal m00_axis_tdata: std_logic_vector(C_M00_AXIS_TDATA_WIDTH-1 downto 0);
    signal m00_axis_tlast: std_logic;
    signal m00_axis_tready: std_logic := '0'; 
    
    signal clk_100: STD_LOGIC := '0';
    signal clk_200: STD_LOGIC := '0';
    signal aresetn: STD_LOGIC := '0';
    
    constant clk_100_period: time := 10 ns;
    constant clk_200_period: time := 5 ns;
    
    -- LOAD COEFFICIENTS STATE MACHINE
    type states_load is (idle, load_input1, load_end1, load_input2, load_end2, load_end3); 
    signal state_load : states_load; 
    signal load_counter : unsigned(31 downto 0) := (others => '0'); 
    signal begin_load: STD_LOGIC := '0'; 
    
    -- LOAD SIGNAL STATE MACHINE
    type states_filter is (idle, filter_input, filter_end); 
    signal state_filter : states_filter;  
    signal start_filter : std_logic := '0';
    signal filter_counter : unsigned(15 downto 0) := (others => '0'); 
    signal filter_tvalid_counter: unsigned(31 downto 0) := (others => '0'); 
    
        -- Coefficients
    type coefficient_matrix is array(0 to 1023) of std_logic_vector(REGISTER_WIDTH-1 downto 0);
    
    signal coefficients_automatic_freq_filter: coefficient_matrix;

begin

   -- aclk process 
   aclk_period_process :process
   begin
    clk_100 <= '1';
    clk_200 <= '1';
    wait for clk_200_period/2;
    clk_200 <= '0';
    wait for clk_200_period/2;
    clk_100 <= '0';
    clk_200 <= '1';
    wait for clk_200_period/2;
    clk_200 <= '0';
    wait for clk_200_period/2;
   end process;
    
  -- Insert values for generic parameters !!
   uut: fir_filter_512 generic map ( REGISTER_WIDTH         => REGISTER_WIDTH,
                                     COEFF_WIDTH            => COEFF_WIDTH,
                                     N_COEFFICIENTS         => N_COEFFICIENTS,
                                     COUNTER_WAIT           => COUNTER_WAIT,
                                     N                      => N,
                                     C_S00_AXIS_TDATA_WIDTH => C_S00_AXIS_TDATA_WIDTH,
                                     C_M00_AXIS_TDATA_WIDTH => C_M00_AXIS_TDATA_WIDTH )
                          port map ( 
                                    clk_100          => clk_100,
                                    clk_200          => clk_200,
                                    aresetn          => aresetn, 
                                     FIR_FILTER_EN          => FIR_FILTER_EN,
                                     AUTOMATIC_FREQ_FILTER_EN          => AUTOMATIC_FREQ_FILTER_EN,
                                     DATA_WINDOW            => DATA_WINDOW,
                                     LOAD_CONFIG_FIR      => LOAD_CONFIG_FIR,
                                     LOAD_CONFIG_FIR_TYPE      => LOAD_CONFIG_FIR_TYPE,
                                     CONFIG_VALUE_FIR      => CONFIG_VALUE_FIR,
                                     CONFIG_INDEX_FIR      => CONFIG_INDEX_FIR,
                                     LOADING_COEFFICIENTS   => LOADING_COEFFICIENTS,
                                     s00_axis_tready        => s00_axis_tready,
                                     s00_axis_tdata         => s00_axis_tdata,
                                     s00_axis_tlast         => s00_axis_tlast,
                                     s00_axis_tvalid        => s00_axis_tvalid,
                                     m00_axis_tvalid        => m00_axis_tvalid,
                                     m00_axis_tdata         => m00_axis_tdata,
                                     m00_axis_tlast         => m00_axis_tlast,
                                     m00_axis_tready        => m00_axis_tready );
                                     
                                     
                                    stimulus: process
                                     begin
                                       -- hold reset state for 100 ns.
                                       FIR_FILTER_EN <= '1';
                                       --FIR_FILTER_EN <= '0';
                                       --AUTOMATIC_FREQ_FILTER_EN <= '0';
                                       AUTOMATIC_FREQ_FILTER_EN <= '1';
                                       --DATA_WINDOW      <= "00000000000000000010000000000000"; --8192
                                       --DATA_WINDOW      <= "00000000000000000001000000000000"; --4096
                                       --DATA_WINDOW      <= "00000000000000000001000000000000"; --2048
                                       DATA_WINDOW      <= "00000000000000000000010000000000"; --1024
                                       wait for clk_100_period*20;  
                                       aresetn <= '0';  
                                       wait for clk_100_period*20;
                                       aresetn <= '1';
                                       m00_axis_tready <= '1';
                                       wait for clk_100_period*20;
                                       -- load 
                                       begin_load <= '1';   
                                       wait for clk_100_period*10;
                                       begin_load <= '0'; 
                                       wait;
                                     end process;
                                        
                                    --COEFFICIENTS LOAD
                                     load_coeff_inst: process(clk_100, aresetn)
                                                                
                                       type dataFile is file of integer;
                                       file data_file: dataFile is in "coefficients_512.dat";
                                       variable data_in: integer; 
                                                                
                                     begin 
                                       if (aresetn = '0') then
                                           CONFIG_INDEX_FIR <= (others => '0');
                                           CONFIG_VALUE_FIR <= (others => '0');
                                           LOAD_CONFIG_FIR_TYPE <= '0';
                                           LOAD_CONFIG_FIR <= '0'; 
                                           state_load <= idle;  
                                           load_counter <= (others => '0');
                                           start_filter <= '0';
                                       elsif (clk_100'event and clk_100 = '1') then
                                           case state_load is  
                                               when idle =>      
                                                   start_filter <= '0';
                                                   LOAD_CONFIG_FIR_TYPE <= '0';    
                                                   if(begin_load = '1') then
                                                       state_load <= load_input1;  
                                                       load_counter <= (others => '0'); 
                                                       CONFIG_INDEX_FIR <= (others => '0');
                                                       CONFIG_VALUE_FIR <= (others => '0');   
                                                       LOAD_CONFIG_FIR <= '0';        
                                                   end if;
                                               when load_input1 =>               
                                                   -- read
                                                   LOAD_CONFIG_FIR <= '1';  
                                                   load_counter <= load_counter + 1;
                                                   CONFIG_INDEX_FIR <= std_logic_vector(load_counter);
                                                   if(load_counter = 0) then
                                                        CONFIG_VALUE_FIR <= "00000000000000000000001000000000"; --512
                                                        coefficients_automatic_freq_filter(to_integer(load_counter)) <= "00000000000000000000001000000000"; --512
                                                   elsif(load_counter = 1) then
                                                        CONFIG_VALUE_FIR <= "00000000000000000000000000010011"; --19
                                                        coefficients_automatic_freq_filter(to_integer(load_counter)) <= "00000000000000000000000000010011"; --19
                                                   else
                                                        if not endfile(data_file) then
                                                            read (data_file, data_in);
                                                            CONFIG_VALUE_FIR<=std_logic_vector(to_signed(data_in, CONFIG_VALUE_FIR'length));
                                                            coefficients_automatic_freq_filter(to_integer(load_counter)) <= std_logic_vector(to_signed(data_in, CONFIG_VALUE_FIR'length));
                                                        end if;
                                                        if not endfile(data_file) then
                                                            read (data_file, data_in);
                                                        end if;                                                           
                                                        if(to_integer(load_counter) >= (N_COEFFICIENTS)+1) then
                                                                load_counter <= (others => '0'); 
                                                                state_load <= load_end1; 
                                                             --state_load <= load_end2;     
                                                        end if;   
                                                   end if;                                                              
                                               when load_end1 =>   
                                                   LOAD_CONFIG_FIR <= '0'; 
                                                   LOAD_CONFIG_FIR_TYPE <= '1'; 
                                                   if(LOADING_COEFFICIENTS = '0') then
                                                       state_load <= load_input2; 
                                                   end if;
                                               when load_input2 =>     
                                                    LOAD_CONFIG_FIR <= '1'; 
                                                    CONFIG_VALUE_FIR<=coefficients_automatic_freq_filter(to_integer(load_counter));
                                                    CONFIG_INDEX_FIR <= std_logic_vector(load_counter);
                                                    if(to_integer(load_counter) >= (N_COEFFICIENTS)+1) then
                                                        load_counter <= (others => '0'); 
                                                        state_load <= load_end2;  
                                                    else
                                                        load_counter <= load_counter + 1;   
                                                    end if; 
                                               when load_end2 => 
                                                   LOAD_CONFIG_FIR <= '0';   
                                                   if(begin_load = '0') then
                                                       start_filter <= '1';
                                                       state_load <= load_end3; 
                                                   end if;
                                               when load_end3 => 
                                                    if(begin_load = '0') then  
                                                        state_load <= idle; 
                                                    end if;  
                                               when others =>
                                                   null;                   
                                           end case;                                                                                            
                                       end if;
                                     end process; 
                                     
                                   --PERFORM FILTER
                                   filter_inst: process(clk_100, aresetn)
                                       type SIGNALFile is file of integer;
                                       file signal_file: SIGNALFile is in "signal_to_filt.dat";
                                       variable signal_in: integer; 
                                   begin 
                                       if (aresetn = '0') then
                                           -- dividend signals
                                           s00_axis_tvalid <= '0';
                                           s00_axis_tlast <= '0';
                                           s00_axis_tdata <= (others => '0');
                                           state_filter <= idle;  
                                           filter_counter <= (others => '0');
                                       elsif (clk_100'event and clk_100 = '1') then
                                           case state_filter is        
                                               when idle =>
                                                   if(start_filter = '1') then
                                                       state_filter<= filter_input;  
                                                       filter_counter <= (others => '0'); 
                                                   end if;                 
                                               when filter_input => 
                                                   if(s00_axis_tready = '1') then                      
                                                       s00_axis_tvalid <= '1';
                                                       s00_axis_tlast <= '0';
                                                       -- read coeff
                                                       if not endfile(signal_file) then
                                                           read (signal_file, signal_in);
                                                           s00_axis_tdata<=std_logic_vector(to_signed(signal_in, s00_axis_tdata'length));
                                                       end if;
                                                       if not endfile(signal_file) then
                                                           read (signal_file, signal_in);
                                                       end if;
                                                       --
                                                       if(filter_counter >= (unsigned(DATA_WINDOW)-1)) then
                                                           s00_axis_tlast <= '1';  
                                                           state_filter <= filter_end;
                                                       end if;
                                                       filter_counter <= filter_counter + 1;
                                                   else
                                                       s00_axis_tvalid <= '0';  
                                                   end if;  
                                               when filter_end =>
                                                   s00_axis_tvalid <= '0';
                                                   s00_axis_tlast <= '0';  
                                                   s00_axis_tdata <= (others => '0');
                                                   if(start_filter = '0') then
                                                     state_filter <= idle;     
                                                   end if;         
                                               when others =>
                                                   null;                   
                                           end case;                                                                                            
                                       end if;
                                   end process;  
                                   


end Behavioral;
