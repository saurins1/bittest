----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 09/27/2017 10:07:23 AM
-- Design Name: 
-- Module Name: fir_correlation - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

-- LIMITATIONS:
-- 1- Oversampling must to be power of 2. Minimum value = 2
-- 2- Minimum N_DSP = 8. N_DSP = N_TAPS/OVERSAMPLING
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity fir_correlation is
	generic (
	    REGISTER_W48        : INTEGER    := 48;
        REGISTER_W32        : INTEGER    := 32;
        REGISTER_W16        : INTEGER    := 16;
        N_TAPS              : INTEGER    := 512;
        N_SETS_TAP          : INTEGER    := 3;
        OVERSAMPLING        : INTEGER    := 2;
        WAIT_CYCLES         : INTEGER    := 4
    );
    port (
        aclk         : in std_logic;
        aresetn      : in std_logic;
        
        NO_READ_FIR_RESULT          : in STD_LOGIC;
        READ_FIR_RESULT             : in STD_LOGIC;
        DATA_WINDOW                 : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        SAMPLE_FREQ                 : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        DATA_WINDOW_RESULT          : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        SAMPLE_FREQ_RESULT          : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        DATA_WINDOW_HALF_RESULT     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		FIR_END                     : out STD_LOGIC;
        
		-- FIR config
        BAND_PASS_FILTER_EN         : in STD_LOGIC;
        BAND_PASS_FILTER_L_DIVIDER  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GAUSSIAN_FILTER_EN          : in STD_LOGIC;
        GAUSSIAN_FILTER_L_DIVIDER   : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        ENVELOPE_FILTER_EN          : in STD_LOGIC;
        ENVELOPE_FILTER_L_DIVIDER   : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        
        FIR_TAPS_AXIS_TREADY        : out STD_LOGIC;
        FIR_TAPS_AXIS_TDATA         : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        FIR_TAPS_AXIS_TLAST         : in STD_LOGIC;
        FIR_TAPS_AXIS_TVALID        : in STD_LOGIC;
     
		-- Signal Input
        SIGNAL_INPUT_AXIS_TREADY    : out STD_LOGIC;
        SIGNAL_INPUT_AXIS_TDATA     : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        SIGNAL_INPUT_AXIS_TLAST     : in STD_LOGIC;
        SIGNAL_INPUT_AXIS_TVALID    : in STD_LOGIC;
        
		-- Gates Config
        GATES_EN                    : in STD_LOGIC;
        GATES_SAMPLE_START          : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATES_SAMPLE_END            : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        
		-- Gates Result
        GATES_AXIS_TVALID           : out STD_LOGIC;
        GATES_AXIS_TDATA            : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATES_AXIS_TLAST            : out STD_LOGIC;
        GATES_AXIS_TREADY           : in STD_LOGIC;
        
		-- Correlation Config
        CORRELATION_EN                  : in STD_LOGIC;
        CORRELATION_GATE_SAMPLE_START   : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        CORRELATION_GATE_SAMPLE_END     : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        
		-- Correlation Result
        CORRELATION_AXIS_TVALID     : out STD_LOGIC;
        CORRELATION_AXIS_TDATA      : out STD_LOGIC_VECTOR(REGISTER_W48-1 downto 0);
        CORRELATION_AXIS_TLAST      : out STD_LOGIC;
        CORRELATION_AXIS_TREADY     : in STD_LOGIC;

		-- FIR Result
        FIR_RESULT_AXIS_TVALID      : out STD_LOGIC;
        FIR_RESULT_AXIS_TDATA       : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        FIR_RESULT_AXIS_TLAST       : out STD_LOGIC;
        FIR_RESULT_AXIS_TREADY      : in STD_LOGIC
    );
end fir_correlation;

architecture Behavioral of fir_correlation is

    -- Number of dsps
    constant N_DSP                      : INTEGER range 0 to 16383 := N_TAPS/OVERSAMPLING;
    constant DSP_DELAY                  : INTEGER range 0 to 16383 := 4;
    constant N_HALF_TAPS                : INTEGER range 0 to 16383 := N_TAPS/2;
    
    -- FIR Window oversampling
    constant WINDOW_OVERSAMPLING        : INTEGER range 0 to 16383 := N_TAPS + (N_TAPS/OVERSAMPLING) - 1;
    
    -- FIR Window oversampling step
    constant WINDOW_OVERSAMPLING_STEP   : INTEGER range 0 to 16383 := N_DSP;

    signal data_window_int                  : INTEGER range 0 to 65536 :=0;
    
    -- FIR Window oversampling offset
    signal window_oversampling_offset       : UNSIGNED(16 downto 0);
    signal window_oversampling_offset_end   : UNSIGNED(16 downto 0);
    
    -- TAPS registers
    type fir_taps_array is array ( 0 to N_DSP-1 ) of STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal fir_taps : fir_taps_array;
     
    -- DSP SLICE 48   
    COMPONENT fir_correlation_DSP
      PORT (
        CLK : IN STD_LOGIC;
        CE : IN STD_LOGIC;
        SCLR : IN STD_LOGIC;
        A : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
        B : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        P : OUT STD_LOGIC_VECTOR(47 DOWNTO 0)
      );
    END COMPONENT;
    
    -- DSP SCLR 
    signal fir_corr_dsp_ce          : STD_LOGIC;
    signal fir_corr_dsp_ce_array    : STD_LOGIC_VECTOR(N_DSP-1 DOWNTO 0);
    
    -- DSP SCLR 
    signal fir_corr_dsp_sclr          : STD_LOGIC;
    signal fir_corr_dsp_sclr_array    : STD_LOGIC_VECTOR(N_DSP-1 DOWNTO 0);
    signal fir_corr_dsp_sclr_first          : STD_LOGIC;
      
    -- DSP result array
    type dsp_48_p_array is array ( 0 to N_DSP-1 ) of std_logic_vector(47 downto 0);
    signal dsp_48_p : dsp_48_p_array;
           
    -- BRAM stores FIR taps (3 sets)
    COMPONENT fir_correlation_taps_BRAM
      PORT (
        clka : IN STD_LOGIC;
        ena : IN STD_LOGIC;
        wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addra : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
        dina : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        douta : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
        clkb : IN STD_LOGIC;
        enb : IN STD_LOGIC;
        web : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addrb : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
        dinb : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        doutb : OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
      );
    END COMPONENT;
    
    -- FIR taps BRAM port a
    signal fir_taps_bram_ena    : STD_LOGIC;
    signal fir_taps_bram_wea    : STD_LOGIC_VECTOR(0 DOWNTO 0);
    signal fir_taps_bram_addra  : STD_LOGIC_VECTOR(11 DOWNTO 0);
    signal fir_taps_bram_dina   : STD_LOGIC_VECTOR(15 DOWNTO 0);
    signal fir_taps_bram_douta  : STD_LOGIC_VECTOR(15 DOWNTO 0);
    
    signal fir_taps_axis_tvalid_first   : STD_LOGIC;
    signal fir_taps_axis_tlast_i        : std_logic_vector(1 downto 0);

    -- FIR taps BRAM port b
    signal fir_taps_bram_enb    : STD_LOGIC;
    signal fir_taps_bram_web    : STD_LOGIC_VECTOR(0 DOWNTO 0);
    signal fir_taps_bram_addrb  : STD_LOGIC_VECTOR(11 DOWNTO 0);
    signal fir_taps_bram_dinb   : STD_LOGIC_VECTOR(15 DOWNTO 0);
    signal fir_taps_bram_doutb  : STD_LOGIC_VECTOR(15 DOWNTO 0);
    
    -- FIR coefficient set select
    signal fir_taps_set  : STD_LOGIC_VECTOR(2 downto 0);
    type fir_taps_offset_array is array ( 0 to N_SETS_TAP-1 ) of STD_LOGIC_VECTOR(11 downto 0);
    signal fir_taps_offset_aux      : fir_taps_offset_array;
    signal fir_taps_offset_aux_i    : STD_LOGIC_VECTOR(11 downto 0);
    signal fir_taps_offset          : STD_LOGIC_VECTOR(11 downto 0);
    signal fir_l_divider            : INTEGER range 0 to 128;
       
    signal read_fir_taps_first : STD_LOGIC;
   
    -- BRAM stores signal input or FIR result
    COMPONENT fir_correlation_signal_BRAM
      PORT (
        clka : IN STD_LOGIC;
        ena : IN STD_LOGIC;
        wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addra : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        dina : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
        douta : OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
        clkb : IN STD_LOGIC;
        enb : IN STD_LOGIC;
        web : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addrb : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        dinb : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
        doutb : OUT STD_LOGIC_VECTOR(13 DOWNTO 0)
      );
    END COMPONENT;
    
    -- signal BRAM port a
    signal signal_bram_ena      : STD_LOGIC;
    signal signal_bram_wea      : STD_LOGIC_VECTOR(0 DOWNTO 0);
    signal signal_bram_addra    : STD_LOGIC_VECTOR(15 DOWNTO 0);
    signal signal_bram_addra_aux: STD_LOGIC_VECTOR(15 DOWNTO 0); 
    signal signal_bram_dina     : STD_LOGIC_VECTOR(13 DOWNTO 0);
    signal signal_bram_douta    : STD_LOGIC_VECTOR(13 DOWNTO 0);
 
    signal signal_input_axis_tlast_i    : std_logic_vector(1 downto 0);
    signal write_fir_result_first       : STD_LOGIC;
    
    -- signal BRAM port b
    signal signal_bram_enb      : STD_LOGIC;
    signal signal_bram_web      : STD_LOGIC_VECTOR(0 DOWNTO 0);
    signal signal_bram_addrb    : STD_LOGIC_VECTOR(16 DOWNTO 0);
    signal signal_bram_dinb     : STD_LOGIC_VECTOR(13 DOWNTO 0);
    signal signal_bram_doutb    : STD_LOGIC_VECTOR(13 DOWNTO 0);
    
    signal rectified_signal_bram_doutb    : STD_LOGIC_VECTOR(13 DOWNTO 0);
    
    -- GENERAL STATE MACHINE
    type states_general_sm is (idle, fir_input, select_fir_tap_set, fir_filtering, provide_gates, check_operation, provide_correlation, provide_fir_result, provide_fir_result_finish); 
    signal state_general_sm : states_general_sm;
    
    signal fir_corr_signal : std_logic_vector(13 downto 0);
    
    signal low_pass_filter_flag    : STD_LOGIC;
    
    -- FIR filtering 
    signal fir_filtering_flag   : STD_LOGIC;
    signal fir_filtering_end    : STD_LOGIC;
    
    -- Provide gates
    signal provide_gates_flag   : STD_LOGIC;
    signal provide_gates_end    : STD_LOGIC;
    
    signal read_gates_first     : STD_LOGIC;
    signal gates_axis_tvalid_i  : STD_LOGIC_VECTOR(1 downto 0);
    signal gates_axis_tlast_i   : STD_LOGIC_VECTOR(1 downto 0);
    
    -- Provide fir result
    signal provide_fir_result_flag  : STD_LOGIC;
    signal provide_fir_result_end   : STD_LOGIC;
    
    signal read_fir_result_first    : STD_LOGIC;
    signal fir_result_axis_tvalid_i : STD_LOGIC_VECTOR(1 downto 0);
    signal fir_result_axis_tlast_i  : STD_LOGIC_VECTOR(1 downto 0);
    
    signal data_window_half_int     : INTEGER range 0 to 65536 :=0;
        
    -- Provide correlation
    signal provide_correlation_flag : STD_LOGIC;
    signal provide_correlation_end  : STD_LOGIC;
    signal provide_correlation_ending  : STD_LOGIC;
    
    signal correlation_axis_tlast_i    : STD_LOGIC;
    
    type states_write_signal_sm is (idle, write_signal_input, write_fir_result, read_result, read_correlation_gate, read_correlation_gate_wait); 
    signal state_write_signal_sm : states_write_signal_sm;
    
    type states_read_fir_signal_sm is (idle, read_fir_filtering_signal, padding_signal_input1, padding_signal_input2, read_gates, read_result, read_correlation_gate, read_correlation_gate_wait); 
    signal state_read_fir_signal_sm : states_read_fir_signal_sm;
    
    -- Correlation
    type states_provide_correlation is (idle, provide_correlation_1, provide_correlation_2, provide_correlation_3, provide_correlation_4); 
    signal state_provide_correlation : states_provide_correlation;
    
    signal correlation_addb_offset_end   : UNSIGNED(REGISTER_W16 downto 0);  
    signal corr_en : STD_LOGIC;

    
begin

    fir_taps_offset_aux_i   <= std_logic_vector(to_unsigned(N_TAPS,fir_taps_offset_aux_i'length));
    fir_taps_offset_aux(0)  <= (others => '0');
    fir_taps_offset_aux(1)  <= fir_taps_offset_aux_i;
    fir_taps_offset_aux(2)  <= fir_taps_offset_aux_i(10 downto 0) & "0";
    
    DATA_WINDOW_RESULT <= std_logic_vector(to_unsigned(data_window_int, DATA_WINDOW_RESULT'length));
    DATA_WINDOW_HALF_RESULT <= std_logic_vector(to_unsigned(data_window_half_int, DATA_WINDOW_HALF_RESULT'length));
       
    -- GENERAL STATE MACHINE
    general_sm: process(aclk)
        variable var_counter_wait  : integer range 0 to 65536 :=0; 
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then  
                -- FIR taps
                fir_taps_set            <= (others => '0');
                fir_taps_offset         <= (others => '0');
                -- Fir filtering
                fir_filtering_flag      <= '0';
                low_pass_filter_flag    <= '0';
                -- Gates    
                provide_gates_flag      <= '0';
                -- FIR result
                provide_fir_result_flag <= '0';
                SIGNAL_INPUT_AXIS_TREADY<= '0';
                FIR_TAPS_AXIS_TREADY    <= '0';
                FIR_END                 <= '0';
                SAMPLE_FREQ_RESULT      <= (others => '0');
                -- Correlation
                provide_correlation_flag<= '0';
                corr_en                 <= '0';
                -- State
                state_general_sm        <= idle;
            else
                case state_general_sm is        
                    when idle =>
                        -- waiting for signal input
                        SIGNAL_INPUT_AXIS_TREADY <= '1';
                        FIR_TAPS_AXIS_TREADY     <= '1';
                        if(SIGNAL_INPUT_AXIS_TVALID = '1') then
                            SAMPLE_FREQ_RESULT  <= SAMPLE_FREQ;
                            data_window_int     <= to_integer(unsigned(DATA_WINDOW));
                            if(DATA_WINDOW(0) = '0') then
                                data_window_half_int<= to_integer(unsigned(DATA_WINDOW(REGISTER_W32-1 downto 1)));
                            else
                                data_window_half_int<= to_integer(unsigned(DATA_WINDOW(REGISTER_W32-1 downto 1))) + 1;
                            end if;
                            fir_taps_set        <= (others => '1');
                            corr_en             <= '1';
                            state_general_sm    <= fir_input; 
                        end if;
                    when fir_input =>
                        -- waiting signal input end
                        if(signal_input_axis_tlast_i(1) = '1') then
                            state_general_sm <= select_fir_tap_set;                     
                        end if;
                    when select_fir_tap_set => 
                        -- select FIR coefficients set
                        SIGNAL_INPUT_AXIS_TREADY <= '0';
                        --FIR_TAPS_AXIS_TREADY     <= '0';  
                        var_counter_wait := 0;                            
                        if(BAND_PASS_FILTER_EN = '1' and fir_taps_set(0) = '1') then
                            fir_taps_set(0)     <= '0';
                            fir_taps_offset     <= fir_taps_offset_aux(0);
                            fir_l_divider       <= to_integer(unsigned(BAND_PASS_FILTER_L_DIVIDER));
                            state_general_sm    <= fir_filtering;
                        elsif(GAUSSIAN_FILTER_EN = '1' and fir_taps_set(1) = '1') then
                            fir_taps_set(1)     <= '0';
                            fir_taps_offset     <= fir_taps_offset_aux(1);  
                            fir_l_divider       <= to_integer(unsigned(GAUSSIAN_FILTER_L_DIVIDER));
                            state_general_sm    <= fir_filtering;
                        elsif(ENVELOPE_FILTER_EN = '1' and fir_taps_set(2) = '1') then
                            fir_taps_set(2)     <= '0';
                            fir_taps_offset     <= fir_taps_offset_aux(2);
                            fir_l_divider       <= to_integer(unsigned(ENVELOPE_FILTER_L_DIVIDER));
                            low_pass_filter_flag<= '1';
                            state_general_sm    <= fir_filtering;
                        else
                            state_general_sm    <= provide_gates; 
                        end if;
                    when fir_filtering =>
                        -- FIR filtering
                        if(var_counter_wait >= WAIT_CYCLES) then
                            if(fir_filtering_end = '1') then
                                fir_filtering_flag  <= '0';
                                low_pass_filter_flag<= '0';
                                state_general_sm    <= select_fir_tap_set;
                            else
                                fir_filtering_flag  <= '1';
                            end if; 
                        else
                            var_counter_wait := var_counter_wait + 1;
                        end if;                       
                    when provide_gates =>
                        -- Provide gates
                        if(GATES_EN = '1') then
                            if(provide_gates_end = '1') then
                                provide_gates_flag  <= '0';
                                state_general_sm    <= check_operation;
                            else
                                provide_gates_flag  <= '1';
                            end if;
                        else
                            if(NO_READ_FIR_RESULT = '1') then 
                                FIR_END <= '0'; 
                                state_general_sm  <= idle;   
                            else 
                                FIR_END <= '1';
                                if(READ_FIR_RESULT = '1') then
                                    state_general_sm  <= provide_fir_result;
                                end if;
                            end if;
                        end if;
                    when check_operation =>
                        -- Wait for read result
                        if(CORRELATION_EN = '1' and corr_en = '1' and 
                          (unsigned(CORRELATION_GATE_SAMPLE_END) > unsigned(CORRELATION_GATE_SAMPLE_START))) then
                            state_general_sm    <= provide_correlation;
                            corr_en             <= '0';         
                            var_counter_wait        := 0;
                        else
                            if(NO_READ_FIR_RESULT = '1') then
                                FIR_END <= '0';
                                state_general_sm  <= idle; 
                            else
                                FIR_END <= '1';
                                if(READ_FIR_RESULT = '1') then
                                    state_general_sm  <= provide_fir_result;
                                end if;
                            end if;
                        end if;
                    when provide_correlation =>
                        -- Provide correlation
                        if(provide_correlation_end = '1') then
                            if(var_counter_wait >= 16) then
                                provide_correlation_flag <= '0';
                                state_general_sm         <= check_operation;
                            else
                                var_counter_wait := var_counter_wait + 1;
                            end if;
                        else
                            provide_correlation_flag <= '1';
                        end if;
                    when provide_fir_result =>
                        -- Provide FIR result
                        if(provide_fir_result_end = '1') then
                            provide_fir_result_flag <= '0';
                            FIR_END                 <= '0';
                            state_general_sm        <= provide_fir_result_finish;
                        else
                            provide_fir_result_flag <= '1';
                        end if;
                    when provide_fir_result_finish =>
                        if(READ_FIR_RESULT = '0') then
                            state_general_sm        <= idle;
                        end if;
                    when others =>
                        null;
                end case;
             end if; 
         end if;
    end process;
     
    -- Write signal input
    -- Write FIR result
    -- Read correlation gate
    signal_BRAM_porta: process(aclk) 
        variable var_counter_aux  : integer range 0 to 65536 :=0;
        variable var_counter_result    : integer range 0 to 65536 :=0;
        variable var_counter_wait      : integer range 0 to 8192 :=0;
        variable var_time_to_wait      : integer range 0 to 8192 :=0;
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                signal_bram_ena   <= '1';
                signal_bram_wea   <= (others => '0');
                signal_bram_addra <= (others => '0');
                signal_bram_addra_aux <= (others => '0');
                signal_bram_dina  <= (others => '0');
                signal_input_axis_tlast_i <= (others => '0');
                write_fir_result_first      <= '1';
                -- State
                state_write_signal_sm       <= idle;
            else     
                signal_input_axis_tlast_i(0) <= SIGNAL_INPUT_AXIS_TLAST;
                signal_input_axis_tlast_i(1) <= signal_input_axis_tlast_i(0);
                case state_write_signal_sm is        
                    when idle =>
                        signal_bram_wea <= (others => '0');
                        if(fir_filtering_flag = '1') then
                            if(fir_corr_dsp_sclr_array(0) = '0' and fir_corr_dsp_sclr_array(1) = '1') then   
                                var_time_to_wait    := N_TAPS+DSP_DELAY-1; --N_TAPS + delay_dsp - 1
                                var_counter_result  := 0;
                                var_counter_wait    := 0;
                                var_counter_aux     := 0;
                                write_fir_result_first  <= '1';
                                state_write_signal_sm   <= write_fir_result;
                            end if;
                        elsif(SIGNAL_INPUT_AXIS_TVALID = '1') then
                            signal_bram_wea   <= (others => '1');
                            signal_bram_dina  <= SIGNAL_INPUT_AXIS_TDATA(13 downto 0); 
                            --signal_bram_addra <= (others => '0');
                            if(BAND_PASS_FILTER_EN = '1' or GAUSSIAN_FILTER_EN = '1' or ENVELOPE_FILTER_EN = '1') then
                                signal_bram_addra <= std_logic_vector(to_unsigned(N_HALF_TAPS-1, signal_bram_addra'length));
                            else
                                signal_bram_addra <= (others => '0');
                            end if;
                            state_write_signal_sm <= write_signal_input;
                        elsif(provide_correlation_flag = '1') then
                            signal_bram_addra       <= std_logic_vector(resize(unsigned(CORRELATION_GATE_SAMPLE_START), signal_bram_addra'length));
                            signal_bram_addra_aux   <= std_logic_vector(resize(unsigned(CORRELATION_GATE_SAMPLE_START), signal_bram_addra_aux'length));
                            state_write_signal_sm   <= read_correlation_gate; 
                        elsif(provide_fir_result_flag = '1') then  
                            signal_bram_addra <= (others => '0');
                            state_write_signal_sm <= read_result;                      
                        end if;
                    when write_signal_input =>
                        if(SIGNAL_INPUT_AXIS_TVALID = '1') then
                            signal_bram_dina  <= SIGNAL_INPUT_AXIS_TDATA(13 downto 0); 
                            signal_bram_addra <= std_logic_vector(unsigned(signal_bram_addra)+1);
                        else
                            if(signal_input_axis_tlast_i(1) = '1') then
                                state_write_signal_sm   <= idle;
                            end if;
                        end if;
                    when write_fir_result =>
                        if(var_counter_wait >= var_time_to_wait-1) then 
                            signal_bram_wea   <= (others => '1');
                            if(var_counter_result >= data_window_int) then
                                if(fir_filtering_flag = '0') then      
                                    state_write_signal_sm   <= idle; 
                                end if;   
                            else
                                if(write_fir_result_first = '1') then
                                    signal_bram_addra <= (others => '0');
                                    write_fir_result_first  <= '0';
                                else
                                    signal_bram_addra <= std_logic_vector(unsigned(signal_bram_addra)+1);
                                end if; 
                                signal_bram_dina  <= dsp_48_p(var_counter_aux)(REGISTER_W16 + fir_l_divider -3 downto fir_l_divider);          
                                if(var_counter_aux >= N_DSP-1) then
                                    var_counter_wait := 0;
                                    var_counter_aux  := 0;
                                    var_time_to_wait := N_TAPS;
                                    --time_to_wait := N_TAPS+N_DSP-N_DSP;
                                else             
                                    var_counter_aux := var_counter_aux + 1;       
                                end if;
                                var_counter_result := var_counter_result + 1;
                            end if;
                        else
                            var_counter_wait := var_counter_wait + 1;
                        end if;
                    when read_correlation_gate =>
                        if(provide_correlation_flag = '1') then
                            if(CORRELATION_AXIS_TREADY = '1') then
                                if(unsigned(signal_bram_addra) >= (unsigned(CORRELATION_GATE_SAMPLE_END)) and provide_correlation_ending = '0') then
                                    var_counter_wait := 0;
                                    state_write_signal_sm <= read_correlation_gate_wait;
                                    signal_bram_addra <= std_logic_vector(unsigned(signal_bram_addra_aux) + N_DSP);
                                    signal_bram_addra_aux <= std_logic_vector(unsigned(signal_bram_addra_aux) + N_DSP);
                                else
                                    if(unsigned(signal_bram_addra) <= (unsigned(CORRELATION_GATE_SAMPLE_END))) then
                                        signal_bram_addra <= std_logic_vector(unsigned(signal_bram_addra)+1);
                                    end if;
                                end if;
                            end if;
                        else
                            state_write_signal_sm <= idle;
                        end if;
                    when read_correlation_gate_wait =>
                        --if(counter_wait >= N_DSP-1+DSP_DELAY) then -- N_DSP+DSP_DELAY-1
                        if(var_counter_wait >= N_DSP-1) then -- N_DSP+DSP_DELAY-1
                            state_write_signal_sm <= read_correlation_gate;
                        else
                            var_counter_wait:= var_counter_wait + 1;
                        end if;
                    when read_result =>
                        if(provide_fir_result_flag = '1') then
                            if(FIR_RESULT_AXIS_TREADY = '1') then
                                if(provide_fir_result_end = '0') then 
                                    if(read_fir_result_first = '1') then
                                        signal_bram_addra <= std_logic_vector(to_unsigned(1, signal_bram_addra'length));       
                                    else
                                        signal_bram_addra <= std_logic_vector(unsigned(signal_bram_addra)+2);
                                    end if; 
                                end if;    
                            end if;
                        else
                            state_write_signal_sm <= idle;
                        end if;                         
                    when others => 
                        null;
                end case;                                 
            end if;
        end if;
    end process;
    
    CORRELATION_AXIS_TLAST <= correlation_axis_tlast_i;
    
    -- Correlation result
    correlation_output_process: process(aclk) 
        variable var_counter_aux  : integer range 0 to 65536 :=0;
        variable var_counter_result  : integer range 0 to 65536 :=0;
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                CORRELATION_AXIS_TVALID <= '0';
                CORRELATION_AXIS_TDATA  <= (others => '0');
                correlation_axis_tlast_i  <= '0';
                state_provide_correlation <= idle;
            else    
                case state_provide_correlation is
                    when idle =>
                        if(provide_correlation_flag = '1') then
                            if(fir_corr_dsp_sclr_array(1) = '1' and fir_corr_dsp_sclr_array(0) = '0') then 
                                var_counter_result          := 0;
                                state_provide_correlation   <= provide_correlation_1;
                            end if;
                        end if;
                    when provide_correlation_1 =>
                        if(fir_corr_dsp_ce = '0' and fir_corr_dsp_ce_array(0) = '1') then 
                            state_provide_correlation <= provide_correlation_2;
                            var_counter_aux := 0;
                        end if;
                    when provide_correlation_2 =>
                        if(var_counter_result <= (unsigned(CORRELATION_GATE_SAMPLE_END)-unsigned(CORRELATION_GATE_SAMPLE_START))) then
                            if(var_counter_aux >= N_DSP) then
                                CORRELATION_AXIS_TVALID <= '0';
                                if(var_counter_result = (unsigned(CORRELATION_GATE_SAMPLE_END)-unsigned(CORRELATION_GATE_SAMPLE_START))) then
                                    state_provide_correlation <= provide_correlation_3;
                                else
                                    CORRELATION_AXIS_TVALID <= '0';
                                    state_provide_correlation <= provide_correlation_1;
                                end if;
                            else
                                CORRELATION_AXIS_TVALID <= '1';
                                CORRELATION_AXIS_TDATA  <= dsp_48_p(var_counter_aux);
                                if(var_counter_result = (unsigned(CORRELATION_GATE_SAMPLE_END)-unsigned(CORRELATION_GATE_SAMPLE_START))) then
                                    correlation_axis_tlast_i <= '1';
                                    state_provide_correlation <= provide_correlation_4;
                                end if;
                                var_counter_result  := var_counter_result + 1; 
                                var_counter_aux     := var_counter_aux + 1; 
                            end if;
                        else
                            CORRELATION_AXIS_TVALID <= '0';
                            correlation_axis_tlast_i <= '0';
                            if(provide_correlation_flag = '0') then      
                                state_provide_correlation   <= idle; 
                            end if; 
                        end if;
                    when provide_correlation_3 =>
                        CORRELATION_AXIS_TVALID <= '1';
                        CORRELATION_AXIS_TDATA  <= dsp_48_p(0);
                        correlation_axis_tlast_i <= '1';
                        state_provide_correlation <= provide_correlation_4;
                    when provide_correlation_4 =>
                        CORRELATION_AXIS_TVALID <= '0';
                        correlation_axis_tlast_i <= '0';
                        if(provide_correlation_flag = '0') then      
                            state_provide_correlation   <= idle; 
                        end if; 
                    when others =>
                        null;
                end case;
            end if;
        end if;
    end process; 
    
    GATES_AXIS_TDATA <= std_logic_vector(resize(signed(signal_bram_doutb), GATES_AXIS_TDATA'length));
    FIR_RESULT_AXIS_TDATA <= std_logic_vector(resize(signed(signal_bram_douta), SIGNAL_INPUT_AXIS_TDATA'length)) &
                             std_logic_vector(resize(signed(signal_bram_doutb), SIGNAL_INPUT_AXIS_TDATA'length));
  
    -- Read signal to filter
    -- Read gates
    -- Read FIR result
    -- Read correlation gate
    signal_BRAM_portb: process(aclk) 
        variable var_counter_wait      : integer range 0 to 8192 :=0;
        variable var_counter_result      : integer range 0 to 65535 :=0;
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                signal_bram_enb     <= '1';
                signal_bram_web     <= (others => '0');
                signal_bram_addrb   <= (others => '0');
                signal_bram_dinb   <= (others => '0');
                -- FIR filtering
                window_oversampling_offset <= (others => '0');
                window_oversampling_offset_end <= (others => '0');
                fir_filtering_end   <= '0';
                -- Gates
                provide_gates_end   <= '0';
                read_gates_first    <= '1';
                gates_axis_tvalid_i <= (others => '0');
                gates_axis_tlast_i  <= (others => '0');
                GATES_AXIS_TVALID   <= '0';
                GATES_AXIS_TLAST    <= '0';
                -- FIR result
                provide_fir_result_end <= '0';
                read_fir_result_first <= '1';
                fir_result_axis_tvalid_i <= (others => '0');
                fir_result_axis_tlast_i  <= (others => '0');
                FIR_RESULT_AXIS_TVALID   <= '0';
                FIR_RESULT_AXIS_TLAST    <= '0';
                -- DSP SCLR
                fir_corr_dsp_sclr <= '1';
                fir_corr_dsp_sclr_first <= '1';
                -- DSP CE
                fir_corr_dsp_ce <= '1';
                -- Correlation
                provide_correlation_end     <= '0';
                provide_correlation_ending  <= '0';   
                correlation_addb_offset_end <= (others => '0');
                -- State    
                state_read_fir_signal_sm <= idle;
            else
                -- GATES
                gates_axis_tvalid_i(0)      <= gates_axis_tvalid_i(1);
                GATES_AXIS_TVALID           <= gates_axis_tvalid_i(0);
                gates_axis_tlast_i(0)       <= gates_axis_tlast_i(1);
                GATES_AXIS_TLAST            <= gates_axis_tlast_i(0);
                -- FIR RESULT
                fir_result_axis_tvalid_i(0) <= fir_result_axis_tvalid_i(1);
                FIR_RESULT_AXIS_TVALID      <= fir_result_axis_tvalid_i(0);
                fir_result_axis_tlast_i(0)  <= fir_result_axis_tlast_i(1);
                FIR_RESULT_AXIS_TLAST       <= fir_result_axis_tlast_i(0);
                -- State machine
                case state_read_fir_signal_sm is 
                    when idle =>
                        fir_corr_dsp_ce <= '1';
                        --fir_corr_ACC_BYPASS(0)  <= '1';                     
                        if(fir_filtering_flag = '1') then
                            signal_bram_web   <= (others => '0'); 
                            fir_corr_dsp_sclr_first <= '1';
                            signal_bram_addrb <= (others => '0');
                            window_oversampling_offset <= (others => '0');
                            window_oversampling_offset_end <= to_unsigned(WINDOW_OVERSAMPLING-1,window_oversampling_offset_end'length);
                            state_read_fir_signal_sm <= read_fir_filtering_signal;
                        elsif(SIGNAL_INPUT_AXIS_TVALID = '1') then
                            if(BAND_PASS_FILTER_EN = '1' or GAUSSIAN_FILTER_EN = '1' or ENVELOPE_FILTER_EN = '1') then
                                signal_bram_web   <= (others => '1');
                                signal_bram_dinb  <= (others => '0');
                                signal_bram_addrb <= (others => '0');
                                state_read_fir_signal_sm <= padding_signal_input1;
                            end if;
                        elsif(provide_gates_flag = '1') then
                            signal_bram_web   <= (others => '0'); 
                            fir_corr_dsp_sclr <= '1';
                            signal_bram_addrb <= (others => '0');
                            state_read_fir_signal_sm <= read_gates;
                        elsif(provide_correlation_flag = '1') then
                            signal_bram_web   <= (others => '0'); 
                            fir_corr_dsp_sclr_first <= '1';
                            signal_bram_addrb           <= std_logic_vector(resize(unsigned(CORRELATION_GATE_SAMPLE_START), signal_bram_addrb'length));
                            correlation_addb_offset_end <= resize(unsigned(CORRELATION_GATE_SAMPLE_END), signal_bram_addrb'length);
                            state_read_fir_signal_sm <= read_correlation_gate;
                        elsif(provide_fir_result_flag = '1') then 
                            signal_bram_web   <= (others => '0'); 
                            fir_corr_dsp_sclr <= '1';
                            signal_bram_addrb <= (others => '0');
                            var_counter_result := 0;
                            state_read_fir_signal_sm <= read_result;
                        else
                            signal_bram_web   <= (others => '0'); 
                            fir_corr_dsp_sclr <= '1'; 
                        end if;
                    when read_fir_filtering_signal =>
                        if(fir_filtering_flag = '1') then
                            if(fir_corr_dsp_sclr_first = '1')  then
                                fir_corr_dsp_sclr_first <= '0';
                                fir_corr_dsp_sclr <= '0';
                            end if;
                            if(unsigned(signal_bram_addrb) >= (data_window_int+N_TAPS-1)) then
                                fir_filtering_end <= '1';
                            else
                                if(unsigned(signal_bram_addrb) >= window_oversampling_offset_end) then
                                    window_oversampling_offset <= window_oversampling_offset + WINDOW_OVERSAMPLING_STEP;
                                    signal_bram_addrb <= std_logic_vector(window_oversampling_offset + WINDOW_OVERSAMPLING_STEP);
                                    window_oversampling_offset_end <= window_oversampling_offset + WINDOW_OVERSAMPLING_STEP + WINDOW_OVERSAMPLING - 1;
                                    fir_corr_dsp_sclr <= '1';
                                else
                                    signal_bram_addrb <= std_logic_vector(unsigned(signal_bram_addrb)+1);
                                    fir_corr_dsp_sclr <= '0';
                                end if; 
                            end if;                 
                        else
                            fir_filtering_end <= '0';
                            state_read_fir_signal_sm <= idle;
                        end if;
                    when padding_signal_input1 =>
                        if(SIGNAL_INPUT_AXIS_TVALID = '1') then
                            if(unsigned(signal_bram_addrb) < N_HALF_TAPS-2) then
                                signal_bram_addrb <= std_logic_vector(unsigned(signal_bram_addrb)+1);
                            else
                                signal_bram_addrb <= std_logic_vector(unsigned(signal_bram_addrb)+
                                                                      resize(unsigned(DATA_WINDOW), signal_bram_addrb'length)+
                                                                      1);
                                state_read_fir_signal_sm <= padding_signal_input2;
                            end if;
                        end if;
                    when padding_signal_input2 =>
                        if(SIGNAL_INPUT_AXIS_TVALID = '1') then
                            if(unsigned(signal_bram_addrb) < (unsigned(DATA_WINDOW)+N_TAPS-2)) then
                                signal_bram_addrb <= std_logic_vector(unsigned(signal_bram_addrb)+1);
                            end if;
                        else
                            if(signal_input_axis_tlast_i(1) = '1') then
                                state_read_fir_signal_sm   <= idle;
                            end if;
                        end if;
                    when read_gates =>
                        if(provide_gates_flag = '1') then
                            if(GATES_AXIS_TREADY = '1') then
                                if(unsigned(signal_bram_addrb) >= unsigned(GATES_SAMPLE_END)) then
                                    gates_axis_tvalid_i(1)  <= '0';
                                    gates_axis_tlast_i(1)   <= '0';
                                    provide_gates_end       <= '1';
                                else
                                    gates_axis_tvalid_i(1) <= '1';
                                    if(read_gates_first = '1') then
                                        read_gates_first  <= '0';
                                        signal_bram_addrb <= std_logic_vector(resize(unsigned(GATES_SAMPLE_START), signal_bram_addrb'length));
                                    else
                                        signal_bram_addrb <= std_logic_vector(unsigned(signal_bram_addrb)+1); 
                                    end if;
                                    if(unsigned(signal_bram_addrb) >= (unsigned(GATES_SAMPLE_END)-1)) then
                                        gates_axis_tlast_i(1) <= '1';
                                    end if;                                  
                                end if;
                            else
                                gates_axis_tvalid_i(1) <= '0';
                            end if;  
                        else
                            provide_gates_end <= '0';
                            read_gates_first  <= '1';
                            state_read_fir_signal_sm <= idle;
                        end if;
                    when read_result =>
                        if(provide_fir_result_flag = '1') then
                            if(FIR_RESULT_AXIS_TREADY = '1') then
                                if(var_counter_result >= (data_window_half_int)) then
                                    fir_result_axis_tvalid_i(1)  <= '0';
                                    fir_result_axis_tlast_i(1)   <= '0';
                                    provide_fir_result_end  <= '1';
                                else
                                    fir_result_axis_tvalid_i(1) <= '1';
                                    if(read_fir_result_first = '1') then
                                        read_fir_result_first <= '0';
                                        signal_bram_addrb <= (others => '0');
                                    else
                                        signal_bram_addrb <= std_logic_vector(unsigned(signal_bram_addrb)+2); 
                                    end if;
                                    if(var_counter_result >= (data_window_half_int-1)) then
                                        fir_result_axis_tlast_i(1) <= '1';
                                    end if;
                                    var_counter_result := var_counter_result + 1;
                                end if;
							else
								fir_result_axis_tvalid_i(1)  <= '0';														 
                            end if;  
                        else
                            provide_fir_result_end <= '0';
                            read_fir_result_first  <= '1';
                            state_read_fir_signal_sm <= idle;
                        end if;
                    when read_correlation_gate =>
                        if(provide_correlation_flag = '1') then
                            if(CORRELATION_AXIS_TREADY = '1') then
                                if(fir_corr_dsp_sclr_first = '1')  then
                                    fir_corr_dsp_sclr_first <= '0';
                                    fir_corr_dsp_sclr <= '0';
                                end if;
                                if(unsigned(signal_bram_addrb) >= correlation_addb_offset_end and provide_correlation_ending = '0') then
                                    signal_bram_addrb <= std_logic_vector(resize(unsigned(CORRELATION_GATE_SAMPLE_START), signal_bram_addrb'length));
                                    if(correlation_addb_offset_end = (unsigned(CORRELATION_GATE_SAMPLE_START) + N_DSP-1)) then
                                        provide_correlation_ending <= '1';
                                        correlation_addb_offset_end <= resize(unsigned(CORRELATION_GATE_SAMPLE_START), correlation_addb_offset_end'length);
                                    elsif(correlation_addb_offset_end > (unsigned(CORRELATION_GATE_SAMPLE_START) + N_DSP-1)) then
                                        correlation_addb_offset_end <= correlation_addb_offset_end - N_DSP;
                                    else
                                        correlation_addb_offset_end <= resize(unsigned(CORRELATION_GATE_SAMPLE_START), correlation_addb_offset_end'length);
                                        provide_correlation_ending <= '1';
                                    end if;
                                    var_counter_wait := 0;
                                    state_read_fir_signal_sm <= read_correlation_gate_wait;
                                else
                                    if(unsigned(signal_bram_addrb) >= correlation_addb_offset_end) then
                                        fir_corr_dsp_sclr <= '1';   
                                        provide_correlation_end <= '1';     
                                    else
                                        fir_corr_dsp_sclr <= '0';
                                        signal_bram_addrb <= std_logic_vector(unsigned(signal_bram_addrb)+1);
                                    end if;
                                end if;
                            else
                                fir_corr_dsp_sclr <= '1';   
                                provide_correlation_end <= '1';
                            end if;
                        else
                            provide_correlation_end <= '0';
                            provide_correlation_ending <= '0';
                            state_read_fir_signal_sm <= idle;
                        end if; 
                    when read_correlation_gate_wait =>
                        --if(counter_wait >= N_DSP-1+DSP_DELAY) then -- N_DSP+DSP_DELAY-1
                        if(var_counter_wait >= N_DSP-1) then -- N_DSP+DSP_DELAY-1
                            --fir_corr_dsp_sclr <= '0';
                            fir_corr_dsp_ce <= '1';
                            fir_corr_dsp_sclr <= '1';
                            state_read_fir_signal_sm <= read_correlation_gate;
                        else
                            if(var_counter_wait >= 3) then -- dsp  delay output -1
                                --fir_corr_dsp_sclr <= '1';
                            end if;
                            if(var_counter_wait >= 3) then -- dsp  delay output -1
                                fir_corr_dsp_ce <= '0';
                            end if;
                            var_counter_wait := var_counter_wait + 1;
                        end if;                
                    when others => 
                        null;
                end case;     
            end if;
        end if;
    end process;
       
    -- Update TAPS
    write_taps_BRAM: process(aclk) 
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                fir_taps_bram_ena   <= '1';
                fir_taps_bram_wea   <= (others => '0');
                fir_taps_bram_addra <= (others => '0');
                fir_taps_bram_dina  <= (others => '0');
                fir_taps_axis_tlast_i <= (others => '0');
                fir_taps_axis_tvalid_first <= '1';
            else
                fir_taps_axis_tlast_i(0) <= FIR_TAPS_AXIS_TLAST;
                fir_taps_axis_tlast_i(1) <= fir_taps_axis_tlast_i(0);
                if(FIR_TAPS_AXIS_TVALID = '1') then
                    fir_taps_bram_wea   <= (others => '1');
                    fir_taps_bram_dina  <= FIR_TAPS_AXIS_TDATA;                   
                    if(fir_taps_axis_tvalid_first = '1') then
                        fir_taps_bram_addra <= (others => '0');
                        fir_taps_axis_tvalid_first <= '0';
                    else
                        fir_taps_bram_addra <= std_logic_vector(unsigned(fir_taps_bram_addra)+1);
                    end if;
                else
                    if(fir_taps_axis_tlast_i(0) = '0' and fir_taps_axis_tlast_i(1) = '1') then
                        fir_taps_bram_wea   <= (others => '0');
                        fir_taps_axis_tvalid_first <= '1';
                    end if;
                end if;
            end if;
        end if;
    end process;
    
    -- Read TAPs to filtering
    read_taps_BRAM: process(aclk) 
        variable var_wait_counter  : integer range 0 to 16383 :=0;
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                fir_taps_bram_enb   <= '1';
                fir_taps_bram_web   <= (others => '0');
                fir_taps_bram_addrb <= (others => '0');
                fir_taps_bram_dinb  <= (others => '0');
                read_fir_taps_first <= '1';
            else
                if(fir_filtering_flag = '1') then
                    if(read_fir_taps_first = '1') then
                        fir_taps_bram_addrb <= fir_taps_offset;
                        read_fir_taps_first <= '0';       
                        var_wait_counter := 0;
                    else      
                        if(unsigned(fir_taps_bram_addrb) >= (unsigned(fir_taps_offset)+(N_TAPS-1))) then
                            if(var_wait_counter >= WINDOW_OVERSAMPLING_STEP-1) then
                                fir_taps_bram_addrb <= fir_taps_offset;  
                                var_wait_counter := 0;   
                            else
                                var_wait_counter := var_wait_counter + 1;
                            end if; 
                        else
                            fir_taps_bram_addrb <= std_logic_vector(unsigned(fir_taps_bram_addrb)+1);
                        end if;
                    end if;
                else
                    read_fir_taps_first <= '1';
                end if;
            end if;
        end if;
    end process;
   
    --fir_taps(0) <= fir_taps_bram_doutb;
    fir_taps(0) <= std_logic_vector(resize(signed(signal_bram_doutb), fir_taps(0)'length)) when state_general_sm = provide_correlation  else
                   fir_taps_bram_doutb;
                        
    -- select fir corr
    --fir_corr_signal <= signal_bram_doutb;
    fir_corr_signal <= signal_bram_douta  when state_general_sm = provide_correlation else
                       rectified_signal_bram_doutb when low_pass_filter_flag = '1' else
                       signal_bram_doutb;
                
    -- Rectify signal for envelope
    rectified_signal_bram_doutb <= std_logic_vector(not signed(signal_bram_doutb) + 1) when signal_bram_doutb(13) = '1' else
                                   signal_bram_doutb;                               
                    
    -- Delay taps     
    fir_taps_process : for i in 1 to N_DSP-1 generate
        process(aclk) 
        begin
           if (rising_edge(aclk)) then
                if (aresetn = '0') then
                    fir_taps(i) <= (others => '0');
                else
                    fir_taps(i) <= fir_taps(i-1);
                end if;
           end if;
        end process;
    end generate;
    
    -- DSP mult
    fir_correlation_DSPs: for i in 0 to N_DSP-1 generate
        fir_correlation_DSP_inst: fir_correlation_DSP
            port map (
            CLK => aclk,  
            CE => fir_corr_dsp_ce_array(i),
            SCLR => fir_corr_dsp_sclr_array(i),
            A   => fir_corr_signal,
            B   => fir_taps(i),     
            P   => dsp_48_p(i)
    );
    end generate;
    
    process(aclk) 
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                fir_corr_dsp_sclr_array(0) <= '0';
                fir_corr_dsp_ce_array(0)   <= '1';
            else
                fir_corr_dsp_sclr_array(0) <= fir_corr_dsp_sclr;
                fir_corr_dsp_ce_array(0)   <= fir_corr_dsp_ce;
            end if;
        end if;
    end process;
        
    -- Delay ACC BYPASS
    fir_corr_DSP_SCLR_array_process : for i in 1 to N_DSP-1 generate
        process(aclk) 
        begin
           if (rising_edge(aclk)) then
                if (aresetn = '0') then
                    fir_corr_dsp_sclr_array(i) <= '0';
                    fir_corr_dsp_ce_array(i) <= '0';
                else
                    fir_corr_dsp_sclr_array(i) <= fir_corr_dsp_sclr_array(i-1);
                    fir_corr_dsp_ce_array(i) <= fir_corr_dsp_ce_array(i-1) and fir_corr_dsp_ce;  
                end if;
           end if;
        end process;
    end generate;
                      
    -- FIR TAPS BRAM
    fir_correlation_taps_BRAM_inst : fir_correlation_taps_BRAM
        port map (
            clka => aclk,
            ena => fir_taps_bram_ena,
            wea => fir_taps_bram_wea,
            addra => fir_taps_bram_addra,
            dina => fir_taps_bram_dina,
            douta => fir_taps_bram_douta,
            clkb => aclk,
            enb => fir_taps_bram_enb,
            web => fir_taps_bram_web,
            addrb => fir_taps_bram_addrb,
            dinb => fir_taps_bram_dinb,
            doutb => fir_taps_bram_doutb
        );
    
    -- FIR SIGNAL BRAM
    fir_correlation_signal_BRAM_inst : fir_correlation_signal_BRAM
        port map (
            clka => aclk,
            ena => signal_bram_ena,
            wea => signal_bram_wea,
            addra => signal_bram_addra,
            dina => signal_bram_dina,
            douta => signal_bram_douta,
            clkb => aclk,
            enb => signal_bram_enb,
            web => signal_bram_web,
            addrb => signal_bram_addrb(15 downto 0),
            dinb => signal_bram_dinb,
            doutb => signal_bram_doutb
        );

end Behavioral;
