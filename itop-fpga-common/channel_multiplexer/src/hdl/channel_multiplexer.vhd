----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 12/12/2017 10:07:23 AM
-- Design Name: 
-- Module Name: channel multiplexer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity channel_multiplexer is
	generic (
	    N_CHANNELS		: INTEGER := 2;
        REGISTER_W16    : INTEGER := 16
	);
	port (
	    -- Sync
	    aclk         : in STD_LOGIC;
        aresetn      : in STD_LOGIC;
        
        -- Control
        CHANNEL_MULTIPLEXER_EN : in STD_LOGIC;
        
        -- Tready
        TREADY_IN  : in STD_LOGIC_VECTOR(N_CHANNELS-1 downto 0);
        TREADY_OUT  : out STD_LOGIC_VECTOR(N_CHANNELS-1 downto 0)
	);
end channel_multiplexer;

architecture arch_imp of channel_multiplexer is

    -- Aux
    signal tready_in_i  : STD_LOGIC_VECTOR(N_CHANNELS-1 downto 0);
    signal tready_out_i : STD_LOGIC_VECTOR(N_CHANNELS-1 downto 0);
    
    signal channel_selected : INTEGER range 0 to 16383;
     
    -- Priority
    type priority_array is array ( 0 to N_CHANNELS-1 ) of UNSIGNED(REGISTER_W16-1 downto 0);
    signal channel_priority : priority_array;
    
    signal priority_max : UNSIGNED(REGISTER_W16-1 downto 0);
    
    signal tready_first : STD_LOGIC;
		
    -- Channel multipler state machine
    type states_channel_multiplexer_sm is (idle, check_tready_in, select_channel, mux_end); 

    signal state_channel_multiplexer_sm : states_channel_multiplexer_sm;
     
begin

    -- Mask TREADY_OUT with tready_out_i(temporal)
    TREADY_OUT <= (tready_out_i and TREADY_IN);

	-- State machine for channel multiplexer
    channel_multiplexer_sm: process(aclk, aresetn) 
		variable var_channel_counter : integer range 0 to 16383 :=0;																			 
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then	
                -- Initialize neccesary signals 												 														 
				tready_out_i <= (others => '0');
				priority_max <= (others => '0');
				channel_priority <= (others => (others => '0'));																 
				-- state
				state_channel_multiplexer_sm <= idle;
        	else														 
				case state_channel_multiplexer_sm is
					when idle =>
					    if(CHANNEL_MULTIPLEXER_EN = '1') then
                            tready_out_i <= (others => '0');												 
                            if(unsigned(TREADY_IN) > 0) then
                                tready_in_i <= TREADY_IN;
                                var_channel_counter := 0;
                                tready_first <= '1';
                                state_channel_multiplexer_sm <= check_tready_in;  	
                            else
                                channel_priority <= (others => (others => '0'));	   									 
                            end if;	
                        else
                            tready_out_i <= (others => '1');
                        end if; 
					when check_tready_in =>
					    if(var_channel_counter >= N_CHANNELS) then  
					        state_channel_multiplexer_sm <= select_channel;
					    else
					        if(tready_in_i(var_channel_counter) = '1') then
					            -- Set as channel selected the first with tready_in activated
					            if(tready_first = '1') then
					                tready_first <= '0';
                                    priority_max <= channel_priority(var_channel_counter);
                                    channel_selected <= var_channel_counter;
                                else
                                    -- Check if channel priority is bigger than the temporal maximum one
                                    if(channel_priority(var_channel_counter) > priority_max) then
                                        priority_max <= channel_priority(var_channel_counter);
                                        channel_selected <= var_channel_counter;     
                                    else
                                        -- Otherwise update current channel priority
                                        channel_priority(var_channel_counter) <= channel_priority(var_channel_counter) + 1;    
                                    end if;
                                end if;
					        end if;
					        var_channel_counter := var_channel_counter + 1;  
					    end if;				  												 											 
					when select_channel =>
					   -- If any channel has been selected
					   if(tready_first = '0') then
					       tready_out_i(channel_selected) <= '1';
					       channel_priority(channel_selected) <= (others => '0');
					       state_channel_multiplexer_sm <= mux_end;
					   else
					       -- Otherwise go to idle to start again
					       state_channel_multiplexer_sm <= idle;
					   end if;														 																																											
					when mux_end =>
					   -- Wait until tready of channel selected goes to '0'
					   if(TREADY_IN(channel_selected) = '0') then
					       state_channel_multiplexer_sm <= idle;
					   end if;																														
					when others =>
						null;
				end case;
			end if;
         end if;
    end process;
    					      									 	   			                 
end arch_imp;
