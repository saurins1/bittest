library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity channel_multiplexer_tb is
  	generic (
        N_CHANNELS      : INTEGER := 2;
        REGISTER_W16    : INTEGER := 16
    );
end;

architecture bench of channel_multiplexer_tb is

  component channel_multiplexer
  	generic (
  	    N_CHANNELS		: INTEGER := 2;
        REGISTER_W16    : INTEGER := 16
  	);
  	port (
  	    aclk         : in STD_LOGIC;
        aresetn      : in STD_LOGIC;
        CHANNEL_MULTIPLEXER_EN : in STD_LOGIC;
        TREADY_IN  : in STD_LOGIC_VECTOR(N_CHANNELS-1 downto 0);
        TREADY_OUT  : out STD_LOGIC_VECTOR(N_CHANNELS-1 downto 0)
  	);
  end component;

  signal aclk: STD_LOGIC:= '0';
  signal aresetn: STD_LOGIC:= '0';
  signal CHANNEL_MULTIPLEXER_EN: STD_LOGIC := '0';
  signal TREADY_IN: STD_LOGIC_VECTOR(N_CHANNELS-1 downto 0):= (others => '0');
  signal TREADY_OUT: STD_LOGIC_VECTOR(N_CHANNELS-1 downto 0) ;
  
  signal CHANNEL_MULTIPLEXER_EN_i: STD_LOGIC := '0';

  constant aclk_period: time := 10 ns;
  
  type states_tready_in_sm is (idle, wait_tready_out_on_channel, wait_tready_out_off_channel); 
  signal state_tready_in_sm : states_tready_in_sm; 
  
  signal channel_tready_on : INTEGER range 0 to 16383;

begin

	-- aclk process 
    aclk_process :process
    begin
        aclk <= not aclk;
        wait for aclk_period/2;
        aclk <= not aclk;
        wait for aclk_period/2;
    end process;

  -- Insert values for generic parameters !!
  uut: channel_multiplexer generic map ( N_CHANNELS             => N_CHANNELS,
                                         REGISTER_W16           => REGISTER_W16)
                              port map ( aclk                   => aclk,
                                         aresetn                => aresetn,
                                         CHANNEL_MULTIPLEXER_EN => CHANNEL_MULTIPLEXER_EN,
                                         TREADY_IN              => TREADY_IN,
                                         TREADY_OUT             => TREADY_OUT );

	stimulus: process
	begin
		aresetn <= '0';
		wait for aclk_period*10;
		CHANNEL_MULTIPLEXER_EN_i <= '0';
		--CHANNEL_MULTIPLEXER_EN_i <= '1';
		aresetn <= '1';
		wait;
	end process;
	
	CHANNEL_MULTIPLEXER_EN <= CHANNEL_MULTIPLEXER_EN_i;

    tready_inst: process(aclk, aresetn)  
        variable my_counter  : integer range 0 to 16383 :=0;
        variable wait_counter  : integer range 0 to 16383 :=0;
    begin 
        if (aresetn = '0') then
			TREADY_IN <= (others => '0');
			my_counter := 0;	
			wait_counter := 0;													  	
            state_tready_in_sm <= idle;  
        elsif (aclk'event and aclk = '1') then
            case state_tready_in_sm is  
                when idle =>       
                    if(wait_counter >= 32) then
                        my_counter := 0; 
                        wait_counter := 0; 
                        TREADY_IN <= (others => '1');   
                        state_tready_in_sm <= wait_tready_out_on_channel; 
                    else
                        TREADY_IN <= (others => '0');
                        wait_counter := wait_counter + 1;
                    end if;    
                when wait_tready_out_on_channel =>               
                    if(UNSIGNED(TREADY_OUT) > 0) then
                        wait_counter := 0;
                        if(TREADY_OUT(my_counter) = '1') then
                            channel_tready_on <= my_counter; 
                            state_tready_in_sm <= wait_tready_out_off_channel;    
                        else
                            my_counter := my_counter + 1;
                        end if;
                    else
                        if(wait_counter >= 32) then 
                            state_tready_in_sm <= idle; 
                        else
                            wait_counter := wait_counter + 1;
                        end if;       
                    end if;
                when wait_tready_out_off_channel =>
                    if(wait_counter >= 32) then 
                        TREADY_IN(channel_tready_on) <= '0';
                        if(TREADY_OUT(channel_tready_on) = '0') then
                            state_tready_in_sm <= wait_tready_out_on_channel; 
                        end if;
                    else
                        wait_counter := wait_counter + 1;
                    end if;													
                when others =>
                    null;                   
            end case;                                                                                            
        end if;
    end process; 
    
end;