library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity median_filter is
	generic (
        -- Users to add parameters here
        REGISTER_WIDTH              : integer := 32;
        COUNTER_WAIT                : integer    := 4;
        STATE                       : integer    := 4;
        MIN_MEDIAN_LEVEL            : integer    := 3;
        -- User parameters ends
        -- Do not modify the parameters beyond this line
        -- Parameters of Axi Slave Bus Interface S00_AXIS
        C_S00_AXIS_TDATA_WIDTH    : integer    := 16;
    
        -- Parameters of Axi Master Bus Interface M00_AXIS
        C_M00_AXIS_TDATA_WIDTH    : integer    := 16;
        C_M00_AXIS_START_COUNT    : integer    := 32
    );
	port (
		-- Users to add ports here
        DATA_WINDOW         : in std_logic_vector(REGISTER_WIDTH-1 downto 0);
        FILTERING           : in std_logic; 
        MEDIAN_LEVEL        : in std_logic_vector(REGISTER_WIDTH-1 downto 0);
        MEDIAN_READ         : in std_logic; -- 1= read median result
        MEDIAN_READ_MODE    : in std_logic; -- 0=output automatic / 1=nedded read median = '1'
        MEDIAN_STATE        : out std_logic_vector(STATE-1 downto 0);        
        -- 0000 idle
        -- 0001 frame request
        -- 0010 accumulating frame
        -- 0100 wait for read
        -- 0101 reading
        -- 0110 finish read
        
        -- User ports ends
        -- Do not modify the ports beyond this line

		-- Ports of Axi Slave Bus Interface S00_AXIS
		s00_axis_aclk	: in std_logic;
		s00_axis_aresetn	: in std_logic;
		s00_axis_tready	: out std_logic;
		s00_axis_tdata	: in std_logic_vector(C_S00_AXIS_TDATA_WIDTH-1 downto 0);
		s00_axis_tstrb	: in std_logic_vector((C_S00_AXIS_TDATA_WIDTH/8)-1 downto 0);
		s00_axis_tlast	: in std_logic;
		s00_axis_tvalid	: in std_logic;

		-- Ports of Axi Master Bus Interface M00_AXIS
		m00_axis_aclk	: in std_logic;
		m00_axis_aresetn	: in std_logic;
		m00_axis_tvalid	: out std_logic;
		m00_axis_tdata	: out std_logic_vector(C_M00_AXIS_TDATA_WIDTH-1 downto 0);
		m00_axis_tstrb	: out std_logic_vector((C_M00_AXIS_TDATA_WIDTH/8)-1 downto 0);
		m00_axis_tlast	: out std_logic;
		m00_axis_tready	: in std_logic
	);
end median_filter;

architecture arch_imp of median_filter is 

    -- aux 
    signal MEDIAN_LEVEL_i	    : std_logic_vector(REGISTER_WIDTH-1 downto 0);
    
    -- Delayed input signals
    signal s00_axis_tdata_i1    : std_logic_vector(C_S00_AXIS_TDATA_WIDTH-1 downto 0);
    signal s00_axis_tlast_i1    : std_logic;
    signal s00_axis_tvalid_i1   : std_logic;
    
    signal s00_axis_tdata_i2    : std_logic_vector(C_S00_AXIS_TDATA_WIDTH-1 downto 0);
    signal s00_axis_tlast_i2    : std_logic;
    signal s00_axis_tvalid_i2   : std_logic;
    
    signal s00_axis_tdata_i3    : std_logic_vector(C_S00_AXIS_TDATA_WIDTH-1 downto 0);
    signal s00_axis_tlast_i3    : std_logic;
    signal s00_axis_tvalid_i3   : std_logic;
    
    signal s00_axis_tlast_i4   : std_logic;
    signal s00_axis_tvalid_i4   : std_logic;
    
    signal s00_axis_tlast_i5    : std_logic;
    signal s00_axis_tvalid_i5   : std_logic;
    
    -- Median signals
    signal MEDIAN_tdata    : std_logic_vector(C_S00_AXIS_TDATA_WIDTH-1 downto 0);
    signal MEDIAN_tlast    : std_logic;
    signal MEDIAN_tvalid   : std_logic;
    
    signal MEDIAN_tlast_i1    : std_logic;
    signal MEDIAN_tvalid_i1   : std_logic;
    
    signal MEDIAN_tlast_i2    : std_logic;
    signal MEDIAN_tvalid_i2   : std_logic;
    
    signal MEDIAN_LEVEL_counter  : unsigned(REGISTER_WIDTH-1 downto 0);
    
    -- BRAM      
    COMPONENT median_filter_BRAM
      PORT (
        clka : IN STD_LOGIC;
        ena : IN STD_LOGIC;
        wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addra : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        dina : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
        douta : OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
        clkb : IN STD_LOGIC;
        enb : IN STD_LOGIC;
        web : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addrb : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        dinb : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
        doutb : OUT STD_LOGIC_VECTOR(13 DOWNTO 0)
      );
    END COMPONENT;
    
    type BRAM_d is array ( 0 to 3 ) of STD_LOGIC_VECTOR(13 downto 0);
   
    -- PORTA - write
    signal BRAM_PORTA_addr : STD_LOGIC_VECTOR(15 downto 0);
    signal BRAM_PORTA_addr_counter : UNSIGNED(15 downto 0);
    signal BRAM_PORTA_din : STD_LOGIC_VECTOR(13 downto 0);
    signal BRAM_PORTA_dout : BRAM_d; 
    signal BRAM_PORTA_we : STD_LOGIC_VECTOR ( 0 to 0 );
    signal BRAM_PORTA_first_write : STD_LOGIC;
    signal BRAM_PORTA_en  : STD_LOGIC_VECTOR(3 downto 0);
    
    -- PORTB - read
    signal BRAM_PORTB_addr : STD_LOGIC_VECTOR(15 downto 0);
    signal BRAM_PORTB_addr_counter : UNSIGNED(15 downto 0);
    signal BRAM_PORTB_din : STD_LOGIC_VECTOR(13 downto 0); 
    signal BRAM_PORTB_dout : BRAM_d; 
    signal BRAM_PORTB_en : STD_LOGIC;
    signal BRAM_PORTB_we : STD_LOGIC_VECTOR ( 0 to 0 );
    signal BRAM_PORTB_first_read : STD_LOGIC;
    
    -- Median state machine
    type states_med is (idle, write_med_first, write_read_med, read_result, read_result_end); 
    signal state_med : states_med;	

begin

    BRAM_PORTA_addr <= std_logic_vector(BRAM_PORTA_addr_counter);
    BRAM_PORTB_addr <= std_logic_vector(BRAM_PORTB_addr_counter);

    -- Median SM
    med_inst: process(s00_axis_aclk, s00_axis_aresetn) 
        variable end_med_counter : integer range 0 to 32 :=0;
    begin 
        if (s00_axis_aresetn = '0') then
            s00_axis_tready         <= '0';
            -- BRAM 
            BRAM_PORTA_en           <= (others => '0');
            BRAM_PORTA_we           <= (others => '1');
            BRAM_PORTA_din          <= (others => '0');
            BRAM_PORTA_addr_counter <= (others => '0');   
            BRAM_PORTA_first_write  <= '1';  
            BRAM_PORTB_en           <= '1';
            BRAM_PORTB_we           <= (others => '0');
            BRAM_PORTB_din          <= (others => '0');
            BRAM_PORTB_addr_counter <= (others => '0');  
            BRAM_PORTB_first_read   <= '1';
            -- Median
            MEDIAN_tvalid            <= '0';
            MEDIAN_tlast             <= '0';
            MEDIAN_tdata             <= (others => '0');  
            MEDIAN_LEVEL_counter    <= (others => '0');    
            -- State
            MEDIAN_STATE <= "0000";
            end_med_counter := 0;
            state_med <= idle;  
        elsif (s00_axis_aclk'event and s00_axis_aclk = '1') then
            case state_med is        
                when idle =>
                    MEDIAN_STATE <= "0000"; -- idle
                    s00_axis_tready <= '1';
                    if(s00_axis_tvalid = '1' and FILTERING = '1') then
                        MEDIAN_LEVEL_i <= MEDIAN_LEVEL; 
                        if(unsigned(MEDIAN_LEVEL) < MIN_MEDIAN_LEVEL) then
                            state_med <= write_read_med;
                        else
                            state_med <= write_med_first;
                        end if;
                    end if; 
                when write_med_first =>
                    s00_axis_tready <= '1';
                    -- write in BRAM first frame
                    if(s00_axis_tvalid_i1 = '1') then
                        MEDIAN_STATE <= "0010"; -- accumulating
                        --BRAM_PORTA_en(to_integer(MEDIAN_LEVEL_counter)) <= '1';
                        BRAM_PORTA_en           <= (others => '0');
                        if(MEDIAN_LEVEL_counter = 0) then
                            BRAM_PORTA_en <= "0001";
                            BRAM_PORTA_din <= s00_axis_tdata_i1(13 downto 0);
                        elsif(MEDIAN_LEVEL_counter = 1) then
                            BRAM_PORTA_en <= "0010";
                            BRAM_PORTA_din <= s00_axis_tdata_i1(13 downto 0);
                        elsif(MEDIAN_LEVEL_counter = 2) then
                            BRAM_PORTA_en <= "0100";
                            BRAM_PORTA_din <= s00_axis_tdata_i1(13 downto 0);
                        elsif(MEDIAN_LEVEL_counter = 3) then
                            BRAM_PORTA_en <= "1000";
                            BRAM_PORTA_din <= s00_axis_tdata_i1(13 downto 0);
                        end if;
                        if(BRAM_PORTA_first_write = '1') then
                            BRAM_PORTA_first_write <= '0';
                        else
                            BRAM_PORTA_addr_counter <= BRAM_PORTA_addr_counter + 1;
                        end if;
                     else
                        MEDIAN_STATE <= "0001"; -- frame request
                        if(s00_axis_tvalid_i2 = '1' or s00_axis_tvalid_i3 = '1') then
                            MEDIAN_STATE <= "0010"; -- accumulating
                        end if;
                        if(s00_axis_tlast_i3 = '1') then
                            BRAM_PORTA_en           <= (others => '0');
                            BRAM_PORTA_addr_counter <= (others => '0');  
                            BRAM_PORTA_first_write <= '1';
                        end if;
                        if(s00_axis_tlast_i5 = '1') then
                            if(MEDIAN_LEVEL_counter >= (unsigned(MEDIAN_LEVEL_i)-2)) then
                                state_med <= write_read_med;
                            else
                                MEDIAN_LEVEL_counter <= MEDIAN_LEVEL_counter + 1;
                            end if;
                        end if;
                     end if;            
                when write_read_med =>
                    s00_axis_tready <= '1';
                    -- read
                    if(s00_axis_tvalid = '1') then
                        if(BRAM_PORTB_first_read = '1') then
                            BRAM_PORTB_first_read <= '0';
                        else
                            BRAM_PORTB_addr_counter <= BRAM_PORTB_addr_counter + 1;
                        end if; 
                    end if;
                    -- write
                    if(s00_axis_tvalid_i3 = '1') then
                        MEDIAN_STATE <= "0010"; -- accumulating
                        BRAM_PORTA_en <= "1000";
                        MEDIAN_tlast <= s00_axis_tlast_i3;
                        MEDIAN_tvalid <= s00_axis_tvalid_i3;
                        if(BRAM_PORTA_first_write = '1') then
                            BRAM_PORTA_first_write <= '0';
                        else
                            BRAM_PORTA_addr_counter <= BRAM_PORTA_addr_counter + 1;  
                        end if;                                       
                        if(unsigned(MEDIAN_LEVEL_i) < MIN_MEDIAN_LEVEL) then
                            BRAM_PORTA_din <= s00_axis_tdata_i3(13 downto 0);
                            MEDIAN_tdata <= s00_axis_tdata_i3;
                        elsif(unsigned(MEDIAN_LEVEL_i) = 3) then
                            if((signed(BRAM_PORTB_dout(0)) >= signed(BRAM_PORTB_dout(1))) and (signed(BRAM_PORTB_dout(0)) <= signed(s00_axis_tdata_i3(13 downto 0)))) then
                                BRAM_PORTA_din <= BRAM_PORTB_dout(0);   
                                MEDIAN_tdata    <= BRAM_PORTB_dout(0)(13) & BRAM_PORTB_dout(0)(13) & BRAM_PORTB_dout(0);                            
                            elsif((signed(BRAM_PORTB_dout(1)) >= signed(BRAM_PORTB_dout(0))) and (signed(BRAM_PORTB_dout(1)) <= signed(s00_axis_tdata_i3(13 downto 0)))) then
                                BRAM_PORTA_din <= BRAM_PORTB_dout(1);   
                                MEDIAN_tdata    <= BRAM_PORTB_dout(1)(13) & BRAM_PORTB_dout(1)(13) & BRAM_PORTB_dout(1);   
                            else
                                BRAM_PORTA_din <= s00_axis_tdata_i3(13 downto 0);   
                                MEDIAN_tdata    <= s00_axis_tdata_i3;  
                            end if;    
                        elsif(unsigned(MEDIAN_LEVEL_i) = 5) then
                              if( ((signed(BRAM_PORTB_dout(0)) >= signed(BRAM_PORTB_dout(1)))  and (signed(BRAM_PORTB_dout(0)) >= signed(BRAM_PORTB_dout(2))) and 
                                  (signed(BRAM_PORTB_dout(0)) <= signed(BRAM_PORTB_dout(3)))  and (signed(BRAM_PORTB_dout(0)) <= signed(s00_axis_tdata_i3(13 downto 0)))) or
                                  
                                  ((signed(BRAM_PORTB_dout(0)) >= signed(BRAM_PORTB_dout(1)))  and (signed(BRAM_PORTB_dout(0)) >= signed(BRAM_PORTB_dout(3))) and 
                                  (signed(BRAM_PORTB_dout(0)) <= signed(BRAM_PORTB_dout(2)))  and (signed(BRAM_PORTB_dout(0)) <= signed(s00_axis_tdata_i3(13 downto 0)))) or
                                  
                                  ((signed(BRAM_PORTB_dout(0)) >= signed(BRAM_PORTB_dout(1)))  and (signed(BRAM_PORTB_dout(0)) >= signed(s00_axis_tdata_i3(13 downto 0))) and 
                                  (signed(BRAM_PORTB_dout(0)) <= signed(BRAM_PORTB_dout(2)))  and (signed(BRAM_PORTB_dout(0)) <= signed(BRAM_PORTB_dout(3)))) or
                                  
                                  ((signed(BRAM_PORTB_dout(0)) >= signed(BRAM_PORTB_dout(2)))  and (signed(BRAM_PORTB_dout(0)) >= signed(BRAM_PORTB_dout(3))) and 
                                   (signed(BRAM_PORTB_dout(0)) <= signed(BRAM_PORTB_dout(1)))  and (signed(BRAM_PORTB_dout(0)) <= signed(s00_axis_tdata_i3(13 downto 0)))) or
                                   
                                  ((signed(BRAM_PORTB_dout(0)) >= signed(BRAM_PORTB_dout(2)))  and (signed(BRAM_PORTB_dout(0)) >= signed(s00_axis_tdata_i3(13 downto 0))) and 
                                  (signed(BRAM_PORTB_dout(0)) <= signed(BRAM_PORTB_dout(1)))  and (signed(BRAM_PORTB_dout(0)) <= signed(BRAM_PORTB_dout(3)))) or
                                  
                                  ((signed(BRAM_PORTB_dout(0)) >= signed(BRAM_PORTB_dout(3)))  and (signed(BRAM_PORTB_dout(0)) >= signed(s00_axis_tdata_i3(13 downto 0))) and 
                                  (signed(BRAM_PORTB_dout(0)) <= signed(BRAM_PORTB_dout(1)))  and (signed(BRAM_PORTB_dout(0)) <= signed(BRAM_PORTB_dout(2))))                               
                                ) then
                                    BRAM_PORTA_din <= BRAM_PORTB_dout(0);   
                                    MEDIAN_tdata    <= BRAM_PORTB_dout(0)(13) & BRAM_PORTB_dout(0)(13) & BRAM_PORTB_dout(0);             
                              elsif( ((signed(BRAM_PORTB_dout(1)) >= signed(BRAM_PORTB_dout(0)))  and (signed(BRAM_PORTB_dout(1)) >= signed(BRAM_PORTB_dout(2))) and 
                                     (signed(BRAM_PORTB_dout(1)) <= signed(BRAM_PORTB_dout(3)))  and (signed(BRAM_PORTB_dout(1)) <= signed(s00_axis_tdata_i3(13 downto 0)))) or
                                     
                                     ((signed(BRAM_PORTB_dout(1)) >= signed(BRAM_PORTB_dout(0)))  and (signed(BRAM_PORTB_dout(1)) >= signed(BRAM_PORTB_dout(3))) and 
                                     (signed(BRAM_PORTB_dout(1)) <= signed(BRAM_PORTB_dout(2)))  and (signed(BRAM_PORTB_dout(1)) <= signed(s00_axis_tdata_i3(13 downto 0)))) or
                                     
                                     ((signed(BRAM_PORTB_dout(1)) >= signed(BRAM_PORTB_dout(0)))  and (signed(BRAM_PORTB_dout(1)) >= signed(s00_axis_tdata_i3(13 downto 0))) and 
                                     (signed(BRAM_PORTB_dout(1)) <= signed(BRAM_PORTB_dout(2)))  and (signed(BRAM_PORTB_dout(1)) <= signed(BRAM_PORTB_dout(3)))) or
                                     
                                     ((signed(BRAM_PORTB_dout(1)) >= signed(BRAM_PORTB_dout(2)))  and (signed(BRAM_PORTB_dout(1)) >= signed(BRAM_PORTB_dout(3))) and 
                                      (signed(BRAM_PORTB_dout(1)) <= signed(BRAM_PORTB_dout(0)))  and (signed(BRAM_PORTB_dout(1)) <= signed(s00_axis_tdata_i3(13 downto 0)))) or
                                      
                                     ((signed(BRAM_PORTB_dout(1)) >= signed(BRAM_PORTB_dout(2)))  and (signed(BRAM_PORTB_dout(1)) >= signed(s00_axis_tdata_i3(13 downto 0))) and 
                                     (signed(BRAM_PORTB_dout(1)) <= signed(BRAM_PORTB_dout(0)))  and (signed(BRAM_PORTB_dout(1)) <= signed(BRAM_PORTB_dout(3)))) or
                                     
                                     ((signed(BRAM_PORTB_dout(1)) >= signed(BRAM_PORTB_dout(3)))  and (signed(BRAM_PORTB_dout(1)) >= signed(s00_axis_tdata_i3(13 downto 0))) and 
                                     (signed(BRAM_PORTB_dout(1)) <= signed(BRAM_PORTB_dout(0)))  and (signed(BRAM_PORTB_dout(1)) <= signed(BRAM_PORTB_dout(2))))                               
                                   ) then
                                    BRAM_PORTA_din <= BRAM_PORTB_dout(1);   
                                    MEDIAN_tdata    <= BRAM_PORTB_dout(1)(13) & BRAM_PORTB_dout(1)(13) & BRAM_PORTB_dout(1); 
                              elsif( ((signed(BRAM_PORTB_dout(2)) >= signed(BRAM_PORTB_dout(0)))  and (signed(BRAM_PORTB_dout(2)) >= signed(BRAM_PORTB_dout(1))) and 
                                      (signed(BRAM_PORTB_dout(2)) <= signed(BRAM_PORTB_dout(3)))  and (signed(BRAM_PORTB_dout(2)) <= signed(s00_axis_tdata_i3(13 downto 0)))) or
                                        
                                     ((signed(BRAM_PORTB_dout(2)) >= signed(BRAM_PORTB_dout(0)))  and (signed(BRAM_PORTB_dout(2)) >= signed(BRAM_PORTB_dout(3))) and 
                                     (signed(BRAM_PORTB_dout(2)) <= signed(BRAM_PORTB_dout(1)))  and (signed(BRAM_PORTB_dout(2)) <= signed(s00_axis_tdata_i3(13 downto 0)))) or
                                        
                                     ((signed(BRAM_PORTB_dout(2)) >= signed(BRAM_PORTB_dout(0)))  and (signed(BRAM_PORTB_dout(2)) >= signed(s00_axis_tdata_i3(13 downto 0))) and 
                                     (signed(BRAM_PORTB_dout(2)) <= signed(BRAM_PORTB_dout(1)))  and (signed(BRAM_PORTB_dout(2)) <= signed(BRAM_PORTB_dout(3)))) or
                                        
                                     ((signed(BRAM_PORTB_dout(2)) >= signed(BRAM_PORTB_dout(1)))  and (signed(BRAM_PORTB_dout(2)) >= signed(BRAM_PORTB_dout(3))) and 
                                      (signed(BRAM_PORTB_dout(2)) <= signed(BRAM_PORTB_dout(0)))  and (signed(BRAM_PORTB_dout(2)) <= signed(s00_axis_tdata_i3(13 downto 0)))) or
                                         
                                     ((signed(BRAM_PORTB_dout(2)) >= signed(BRAM_PORTB_dout(1)))  and (signed(BRAM_PORTB_dout(2)) >= signed(s00_axis_tdata_i3(13 downto 0))) and 
                                     (signed(BRAM_PORTB_dout(2)) <= signed(BRAM_PORTB_dout(0)))  and (signed(BRAM_PORTB_dout(2)) <= signed(BRAM_PORTB_dout(3)))) or
                                        
                                     ((signed(BRAM_PORTB_dout(2)) >= signed(BRAM_PORTB_dout(3)))  and (signed(BRAM_PORTB_dout(2)) >= signed(s00_axis_tdata_i3(13 downto 0))) and 
                                     (signed(BRAM_PORTB_dout(2)) <= signed(BRAM_PORTB_dout(0)))  and (signed(BRAM_PORTB_dout(2)) <= signed(BRAM_PORTB_dout(1))))                               
                                   ) then   
                                    BRAM_PORTA_din <= BRAM_PORTB_dout(2);   
                                    MEDIAN_tdata    <= BRAM_PORTB_dout(2)(13) & BRAM_PORTB_dout(2)(13) & BRAM_PORTB_dout(2);          
                              elsif( ((signed(BRAM_PORTB_dout(3)) >= signed(BRAM_PORTB_dout(0)))  and (signed(BRAM_PORTB_dout(3)) >= signed(BRAM_PORTB_dout(1))) and 
                                     (signed(BRAM_PORTB_dout(3)) <= signed(BRAM_PORTB_dout(2)))  and (signed(BRAM_PORTB_dout(3)) <= signed(s00_axis_tdata_i3(13 downto 0)))) or
                                                 
                                     ((signed(BRAM_PORTB_dout(3)) >= signed(BRAM_PORTB_dout(0)))  and (signed(BRAM_PORTB_dout(3)) >= signed(BRAM_PORTB_dout(2))) and 
                                     (signed(BRAM_PORTB_dout(3)) <= signed(BRAM_PORTB_dout(1)))  and (signed(BRAM_PORTB_dout(3)) <= signed(s00_axis_tdata_i3(13 downto 0)))) or
                                                 
                                     ((signed(BRAM_PORTB_dout(3)) >= signed(BRAM_PORTB_dout(0)))  and (signed(BRAM_PORTB_dout(3)) >= signed(s00_axis_tdata_i3(13 downto 0))) and 
                                     (signed(BRAM_PORTB_dout(3)) <= signed(BRAM_PORTB_dout(1)))  and (signed(BRAM_PORTB_dout(3)) <= signed(BRAM_PORTB_dout(2)))) or
                                                 
                                     ((signed(BRAM_PORTB_dout(3)) >= signed(BRAM_PORTB_dout(1)))  and (signed(BRAM_PORTB_dout(3)) >= signed(BRAM_PORTB_dout(2))) and 
                                     (signed(BRAM_PORTB_dout(3)) <= signed(BRAM_PORTB_dout(0)))  and (signed(BRAM_PORTB_dout(3)) <= signed(s00_axis_tdata_i3(13 downto 0)))) or
                                                  
                                     ((signed(BRAM_PORTB_dout(3)) >= signed(BRAM_PORTB_dout(1)))  and (signed(BRAM_PORTB_dout(3)) >= signed(s00_axis_tdata_i3(13 downto 0))) and 
                                     (signed(BRAM_PORTB_dout(3)) <= signed(BRAM_PORTB_dout(0)))  and (signed(BRAM_PORTB_dout(3)) <= signed(BRAM_PORTB_dout(2)))) or
                                                 
                                     ((signed(BRAM_PORTB_dout(3)) >= signed(BRAM_PORTB_dout(2)))  and (signed(BRAM_PORTB_dout(3)) >= signed(s00_axis_tdata_i3(13 downto 0))) and 
                                     (signed(BRAM_PORTB_dout(3)) <= signed(BRAM_PORTB_dout(0)))  and (signed(BRAM_PORTB_dout(3)) <= signed(BRAM_PORTB_dout(1))))                               
                                   ) then       
                                    BRAM_PORTA_din <= BRAM_PORTB_dout(3);   
                                    MEDIAN_tdata    <= BRAM_PORTB_dout(3)(13) & BRAM_PORTB_dout(3)(13) & BRAM_PORTB_dout(3);   
                              else
                                    BRAM_PORTA_din <= s00_axis_tdata_i3(13 downto 0);   
                                    MEDIAN_tdata    <= s00_axis_tdata_i3; 
                              end if;
                        end if;    
                    else
                        MEDIAN_STATE <= "0001"; -- frame request
                        if(s00_axis_tvalid_i2 = '1' or s00_axis_tvalid_i3 = '1') then
                            MEDIAN_STATE <= "0010"; -- accumulating
                        end if;
                        MEDIAN_tvalid <= '0';
                        MEDIAN_tlast <= '0';
                        if(s00_axis_tlast_i5 = '1') then
                            --BRAM_PORTA_en <= '0';
                            MEDIAN_LEVEL_counter <= (others => '0'); 
                            s00_axis_tready <= '0';
                            BRAM_PORTA_first_write <= '1';
                            BRAM_PORTB_first_read <= '1';
                            BRAM_PORTA_addr_counter <= (others => '0');  
                            BRAM_PORTB_addr_counter <= (others => '0');
                            if(MEDIAN_READ_MODE = '0') then
                                state_med <= idle;
                            else
                                state_med <= read_result;
                            end if;
                        end if;
                    end if;
                when read_result =>
                    MEDIAN_STATE <= "0100"; -- wating for read
                    if(MEDIAN_READ = '1' and m00_axis_tready = '1') then
                        MEDIAN_STATE <= "0101"; -- reading
                        if(BRAM_PORTB_first_read = '1') then
                            BRAM_PORTB_first_read <= '0';
                        else
                            BRAM_PORTB_addr_counter <= BRAM_PORTB_addr_counter + 1;
                        end if; 
                        MEDIAN_tvalid <= '1';
                        if(BRAM_PORTB_addr_counter >= (unsigned(DATA_WINDOW)-2)) then
                            MEDIAN_tlast <= '1';
                        else
                            MEDIAN_tlast <= '0';
                        end if;
                        if(BRAM_PORTB_addr_counter >= (unsigned(DATA_WINDOW)-1)) then
                            MEDIAN_tvalid <= '0';  
                            MEDIAN_tlast <= '0';   
                            state_med <= read_result_end;                   
                        end if;
                    else
                        MEDIAN_tvalid <= '0';
                        MEDIAN_tlast <= '0';
                    end if;
                when read_result_end =>
                    MEDIAN_STATE <= "0110"; -- read finished
                    if(MEDIAN_READ = '0') then
                        state_med <= idle;
                    end if;
                when others =>
                    null;
            end case;
         end if;
    end process;
      
    -- Delayed input
    delayed_inst: process(s00_axis_aclk, s00_axis_aresetn) 
    begin 
        if (s00_axis_aresetn = '0') then
            s00_axis_tvalid_i1  <= '0';
            s00_axis_tlast_i1   <= '0';
            s00_axis_tdata_i1   <= (others => '0');
            s00_axis_tvalid_i2  <= '0';
            s00_axis_tlast_i2   <= '0';
            s00_axis_tdata_i2   <= (others => '0');
            s00_axis_tvalid_i3  <= '0';
            s00_axis_tlast_i3   <= '0';
            s00_axis_tdata_i3   <= (others => '0');
            s00_axis_tlast_i4   <= '0';
            s00_axis_tvalid_i4  <= '0';
            s00_axis_tlast_i5   <= '0';
            s00_axis_tvalid_i5  <= '0';
            MEDIAN_tlast_i1      <= '0';
            MEDIAN_tlast_i2      <= '0';
            MEDIAN_tvalid_i1     <= '0';
            MEDIAN_tvalid_i2     <= '0';
        elsif (s00_axis_aclk'event and s00_axis_aclk = '1') then
            s00_axis_tvalid_i1  <= s00_axis_tvalid;
            s00_axis_tlast_i1   <= s00_axis_tlast;
            s00_axis_tdata_i1   <= s00_axis_tdata;
            s00_axis_tvalid_i2  <= s00_axis_tvalid_i1;
            s00_axis_tlast_i2   <= s00_axis_tlast_i1;
            s00_axis_tdata_i2   <= s00_axis_tdata_i1;
            s00_axis_tvalid_i3  <= s00_axis_tvalid_i2;
            s00_axis_tlast_i3   <= s00_axis_tlast_i2;
            s00_axis_tdata_i3   <= s00_axis_tdata_i2;
            s00_axis_tvalid_i4  <= s00_axis_tvalid_i3;
            s00_axis_tlast_i4   <= s00_axis_tlast_i3;
            s00_axis_tvalid_i5  <= s00_axis_tvalid_i4;
            s00_axis_tlast_i5   <= s00_axis_tlast_i4;
            MEDIAN_tlast_i1      <= MEDIAN_tlast;
            MEDIAN_tlast_i2      <= MEDIAN_tlast_i1;
            MEDIAN_tvalid_i1     <= MEDIAN_tvalid;
            MEDIAN_tvalid_i2     <= MEDIAN_tvalid_i1;
        end if;
    end process;    
    
    -- Stream output (result)
    output_inst: process(s00_axis_aclk, s00_axis_aresetn)
    begin 
        if (s00_axis_aresetn = '0') then
            m00_axis_tvalid <= '0';
            m00_axis_tlast <= '0';
            m00_axis_tdata <= (others => '0');
        elsif (s00_axis_aclk'event and s00_axis_aclk = '1') then
            if(unsigned(MEDIAN_LEVEL) >= MIN_MEDIAN_LEVEL) then
                if(MEDIAN_READ_MODE = '0') then
                    m00_axis_tvalid <= MEDIAN_tvalid;
                    m00_axis_tlast <= MEDIAN_tlast;
                    m00_axis_tdata <= MEDIAN_tdata;
                    if(MEDIAN_tvalid = '0') then
                        m00_axis_tdata <= (others => '0');
                    end if;
                else
                    if( state_med = read_result or state_med= read_result_end) then
                        m00_axis_tvalid <= MEDIAN_tvalid_i2;
                        m00_axis_tlast <= MEDIAN_tlast_i2;
                        m00_axis_tdata <= BRAM_PORTB_dout(3)(13) & BRAM_PORTB_dout(3)(13) & BRAM_PORTB_dout(3);
                        if(MEDIAN_tvalid_i2 = '0') then
                            m00_axis_tdata <= (others => '0');
                        end if;
                    end if; 
                end if;
            else
                if(MEDIAN_READ_MODE = '0') then
                    m00_axis_tvalid <= s00_axis_tvalid;
                    m00_axis_tlast <= s00_axis_tlast;
                    m00_axis_tdata <= s00_axis_tdata;
                    if(s00_axis_tvalid = '0') then
                        m00_axis_tdata <= (others => '0');
                    end if;
                else
                    if( state_med = read_result) then
                        m00_axis_tvalid <= MEDIAN_tvalid;
                        m00_axis_tlast <= MEDIAN_tlast;
                        m00_axis_tdata <= BRAM_PORTB_dout(3)(13) & BRAM_PORTB_dout(3)(13) & BRAM_PORTB_dout(3);
                        if(MEDIAN_tvalid = '0') then
                            m00_axis_tdata <= (others => '0');
                        end if;
                    end if; 
                end if;
            end if;
        end if;
    end process;
    
    -- BRAM
    median_filter_BRAM_x: for i in 0 to 3 generate
      median_filter_BRAM_inst : median_filter_BRAM
        PORT MAP (
          clka => s00_axis_aclk,
          ena => BRAM_PORTA_en(i),
          wea => BRAM_PORTA_we,
          addra => BRAM_PORTA_addr,
          dina => BRAM_PORTA_din,
          douta => BRAM_PORTA_dout(i),
          clkb => s00_axis_aclk,
          enb => BRAM_PORTB_en,
          web => BRAM_PORTB_we,
          addrb => BRAM_PORTB_addr,
          dinb => BRAM_PORTB_din,
          doutb => BRAM_PORTB_dout(i)
        );
    end generate;

end arch_imp;
