----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/10/2017 09:30:59 AM
-- Design Name: 
-- Module Name: AIRBUS_MEDIAN_FILTER_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity median_filter_tb is
  	generic (
    REGISTER_WIDTH              : integer := 32;
    COUNTER_WAIT                : integer    := 4;
    STATE                       : integer    := 4;
    MIN_MEDIAN_LEVEL            : integer    := 3;
    C_S00_AXIS_TDATA_WIDTH    : integer    := 16;
    C_M00_AXIS_TDATA_WIDTH    : integer    := 16;
    C_M00_AXIS_START_COUNT    : integer    := 32
);
end;

architecture bench of median_filter_tb is

  component median_filter
  	generic (
          REGISTER_WIDTH              : integer := 32;
          COUNTER_WAIT                : integer    := 4;
          STATE                       : integer    := 4;
          MIN_MEDIAN_LEVEL            : integer    := 3;
          C_S00_AXIS_TDATA_WIDTH    : integer    := 16;
          C_M00_AXIS_TDATA_WIDTH    : integer    := 16;
          C_M00_AXIS_START_COUNT    : integer    := 32
  	);
  	port (
  	    MEDIAN_LEVEL	    : in std_logic_vector(REGISTER_WIDTH-1 downto 0);
          DATA_WINDOW         : in std_logic_vector(REGISTER_WIDTH-1 downto 0);
          FILTERING           : in std_logic; 
          MEDIAN_READ         : in std_logic;
          MEDIAN_READ_MODE    : in std_logic;
          MEDIAN_STATE        : out std_logic_vector(STATE-1 downto 0);
  		s00_axis_aclk	: in std_logic;
  		s00_axis_aresetn	: in std_logic;
  		s00_axis_tready	: out std_logic;
  		s00_axis_tdata	: in std_logic_vector(C_S00_AXIS_TDATA_WIDTH-1 downto 0);
  		s00_axis_tstrb	: in std_logic_vector((C_S00_AXIS_TDATA_WIDTH/8)-1 downto 0);
  		s00_axis_tlast	: in std_logic;
  		s00_axis_tvalid	: in std_logic;
  		m00_axis_aclk	: in std_logic;
  		m00_axis_aresetn	: in std_logic;
  		m00_axis_tvalid	: out std_logic;
  		m00_axis_tdata	: out std_logic_vector(C_M00_AXIS_TDATA_WIDTH-1 downto 0);
  		m00_axis_tstrb	: out std_logic_vector((C_M00_AXIS_TDATA_WIDTH/8)-1 downto 0);
  		m00_axis_tlast	: out std_logic;
  		m00_axis_tready	: in std_logic
  	);
  end component;

  signal MEDIAN_LEVEL: std_logic_vector(REGISTER_WIDTH-1 downto 0);
  signal DATA_WINDOW: std_logic_vector(REGISTER_WIDTH-1 downto 0);
  signal MEDIAN_READ: std_logic;
  signal FILTERING: std_logic;
  signal MEDIAN_READ_MODE: std_logic;
  signal MEDIAN_STATE: std_logic_vector(STATE-1 downto 0);
  signal s00_axis_tready: std_logic;
  signal s00_axis_tdata: std_logic_vector(C_S00_AXIS_TDATA_WIDTH-1 downto 0);
  signal s00_axis_tstrb: std_logic_vector((C_S00_AXIS_TDATA_WIDTH/8)-1 downto 0);
  signal s00_axis_tlast: std_logic;
  signal s00_axis_tvalid: std_logic;
  signal m00_axis_tvalid: std_logic;
  signal m00_axis_tdata: std_logic_vector(C_M00_AXIS_TDATA_WIDTH-1 downto 0);
  signal m00_axis_tstrb: std_logic_vector((C_M00_AXIS_TDATA_WIDTH/8)-1 downto 0);
  signal m00_axis_tlast: std_logic;
  signal m00_axis_tready: std_logic ;

  signal aclk: STD_LOGIC := '0';
  signal aresetn: STD_LOGIC := '0';
  
  constant aclk_period: time := 10 ns;
  
  -- LOAD DATA
  signal begin_load: STD_LOGIC := '0';
  type states_load is (idle, load_input, load_wait); 
  signal state_load : states_load; 
  
  signal load_counter : unsigned(15 downto 0) := (others => '0'); 
  signal load_counter_offset : unsigned(15 downto 0) := (others => '0'); 
  signal wait_counter : unsigned(15 downto 0) := (others => '0'); 

begin

  -- Insert values for generic parameters !!
  uut: median_filter generic map ( REGISTER_WIDTH         => REGISTER_WIDTH,
                                               COUNTER_WAIT           => COUNTER_WAIT,
                                               STATE                  => STATE,
                                               MIN_MEDIAN_LEVEL       => MIN_MEDIAN_LEVEL,
                                               C_S00_AXIS_TDATA_WIDTH => C_S00_AXIS_TDATA_WIDTH,
                                               C_M00_AXIS_TDATA_WIDTH => C_M00_AXIS_TDATA_WIDTH,
                                               C_M00_AXIS_START_COUNT => C_M00_AXIS_START_COUNT )
                                    port map ( MEDIAN_LEVEL           => MEDIAN_LEVEL,
                                               DATA_WINDOW            => DATA_WINDOW,
                                               FILTERING              => FILTERING,
                                               MEDIAN_READ            => MEDIAN_READ,
                                               MEDIAN_READ_MODE       => MEDIAN_READ_MODE,
                                               MEDIAN_STATE           => MEDIAN_STATE,
                                               s00_axis_aclk          => aclk,
                                               s00_axis_aresetn       => aresetn,
                                               s00_axis_tready        => s00_axis_tready,
                                               s00_axis_tdata         => s00_axis_tdata,
                                               s00_axis_tstrb         => s00_axis_tstrb,
                                               s00_axis_tlast         => s00_axis_tlast,
                                               s00_axis_tvalid        => s00_axis_tvalid,
                                               m00_axis_aclk          => aclk,
                                               m00_axis_aresetn       => aresetn,
                                               m00_axis_tvalid        => m00_axis_tvalid,
                                               m00_axis_tdata         => m00_axis_tdata,
                                               m00_axis_tstrb         => m00_axis_tstrb,
                                               m00_axis_tlast         => m00_axis_tlast,
                                               m00_axis_tready        => m00_axis_tready );

  -- stimulus
  stimulus: process
  begin
    aresetn <= '0';
    MEDIAN_LEVEL <= "00000000000000000000000000000101";
    --MEDIAN_LEVEL <= "00000000000000000000000000000000";
    DATA_WINDOW  <= "00000000000000000000010000000000";
    FILTERING <= '1';
    MEDIAN_READ_MODE <= '0';
    --MEDIAN_READ_MODE <= '1';
    begin_load <= '0';
    wait for aclk_period*10;
    aresetn <= '1';
    wait for aclk_period*10; 
    begin_load <= '1';
    wait for aclk_period*10; 
    begin_load <= '0';
    wait;
  end process;

   -- aclk process 
   aclk_period_process :process
   begin
    aclk <= not aclk;
    wait for aclk_period/2;
    aclk <= not aclk;
    wait for aclk_period/2;
   end process;
   
  load_data_inst_inst: process(aclk, aresetn)                         
    begin 
      if (aresetn = '0') then
          s00_axis_tdata <= (others => '0');
          s00_axis_tlast <= '0';
          s00_axis_tvalid <= '0';
          m00_axis_tready <= '1';
          state_load <= idle;  
          load_counter <= (others => '0');
          load_counter_offset   <= (others => '0');
          wait_counter  <= (others => '0');
      elsif (aclk'event and aclk = '1') then
          case state_load is  
              when idle =>       
                  if(begin_load = '1') then
                      state_load <= load_input;  
                      s00_axis_tdata <= (others => '0');
                      s00_axis_tlast <= '0';
                      s00_axis_tvalid <= '0'; 
                  end if;
              when load_input =>   
                 if(s00_axis_tready = '1') then            
                     s00_axis_tvalid <= '1'; 
                     load_counter <= load_counter + 1;
                     s00_axis_tdata <= std_logic_vector(load_counter);
                     if((load_counter) >=  (unsigned(DATA_WINDOW(15 downto 0)) -1 + load_counter_offset)) then
                         s00_axis_tlast <= '1';
                         state_load <= load_wait;
                     end if;    
                 else
                     s00_axis_tdata <= (others => '0');
                     s00_axis_tlast <= '0';
                     s00_axis_tvalid <= '0';
                 end if;                                                      
              when load_wait =>   
                  s00_axis_tvalid <= '0'; 
                  s00_axis_tlast <= '0';
                  wait_counter <= wait_counter + 1;
                  if(wait_counter >= 127) then
                      wait_counter  <= (others => '0');
                      load_counter_offset <= load_counter_offset + 1; 
                      if(load_counter_offset >= (unsigned(MEDIAN_LEVEL(15 downto 0))-1)) then   
                        load_counter_offset   <= (others => '0'); 
                        load_counter    <= (others => '0');        
                        --state_load <= idle;  
                        state_load <= load_input;  
                      else
                        load_counter <= load_counter_offset + 1;
                        state_load <= load_input; 
                      end if;
                  end if;
              when others =>
                  null;                   
          end case;                                                                                            
      end if;
    end process;  
    
  read_median_process: process(aclk, aresetn)                         
      begin 
        if (aresetn = '0') then
            MEDIAN_READ <= '0';
        elsif (aclk'event and aclk = '1') then
            if(MEDIAN_READ_MODE = '1') then
                if(MEDIAN_STATE = "0100") then
                    MEDIAN_READ <= '1';
                end if;
                if(MEDIAN_STATE = "0110") then
                    MEDIAN_READ <= '0';
                end if;
            end if;
        end if;
    end process;

end;
