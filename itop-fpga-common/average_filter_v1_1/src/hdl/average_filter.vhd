----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 05/01/2018 10:07:23 AM
-- Design Name: 
-- Module Name: average filter light - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity average_filter is
	generic (
        REGISTER_W32        : INTEGER := 32;
        REGISTER_W16        : INTEGER := 16;
        REGISTER_W8         : INTEGER := 8;
        REGISTER_W14        : INTEGER := 14;
        AVG_TYPE_NORMAL     : INTEGER := 0;
        AVG_TYPE_PIPELINE   : INTEGER := 1;
        TRIGGER_WAIT        : INTEGER := 32;
        MIN_AVG_LEVEL       : INTEGER := 2
	);
	port (
	    -- Sync
	    aclk         : in STD_LOGIC;
        aresetn      : in STD_LOGIC;
	
	    -- Control
	    AVG_LEVEL	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);    
	    DATA_WINDOW	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	    SAMPLE_FREQ	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	    DATA_WINDOW_RESULT	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	    SAMPLE_FREQ_RESULT	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	    AVG_COUNTER         : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
		AVG_WINDOW  	    : out STD_LOGIC;
		
		AVG_TYPE	     : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        AVG_RESET        : in STD_LOGIC;
        AVG_RESET_ACK    : out STD_LOGIC;
		AVG_TYPE_RESULT	 : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		AVG_LEVEL_RESULT : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

		-- Signal input
		SIGNAL_INPUT_AXIS_TREADY  : out STD_LOGIC;
		SIGNAL_INPUT_AXIS_TDATA	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		SIGNAL_INPUT_AXIS_TLAST	  : in STD_LOGIC;
		SIGNAL_INPUT_AXIS_TVALID  : in STD_LOGIC;

		-- Average result
		AVG_RESULT_AXIS_TVALID    : out STD_LOGIC;
		AVG_RESULT_AXIS_TDATA	  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		AVG_RESULT_AXIS_TLAST	  : out STD_LOGIC;
		AVG_RESULT_AXIS_TREADY    : in STD_LOGIC
	);
end average_filter;

architecture arch_imp of average_filter is

    -- Delayed signal input
    type signal_input_axis_tdata_array is array ( 0 to 15 ) of STD_LOGIC_VECTOR(13 downto 0);
    signal signal_input_axis_tdata_i    : signal_input_axis_tdata_array;
    signal signal_input_axis_tlast_i    : STD_LOGIC_VECTOR(15 downto 0);
    signal signal_input_axis_tvalid_i   : STD_LOGIC_VECTOR(15 downto 0);
    
    -- Delayed average result
    signal avg_result_axis_tlast_i    : STD_LOGIC_VECTOR(3 downto 0);
    signal avg_result_axis_tvalid_i   : STD_LOGIC_VECTOR(3 downto 0);
  
    -- BRAM
    COMPONENT average_filter_BRAM 
      PORT (
        clka : IN STD_LOGIC;
        ena : IN STD_LOGIC;
        wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addra : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        dina : IN STD_LOGIC_VECTOR(20 DOWNTO 0);
        douta : OUT STD_LOGIC_VECTOR(20 DOWNTO 0);
        clkb : IN STD_LOGIC;
        enb : IN STD_LOGIC;
        web : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addrb : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        dinb : IN STD_LOGIC_VECTOR(20 DOWNTO 0);
        doutb : OUT STD_LOGIC_VECTOR(20 DOWNTO 0)
      );
    END COMPONENT;
    
    -- average BRAM port a
    signal average_bram_addra   : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal average_bram_dina    : STD_LOGIC_VECTOR ( 20 downto 0 );
    signal average_bram_douta   : STD_LOGIC_VECTOR ( 20 downto 0 );
    signal average_bram_ena     : STD_LOGIC;
    signal average_bram_wea     : STD_LOGIC_VECTOR ( 0 to 0 );

	signal average_bram_addra_first : STD_LOGIC;
    
    -- average BRAM port b
    signal average_bram_addrb   : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal average_bram_dinb    : STD_LOGIC_VECTOR ( 20 downto 0 );
    signal average_bram_doutb   : STD_LOGIC_VECTOR ( 20 downto 0 );
    signal average_bram_enb     : STD_LOGIC;
    signal average_bram_web     : STD_LOGIC_VECTOR ( 0 to 0 );

	signal average_bram_addrb_first : STD_LOGIC;
   
    -- Average state machine
    type states_general_sm is (idle, by_pass, first_frame, next_frame, check_frame, read_result,
                               pipeline_acc_first, pipeline_acc, pipeline_read_result, internal_reset); 
    signal state_general_sm : states_general_sm;

    -- BRAM porta state machine
	type states_bram_porta_sm is (idle, first_frame, next_frame, pipeline_next_frame, pipeline_read_result); 
    signal state_bram_porta_sm : states_bram_porta_sm;

    -- BRAM portb state machine
	type states_bram_portb_sm is (idle, read_result, next_frame, pipeline_next_frame, pipeline_read_result); 
    signal state_bram_portb_sm : states_bram_portb_sm;
   
    -- handshake flags between state machines 
    signal first_frame_flag  : STD_LOGIC;
    signal first_frame_end   : STD_LOGIC;

	signal next_frame_flag  : STD_LOGIC;
    signal next_frame_end   : STD_LOGIC;

	signal read_result_flag  : STD_LOGIC;
    signal read_result_end   : STD_LOGIC;
               
    signal pipeline_read_result_flag  : STD_LOGIC;
    signal pipeline_read_result_end   : STD_LOGIC;
	signal pipeline_read_result_end_a : STD_LOGIC;  
	signal pipeline_read_result_end_b : STD_LOGIC;  
    
    signal signal_input_axis_tready_i1 : STD_LOGIC;
    signal signal_input_axis_tready_i2 : STD_LOGIC;
    signal signal_input_axis_tready_i3 : STD_LOGIC;
      
	signal input_output_bypass : STD_LOGIC;
		
	signal avg_counter_i  : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
	
	-- Divider
    COMPONENT average_filter_divider_24_8
      PORT (
        aclk : IN STD_LOGIC;
        aclken : IN STD_LOGIC;
        aresetn : IN STD_LOGIC;
        s_axis_divisor_tvalid : IN STD_LOGIC;
        s_axis_divisor_tlast : IN STD_LOGIC;
        s_axis_divisor_tdata : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        s_axis_dividend_tvalid : IN STD_LOGIC;
        s_axis_dividend_tlast : IN STD_LOGIC;
        s_axis_dividend_tdata : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
        m_axis_dout_tvalid : OUT STD_LOGIC;
        m_axis_dout_tlast : OUT STD_LOGIC;
        m_axis_dout_tdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
      );
    END COMPONENT;
    
    signal s_axis_divisor_tvalid : STD_LOGIC;
    signal s_axis_divisor_tlast : STD_LOGIC;
    signal s_axis_divisor_tdata : STD_LOGIC_VECTOR(7 DOWNTO 0);
    signal s_axis_dividend_tvalid : STD_LOGIC;
    signal s_axis_dividend_tlast : STD_LOGIC;
    signal s_axis_dividend_tdata : STD_LOGIC_VECTOR(23 DOWNTO 0);
    signal m_axis_dout_tvalid : STD_LOGIC;
    signal m_axis_dout_tlast : STD_LOGIC;
    signal m_axis_dout_tdata : STD_LOGIC_VECTOR(31 DOWNTO 0);
    
    signal avg_counter_divisor  : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);

	-- Multiplier
	COMPONENT average_filter_multiplier_16_8
	  PORT (
		CLK : IN STD_LOGIC;
		A : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
		B : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
		P : OUT STD_LOGIC_VECTOR(23 DOWNTO 0)
	  );
	END COMPONENT;

	signal multiplier_a : STD_LOGIC_VECTOR(15 DOWNTO 0);
	signal multiplier_b : STD_LOGIC_VECTOR(7 DOWNTO 0);
	signal multiplier_p : STD_LOGIC_VECTOR(23 DOWNTO 0);

	-- Aux Signals
	signal internal_reset_on 		: STD_LOGIC;
	signal internal_reset_on_ack 	: STD_LOGIC;	
	signal avg_level_d1	     : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
	signal data_window_d1	 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
	signal sample_freq_d1	 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
	signal avg_type_d1	 	 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	
	signal avg_type_result_i	 	 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal avg_level_result_i	 	 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal data_window_result_i	 	 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

	signal next_frame_offset_a: integer range 0 to 1024 :=0;
	signal next_frame_offset_b: integer range 0 to 1024 :=0;
	signal pipeline_level_reached_flag  : STD_LOGIC;

begin
	
	-- Cma1 = (X1+X2+....+Xn)/n
	-- Cma2 = (Xn+1 + (n-1).Cma1)/n
	-- Cma3 = (Xn+2 + (n-1).Cma2)/n
	
	-- Tready in													
	SIGNAL_INPUT_AXIS_TREADY <= signal_input_axis_tready_i1 and signal_input_axis_tready_i2 and signal_input_axis_tready_i3;
    					      						
	-- Average result
    avg_result_process: process(aclk) 
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				AVG_RESULT_AXIS_TVALID <= '0';
				AVG_RESULT_AXIS_TLAST  <= '0';
				AVG_RESULT_AXIS_TDATA  <= (others => '0');												
        	else
				if(input_output_bypass = '1') then
					AVG_RESULT_AXIS_TVALID <= SIGNAL_INPUT_AXIS_TVALID;				
					AVG_RESULT_AXIS_TLAST  <= SIGNAL_INPUT_AXIS_TLAST;	
					AVG_RESULT_AXIS_TDATA  <= SIGNAL_INPUT_AXIS_TDATA;											
				else
					AVG_RESULT_AXIS_TVALID <= m_axis_dout_tvalid;
					AVG_RESULT_AXIS_TLAST  <= m_axis_dout_tlast;
					AVG_RESULT_AXIS_TDATA  <= std_logic_vector(resize(signed(m_axis_dout_tdata(31 downto 8)), AVG_RESULT_AXIS_TDATA'length));									
				end if;												
			end if;
		end if;
	end process;
																	  												 																  
    -- Internal Reset
    internal_reset_process: process(aclk) 
		variable my_counter : integer range 0 to 8192 :=0;														
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				internal_reset_on <= '0';
				avg_level_d1  	<= (others => '0');
				data_window_d1  <= (others => '0');	
				sample_freq_d1  <= (others => '0');	
				avg_type_d1  	<= (others => '0');														  
            else
				avg_level_d1 <= AVG_LEVEL;
				data_window_d1 <= DATA_WINDOW;	
				sample_freq_d1 <= SAMPLE_FREQ;
				avg_type_d1 <= AVG_TYPE;													  										  
                if(avg_level_d1 /= AVG_LEVEL or data_window_d1 /= DATA_WINDOW or 
				   sample_freq_d1 /= SAMPLE_FREQ or avg_type_d1 /= AVG_TYPE) then								
					internal_reset_on <= '1';	
				else
					if(internal_reset_on_ack = internal_reset_on) then
						internal_reset_on <= '0';												  
					end if;
                end if;
            end if;
        end if;
    end process;	
    
    AVG_TYPE_RESULT <= avg_type_result_i;			
    AVG_LEVEL_RESULT <= avg_level_result_i;	
	DATA_WINDOW_RESULT <= data_window_result_i;																	

    -- Average SM
    avg_inst: process(aclk) 
        variable var_counter_wait  : integer range 0 to 65536 :=0;
        variable var_counter_avg : integer range 0 to 65536 :=0; 
        variable var_counter_sample: integer range 0 to 65536 :=0;
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				signal_input_axis_tready_i1 <= '0';
				first_frame_flag         <= '0';
				next_frame_flag			 <= '0';
				read_result_flag		 <= '0';
				pipeline_read_result_flag<= '0';
				avg_counter_divisor      <= (others => '0');
				AVG_RESET_ACK            <= '0';
				input_output_bypass		 <= '0';	
				data_window_result_i	 <= (others => '0');
				SAMPLE_FREQ_RESULT	     <= (others => '0');
				avg_type_result_i	     <= (others => '0');	
				avg_level_result_i		 <= (others => '0');									  
				AVG_COUNTER              <= (others => '0');
				AVG_WINDOW               <= '0';	
				var_counter_wait         := 0;		
				internal_reset_on_ack	 <= '0';
				pipeline_level_reached_flag	<= '0';													  
				state_general_sm <= idle;
        	else
				case state_general_sm is        
					when idle =>
						var_counter_sample := 0;											  
						if(internal_reset_on = '0' and AVG_RESET = '0' and avg_level_d1 = AVG_LEVEL and
						  data_window_d1 = DATA_WINDOW and sample_freq_d1 = SAMPLE_FREQ and 
						  avg_type_d1 = AVG_TYPE) then		
                            data_window_result_i <= DATA_WINDOW;
                            SAMPLE_FREQ_RESULT <= SAMPLE_FREQ;
                            avg_type_result_i <= AVG_TYPE;
                            if(unsigned(AVG_LEVEL) = 0) then
                                avg_level_result_i <= std_logic_vector(to_unsigned(1, avg_level_result_i'length));
                            else 
                                avg_level_result_i <= AVG_LEVEL;
                            end if;	  
                            if(unsigned(AVG_LEVEL) >= MIN_AVG_LEVEL) then
                                input_output_bypass	<= '0';								
                                signal_input_axis_tready_i1 <= '1';
                                if(SIGNAL_INPUT_AXIS_TVALID = '1') then
                                    AVG_WINDOW <= '1';
                                    if(unsigned(AVG_TYPE) = AVG_TYPE_NORMAL) then							
                                        state_general_sm <= first_frame;
                                    else
										if(unsigned(avg_counter_i) = unsigned(avg_level_result_i)) then		
											pipeline_level_reached_flag	<= '1';													   
										end if;																				   
                                        if(unsigned(avg_counter_i) = 0) then
                                            state_general_sm <= pipeline_acc_first;
                                        else
                                            state_general_sm <= pipeline_acc;
                                        end if;
                                    end if;	
                                end if;
                            else											
                                input_output_bypass	<= '1';										
                                signal_input_axis_tready_i1 <= AVG_RESULT_AXIS_TREADY;
                                if(SIGNAL_INPUT_AXIS_TVALID = '1') then
									AVG_WINDOW <= '1';								  
                                    state_general_sm <= by_pass;    		
                                end if;											
                            end if;
                        else
                            AVG_WINDOW <= '0';
                            signal_input_axis_tready_i1 <= '0';
                            var_counter_avg:= 0;
                            state_general_sm <= internal_reset;
                        end if;
                    when by_pass => 
                        signal_input_axis_tready_i1 <= AVG_RESULT_AXIS_TREADY;
                        if(SIGNAL_INPUT_AXIS_TLAST = '1') then
                            AVG_COUNTER <= (others => '0');
                            AVG_WINDOW <= '0';
                            state_general_sm <= idle;
                        else
                            if(var_counter_sample = 1) then
                                AVG_COUNTER <= std_logic_vector(to_unsigned(1, AVG_COUNTER'length));
                            end if;    
                            var_counter_sample := var_counter_sample + 1;
                        end if; 
					when internal_reset => 																
						internal_reset_on_ack <= internal_reset_on;
						AVG_RESET_ACK <= AVG_RESET;
						pipeline_level_reached_flag	<= '0';														
						if(internal_reset_on = '0' and AVG_RESET = '0') then
							state_general_sm <= idle;															
						end if;
					when pipeline_acc_first =>
                        if(first_frame_end = '1') then
                            signal_input_axis_tready_i1 <= '0';
                            first_frame_flag  <= '0';
                            if(var_counter_avg < unsigned(avg_level_result_i)) then
                                var_counter_avg := var_counter_avg + 1; 																						
                            end if;		
                            avg_counter_divisor <= avg_counter_i;
                            state_general_sm <= pipeline_read_result;
                        else
                            AVG_COUNTER <= avg_counter_i;
                            first_frame_flag  <= '1';
                            signal_input_axis_tready_i1 <= '1';
                        end if;
					when pipeline_acc =>
                        if(next_frame_end = '1') then
                            signal_input_axis_tready_i1 <= '0';
                            next_frame_flag  <= '0';
                            if(var_counter_avg < unsigned(avg_level_result_i)) then
                                var_counter_avg := var_counter_avg + 1;												
                            end if;												
                            avg_counter_divisor <= avg_counter_i;
                            state_general_sm <= pipeline_read_result;
                        else
                            AVG_COUNTER <= avg_counter_i;
                            next_frame_flag  <= '1';
                            signal_input_axis_tready_i1 <= '1';
                        end if;
					when pipeline_read_result =>                                                 
                        if(pipeline_read_result_end = '1') then
                            pipeline_read_result_flag  <= '0';														
                            state_general_sm  <= idle;     
                        else
                            pipeline_read_result_flag  <= '1';
                        end if;
					when first_frame =>
						if(first_frame_end = '1') then
							signal_input_axis_tready_i1 <= '0';
							first_frame_flag  <= '0';
							var_counter_avg := var_counter_avg + 1; 
							state_general_sm <= check_frame;
						 else
						    AVG_COUNTER <= avg_counter_i;
							first_frame_flag  <= '1';
						end if;
					when next_frame =>
						if(next_frame_end = '1') then
							signal_input_axis_tready_i1 <= '0';	
							next_frame_flag  <= '0';
							var_counter_avg := var_counter_avg + 1;
							state_general_sm <= check_frame;
						else
						    AVG_COUNTER <= avg_counter_i;
							signal_input_axis_tready_i1 <= '1';
							next_frame_flag <= '1';	
						end if;
					when check_frame =>
						if(var_counter_wait >= TRIGGER_WAIT) then
							var_counter_wait := 0;	
							avg_counter_divisor <= avg_counter_i;
							if(unsigned(avg_level_result_i) < MIN_AVG_LEVEL) then
								state_general_sm  <= read_result;
							else
								if(var_counter_avg < unsigned(avg_level_result_i)) then
									state_general_sm  <= next_frame;
								else
									state_general_sm  <= read_result;	
								end if;
							end if;		
						else
							var_counter_wait := var_counter_wait + 1;
							signal_input_axis_tready_i1 <= '0';	
						end if;						
					when read_result =>
					    AVG_COUNTER <= (others => '0');
						AVG_WINDOW <= '0';	
						var_counter_avg:= 0;												
						if(read_result_end = '1') then
							read_result_flag  <= '0';
							state_general_sm  <= idle;
						 else
							read_result_flag  <= '1';
						end if;
					when others =>
						null;
				end case;
			end if;
         end if;
    end process;
    	 
	-- Write first frame
	-- Write avg result
    signal_BRAM_porta: process(aclk) 
		variable var_counter_result : integer range 0 to 65536 :=0;
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                average_bram_ena   	<= '1';
                average_bram_wea   	<= (others => '0');
                average_bram_addra 	<= (others => '0');
                average_bram_dina  	<= (others => '0');
				average_bram_addra_first <= '0';
                first_frame_end			<= '0';
				next_frame_end			<= '0';
				pipeline_read_result_end_a <= '0';																
				signal_input_axis_tready_i2 <= '0';
				avg_counter_i <= (others => '0');
                -- State
                state_bram_porta_sm    	<= idle;
            else     
                case state_bram_porta_sm is        
                    when idle =>
                        average_bram_wea <= (others => '0');
						var_counter_result 	 := 0;
						average_bram_addra_first <= '1';
						signal_input_axis_tready_i2 <= '1';
						if(state_general_sm = read_result or state_general_sm = internal_reset) then
                            avg_counter_i <= (others => '0'); 
                        end if;
                        if(first_frame_flag = '1') then
							state_bram_porta_sm <= first_frame;
						elsif(next_frame_flag = '1') then
							state_bram_porta_sm <= next_frame;
                        elsif(pipeline_read_result_flag = '1') then
							if(unsigned(avg_counter_i) < unsigned(avg_level_result_i) or 
							   unsigned(avg_type_result_i) = AVG_TYPE_NORMAL) then
								pipeline_read_result_end_a <= '1';
							end if;
                            state_bram_porta_sm <= pipeline_read_result; 									
                        end if;
                    when first_frame =>
						if(first_frame_flag = '1') then
							if(signal_input_axis_tvalid_i(3) = '1') then
								if(var_counter_result >= unsigned(data_window_result_i)) then
									-- end
									first_frame_end   <= '1';
									signal_input_axis_tready_i2 <= '0';
									var_counter_result := to_integer(unsigned(unsigned(data_window_result_i)));	
								else
									-- write
									average_bram_wea   <= (others => '1');
									-- addr
									if(average_bram_addra_first = '1') then
										average_bram_addra_first <= '0';
										average_bram_addra 	<= (others => '0');	
									else
										average_bram_addra <= std_logic_vector(unsigned(average_bram_addra)+1);
									end if;
									-- din
									average_bram_dina  <= std_logic_vector(resize(signed(signal_input_axis_tdata_i(3)), average_bram_dina'length));	
									-- counter
									if(signal_input_axis_tlast_i(3) = '1') then
										first_frame_end   <= '1';
										signal_input_axis_tready_i2 <= '0';
										var_counter_result := to_integer(unsigned(unsigned(data_window_result_i)));
									else
									    if(var_counter_result = 1) then
									        if(unsigned(avg_counter_i) < unsigned(AVG_LEVEL)) then
                                                avg_counter_i <= std_logic_vector(unsigned(avg_counter_i) + 1);
                                            end if;
                                        end if;
										var_counter_result := var_counter_result + 1;
									end if;
								end if;
							end if;
						else						    
							first_frame_end   	<= '0';
							state_bram_porta_sm <= idle;
						end if;
					when next_frame =>
						if(next_frame_flag = '1') then
							if(signal_input_axis_tvalid_i(4+next_frame_offset_a) = '1') then
								if(var_counter_result >= unsigned(data_window_result_i)) then
									-- end
									next_frame_end <= '1';
									signal_input_axis_tready_i2 <= '0';
									var_counter_result := to_integer(unsigned(unsigned(data_window_result_i)));
								else
									-- write
									average_bram_wea <= (others => '1');
									-- addr
									if(average_bram_addra_first = '1') then
										average_bram_addra_first <= '0';
										average_bram_addra 	<= (others => '0');
									else
										average_bram_addra <= std_logic_vector(unsigned(average_bram_addra)+1);	
									end if;
									-- din
									if(pipeline_level_reached_flag = '0') then											  
										average_bram_dina  <= std_logic_vector(
																resize(signed(signal_input_axis_tdata_i(4+next_frame_offset_a)), average_bram_dina'length) +
																signed(average_bram_doutb)
															  ); 
									else
										average_bram_dina  <= std_logic_vector(
																resize(signed(signal_input_axis_tdata_i(4+next_frame_offset_a)), average_bram_dina'length) +
																resize(signed(multiplier_p), average_bram_dina'length)
															  ); 	
									end if;
									-- counter															  	
									if(signal_input_axis_tlast_i(4+next_frame_offset_a) = '1') then
                                        next_frame_end   <= '1';
                                        signal_input_axis_tready_i2 <= '0';
                                        var_counter_result := to_integer(unsigned(unsigned(data_window_result_i)));
                                    else
									    if(var_counter_result = 1) then
									        if(unsigned(avg_counter_i) < unsigned(AVG_LEVEL)) then
                                                avg_counter_i <= std_logic_vector(unsigned(avg_counter_i) + 1);
                                            end if;
                                        end if;
                                        var_counter_result := var_counter_result + 1;
                                    end if;	
								end if;
							end if;
						else
							next_frame_end <= '0';																		
							state_bram_porta_sm <= idle;	
						end if;
					when pipeline_read_result => 
						signal_input_axis_tready_i2 <= '0';				
						if(pipeline_read_result_flag = '1') then
							if(m_axis_dout_tvalid = '1' and pipeline_read_result_end_a = '0') then
								if(var_counter_result >= unsigned(data_window_result_i)) then
									-- end
									pipeline_read_result_end_a   <= '1';
									var_counter_result := to_integer(unsigned(unsigned(data_window_result_i)));	
								else
									-- write
									average_bram_wea   <= (others => '1');
									-- addr
									if(average_bram_addra_first = '1') then
										average_bram_addra_first <= '0';
										average_bram_addra 	<= (others => '0');	
									else
										average_bram_addra <= std_logic_vector(unsigned(average_bram_addra)+1);
									end if;
									-- din
									average_bram_dina  <= std_logic_vector(resize(signed(m_axis_dout_tdata(31 downto 8)), average_bram_dina'length));
									-- counter
									if(m_axis_dout_tlast = '1') then
										pipeline_read_result_end_a  <= '1';
										var_counter_result := to_integer(unsigned(unsigned(data_window_result_i)));
									else
										var_counter_result := var_counter_result + 1;
									end if;
								end if;
							end if;
						else						    
							pipeline_read_result_end_a <= '0';
							state_bram_porta_sm <= idle;
						end if;			
                    when others => 
                        null;
                end case;                                 
            end if;
        end if;
    end process;
																				  									
    -- Next frame offset
    next_frame_offset_process: process(aclk) 													
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				next_frame_offset_a <= 0;
				next_frame_offset_b <= 0;																  
				pipeline_read_result_end <= '0';																  
            else
				if(unsigned(avg_type_result_i) = AVG_TYPE_NORMAL) then
					next_frame_offset_a <= 0;
					next_frame_offset_b <= 0;															  
				else
					if(pipeline_level_reached_flag = '0') then																  
						next_frame_offset_a <= 2;
						next_frame_offset_b <= 2;														  
					else
						next_frame_offset_a <= 6;
						next_frame_offset_b <= 2;														  
					end if;
				end if;
				pipeline_read_result_end <= pipeline_read_result_end_a and pipeline_read_result_end_b;																	  
            end if;
        end if;
    end process;				
															
	-- Read result
	-- Read previous frame for filter
    signal_BRAM_portb: process(aclk) 
		variable var_counter_result : integer range 0 to 65536 :=0; 
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				-- Bram
                average_bram_enb   	<= '1';
                average_bram_web   	<= (others => '0');
                average_bram_addrb 	<= (others => '0');
                average_bram_dinb  	<= (others => '0');
				average_bram_addrb_first <= '0';
                read_result_end	    <= '0';
                pipeline_read_result_end_b <= '0';
                signal_input_axis_tready_i3 <= '0';
				-- Average result					
                avg_result_axis_tlast_i(0)    <= '0';
                avg_result_axis_tvalid_i(0)	<= '0';
                -- State
                state_bram_portb_sm    	<= idle;
            else     
                case state_bram_portb_sm is        
                    when idle =>
						average_bram_addrb_first <= '1';
						var_counter_result := 0;
						signal_input_axis_tready_i3 <= '1';
                        if(read_result_flag = '1') then
							state_bram_portb_sm <= read_result;
						elsif(next_frame_flag = '1') then
							state_bram_portb_sm <= next_frame;					
                        elsif(pipeline_read_result_flag = '1') then
                            state_bram_portb_sm <= pipeline_read_result; 
                        end if;
                    when read_result =>
                        signal_input_axis_tready_i3 <= '0';
						if(read_result_flag = '1') then
							if(var_counter_result >= unsigned(data_window_result_i)) then
								-- end
								avg_result_axis_tlast_i(0)    <= '0';
                				avg_result_axis_tvalid_i(0)	<= '0';
								read_result_end <= '1';
							else
							    if(AVG_RESULT_AXIS_TREADY = '1') then
                                    -- addr
                                    if(average_bram_addrb_first = '1') then
                                        average_bram_addrb_first <= '0';
                                        average_bram_addrb 	<= (others => '0');
                                    else
                                        average_bram_addrb <= std_logic_vector(unsigned(average_bram_addrb)+1);	
                                    end if;
                                    -- tvalid																	 
                                    avg_result_axis_tvalid_i(0) <= '1';
                                    -- tlast
                                    if(var_counter_result = unsigned(data_window_result_i)-1) then
                                        avg_result_axis_tlast_i(0) <= '1';	
                                    end if;
                                    -- counter
                                    var_counter_result := var_counter_result + 1;
                                else
                                    avg_result_axis_tvalid_i(0) <= '0';
                                end if;
							end if;
						else
							read_result_end   	<= '0';
							state_bram_portb_sm <= idle;
						end if;
					when next_frame =>
						if(next_frame_flag = '1') then
							if(signal_input_axis_tvalid_i(1+next_frame_offset_b) = '1') then
								if(var_counter_result >= unsigned(data_window_result_i)) then
									-- end
									signal_input_axis_tready_i3 <= '0';
									var_counter_result := to_integer(unsigned(unsigned(data_window_result_i)));																	  
								else
									-- addr
									if(average_bram_addrb_first = '1') then
										average_bram_addrb_first <= '0';
										average_bram_addrb 	<= (others => '0');
									else
										average_bram_addrb <= std_logic_vector(unsigned(average_bram_addrb)+1);	
									end if;
									-- counter
									if(signal_input_axis_tlast_i(4+next_frame_offset_b) = '1') then
                                        signal_input_axis_tready_i3 <= '0';
                                        var_counter_result := to_integer(unsigned(unsigned(data_window_result_i)));
                                    else
                                        var_counter_result := var_counter_result + 1;
                                    end if;    																	  
								end if;
							end if;
						else
							state_bram_portb_sm <= idle;	
						end if;	
                    when pipeline_read_result =>
                        signal_input_axis_tready_i3 <= '0';
                        if(pipeline_read_result_flag = '1') then
                            if(var_counter_result >= unsigned(data_window_result_i)) then
                                -- end
                                avg_result_axis_tlast_i(0)    <= '0';
                                avg_result_axis_tvalid_i(0)    <= '0';
                                pipeline_read_result_end_b <= '1';
                            else
                                if(AVG_RESULT_AXIS_TREADY = '1') then
                                    -- addr
                                    if(average_bram_addrb_first = '1') then
                                        average_bram_addrb_first <= '0';
                                        average_bram_addrb     <= (others => '0');
                                    else
                                        average_bram_addrb <= std_logic_vector(unsigned(average_bram_addrb)+1);    
                                    end if;
                                    -- tvalid                                                                     
                                    avg_result_axis_tvalid_i(0) <= '1';
                                    -- tlast
                                    if(var_counter_result = unsigned(data_window_result_i)-1) then
                                        avg_result_axis_tlast_i(0) <= '1';   
                                    end if;
                                    -- counter
                                    var_counter_result := var_counter_result + 1;
                                else
                                    avg_result_axis_tvalid_i(0) <= '0';
                                end if;
                            end if;
                        else
                            pipeline_read_result_end_b <= '0';
                            state_bram_portb_sm <= idle;
                        end if;
                    when others => 
                        null;
                end case;                                 
            end if;
        end if;
    end process;
    
    signal_input_axis_tdata_i(0) <= std_logic_vector(resize(signed(SIGNAL_INPUT_AXIS_TDATA), signal_input_axis_tdata_i(0)'length));
    signal_input_axis_tlast_i(0) <= SIGNAL_INPUT_AXIS_TLAST;
    signal_input_axis_tvalid_i(0) <= SIGNAL_INPUT_AXIS_TVALID;
            
    -- Delay signal input     
    delay_signal_input : for i in 1 to 15 generate
        process(aclk) 
        begin
           if (rising_edge(aclk)) then
                if (aresetn = '0') then
                    signal_input_axis_tdata_i(i)    <= (others => '0');
                    signal_input_axis_tlast_i(i)    <= '0';
                    signal_input_axis_tvalid_i(i)   <= '0';
                else
                    signal_input_axis_tdata_i(i) <= signal_input_axis_tdata_i(i-1);
                    signal_input_axis_tlast_i(i) <= signal_input_axis_tlast_i(i-1);
                    signal_input_axis_tvalid_i(i) <= signal_input_axis_tvalid_i(i-1);
                end if;
           end if;
        end process;
    end generate;
			    
    -- Delay average result     
    delay_avg_result : for i in 1 to 3 generate
        process(aclk) 
        begin
           if (rising_edge(aclk)) then
                if (aresetn = '0') then
                    avg_result_axis_tlast_i(i)    <= '0';
                    avg_result_axis_tvalid_i(i)   <= '0';
                else
                    avg_result_axis_tlast_i(i) <= avg_result_axis_tlast_i(i-1);
                    avg_result_axis_tvalid_i(i) <= avg_result_axis_tvalid_i(i-1);
                end if;
           end if;
        end process;
    end generate;
    
--    s_axis_divisor_tvalid <= avg_result_axis_tvalid_i(2);
--    s_axis_dividend_tvalid <= avg_result_axis_tvalid_i(2); 
--    s_axis_divisor_tlast <= avg_result_axis_tlast_i(2);
--    s_axis_dividend_tlast <= avg_result_axis_tlast_i(2); 
--    s_axis_divisor_tdata <= avg_counter_divisor;	
--    s_axis_dividend_tdata <= std_logic_vector(resize(signed(average_bram_doutb), s_axis_dividend_tdata'length));	
    
	-- Divider inputs
    divider_inputs_process: process(aclk) 
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                s_axis_divisor_tvalid  <= '0';
                s_axis_dividend_tvalid <= '0';      
                s_axis_divisor_tlast   <= '0';
                s_axis_dividend_tlast  <= '0'; 
				s_axis_divisor_tdata   <= (others => '0');
                s_axis_dividend_tdata  <= (others => '0');                 
            else
                if(unsigned(avg_type_result_i) = AVG_TYPE_NORMAL) then
                    s_axis_divisor_tvalid <= avg_result_axis_tvalid_i(2);
                    s_axis_dividend_tvalid <= avg_result_axis_tvalid_i(2); 
                    s_axis_divisor_tlast <= avg_result_axis_tlast_i(2);
                    s_axis_dividend_tlast <= avg_result_axis_tlast_i(2); 
                    s_axis_divisor_tdata <= avg_counter_divisor;    
                    s_axis_dividend_tdata <= std_logic_vector(resize(signed(average_bram_doutb), s_axis_dividend_tdata'length));
                else
                    s_axis_divisor_tvalid <= avg_result_axis_tvalid_i(2);
                    s_axis_dividend_tvalid <= avg_result_axis_tvalid_i(2); 
                    s_axis_divisor_tlast <= avg_result_axis_tlast_i(2);
                    s_axis_dividend_tlast <= avg_result_axis_tlast_i(2); 
                    s_axis_divisor_tdata <= avg_counter_divisor;    
                    s_axis_dividend_tdata <= std_logic_vector(resize(signed(average_bram_doutb), s_axis_dividend_tdata'length));
                end if;  
            end if;
        end if;
    end process;
													 
	-- Multiplier inputs
    mult_inputs_process: process(aclk) 
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				multiplier_a  <= (others => '0');
				multiplier_b  <= (others => '0');										 
        	else
				if(unsigned(avg_level_result_i) >= MIN_AVG_LEVEL) then								 
					multiplier_b  <= std_logic_vector(unsigned(avg_level_result_i(7 downto 0)) - 1);
				else
					multiplier_b  <= std_logic_vector(to_unsigned(1, multiplier_b'length));								 
				end if;
				multiplier_a  <= std_logic_vector(resize(signed(average_bram_doutb), multiplier_a'length));	
			end if;
		end if;
	end process;
          
    -- BRAM
    average_filter_BRAM_inst : average_filter_BRAM
      PORT MAP (
        clka => aclk,
        ena => average_bram_ena,
        wea => average_bram_wea,
        addra => average_bram_addra,
        dina => average_bram_dina,
        douta => average_bram_douta,
        clkb => aclk,
        enb => average_bram_enb,
        web => average_bram_web,
        addrb => average_bram_addrb,
        dinb => average_bram_dinb,
        doutb => average_bram_doutb
      );
      
    -- Divider
    average_filter_divider_24_8_inst : average_filter_divider_24_8
      PORT MAP (
        aclk => aclk,
        aclken => '1',
        aresetn => aresetn,
        s_axis_divisor_tvalid => s_axis_divisor_tvalid,
        s_axis_divisor_tlast => s_axis_divisor_tlast,
        s_axis_divisor_tdata => s_axis_divisor_tdata,
        s_axis_dividend_tvalid => s_axis_dividend_tvalid,
        s_axis_dividend_tlast => s_axis_dividend_tlast,
        s_axis_dividend_tdata => s_axis_dividend_tdata,
        m_axis_dout_tvalid => m_axis_dout_tvalid,
        m_axis_dout_tlast => m_axis_dout_tlast,
        m_axis_dout_tdata => m_axis_dout_tdata
      );
													 
	-- Multiplier
	average_filter_multiplier_16_8_inst : average_filter_multiplier_16_8
	  PORT MAP (
		CLK => aclk,
		A => multiplier_a,
		B => multiplier_b,
		P => multiplier_p
	  );												 
      
end arch_imp;
