library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity average_filter is
	generic (
        -- Users to add parameters here
        REGISTER_WIDTH              : integer    := 32;
        COUNTER_WAIT_1              : integer    := 16;
        COUNTER_WAIT_2              : integer    := 32;
        STATE                       : integer    := 4;
        MIN_AVG_LEVEL               : integer    := 2;
    
        -- User parameters ends
        -- Do not modify the parameters beyond this line
    
    
        -- Parameters of Axi Slave Bus Interface S00_AXIS
        C_S00_AXIS_TDATA_WIDTH    : integer    := 16;
    
        -- Parameters of Axi Slave Bus Interface sdma_AXIS
        C_SDMA_AXIS_TDATA_WIDTH    : integer    := 16;
    
        -- Parameters of Axi Master Bus Interface ascan_AXIS
        C_ASCAN_AXIS_TDATA_WIDTH    : integer    := 16;
        C_ASCAN_AXIS_START_COUNT    : integer    := 32;
    
        -- Parameters of Axi Master Bus Interface gates_AXIS
        C_GATES_AXIS_TDATA_WIDTH    : integer    := 16;
        C_GATES_AXIS_START_COUNT    : integer    := 32;
    
        -- Parameters of Axi Master Bus Interface mdma_AXIS
        C_MDMA_AXIS_TDATA_WIDTH    : integer    := 16;
        C_MDMA_AXIS_START_COUNT    : integer    := 32
    );
	port (
		-- Users to add ports here
        DATA_WINDOW         : in std_logic_vector(REGISTER_WIDTH-1 downto 0);
        FILTERING            : in std_logic; 
        AVG_LEVEL            : in std_logic_vector(REGISTER_WIDTH-1 downto 0);
        AVG_READ            : in std_logic; -- 1= read coinc result
        AVG_STATE           : out std_logic_vector(STATE-1 downto 0); 
        AVG_DMA_STATE       : out std_logic_vector(STATE-1 downto 0); 
        -- 0000 idle
        -- 0001 first_acc
        -- 0010 avg_wait
        -- 0011 pipe_line_acc
        -- 1111 finish
        
        -- 0000 idle
        -- 0001 frame request
        -- 0010 accumulating frame
        -- 0011 checking n frame
        -- 0100 wait for read
        -- 0101 reading
        -- 0110 finish read
        
        -- DMA
        AVG_DMA_UPLOAD_frame    : out std_logic_vector(REGISTER_WIDTH-1 downto 0);

		-- User ports ends
		-- Do not modify the ports beyond this line


		-- Ports of Axi Slave Bus Interface S00_AXIS
		s00_axis_aclk	: in std_logic;
		s00_axis_aresetn	: in std_logic;
		s00_axis_tready	: out std_logic;
		s00_axis_tdata	: in std_logic_vector(C_S00_AXIS_TDATA_WIDTH-1 downto 0);
		s00_axis_tstrb	: in std_logic_vector((C_S00_AXIS_TDATA_WIDTH/8)-1 downto 0);
		s00_axis_tlast	: in std_logic;
		s00_axis_tvalid	: in std_logic;

		-- Ports of Axi Slave Bus Interface SDMA_AXIS
		sdma_axis_aclk	: in std_logic;
		sdma_axis_aresetn	: in std_logic;
		sdma_axis_tready	: out std_logic;
		sdma_axis_tdata	: in std_logic_vector(C_SDMA_AXIS_TDATA_WIDTH-1 downto 0);
		sdma_axis_tstrb	: in std_logic_vector((C_SDMA_AXIS_TDATA_WIDTH/8)-1 downto 0);
		sdma_axis_tlast	: in std_logic;
		sdma_axis_tvalid	: in std_logic;

		-- Ports of Axi Master Bus Interface ASCAN_AXIS
		ascan_axis_aclk	: in std_logic;
		ascan_axis_aresetn	: in std_logic;
		ascan_axis_tvalid	: out std_logic;
		ascan_axis_tdata	: out std_logic_vector(C_ASCAN_AXIS_TDATA_WIDTH-1 downto 0);
		ascan_axis_tstrb	: out std_logic_vector((C_ASCAN_AXIS_TDATA_WIDTH/8)-1 downto 0);
		ascan_axis_tlast	: out std_logic;
		ascan_axis_tready	: in std_logic;

		-- Ports of Axi Master Bus Interface GATES_AXIS
		gates_axis_aclk	: in std_logic;
		gates_axis_aresetn	: in std_logic;
		gates_axis_tvalid	: out std_logic;
		gates_axis_tdata	: out std_logic_vector(C_GATES_AXIS_TDATA_WIDTH-1 downto 0);
		gates_axis_tstrb	: out std_logic_vector((C_GATES_AXIS_TDATA_WIDTH/8)-1 downto 0);
		gates_axis_tlast	: out std_logic;
		gates_axis_tready	: in std_logic;

		-- Ports of Axi Master Bus Interface MDMA_AXIS
		mdma_axis_aclk	: in std_logic;
		mdma_axis_aresetn	: in std_logic;
		mdma_axis_tvalid	: out std_logic;
		mdma_axis_tdata	: out std_logic_vector(C_MDMA_AXIS_TDATA_WIDTH-1 downto 0);
		mdma_axis_tstrb	: out std_logic_vector((C_MDMA_AXIS_TDATA_WIDTH/8)-1 downto 0);
		mdma_axis_tlast	: out std_logic;
		mdma_axis_tready	: in std_logic
	);
end average_filter;

architecture arch_imp of average_filter is
    -- Delayed input signals
signal s00_axis_tdata_i1    : std_logic_vector(C_S00_AXIS_TDATA_WIDTH-1 downto 0);
signal s00_axis_tlast_i1    : std_logic;
signal s00_axis_tvalid_i1   : std_logic;

signal s00_axis_tdata_i2    : std_logic_vector(C_S00_AXIS_TDATA_WIDTH-1 downto 0);
signal s00_axis_tlast_i2    : std_logic;
signal s00_axis_tvalid_i2   : std_logic;

signal s00_axis_tdata_i3    : std_logic_vector(C_S00_AXIS_TDATA_WIDTH-1 downto 0);
signal s00_axis_tlast_i3    : std_logic;
signal s00_axis_tvalid_i3   : std_logic;

signal s00_axis_tdata_i4    : std_logic_vector(C_S00_AXIS_TDATA_WIDTH-1 downto 0);
signal s00_axis_tlast_i4    : std_logic;
signal s00_axis_tvalid_i4   : std_logic;

signal s00_axis_tlast_i5    : std_logic;
signal s00_axis_tvalid_i5   : std_logic;

signal s00_axis_tlast_i6    : std_logic;
signal s00_axis_tvalid_i6   : std_logic;

signal s00_axis_tlast_i7    : std_logic;
signal s00_axis_tvalid_i7   : std_logic;

-- Delayed dma input signals
signal sdma_axis_tlast_i1    : std_logic;
signal sdma_axis_tvalid_i1   : std_logic;

signal sdma_axis_tlast_i2    : std_logic;
signal sdma_axis_tvalid_i2   : std_logic;

signal sdma_axis_tlast_i3    : std_logic;
signal sdma_axis_tvalid_i3   : std_logic;

-- Delayed dma output signals
signal mdma_axis_tlast_i1    : std_logic;
signal mdma_axis_tvalid_i1   : std_logic;

signal mdma_axis_tlast_i2    : std_logic;
signal mdma_axis_tvalid_i2   : std_logic;
 
-- Averaging signals
signal AVG_GATES_tlast        : std_logic;
signal AVG_GATES_tvalid       : std_logic;

signal AVG_GATES_tlast_i1     : std_logic;
signal AVG_GATES_tvalid_i1    : std_logic;

signal AVG_GATES_tlast_i2     : std_logic;
signal AVG_GATES_tvalid_i2    : std_logic;

signal AVG_ASCAN_tlast        : std_logic;
signal AVG_ASCAN_tvalid       : std_logic;

signal AVG_ASCAN_tlast_i1     : std_logic;
signal AVG_ASCAN_tvalid_i1    : std_logic;

signal AVG_ASCAN_tlast_i2     : std_logic;
signal AVG_ASCAN_tvalid_i2    : std_logic;

signal AVG_LEVEL_counter  : unsigned(REGISTER_WIDTH-1 downto 0);
signal AVG_LEVEL_i        : std_logic_vector(REGISTER_WIDTH-1 downto 0);

-- Accumulator BRAM  
COMPONENT average_filter_ACC_BRAM
  PORT (
    clka : IN STD_LOGIC;
    ena : IN STD_LOGIC;
    wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    addra : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    dina : IN STD_LOGIC_VECTOR(21 DOWNTO 0);
    douta : OUT STD_LOGIC_VECTOR(21 DOWNTO 0);
    clkb : IN STD_LOGIC;
    enb : IN STD_LOGIC;
    web : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    addrb : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    dinb : IN STD_LOGIC_VECTOR(21 DOWNTO 0);
    doutb : OUT STD_LOGIC_VECTOR(21 DOWNTO 0)
  );
END COMPONENT;

-- PORTA ACC- write
signal BRAM_ACC_PORTA_addr : STD_LOGIC_VECTOR ( 15 downto 0 );
signal BRAM_ACC_PORTA_addr_counter : UNSIGNED ( 15 downto 0 );
signal BRAM_ACC_PORTA_din : STD_LOGIC_VECTOR ( 21 downto 0 );
signal BRAM_ACC_PORTA_dout : STD_LOGIC_VECTOR ( 21 downto 0 );
signal BRAM_ACC_PORTA_en : STD_LOGIC;
signal BRAM_ACC_PORTA_we : STD_LOGIC_VECTOR ( 0 to 0 );

signal BRAM_ACC_PORTA_first_write : STD_LOGIC;
signal BRAM_ACC_PORTA_write_end : STD_LOGIC;

-- PORTB ACC - read
signal BRAM_ACC_PORTB_addr : STD_LOGIC_VECTOR ( 15 downto 0 );
signal BRAM_ACC_PORTB_addr_counter : UNSIGNED ( 15 downto 0 );
signal BRAM_ACC_PORTB_din : STD_LOGIC_VECTOR ( 21 downto 0 );
signal BRAM_ACC_PORTB_dout : STD_LOGIC_VECTOR ( 21 downto 0 );
signal BRAM_ACC_PORTB_en : STD_LOGIC;
signal BRAM_ACC_PORTB_we : STD_LOGIC_VECTOR ( 0 to 0 );

signal BRAM_ACC_PORTB_first_read : STD_LOGIC; 
signal BRAM_ACC_PORTB_read_end : STD_LOGIC; 

-- Buffer BRAM
COMPONENT average_filter_BUF_BRAM
  PORT (
    clka : IN STD_LOGIC;
    ena : IN STD_LOGIC;
    wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    addra : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    dina : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
    douta : OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
    clkb : IN STD_LOGIC;
    enb : IN STD_LOGIC;
    web : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    addrb : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    dinb : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
    doutb : OUT STD_LOGIC_VECTOR(13 DOWNTO 0)
  );
END COMPONENT;

-- PORTA Buffer- write
signal BRAM_BUF_PORTA_addr : STD_LOGIC_VECTOR ( 15 downto 0 );
signal BRAM_BUF_PORTA_addr_counter : UNSIGNED ( 15 downto 0 );
signal BRAM_BUF_PORTA_din : STD_LOGIC_VECTOR ( 13 downto 0 );
signal BRAM_BUF_PORTA_dout : STD_LOGIC_VECTOR ( 13 downto 0 );
signal BRAM_BUF_PORTA_en : STD_LOGIC;
signal BRAM_BUF_PORTA_we : STD_LOGIC_VECTOR ( 0 to 0 );

signal BRAM_BUF_PORTA_first_write : STD_LOGIC;
signal BRAM_BUF_PORTA_write_end_acc : STD_LOGIC;
signal BRAM_BUF_PORTA_write_end_rec : STD_LOGIC;

-- PORTB Buffer - read
signal BRAM_BUF_PORTB_addr : STD_LOGIC_VECTOR ( 15 downto 0 );
signal BRAM_BUF_PORTB_addr_counter : UNSIGNED ( 15 downto 0 );
signal BRAM_BUF_PORTB_din : STD_LOGIC_VECTOR ( 13 downto 0 );
signal BRAM_BUF_PORTB_dout : STD_LOGIC_VECTOR ( 13 downto 0 );
signal BRAM_BUF_PORTB_en : STD_LOGIC;
signal BRAM_BUF_PORTB_we : STD_LOGIC_VECTOR ( 0 to 0 );

signal BRAM_BUF_PORTB_first_read : STD_LOGIC;  
signal BRAM_BUF_PORTB_read_input_end : STD_LOGIC;
signal BRAM_BUF_PORTB_read_dma_end : STD_LOGIC;

-- Averaging state machine
type states_avg is (idle, first_acc, avg_wait, pipe_line_acc, read_avg); 
signal state_avg : states_avg;

signal SITUATION : STD_LOGIC_VECTOR(1 downto 0 );

begin

BRAM_ACC_PORTA_addr <= std_logic_vector(BRAM_ACC_PORTA_addr_counter);
BRAM_ACC_PORTB_addr <= std_logic_vector(BRAM_ACC_PORTB_addr_counter);
BRAM_BUF_PORTA_addr <= std_logic_vector(BRAM_BUF_PORTA_addr_counter);
BRAM_BUF_PORTB_addr <= std_logic_vector(BRAM_BUF_PORTB_addr_counter);

-- write BRAM_ACC_PORTA
BRAM_ACC_PORTA: process(s00_axis_aclk, s00_axis_aresetn) 
begin 
    if (s00_axis_aresetn = '0') then
        BRAM_ACC_PORTA_en           <= '0';
        BRAM_ACC_PORTA_we           <= (others => '1');
        BRAM_ACC_PORTA_din          <= (others => '0');
        BRAM_ACC_PORTA_addr_counter <= (others => '0');   
        BRAM_ACC_PORTA_first_write  <= '1';
        BRAM_ACC_PORTA_write_end    <= '0';
        -- Averaging
        AVG_GATES_tvalid            <= '0';
        AVG_GATES_tlast             <= '0';
    elsif (s00_axis_aclk'event and s00_axis_aclk = '1') then
        if(SITUATION /= "00") then
            if(s00_axis_tvalid_i4 = '1' and BRAM_ACC_PORTA_addr_counter <= (unsigned(DATA_WINDOW)-1)) then
                BRAM_ACC_PORTA_en <= '1';
                if(SITUATION = "01") then
                    if(signed(AVG_LEVEL_counter) > 0) then
                        BRAM_ACC_PORTA_din <= std_logic_vector(
                            signed(s00_axis_tdata_i4(15) & s00_axis_tdata_i4(15) & s00_axis_tdata_i4(15) & 
                                s00_axis_tdata_i4(15) & s00_axis_tdata_i4(15) & s00_axis_tdata_i4(15) & s00_axis_tdata_i4) +
                            signed(BRAM_ACC_PORTB_dout)
                        );
                    else
                        BRAM_ACC_PORTA_din <= s00_axis_tdata_i4(15) & s00_axis_tdata_i4(15) & s00_axis_tdata_i4(15) & 
                            s00_axis_tdata_i4(15) & s00_axis_tdata_i4(15) & s00_axis_tdata_i4(15) & s00_axis_tdata_i4;
                    end if;
                    if(signed(AVG_LEVEL_counter) >= (signed(AVG_LEVEL_i)-1) or (unsigned(AVG_LEVEL_i) < MIN_AVG_LEVEL)) then
                        AVG_GATES_tvalid <= '1';
                        if(BRAM_ACC_PORTA_addr_counter >= (unsigned(DATA_WINDOW)-1) or s00_axis_tlast_i4 = '1') then
                            AVG_GATES_tlast  <= '1';
                        end if;
                    end if;
                else
                    BRAM_ACC_PORTA_din <= std_logic_vector(
                        signed(s00_axis_tdata_i4(15) & s00_axis_tdata_i4(15) & s00_axis_tdata_i4(15) & 
                            s00_axis_tdata_i4(15) & s00_axis_tdata_i4(15) & s00_axis_tdata_i4(15) & s00_axis_tdata_i4) +
                        signed(BRAM_ACC_PORTB_dout) -
                        signed(BRAM_BUF_PORTB_dout(13) & BRAM_BUF_PORTB_dout(13) & BRAM_BUF_PORTB_dout(13) &
                            BRAM_BUF_PORTB_dout(13) & BRAM_BUF_PORTB_dout(13) & BRAM_BUF_PORTB_dout(13) &
                            BRAM_BUF_PORTB_dout(13) & BRAM_BUF_PORTB_dout(13) & BRAM_BUF_PORTB_dout)
                    );
                    
                    AVG_GATES_tvalid <= '1';
                    if(BRAM_ACC_PORTA_addr_counter >= (unsigned(DATA_WINDOW)-1) or s00_axis_tlast_i4 = '1') then
                        AVG_GATES_tlast  <= '1';
                    end if;
                end if;
                if(BRAM_ACC_PORTA_first_write = '1') then
                    BRAM_ACC_PORTA_first_write <= '0';    
                else
                    BRAM_ACC_PORTA_addr_counter <= BRAM_ACC_PORTA_addr_counter + 1;
                end if;
            else
                AVG_GATES_tvalid <= '0';
                AVG_GATES_tlast  <= '0';
                if(s00_axis_tlast_i7 = '1') then
                    BRAM_ACC_PORTA_en <= '0';                                                 
                    BRAM_ACC_PORTA_addr_counter <= (others => '0'); 
                    BRAM_ACC_PORTA_write_end <= '1';
                end if;
            end if;    
        else
            BRAM_ACC_PORTA_en <= '0';
            BRAM_ACC_PORTA_addr_counter <= (others => '0'); 
            BRAM_ACC_PORTA_first_write  <= '1';
            BRAM_ACC_PORTA_write_end    <= '0';
            AVG_GATES_tvalid <= '0';
            AVG_GATES_tlast  <= '0';
        end if;          
    end if;
end process;
  
-- read BRAM_ACC_PORTB - always enabled
BRAM_ACC_PORTB: process(s00_axis_aclk, s00_axis_aresetn) 
begin 
    if (s00_axis_aresetn = '0') then
        BRAM_ACC_PORTB_en           <= '1';
        BRAM_ACC_PORTB_din          <= (others => '0');
        BRAM_ACC_PORTB_we           <= (others => '0');
        BRAM_ACC_PORTB_addr_counter <= (others => '0');   
        BRAM_ACC_PORTB_first_read   <= '1';
        BRAM_ACC_PORTB_read_end     <= '0';
        AVG_ASCAN_tlast             <= '0';
        AVG_ASCAN_tvalid            <= '0';
    elsif (s00_axis_aclk'event and s00_axis_aclk = '1') then
        if(SITUATION = "11") then
            if(gates_axis_tready = '1') then
                AVG_ASCAN_tlast             <= '0';
                AVG_ASCAN_tvalid            <= '0';
                if(BRAM_ACC_PORTB_read_end = '0') then
                    AVG_ASCAN_tlast             <= '0';
                    AVG_ASCAN_tvalid            <= '1';
                    if(BRAM_ACC_PORTB_first_read = '1') then
                        BRAM_ACC_PORTB_first_read <= '0'; 
                    else
                        BRAM_ACC_PORTB_addr_counter <= BRAM_ACC_PORTB_addr_counter + 1;
                        if(BRAM_ACC_PORTB_addr_counter >= (unsigned(DATA_WINDOW)-2)) then
                            AVG_ASCAN_tlast                 <= '1';
                            if(BRAM_ACC_PORTB_addr_counter >= (unsigned(DATA_WINDOW)-1)) then
                                AVG_ASCAN_tlast             <= '0';
                                AVG_ASCAN_tvalid            <= '0';
                                BRAM_ACC_PORTB_first_read  <= '1';
                                BRAM_ACC_PORTB_addr_counter <= (others => '0');
                                BRAM_ACC_PORTB_read_end     <= '1';
                            end if;
                        end if;
                    end if;
                end if;
            else
                AVG_ASCAN_tlast             <= '0';
                AVG_ASCAN_tvalid            <= '0';
            end if;
        elsif(SITUATION /= "00") then
             if(s00_axis_tvalid_i1 = '1') then
                if(BRAM_ACC_PORTB_first_read = '1') then
                    BRAM_ACC_PORTB_first_read <= '0'; 
                else
                    BRAM_ACC_PORTB_addr_counter <= BRAM_ACC_PORTB_addr_counter + 1;
                end if;  
             else
                if(s00_axis_tlast_i4 = '1') then
                    BRAM_ACC_PORTB_addr_counter <= (others => '0');
                    BRAM_ACC_PORTB_first_read  <= '1';
                    BRAM_ACC_PORTB_read_end   <= '1';  
                end if;
             end if; 
        else
            BRAM_ACC_PORTB_addr_counter <= (others => '0'); 
            BRAM_ACC_PORTB_first_read  <= '1';
            BRAM_ACC_PORTB_read_end   <= '0';
        end if;          
    end if;
end process;

-- write BRAM_BUF_PORTA
BRAM_BUF_PORTA: process(s00_axis_aclk, s00_axis_aresetn) 
begin 
    if (s00_axis_aresetn = '0') then
        BRAM_BUF_PORTA_en           <= '0';
        BRAM_BUF_PORTA_we           <= (others => '1');
        BRAM_BUF_PORTA_din          <= (others => '0');
        BRAM_BUF_PORTA_addr_counter <= (others => '0');   
        BRAM_BUF_PORTA_first_write  <= '1';
        BRAM_BUF_PORTA_write_end_acc<= '0';
        BRAM_BUF_PORTA_write_end_rec<= '0';
        sdma_axis_tready <= '0';
    elsif (s00_axis_aclk'event and s00_axis_aclk = '1') then
        sdma_axis_tready <= '1'; -- For the momment always enabled
        if((SITUATION /= "00") and (unsigned(AVG_LEVEL_i) >= MIN_AVG_LEVEL)) then
            if(BRAM_BUF_PORTA_write_end_rec = '0') then
                if(AVG_LEVEL_counter >= unsigned(AVG_LEVEL_i)) then
                    if(sdma_axis_tvalid = '1') then
                        BRAM_BUF_PORTA_en <= '1';
                        BRAM_BUF_PORTA_din <= sdma_axis_tdata(13 downto 0);
                        if(BRAM_BUF_PORTA_first_write = '1') then
                            BRAM_BUF_PORTA_first_write <= '0';    
                        else
                            BRAM_BUF_PORTA_addr_counter <= BRAM_BUF_PORTA_addr_counter + 1;
                        end if;
                    else
                        if(sdma_axis_tlast_i3 = '1') then
                            BRAM_BUF_PORTA_en <= '0'; 
                            BRAM_BUF_PORTA_write_end_rec <= '1';
                        end if; 
                    end if;
                else
                    BRAM_BUF_PORTA_write_end_rec <= '1';
                end if;
            else
                if(s00_axis_tvalid_i2 = '1') then
                    BRAM_BUF_PORTA_en <= '1';
                    BRAM_BUF_PORTA_din <= s00_axis_tdata_i2(13 downto 0);
                    if(BRAM_BUF_PORTA_first_write = '1') then
                        BRAM_BUF_PORTA_first_write <= '0';    
                    else
                        BRAM_BUF_PORTA_addr_counter <= BRAM_BUF_PORTA_addr_counter + 1;
                    end if;
                else
                    if(s00_axis_tlast_i5 = '1') then
                        BRAM_BUF_PORTA_en <= '0';
                        BRAM_BUF_PORTA_addr_counter <= (others => '0'); 
                        BRAM_BUF_PORTA_first_write <= '1'; 
                        BRAM_BUF_PORTA_write_end_acc <= '1';
                    end if;
                end if; 
            end if;
        else
            BRAM_BUF_PORTA_en <= '0';
            BRAM_BUF_PORTA_addr_counter <= (others => '0'); 
            BRAM_BUF_PORTA_first_write  <= '1';
            BRAM_BUF_PORTA_write_end_acc<= '0';
            BRAM_BUF_PORTA_write_end_rec<= '0';
        end if;         
    end if;
end process;
  
-- read BRAM_BUF_PORTB - always enabled
BRAM_BUF_PORTB: process(s00_axis_aclk, s00_axis_aresetn) 
begin 
    if (s00_axis_aresetn = '0') then
        BRAM_BUF_PORTB_en               <= '1';
        BRAM_BUF_PORTB_we               <= (others => '0');
        BRAM_BUF_PORTB_din              <= (others => '0');
        BRAM_BUF_PORTB_addr_counter     <= (others => '0');   
        BRAM_BUF_PORTB_first_read       <= '1';
        BRAM_BUF_PORTB_read_input_end   <= '0';
        BRAM_BUF_PORTB_read_dma_end     <= '0';
        mdma_axis_tvalid_i1             <= '0';
        mdma_axis_tlast_i1              <= '0';
        AVG_DMA_UPLOAD_frame        <= (others => '0'); 
    elsif (s00_axis_aclk'event and s00_axis_aclk = '1') then
        if(SITUATION /= "00" and (unsigned(AVG_LEVEL_i) >= MIN_AVG_LEVEL)) then   
            if(BRAM_BUF_PORTB_read_input_end = '0') then
                if(s00_axis_tvalid_i1 = '1') then
                    if(BRAM_BUF_PORTB_first_read = '1') then
                        BRAM_BUF_PORTB_first_read <= '0'; 
                    else
                        BRAM_BUF_PORTB_addr_counter <= BRAM_BUF_PORTB_addr_counter + 1;
                    end if;
                else
                    if(s00_axis_tlast_i4 = '1') then
                        BRAM_BUF_PORTB_read_input_end   <= '1'; 
                        BRAM_BUF_PORTB_addr_counter <= (others => '0');  
                    end if;    
                end if;
            else
                if(mdma_axis_tready = '1' and BRAM_BUF_PORTB_addr_counter <= (unsigned(DATA_WINDOW)-1)) then
                    mdma_axis_tvalid_i1  <= '1';
                    if(BRAM_BUF_PORTB_addr_counter = (unsigned(DATA_WINDOW)-1)) then
                        mdma_axis_tlast_i1   <= '1'; 
                    else
                        mdma_axis_tlast_i1   <= '0';
                    end if;
                    if(BRAM_BUF_PORTB_first_read = '1') then
                        BRAM_BUF_PORTB_first_read <= '0'; 
                    else
                        BRAM_BUF_PORTB_addr_counter <= BRAM_BUF_PORTB_addr_counter + 1;
                    end if;
                else
                    mdma_axis_tvalid_i1  <= '0';
                    mdma_axis_tlast_i1   <= '0';
                    if(BRAM_BUF_PORTB_addr_counter >= (unsigned(DATA_WINDOW)-1)) then
                        BRAM_BUF_PORTB_read_dma_end     <= '1';
                        AVG_DMA_UPLOAD_frame <= std_logic_vector(AVG_LEVEL_counter);
                    end if;
                end if;
            end if;
        else
            BRAM_BUF_PORTB_addr_counter     <= (others => '0'); 
            BRAM_BUF_PORTB_first_read       <= '1';
            BRAM_BUF_PORTB_read_input_end   <= '0';
            BRAM_BUF_PORTB_read_dma_end     <= '0';
            mdma_axis_tvalid_i1             <= '0';
            mdma_axis_tlast_i1              <= '0';
        end if;          
    end if;
end process;

AVG_DMA_STATE <= "00" & SITUATION;

-- Averaging SM
avg_inst: process(s00_axis_aclk, s00_axis_aresetn) 
    variable wait_counter : integer range 0 to 128 :=0;
begin 
    if (s00_axis_aresetn = '0') then
        SITUATION             <= (others => '0');  
        s00_axis_tready       <= '0';
        -- Averaging
        AVG_LEVEL_counter     <= (others => '0');  
        AVG_LEVEL_i           <= (others => '0');  
        -- State
        AVG_STATE             <= "0000";
        wait_counter          := 0;
        state_avg <= idle;  
    elsif (s00_axis_aclk'event and s00_axis_aclk = '1') then
        case state_avg is        
            when idle =>
                AVG_STATE           <= "0000"; -- idle
                SITUATION           <= "00";
                s00_axis_tready     <= '1';
                AVG_LEVEL_i         <= AVG_LEVEL;
                if(s00_axis_tvalid = '1' and FILTERING = '1') then
                    SITUATION   <= "01";
                    state_avg   <= first_acc;
                end if; 
            when first_acc =>
                SITUATION   <= "01";
                AVG_STATE   <= "0001"; -- frame request
                if(s00_axis_tvalid = '1') then
                    AVG_STATE <= "0010"; -- accumulating
                end if;
                if(unsigned(AVG_LEVEL_i) < MIN_AVG_LEVEL) then
                    if(BRAM_ACC_PORTA_write_end = '1') then
                        state_avg           <= avg_wait;
                    end if;
                else
                    if(BRAM_ACC_PORTA_write_end = '1' and BRAM_ACC_PORTB_read_end = '1' and
                        BRAM_BUF_PORTA_write_end_acc = '1' and BRAM_BUF_PORTB_read_dma_end = '1'  ) then
                        AVG_LEVEL_counter   <= AVG_LEVEL_counter + 1;
                        state_avg           <= avg_wait;
                    end if;
                end if;
            when avg_wait =>  
                AVG_STATE <= "0011";  -- checking n avg          
                if(wait_counter > COUNTER_WAIT_1) then
                    --wait_counter := 0;
                    if(AVG_LEVEL_counter < unsigned(AVG_LEVEL_i) and (unsigned(AVG_LEVEL_i) >= MIN_AVG_LEVEL)) then
                        SITUATION   <= "01";
                        wait_counter := 0;
                        state_avg   <= first_acc;
                    else
                        AVG_STATE <= "0100"; -- wating for read
                        if(AVG_READ = '1') then  
                            wait_counter := 0;
                            state_avg   <= read_avg;
                        end if;
                    end if;
                else
                    SITUATION       <= "00";   
                    wait_counter    := wait_counter + 1;  
                end if;
            when pipe_line_acc =>
                SITUATION   <= "10";
                AVG_STATE   <= "0001"; -- frame request
                if(s00_axis_tvalid = '1') then
                    AVG_STATE <= "0010"; -- accumulating
                end if;
                -- check if operations finished
                if(BRAM_ACC_PORTA_write_end = '1' and BRAM_ACC_PORTB_read_end = '1' and
                    BRAM_BUF_PORTA_write_end_acc = '1' and BRAM_BUF_PORTB_read_dma_end = '1'  ) then
                    state_avg <= avg_wait;
                end if;
            when read_avg =>
                SITUATION   <= "11";
                if(gates_axis_tready = '1') then
                    AVG_STATE <= "0101"; -- reading
                end if;
                if(BRAM_ACC_PORTB_read_end = '1') then
                    AVG_STATE <= "0110"; -- read finished
                    if(wait_counter > COUNTER_WAIT_1) then
                        wait_counter := 0;
                        if(AVG_LEVEL_i = AVG_LEVEL) then
                            if(FILTERING = '0' or unsigned(AVG_LEVEL_i) < MIN_AVG_LEVEL) then 
                                SITUATION   <= "00";
                                state_avg   <= idle;
                            else
                                SITUATION   <= "10";
                                state_avg   <= pipe_line_acc;  
                            end if;
                        else
                            state_avg           <= idle; 
                        end if;
                    else
                        wait_counter    := wait_counter + 1;  
                    end if;
                end if;
            when others =>
                null;
        end case;
     end if; 
end process;

-- Delayed signals
delayed_inst: process(s00_axis_aclk, s00_axis_aresetn) 
begin 
    if (s00_axis_aresetn = '0') then
        -- input
        s00_axis_tvalid_i1  <= '0';
        s00_axis_tlast_i1   <= '0';
        s00_axis_tdata_i1   <= (others => '0');
        s00_axis_tvalid_i2  <= '0';
        s00_axis_tlast_i2   <= '0';
        s00_axis_tdata_i2   <= (others => '0');
        s00_axis_tvalid_i3  <= '0';
        s00_axis_tlast_i3   <= '0';
        s00_axis_tdata_i3   <= (others => '0');
        s00_axis_tdata_i4   <= (others => '0');
        s00_axis_tvalid_i4  <= '0';
        s00_axis_tlast_i4   <= '0';
        s00_axis_tvalid_i5  <= '0';
        s00_axis_tlast_i5   <= '0';
        s00_axis_tvalid_i6  <= '0';
        s00_axis_tlast_i6   <= '0';
        s00_axis_tvalid_i7  <= '0';
        s00_axis_tlast_i7   <= '0';
        --dma input
        sdma_axis_tvalid_i1  <= '0';
        sdma_axis_tlast_i1   <= '0';
        sdma_axis_tvalid_i2  <= '0';
        sdma_axis_tlast_i2   <= '0';
        sdma_axis_tvalid_i3  <= '0';
        sdma_axis_tlast_i3   <= '0';
        -- dma output
        mdma_axis_tvalid_i2 <= '0';
        mdma_axis_tlast_i2  <= '0';
        mdma_axis_tvalid    <= '0';
        mdma_axis_tlast     <= '0';
        mdma_axis_tdata     <= (others => '0');
        -- AVG signals
        AVG_GATES_tvalid_i1       <= '0';
        AVG_GATES_tlast_i1        <= '0';
        AVG_GATES_tvalid_i2       <= '0';
        AVG_GATES_tlast_i2        <= '0';
        AVG_ASCAN_tvalid_i1       <= '0';
        AVG_ASCAN_tlast_i1        <= '0';
        AVG_ASCAN_tvalid_i2       <= '0';
        AVG_ASCAN_tlast_i2        <= '0';            
    elsif (s00_axis_aclk'event and s00_axis_aclk = '1') then
        -- input
        s00_axis_tvalid_i1      <= s00_axis_tvalid;
        s00_axis_tlast_i1       <= s00_axis_tlast;
        s00_axis_tdata_i1       <= s00_axis_tdata;
        s00_axis_tvalid_i2      <= s00_axis_tvalid_i1;
        s00_axis_tlast_i2       <= s00_axis_tlast_i1;
        s00_axis_tdata_i2       <= s00_axis_tdata_i1;
        s00_axis_tvalid_i3      <= s00_axis_tvalid_i2;
        s00_axis_tlast_i3       <= s00_axis_tlast_i2;
        s00_axis_tdata_i3       <= s00_axis_tdata_i2;
        s00_axis_tvalid_i4      <= s00_axis_tvalid_i3;
        s00_axis_tlast_i4       <= s00_axis_tlast_i3;
        s00_axis_tdata_i4       <= s00_axis_tdata_i3;
        s00_axis_tvalid_i5      <= s00_axis_tvalid_i4;
        s00_axis_tlast_i5       <= s00_axis_tlast_i4;
        s00_axis_tvalid_i6      <= s00_axis_tvalid_i5;
        s00_axis_tlast_i6       <= s00_axis_tlast_i5;
        s00_axis_tvalid_i7      <= s00_axis_tvalid_i6;
        s00_axis_tlast_i7       <= s00_axis_tlast_i6;
        --dma input
        sdma_axis_tvalid_i1     <= sdma_axis_tvalid;
        sdma_axis_tlast_i1      <= sdma_axis_tlast;
        sdma_axis_tvalid_i2     <= sdma_axis_tvalid_i1;
        sdma_axis_tlast_i2      <= sdma_axis_tlast_i1;
        sdma_axis_tvalid_i3     <= sdma_axis_tvalid_i2;
        sdma_axis_tlast_i3      <= sdma_axis_tlast_i1;
        -- dma output
        mdma_axis_tvalid_i2     <= mdma_axis_tvalid_i1;
        mdma_axis_tlast_i2      <= mdma_axis_tlast_i1;
        mdma_axis_tvalid        <= mdma_axis_tvalid_i2;
        mdma_axis_tlast         <= mdma_axis_tlast_i2; 
        mdma_axis_tdata         <= BRAM_BUF_PORTB_dout(13) & BRAM_BUF_PORTB_dout(13) & BRAM_BUF_PORTB_dout;
        -- AVG signals
        AVG_GATES_tvalid_i1           <= AVG_GATES_tvalid;
        AVG_GATES_tlast_i1            <= AVG_GATES_tlast;
        AVG_GATES_tvalid_i2           <= AVG_GATES_tvalid_i1;
        AVG_GATES_tlast_i2            <= AVG_GATES_tlast_i1;
        AVG_ASCAN_tvalid_i1           <= AVG_ASCAN_tvalid;
        AVG_ASCAN_tlast_i1            <= AVG_ASCAN_tlast;
        AVG_ASCAN_tvalid_i2           <= AVG_ASCAN_tvalid_i1;
        AVG_ASCAN_tlast_i2            <= AVG_ASCAN_tlast_i1;
    end if;
end process;   
 
-- Ascan output (result)
ascan_output_inst: process(s00_axis_aclk, s00_axis_aresetn)
begin 
    if (s00_axis_aresetn = '0') then
        ascan_axis_tvalid <= '0';
        ascan_axis_tlast <= '0';
        ascan_axis_tdata <= (others => '0');
    elsif (s00_axis_aclk'event and s00_axis_aclk = '1') then
        if(AVG_ASCAN_tvalid_i2 = '1') then 
            ascan_axis_tvalid <= AVG_ASCAN_tvalid_i2;
            ascan_axis_tlast  <= AVG_ASCAN_tlast_i2;
            --m00_axis_tdata  <= BRAM_ACC_PORTA_dout(15+log2(AVG_LEVEL_i) downto (0+log2(AVG_LEVEL_i));
            if(unsigned(AVG_LEVEL_i) = 2) then
                ascan_axis_tdata  <= BRAM_ACC_PORTB_dout(16 downto 1);
            elsif(unsigned(AVG_LEVEL_i) = 4) then
                ascan_axis_tdata  <= BRAM_ACC_PORTB_dout(17 downto 2);
            elsif(unsigned(AVG_LEVEL_i) = 8) then
                ascan_axis_tdata  <= BRAM_ACC_PORTB_dout(18 downto 3);
            elsif(unsigned(AVG_LEVEL_i) = 16) then
                ascan_axis_tdata  <= BRAM_ACC_PORTB_dout(19 downto 4); 
            elsif(unsigned(AVG_LEVEL_i) = 32) then
                ascan_axis_tdata  <= BRAM_ACC_PORTB_dout(20 downto 5);
            elsif(unsigned(AVG_LEVEL_i) = 64) then
                ascan_axis_tdata  <= BRAM_ACC_PORTB_dout(21 downto 6);
            else
                ascan_axis_tdata  <= BRAM_ACC_PORTB_dout(15 downto 0);
            end if;
        else
            ascan_axis_tvalid <= '0';
            ascan_axis_tlast <= '0';
            ascan_axis_tdata <= (others => '0');
        end if;
    end if;
end process; 

-- Gates output (result)
gates_output_inst: process(s00_axis_aclk, s00_axis_aresetn)
begin 
    if (s00_axis_aresetn = '0') then
        gates_axis_tvalid <= '0';
        gates_axis_tlast <= '0';
        gates_axis_tdata <= (others => '0');
    elsif (s00_axis_aclk'event and s00_axis_aclk = '1') then
        if(unsigned(AVG_LEVEL_i) >= MIN_AVG_LEVEL) then 
            gates_axis_tvalid <= AVG_GATES_tvalid_i2;
            gates_axis_tlast  <= AVG_GATES_tlast_i2;
            --m00_axis_tdata  <= BRAM_ACC_PORTA_dout(15+log2(AVG_LEVEL_i) downto (0+log2(AVG_LEVEL_i));
            if(unsigned(AVG_LEVEL_i) = 2) then
                gates_axis_tdata  <= BRAM_ACC_PORTA_dout(16 downto 1);
            elsif(unsigned(AVG_LEVEL_i) = 4) then
                gates_axis_tdata  <= BRAM_ACC_PORTA_dout(17 downto 2);
            elsif(unsigned(AVG_LEVEL_i) = 8) then
                gates_axis_tdata  <= BRAM_ACC_PORTA_dout(18 downto 3);
            elsif(unsigned(AVG_LEVEL_i) = 16) then
                gates_axis_tdata  <= BRAM_ACC_PORTA_dout(19 downto 4); 
            elsif(unsigned(AVG_LEVEL_i) = 32) then
                gates_axis_tdata  <= BRAM_ACC_PORTA_dout(20 downto 5);
            elsif(unsigned(AVG_LEVEL_i) = 64) then
                gates_axis_tdata  <= BRAM_ACC_PORTA_dout(21 downto 6);
            else
                gates_axis_tdata  <= BRAM_ACC_PORTA_dout(15 downto 0);
            end if;
        else
            gates_axis_tvalid <= s00_axis_tvalid;
            gates_axis_tlast <= s00_axis_tlast;
            gates_axis_tdata <= s00_axis_tdata;
        end if;
    end if;
end process;

-- Accumulator BRAM
average_filter_ACC_BRAM_inst: average_filter_ACC_BRAM
    PORT MAP (
      clka => s00_axis_aclk,
      ena => BRAM_ACC_PORTA_en,
      wea => BRAM_ACC_PORTA_we,
      addra => BRAM_ACC_PORTA_addr,
      dina => BRAM_ACC_PORTA_din,
      douta => BRAM_ACC_PORTA_dout,
      clkb => s00_axis_aclk,
      enb => BRAM_ACC_PORTB_en,
      web => BRAM_ACC_PORTB_we,
      addrb => BRAM_ACC_PORTB_addr,
      dinb => BRAM_ACC_PORTB_din,
      doutb => BRAM_ACC_PORTB_dout
    );
  
-- Buffer BRAM
average_filter_BUF_BRAM_inst: average_filter_BUF_BRAM
    PORT MAP (
      clka => s00_axis_aclk,
      ena => BRAM_BUF_PORTA_en,
      wea => BRAM_BUF_PORTA_we,
      addra => BRAM_BUF_PORTA_addr,
      dina => BRAM_BUF_PORTA_din,
      douta => BRAM_BUF_PORTA_dout,
      clkb => s00_axis_aclk,
      enb => BRAM_BUF_PORTB_en,
      web => BRAM_BUF_PORTB_we,
      addrb => BRAM_BUF_PORTB_addr,
      dinb => BRAM_BUF_PORTB_din,
      doutb => BRAM_BUF_PORTB_dout
    );

end arch_imp;
