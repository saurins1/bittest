----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/29/2017 07:53:20 PM
-- Design Name: 
-- Module Name: AIRBUS_AVG_FILTER_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity average_filter_tb is
  	generic (
    REGISTER_WIDTH              : integer    := 32;
        COUNTER_WAIT_1              : integer    := 16;
    COUNTER_WAIT_2              : integer    := 32;
    STATE                       : integer    := 4;
    MIN_AVG_LEVEL               : integer    := 2;
    C_S00_AXIS_TDATA_WIDTH    : integer    := 16;
    C_GATES_AXIS_TDATA_WIDTH    : integer    := 16;
    C_GATES_AXIS_START_COUNT    : integer    := 16;
    C_ASCAN_AXIS_TDATA_WIDTH    : integer    := 16;
    C_ASCAN_AXIS_START_COUNT    : integer    := 16;
    C_SDMA_AXIS_TDATA_WIDTH    : integer    := 16;
    C_MDMA_AXIS_TDATA_WIDTH    : integer    := 16;
    C_MDMA_AXIS_START_COUNT    : integer    := 16
);
end;

architecture bench of average_filter_tb is

 component average_filter
  	generic (
          REGISTER_WIDTH              : integer    := 32;
          COUNTER_WAIT_1              : integer    := 16;
          COUNTER_WAIT_2              : integer    := 32;
          STATE                       : integer    := 4;
          MIN_AVG_LEVEL               : integer    := 2;
  		C_S00_AXIS_TDATA_WIDTH	: integer	:= 16;
  		C_sdma_AXIS_TDATA_WIDTH	: integer	:= 16;
  		C_ascan_AXIS_TDATA_WIDTH	: integer	:= 16;
  		C_ascan_AXIS_START_COUNT	: integer	:= 32;
  		C_gates_AXIS_TDATA_WIDTH	: integer	:= 16;
  		C_gates_AXIS_START_COUNT	: integer	:= 32;
  		C_mdma_AXIS_TDATA_WIDTH	: integer	:= 16;
  		C_mdma_AXIS_START_COUNT	: integer	:= 32
  	);
  	port (
          DATA_WINDOW         : in std_logic_vector(REGISTER_WIDTH-1 downto 0);
          FILTERING            : in std_logic; 
          AVG_LEVEL            : in std_logic_vector(REGISTER_WIDTH-1 downto 0);
          AVG_READ            : in std_logic;
          AVG_STATE           : out std_logic_vector(STATE-1 downto 0); 
          AVG_DMA_STATE       : out std_logic_vector(STATE-1 downto 0);
          AVG_DMA_UPLOAD_frame    : out std_logic_vector(REGISTER_WIDTH-1 downto 0);
  		s00_axis_aclk	: in std_logic;
  		s00_axis_aresetn	: in std_logic;
  		s00_axis_tready	: out std_logic;
  		s00_axis_tdata	: in std_logic_vector(C_S00_AXIS_TDATA_WIDTH-1 downto 0);
  		s00_axis_tstrb	: in std_logic_vector((C_S00_AXIS_TDATA_WIDTH/8)-1 downto 0);
  		s00_axis_tlast	: in std_logic;
  		s00_axis_tvalid	: in std_logic;
  		sdma_axis_aclk	: in std_logic;
  		sdma_axis_aresetn	: in std_logic;
  		sdma_axis_tready	: out std_logic;
  		sdma_axis_tdata	: in std_logic_vector(C_sdma_AXIS_TDATA_WIDTH-1 downto 0);
  		sdma_axis_tstrb	: in std_logic_vector((C_sdma_AXIS_TDATA_WIDTH/8)-1 downto 0);
  		sdma_axis_tlast	: in std_logic;
  		sdma_axis_tvalid	: in std_logic;
  		ascan_axis_aclk	: in std_logic;
  		ascan_axis_aresetn	: in std_logic;
  		ascan_axis_tvalid	: out std_logic;
  		ascan_axis_tdata	: out std_logic_vector(C_ascan_AXIS_TDATA_WIDTH-1 downto 0);
  		ascan_axis_tstrb	: out std_logic_vector((C_ascan_AXIS_TDATA_WIDTH/8)-1 downto 0);
  		ascan_axis_tlast	: out std_logic;
  		ascan_axis_tready	: in std_logic;
  		gates_axis_aclk	: in std_logic;
  		gates_axis_aresetn	: in std_logic;
  		gates_axis_tvalid	: out std_logic;
  		gates_axis_tdata	: out std_logic_vector(C_gates_AXIS_TDATA_WIDTH-1 downto 0);
  		gates_axis_tstrb	: out std_logic_vector((C_gates_AXIS_TDATA_WIDTH/8)-1 downto 0);
  		gates_axis_tlast	: out std_logic;
  		gates_axis_tready	: in std_logic;
  		mdma_axis_aclk	: in std_logic;
  		mdma_axis_aresetn	: in std_logic;
  		mdma_axis_tvalid	: out std_logic;
  		mdma_axis_tdata	: out std_logic_vector(C_mdma_AXIS_TDATA_WIDTH-1 downto 0);
  		mdma_axis_tstrb	: out std_logic_vector((C_mdma_AXIS_TDATA_WIDTH/8)-1 downto 0);
  		mdma_axis_tlast	: out std_logic;
  		mdma_axis_tready	: in std_logic
  	);
  end component;

  signal DATA_WINDOW: std_logic_vector(REGISTER_WIDTH-1 downto 0);
  signal FILTERING: std_logic;
  signal AVG_LEVEL: std_logic_vector(REGISTER_WIDTH-1 downto 0);
  signal AVG_READ: std_logic;
  signal AVG_STATE: std_logic_vector(STATE-1 downto 0);
  signal AVG_DMA_STATE: std_logic_vector(STATE-1 downto 0);
  signal AVG_DMA_UPLOAD_frame: std_logic_vector(REGISTER_WIDTH-1 downto 0);
  signal s00_axis_tready: std_logic;
  signal s00_axis_tdata: std_logic_vector(C_S00_AXIS_TDATA_WIDTH-1 downto 0);
  signal s00_axis_tstrb: std_logic_vector((C_S00_AXIS_TDATA_WIDTH/8)-1 downto 0);
  signal s00_axis_tlast: std_logic;
  signal s00_axis_tvalid: std_logic;
  signal sdma_axis_tready: std_logic;
  signal sdma_axis_tdata: std_logic_vector(C_sdma_AXIS_TDATA_WIDTH-1 downto 0);
  signal sdma_axis_tstrb: std_logic_vector((C_sdma_AXIS_TDATA_WIDTH/8)-1 downto 0);
  signal sdma_axis_tlast: std_logic;
  signal sdma_axis_tvalid: std_logic;
  signal ascan_axis_tvalid: std_logic;
  signal ascan_axis_tdata: std_logic_vector(C_ascan_AXIS_TDATA_WIDTH-1 downto 0);
  signal ascan_axis_tstrb: std_logic_vector((C_ascan_AXIS_TDATA_WIDTH/8)-1 downto 0);
  signal ascan_axis_tlast: std_logic;
  signal ascan_axis_tready: std_logic;
  signal gates_axis_tvalid: std_logic;
  signal gates_axis_tdata: std_logic_vector(C_gates_AXIS_TDATA_WIDTH-1 downto 0);
  signal gates_axis_tstrb: std_logic_vector((C_gates_AXIS_TDATA_WIDTH/8)-1 downto 0);
  signal gates_axis_tlast: std_logic;
  signal gates_axis_tready: std_logic;
  signal mdma_axis_tvalid: std_logic;
  signal mdma_axis_tdata: std_logic_vector(C_mdma_AXIS_TDATA_WIDTH-1 downto 0);
  signal mdma_axis_tstrb: std_logic_vector((C_mdma_AXIS_TDATA_WIDTH/8)-1 downto 0);
  signal mdma_axis_tlast: std_logic;
  signal mdma_axis_tready: std_logic ;

  signal aclk: STD_LOGIC := '0';
  signal aresetn: STD_LOGIC := '0';
  
  constant aclk_period: time := 10 ns;
  
  -- LOAD DATA
  signal begin_load: STD_LOGIC := '0';
  type states_load is (idle, load_input, load_wait); 
  signal state_load : states_load; 
  
  signal load_counter : unsigned(15 downto 0) := (others => '0'); 
  signal load_counter_offset : unsigned(15 downto 0) := (others => '0'); 
  signal wait_counter : unsigned(15 downto 0) := (others => '0'); 

begin

  -- Insert values for generic parameters !!
  uut: average_filter generic map ( REGISTER_WIDTH           => REGISTER_WIDTH,
                                           COUNTER_WAIT_1           => COUNTER_WAIT_1,
                                           COUNTER_WAIT_2           => COUNTER_WAIT_2,
                                           STATE                    => STATE,
                                           MIN_AVG_LEVEL            => MIN_AVG_LEVEL,
                                           C_S00_AXIS_TDATA_WIDTH   => C_S00_AXIS_TDATA_WIDTH,
                                           C_sdma_AXIS_TDATA_WIDTH  => C_sdma_AXIS_TDATA_WIDTH,
                                           C_ascan_AXIS_TDATA_WIDTH => C_ascan_AXIS_TDATA_WIDTH,
                                           C_ascan_AXIS_START_COUNT => C_ascan_AXIS_START_COUNT,
                                           C_gates_AXIS_TDATA_WIDTH => C_gates_AXIS_TDATA_WIDTH,
                                           C_gates_AXIS_START_COUNT => C_gates_AXIS_START_COUNT,
                                           C_mdma_AXIS_TDATA_WIDTH  => C_mdma_AXIS_TDATA_WIDTH,
                                           C_mdma_AXIS_START_COUNT  => C_mdma_AXIS_START_COUNT )
                                port map ( DATA_WINDOW              => DATA_WINDOW,
                                           FILTERING                => FILTERING,
                                           AVG_LEVEL                => AVG_LEVEL,
                                           AVG_READ                 => AVG_READ,
                                           AVG_STATE                => AVG_STATE,
                                           AVG_DMA_STATE            => AVG_DMA_STATE,
                                           AVG_DMA_UPLOAD_frame     => AVG_DMA_UPLOAD_frame,
                                           s00_axis_aclk            => aclk,
                                           s00_axis_aresetn         => aresetn,
                                           s00_axis_tready          => s00_axis_tready,
                                           s00_axis_tdata           => s00_axis_tdata,
                                           s00_axis_tstrb           => s00_axis_tstrb,
                                           s00_axis_tlast           => s00_axis_tlast,
                                           s00_axis_tvalid          => s00_axis_tvalid,
                                           sdma_axis_aclk           => aclk,
                                           sdma_axis_aresetn        => aresetn,
                                           sdma_axis_tready         => sdma_axis_tready,
                                           sdma_axis_tdata          => sdma_axis_tdata,
                                           sdma_axis_tstrb          => sdma_axis_tstrb,
                                           sdma_axis_tlast          => sdma_axis_tlast,
                                           sdma_axis_tvalid         => sdma_axis_tvalid,
                                           ascan_axis_aclk          => aclk,
                                           ascan_axis_aresetn       => aresetn,
                                           ascan_axis_tvalid        => ascan_axis_tvalid,
                                           ascan_axis_tdata         => ascan_axis_tdata,
                                           ascan_axis_tstrb         => ascan_axis_tstrb,
                                           ascan_axis_tlast         => ascan_axis_tlast,
                                           ascan_axis_tready        => ascan_axis_tready,
                                           gates_axis_aclk          => aclk,
                                           gates_axis_aresetn       => aresetn,
                                           gates_axis_tvalid        => gates_axis_tvalid,
                                           gates_axis_tdata         => gates_axis_tdata,
                                           gates_axis_tstrb         => gates_axis_tstrb,
                                           gates_axis_tlast         => gates_axis_tlast,
                                           gates_axis_tready        => gates_axis_tready,
                                           mdma_axis_aclk           => aclk,
                                           mdma_axis_aresetn        => aresetn,
                                           mdma_axis_tvalid         => mdma_axis_tvalid,
                                           mdma_axis_tdata          => mdma_axis_tdata,
                                           mdma_axis_tstrb          => mdma_axis_tstrb,
                                           mdma_axis_tlast          => mdma_axis_tlast,
                                           mdma_axis_tready         => mdma_axis_tready );
  -- stimulus
  stimulus: process
  begin
    aresetn <= '0';
    gates_axis_tready <= '1';
    ascan_axis_tready <= '1';
    AVG_LEVEL   <= "00000000000000000000000000010000"; --16
    --AVG_LEVEL   <= "00000000000000000000000000000010"; --2
    --AVG_LEVEL   <= "00000000000000000000000000000001"; --1
    --AVG_LEVEL   <= "00000000000000000000000000000000"; --0
    DATA_WINDOW <= "00000000000000000000000010000000"; --128
    FILTERING <= '1';
    begin_load <= '0';
    wait for aclk_period*10;
    aresetn <= '1';
    wait for aclk_period*10; 
    begin_load <= '1';
    wait for aclk_period*10; 
    begin_load <= '0';
    wait;
  end process;

   -- aclk process 
   aclk_period_process :process
   begin
    aclk <= not aclk;
    wait for aclk_period/2;
    aclk <= not aclk;
    wait for aclk_period/2;
   end process;
   
  load_data_inst_inst: process(aclk, aresetn)                         
     begin 
       if (aresetn = '0') then
           s00_axis_tdata <= (others => '0');
           s00_axis_tlast <= '0';
           s00_axis_tvalid <= '0';
           sdma_axis_tdata <= (others => '0');
           sdma_axis_tlast <= '0';
           sdma_axis_tvalid <= '0';
           mdma_axis_tready <= '0';
           state_load <= idle;  
           load_counter <= (others => '0');
           load_counter_offset   <= (others => '0');
           wait_counter  <= (others => '0');
       elsif (aclk'event and aclk = '1') then
           case state_load is  
               when idle =>  
                   mdma_axis_tready <= '1';    
                   if(begin_load = '1') then
                       state_load <= load_input;  
                       s00_axis_tdata <= (others => '0');
                       s00_axis_tlast <= '0';
                       s00_axis_tvalid <= '0'; 
                   end if;
               when load_input =>   
                    sdma_axis_tvalid <= '0'; 
                    sdma_axis_tlast <= '0';
                    if(wait_counter >= 1024) then
                        if(s00_axis_tready = '1') then            
                            s00_axis_tvalid <= '1'; 
                            load_counter <= load_counter + 1;
                            s00_axis_tdata <= std_logic_vector(load_counter);
                            if((load_counter) >=  (unsigned(DATA_WINDOW(15 downto 0))-1)) then
                                s00_axis_tlast <= '1';
                                state_load <= load_wait; 
                                wait_counter  <= (others => '0');
                                load_counter <= (others => '0');
                            end if;    
                        else
                            s00_axis_tdata  <= (others => '0');
                            s00_axis_tlast  <= '0';
                            s00_axis_tvalid <= '0';
                        end if; 
                    else
                        wait_counter <= wait_counter + 1;
                    end if;                                                     
               when load_wait =>   
                   s00_axis_tvalid <= '0'; 
                   s00_axis_tlast <= '0';
                   if(wait_counter >= 1024) then
                        if(sdma_axis_tready = '1') then            
                            sdma_axis_tvalid <= '1'; 
                            load_counter <= load_counter + 1;
                            sdma_axis_tdata <= std_logic_vector(load_counter);
                            if((load_counter) >=  (unsigned(DATA_WINDOW(15 downto 0))-1)) then
                                sdma_axis_tlast <= '1';
                                state_load <= load_input; 
                                wait_counter <= (others =>'0');
                                load_counter <= (others => '0');
                            end if;    
                        else
                            sdma_axis_tdata  <= (others => '0');
                            sdma_axis_tlast  <= '0';
                            sdma_axis_tvalid <= '0'; 
                        end if; 
                   else
                        wait_counter <= wait_counter + 1;
                   end if;
               when others =>
                   null;                   
           end case;                                                                                            
       end if;
     end process;  
     
    read_ascan_process: process(aclk, aresetn)                         
        begin 
          if (aresetn = '0') then
            AVG_READ <= '0';   
          elsif (aclk'event and aclk = '1') then
            if(AVG_STATE  = "0100") then
                AVG_READ <= '1';
            else
                AVG_READ <= '0';
            end if;
          end if;
    end process;
end;
