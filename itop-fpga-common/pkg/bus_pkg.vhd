library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
package bus_pkg is
        --type bus_array is array(natural range <>) of std_logic_vector;
	type bus_array_2_32 is array( 0 to 1 ) of std_logic_vector(31 downto 0);
	type bus_array_2_16 is array( 0 to 1 ) of std_logic_vector(15 downto 0);
	type bus_array_2_8  is array( 0 to 1 ) of std_logic_vector(7 downto 0);
	type bus_array_4_32 is array( 0 to 3 ) of std_logic_vector(31 downto 0);
	type bus_array_4_16 is array( 0 to 3 ) of std_logic_vector(15 downto 0);
	type bus_array_4_8  is array( 0 to 3 ) of std_logic_vector(7 downto 0);
end package;

