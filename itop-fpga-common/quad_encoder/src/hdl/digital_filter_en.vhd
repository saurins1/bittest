----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/30/2016 06:32:08 PM
-- Design Name: 
-- Module Name: digital_filter_en - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee; 
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity digital_filter_en is
    Port ( 
        aclk : in std_logic; 
        arstn : in std_logic; 
        din : in std_logic; 
        ce : in std_logic; 
        dout : out std_logic);
end digital_filter_en;

architecture Behavioral of digital_filter_en is

    signal tap :std_logic_vector(3 downto 0):= (others => '0');
    signal all_ones:std_logic := '0';
    signal all_zeros:std_logic := '0';

begin

    process(aclk, arstn)
	begin 
	   if (arstn = '0') then
           tap      <= (others => '0');
	   elsif (aclk'event and aclk = '1') then	     
	       if(ce = '1') then
		      tap <= tap(2 downto 0) & din;
		   end if;        
	   end if;
	end process;
	
	all_ones  <= '1' when (tap = "1111") else '0';
    all_zeros <= '1' when (tap = "0000") else '0';
    
    process(aclk, arstn)
    begin 
       if (arstn = '0') then
           dout      <= '0';
       elsif (aclk'event and aclk = '1') then         
           if(all_ones = '1') then
              dout <= '1';
           elsif(all_zeros = '1') then
              dout <= '0';
           end if;        
       end if;
    end process;    

end Behavioral;

