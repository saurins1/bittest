library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity quad_encoder_tb is
end;

architecture bench of quad_encoder_tb is
 
  component quad_encoder is
      Port (  aclk           : in STD_LOGIC; 
              arstn          : in STD_LOGIC; 
              ENC_EN         : in STD_LOGIC; 
              ENC_CHA        : in STD_LOGIC; 
              ENC_CHB        : in STD_LOGIC; 
              ENC_PULSE_ROUND: in STD_LOGIC_VECTOR (31 downto 0);  -- Pulses per round
              ENC_POS_RST    : in STD_LOGIC; 
              ENC_DIR        : out STD_LOGIC_VECTOR (1 downto 0);  -- Direction 
              ENC_SPEED      : out STD_LOGIC_VECTOR (31 downto 0); -- Clock cycles per round
              ENC_POS        : out STD_LOGIC_VECTOR (31 downto 0)  -- Distance in pulses (ticks)
      );
  end component quad_encoder;

  signal aclk: STD_LOGIC := '0';
  signal arstn: STD_LOGIC := '0';
  signal ENC_EN: STD_LOGIC := '0';
  signal ENC_CHA: STD_LOGIC := '0';
  signal ENC_CHB: STD_LOGIC := '0';
  signal ENC_PULSE_ROUND: STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
  signal ENC_POS_RST: STD_LOGIC := '0';
  signal ENC_DIR: STD_LOGIC_VECTOR (1 downto 0) := (others => '0');
  signal ENC_SPEED: STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
  signal ENC_POS: STD_LOGIC_VECTOR (31 downto 0) := (others => '0');

  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean := false;
  
  signal en_engine: STD_LOGIC := '0';
  
  
  --constant encoder_period: time := 3600 us;
  --signal encoder_period: time := 360 us;
  signal encoder_period: time := 13020 us; --3255*4
  
  signal dir_control: STD_LOGIC := '0';

begin

  uut: quad_encoder port map ( aclk     => aclk,
                              arstn     => arstn,
                              ENC_EN    => ENC_EN,
                              ENC_CHA   => ENC_CHA,
                              ENC_CHB   => ENC_CHB,
                              ENC_PULSE_ROUND       => ENC_PULSE_ROUND,
                              ENC_POS_RST=> ENC_POS_RST,
                              ENC_DIR   => ENC_DIR,
                              ENC_SPEED => ENC_SPEED,
                              ENC_POS   => ENC_POS );

  stimulus: process
  begin
    arstn <= '0';
    ENC_EN <= '1';
    en_engine <= '0';
    dir_control <= '0';
    encoder_period <= 3255 us * 4;
    ENC_POS_RST <= '0';
    ENC_PULSE_ROUND <= "00000000000000000000000000101111";
    -- Put initialisation code here   
    wait for 100 ns;
    -- Put test bench stimulus code here
    arstn <= '1';
    wait for clock_period*25000;
    en_engine <= '1';
    wait for encoder_period*25;
    en_engine <= '0';
    wait for encoder_period*25;
    en_engine <= '1';
    encoder_period <= 6510 us * 4;
    wait for encoder_period*25;
    en_engine <= '0';
    wait for encoder_period*25;
    en_engine <= '1';
    encoder_period <= 1627 us * 4;
    wait for encoder_period*25;
    dir_control <= '1';
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      aclk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;
  
  cha_generation: process
  begin
    if (en_engine = '1') then
        if(dir_control = '0') then
            ENC_CHA <= '0';
            wait for encoder_period/4;
            ENC_CHA <= '1';
            wait for encoder_period/2;
            ENC_CHA <= '0';
            wait for encoder_period/4;
        else
            ENC_CHA <= '1';
            wait for encoder_period/2;
            ENC_CHA <= '0';
            wait for encoder_period/2; 
        end if;
    else
        ENC_CHA <= '0';
        wait for encoder_period;
    end if;
  end process;
  
  chb_generation: process
  begin
    if (en_engine = '1') then
        if(dir_control = '0') then
            ENC_CHB <= '1';
            wait for encoder_period/2;
            ENC_CHB <= '0';
            wait for encoder_period/2;
        else
            ENC_CHB <= '0';
            wait for encoder_period/4;
            ENC_CHB <= '1';
            wait for encoder_period/2;
            ENC_CHB <= '0';
            wait for encoder_period/4;
        end if;
    else
        ENC_CHB <= '0';
        wait for encoder_period;
    end if;
  end process;

end;