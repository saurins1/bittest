----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/27/2017 09:37:32 AM
-- Design Name: 
-- Module Name: pulser_receiver_sim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity pulser_receiver_sim is
	generic (
        REGISTER_WIDTH      : integer    := 32;
        TDATA_WIDTH         : integer    := 16;
        -- RANDOM NUMBER GEN
        W : integer := 16;					-- LFSR scaleable from 24 down to 4 bits
        V : integer := 18;                    -- LFSR for non uniform clocking scalable from 24 down to 18 bit
        g_type : integer := 0;            -- gausian distribution type, 0 = unimodal, 1 = bimodal, from g_noise_out
        u_type : integer := 1            -- uniform distribution type, 0 = uniform, 1 =  ave-uniform, from u_noise_out
    );
    Port ( 
        aclk                                : in std_logic;
        aresetn                             : in std_logic;
        -- LOAD SIGNAL
        LOAD_SIGNAL                         : in std_logic;
        SIGNAL_INDEX                        : in std_logic_vector(REGISTER_WIDTH-1 downto 0);   
        SIGNAL_VALUE                        : in std_logic_vector(REGISTER_WIDTH-1 downto 0);
        -- PULSER/RECEIVER
        PULSER_RECEIVER_SIM_BEGIN               : in std_logic;
        PULSER_RECEIVER_SIM_END                 : out std_logic;
        DATA_WINDOW                         : in std_logic_vector(REGISTER_WIDTH-1 downto 0);
        -- SIGNAL STREAM
        SIGNAL_TVALID       : out std_logic;
        SIGNAL_TDATA        : out std_logic_vector(TDATA_WIDTH-1 downto 0);
        SIGNAL_TLAST        : out std_logic;
        SIGNAL_TREADY       : in std_logic
        
    );
end pulser_receiver_sim;

architecture Behavioral of pulser_receiver_sim is

    signal LOAD_SIGNAL_i1  : std_logic;
    signal LOAD_SIGNAL_i2  : std_logic;
    
    signal SIGNAL_TVALID_i1       : std_logic;
    signal SIGNAL_TLAST_i1        : std_logic;
    
    signal SIGNAL_TVALID_i2       : std_logic;
    signal SIGNAL_TLAST_i2        : std_logic;
    
    signal SIGNAL_TVALID_i3       : std_logic;
    signal SIGNAL_TLAST_i3        : std_logic;
    
    signal SIGNAL_TDATA_i1        : std_logic_vector(TDATA_WIDTH-1 downto 0);

    COMPONENT pulser_receiver_sim_BRAM
      PORT (
        clka : IN STD_LOGIC;
        ena : IN STD_LOGIC;
        wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addra : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        dina : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
        douta : OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
        clkb : IN STD_LOGIC;
        enb : IN STD_LOGIC;
        web : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addrb : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        dinb : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
        doutb : OUT STD_LOGIC_VECTOR(13 DOWNTO 0)
      );
    END COMPONENT;
    
    signal BRAM_PORTA_addr : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal BRAM_PORTA_din : STD_LOGIC_VECTOR ( 13 downto 0 );
    signal BRAM_PORTA_dout : STD_LOGIC_VECTOR ( 13 downto 0 );
    signal BRAM_PORTA_en : STD_LOGIC;
    signal BRAM_PORTA_we : STD_LOGIC_VECTOR ( 0 to 0 );
    
    signal BRAM_PORTB_addr : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal BRAM_PORTB_addr_counter : UNSIGNED ( 15 downto 0 );
    signal BRAM_PORTB_din : STD_LOGIC_VECTOR ( 13 downto 0 );
    signal BRAM_PORTB_dout : STD_LOGIC_VECTOR ( 13 downto 0 );
    signal BRAM_PORTB_en : STD_LOGIC;
    signal BRAM_PORTB_we : STD_LOGIC_VECTOR ( 0 to 0 );
    signal BRAM_PORTB_first_read : STD_LOGIC;
    
    COMPONENT random_number_generator is
        Generic (
            W : integer := 16;                    -- LFSR scaleable from 24 down to 4 bits
            V : integer := 18;                    -- LFSR for non uniform clocking scalable from 24 down to 18 bit
            g_type : integer := 0;            -- gausian distribution type, 0 = unimodal, 1 = bimodal, from g_noise_out
            u_type : integer := 1            -- uniform distribution type, 0 = uniform, 1 =  ave-uniform, from u_noise_out
        );
        Port ( 
            aclk              :         in  STD_LOGIC;
            aresetn           :         in  STD_LOGIC;
            enable            :         in  STD_LOGIC;
            level             :        in STD_LOGIC_VECTOR (4 downto 0);
            g_noise_out     :        out STD_LOGIC_VECTOR (W-1 downto 0);                    -- port for bimodal/unimodal gaussian distributions
            u_noise_out     :         out  STD_LOGIC_VECTOR (W-1 downto 0)                    -- port for uniform/ave-uniform distributions
        );
    end COMPONENT random_number_generator;    
    
    signal random_number   : std_logic_vector (W-1 downto 0);  
    
    -- READ state machine
    type states_read is (idle, reading, read_end); 
    signal state_read : states_read;

begin

    -- LOAD SIGNAL
    load_signal_process: process(aclk, aresetn)
    begin 
        if (aresetn = '0') then
            LOAD_SIGNAL_i1  <= '0';
            LOAD_SIGNAL_i2  <= '0';
            BRAM_PORTA_we   <= (others => '1');
            BRAM_PORTA_en   <= '0';   
            BRAM_PORTA_din  <= (others => '0');   
            BRAM_PORTA_addr <= (others => '0');                    
        elsif (aclk'event and aclk = '1') then
            LOAD_SIGNAL_i1 <= LOAD_SIGNAL;
            LOAD_SIGNAL_i2 <= LOAD_SIGNAL_i1;
            if(LOAD_SIGNAL = '1') then
                BRAM_PORTA_en <= '1';
                BRAM_PORTA_din <= SIGNAL_VALUE(13 downto 0);
                BRAM_PORTA_addr <= SIGNAL_INDEX(15 downto 0);
            else
                -- Its neccesay hold BRAM_PORTA_en at least 2 clocks for write value
                if(LOAD_SIGNAL_i2 = '0') then
                    BRAM_PORTA_en <= '0';
                end if;
            end if;                                                                                         
        end if;
    end process;
    
    BRAM_PORTB_addr <= std_logic_vector(BRAM_PORTB_addr_counter);
    
    -- READ SIGNAL
    read_signal_process: process(aclk, aresetn)
    begin 
        if (aresetn = '0') then
            SIGNAL_TVALID_i1 <= '0';
            SIGNAL_TLAST_i1 <= '0';
            BRAM_PORTB_we   <= (others => '0'); 
            BRAM_PORTB_din  <= (others => '0'); 
            BRAM_PORTB_en   <= '1'; 
            BRAM_PORTB_addr_counter <= (others => '0'); 
            BRAM_PORTB_first_read <= '1';
            PULSER_RECEIVER_SIM_END <= '0';
            state_read <= idle;                  
        elsif (aclk'event and aclk = '1') then
            case state_read is        
                when idle =>
                    if(PULSER_RECEIVER_SIM_BEGIN = '1') then
                        state_read <= reading;   
                    end if;
                when reading =>
                    if(SIGNAL_TREADY = '1') then
                        SIGNAL_TVALID_i1 <= '1';
                        SIGNAL_TLAST_i1 <= '0';
                        if(BRAM_PORTB_first_read = '1') then
                            BRAM_PORTB_addr_counter <= (others => '0'); 
                            BRAM_PORTB_first_read <= '0';
                        else
                            BRAM_PORTB_addr_counter <= BRAM_PORTB_addr_counter + 1;
                        end if;
                        if(BRAM_PORTB_addr_counter >= (unsigned(DATA_WINDOW)-2)) then
                            SIGNAL_TLAST_i1 <= '1';
                            if(BRAM_PORTB_addr_counter >= (unsigned(DATA_WINDOW)-1)) then
                                PULSER_RECEIVER_SIM_END <= '1';
                                SIGNAL_TVALID_i1 <= '0';
                                SIGNAL_TLAST_i1 <= '0';
                                state_read <= read_end;  
                            end if;
                        end if;
                    else
                        SIGNAL_TVALID_i1 <= '0';
                        SIGNAL_TLAST_i1 <= '0';
                    end if;
                when read_end =>
                    if(PULSER_RECEIVER_SIM_BEGIN = '0') then
                        PULSER_RECEIVER_SIM_END <= '0';
                        BRAM_PORTB_first_read <= '1';
                        BRAM_PORTB_addr_counter <= (others => '0'); 
                        state_read <= idle;  
                    end if;
                when others => 
                    null;
            end case;                                                                        
        end if;
    end process;
    
    output_process: process(aclk, aresetn)
    begin 
        if (aresetn = '0') then
            SIGNAL_TVALID_i2 <= '0';
            SIGNAL_TLAST_i2 <= '0';     
            SIGNAL_TVALID_i3 <= '0';
            SIGNAL_TLAST_i3 <= '0';
            SIGNAL_TVALID <= '0';
            SIGNAL_TLAST <= '0'; 
            SIGNAL_TDATA_i1 <= (others => '0');
            SIGNAL_TDATA <= (others => '0');
        elsif (aclk'event and aclk = '1') then
            SIGNAL_TVALID_i2 <= SIGNAL_TVALID_i1;
            SIGNAL_TVALID_i3 <= SIGNAL_TVALID_i2;
            SIGNAL_TVALID <= SIGNAL_TVALID_i3;
            SIGNAL_TLAST_i2 <= SIGNAL_TLAST_i1;
            SIGNAL_TLAST_i3 <= SIGNAL_TLAST_i2;
            SIGNAL_TLAST <= SIGNAL_TLAST_i3;
            SIGNAL_TDATA_i1 <= std_logic_vector(signed(BRAM_PORTB_dout(13) & BRAM_PORTB_dout(13) & BRAM_PORTB_dout) + signed(random_number));
            SIGNAL_TDATA <= SIGNAL_TDATA_i1;
        end if;
    end process;
          
    pulser_receiver_sim_BRAM_inst : pulser_receiver_sim_BRAM
      PORT MAP (
        clka => aclk,
        ena => BRAM_PORTA_en,
        wea => BRAM_PORTA_we,
        addra => BRAM_PORTA_addr,
        dina => BRAM_PORTA_din,
        douta => BRAM_PORTA_dout,
        clkb => aclk,
        enb => BRAM_PORTB_en,
        web => BRAM_PORTB_we,
        addrb => BRAM_PORTB_addr,
        dinb => BRAM_PORTB_din,
        doutb => BRAM_PORTB_dout
      );
    
    random_number_generator_inst: random_number_generator
      generic map(W => W,
                  V => V,
                  g_type => g_type,
                  u_type => u_type
      )
      port map ( aclk    => aclk,
                aresetn => aresetn,
                enable  => '1',
                level   => "01010", --10 noise level
                g_noise_out => random_number,
                u_noise_out => open );
      
end Behavioral;
