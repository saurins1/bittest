library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity random_number_generator_tb is
	Generic (
            W : integer := 14;                    -- LFSR scaleable from 24 down to 4 bits
            V : integer := 18;                    -- LFSR for non uniform clocking scalable from 24 down to 18 bit
            g_type : integer := 0;            -- gausian distribution type, 0 = unimodal, 1 = bimodal, from g_noise_out
            u_type : integer := 1            -- uniform distribution type, 0 = uniform, 1 =  ave-uniform, from u_noise_out
        );
end;

architecture bench of random_number_generator_tb is

component random_number_generator is
    Port ( 
			aclk 			: 		in  STD_LOGIC;
			aresetn 	    : 		in  STD_LOGIC;
			enable			: 		in  STD_LOGIC;
			level 	        :		in STD_LOGIC_VECTOR (4 downto 0);
			g_noise_out 	:		out STD_LOGIC_VECTOR (W-1 downto 0);					-- port for bimodal/unimodal gaussian distributions
			u_noise_out 	: 		out  STD_LOGIC_VECTOR (W-1 downto 0)					-- port for uniform/ave-uniform distributions
			);
end component;

    signal g_noise_out: std_logic_vector (W-1 downto 0) ;
    signal u_noise_out: std_logic_vector (W-1 downto 0) ;

    signal aclk: STD_LOGIC := '0';
    signal aresetn: STD_LOGIC := '0';
    signal enable: STD_LOGIC := '0';
    signal level: std_logic_vector (4 downto 0):=(others => '0');
    
    constant aclk_period: time := 10 ns;

begin

   -- aclk process 
   aclk_period_process :process
   begin
    aclk <= not aclk;
    wait for aclk_period/2;
    aclk <= not aclk;
    wait for aclk_period/2;
   end process;

  -- Insert values for generic parameters !!
  uut: random_number_generator 
                                  port map ( aclk    => aclk,
                                             aresetn => aresetn,
                                             enable  => enable,
                                             level   => level,
                                             g_noise_out => g_noise_out,
                                             u_noise_out => u_noise_out );

  stimulus: process
  begin
    aresetn <= '0';
    enable <= '1';
    --level <= "1000";
    level <= "01010";
    wait for aclk_period*20;  
    aresetn <= '1';
    wait;
  end process;

end;