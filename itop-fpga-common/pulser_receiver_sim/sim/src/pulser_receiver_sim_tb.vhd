----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/03/2017 07:22:28 AM
-- Design Name: 
-- Module Name: channel_configuration_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity pulser_receiver_sim_tb is
	generic (
        REGISTER_WIDTH      : integer    := 32;
        TDATA_WIDTH         : integer    := 16;
        -- RANDOM NUMBER GEN
        W : integer := 16;                    -- LFSR scaleable from 24 down to 4 bits
        V : integer := 18;                    -- LFSR for non uniform clocking scalable from 24 down to 18 bit
        g_type : integer := 0;            -- gausian distribution type, 0 = unimodal, 1 = bimodal, from g_noise_out
        u_type : integer := 1            -- uniform distribution type, 0 = uniform, 1 =  ave-uniform, from u_noise_out
    );
end;

architecture bench of pulser_receiver_sim_tb is

  component pulser_receiver_sim
  	generic (
          REGISTER_WIDTH      : integer    := 32;
          TDATA_WIDTH         : integer    := 16;
          W : integer := 16;
          V : integer := 18;
          g_type : integer := 0;
          u_type : integer := 1
      );
      Port ( 
          aclk                                : in std_logic;
          aresetn                             : in std_logic;
          LOAD_SIGNAL                         : in std_logic;
          SIGNAL_INDEX                        : in std_logic_vector(REGISTER_WIDTH-1 downto 0);   
          SIGNAL_VALUE                        : in std_logic_vector(REGISTER_WIDTH-1 downto 0);
          PULSER_RECEIVER_SIM_BEGIN               : in std_logic;
          PULSER_RECEIVER_SIM_END                 : out std_logic;
          DATA_WINDOW                         : in std_logic_vector(REGISTER_WIDTH-1 downto 0);
          SIGNAL_TVALID       : out std_logic;
          SIGNAL_TDATA        : out std_logic_vector(TDATA_WIDTH-1 downto 0);
          SIGNAL_TLAST        : out std_logic;
          SIGNAL_TREADY       : in std_logic
      );
  end component;

  signal aclk: std_logic:= '0';
  signal aresetn: std_logic:= '0';
  signal LOAD_SIGNAL: std_logic:= '0';
  signal SIGNAL_INDEX: std_logic_vector(REGISTER_WIDTH-1 downto 0):= (others => '0');
  signal SIGNAL_VALUE: std_logic_vector(REGISTER_WIDTH-1 downto 0):= (others => '0');
  signal PULSER_RECEIVER_SIM_BEGIN: std_logic:= '0';
  signal PULSER_RECEIVER_SIM_END: std_logic;
  signal DATA_WINDOW: std_logic_vector(REGISTER_WIDTH-1 downto 0):= (others => '0');
  signal SIGNAL_TVALID: std_logic;
  signal SIGNAL_TDATA: std_logic_vector(TDATA_WIDTH-1 downto 0);
  signal SIGNAL_TLAST: std_logic;
  signal SIGNAL_TREADY: std_logic:='0';

  constant aclk_period: time := 10 ns;
  
  signal begin_load: STD_LOGIC := '0'; 
  
  -- STATE MACHINE STATES
  type states_load is (idle, load_wait, load_input, wait_result); 
  signal state_load : states_load; 
  signal load_counter : unsigned(31 downto 0) := (others => '0'); 
  
  type signal_array is array ( 0 to 9999 ) of std_logic_vector(15 downto 0);
  signal signal_tb : signal_array;

begin

   -- aclk process 
   aclk_period_process :process
   begin
    aclk <= not aclk;
    wait for aclk_period/2;
    aclk <= not aclk;
    wait for aclk_period/2;
   end process;

  -- Insert values for generic parameters !!
  uut: pulser_receiver_sim generic map ( REGISTER_WIDTH            => REGISTER_WIDTH,
                                         TDATA_WIDTH               => TDATA_WIDTH,
                                         W                         => W,
                                         V                         => V,
                                         g_type                    => g_type,
                                         u_type                    => u_type)
                              port map ( aclk                      => aclk,
                                         aresetn                   => aresetn,
                                         LOAD_SIGNAL               => LOAD_SIGNAL,
                                         SIGNAL_INDEX              => SIGNAL_INDEX,
                                         SIGNAL_VALUE              => SIGNAL_VALUE,
                                         PULSER_RECEIVER_SIM_BEGIN => PULSER_RECEIVER_SIM_BEGIN,
                                         PULSER_RECEIVER_SIM_END   => PULSER_RECEIVER_SIM_END,
                                         DATA_WINDOW               => DATA_WINDOW,
                                         SIGNAL_TVALID             => SIGNAL_TVALID,
                                         SIGNAL_TDATA              => SIGNAL_TDATA,
                                         SIGNAL_TLAST              => SIGNAL_TLAST,
                                         SIGNAL_TREADY             => SIGNAL_TREADY );

  stimulus: process
  begin
    aresetn <= '0';
    begin_load <= '0';
    wait for aclk_period*20;  
    aresetn <= '1';
    wait for aclk_period*20;  
    begin_load <= '1';
    wait for aclk_period*20;  
    begin_load <= '0';
    wait;
  end process;
   
  load_inst: process(aclk, aresetn)      
    type dataFile is file of integer;
    file data_file: dataFile is in "ascan.dat";
    variable data_in_x: integer;                                               
  begin 
    if (aresetn = '0') then
        LOAD_SIGNAL <= '0';
        PULSER_RECEIVER_SIM_BEGIN <= '0';
        SIGNAL_TREADY <= '0';
        SIGNAL_INDEX <= (others => '0');
        SIGNAL_VALUE <= (others => '0');
        DATA_WINDOW  <= (others => '0');
        state_load <= idle;  
        load_counter <= (others => '0');
    elsif (aclk'event and aclk = '1') then
        case state_load is  
           when idle =>   
                DATA_WINDOW <= "00000000000000000010011001001000"; --9800
                SIGNAL_TREADY <= '1';
                if(begin_load = '1') then
                    state_load <= load_input;  
                end if;
           when load_wait =>
                LOAD_SIGNAL <= '0';
                if(load_counter >= 128) then
                    load_counter <= (others => '0'); 
                    state_load <= wait_result; 
                else
                    if(load_counter >= 64) then
                        PULSER_RECEIVER_SIM_BEGIN <= '1';
                    end if;
                    load_counter <= load_counter + 1;
                end if;
            when load_input =>
                load_counter <= load_counter + 1;
                SIGNAL_INDEX <= std_logic_vector(load_counter);
                LOAD_SIGNAL <= '1';
                if not endfile(data_file) then
                    read (data_file, data_in_x);
                    SIGNAL_VALUE<=std_logic_vector(to_signed(data_in_x, SIGNAL_VALUE'length));
                end if;
                if not endfile(data_file) then
                    read (data_file, data_in_x);
                end if;
                if(load_counter >= (unsigned(DATA_WINDOW)-1)) then
                    state_load <= load_wait; 
                    load_counter <= (others => '0');
                end if;  
            when wait_result =>   
                if(PULSER_RECEIVER_SIM_END = '1') then
                    PULSER_RECEIVER_SIM_BEGIN <= '0';
                    state_load <= load_wait;
                end if;
            when others =>
                null;                   
        end case;                                                                                            
    end if;
  end process;

end;