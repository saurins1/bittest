----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 05/02/2019 10:07:23 AM
-- Design Name: 
-- Module Name: DSP filter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity dsp_filter is
	generic (
        REGISTER_W16        : INTEGER := 16;
		REGISTER_W2         : INTEGER := 2;
	    RECTIFICATION_DISABLED	: INTEGER := 0;
        RECTIFICATION_MED   : INTEGER := 1;
        RECTIFICATION_FULL  : INTEGER := 2
	);
	port (
	    -- Sync
	    aclk         : in STD_LOGIC;
        aresetn      : in STD_LOGIC;
	
	    -- Control
		DSP_INVERSION  	    	: in STD_LOGIC;
		DSP_RECTIFICATION  	    : in STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
			
		-- Signal input
		SIGNAL_INPUT_AXIS_TREADY  : out STD_LOGIC;
		SIGNAL_INPUT_AXIS_TDATA	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		SIGNAL_INPUT_AXIS_TLAST	  : in STD_LOGIC;
		SIGNAL_INPUT_AXIS_TVALID  : in STD_LOGIC;

		-- Dsp result
		DSP_RESULT_AXIS_TVALID    : out STD_LOGIC;
		DSP_RESULT_AXIS_TDATA	  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		DSP_RESULT_AXIS_TLAST	  : out STD_LOGIC;
		DSP_RESULT_AXIS_TREADY    : in STD_LOGIC
	);
end dsp_filter;

architecture arch_imp of dsp_filter is

    signal dsp_inversion_axis_tready  : STD_LOGIC;
    signal dsp_inversion_axis_tvalid  : STD_LOGIC;
    signal dsp_inversion_axis_tlast   : STD_LOGIC;
    signal dsp_inversion_axis_tdata   : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    
begin

    dsp_result_process: process(aclk) 
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				dsp_inversion_axis_tready <= '0';
				SIGNAL_INPUT_AXIS_TREADY <= '0';
		
				dsp_inversion_axis_tvalid <= '0';
				DSP_RESULT_AXIS_TVALID <= '0';

				dsp_inversion_axis_tlast <= '0';
				DSP_RESULT_AXIS_TLAST <= '0';
				
				dsp_inversion_axis_tdata <= (others => '0');
				DSP_RESULT_AXIS_TDATA  <= (others => '0');												
        	else
				dsp_inversion_axis_tready <= DSP_RESULT_AXIS_TREADY;	
				SIGNAL_INPUT_AXIS_TREADY <= dsp_inversion_axis_tready;

				dsp_inversion_axis_tvalid <= SIGNAL_INPUT_AXIS_TVALID;	
				DSP_RESULT_AXIS_TVALID <= dsp_inversion_axis_tvalid;

				dsp_inversion_axis_tlast <= SIGNAL_INPUT_AXIS_TLAST;	
				DSP_RESULT_AXIS_TLAST <= dsp_inversion_axis_tlast;
				if(DSP_INVERSION = '1') then
					dsp_inversion_axis_tdata <= std_logic_vector(-signed(SIGNAL_INPUT_AXIS_TDATA));	
				else
					dsp_inversion_axis_tdata <= SIGNAL_INPUT_AXIS_TDATA;
				end if;
				DSP_RESULT_AXIS_TDATA <= dsp_inversion_axis_tdata;
--				if(unsigned(DSP_RECTIFICATION) = RECTIFICATION_DISABLED) then
--					DSP_RESULT_AXIS_TDATA <= dsp_inversion_axis_tdata;
--				elsif(unsigned(DSP_RECTIFICATION) = RECTIFICATION_MED) then
--					if(signed(dsp_inversion_axis_tdata) < 0) then
--						DSP_RESULT_AXIS_TDATA <= (others => '0');	
--					else
--						DSP_RESULT_AXIS_TDATA <= dsp_inversion_axis_tdata;	
--					end if;
--				else
--					if(signed(dsp_inversion_axis_tdata) < 0) then
--						DSP_RESULT_AXIS_TDATA <= std_logic_vector(-signed(dsp_inversion_axis_tdata));	
--					else
--						DSP_RESULT_AXIS_TDATA <= dsp_inversion_axis_tdata;	
--					end if;
--				end if;	
			end if;
		end if;
	end process;
																	  												 																  											     
end arch_imp;
