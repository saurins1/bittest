library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity dsp_filter_tb is
    generic (
        REGISTER_W32        : INTEGER := 32;
        REGISTER_W16        : INTEGER := 16;
        REGISTER_W2         : INTEGER := 2;
        RECTIFICATION_DISABLED    : INTEGER := 0;
        RECTIFICATION_MED   : INTEGER := 1;
        RECTIFICATION_FULL  : INTEGER := 2
    );
end;

architecture bench of dsp_filter_tb is
 
    component dsp_filter is
        generic (
            REGISTER_W16        : INTEGER := 16;
            REGISTER_W2         : INTEGER := 2;
            RECTIFICATION_DISABLED	: INTEGER := 0;
            RECTIFICATION_MED   : INTEGER := 1;
            RECTIFICATION_FULL  : INTEGER := 2
        );
        port (
            -- Sync
            aclk         : in STD_LOGIC;
            aresetn      : in STD_LOGIC;
        
            -- Control
            DSP_INVERSION  	    	: in STD_LOGIC;
            DSP_RECTIFICATION  	    : in STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
                
            -- Signal input
            SIGNAL_INPUT_AXIS_TREADY  : out STD_LOGIC;
            SIGNAL_INPUT_AXIS_TDATA	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
            SIGNAL_INPUT_AXIS_TLAST	  : in STD_LOGIC;
            SIGNAL_INPUT_AXIS_TVALID  : in STD_LOGIC;
    
            -- Average result
            DSP_RESULT_AXIS_TVALID    : out STD_LOGIC;
            DSP_RESULT_AXIS_TDATA	  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
            DSP_RESULT_AXIS_TLAST	  : out STD_LOGIC;
            DSP_RESULT_AXIS_TREADY    : in STD_LOGIC
        );
    end component dsp_filter;

  signal aclk: STD_LOGIC := '0';
  signal aresetn: STD_LOGIC := '0';
  
  signal DSP_INVERSION: STD_LOGIC := '0';
  signal DSP_RECTIFICATION: STD_LOGIC_VECTOR (1 downto 0) := (others => '0');
  
  signal SIGNAL_INPUT_AXIS_TREADY  : STD_LOGIC;
  signal SIGNAL_INPUT_AXIS_TDATA   : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0');
  signal SIGNAL_INPUT_AXIS_TLAST   : STD_LOGIC:= '0';
  signal SIGNAL_INPUT_AXIS_TVALID  : STD_LOGIC:= '0';

  -- Average result
  signal DSP_RESULT_AXIS_TVALID    : STD_LOGIC;
  signal DSP_RESULT_AXIS_TDATA     : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  signal DSP_RESULT_AXIS_TLAST     : STD_LOGIC;
  signal DSP_RESULT_AXIS_TREADY    : STD_LOGIC:= '0';
  
  signal DATA_WINDOW: STD_LOGIC_VECTOR (31 downto 0) := (others => '0');

  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean := false;
  
  type states_signal_sm is (idle, open_file, load_signal, load_signal_wait, close_file); 
  signal state_signal_sm : states_signal_sm; 
  
  signal start_sm: STD_LOGIC:= '0';
  signal direction: STD_LOGIC:= '0';
  signal data_top_pos: signed (31 downto 0) := (others => '0');
  signal data_top_neg: signed (31 downto 0) := (others => '0');
 
  signal decimation_level: std_logic_vector(REGISTER_W32-1 downto 0):= (others => '0');
  signal load_signal_counter: unsigned(REGISTER_W16-1 downto 0):= (others => '0');
  
begin


  stimulus: process
  begin
    aresetn <= '0';
    start_sm  <= '0';
    -- Put initialisation code here   
    wait for 100 ns;
    -- Put test bench stimulus code here
    aresetn <= '1';
    wait for 100 ns;
    start_sm  <= '1';
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      aclk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;
  
  -- Insert values for generic parameters !!
  uut: dsp_filter 
        generic map ( 
            REGISTER_W16   => REGISTER_W16,
            REGISTER_W2   => REGISTER_W2,
            RECTIFICATION_DISABLED   => RECTIFICATION_DISABLED,
            RECTIFICATION_MED  => RECTIFICATION_MED,
            RECTIFICATION_FULL  => RECTIFICATION_FULL 
        )
      port map(
          -- Sync
          aclk         => aclk,
          aresetn      => aresetn,
      
          -- Control
          DSP_INVERSION              => DSP_INVERSION,
          DSP_RECTIFICATION          => DSP_RECTIFICATION,
              
          -- Signal input
          SIGNAL_INPUT_AXIS_TREADY  => SIGNAL_INPUT_AXIS_TREADY,
          SIGNAL_INPUT_AXIS_TDATA   => SIGNAL_INPUT_AXIS_TDATA,
          SIGNAL_INPUT_AXIS_TLAST   => SIGNAL_INPUT_AXIS_TLAST,
          SIGNAL_INPUT_AXIS_TVALID  => SIGNAL_INPUT_AXIS_TVALID,
  
          -- Average result
          DSP_RESULT_AXIS_TVALID    => DSP_RESULT_AXIS_TVALID,
          DSP_RESULT_AXIS_TDATA     => DSP_RESULT_AXIS_TDATA,
          DSP_RESULT_AXIS_TLAST     => DSP_RESULT_AXIS_TLAST,
          DSP_RESULT_AXIS_TREADY    => DSP_RESULT_AXIS_TREADY
      );
      
    signal_input_inst: process(aclk, aresetn)  
          variable my_counter  : integer range 0 to 16383 :=0;   
          variable decimation_counter  : integer range 0 to 16383 :=0; 
      begin 
          if (aresetn = '0') then
              -- config
              --DSP_INVERSION    <= '0';
              DSP_INVERSION    <= '1';
              DSP_RECTIFICATION <= std_logic_vector(to_unsigned(RECTIFICATION_DISABLED, DSP_RECTIFICATION'length));  
              --DSP_RECTIFICATION <= std_logic_vector(to_unsigned(RECTIFICATION_MED, DSP_RECTIFICATION'length)); 
              --DSP_RECTIFICATION <= std_logic_vector(to_unsigned(RECTIFICATION_FULL, DSP_RECTIFICATION'length));       
              
              DATA_WINDOW <= std_logic_vector(to_unsigned(8192, DATA_WINDOW'length));   
                                                                                            
              -- SIGNAL INPUT AXIS
              SIGNAL_INPUT_AXIS_TDATA  <= (others => '0');
              SIGNAL_INPUT_AXIS_TLAST  <= '0';
              SIGNAL_INPUT_AXIS_TVALID <= '0';
              -- AVG RESULT AXIS
              DSP_RESULT_AXIS_TREADY <= '0';                                                                  
              -- decimation
              --decimation_level <= std_logic_vector(to_unsigned(1, DATA_WINDOW'length));  
              decimation_level <= std_logic_vector(to_unsigned(4, DATA_WINDOW'length));    
              load_signal_counter <= (others => '0');                                                              
              -- state                                                                              
              state_signal_sm <= idle;  
          elsif (aclk'event and aclk = '1') then
              case state_signal_sm is  
                  when idle =>     
                      DSP_RESULT_AXIS_TREADY <= '0';     
                      direction <= '0'; 
                      data_top_pos <= to_signed(128, data_top_pos'length);  
                      data_top_neg <= to_signed(-128, data_top_pos'length);  
                      SIGNAL_INPUT_AXIS_TDATA <= std_logic_vector(to_unsigned(0, SIGNAL_INPUT_AXIS_TDATA'length));  
                      if(start_sm = '1') then
                          my_counter := 0;
                          load_signal_counter <= (others => '0');    
                          state_signal_sm <= load_signal;        
                      end if; 
                  when load_signal =>                 
                      if(SIGNAL_INPUT_AXIS_TREADY = '1') then
                          if(my_counter <= to_integer(unsigned(DATA_WINDOW))-1) then
                              if(direction = '0') then
                                if(signed(SIGNAL_INPUT_AXIS_TDATA) >= data_top_pos) then
                                    SIGNAL_INPUT_AXIS_TDATA<=std_logic_vector(unsigned(SIGNAL_INPUT_AXIS_TDATA)-1);  
                                    direction <= '1';     
                                else
                                    SIGNAL_INPUT_AXIS_TDATA<=std_logic_vector(unsigned(SIGNAL_INPUT_AXIS_TDATA)+1);
                                end if;
                              else
                                if(signed(SIGNAL_INPUT_AXIS_TDATA) <= data_top_neg) then
                                    SIGNAL_INPUT_AXIS_TDATA<=std_logic_vector(unsigned(SIGNAL_INPUT_AXIS_TDATA)+1);  
                                    direction <= '0';     
                                else
                                    SIGNAL_INPUT_AXIS_TDATA<=std_logic_vector(unsigned(SIGNAL_INPUT_AXIS_TDATA)-1);
                                end if;
                              end if;                           
                              SIGNAL_INPUT_AXIS_TVALID <= '1';        
                              if(my_counter = to_integer(unsigned(DATA_WINDOW))-1) then
                                  SIGNAL_INPUT_AXIS_TLAST <= '1';
                              end if;
                              if(unsigned(decimation_level) > 1) then    
                                  decimation_counter := 0;    
                                  state_signal_sm <= load_signal_wait;                                                            
                              end if;
                              load_signal_counter <= load_signal_counter + 1;
                              my_counter := my_counter + 1; 
                          else
                              DSP_RESULT_AXIS_TREADY <= '0'; 
                              SIGNAL_INPUT_AXIS_TVALID <= '0';
                              SIGNAL_INPUT_AXIS_TLAST <= '0';                                                    
                              my_counter  := 0;
                              state_signal_sm    <= close_file;    
                          end if;   
                      else
                        DSP_RESULT_AXIS_TREADY <= '1';    
                      end if;                                                                        
                  when load_signal_wait =>
                      SIGNAL_INPUT_AXIS_TVALID <= '0';
                      SIGNAL_INPUT_AXIS_TLAST <= '0';                                                                
                      if(decimation_counter >= unsigned(decimation_level)-2) then
                          state_signal_sm <= load_signal;                                                                 
                      else
                          decimation_counter := decimation_counter + 1;                                                                
                      end if;                                                                                                                                
                  when close_file =>
                      if(my_counter >= 32) then
                          my_counter  := 0;
                          state_signal_sm <= idle;
                      else
                          my_counter := my_counter + 1;
                      end if;   
                  when others =>
                      null;                   
              end case;                                                                                            
          end if;
      end process;

end;