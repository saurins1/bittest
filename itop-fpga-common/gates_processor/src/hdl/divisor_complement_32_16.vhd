----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/05/2017 03:52:07 PM
-- Design Name: 
-- Module Name: divisor__complement_64_64 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_signed.all;
use ieee.numeric_std.all; 

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity divisor_complement_32_16 is
	generic (
        wait_cycles   : integer    := 8
    );
    port (
        aclk            : in std_logic;
        aresetn         : in std_logic;
        DIVIDEND        : in std_logic_vector(31 downto 0);
        DIVISOR         : in std_logic_vector(15 downto 0);
        DIVISION_START  : in std_logic;
        DIVISION_VALID  : out std_logic;
        DIVISION_RESULT : out std_logic_vector(31 downto 0)
    );
end divisor_complement_32_16;

architecture Behavioral of divisor_complement_32_16 is

    signal DIVIDEND_i   : std_logic_vector(31 downto 0);
    signal DIVISOR_i    : std_logic_vector(31 downto 0);
    signal DIVISION_RESULT_i: std_logic_vector(31 downto 0);
    signal DIVIDEND_pos : unsigned(31 downto 0);
    signal DIVISOR_pos  : unsigned(31 downto 0);
    signal division_sign: std_logic;
    
    signal division_i : unsigned(31 downto 0);
    signal division_counter : unsigned(31 downto 0);
    signal division_valid_counter : integer range 0 to 63;

    -- STATE MACHINE STATES
    type states_divider is (idle, get_sign, prepare_division, perform_division, calculate_division, validate_result, division_end); 
    signal state_divider : states_divider; 

begin

    -- Divisor
    divisor_process: process(aresetn, aclk)   
    begin 
        if (aresetn = '0') then
            DIVIDEND_i <= (others => '0');
            DIVISOR_i <= (others => '0');
            DIVISION_RESULT_i <= (others => '0');
            DIVIDEND_pos <= (others => '0');
            DIVISOR_pos <= (others => '0');
            division_sign <= '0';
            division_i <= (others => '0');
            division_counter <= (others => '0');
            division_valid_counter <= 0;
            DIVISION_RESULT <= (others => '0');
            DIVISION_VALID <= '0';
            -- state
            state_divider <= idle;  
        elsif (aclk'event and aclk = '1') then
		    case state_divider is                  
		      when idle =>
		          if(DIVISION_START = '1') then
		              state_divider <= get_sign;       
		          end if;
		      when get_sign =>
                  if(DIVIDEND(31) = '1') then
                      DIVIDEND_i <= not DIVIDEND + 1;
                      if(DIVISOR(15) = '1') then
                          DIVISOR_i <= not ("1111111111111111" & DIVISOR) + 1;
                          division_sign <= '0';
                      else
                          DIVISOR_i <= "0000000000000000" & DIVISOR;
                          division_sign <= '1';
                      end if;
                  else
                      DIVIDEND_i <= DIVIDEND;
                      if(DIVISOR(15) = '1') then
                          DIVISOR_i <= not ("1111111111111111" & DIVISOR) + 1;
                          division_sign <= '1';
                      else
                          DIVISOR_i <= "0000000000000000" & DIVISOR;
                          division_sign <= '0';
                      end if;
                  end if;
                  state_divider <= prepare_division;
              when prepare_division =>
                  if( (unsigned(DIVIDEND_i) = 0) or (unsigned(DIVISOR_i) = 0) or (unsigned(DIVISOR_i) > unsigned(DIVIDEND_i)) ) then
                      DIVISION_RESULT_i <= (others => '0');
                      division_sign <= '0';
                      state_divider <= calculate_division;
                  else
                      DIVIDEND_pos <= unsigned(DIVIDEND_i);
                      DIVISOR_pos <= unsigned(DIVISOR_i);
                      division_i <= unsigned(DIVIDEND_i);
                      division_counter <= (others => '0');
                      state_divider <= perform_division;
                  end if;
              when perform_division =>
                  if(division_i >=  DIVISOR_pos) then
                      division_i <= division_i - DIVISOR_pos;
                      division_counter <= division_counter + 1;
                  else
                      DIVISION_RESULT_i <= std_logic_vector(division_counter);
                      state_divider <= calculate_division;  
                  end if;
              when calculate_division =>
                  if(division_sign = '0') then
                      DIVISION_RESULT <= DIVISION_RESULT_i;
                  else
                      DIVISION_RESULT <= not DIVISION_RESULT_i + 1;
                  end if;
                  division_valid_counter <= 0;
                  state_divider <= validate_result;
              when validate_result =>
                  if(division_valid_counter >= wait_cycles) then
                      DIVISION_VALID <= '0';
                      state_divider <= division_end;
                  else
                      DIVISION_VALID <= '1';
                      division_valid_counter <= division_valid_counter + 1;
                  end if;  
              when division_end =>
                  if(DIVISION_START = '0') then
                      state_divider <= idle;
                  end if;               
		      when others =>
		          null;
		    end case;
		end if;
	end process;

end Behavioral;
