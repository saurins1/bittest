----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 09/27/2017 10:07:23 AM
-- Design Name: 
-- Module Name: channel capabilities - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity capabilities is
	generic (
	    RAM_ADDR_LENGTH         : INTEGER := 10;
        REGISTER_W32            : INTEGER := 32;
		BRAM_TREADY_SYNC        : INTEGER := 3;
		WAIT_FOR_BRAM           : INTEGER := 16
	);
	port (
	    -- Sync
	    aclk         : in STD_LOGIC;
        aresetn      : in STD_LOGIC;
        
        -- Control
        READ_CAPABILITIES  : in STD_LOGIC;
        CAPABILITIES_READY : out STD_LOGIC;
		CAPABILITIES_SIZE  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		-- Capabilities
        CAPABILITIES_AXIS_TREADY  : in STD_LOGIC;
        CAPABILITIES_AXIS_TDATA   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        CAPABILITIES_AXIS_TLAST   : out STD_LOGIC;
        CAPABILITIES_AXIS_TVALID  : out STD_LOGIC;
        CAPABILITIES_AXIS_TKEEP   : out STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0)
	);
end capabilities;

architecture arch_imp of capabilities is
	
    -- BRAM capabilities
    COMPONENT capabilities_RAM is
        generic (
            ADDR_LENGTH    : INTEGER := 10;
            REGISTER_W32   : INTEGER := 32
        );
        port(
            aclk    : IN STD_LOGIC;
            en      : IN STD_LOGIC;
            we      : IN STD_LOGIC;
            addr    : IN STD_LOGIC_VECTOR(ADDR_LENGTH-1 downto 0);
            din     : IN STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            dout    : OUT STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0)
        );
    end COMPONENT capabilities_RAM;
    
    -- Capabilities BRAM signals
    signal capabilities_bram_addra   : STD_LOGIC_VECTOR ( RAM_ADDR_LENGTH-1 downto 0 );
    signal capabilities_bram_dina    : STD_LOGIC_VECTOR ( REGISTER_W32-1 downto 0 );
    signal capabilities_bram_douta   : STD_LOGIC_VECTOR ( REGISTER_W32-1 downto 0 );
    signal capabilities_bram_ena     : STD_LOGIC;
    signal capabilities_bram_wea     : STD_LOGIC;
    
    signal capabilities_bram_douta_i1   : STD_LOGIC_VECTOR ( REGISTER_W32-1 downto 0 );
   
	signal capabilities_bram_addra_first : STD_LOGIC;
	signal read_capabilities_flag : STD_LOGIC;
	signal read_bram_ready : STD_LOGIC;
	
	signal capabilities_axis_tdata_1   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal capabilities_axis_tdata_2   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal tready_flank_up_flag        : STD_LOGIC;
	signal capabilities_counter        : UNSIGNED(REGISTER_W32-1 downto 0);
	signal capabilities_axis_tready_i1 : STD_LOGIC;
	signal wait_for_tlast_flag         : STD_LOGIC;
	
    -- Read Capabilities state machine
    type states_read_capabilities_sm is (idle, read_capabilities_start, upload_capabilities_data_flank_down, 
        upload_capabilities_data_flank_up, check_tlast, upload_capabilities_end); 

    signal state_read_capabilities_sm : states_read_capabilities_sm;
    
    signal capabilities_size_i  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
   
begin

	-- State machine for upload Capabilities
    upload_capabilities_sm: process(aclk, aresetn) 
		variable var_capabilities_counter : integer range 0 to 65536 :=0;
		variable var_wait_counter : integer range 0 to 65536 :=0;																			 
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				read_capabilities_flag <= '0';
				-- CAPABILITIES AXIS														 														 
				CAPABILITIES_AXIS_TDATA <= (others => '0');
				CAPABILITIES_AXIS_TKEEP <= (others => '0');
				CAPABILITIES_AXIS_TVALID <= '0';
				CAPABILITIES_AXIS_TLAST <= '0';
				-- Sync BRAM delay with TREADY evolution		
				tready_flank_up_flag <= '0';
				capabilities_axis_tdata_1 <= (others => '0');	 
				capabilities_axis_tdata_2 <= (others => '0');																	 
				capabilities_axis_tready_i1 <= '0';
				wait_for_tlast_flag         <= '0';
				capabilities_counter <= (others => '0');
				capabilities_bram_douta <= (others => '0');													 
				-- state
				state_read_capabilities_sm <= idle;
        	else
				capabilities_axis_tready_i1 <= CAPABILITIES_AXIS_TREADY;
				capabilities_bram_douta     <= capabilities_bram_douta_i1;															 
				case state_read_capabilities_sm is
					when idle =>														 
						if(READ_CAPABILITIES = '1') then
						    var_wait_counter := 0;		
						    var_capabilities_counter:= 0;
						    capabilities_counter <= (others => '0');
							state_read_capabilities_sm <= read_capabilities_start;													 
						end if;	
					when read_capabilities_start =>
					    if(read_bram_ready = '1') then
                            CAPABILITIES_AXIS_TKEEP <= (others => '1');
                            if(CAPABILITIES_AXIS_TREADY = '1') then
                                read_capabilities_flag <= '1';
                                if(var_wait_counter >= BRAM_TREADY_SYNC) then
                                    state_read_capabilities_sm <= upload_capabilities_data_flank_down;
                                else
                                    var_wait_counter := var_wait_counter + 1;
                                end if;
                            end if;	
                        end if;							  												 											 
					when upload_capabilities_data_flank_down =>															 														
                        if(CAPABILITIES_AXIS_TREADY = '1') then
                            capabilities_counter <= capabilities_counter + 1;
							CAPABILITIES_AXIS_TVALID <= '1';													
                            if(tready_flank_up_flag = '1') then
                                CAPABILITIES_AXIS_TDATA <= capabilities_axis_tdata_2;
                                tready_flank_up_flag <= '0';
                            else
                                CAPABILITIES_AXIS_TDATA <= capabilities_bram_douta;
                            end if;
							if(var_capabilities_counter >= to_integer(unsigned(capabilities_size_i))-1) then
								CAPABILITIES_AXIS_TLAST <= '1';
								state_read_capabilities_sm <= check_tlast;												
							else
								var_capabilities_counter := var_capabilities_counter + 1;													
							end if;
                        else
                            if(capabilities_axis_tready_i1 = '1') then
                                capabilities_axis_tdata_1 <= capabilities_bram_douta;
                                var_wait_counter:= 0;
                                state_read_capabilities_sm <= upload_capabilities_data_flank_up;
                            end if;
                        end if;																														
					when upload_capabilities_data_flank_up =>																									
                        if(var_wait_counter = 0) then
                            capabilities_axis_tdata_2 <= capabilities_bram_douta;
                        end if;
                        if(CAPABILITIES_AXIS_TREADY = '1' and capabilities_axis_tready_i1 = '0') then
                            capabilities_counter <= capabilities_counter + 1;
							CAPABILITIES_AXIS_TVALID <= '1';													
                            if(var_wait_counter = 0) then
                                CAPABILITIES_AXIS_TDATA <= capabilities_bram_douta;
                            else
                                CAPABILITIES_AXIS_TDATA <= capabilities_axis_tdata_1;
                            end if;      
                            tready_flank_up_flag <= '1';
							if(var_capabilities_counter >= to_integer(unsigned(capabilities_size_i))-1) then
								CAPABILITIES_AXIS_TLAST <= '1';
								state_read_capabilities_sm <= check_tlast;												
							else
								var_capabilities_counter := var_capabilities_counter + 1;		
								state_read_capabilities_sm <= upload_capabilities_data_flank_down;													
							end if;														
                        end if;	
                        if(var_wait_counter < 1) then
                            var_wait_counter := var_wait_counter + 1;
                        end if;	
                    when check_tlast =>
                        if(wait_for_tlast_flag = '0') then
                            if(CAPABILITIES_AXIS_TREADY = '0' and capabilities_axis_tready_i1 = '1') then
                                wait_for_tlast_flag <= '1';
                            else
                                CAPABILITIES_AXIS_TLAST  <= '0';    
                                CAPABILITIES_AXIS_TVALID <= '0';
                                CAPABILITIES_AXIS_TKEEP  <= (others => '0');
                                read_capabilities_flag   <= '0';
                                tready_flank_up_flag     <= '0';
                                state_read_capabilities_sm    <= upload_capabilities_end;
                            end if;  
                        else
                            if(CAPABILITIES_AXIS_TREADY = '1') then
                                CAPABILITIES_AXIS_TLAST  <= '0';    
                                CAPABILITIES_AXIS_TVALID <= '0';
                                CAPABILITIES_AXIS_TKEEP  <= (others => '0');
                                read_capabilities_flag   <= '0';
                                tready_flank_up_flag     <= '0';
                                state_read_capabilities_sm    <= upload_capabilities_end;
                            end if;
                        end if; 							
					when upload_capabilities_end => 																 
						CAPABILITIES_AXIS_TLAST <= '0';	
						CAPABILITIES_AXIS_TVALID <= '0';
						CAPABILITIES_AXIS_TKEEP <= (others => '0');
						read_capabilities_flag <= '0';														
						tready_flank_up_flag <= '0';													
						if(READ_CAPABILITIES = '0' and read_capabilities_flag = '0') then			
							state_read_capabilities_sm <= idle;													
						end if;															
					when others =>
						null;
				end case;
			end if;
         end if;
    end process;
    
    CAPABILITIES_SIZE <= capabilities_size_i;
    CAPABILITIES_READY <= read_bram_ready;
    
    -- Capabilities BRAM read process
    read_bram_capabilities_inst: process(aclk, aresetn) 
        variable var_wait_counter : integer range 0 to 127 :=0;
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                capabilities_bram_addra <= (others => '0');
                capabilities_bram_dina <= (others => '0');
                capabilities_bram_ena <= '1';
                capabilities_bram_wea <= '0';
                capabilities_bram_addra_first <= '1';
                read_bram_ready <= '0';
                var_wait_counter := 0;
                capabilities_size_i <= (others => '0');
            else
                if( var_wait_counter >= WAIT_FOR_BRAM) then
                    read_bram_ready <= '1';
                    if(read_capabilities_flag = '1') then
                        if(CAPABILITIES_AXIS_TREADY = '1') then
                            if(capabilities_bram_addra_first = '1') then
                                capabilities_bram_addra <= (others => '0');
                                capabilities_bram_addra_first <= '0';
                            else
                                capabilities_bram_addra <= std_logic_vector(unsigned(capabilities_bram_addra)+1);
                            end if;
                        end if;
                    else
                        capabilities_bram_addra <= (others => '0');
                        capabilities_bram_addra_first <= '1';
                    end if;
                else
                    var_wait_counter := var_wait_counter + 1;
                    if(var_wait_counter >= 2) then
                        capabilities_size_i <= capabilities_bram_douta;
                    end if;
                end if;
            end if;
        end if;    
    end process;
					      									 	   			   
    -- Capabilities BRAM
    capabilities_RAM_inst : capabilities_RAM
       GENERIC MAP(
          ADDR_LENGTH => RAM_ADDR_LENGTH,
          REGISTER_W32 => REGISTER_W32
       )
       PORT MAP (
          aclk => aclk,
          en => capabilities_bram_ena,
          we => capabilities_bram_wea,
          addr => capabilities_bram_addra,
          din => capabilities_bram_dina,
          dout => capabilities_bram_douta_i1
       );
                 
end arch_imp;
