----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/03/2017 05:02:22 AM
-- Design Name: 
-- Module Name: gate_compute - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity gatex_compute is
	generic (
        -- Users to add parameters here
        GATE_RANGE                      : integer    := 32;
        GATE_ALARM_CROSSING             : integer    := 2;
        GATE_TOF_ALGORITHM_RANGE        : integer    := 4;
        GATE_TOF_AVG_RANGE              : integer    := 8;
        GATE_TOF_RANGE                  : integer    := 32;
        GATE_AXIS_GATE_RANGE_48         : integer  := 48;
        GATE_AXIS_GATE_RANGE_16         : integer   := 16
  );
  Port ( 
      -- sync
      ACLK                             : in std_logic;
      ARESETN                          : in std_logic;                   
      -- GATE PARAMETERS
      GATE_EN                          : in std_logic;  
      GATE_START                       : in std_logic_vector(GATE_RANGE-1 downto 0);
      GATE_WIDTH                       : in std_logic_vector(GATE_RANGE-1 downto 0);    
      GATE_AMP_THRESHOLD               : in std_logic_vector(GATE_AXIS_GATE_RANGE_16-1 downto 0);
      GATE_AMP_THRESHOLD_ALARM_EN      : in std_logic; 
      GATE_AMP_THRESHOLD_ALARM_CROSSING: in std_logic_vector(GATE_ALARM_CROSSING-1 downto 0);
      GATE_TOF_ALGORITHM               : in std_logic_vector(GATE_TOF_ALGORITHM_RANGE-1 downto 0);
      GATE_TOF_AVG                     : in std_logic_vector(GATE_TOF_AVG_RANGE-1 downto 0);
      GATE_TOF_MIN_THICKNESS           : in std_logic_vector(GATE_TOF_RANGE-1 downto 0);
      GATE_TOF_ALARM_EN                : in std_logic; 
      GATE_TOF_ALARM_TYPE              : in std_logic;
      GATE_TOF_ALARM_CROSSING          : in std_logic_vector(GATE_ALARM_CROSSING-1 downto 0);
      GATE_TOF_ALARM_MIN_VALUE         : in std_logic_vector(GATE_TOF_RANGE-1 downto 0);
      GATE_TOF_ALARM_MAX_VALUE         : in std_logic_vector(GATE_TOF_RANGE-1 downto 0);
      GATE_TRACK_GATE_3                : in std_logic; 
      GATE_NORMALIZE_TO_AMP_GATE_1     : in std_logic; 
      -- GATE output
      END_GATE                         : in std_logic;
      GATE_VALID                       : out std_logic; 
      GATE_TOF                         : out std_logic_vector(GATE_TOF_RANGE-1 downto 0);
      GATE_TOF_AVERAGED                : out std_logic_vector(GATE_TOF_RANGE-1 downto 0);
      GATE_AMP_ALARM                   : out std_logic; 
      GATE_TOF_ALARM                   : out std_logic;
      GATE_MAX                         : out std_logic_vector(GATE_AXIS_GATE_RANGE_16-1 downto 0);
      GATE_MAX_SAMPLE                  : out std_logic_vector(GATE_RANGE-1 downto 0);
      GATE_MIN                         : out std_logic_vector(GATE_AXIS_GATE_RANGE_16-1 downto 0);
      GATE_MIN_SAMPLE                  : out std_logic_vector(GATE_RANGE-1 downto 0);
      -- AXI SLAVE PORT
      GATE_TREADY                      : out std_logic;
      GATE_TDATA                       : in std_logic_vector(GATE_AXIS_GATE_RANGE_16-1 downto 0);
      GATE_TLAST                       : in std_logic;
      GATE_TVALID                      : in std_logic
  );
end gatex_compute;

architecture Behavioral of gatex_compute is

    signal GATE_MAX_i           : std_logic_vector(GATE_AXIS_GATE_RANGE_16-1 downto 0);
    signal GATE_MIN_i           : std_logic_vector(GATE_AXIS_GATE_RANGE_16-1 downto 0);
    signal GATE_MAX_MIN_END     : std_logic;

    -- Gate data
    signal GATE_TDATA_RECT      : STD_LOGIC_VECTOR (GATE_AXIS_GATE_RANGE_16-1 downto 0);
    signal GATE_TDATA_i         : STD_LOGIC_VECTOR (GATE_AXIS_GATE_RANGE_16-1 downto 0);
    signal GATE_TDATA_i1        : STD_LOGIC_VECTOR (GATE_AXIS_GATE_RANGE_16-1 downto 0);
    signal GATE_TDATA_i2        : STD_LOGIC_VECTOR (GATE_AXIS_GATE_RANGE_16-1 downto 0);
    signal GATE_TVALID_i        : STD_LOGIC;
    signal GATE_TVALID_i1       : STD_LOGIC;
    signal GATE_TLAST_i         : STD_LOGIC;
    signal GATE_TLAST_i1        : STD_LOGIC;
    
    signal peak_plus_flag       : std_logic;
    
    -- Peak points
    signal peak_i               : STD_LOGIC_VECTOR (GATE_AXIS_GATE_RANGE_16-1 downto 0);
    signal peak_plus_i          : STD_LOGIC_VECTOR (GATE_AXIS_GATE_RANGE_16-1 downto 0);
    signal peak_minus_i         : STD_LOGIC_VECTOR (GATE_AXIS_GATE_RANGE_16-1 downto 0);
    signal peak_tof_i           : STD_LOGIC_VECTOR (GATE_RANGE-1 downto 0);
    signal tof_counter_sample   : unsigned (GATE_RANGE-1 downto 0);
     
    -- peak divider params
    signal peak_plus_divisor    : STD_LOGIC_VECTOR (GATE_TOF_RANGE-1 downto 0);
    signal peak_plus_dividend   : STD_LOGIC_VECTOR (GATE_TOF_RANGE-1 downto 0);
    signal peak_minus_divisor   : STD_LOGIC_VECTOR (GATE_TOF_RANGE-1 downto 0);
    signal peak_minus_dividend  : STD_LOGIC_VECTOR (GATE_TOF_RANGE-1 downto 0);  
  
    -- general divider prams
    signal dividend             : STD_LOGIC_VECTOR (GATE_TOF_RANGE-1 downto 0);
    signal divisor              : STD_LOGIC_VECTOR (GATE_TOF_RANGE-1 downto 0);
    signal divition_result      : signed (GATE_TOF_RANGE-1 downto 0);
    
    -- TOF SM   
    type states_tof is (idle1, idle2, peak_detect,prepare_divider_peak1, prepare_divider_peak2, 
        divide, divide_result, calculate_tof, calculate_tof_alarm, set_result, finish); 
    signal state_tof : states_tof; 
    
    -- Amplitude alarm SM   
    type states_amplitude_alarm is (idle, alarm, set_alarm,finish); 
    signal state_amplitude_alarm : states_amplitude_alarm; 
    
    -- Divider    
    COMPONENT gates_divider_32_32
      PORT (
        aclk : IN STD_LOGIC;
        aclken : IN STD_LOGIC;
        s_axis_divisor_tvalid : IN STD_LOGIC;
        s_axis_divisor_tdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        s_axis_dividend_tvalid : IN STD_LOGIC;
        s_axis_dividend_tdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        m_axis_dout_tvalid : OUT STD_LOGIC;
        m_axis_dout_tdata : OUT STD_LOGIC_VECTOR(63 DOWNTO 0)
      );
    END COMPONENT;
    
    signal M_AXIS_DOUT_tdata        : STD_LOGIC_VECTOR ( 63 downto 0 );
    signal M_AXIS_DOUT_tvalid       : STD_LOGIC;
    signal S_AXIS_DIVIDEND_tvalid   : STD_LOGIC;
    signal S_AXIS_DIVISOR_tvalid    : STD_LOGIC;
    signal divider_aclken           : STD_LOGIC;  
    
    -- TOF
    signal TOF_i                    : signed ( GATE_TOF_RANGE-1 downto 0 );
      
    -- AMP alarm
    signal GATE_AMP_VALID           : std_logic; 
    signal gate_amplitude_alarm_crossed : STD_LOGIC;
    
begin

	-- Rect negative value of DATA_IN
	process(aclk, aresetn)   
	begin
	    if (aresetn = '0') then
	       GATE_TVALID_i <= '0';
	       GATE_TLAST_i <= '0';
	       GATE_TDATA_RECT <= (others => '0');
	       GATE_TDATA_i <= (others => '0');
		elsif (rising_edge(aclk)) then
		   GATE_TVALID_i <= GATE_TVALID;
		   GATE_TLAST_i <= GATE_TLAST;
		   if(GATE_TVALID = '1') then
			 if (GATE_TDATA(GATE_AXIS_GATE_RANGE_16-1) = '0') then
	            GATE_TDATA_RECT <= GATE_TDATA;
			 else
				GATE_TDATA_RECT <= STD_LOGIC_VECTOR(not signed(GATE_TDATA) + 1);
			 end if;
			 GATE_TDATA_i <= GATE_TDATA;
	       else
	           GATE_TDATA_RECT <= (others => '0');
	           GATE_TDATA_i <= (others => '0');
		   end if;
		end if;
	end process;
	
	-- Delay GATE_DATA_IN_TDATA_RECT and GATE_DATA_IN_TVALID_i
    process(aclk, aresetn)   
    begin
        if (aresetn = '0') then
           GATE_TDATA_i1 <= (others => '0');
           GATE_TDATA_i2 <= (others => '0');
           GATE_TVALID_i1 <= '0';
           GATE_TLAST_i1 <= '0';
           tof_counter_sample <= (others => '0');
        elsif (rising_edge(aclk)) then
           GATE_TDATA_i1 <= GATE_TDATA_RECT;
           GATE_TDATA_i2 <= GATE_TDATA_i1;
           GATE_TVALID_i1 <= GATE_TVALID_i;
           GATE_TLAST_i1 <= GATE_TLAST_i;
           if(GATE_TVALID_i1 = '1') then
                tof_counter_sample <= tof_counter_sample + 1;
           else
                tof_counter_sample <= (others => '0');
           end if; 
        end if;
    end process;
      
	-- Peak Detector SM
    peak_detector_process: process(aclk, aresetn)   
    begin 
        if (aresetn = '0') then
            -- General
            GATE_TREADY <= '0';
            GATE_VALID <= '0';
            GATE_TOF <= (others => '0');
            GATE_TOF_AVERAGED <= (others => '0');
            GATE_TOF_ALARM <= '0';
            -- divider
            divider_aclken <= '0';
            S_AXIS_DIVIDEND_tvalid <= '0';
            S_AXIS_DIVISOR_tvalid <= '0';
            -- state
            state_tof <= idle1;  
        elsif (aclk'event and aclk = '1') then
            case state_tof is                    
                when idle1=>
                    if(GATE_EN = '1') then
                        GATE_TREADY <= '1';
                        state_tof <= idle2; 
                    else
                        GATE_TREADY <= '0';
                    end if;
                when idle2=>
                    if(GATE_EN = '1') then
                        if(GATE_TVALID = '1') then
                            peak_i <= (others => '0');
                            peak_plus_i <= (others => '0');
                            peak_minus_i <= (others => '0');
                            peak_tof_i <= (others => '0');
                            peak_plus_flag <= '0';
                            state_tof <= peak_detect;
                        end if;
                    else
                        state_tof <= idle1;
                    end if;
                when peak_detect =>
                    if(GATE_TVALID_i1 = '1') then
                        if(GATE_AMP_THRESHOLD_ALARM_EN = '1') then
                            if(unsigned(GATE_TDATA_i1) >= unsigned(GATE_AMP_THRESHOLD)) then
                                if (unsigned(GATE_TDATA_i1) > unsigned(peak_i)) then
                                    peak_i <= GATE_TDATA_i1;
                                    peak_minus_i <= GATE_TDATA_i2;
                                    peak_plus_i <= GATE_TDATA_RECT;
                                    peak_tof_i <= std_logic_vector(tof_counter_sample);    
                                end if;   
                            end if; 
                        else
                            if (unsigned(GATE_TDATA_i1) > unsigned(peak_i)) then
                                peak_i <= GATE_TDATA_i1;
                                peak_minus_i <= GATE_TDATA_i2;
                                peak_plus_i <= GATE_TDATA_RECT;
                                peak_tof_i <= std_logic_vector(tof_counter_sample);    
                            end if;                                                    
                        end if;
                        if(GATE_TLAST_i1 = '1' or tof_counter_sample >= (unsigned(GATE_WIDTH)-1)) then
                           state_tof <= prepare_divider_peak1;
                        end if;  
                    else
                        state_tof <= idle1;                
                    end if;  
                when prepare_divider_peak1 =>
                    peak_minus_dividend <= peak_minus_i(15) & peak_minus_i(15) &  peak_minus_i(15) &  peak_minus_i(15) & 
                            peak_minus_i(15) &  peak_minus_i(15) &  peak_minus_i(15) & peak_minus_i & "000000000";
                    peak_minus_divisor <= peak_minus_i(15) &  peak_minus_i(15) &  peak_minus_i(15) &  peak_minus_i(15) &  
                            peak_minus_i(15) &  peak_minus_i(15) &  peak_minus_i(15) &  peak_minus_i(15) & 
                            peak_minus_i(15) &  peak_minus_i(15) &  peak_minus_i(15) &  peak_minus_i(15) & 
                            peak_minus_i(15) &  peak_minus_i(15) &  peak_minus_i(15) &  peak_minus_i(15) & peak_minus_i;              
                    peak_plus_dividend <= peak_plus_i(15) & peak_plus_i(15) & peak_plus_i(15) & peak_plus_i(15) & 
                            peak_plus_i(15) & peak_plus_i(15) & peak_plus_i(15) & peak_plus_i & "000000000";
                    peak_plus_divisor <= peak_plus_i(15) & peak_plus_i(15) & peak_plus_i(15) & peak_plus_i(15) & 
                            peak_plus_i(15) & peak_plus_i(15) & peak_plus_i(15) & peak_plus_i(15) & 
                            peak_plus_i(15) & peak_plus_i(15) & peak_plus_i(15) & peak_plus_i(15) & 
                            peak_plus_i(15) & peak_plus_i(15) & peak_plus_i(15) & peak_plus_i(15) & peak_plus_i;
                    state_tof <= prepare_divider_peak2; 
                when prepare_divider_peak2 =>
                    if(peak_minus_dividend = peak_plus_dividend) then
                        dividend <= (others => '0'); 
                    else
                        dividend <= std_logic_vector(signed(peak_minus_dividend) - signed(peak_plus_dividend));
                    end if;
                    if(peak_minus_divisor = peak_plus_divisor) then
                        divisor <= "00000000000000000000000000000001";
                        dividend <= (others => '0');
                    else
                        divisor <= std_logic_vector(signed(peak_minus_divisor) + signed(peak_plus_divisor) - signed(peak_i) - signed(peak_i));
                    end if;
                    state_tof <= divide; 
                when divide =>
                    if((signed(dividend) = 0) or (signed(divisor) = 0)) then
                        state_tof <= set_result;  
                    else
                        divider_aclken <= '1';
                        S_AXIS_DIVIDEND_tvalid <= '1';
                        S_AXIS_DIVISOR_tvalid <= '1';
                        state_tof <= divide_result;
                    end if;                             
                when divide_result =>
                    if(M_AXIS_DOUT_tvalid = '1') then
                        divition_result <= signed(M_AXIS_DOUT_tdata(63 downto 32));
                        state_tof <= calculate_tof;   
                    end if;
                when calculate_tof =>
                    if(signed(peak_tof_i) = 0) then
                        TOF_i <= (others => '0');
                    else
                        TOF_i <= signed(GATE_START(21 downto 0) & "0000000000") + signed(peak_tof_i(21 downto 0) & "0000000000") + divition_result;
                    end if;
                    state_tof <= calculate_tof_alarm;
                when calculate_tof_alarm =>
                    if(GATE_TOF_ALARM_EN = '1') then
                        GATE_TOF_ALARM <= '0';
                        if(GATE_TOF_ALARM_CROSSING = "01") then
                            if(TOF_i > signed(GATE_TOF_ALARM_MAX_VALUE)) then
                                GATE_TOF_ALARM <= '1';
                            end if;
                        elsif(GATE_TOF_ALARM_CROSSING = "10") then
                            if(TOF_i < signed(GATE_TOF_ALARM_MIN_VALUE)) then
                                GATE_TOF_ALARM <= '1';
                            end if;    
                        elsif(GATE_TOF_ALARM_CROSSING = "11") then
                            if(TOF_i >= signed(GATE_TOF_ALARM_MIN_VALUE) and TOF_i <= signed(GATE_TOF_ALARM_MAX_VALUE)) then
                                GATE_TOF_ALARM <= '1';
                            end if;
                        else
                            GATE_TOF_ALARM <= '0';
                        end if;  
                    else
                        GATE_TOF_ALARM <= '0';
                    end if;   
                    state_tof <= set_result;             
                when set_result =>
                    GATE_TOF <= std_logic_vector(TOF_i);
                    GATE_TOF_AVERAGED <= std_logic_vector(TOF_i); -- TODO
                    divider_aclken <= '0';
                    S_AXIS_DIVIDEND_tvalid <= '0';
                    S_AXIS_DIVISOR_tvalid <= '0';
                    state_tof <= finish; 
                when finish => 
                    if((GATE_AMP_VALID = '1' or GATE_AMP_THRESHOLD_ALARM_EN = '0') and GATE_MAX_MIN_END = '1') then
                        GATE_VALID <= '1'; 
                        if(END_GATE = '1') then 
                            GATE_VALID <= '0'; 
                            state_tof <= idle1;
                        end if;
                    end if;                 
                when others =>
                    null;                   
            end case;                                                                                            
        end if;
    end process;

    -- signed divider
    gates_divider_32_32_inst : gates_divider_32_32
      PORT MAP (
        aclk => ACLK,
        aclken => divider_aclken,
        s_axis_divisor_tvalid => S_AXIS_DIVISOR_tvalid,
        s_axis_divisor_tdata => divisor,
        s_axis_dividend_tvalid => S_AXIS_DIVIDEND_tvalid,
        s_axis_dividend_tdata => dividend,
        m_axis_dout_tvalid => M_AXIS_DOUT_tvalid,
        m_axis_dout_tdata => M_AXIS_DOUT_tdata
      );
     
    -- Amplitude alarm
    amp_alarm:process(aclk, aresetn)   
    begin
        if (aresetn = '0') then
           gate_amplitude_alarm_crossed <= '0';
           -- output
           GATE_AMP_ALARM <= '0';
           GATE_AMP_VALID <= '0';        
        elsif (rising_edge(aclk)) then
            case state_amplitude_alarm is                    
                when idle =>   
                    if(GATE_AMP_THRESHOLD_ALARM_EN = '1' and GATE_EN = '1') then
                        if(GATE_TVALID = '1') then
                            state_amplitude_alarm <= alarm;
                            gate_amplitude_alarm_crossed <= '0';
                        end if;
                    else
                        GATE_AMP_ALARM <= '0';
                    end if;
                when alarm=>
                    if(GATE_TVALID_i1 = '1') then
                        if(signed(GATE_TDATA_i1) > signed(GATE_AMP_THRESHOLD)) then
                            gate_amplitude_alarm_crossed <= '1';
                        end if;                 
                        if(GATE_TLAST_i1 = '1' or tof_counter_sample >= (unsigned(GATE_WIDTH)-1)) then
                            state_amplitude_alarm <= set_alarm;
                        end if;                  
                    end if;   
                when set_alarm => 
                    if(GATE_AMP_THRESHOLD_ALARM_CROSSING = "01") then
                        if( gate_amplitude_alarm_crossed = '1') then
                            GATE_AMP_ALARM <= '1';   
                        else
                            GATE_AMP_ALARM <= '0';
                        end if;
                    elsif(GATE_AMP_THRESHOLD_ALARM_CROSSING = "10") then
                        if( gate_amplitude_alarm_crossed = '1') then
                            GATE_AMP_ALARM <= '0';   
                        else
                            GATE_AMP_ALARM <= '1';
                        end if;
                    else
                        GATE_AMP_ALARM <= '0';
                    end if;
                    state_amplitude_alarm <= finish;     
                when finish =>
                    GATE_AMP_VALID <= '1';                  
                    if(END_GATE = '1') then
                        GATE_AMP_VALID <= '0'; 
                        state_amplitude_alarm <= idle;
                    end if;        
                when others=>
                    null;
            end case;
        end if;
    end process;
    
    GATE_MIN <= GATE_MIN_i;
    GATE_MAX <= GATE_MAX_i;
    
    -- Max and min of the gate
    max_min:process(aclk, aresetn)   
    begin
        if (aresetn = '0') then
           -- output
           GATE_MAX_i <= (others => '0');
           GATE_MIN_i <= (others => '0'); 
           GATE_MAX_SAMPLE  <= (others => '0'); 
           GATE_MIN_SAMPLE  <= (others => '0'); 
           GATE_MAX_MIN_END <= '0';
        elsif (rising_edge(aclk)) then
            if(GATE_TVALID = '1' and GATE_TVALID_i= '0') then
                GATE_MAX_i <= GATE_TDATA;
                GATE_MIN_i <= GATE_TDATA;
                GATE_MAX_MIN_END <= '0';
            else
                if(GATE_TVALID_i = '1') then
                    if(signed(GATE_TDATA_i) >  signed(GATE_MAX_i)) then
                        GATE_MAX_i <= GATE_TDATA_i;
                        GATE_MAX_SAMPLE <= std_logic_vector(unsigned(GATE_START)+tof_counter_sample);
                    end if;
                    if(signed(GATE_TDATA_i) <  signed(GATE_MIN_i)) then
                        GATE_MIN_i <= GATE_TDATA_i;
                        GATE_MIN_SAMPLE <= std_logic_vector(unsigned(GATE_START)+tof_counter_sample);
                    end if;
                    if(GATE_TVALID = '0') then
                        GATE_MAX_MIN_END <= '1';
                    end if;
                end if;    
            end if;
        end if;
    end process;
                          
end Behavioral;
