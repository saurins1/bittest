library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity gates is
	generic (
		-- Users to add parameters here
        GATE_RANGE                      : integer    := 32;
        GATE_ALARM_CROSSING             : integer    := 2;
        GATE_TOF_ALGORITHM_RANGE        : integer    := 4;
        GATE_TOF_AVG_RANGE              : integer    := 8;
        GATE_TOF_RANGE                  : integer    := 32;
        GATE_AXIS_GATE_RANGE_48         : integer    := 48;
        GATE_AXIS_GATE_RANGE_16         : integer    := 16;
		-- User parameters ends
		-- Do not modify the parameters beyond this line

		-- Parameters of Axi Slave Bus Interface GATE_AXIS
		C_GATE_AXIS_TDATA_WIDTH	: integer	:= 16
	);
	port (
	
	    BEGIN_GATE                        : in std_logic;  
	    END_GATE                          : out std_logic;  

		GATE1_EN                          : in std_logic;  
		GATE1_START                       : in std_logic_vector(GATE_RANGE-1 downto 0);
		GATE1_WIDTH                       : in std_logic_vector(GATE_RANGE-1 downto 0);	
        GATE1_AMP_THRESHOLD               : in std_logic_vector(GATE_RANGE-1 downto 0);
        GATE1_AMP_THRESHOLD_ALARM_EN      : in std_logic; 
        GATE1_AMP_THRESHOLD_ALARM_CROSSING: in std_logic_vector(GATE_ALARM_CROSSING-1 downto 0);
        GATE1_TOF_ALGORITHM               : in std_logic_vector(GATE_TOF_ALGORITHM_RANGE-1 downto 0);
        GATE1_TOF_AVG                     : in std_logic_vector(GATE_TOF_AVG_RANGE-1 downto 0);
        GATE1_TOF_MIN_THICKNESS           : in std_logic_vector(GATE_TOF_RANGE-1 downto 0);
        GATE1_TOF_ALARM_EN                : in std_logic; 
        GATE1_TOF_ALARM_TYPE              : in std_logic;
        GATE1_TOF_ALARM_CROSSING          : in std_logic_vector(GATE_ALARM_CROSSING-1 downto 0);
        GATE1_TOF_ALARM_MIN_VALUE         : in std_logic_vector(GATE_TOF_RANGE-1 downto 0);
        GATE1_TOF_ALARM_MAX_VALUE         : in std_logic_vector(GATE_TOF_RANGE-1 downto 0);
        GATE1_TRACK_GATE_3                : in std_logic; 
        GATE1_NORMALIZE_TO_AMP_GATE_1     : in std_logic; 
        GATE1_TOF                         : out std_logic_vector(GATE_TOF_RANGE-1 downto 0);
        GATE1_TOF_AVERAGED                : out std_logic_vector(GATE_TOF_RANGE-1 downto 0);
        GATE1_AMP_ALARM                   : out std_logic; 
        GATE1_TOF_ALARM                   : out std_logic; 
        GATE1_MAX                         : out std_logic_vector(GATE_AXIS_GATE_RANGE_16-1 downto 0);
        GATE1_MAX_SAMPLE                  : out std_logic_vector(GATE_RANGE-1 downto 0);
        GATE1_MIN                         : out std_logic_vector(GATE_AXIS_GATE_RANGE_16-1 downto 0);
        GATE1_MIN_SAMPLE                  : out std_logic_vector(GATE_RANGE-1 downto 0);
        
        GATE2_EN                          : in std_logic;  
        GATE2_START                       : in std_logic_vector(GATE_RANGE-1 downto 0);
        GATE2_WIDTH                       : in std_logic_vector(GATE_RANGE-1 downto 0);    
        GATE2_AMP_THRESHOLD               : in std_logic_vector(GATE_RANGE-1 downto 0);
        GATE2_AMP_THRESHOLD_ALARM_EN      : in std_logic; 
        GATE2_AMP_THRESHOLD_ALARM_CROSSING: in std_logic_vector(GATE_ALARM_CROSSING-1 downto 0);
        GATE2_TOF_ALGORITHM               : in std_logic_vector(GATE_TOF_ALGORITHM_RANGE-1 downto 0);
        GATE2_TOF_AVG                     : in std_logic_vector(GATE_TOF_AVG_RANGE-1 downto 0);
        GATE2_TOF_MIN_THICKNESS           : in std_logic_vector(GATE_TOF_RANGE-1 downto 0);
        GATE2_TOF_ALARM_EN                : in std_logic; 
        GATE2_TOF_ALARM_TYPE              : in std_logic;
        GATE2_TOF_ALARM_CROSSING          : in std_logic_vector(GATE_ALARM_CROSSING-1 downto 0);
        GATE2_TOF_ALARM_MIN_VALUE         : in std_logic_vector(GATE_TOF_RANGE-1 downto 0);
        GATE2_TOF_ALARM_MAX_VALUE         : in std_logic_vector(GATE_TOF_RANGE-1 downto 0);
        GATE2_TRACK_GATE_3                : in std_logic; 
        GATE2_NORMALIZE_TO_AMP_GATE_1     : in std_logic; 
        GATE2_TOF                         : out std_logic_vector(GATE_TOF_RANGE-1 downto 0);
        GATE2_TOF_AVERAGED                : out std_logic_vector(GATE_TOF_RANGE-1 downto 0);
        GATE2_AMP_ALARM                   : out std_logic; 
        GATE2_TOF_ALARM                   : out std_logic; 
        GATE2_MAX                         : out std_logic_vector(GATE_AXIS_GATE_RANGE_16-1 downto 0);
        GATE2_MAX_SAMPLE                  : out std_logic_vector(GATE_RANGE-1 downto 0);
        GATE2_MIN                         : out std_logic_vector(GATE_AXIS_GATE_RANGE_16-1 downto 0);
        GATE2_MIN_SAMPLE                  : out std_logic_vector(GATE_RANGE-1 downto 0);
        
        GATE3_EN                          : in std_logic;  
        GATE3_START                       : in std_logic_vector(GATE_RANGE-1 downto 0);
        GATE3_WIDTH                       : in std_logic_vector(GATE_RANGE-1 downto 0);    
        GATE3_AMP_THRESHOLD               : in std_logic_vector(GATE_RANGE-1 downto 0);
        GATE3_AMP_THRESHOLD_ALARM_EN      : in std_logic; 
        GATE3_AMP_THRESHOLD_ALARM_CROSSING: in std_logic_vector(GATE_ALARM_CROSSING-1 downto 0);
        GATE3_TOF_ALGORITHM               : in std_logic_vector(GATE_TOF_ALGORITHM_RANGE-1 downto 0);
        GATE3_TOF_AVG                     : in std_logic_vector(GATE_TOF_AVG_RANGE-1 downto 0);
        GATE3_TOF_MIN_THICKNESS           : in std_logic_vector(GATE_TOF_RANGE-1 downto 0);
        GATE3_TOF_ALARM_EN                : in std_logic; 
        GATE3_TOF_ALARM_TYPE              : in std_logic;
        GATE3_TOF_ALARM_CROSSING          : in std_logic_vector(GATE_ALARM_CROSSING-1 downto 0);
        GATE3_TOF_ALARM_MIN_VALUE         : in std_logic_vector(GATE_TOF_RANGE-1 downto 0);
        GATE3_TOF_ALARM_MAX_VALUE         : in std_logic_vector(GATE_TOF_RANGE-1 downto 0);
        GATE3_TRACK_GATE_3                : in std_logic; 
        GATE3_NORMALIZE_TO_AMP_GATE_1     : in std_logic; 
        GATE3_TOF                         : out std_logic_vector(GATE_TOF_RANGE-1 downto 0);
        GATE3_TOF_AVERAGED                : out std_logic_vector(GATE_TOF_RANGE-1 downto 0);
        GATE3_AMP_ALARM                   : out std_logic; 
        GATE3_TOF_ALARM                   : out std_logic; 
        GATE3_MAX                         : out std_logic_vector(GATE_AXIS_GATE_RANGE_16-1 downto 0);
        GATE3_MAX_SAMPLE                  : out std_logic_vector(GATE_RANGE-1 downto 0);
        GATE3_MIN                         : out std_logic_vector(GATE_AXIS_GATE_RANGE_16-1 downto 0);
        GATE3_MIN_SAMPLE                  : out std_logic_vector(GATE_RANGE-1 downto 0);

		-- Ports of Axi Slave Bus Interface GATE1_AXIS
		gate_axis_aclk	  : in std_logic;
		gate_axis_aresetn : in std_logic;
		gate_axis_tready  : out std_logic;
		gate_axis_tdata	  : in std_logic_vector(C_GATE_AXIS_TDATA_WIDTH-1 downto 0);
		gate_axis_tstrb	  : in std_logic_vector((C_GATE_AXIS_TDATA_WIDTH/8)-1 downto 0);
		gate_axis_tlast	  : in std_logic;
		gate_axis_tvalid  : in std_logic
	);
	
end gates;

architecture arch_imp of gates is

    -- Ports of Axi Slave Bus Interface GATE1_AXIS
    signal gate1_axis_tready	: std_logic;
    signal gate1_axis_tdata	: std_logic_vector(C_GATE_AXIS_TDATA_WIDTH-1 downto 0);
    signal gate1_axis_tlast	: std_logic;
    signal gate1_axis_tvalid	: std_logic;

    -- Ports of Axi Slave Bus Interface GATE2_AXIS
    signal gate2_axis_tready	: std_logic;
    signal gate2_axis_tdata	: std_logic_vector(C_GATE_AXIS_TDATA_WIDTH-1 downto 0);
    signal gate2_axis_tlast	: std_logic;
    signal gate2_axis_tvalid	: std_logic;

    -- Ports of Axi Slave Bus Interface GATE3_AXIS
    signal gate3_axis_tready	: std_logic;
    signal gate3_axis_tdata	: std_logic_vector(C_GATE_AXIS_TDATA_WIDTH-1 downto 0);
    signal gate3_axis_tlast	: std_logic;
    signal gate3_axis_tvalid	: std_logic;
    
    -- GATE counter
    signal GATE_COUNTER  : unsigned(GATE_RANGE-1 downto 0);

    -- GATE valid
    signal GATE1_VALID  : std_logic;
    signal GATE2_VALID  : std_logic;
    signal GATE3_VALID  : std_logic;
       
    signal END_GATE_i  : std_logic;

    COMPONENT gate2_compute is
	generic (
        -- Users to add parameters here
        GATE_RANGE                      : integer    := 32;
        GATE_ALARM_CROSSING             : integer    := 2;
        GATE_TOF_ALGORITHM_RANGE        : integer    := 4;
        GATE_TOF_AVG_RANGE              : integer    := 8;
        GATE_TOF_RANGE                  : integer    := 32;
        GATE_AXIS_GATE_RANGE_48         : integer  := 48;
        GATE_AXIS_GATE_RANGE_16         : integer   := 16
    );
    Port ( 
      -- sync
      ACLK                             : in std_logic;
      ARESETN                          : in std_logic;                   
      -- GATE PARAMETERS
      GATE_EN                          : in std_logic;  
      GATE_START                       : in std_logic_vector(GATE_RANGE-1 downto 0);
      GATE_WIDTH                       : in std_logic_vector(GATE_RANGE-1 downto 0);    
      GATE_AMP_THRESHOLD               : in std_logic_vector(GATE_AXIS_GATE_RANGE_48-1 downto 0);
      GATE_AMP_THRESHOLD_ALARM_EN      : in std_logic; 
      GATE_AMP_THRESHOLD_ALARM_CROSSING: in std_logic_vector(GATE_ALARM_CROSSING-1 downto 0);
      GATE_TOF_ALGORITHM               : in std_logic_vector(GATE_TOF_ALGORITHM_RANGE-1 downto 0);
      GATE_TOF_AVG                     : in std_logic_vector(GATE_TOF_AVG_RANGE-1 downto 0);
      GATE_TOF_MIN_THICKNESS           : in std_logic_vector(GATE_TOF_RANGE-1 downto 0);
      GATE_TOF_ALARM_EN                : in std_logic; 
      GATE_TOF_ALARM_TYPE              : in std_logic;
      GATE_TOF_ALARM_CROSSING          : in std_logic_vector(GATE_ALARM_CROSSING-1 downto 0);
      GATE_TOF_ALARM_MIN_VALUE         : in std_logic_vector(GATE_TOF_RANGE-1 downto 0);
      GATE_TOF_ALARM_MAX_VALUE         : in std_logic_vector(GATE_TOF_RANGE-1 downto 0);
      GATE_TRACK_GATE_3                : in std_logic; 
      GATE_NORMALIZE_TO_AMP_GATE_1     : in std_logic; 
      -- GATE output
      END_GATE                         : in std_logic;
      GATE_VALID                       : out std_logic; 
      GATE_TOF                         : out std_logic_vector(GATE_TOF_RANGE-1 downto 0);
      GATE_TOF_AVERAGED                : out std_logic_vector(GATE_TOF_RANGE-1 downto 0);
      GATE_AMP_ALARM                   : out std_logic; 
      GATE_TOF_ALARM                   : out std_logic;
      GATE_MAX                         : out std_logic_vector(GATE_AXIS_GATE_RANGE_16-1 downto 0);
      GATE_MAX_SAMPLE                  : out std_logic_vector(GATE_RANGE-1 downto 0);
      GATE_MIN                         : out std_logic_vector(GATE_AXIS_GATE_RANGE_16-1 downto 0);
      GATE_MIN_SAMPLE                  : out std_logic_vector(GATE_RANGE-1 downto 0);
      -- AXI SLAVE PORT
      GATE_TREADY                      : out std_logic;
      GATE_TDATA                       : in std_logic_vector(GATE_AXIS_GATE_RANGE_48-1 downto 0);
      GATE_TLAST                       : in std_logic;
      GATE_TVALID                      : in std_logic
    );
    end COMPONENT gate2_compute;
    
    COMPONENT gatex_compute is
        generic (
            -- Users to add parameters here
            GATE_RANGE                      : integer    := 32;
            GATE_ALARM_CROSSING             : integer    := 2;
            GATE_TOF_ALGORITHM_RANGE        : integer    := 4;
            GATE_TOF_AVG_RANGE              : integer    := 8;
            GATE_TOF_RANGE                  : integer    := 32;
            GATE_AXIS_GATE_RANGE_48         : integer  := 48;
            GATE_AXIS_GATE_RANGE_16         : integer   := 16
      );
      Port ( 
          -- sync
          ACLK                             : in std_logic;
          ARESETN                          : in std_logic;                   
          -- GATE PARAMETERS
          GATE_EN                          : in std_logic;  
          GATE_START                       : in std_logic_vector(GATE_RANGE-1 downto 0);
          GATE_WIDTH                       : in std_logic_vector(GATE_RANGE-1 downto 0);    
          GATE_AMP_THRESHOLD               : in std_logic_vector(GATE_AXIS_GATE_RANGE_16-1 downto 0);
          GATE_AMP_THRESHOLD_ALARM_EN      : in std_logic; 
          GATE_AMP_THRESHOLD_ALARM_CROSSING: in std_logic_vector(GATE_ALARM_CROSSING-1 downto 0);
          GATE_TOF_ALGORITHM               : in std_logic_vector(GATE_TOF_ALGORITHM_RANGE-1 downto 0);
          GATE_TOF_AVG                     : in std_logic_vector(GATE_TOF_AVG_RANGE-1 downto 0);
          GATE_TOF_MIN_THICKNESS           : in std_logic_vector(GATE_TOF_RANGE-1 downto 0);
          GATE_TOF_ALARM_EN                : in std_logic; 
          GATE_TOF_ALARM_TYPE              : in std_logic;
          GATE_TOF_ALARM_CROSSING          : in std_logic_vector(GATE_ALARM_CROSSING-1 downto 0);
          GATE_TOF_ALARM_MIN_VALUE         : in std_logic_vector(GATE_TOF_RANGE-1 downto 0);
          GATE_TOF_ALARM_MAX_VALUE         : in std_logic_vector(GATE_TOF_RANGE-1 downto 0);
          GATE_TRACK_GATE_3                : in std_logic; 
          GATE_NORMALIZE_TO_AMP_GATE_1     : in std_logic; 
          -- GATE output
          END_GATE                         : in std_logic;
          GATE_VALID                       : out std_logic; 
          GATE_TOF                         : out std_logic_vector(GATE_TOF_RANGE-1 downto 0);
          GATE_TOF_AVERAGED                : out std_logic_vector(GATE_TOF_RANGE-1 downto 0);
          GATE_AMP_ALARM                   : out std_logic; 
          GATE_TOF_ALARM                   : out std_logic;
          GATE_MAX                         : out std_logic_vector(GATE_AXIS_GATE_RANGE_16-1 downto 0);
          GATE_MAX_SAMPLE                  : out std_logic_vector(GATE_RANGE-1 downto 0);
          GATE_MIN                         : out std_logic_vector(GATE_AXIS_GATE_RANGE_16-1 downto 0);
          GATE_MIN_SAMPLE                  : out std_logic_vector(GATE_RANGE-1 downto 0);
          -- AXI SLAVE PORT
          GATE_TREADY                      : out std_logic;
          GATE_TDATA                       : in std_logic_vector(GATE_AXIS_GATE_RANGE_16-1 downto 0);
          GATE_TLAST                       : in std_logic;
          GATE_TVALID                      : in std_logic
      );
    end COMPONENT gatex_compute;
    
    signal GATE1_TDATA    : std_logic_vector(GATE_AXIS_GATE_RANGE_16-1 downto 0);
    signal GATE2_TDATA    : std_logic_vector(GATE_AXIS_GATE_RANGE_48-1 downto 0);
    signal GATE3_TDATA    : std_logic_vector(GATE_AXIS_GATE_RANGE_16-1 downto 0);
    
    signal GATE1_AMP_THRESHOLD_i  : std_logic_vector(GATE_AXIS_GATE_RANGE_16-1 downto 0);
    signal GATE2_AMP_THRESHOLD_i  : std_logic_vector(GATE_AXIS_GATE_RANGE_48-1 downto 0);
    signal GATE3_AMP_THRESHOLD_i  : std_logic_vector(GATE_AXIS_GATE_RANGE_16-1 downto 0);
    
    -- VOLTA_GATE SM   
    type states_volta_gate is (idle, volta_gate_valid, volta_gate_end); 
    signal state_volta_gate : states_volta_gate; 

begin

    gate_axis_tready <= gate1_axis_tready and gate2_axis_tready and gate3_axis_tready;

	-- INPUT process
    input_process: process(gate_axis_aclk, gate_axis_aresetn)   
    begin 
        if (gate_axis_aresetn = '0') then
            GATE_COUNTER <= (others => '0');
            -- gates
            gate1_axis_tdata <= (others => '0');
            gate1_axis_tlast <= '0';
            gate1_axis_tvalid <= '0';
            gate2_axis_tdata <= (others => '0');
            gate2_axis_tlast <= '0';
            gate2_axis_tvalid <= '0';
            gate3_axis_tdata <= (others => '0');
            gate3_axis_tlast <= '0';
            gate3_axis_tvalid <= '0';
        elsif (gate_axis_aclk'event and gate_axis_aclk = '1') then
            if(gate_axis_tvalid = '1') then
                GATE_COUNTER <= GATE_COUNTER + 1;
                if(GATE_COUNTER >= (unsigned(GATE1_START)-1)) then
                    gate1_axis_tvalid <= '1';   
                    gate1_axis_tdata <= gate_axis_tdata; 
                    if(GATE_COUNTER >= (unsigned(GATE1_START)+unsigned(GATE1_WIDTH)-2)) then
                        gate1_axis_tlast <= '1';
                        if(GATE_COUNTER >= (unsigned(GATE1_START)+unsigned(GATE1_WIDTH)-1)) then
                            gate1_axis_tlast <= '0';
                            gate1_axis_tvalid <= '0'; 
                            gate1_axis_tdata <= (others => '0');
                        end if;
                    end if;
                end if;
                if(GATE_COUNTER >= (unsigned(GATE2_START)-1)) then
                    gate2_axis_tvalid <= '1';   
                    gate2_axis_tdata <= gate_axis_tdata; 
                    if(GATE_COUNTER >= (unsigned(GATE2_START)+unsigned(GATE2_WIDTH)-2)) then
                        gate2_axis_tlast <= '1';
                        if(GATE_COUNTER >= (unsigned(GATE2_START)+unsigned(GATE2_WIDTH)-1)) then
                            gate2_axis_tlast <= '0';
                            gate2_axis_tvalid <= '0'; 
                            gate2_axis_tdata <= (others => '0');
                        end if;
                    end if;
                end if;
                if(GATE_COUNTER >= (unsigned(GATE3_START)-1)) then
                    gate3_axis_tvalid <= '1';   
                    gate3_axis_tdata <= gate_axis_tdata; 
                    if(GATE_COUNTER >= (unsigned(GATE3_START)+unsigned(GATE3_WIDTH)-2)) then
                        gate3_axis_tlast <= '1';
                        if(GATE_COUNTER >= (unsigned(GATE3_START)+unsigned(GATE3_WIDTH)-1)) then
                            gate3_axis_tlast <= '0';
                            gate3_axis_tvalid <= '0'; 
                            gate3_axis_tdata <= (others => '0');
                        end if;
                    end if;
                end if;
            else
                GATE_COUNTER <= (others => '0');
                gate1_axis_tlast <= '0';
                gate1_axis_tvalid <= '0'; 
                gate1_axis_tdata <= (others => '0');
                gate2_axis_tdata <= (others => '0');
                gate2_axis_tlast <= '0';
                gate2_axis_tvalid <= '0';
                gate3_axis_tdata <= (others => '0');
                gate3_axis_tlast <= '0';
                gate3_axis_tvalid <= '0';
            end if;
        end if;
    end process;
       
    END_GATE <= END_GATE_i;

	-- MAIN SM
    main_sm: process(gate_axis_aclk, gate_axis_aresetn)   
    begin 
        if (gate_axis_aresetn = '0') then
            END_GATE_i <= '0';
            state_volta_gate <= idle;  
        elsif (gate_axis_aclk'event and gate_axis_aclk = '1') then
            case state_volta_gate is                    
                when idle=>
                    if(BEGIN_GATE = '1') then
                        state_volta_gate <= volta_gate_valid;     
                    end if;
                when volta_gate_valid=>
                    if((GATE1_VALID = '1' or GATE1_EN = '0') and 
                        (GATE2_VALID = '1' or GATE2_EN = '0') and 
                        (GATE3_VALID = '1' or GATE3_EN = '0')) then
                        END_GATE_i <= '1';
                        state_volta_gate <= volta_gate_end;
                    else
                        if(BEGIN_GATE = '0') then
                            state_volta_gate <= idle;
                        end if;
                    end if;
                when volta_gate_end=>
                    if(BEGIN_GATE = '0') then
                        END_GATE_i <= '0';
                        state_volta_gate <= idle;
                    end if;
                when others =>
                    null;
            end case;
        end if;
     end process;

     gate_compute_1: gatex_compute
        generic map(
            -- Users to add parameters here
            GATE_RANGE                  => GATE_RANGE,
            GATE_TOF_ALGORITHM_RANGE    => GATE_TOF_ALGORITHM_RANGE,
            GATE_TOF_AVG_RANGE          => GATE_TOF_AVG_RANGE,
            GATE_TOF_RANGE              => GATE_TOF_RANGE,
            GATE_AXIS_GATE_RANGE_48     => GATE_AXIS_GATE_RANGE_48,
            GATE_AXIS_GATE_RANGE_16     => GATE_AXIS_GATE_RANGE_16
        )
        port map( 
          ACLK                              => gate_axis_aclk,
          ARESETN                           => gate_axis_aresetn,
          -- GATE PARAMETERS
          GATE_EN                           => GATE1_EN,
          GATE_START                        => GATE1_START,
          GATE_WIDTH                        => GATE1_WIDTH,    
          GATE_AMP_THRESHOLD                => GATE1_AMP_THRESHOLD_i,
          GATE_AMP_THRESHOLD_ALARM_EN       => GATE1_AMP_THRESHOLD_ALARM_EN,
          GATE_AMP_THRESHOLD_ALARM_CROSSING => GATE1_AMP_THRESHOLD_ALARM_CROSSING,
          GATE_TOF_ALGORITHM                => GATE1_TOF_ALGORITHM,
          GATE_TOF_AVG                      => GATE1_TOF_AVG,
          GATE_TOF_MIN_THICKNESS            => GATE1_TOF_MIN_THICKNESS,
          GATE_TOF_ALARM_EN                 => GATE1_TOF_ALARM_EN,
          GATE_TOF_ALARM_TYPE               => GATE1_TOF_ALARM_TYPE,
          GATE_TOF_ALARM_CROSSING           => GATE1_TOF_ALARM_CROSSING,
          GATE_TOF_ALARM_MIN_VALUE          => GATE1_TOF_ALARM_MIN_VALUE,
          GATE_TOF_ALARM_MAX_VALUE          => GATE1_TOF_ALARM_MAX_VALUE,
          GATE_TRACK_GATE_3                 => GATE1_TRACK_GATE_3,
          GATE_NORMALIZE_TO_AMP_GATE_1      => GATE1_NORMALIZE_TO_AMP_GATE_1,
          -- GATE output
          END_GATE                          => END_GATE_i,
          GATE_VALID                        => GATE1_VALID,
          GATE_TOF                          => GATE1_TOF,
          GATE_TOF_AVERAGED                 => GATE1_TOF_AVERAGED,
          GATE_AMP_ALARM                    => GATE1_AMP_ALARM,
          GATE_TOF_ALARM                    => GATE1_TOF_ALARM, 
          GATE_MAX                          => GATE1_MAX,
          GATE_MAX_SAMPLE                   => GATE1_MAX_SAMPLE,
          GATE_MIN                          => GATE1_MIN,
          GATE_MIN_SAMPLE                   => GATE1_MIN_SAMPLE,
          -- AXI SLAVE PORT
          GATE_TREADY                       => gate1_axis_tready,
          GATE_TDATA                        => GATE1_TDATA,
          GATE_TLAST                        => gate1_axis_tlast,
          GATE_TVALID                       => gate1_axis_tvalid
      );
      
      GATE1_TDATA <= gate1_axis_tdata;
      GATE1_AMP_THRESHOLD_i <= GATE1_AMP_THRESHOLD(15 downto 0);
      
--     GATE1_TDATA <= gate1_axis_tdata(15) & gate1_axis_tdata(15) & gate1_axis_tdata(15) & gate1_axis_tdata(15) & gate1_axis_tdata(15) &
--        gate1_axis_tdata(15) & gate1_axis_tdata(15) & gate1_axis_tdata(15) & gate1_axis_tdata(15) & gate1_axis_tdata(15) &
--        gate1_axis_tdata(15) & gate1_axis_tdata(15) & gate1_axis_tdata(15) & gate1_axis_tdata(15) & gate1_axis_tdata(15) &
--        gate1_axis_tdata(15) & gate1_axis_tdata(15) & gate1_axis_tdata(15) & gate1_axis_tdata(15) & gate1_axis_tdata(15) &
--        gate1_axis_tdata(15) & gate1_axis_tdata(15) & gate1_axis_tdata(15) & gate1_axis_tdata(15) & gate1_axis_tdata(15) &
--        gate1_axis_tdata(15) & gate1_axis_tdata(15) & gate1_axis_tdata(15) & gate1_axis_tdata(15) & gate1_axis_tdata(15) &
--        gate1_axis_tdata(15) & gate1_axis_tdata(15) & gate1_axis_tdata;
        
--     GATE1_AMP_THRESHOLD_i <= GATE1_AMP_THRESHOLD(31) & GATE1_AMP_THRESHOLD(31) & GATE1_AMP_THRESHOLD(31) & GATE1_AMP_THRESHOLD(31) & GATE1_AMP_THRESHOLD(31) &
--          GATE1_AMP_THRESHOLD(31) & GATE1_AMP_THRESHOLD(31) & GATE1_AMP_THRESHOLD(31) & GATE1_AMP_THRESHOLD(31) & GATE1_AMP_THRESHOLD(31) &
--          GATE1_AMP_THRESHOLD(31) & GATE1_AMP_THRESHOLD(31) & GATE1_AMP_THRESHOLD(31) & GATE1_AMP_THRESHOLD(31) & GATE1_AMP_THRESHOLD(31) &
--          GATE1_AMP_THRESHOLD(31) & GATE1_AMP_THRESHOLD;
          
     gate_compute_2: gate2_compute
         generic map(
            -- Users to add parameters here
            GATE_RANGE                  => GATE_RANGE,
            GATE_TOF_ALGORITHM_RANGE    => GATE_TOF_ALGORITHM_RANGE,
            GATE_TOF_AVG_RANGE          => GATE_TOF_AVG_RANGE,
            GATE_TOF_RANGE              => GATE_TOF_RANGE,
            GATE_AXIS_GATE_RANGE_48     => GATE_AXIS_GATE_RANGE_48,
            GATE_AXIS_GATE_RANGE_16     => GATE_AXIS_GATE_RANGE_16
         )
         port map( 
           ACLK                              => gate_axis_aclk,
           ARESETN                           => gate_axis_aresetn,
           -- GATE PARAMETERS
           GATE_EN                           => GATE2_EN,
           GATE_START                        => GATE2_START,
           GATE_WIDTH                        => GATE2_WIDTH,    
           GATE_AMP_THRESHOLD                => GATE2_AMP_THRESHOLD_i,
           GATE_AMP_THRESHOLD_ALARM_EN       => GATE2_AMP_THRESHOLD_ALARM_EN,
           GATE_AMP_THRESHOLD_ALARM_CROSSING => GATE2_AMP_THRESHOLD_ALARM_CROSSING,
           GATE_TOF_ALGORITHM                => GATE2_TOF_ALGORITHM,
           GATE_TOF_AVG                      => GATE2_TOF_AVG,
           GATE_TOF_MIN_THICKNESS            => GATE2_TOF_MIN_THICKNESS,
           GATE_TOF_ALARM_EN                 => GATE2_TOF_ALARM_EN,
           GATE_TOF_ALARM_TYPE               => GATE2_TOF_ALARM_TYPE,
           GATE_TOF_ALARM_CROSSING           => GATE2_TOF_ALARM_CROSSING,
           GATE_TOF_ALARM_MIN_VALUE          => GATE2_TOF_ALARM_MIN_VALUE,
           GATE_TOF_ALARM_MAX_VALUE          => GATE2_TOF_ALARM_MAX_VALUE,
           GATE_TRACK_GATE_3                 => GATE2_TRACK_GATE_3,
           GATE_NORMALIZE_TO_AMP_GATE_1      => GATE2_NORMALIZE_TO_AMP_GATE_1,
           -- GATE output
           END_GATE                          => END_GATE_i,
           GATE_VALID                        => GATE2_VALID,
           GATE_TOF                          => GATE2_TOF,
           GATE_TOF_AVERAGED                 => GATE2_TOF_AVERAGED,
           GATE_AMP_ALARM                    => GATE2_AMP_ALARM,
           GATE_TOF_ALARM                    => GATE2_TOF_ALARM,
           GATE_MAX                          => GATE2_MAX,
           GATE_MAX_SAMPLE                   => GATE2_MAX_SAMPLE,
           GATE_MIN                          => GATE2_MIN,
           GATE_MIN_SAMPLE                   => GATE2_MIN_SAMPLE,
           -- AXI SLAVE PORT
           GATE_TREADY                       => gate2_axis_tready,
           GATE_TDATA                        => GATE2_TDATA,
           GATE_TLAST                        => gate2_axis_tlast, 
           GATE_TVALID                       => gate2_axis_tvalid
       );
       
     GATE2_TDATA <= gate2_axis_tdata(15) & gate2_axis_tdata(15) & gate2_axis_tdata(15) & gate2_axis_tdata(15) & gate2_axis_tdata(15) &
         gate2_axis_tdata(15) & gate2_axis_tdata(15) & gate2_axis_tdata(15) & gate2_axis_tdata(15) & gate2_axis_tdata(15) &
         gate2_axis_tdata(15) & gate2_axis_tdata(15) & gate2_axis_tdata(15) & gate2_axis_tdata(15) & gate2_axis_tdata(15) &
         gate2_axis_tdata(15) & gate2_axis_tdata(15) & gate2_axis_tdata(15) & gate2_axis_tdata(15) & gate2_axis_tdata(15) &
         gate2_axis_tdata(15) & gate2_axis_tdata(15) & gate2_axis_tdata(15) & gate2_axis_tdata(15) & gate2_axis_tdata(15) &
         gate2_axis_tdata(15) & gate2_axis_tdata(15) & gate2_axis_tdata(15) & gate2_axis_tdata(15) & gate2_axis_tdata(15) &
         gate2_axis_tdata(15) & gate2_axis_tdata(15) & gate2_axis_tdata;
         
     GATE2_AMP_THRESHOLD_i <= GATE2_AMP_THRESHOLD(31) & GATE2_AMP_THRESHOLD(31) & GATE2_AMP_THRESHOLD(31) & GATE2_AMP_THRESHOLD(31) & GATE2_AMP_THRESHOLD(31) &
         GATE2_AMP_THRESHOLD(31) & GATE2_AMP_THRESHOLD(31) & GATE2_AMP_THRESHOLD(31) & GATE2_AMP_THRESHOLD(31) & GATE2_AMP_THRESHOLD(31) &
         GATE2_AMP_THRESHOLD(31) & GATE2_AMP_THRESHOLD(31) & GATE2_AMP_THRESHOLD(31) & GATE2_AMP_THRESHOLD(31) & GATE2_AMP_THRESHOLD(31) &
         GATE2_AMP_THRESHOLD(31) & GATE2_AMP_THRESHOLD;
       
     gate_compute_3: gatex_compute
           generic map(
            -- Users to add parameters here
            GATE_RANGE                  => GATE_RANGE,
            GATE_TOF_ALGORITHM_RANGE    => GATE_TOF_ALGORITHM_RANGE,
            GATE_TOF_AVG_RANGE          => GATE_TOF_AVG_RANGE,
            GATE_AXIS_GATE_RANGE_48     => GATE_AXIS_GATE_RANGE_48,
            GATE_AXIS_GATE_RANGE_16     => GATE_AXIS_GATE_RANGE_16
           )
           port map( 
             ACLK                              => gate_axis_aclk,
             ARESETN                           => gate_axis_aresetn,
             -- GATE PARAMETERS
             GATE_EN                           => GATE3_EN,
             GATE_START                        => GATE3_START,
             GATE_WIDTH                        => GATE3_WIDTH,    
             GATE_AMP_THRESHOLD                => GATE3_AMP_THRESHOLD_i,
             GATE_AMP_THRESHOLD_ALARM_EN       => GATE3_AMP_THRESHOLD_ALARM_EN,
             GATE_AMP_THRESHOLD_ALARM_CROSSING => GATE3_AMP_THRESHOLD_ALARM_CROSSING,
             GATE_TOF_ALGORITHM                => GATE3_TOF_ALGORITHM,
             GATE_TOF_AVG                      => GATE3_TOF_AVG,
             GATE_TOF_MIN_THICKNESS            => GATE3_TOF_MIN_THICKNESS,
             GATE_TOF_ALARM_EN                 => GATE3_TOF_ALARM_EN,
             GATE_TOF_ALARM_TYPE               => GATE3_TOF_ALARM_TYPE,
             GATE_TOF_ALARM_CROSSING           => GATE3_TOF_ALARM_CROSSING,
             GATE_TOF_ALARM_MIN_VALUE          => GATE3_TOF_ALARM_MIN_VALUE,
             GATE_TOF_ALARM_MAX_VALUE          => GATE3_TOF_ALARM_MAX_VALUE,
             GATE_TRACK_GATE_3                 => GATE3_TRACK_GATE_3,
             GATE_NORMALIZE_TO_AMP_GATE_1      => GATE3_NORMALIZE_TO_AMP_GATE_1,
             -- GATE output
             END_GATE                          => END_GATE_i,
             GATE_VALID                        => GATE3_VALID,
             GATE_TOF                          => GATE3_TOF,
             GATE_TOF_AVERAGED                 => GATE3_TOF_AVERAGED,
             GATE_AMP_ALARM                    => GATE3_AMP_ALARM,
             GATE_TOF_ALARM                    => GATE3_TOF_ALARM,
             GATE_MAX                          => GATE3_MAX,
             GATE_MAX_SAMPLE                   => GATE3_MAX_SAMPLE,
             GATE_MIN                          => GATE3_MIN,
             GATE_MIN_SAMPLE                   => GATE3_MIN_SAMPLE,
             -- AXI SLAVE PORT
             GATE_TREADY                       => gate3_axis_tready,
             GATE_TDATA                        => GATE3_TDATA,
             GATE_TLAST                        => gate3_axis_tlast,
             GATE_TVALID                       => gate3_axis_tvalid
         ); 
         
         GATE3_TDATA <= gate3_axis_tdata;
         GATE3_AMP_THRESHOLD_i <= GATE3_AMP_THRESHOLD(15 downto 0);
         
end arch_imp;
