----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 09/27/2017 10:07:23 AM
-- Design Name: 
-- Module Name: channel - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity channel is
	generic (
		-- General
		CHANNEL_ID		: INTEGER := 0;
		REGISTER_W64    : INTEGER := 64;
		REGISTER_W48    : INTEGER := 48;
	    REGISTER_W32    : INTEGER := 32;
	    REGISTER_W24    : INTEGER := 24;
	    REGISTER_W21    : INTEGER := 21;
        REGISTER_W16    : INTEGER := 16;
		REGISTER_W14    : INTEGER := 14;
		REGISTER_W8     : INTEGER := 8;
		REGISTER_W4     : INTEGER := 4;
		REGISTER_W2     : INTEGER := 2;
		
		-- Data compression
        DC_BRAM_ADDR_WIDTH : INTEGER := 11;
        DC_BRAM_DATA_WIDTH : INTEGER := 32;
        
        COMPRESSION_NO          : INTEGER := 0; -- DATA_FORMAT
        COMPRESSION_ASCAN       : INTEGER := 1; -- DATA_FORMAT
        COMPRESSION_GATES       : INTEGER := 2; -- DATA_FORMAT
        COMPRESSION_ASCAN_GATES : INTEGER := 3; -- DATA_FORMAT
        COMPRESSION_BURST       : INTEGER := 4; -- DATA_FORMAT
        COMPRESSION_ASCAN_DEC   : INTEGER := 5; -- DATA_FORMAT
        COMPRESSION_OSCILLOSCOPE: INTEGER := 6; -- DATA_FORMAT
		
		-- SENSOR EMAT
		SENSOR_TYPE   : INTEGER := 3;
		
		-- Scan mode
	    SCAN_MODE_NORMAL           : INTEGER := 0;
        SCAN_MODE_MRUT             : INTEGER := 10;
        SCAN_MODE_MRUT_PITCH_CATH  : INTEGER := 11;
        SCAN_MODE_MRUT_DIR         : INTEGER := 12;
        
		-- Trigger type
        TRIGGER_TYPE_NORMAL     : INTEGER := 0;
        TRIGGER_TYPE_SW         : INTEGER := 10;
        TRIGGER_TYPE_SW_PWM     : INTEGER := 11;
        TRIGGER_TYPE_ENC        : INTEGER := 20;
        TRIGGER_TYPE_ROBOT      : INTEGER := 30;
        TRIGGER_TYPE_ROBOT_ENC  : INTEGER := 40;
        
        -- Decimation type
        DEC_TYPE_NORMAL     : INTEGER := 0;
        DEC_TYPE_CORDINATES : INTEGER := 1;
        
        DEC_TYPE_NONE       : INTEGER := 0;
        DEC_TYPE_SIMPLE     : INTEGER := 1;
        DEC_TYPE_MINMAX     : INTEGER := 2;
        DEC_TYPE_ALOK       : INTEGER := 3;
        
		-- external trigger
		DISPOSITION_PASS  : INTEGER := 1;
		DISPOSITION_FAIL  : INTEGER := 2;
		DISPOSITION_NODET : INTEGER := 0;
		
		-- Coinc
        TRIGGER_WAIT    : INTEGER := 32;
        MIN_COINC_LEVEL : INTEGER := 2;
        COINC_TYPE_NORMAL         : INTEGER := 0;
        COINC_TYPE_PIPELINE       : INTEGER := 1;
        COINC_TYPE_PIPELINE_ACC   : INTEGER := 2;
        COINC_TYPE_ACC            : INTEGER := 3;
		
		-- Avg
		MIN_AVG_LEVEL   	: INTEGER := 2;
		BRAM_ADDR_WIDTH     : INTEGER := 16;
        BRAM_DATA_WIDTH     : INTEGER := 23;
        AVG_TYPE_NORMAL         : INTEGER := 0;
        AVG_TYPE_PIPELINE       : INTEGER := 1;
        AVG_TYPE_PIPELINE_ACC   : INTEGER := 2;
        AVG_TYPE_ACC            : INTEGER := 3;
        
        -- Dsp
        RECTIFICATION_DISABLED    : INTEGER := 0;
        RECTIFICATION_MED   : INTEGER := 1;
        RECTIFICATION_FULL  : INTEGER := 2;
     		
		-- Motor
		MOTOR_STOP_CYCLES   : INTEGER := 50000000; --500ms
				
		-- LRUT
		LRUT_WAIT_CYCLES   : INTEGER := 700000; --7ms
			
		-- Ascan header size
		ASCAN_HEADER_SIZE : INTEGER := 96;
		
		-- FIR CORR
        N_TAPS              : INTEGER    := 512;
        N_SETS_TAP          : INTEGER    := 3;
        OVERSAMPLING        : INTEGER    := 2;
        WAIT_CYCLES         : INTEGER    := 4
	);
	port (
	    -- Sync
        aclk                    : in STD_LOGIC; 
        aresetn                 : in STD_LOGIC;
		
		-- Control	
		CONFIGURE_CONTROL  	  : in STD_LOGIC;
		
		IDLE_STATE_IN  	      : in STD_LOGIC;
		IDLE_STATE_OUT  	  : out STD_LOGIC;
		
		-- Start inspection
		START_INSPECTION_CONTROL  : in STD_LOGIC;
		START_INSPECTION_STATUS   : out STD_LOGIC;
		
		-- Start top inspection
        START_STOP_INSPECTION_CONTROL  : in STD_LOGIC;
        START_STOP_INSPECTION_STATUS   : out STD_LOGIC;
        SYSTEM_READY_STATUS            : out STD_LOGIC;
        DISPOSITION		  : out STD_LOGIC; 
        DISPOSITION_BITS  : out STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0); --
		
		-- Scan mode
		SCAN_MODE_CONTROL : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        SCAN_MODE_STATUS  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        
        -- Trigger type
        TRIGGER_TYPE_CONTROL : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRIGGER_TYPE_STATUS  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        
        -- Trigger sw
        TRIGGER_SW_CONTROL : in STD_LOGIC;
        TRIGGER_SW_STATUS  : out STD_LOGIC;
		
		-- PWM
		PWM_MOTOR_EN     : in STD_LOGIC;
		PWM_END          : in STD_LOGIC;
		PWM_MOTOR_MOVE   : out STD_LOGIC;
		PWM_MOTOR_NOT_MOVING  : out STD_LOGIC;
		
		-- LRUT
		LRUT_EN          : out STD_LOGIC;
		
		--Pulser Status
        PULSER_STATUS    : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        
        --Voltage Status
        VOLTAGE_STATUS   : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        
        --TEMPERATURE
        TEMPERATURE_STATUS : in std_logic_vector(REGISTER_W32-1 downto 0);
        
        -- Channel HW
        CHANNEL_HW       : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        
        -- Ascan
        READ_ASCAN  	 : in STD_LOGIC;
        ASCAN_READY      : out STD_LOGIC;
        ASCAN_SIZE       : out STD_LOGIC_VECTOR(REGISTER_W21-1 downto 0);
				
		-- Debug
		CHANNEL_ENABLED  : out STD_LOGIC;
		CHANNEL_TRIGGER  : out STD_LOGIC;
		CONFIG_ON        : out STD_LOGIC;
		CHANNEL_STATE_A	 : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		CHANNEL_STATE_B	 : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		CHANNEL_STATE_C	 : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
			
		-- Signal input
		SIGNAL_COINC_WINDOW  	  : out STD_LOGIC;
		SIGNAL_COINC_COUNTER      : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		SIGNAL_AVG_WINDOW  		  : out STD_LOGIC;
		SIGNAL_AVG_COUNTER        : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		
		SIGNAL_INPUT_AXIS_TREADY  : out STD_LOGIC;
		SIGNAL_INPUT_AXIS_TDATA	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		SIGNAL_INPUT_AXIS_TLAST	  : in STD_LOGIC;
		SIGNAL_INPUT_AXIS_TVALID  : in STD_LOGIC;
		
		-- Config
		CONFIG_INPUT_LOAD	  	  : in STD_LOGIC;
		CONFIG_INPUT_AXIS_TREADY  : out STD_LOGIC;
		CONFIG_INPUT_AXIS_TDATA	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		CONFIG_INPUT_AXIS_TLAST	  : in STD_LOGIC;
		CONFIG_INPUT_AXIS_TVALID  : in STD_LOGIC;
		
		-- FFT
        CC_CONFIG_ON_ACK          : out STD_LOGIC;
        CC_CONFIG_ON              : in STD_LOGIC;        
        CC_CONFIG_ADDR            : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        CC_CONFIG_DATA            : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);  
		
		-- Encoders
		ENCODER1_POS  	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		ENCODER1_DIR  	  : in STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
		ENCODER1_RPM  	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		ENCODER1_TRIGGER_COUNTS : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		ENCODER2_POS  	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		ENCODER2_DIR  	  : in STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
		ENCODER2_RPM  	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		ENCODER2_TRIGGER_COUNTS : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		ENCODER3_POS  	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		ENCODER3_DIR  	  : in STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
		ENCODER3_RPM  	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		ENCODER3_TRIGGER_COUNTS : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		ENCODER_MAX_OVERSPEED_ALARM : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		ENCODER_RESET_TOP      : in STD_LOGIC;
        ENCODER_RESET_CHANNEL  : out STD_LOGIC;
		MAX_OVERSPEED_ALARM_1  : out STD_LOGIC;
		MAX_OVERSPEED_ALARM_2  : out STD_LOGIC;
				
		-- IO
		IO_INPUTS  	  	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		IO_OUTPUTS  	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		
		-- Config output receiver
		RECEIVER_SAMPLING_FREQUENCY         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        RECEIVER_DATA_WINDOW                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        RECEIVER_DELAY                      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        RECEIVER_ANALOG_GAIN                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  --db*1024
        RECEIVER_DIGITAL_GAIN               : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  --V*1024*1024  
        RECEIVER_EXTERNAL_MULTIPLEXER_EN    : out STD_LOGIC;
		
		-- Config output transmitter
		TRANSMITTER_VOLTAGE                 : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        TRANSMITTER_BURST_FREQUENCY         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        TRANSMITTER_N_CYCLES                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        TRANSMITTER_N_BURST                 : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_DELAY                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_DELTA_DELAY             : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_DELTA_ADD               : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_DIRECTIONAL_PHASING     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        TRANSMITTER_START_CYCLE	            : out STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
		
		-- Config output magnet
		MAGNET_MODE                         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        MAGNET_PULSE_WIDTH                  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        MAGNET_INITIAL_DELAY                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        MAGNET_RAMP_UP_VOLTAGE              : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        MAGNET_VOLTAGE                      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		-- Condig analog filters
		DSP_ANALOG_FILTER  					: out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
		DSP_COINC_LEVEL                     : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		DSP_AVG_LEVEL                       : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		DSP_AVG_TYPE                        : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		-- Config DAC curves
		CONFIG_DAC_AXIS_TREADY    : in STD_LOGIC;
		CONFIG_DAC_AXIS_TDATA	  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		CONFIG_DAC_AXIS_TLAST	  : out STD_LOGIC;
		CONFIG_DAC_AXIS_TVALID    : out STD_LOGIC;
					
		-- ASCAN
		ASCAN_AXIS_TREADY  : in STD_LOGIC;
		ASCAN_AXIS_TDATA   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		ASCAN_AXIS_TLAST   : out STD_LOGIC;
		ASCAN_AXIS_TVALID  : out STD_LOGIC;
		ASCAN_AXIS_TKEEP   : out std_logic_vector((REGISTER_W32/8)-1 downto 0)
	);
end channel;

architecture arch_imp of channel is
	
	-- DIGITAL AMPLIFIER
	COMPONENT digital_amplifier is
	generic (
		REGISTER_W48        : INTEGER    := 48;
        REGISTER_W32        : INTEGER    := 32;
        REGISTER_W16        : INTEGER    := 16;
		DIVIDER_FACTOR      : INTEGER    := 20; -- /(1024*1024)
		MULTIPLIER_DELAY    : INTEGER    := 4
    );
    port (
        aclk         : in std_logic;
        aresetn      : in std_logic;
		
		-- Gain
		DIGITAL_AMPLIFIER_GAIN	: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		-- Signal in
		SIGNAL_INPUT_AXIS_TREADY 	: out STD_LOGIC;
		SIGNAL_INPUT_AXIS_TDATA 	: in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		SIGNAL_INPUT_AXIS_TLAST		: in STD_LOGIC;
		SIGNAL_INPUT_AXIS_TVALID  	: in STD_LOGIC;

		-- Signal out
		SIGNAL_OUTPUT_AXIS_TREADY 	: in STD_LOGIC;
		SIGNAL_OUTPUT_AXIS_TDATA	: out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		SIGNAL_OUTPUT_AXIS_TLAST	: out STD_LOGIC;
		SIGNAL_OUTPUT_AXIS_TVALID  : out STD_LOGIC		
    );
	end COMPONENT digital_amplifier;

	-- signal output = input for coincidence
	signal signal_output_axis_tready_i	: STD_LOGIC;
	signal signal_output_axis_tdata_i	: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal signal_output_axis_tlast_i	: STD_LOGIC;
	signal signal_output_axis_tvalid_i  : STD_LOGIC;	
	
	-- Signal input tready
	signal digital_amplifier_tready  : STD_LOGIC;

	-- COINCIDENCE FILTER
	COMPONENT coincidence_filter is
        generic (
            REGISTER_W32    : INTEGER := 32;
            REGISTER_W16    : INTEGER := 16;
            REGISTER_W8     : INTEGER := 8;
            COINC_TYPE_NORMAL         : INTEGER := 0;
            COINC_TYPE_PIPELINE       : INTEGER := 1;
            COINC_TYPE_PIPELINE_ACC   : INTEGER := 2;
            COINC_TYPE_ACC            : INTEGER := 3;
            TRIGGER_WAIT    : INTEGER := 32;
            MIN_COINC_LEVEL : INTEGER := 2
        );
        port (
            -- Sync
            aclk         : in STD_LOGIC;
            aresetn      : in STD_LOGIC;

            -- Config
            COINC_LEVEL        : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            COINC_RESET     : in STD_LOGIC;
            COINC_RESET_ACK : out STD_LOGIC;
            COINC_END       : out STD_LOGIC;
            
            COINC_TYPE         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            COINC_ACC           : out STD_LOGIC;
            
            PHASE_SHIFT_LEVEL     : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            PHASE_SHIFT           : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            N_BURST               : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            
            DATA_WINDOW        : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            SAMPLE_FREQ        : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

            DATA_WINDOW_RESULT    : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            SAMPLE_FREQ_RESULT    : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            COINC_TYPE_RESULT    : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            COINC_LEVEL_RESULT    : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            COINC_COUNTER     : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
            COINC_WINDOW        : out STD_LOGIC;
            
            FIR_DELAY        : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            FIR_DELAY_ON     : in STD_LOGIC;

            -- Signal input
            SIGNAL_INPUT_AXIS_TREADY  : out STD_LOGIC;
            SIGNAL_INPUT_AXIS_TDATA      : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
            SIGNAL_INPUT_AXIS_TLAST      : in STD_LOGIC;
            SIGNAL_INPUT_AXIS_TVALID  : in STD_LOGIC;

            -- Coincidence result
            COINC_RESULT_AXIS_TVALID  : out STD_LOGIC;
            COINC_RESULT_AXIS_TDATA      : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
            COINC_RESULT_AXIS_TLAST      : out STD_LOGIC;
            COINC_RESULT_AXIS_TREADY  : in STD_LOGIC
        );
    end COMPONENT coincidence_filter;

	-- DATA window
	signal coinc_data_window_result	 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal coinc_sample_freq_result	 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

	-- Advanced filter
	signal transmitter_phase_shift : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal dsp_phase_shift	 	   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal transmitter_n_burst_i   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	
	-- Signal input tready
	signal coinc_signal_input_axis_tready  : STD_LOGIC;
	signal coinc_signal_input_axis_tvalid  : STD_LOGIC;
	signal coinc_signal_input_axis_tlast   : STD_LOGIC;
	signal coinc_signal_input_axis_tdata   : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);

	-- Coincidence result
	signal coinc_result_axis_tready  : STD_LOGIC;
	signal coinc_result_axis_tvalid  : STD_LOGIC;
	signal coinc_result_axis_tdata	 : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal coinc_result_axis_tlast	 : STD_LOGIC;

	signal coinc_level_result : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal coinc_type_result  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	
	signal coinc_type      : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal coinc_acc       : STD_LOGIC;

	signal coinc_reset       : STD_LOGIC;
	signal coinc_reset_ack   : STD_LOGIC;
	signal coinc_counter	 : UNSIGNED(REGISTER_W32-1 downto 0);
	signal avg_counter_i	 : UNSIGNED(REGISTER_W32-1 downto 0);
	
	
	signal avg_coinc_pos : STD_LOGIC;
	
	signal coinc_fir_on  : STD_LOGIC;

	COMPONENT average_filter is
		generic (
			REGISTER_W32        : INTEGER := 32;
			REGISTER_W16        : INTEGER := 16;
			REGISTER_W8         : INTEGER := 8;
			REGISTER_W14        : INTEGER := 14;
			BRAM_ADDR_WIDTH     : INTEGER := 16;
            BRAM_DATA_WIDTH     : INTEGER := 23;
            AVG_TYPE_NORMAL         : INTEGER := 0;
            AVG_TYPE_PIPELINE       : INTEGER := 1;
            AVG_TYPE_PIPELINE_ACC   : INTEGER := 2;
            AVG_TYPE_ACC            : INTEGER := 3;
			TRIGGER_WAIT        : INTEGER := 32;
			MIN_AVG_LEVEL       : INTEGER := 2
		);
		port (
			-- Sync
			aclk         : in STD_LOGIC;
			aresetn      : in STD_LOGIC;

			-- Control
			AVG_LEVEL	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);    
			DATA_WINDOW	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			SAMPLE_FREQ	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			DATA_WINDOW_RESULT	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			SAMPLE_FREQ_RESULT	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			AVG_COUNTER         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
			AVG_WINDOW  	    : out STD_LOGIC;

			AVG_TYPE	     : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			AVG_ACC  	     : out STD_LOGIC;
			AVG_END  	     : out STD_LOGIC;
			
			AVG_RESET        : in STD_LOGIC;
			AVG_RESET_ACK    : out STD_LOGIC;
			AVG_TYPE_RESULT	 : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			AVG_LEVEL_RESULT : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			
		    FIR_DELAY        : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            FIR_DELAY_ON     : in STD_LOGIC;
			
			-- Signal input
			SIGNAL_INPUT_AXIS_TREADY  : out STD_LOGIC;
			SIGNAL_INPUT_AXIS_TDATA	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
			SIGNAL_INPUT_AXIS_TLAST	  : in STD_LOGIC;
			SIGNAL_INPUT_AXIS_TVALID  : in STD_LOGIC;

			-- Average result
			AVG_RESULT_AXIS_TVALID    : out STD_LOGIC;
			AVG_RESULT_AXIS_TDATA	  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
			AVG_RESULT_AXIS_TLAST	  : out STD_LOGIC;
			AVG_RESULT_AXIS_TREADY    : in STD_LOGIC
		);
	end COMPONENT average_filter;
	
	-- DATA window
	signal avg_data_window_result	 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal avg_sample_freq_result	 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

	-- Signal input tready
	signal avg_signal_input_axis_tready  : STD_LOGIC;
	signal avg_signal_input_axis_tvalid  : STD_LOGIC;
    signal avg_signal_input_axis_tlast   : STD_LOGIC;
    signal avg_signal_input_axis_tdata   : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);

	-- Average result
	signal avg_result_axis_tready   : STD_LOGIC;
	signal avg_result_axis_tvalid   : STD_LOGIC;
	signal avg_result_axis_tdata	: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal avg_result_axis_tlast	: STD_LOGIC;

	signal avg_type	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal avg_type_i	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal avg_acc : STD_LOGIC;
	signal avg_acc_i : STD_LOGIC;
	signal avg_end : STD_LOGIC;
	signal avg_type_result	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal avg_level_result	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal avg_reset : STD_LOGIC;
	signal avg_reset_ack : STD_LOGIC;
	signal avg_counter	: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	
	signal avg_fir_on  : STD_LOGIC;
	
    COMPONENT dsp_filter is
        generic (
            REGISTER_W16        : INTEGER := 16;
            REGISTER_W2         : INTEGER := 2;
            RECTIFICATION_DISABLED    : INTEGER := 0;
            RECTIFICATION_MED   : INTEGER := 1;
            RECTIFICATION_FULL  : INTEGER := 2
        );
        port (
            -- Sync
            aclk         : in STD_LOGIC;
            aresetn      : in STD_LOGIC;
        
            -- Control
            DSP_INVERSION              : in STD_LOGIC;
            DSP_RECTIFICATION          : in STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
                
            -- Signal input
            SIGNAL_INPUT_AXIS_TREADY  : out STD_LOGIC;
            SIGNAL_INPUT_AXIS_TDATA      : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
            SIGNAL_INPUT_AXIS_TLAST      : in STD_LOGIC;
            SIGNAL_INPUT_AXIS_TVALID  : in STD_LOGIC;
    
            -- DSP result
            DSP_RESULT_AXIS_TVALID    : out STD_LOGIC;
            DSP_RESULT_AXIS_TDATA      : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
            DSP_RESULT_AXIS_TLAST      : out STD_LOGIC;
            DSP_RESULT_AXIS_TREADY    : in STD_LOGIC
        );
    end COMPONENT dsp_filter;

	signal dsp_signal_input_axis_tready  : STD_LOGIC;
    signal dsp_signal_input_axis_tvalid  : STD_LOGIC;
    signal dsp_signal_input_axis_tlast   : STD_LOGIC;
    signal dsp_signal_input_axis_tdata   : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    
	signal dsp_result_axis_tready   : STD_LOGIC;
    signal dsp_result_axis_tvalid   : STD_LOGIC;
    signal dsp_result_axis_tdata    : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal dsp_result_axis_tlast    : STD_LOGIC;

	-- FIR FILTER + CORRELTION
	COMPONENT fir_correlation is
	generic (
	    REGISTER_W48        : INTEGER    := 48;
        REGISTER_W32        : INTEGER    := 32;
        REGISTER_W16        : INTEGER    := 16;
        TAP_BRAM_ADDR_WIDTH : INTEGER    := 12;
        N_TAPS              : INTEGER    := 512;
        N_SETS_TAP          : INTEGER    := 3;
        OVERSAMPLING        : INTEGER    := 2;
        RECTIFICATION_DISABLED	: INTEGER := 0;
        RECTIFICATION_MED   : INTEGER := 1;
        RECTIFICATION_FULL  : INTEGER := 2;
        WAIT_CYCLES         : INTEGER    := 4
    );
    port (
        aclk         : in std_logic;
        aresetn      : in std_logic;
        
		NO_READ_FIR_RESULT          : in STD_LOGIC;
        READ_FIR_RESULT             : in STD_LOGIC;
        READ_DEC_RESULT             : in STD_LOGIC;
        READ_FIR_RESULT_TYPE        : in STD_LOGIC;
        DATA_WINDOW                 : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        SAMPLE_FREQ                 : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        DATA_WINDOW_RESULT          : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        SAMPLE_FREQ_RESULT          : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		DATA_WINDOW_HALF_RESULT     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		FIR_END                     : out STD_LOGIC;
        
		-- FIR config
        BAND_PASS_FILTER_EN         : in STD_LOGIC;
        BAND_PASS_FILTER_L_DIVIDER  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GAUSSIAN_FILTER_EN          : in STD_LOGIC;
        GAUSSIAN_FILTER_L_DIVIDER   : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GAUSSIAN_FILTER_DELAY       : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        ENVELOPE_FILTER_EN          : in STD_LOGIC;
        ENVELOPE_FILTER_L_DIVIDER   : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        FIR_DELAY                   : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        FIR_DELAY_ON                : out STD_LOGIC;
        FIR_DELAY_STATE             : out STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
        FIR_DELAY_RESULT            : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        DSP_RECTIFICATION  	        : in STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
        
        FIR_TAPS_AXIS_TREADY        : out STD_LOGIC;
        FIR_TAPS_AXIS_TDATA         : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        FIR_TAPS_AXIS_TLAST         : in STD_LOGIC;
        FIR_TAPS_AXIS_TVALID        : in STD_LOGIC;
     
		-- Signal Input
        SIGNAL_INPUT_AXIS_TREADY    : out STD_LOGIC;
        SIGNAL_INPUT_AXIS_TDATA     : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        SIGNAL_INPUT_AXIS_TLAST     : in STD_LOGIC;
        SIGNAL_INPUT_AXIS_TVALID    : in STD_LOGIC;
        
		-- Gates Config
        GATES_EN                    : in STD_LOGIC;
        GATES_SAMPLE_START          : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATES_SAMPLE_END            : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        
		-- Gates Result
        GATES_AXIS_TVALID           : out STD_LOGIC;
        GATES_AXIS_TDATA            : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATES_AXIS_TLAST            : out STD_LOGIC;
        GATES_AXIS_TREADY           : in STD_LOGIC;
        
		-- Correlation Config
        CORRELATION_EN                  : in STD_LOGIC;
        CORRELATION_GATE_SAMPLE_START   : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        CORRELATION_GATE_SAMPLE_END     : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        
		-- Correlation Result
        CORRELATION_AXIS_TVALID     : out STD_LOGIC;
        CORRELATION_AXIS_TDATA      : out STD_LOGIC_VECTOR(REGISTER_W48-1 downto 0);
        CORRELATION_AXIS_TLAST      : out STD_LOGIC;
        CORRELATION_AXIS_TREADY     : in STD_LOGIC;
        
		-- FIR Result for decimation
        DEC_AXIS_TVALID      : out STD_LOGIC;
        DEC_AXIS_TDATA       : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        DEC_AXIS_TMAX        : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        DEC_AXIS_TMIN        : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        DEC_AXIS_TACC        : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        DEC_AXIS_TLAST       : out STD_LOGIC;
        DEC_AXIS_TREADY      : in STD_LOGIC;

		-- FIR Result
        FIR_RESULT_AXIS_TVALID      : out STD_LOGIC;
        FIR_RESULT_AXIS_TDATA       : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        FIR_RESULT_AXIS_TLAST       : out STD_LOGIC;
        FIR_RESULT_AXIS_TREADY      : in STD_LOGIC
    );
	end COMPONENT fir_correlation;

	-- DATA window
	signal fir_data_window_result	 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal fir_data_window_half_result	 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal fir_sample_freq_result	 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

	-- FIR end
	signal fir_end  : STD_LOGIC;

	-- FIR input tready
	signal fir_taps_axis_tready	: STD_LOGIC;
	
	-- Gates Result
	signal gates_axis_tdata	 	: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal gates_axis_tlast  	: STD_LOGIC;
	signal gates_axis_tvalid 	: STD_LOGIC;

	-- Correlation Result
	signal correlation_axis_tdata	: STD_LOGIC_VECTOR(REGISTER_W48-1 downto 0);
	signal correlation_axis_tlast  	: STD_LOGIC;
	signal correlation_axis_tvalid 	: STD_LOGIC;
	
	-- FIR Result for decimation
	signal dec_axis_tdata	: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal dec_axis_tmax	: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal dec_axis_tmin	: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal dec_axis_tacc	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal dec_axis_tlast      : STD_LOGIC;
    signal dec_axis_tvalid     : STD_LOGIC;
    signal dec_axis_tready     : STD_LOGIC;
   
	-- FIR Result
	signal fir_result_axis_tdata	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal fir_result_axis_tlast  	: STD_LOGIC;
	signal fir_result_axis_tvalid 	: STD_LOGIC;

	-- GATES PROCESSOR

	COMPONENT gates_processor is
	generic (
		REGISTER_W64  : INTEGER    := 64;
		REGISTER_W48  : INTEGER    := 48;
        REGISTER_W32  : INTEGER    := 32;
        REGISTER_W24  : INTEGER    := 24;
        REGISTER_W16  : INTEGER    := 16;
        REGISTER_W8   : INTEGER    := 8;
        REGISTER_W4   : INTEGER    := 4;
        REGISTER_W2   : INTEGER    := 2
	);
	port (
	    --sync
	    aclk           : in STD_LOGIC;
        aresetn        : in STD_LOGIC;  
	    
	    -- GENERAL    
	    RX_DELAY       : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
	    TX_DELAY       : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);   

        -- GATE1
		GATE1_EN                          : in STD_LOGIC;  
		GATE1_START                       : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		GATE1_WIDTH                       : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);	
        GATE1_AMP_THRESHOLD               : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE1_AMP_THRESHOLD_ALARM_EN      : in STD_LOGIC; 
        GATE1_AMP_THRESHOLD_ALARM_CROSSING: in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE1_TOF_ALGORITHM               : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE1_TOF_AVG                     : in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
        GATE1_TOF_MIN_THICKNESS           : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE1_TOF_ALARM_EN                : in STD_LOGIC; 
        GATE1_TOF_ALARM_TYPE              : in STD_LOGIC;
        GATE1_TOF_ALARM_CROSSING          : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE1_TOF_ALARM_MIN_VALUE         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE1_TOF_ALARM_MAX_VALUE         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE1_TOF                         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE1_TOF_AVERAGED                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE1_AMP_ALARM                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        GATE1_TOF_ALARM                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        GATE1_MAX                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE1_MAX_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE1_MIN                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE1_MIN_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        
        -- GATE2
        GATE2_EN                          : in STD_LOGIC;  
        GATE2_START                       : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE2_WIDTH                       : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);    
        GATE2_AMP_THRESHOLD               : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE2_AMP_THRESHOLD_ALARM_EN      : in STD_LOGIC; 
        GATE2_AMP_THRESHOLD_ALARM_CROSSING: in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE2_TOF_ALGORITHM               : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE2_TOF_AVG                     : in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
        GATE2_TOF_MIN_THICKNESS           : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE2_TOF_ALARM_EN                : in STD_LOGIC; 
        GATE2_TOF_ALARM_TYPE              : in STD_LOGIC;
        GATE2_TOF_ALARM_CROSSING          : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE2_TOF_ALARM_MIN_VALUE         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE2_TOF_ALARM_MAX_VALUE         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE2_TOF                         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE2_TOF_AVERAGED                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE2_AMP_ALARM                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE2_TOF_ALARM                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE2_MAX                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE2_MAX_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE2_MIN                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE2_MIN_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE2_THR                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE2_THR_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE2_FFT_EN                      : in STD_LOGIC;
        GATE2_CC_EN                       : in STD_LOGIC;
        GATE2_FFT_SEGMENT                 : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE2_FFT_NFFT                    : in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
        GATE2_FFT_WIDTH                   : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE2_FFT_MAX_VALUE               : out STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
        GATE2_FFT_MAX_SAMPLE              : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE2_CC_MAX_VALUE                : out STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
        GATE2_CC_MAX_SAMPLE               : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE2_FFT_CUADRATIC_ERROR         : out STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
        GATE2_CC_CONFIG_ON_ACK            : out STD_LOGIC;
        GATE2_CC_CONFIG_ON                : in STD_LOGIC;        
        GATE2_CC_CONFIG_ADDR              : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE2_CC_CONFIG_DATA              : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);  
        
        -- GATE3
        GATE3_EN                          : in STD_LOGIC;  
        GATE3_START                       : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE3_WIDTH                       : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);    
        GATE3_AMP_THRESHOLD               : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE3_AMP_THRESHOLD_ALARM_EN      : in STD_LOGIC; 
        GATE3_AMP_THRESHOLD_ALARM_CROSSING: in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE3_TOF_ALGORITHM               : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE3_TOF_AVG                     : in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
        GATE3_TOF_MIN_THICKNESS           : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE3_TOF_ALARM_EN                : in STD_LOGIC; 
        GATE3_TOF_ALARM_TYPE              : in STD_LOGIC;
        GATE3_TOF_ALARM_CROSSING          : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE3_TOF_ALARM_MIN_VALUE         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE3_TOF_ALARM_MAX_VALUE         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE3_TOF                         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE3_TOF_AVERAGED                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE3_AMP_ALARM                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        GATE3_TOF_ALARM                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE3_MAX                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE3_MAX_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE3_MIN                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE3_MIN_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);

		--GATES DATA
		GATES_CONFIG : in STD_LOGIC;
		GATES_SAMPLE_START: out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATES_SAMPLE_END  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATES_EN          : out STD_LOGIC;
        GATES_END         : out STD_LOGIC;
        READ_GATES_RESULT : in STD_LOGIC;
        
		GATES_AXIS_TREADY : out STD_LOGIC;
		GATES_AXIS_TDATA  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		GATES_AXIS_TLAST  : in STD_LOGIC;
		GATES_AXIS_TVALID : in STD_LOGIC;
		
		-- CORRELATION
        CORRELATION_EN              	: out STD_LOGIC;
        CORRELATION_GATE_SAMPLE_START   : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        CORRELATION_GATE_SAMPLE_END     : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        CORRELATION_AXIS_TREADY     	: out STD_LOGIC;
        CORRELATION_AXIS_TDATA      	: in STD_LOGIC_VECTOR(REGISTER_W48-1 downto 0);
        CORRELATION_AXIS_TLAST      	: in STD_LOGIC;
        CORRELATION_AXIS_TVALID     	: in STD_LOGIC
	);
	end COMPONENT gates_processor;

	-- GATE1 result
	signal gate1_tof			: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal gate1_tof_averaged   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal gate1_amp_alarm      : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
	signal gate1_tof_alarm      : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
	signal gate1_max            : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal gate1_max_sample     : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal gate1_min            : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal gate1_min_sample     : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);

	-- GATE2 result
	signal gate2_tof			: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal gate2_tof_averaged   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal gate2_amp_alarm      : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    signal gate2_tof_alarm      : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    signal gate2_max            : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal gate2_max_sample     : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal gate2_min            : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal gate2_min_sample     : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal gate2_thr            : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal gate2_thr_sample     : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal gate2_fft_en         : STD_LOGIC;
    signal gate2_cc_en          : STD_LOGIC;
    signal gate2_fft_segment    : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal gate2_fft_nfft       : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
    signal gate2_fft_max_value  : STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
    signal gate2_fft_max_sample : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal gate2_cc_max_value   : STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
    signal gate2_cc_max_sample  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal gate2_fft_cuadratic_error   : STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
 
	-- GATE3 result
	signal gate3_tof			: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal gate3_tof_averaged   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal gate3_amp_alarm      : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    signal gate3_tof_alarm      : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    signal gate3_max            : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal gate3_max_sample     : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal gate3_min            : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal gate3_min_sample     : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);

	-- GATE config for FIR
	signal gates_sample_start	: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal gates_sample_end  	: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal gates_en          	: STD_LOGIC;
	signal gates_end         	: STD_LOGIC;

	signal gates_axis_tready    : STD_LOGIC;

	-- GATE config for CORR
	signal correlation_en              		: STD_LOGIC;
	signal correlation_gate_sample_start   	: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal correlation_gate_sample_end     	: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);

	signal correlation_axis_tready     		: STD_LOGIC;
	
	signal correlation_counter : UNSIGNED(REGISTER_W32-1 downto 0);
	
	-- GATES ACCUMULATOR
	
    COMPONENT gates_accumulator is
        generic (
            N_CHANNELS          : INTEGER := 4;
            REGISTER_W32      : INTEGER := 32;
            REGISTER_W16      : INTEGER := 16;
            REGISTER_W14      : INTEGER := 14;
            REGISTER_W8       : INTEGER := 8;
            BRAM_ADDR_WIDTH   : INTEGER := 11;
            BRAM_DATA_WIDTH   : INTEGER := 32;
            WAIT_CYCLES       : INTEGER := 32;
            ASCAN_HEADER_SIZE : INTEGER := 96;
            COMPRESSION_NO    : INTEGER := 0;
            COMPRESSION_ASCAN : INTEGER := 1;
            COMPRESSION_GATES : INTEGER := 2;
            COMPRESSION_ASCAN_GATES : INTEGER := 3;
            COMPRESSION_BURST : INTEGER := 4;
            COMPRESSION_ASCAN_DEC : INTEGER := 5
        );
        port (
            -- Sync
            aclk         : in STD_LOGIC;
            aresetn      : in STD_LOGIC;
        
            -- Control
            GA_RESET           : in STD_LOGIC;        
            NO_READ_GA_RESULT  : in STD_LOGIC;
            READ_GA_RESULT     : in STD_LOGIC;
            DC_MODE            : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
            
            GA_WINDOW_RESULT   : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
            GA_N_GATES         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
                     
            -- GATES input
            GATES_INPUT_AXIS_TREADY  : out STD_LOGIC;
            GATES_INPUT_AXIS_TDATA     : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            GATES_INPUT_AXIS_TLAST     : in STD_LOGIC;
            GATES_INPUT_AXIS_TVALID  : in STD_LOGIC;
            
            -- Decimation result
            GA_RESULT_AXIS_TVALID    : out STD_LOGIC;
            GA_RESULT_AXIS_TDATA     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            GA_RESULT_AXIS_TLAST     : out STD_LOGIC;
            GA_RESULT_AXIS_TREADY    : in STD_LOGIC
        );
    end COMPONENT gates_accumulator;
    
    signal ga_window_result : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal ga_n_gates : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal ga_n_ascan_counter : UNSIGNED(REGISTER_W16-1 downto 0);
    signal ga_ascan_dec_counter : UNSIGNED(REGISTER_W16-1 downto 0);
    signal ga_reset : STD_LOGIC;
    
    signal compression_reset : STD_LOGIC;
    signal compression_reset_i : STD_LOGIC;
    signal no_read_ga_result : STD_LOGIC;
    signal read_ga_result : STD_LOGIC;
    
    signal gates_input_axis_tready  : STD_LOGIC;
    signal gates_input_axis_tlast   : STD_LOGIC;
    signal gates_input_axis_tvalid  : STD_LOGIC;
    signal gates_input_axis_tdata   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    
    signal ga_result_axis_tready  : STD_LOGIC;
    signal ga_result_axis_tlast   : STD_LOGIC;
    signal ga_result_axis_tvalid  : STD_LOGIC;
    signal ga_result_axis_tdata   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

	-- CONFIGURATION

	COMPONENT channel_configuration is
	generic (
        CHANNEL_ID      : INTEGER := 0;
        RECEIVER_SIZE   : INTEGER := 16;
        TRANSMITTER_SIZE: INTEGER := 16;
        MAGNET_SIZE     : INTEGER := 16;
        GATES_SIZE      : INTEGER := 72;
        DSP_SIZE        : INTEGER := 24;
        BRAM_ADDR_WIDTH : INTEGER := 12;
        BRAM_DATA_WIDTH : INTEGER := 32;
        REGISTER_W32    : INTEGER := 32;
        REGISTER_W16    : INTEGER := 16;
        REGISTER_W8     : INTEGER := 8;
        REGISTER_W4     : INTEGER := 4;
        REGISTER_W2     : INTEGER := 2
    );
	port (
	    -- Sync
	    aclk         : in STD_LOGIC;
        aresetn      : in STD_LOGIC;
		
		-- Control
		CONFIG_UPDATE_RECEIVER  	: in STD_LOGIC;
		CONFIG_NEW_RECEIVER   		: out STD_LOGIC;
		
		CONFIG_UPDATE_TRANSMITTER  		: in STD_LOGIC;
		CONFIG_NEW_TRANSMITTER  		: out STD_LOGIC;
		
		CONFIG_UPDATE_MAGNET 	 	: in STD_LOGIC;
		CONFIG_NEW_MAGNET  			: out STD_LOGIC;
		
		CONFIG_UPDATE_DSP_1  	: in STD_LOGIC;
		CONFIG_NEW_DSP_1  		: out STD_LOGIC;
		
		CONFIG_UPDATE_DSP_2  	: in STD_LOGIC;
		CONFIG_NEW_DSP_2  		: out STD_LOGIC;
		
		CONFIG_UPDATE_FIR  	 	: in STD_LOGIC;
		CONFIG_NEW_FIR  		: out STD_LOGIC;
		
		CONFIG_UPDATE_GATES  	: in STD_LOGIC;
		CONFIG_NEW_GATES  		: out STD_LOGIC;
		
		CONFIG_UPDATE_DAC  	 	: in STD_LOGIC;
		CONFIG_NEW_DAC  		: out STD_LOGIC;
		
		THERE_IS_CONFIGURATION   : out STD_LOGIC;
		
		-- Channel enable
		CHANNEL_EN   : out STD_LOGIC;
		
        -- Channel HW
        CHANNEL_HW     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			
		-- Config input
		CONFIG_INPUT_LOAD	  : in STD_LOGIC;
		CONFIG_INPUT_AXIS_TREADY  : out STD_LOGIC;
		CONFIG_INPUT_AXIS_TDATA	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		CONFIG_INPUT_AXIS_TLAST	  : in STD_LOGIC;
		CONFIG_INPUT_AXIS_TVALID  : in STD_LOGIC;
		
		-- Config output receiver
		RECEIVER_SAMPLING_FREQUENCY         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        RECEIVER_DATA_WINDOW                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        RECEIVER_DELAY                      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        RECEIVER_ANALOG_GAIN                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  --db*1024
        RECEIVER_DIGITAL_GAIN               : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  --V*1024*1024  
        RECEIVER_EXTERNAL_MULTIPLEXER_EN    : out STD_LOGIC;
		
		-- Config output transmitter
		TRANSMITTER_VOLTAGE                 : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        TRANSMITTER_BURST_FREQUENCY         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        TRANSMITTER_N_CYCLES                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_N_BURST                 : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        TRANSMITTER_PRF                     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_DELAY                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_DELTA_DELAY             : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_DELTA_ADD               : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_DIRECTIONAL_PHASING     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        TRANSMITTER_PHASE_SHIFT             : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_START_CYCLE             : out STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
		
		-- Config output magnet
		MAGNET_MODE                         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        MAGNET_PULSE_WIDTH                  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        MAGNET_INITIAL_DELAY                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        MAGNET_RAMP_UP_VOLTAGE              : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        MAGNET_VOLTAGE                      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		-- Config output DSP
		DSP_AVERAGE                         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
		DSP_AVERAGE_TYPE                    : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        DSP_NOISE_REDUCTION_FILTER          : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        DSP_PHASE_SHIFT                     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        DSP_BANDPASS_EN  					: out STD_LOGIC;
		DSP_BANDPASS_N_TAPS  				: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
		DSP_BANDPASS_L  					: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        DSP_GAUSSIAN_EN  					: out STD_LOGIC;
		DSP_GAUSSIAN_N_TAPS  				: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
		DSP_GAUSSIAN_L  					: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
		DSP_ENVELOPE_EN  					: out STD_LOGIC;
		DSP_ENVELOPE_N_TAPS  				: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
		DSP_ENVELOPE_L  					: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		DSP_ANALOG_FILTER  					: out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
	    DSP_DEC_TYPE	                    : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);    
        DSP_DEC_LEVEL                       : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        DSP_DEC_WINDOW                      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        DSP_GAUSSIAN_DELAY  				: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        DSP_INVERSION  					    : out STD_LOGIC;
        DSP_RECTIFICATION                   : out STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
        DSP_FIR_DELAY  				        : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        DSP_COMPRESSION_MODE                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        DSP_COMPRESSION_LEVEL               : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        DSP_AVG_COINC_ORDER                 : out STD_LOGIC; 
				
		-- Config output Gates
		GATE1_EN                          : out STD_LOGIC;  
		GATE1_START                       : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		GATE1_WIDTH                       : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);	
        GATE1_AMP_THRESHOLD               : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		GATE1_TOF_ALGORITHM               : out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
		GATE1_TOF_AVG                     : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
		GATE1_TOF_MIN_THICKNESS           : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE1_AMP_THRESHOLD_ALARM_EN      : out STD_LOGIC; 
        GATE1_AMP_THRESHOLD_ALARM_CROSSING: out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE1_TOF_ALARM_EN                : out STD_LOGIC; 
        GATE1_TOF_ALARM_TYPE              : out STD_LOGIC;
        GATE1_TOF_ALARM_CROSSING          : out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE1_TOF_ALARM_MIN_VALUE         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE1_TOF_ALARM_MAX_VALUE         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);       
				
		GATE2_EN                          : out STD_LOGIC;  
		GATE2_START                       : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		GATE2_WIDTH                       : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);	
        GATE2_AMP_THRESHOLD               : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		GATE2_TOF_ALGORITHM               : out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
		GATE2_TOF_AVG                     : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
		GATE2_TOF_MIN_THICKNESS           : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE2_AMP_THRESHOLD_ALARM_EN      : out STD_LOGIC; 
        GATE2_AMP_THRESHOLD_ALARM_CROSSING: out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE2_TOF_ALARM_EN                : out STD_LOGIC; 
        GATE2_TOF_ALARM_TYPE              : out STD_LOGIC;
        GATE2_TOF_ALARM_CROSSING          : out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE2_TOF_ALARM_MIN_VALUE         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE2_TOF_ALARM_MAX_VALUE         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE2_FFT_EN                      : out STD_LOGIC;
        GATE2_CC_EN                       : out STD_LOGIC;
        GATE2_FFT_SEGMENT                 : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE2_FFT_NFFT                    : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
		
		GATE3_EN                          : out STD_LOGIC;  
		GATE3_START                       : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		GATE3_WIDTH                       : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);	
        GATE3_AMP_THRESHOLD               : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		GATE3_TOF_ALGORITHM               : out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
		GATE3_TOF_AVG                     : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
		GATE3_TOF_MIN_THICKNESS           : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE3_AMP_THRESHOLD_ALARM_EN      : out STD_LOGIC; 
        GATE3_AMP_THRESHOLD_ALARM_CROSSING: out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE3_TOF_ALARM_EN                : out STD_LOGIC; 
        GATE3_TOF_ALARM_TYPE              : out STD_LOGIC;
        GATE3_TOF_ALARM_CROSSING          : out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE3_TOF_ALARM_MIN_VALUE         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE3_TOF_ALARM_MAX_VALUE         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		-- Config output FIR
		CONFIG_FIR_AXIS_TREADY    : in STD_LOGIC;
		CONFIG_FIR_AXIS_TDATA	  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		CONFIG_FIR_AXIS_TLAST	  : out STD_LOGIC;
		CONFIG_FIR_AXIS_TVALID    : out STD_LOGIC;
		
		-- DAC output
		CONFIG_DAC_AXIS_TREADY    : in STD_LOGIC;
		CONFIG_DAC_AXIS_TDATA	  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		CONFIG_DAC_AXIS_TLAST	  : out STD_LOGIC;
		CONFIG_DAC_AXIS_TVALID    : out STD_LOGIC
	);
	end COMPONENT channel_configuration;

	-- Control
	signal config_new_receiver   	: STD_LOGIC;
	signal config_new_transmitter  	: STD_LOGIC;
	signal config_new_magnet  		: STD_LOGIC;
	signal config_new_dsp_1  		: STD_LOGIC;
	signal config_new_dsp_2  		: STD_LOGIC;
	signal config_new_fir  			: STD_LOGIC;
	signal config_new_gates  		: STD_LOGIC;
	signal config_new_dac  			: STD_LOGIC;

	signal there_is_configuration  	: STD_LOGIC;

	-- Config output receiver		
	signal receiver_delay_i                    : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal receiver_delay_i_d1                 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal receiver_delay_gate                 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal receiver_data_window_i              : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
	signal receiver_sampling_frequency_i       : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal receiver_digital_gain_i             : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
	
	signal receiver_delay_gate_flag            : STD_LOGIC;
	signal receiver_delay_gate_dividend        : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal receiver_delay_gate_divisor         : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal receiver_delay_gate_div             : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

	-- Config output transmitter  
	signal transmitter_prf                     : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal transmitter_prf_mrut                : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal transmitter_prf_enc                 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal transmitter_delay_mrut              : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal transmitter_delay_i                 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal transmitter_delta_delay_i           : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal transmitter_delay_gate              : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal transmitter_delay_gate_i            : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal transmitter_start_cycle_mrut        : STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
	signal transmitter_start_cycle_i           : STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
		
	signal magnet_mode_i                       : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal magnet_initial_delay_i              : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal magnet_pulse_width_i                : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);	
	
	signal signal_coinc_counter_i              : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	
	signal transmitter_burst_frequency_i       : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	-- 20 Khz
	constant transmitter_burst_frequency_min   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= std_logic_vector(to_unsigned(25000000, transmitter_burst_frequency_i'length));  
	-- 150 Khz
	constant transmitter_burst_frequency_lrut  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= std_logic_vector(to_unsigned(3333333, transmitter_burst_frequency_i'length));  
	-- 7Mhz
	constant transmitter_burst_frequency_max   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= std_logic_vector(to_unsigned(71428, transmitter_burst_frequency_i'length)); 																													  
	signal transmitter_burst_lrut_on  		   : STD_LOGIC;
	signal transmitter_burst_lrut_on_d1  	   : STD_LOGIC;
	signal lrut_wait_flag  		               : STD_LOGIC;
	signal lrut_wait_end 		               : STD_LOGIC;
	signal lrut_on_counter                     : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal lrut_on_counter_pic                 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			
	-- Config output DSP
	signal dsp_average             		: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
	signal dsp_noise_reduction_filter   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
	signal dsp_bandpass_en  			: STD_LOGIC;
	signal dsp_bandpass_n_taps  		: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
	signal dsp_bandpass_l  				: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
	signal dsp_gaussian_en  			: STD_LOGIC;
	signal dsp_gaussian_n_taps  		: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
	signal dsp_gaussian_l  				: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
	signal dsp_envelope_en  			: STD_LOGIC;
	signal dsp_envelope_n_taps  		: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
	signal dsp_envelope_l  				: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
	signal dsp_dec_type  				: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
	signal dsp_dec_level  				: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
	signal dsp_dec_window  				: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
	signal dsp_gaussian_delay  		    : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal dsp_inversion  			    : STD_LOGIC;
	signal dsp_rectification  			: STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
	signal dsp_fir_delay     			: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal dsp_fir_delay_on  			: STD_LOGIC;
	signal dsp_fir_delay_result         : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal dsp_fir_delay_state          : STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
	signal dsp_compression_mode    		: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal dsp_compression_level        : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal dsp_compression_mode_i       : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal dsp_compression_level_i      : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal dsp_avg_coinc_order  		: STD_LOGIC;
	
    signal dsp_dec_type_i  				: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    signal dsp_dec_level_i              : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    signal dsp_dec_window_i             : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
				
	-- Config output Gates
	signal gate1_en                          : STD_LOGIC;  
	signal gate1_start                       : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal gate1_width                       : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);	
	signal gate1_amp_threshold               : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal gate1_tof_algorithm               : STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
	signal gate1_tof_avg                     : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
	signal gate1_tof_min_thickness           : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal gate1_amp_threshold_alarm_en      : STD_LOGIC; 
	signal gate1_amp_threshold_alarm_crossing: STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
	signal gate1_tof_alarm_en                : STD_LOGIC; 
	signal gate1_tof_alarm_type              : STD_LOGIC;
	signal gate1_tof_alarm_crossing          : STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
	signal gate1_tof_alarm_min_value         : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal gate1_tof_alarm_max_value         : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);	
		
	signal gate2_en                          : STD_LOGIC;  
    signal gate2_start                       : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal gate2_width                       : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);    
    signal gate2_amp_threshold               : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal gate2_tof_algorithm               : STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
    signal gate2_tof_avg                     : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
    signal gate2_tof_min_thickness           : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal gate2_amp_threshold_alarm_en      : STD_LOGIC; 
    signal gate2_amp_threshold_alarm_crossing: STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
    signal gate2_tof_alarm_en                : STD_LOGIC; 
    signal gate2_tof_alarm_type              : STD_LOGIC;
    signal gate2_tof_alarm_crossing          : STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
    signal gate2_tof_alarm_min_value         : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal gate2_tof_alarm_max_value         : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

	signal gate3_en                          : STD_LOGIC;  
	signal gate3_start                       : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal gate3_width                       : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);	
	signal gate3_amp_threshold               : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal gate3_tof_algorithm               : STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
	signal gate3_tof_avg                     : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
	signal gate3_tof_min_thickness           : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal gate3_amp_threshold_alarm_en      : STD_LOGIC; 
	signal gate3_amp_threshold_alarm_crossing: STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
	signal gate3_tof_alarm_en                : STD_LOGIC; 
	signal gate3_tof_alarm_type              : STD_LOGIC;
	signal gate3_tof_alarm_crossing          : STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
	signal gate3_tof_alarm_min_value         : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal gate3_tof_alarm_max_value         : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

	signal config_fir_axis_tdata	 : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal config_fir_axis_tlast	 : STD_LOGIC;
	signal config_fir_axis_tvalid    : STD_LOGIC;

	-- CHANNEL
	signal config_update_receiver  		: STD_LOGIC;
	signal config_update_transmitter	: STD_LOGIC;
	signal config_update_magnet 	 	: STD_LOGIC;
	signal config_update_dsp_1  		: STD_LOGIC;
	signal config_update_dsp_2  		: STD_LOGIC;
	signal config_update_fir  	 		: STD_LOGIC;
	signal config_update_gates  		: STD_LOGIC;
	signal config_update_dac  			: STD_LOGIC;

	-- State machines

	-- State machine A
	type states_general_a_sm is (idle, wait_state, prf_control, trigger_external, trigger_external_clear, wait_stop_motor,
	                             sw_trigger, trigger_encoder, trigger_start, prf_control_mrut, coincidence_average_window, 
								 update_receiver, update_transmitter, update_magnet, update_dsp_1, update_dac, update_control,
								 wait_clear_avg_acc, sync_channels, encoder_alignment, lrut_wait, prf_control_enc); 

    signal state_general_a_sm : states_general_a_sm;

	-- State machine B
	type states_general_b_sm is (idle, wait_input_fir_end, wait_fir_end, wait_fir_delay_end, wait_gates_end, 
	                             wait_decimation_end, data_disposition, data_disposition_flag, store_gates, 
	                             store_gates_check, store_result_check_wait, read_result, read_result_end, 
	                             update_dsp_2, update_FIR, update_gates); 

    signal state_general_b_sm : states_general_b_sm;

	-- Read ASCAN state machine
	type states_read_ascan_sm is (idle, no_read_result, read_ascan_start, upload_general_header, upload_ascan_header, upload_ascan_data_flank_down, 
	                              upload_ascan_data_flank_up, upload_ascan_end, acc_general_header, acc_ascan_header); 

    signal state_read_ascan_sm : states_read_ascan_sm;

	signal fir_result_axis_tready_i	: STD_LOGIC;
	signal fir_result_axis_tready_x	: STD_LOGIC;
	signal general_signal_input_axis_tready	: STD_LOGIC;
	signal coincidence_average_window_exit	: STD_LOGIC;

	signal read_fir_result		: STD_LOGIC;
	signal read_fir_dec_result  : STD_LOGIC;
	signal no_read_fir_result	: STD_LOGIC;
	signal read_fir_result_type	: STD_LOGIC;
	signal read_gates_result    : STD_LOGIC;
	signal channel_en   		: STD_LOGIC;
	signal ascan_ready_i  	    : STD_LOGIC;

	signal prf_on     	: STD_LOGIC;
	signal prf_counter	: UNSIGNED(REGISTER_W32-1 downto 0);
	signal prf_counter_i: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

	signal disposition_on_i   : STD_LOGIC;
	signal store_gates_flag  : STD_LOGIC;
    signal store_gates_end   : STD_LOGIC;

	signal read_ascan_flag   : STD_LOGIC;
	signal read_ascan_end   : STD_LOGIC;
	
	signal tready_flank_up_flag : STD_LOGIC;
	signal ascan_axis_tdata_1: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal ascan_axis_tdata_2: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal ascan_axis_tready_i1: STD_LOGIC;
	
	signal ascan_counter: UNSIGNED(REGISTER_W32-1 downto 0);

	signal fir_signal_input_axis_tready  	: STD_LOGIC;
	signal fir_signal_input_axis_tready_i  	: STD_LOGIC;
	signal fir_signal_input_axis_tready_x  	: STD_LOGIC;
	
	signal fir_signal_input_axis_tvalid  : STD_LOGIC;
    signal fir_signal_input_axis_tlast   : STD_LOGIC;
    signal fir_signal_input_axis_tdata   : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	
	signal external_trigger  : STD_LOGIC;
	signal encoder1_pos_i  	 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal encoder1_trigger_counts_i : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal encoder1_trigger_diff : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal encoder1_pos_result  	 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal encoder1_pos_result_i  	 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal encoder1_dir_result  	 : STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
    signal encoder1_dir_result_i     : STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
	signal encoder1_target_pos       : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal encoder1_target_neg       : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal encoder1_target_first     : STD_LOGIC;
	signal encoder_reset_top_d1      : STD_LOGIC;
	
	signal encoder_reset_counter     : UNSIGNED(REGISTER_W32-1 downto 0);
	signal encoder_reset_counter_i   : UNSIGNED(REGISTER_W32-1 downto 0);
	
	signal pwm_not_moving_counter   : UNSIGNED(REGISTER_W32-1 downto 0);
	
    signal encoder2_pos_result  	 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal encoder2_pos_result_i     : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal encoder2_dir_result  	 : STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
    signal encoder2_dir_result_i     : STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
     
    signal mrut_counter_i : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal mrut_counter_result : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
	signal scan_mode : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal trigger_type : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal trigger_sw : STD_LOGIC;
	signal start_inspection : STD_LOGIC;
	signal start_stop_inspection : STD_LOGIC;
	signal system_ready : STD_LOGIC;

	signal scan_mode_d1 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal trigger_type_d1 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal start_inspection_d1 : STD_LOGIC;
    signal start_stop_inspection_d1 : STD_LOGIC;
    
    signal idle_state_out_i : STD_LOGIC;
    
    -- PRF CALCULATOR
    
    COMPONENT prf_calculator is
        generic (
            REGISTER_W32            : INTEGER := 32;
            REGISTER_W16            : INTEGER := 16;
            AVG_LEVEL_MIN           : INTEGER := 2;
            AVG_LEVEL_MAX           : INTEGER := 32
        );
        port (
            -- Sync
            aclk         : in STD_LOGIC;
            aresetn      : in STD_LOGIC;
            
            -- Control
            START_INSPECTION          : in STD_LOGIC;
            ACQUISITION_PROVIDED      : in STD_LOGIC;
            TRANSMITTER_PRF           : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            AVG_LEVEL                 : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            
            -- Output
            PRF_RESULT                 : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0)
        );
    end COMPONENT prf_calculator;
    
    signal adquisiton_provided : STD_LOGIC;
    signal transmitter_prf_calculator : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal transmitter_prf_result : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal prf_calculator_avg : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    
    -- TIME STAMP
    
    signal time_stamp_counter : STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
    signal time_stamp_reset   : STD_LOGIC;
    
    -- DECIMATION FILTER
    
    COMPONENT decimator_filter is
    generic (
        REGISTER_W32        : INTEGER := 32;
        REGISTER_W16        : INTEGER := 16;
        REGISTER_W14        : INTEGER := 14;
        BRAM_ADDR_WIDTH     : INTEGER := 13;
        BRAM_DATA_WIDTH     : INTEGER := 16;
        WAIT_CYCLES         : INTEGER := 32;
        N_INPUT_DELAY       : INTEGER := 8;
        N_OUTPUT_DELAY      : INTEGER := 4;
        DEC_TYPE_NONE       : INTEGER := 0;
        DEC_TYPE_SIMPLE     : INTEGER := 1;
        DEC_TYPE_MINMAX     : INTEGER := 2;
        DEC_TYPE_ALOK       : INTEGER := 3
    );
    port (
        -- Sync
        aclk         : in STD_LOGIC;
        aresetn      : in STD_LOGIC;
    
        -- Control
        NO_READ_DEC_RESULT  : in STD_LOGIC;
        READ_DEC_RESULT     : in STD_LOGIC;
        DEC_END             : out STD_LOGIC;
        DEC_EN              : out STD_LOGIC;
        
        DEC_TYPE    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);    
        DEC_LEVEL   : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        DEC_WINDOW  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        DATA_WINDOW : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        
        DEC_TYPE_RESULT         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);    
        DEC_LEVEL_RESULT        : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        DEC_WINDOW_RESULT       : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        DEC_WINDOW_HALF_RESULT  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);    
            
        -- Signal input
        SIGNAL_INPUT_AXIS_TREADY  : out STD_LOGIC;
        SIGNAL_INPUT_AXIS_TDATA   : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        SIGNAL_INPUT_AXIS_TMAX	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        SIGNAL_INPUT_AXIS_TLAST   : in STD_LOGIC;
        SIGNAL_INPUT_AXIS_TVALID  : in STD_LOGIC;

        -- Decimation result
        DEC_RESULT_AXIS_TVALID    : out STD_LOGIC;
        DEC_RESULT_AXIS_TDATA     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        DEC_RESULT_AXIS_TLAST     : out STD_LOGIC;
        DEC_RESULT_AXIS_TREADY    : in STD_LOGIC
    );
    end COMPONENT decimator_filter; 
    
    signal no_read_dec_result  : STD_LOGIC;
    signal read_dec_result  : STD_LOGIC;
    signal dec_end          : STD_LOGIC;
    signal dec_en           : STD_LOGIC;
    
    signal dec_type_result      :  STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal dec_level_result     :  STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal dec_window_result    :  STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal dec_window_half_result  :  STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    
    signal dec_result_axis_tready  : STD_LOGIC;
    signal dec_result_axis_tready_x: STD_LOGIC;
    signal dec_result_axis_tdata   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal dec_result_axis_tlast   : STD_LOGIC;
    signal dec_result_axis_tvalid  : STD_LOGIC;
    
    signal data_window_half_result	 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    
    -- Delay CONTROL signals
    signal scan_mode_control_d1 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal start_inspection_control_d1  : STD_LOGIC;
    signal trigger_sw_control_d1  : STD_LOGIC;
    signal start_stop_inspection_control_d1  : STD_LOGIC;
    signal trigger_type_control_d1  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    
    signal configure_control_reset : STD_LOGIC;
    signal configure_control_reset_ack : STD_LOGIC;
    
    signal max_overspeed_alarm_i : STD_LOGIC;
    signal max_overspeed_alarm_x : STD_LOGIC;
    signal max_overspeed_alarm_positions : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal max_overspeed_alarm_y : STD_LOGIC;
    
    -- DSP result array
    type gates_array is array ( 0 to 2047 ) of std_logic_vector(31 downto 0);
    signal gates_acc : gates_array;
    signal gates_acc_counter : UNSIGNED(10 downto 0);
    signal gates_offset      : INTEGER range 0 to 2047;
      
begin

    CHANNEL_ENABLED <= channel_en;
    CONFIG_ON <= there_is_configuration;
	
--	SIGNAL_INPUT_AXIS_TREADY <= digital_amplifier_tready and general_signal_input_axis_tready;
	SIGNAL_INPUT_AXIS_TREADY <= general_signal_input_axis_tready;
						    	
	SCAN_MODE_STATUS <= scan_mode;
	TRIGGER_TYPE_STATUS <= trigger_type;
	START_INSPECTION_STATUS <= start_inspection;
	START_STOP_INSPECTION_STATUS <= start_stop_inspection;
	SYSTEM_READY_STATUS <= system_ready;
	TRIGGER_SW_STATUS <= trigger_sw;
		
	-- for gates
	compensate_delay_process: process(aclk)
        begin 
            if (rising_edge(aclk)) then
                if (aresetn = '0') then
                    receiver_delay_gate	<= (others => '0');	
                    transmitter_delay_gate <= (others => '0');
                    transmitter_delay_gate_i <= (others => '0');  
                    receiver_delay_i_d1 <= (others => '0');    
                    receiver_delay_gate_flag <= '0';
                    receiver_delay_gate_dividend <= (others => '0');  
                    receiver_delay_gate_divisor <= (others => '0');  
                    receiver_delay_gate_div <= (others => '0');  
                else
                    receiver_delay_i_d1 <= receiver_delay_i;
                    --receiver_delay_gate <= std_logic_vector(unsigned(receiver_delay_i) srl to_integer(unsigned(receiver_sampling_frequency_i)-1));
--                    if(receiver_delay_i_d1 /= receiver_delay_i) then
--                        receiver_delay_gate_dividend <= receiver_delay_i;
--                        receiver_delay_gate_divisor <= receiver_sampling_frequency_i;
--                        receiver_delay_gate_div <= (others => '0'); 
--                        receiver_delay_gate_flag <= '1';
--                    else
--                        if(receiver_delay_gate_flag = '1') then
--                            if(unsigned(receiver_sampling_frequency_i) = 1) then
--                                receiver_delay_gate <= receiver_delay_i;
--                                receiver_delay_gate_flag <= '0';
--                            elsif(unsigned(receiver_sampling_frequency_i) = 2) then
--                                receiver_delay_gate <= std_logic_vector(unsigned(receiver_delay_i) srl 1);
--                                receiver_delay_gate_flag <= '0';    
--                            elsif(unsigned(receiver_sampling_frequency_i) = 4) then
--                                receiver_delay_gate <= std_logic_vector(unsigned(receiver_delay_i) srl 2);
--                            else
--                                if(receiver_delay_gate_dividend >= receiver_delay_gate_divisor) then
--                                    receiver_delay_gate_dividend <= std_logic_vector(unsigned(receiver_delay_gate_dividend) - unsigned(receiver_delay_gate_divisor));
--                                    receiver_delay_gate_div <= std_logic_vector(unsigned(receiver_delay_gate_div)+1);
--                                else
--                                    receiver_delay_gate <= receiver_delay_gate_div;
--                                    receiver_delay_gate_flag <= '0';    
--                                end if;          
--                            end if; 
--                        end if;
--                    end if;
                    
                    if(unsigned(receiver_sampling_frequency_i) = 1) then
                        receiver_delay_gate <= receiver_delay_i;
                        receiver_delay_gate_flag <= '0';  
                    elsif(unsigned(receiver_sampling_frequency_i) = 2) then
                        receiver_delay_gate <= std_logic_vector(unsigned(receiver_delay_i) srl 1); 
                        receiver_delay_gate_flag <= '0';  
                    elsif(unsigned(receiver_sampling_frequency_i) = 4) then
                        receiver_delay_gate <= std_logic_vector(unsigned(receiver_delay_i) srl 2);
                        receiver_delay_gate_flag <= '0'; 
                    elsif(unsigned(receiver_sampling_frequency_i) = 8) then
                        receiver_delay_gate <= std_logic_vector(unsigned(receiver_delay_i) srl 3);
                        receiver_delay_gate_flag <= '0'; 
                    else
                        if(receiver_delay_gate_flag = '1') then
                            if(receiver_delay_gate_dividend >= receiver_delay_gate_divisor) then
                                receiver_delay_gate_dividend <= std_logic_vector(unsigned(receiver_delay_gate_dividend) - unsigned(receiver_delay_gate_divisor));
                                receiver_delay_gate_div <= std_logic_vector(unsigned(receiver_delay_gate_div)+1);
                            else
                                receiver_delay_gate <= receiver_delay_gate_div;
                                receiver_delay_gate_flag <= '0';    
                            end if;  
                        else
                            if(unsigned(receiver_delay_i) > unsigned(receiver_sampling_frequency_i)) then
                                receiver_delay_gate_dividend <= receiver_delay_i;
                                receiver_delay_gate_divisor <= receiver_sampling_frequency_i;
                                receiver_delay_gate_div <= (others => '0'); 
                                receiver_delay_gate_flag <= '1';
                            end if;
                        end if;   
                    end if;   
                     
                    if(unsigned(signal_coinc_counter_i) = 0) then
                        --transmitter_delay_gate_i <= std_logic_vector(unsigned(transmitter_delay_i)+unsigned(transmitter_delta_delay_i)); 
                        transmitter_delay_gate_i <= transmitter_delta_delay_i; 
                    else
                        transmitter_delay_gate_i <= (others => '0'); 
                    end if;
                    -- PENDING TO DISCUSS with ALI transmitter_delay_gate
                    --transmitter_delay_gate <= std_logic_vector(unsigned(transmitter_delay_gate_i) srl to_integer(unsigned(receiver_sampling_frequency_i)-1));
                end if;
            end if;
        end process;
					
    parameters_process: process(aclk)
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                TRANSMITTER_DELAY <= (others => '0');
				TRANSMITTER_BURST_FREQUENCY	<= (others => '0');	
				TRANSMITTER_START_CYCLE	<= (others => '0');																										  
                transmitter_prf_mrut <= (others => '0');
                transmitter_prf_enc <= (others => '0');
                encoder1_trigger_counts_i <= (others => '0');
                encoder1_trigger_diff <= (others => '0');
                avg_type <= (others => '0');
                coinc_type <= (others => '0');              					
				transmitter_prf_calculator <= (others => '0');				
				-- Delay CONTROL signals
				scan_mode_control_d1 <= (others => '0');	
				start_inspection_control_d1 <= '0';
				trigger_sw_control_d1 <= '0';
				start_stop_inspection_control_d1 <= '0';
				trigger_type_control_d1 <= (others => '0');		
				MAX_OVERSPEED_ALARM_1 <= '0';	
				MAX_OVERSPEED_ALARM_2 <= '0';													  
            else	
                --coinc_type  <=std_logic_vector(to_signed(COINC_TYPE_ACC, coinc_type'length));				
				if(unsigned(scan_mode) = SCAN_MODE_MRUT or unsigned(scan_mode) = SCAN_MODE_MRUT_DIR) then
                    -- Coincidence type                                                                                                      
                    coinc_type  <=std_logic_vector(to_signed(COINC_TYPE_ACC, coinc_type'length));    
                    avg_type  <= avg_type_i;             
                else
                    -- Coincidence type                                           
                    if(unsigned(trigger_type) = TRIGGER_TYPE_NORMAL or unsigned(trigger_type) = TRIGGER_TYPE_ROBOT or
                        unsigned(trigger_type) = TRIGGER_TYPE_ENC) then
--                    if(unsigned(trigger_type) = TRIGGER_TYPE_NORMAL or unsigned(trigger_type) = TRIGGER_TYPE_ROBOT) then
                        coinc_type  <=std_logic_vector(to_signed(COINC_TYPE_PIPELINE, coinc_type'length));
                        avg_type  <=std_logic_vector(to_signed(AVG_TYPE_PIPELINE, avg_type'length));      
                    else
                        if((unsigned(magnet_mode_i) /= 0) and (unsigned(magnet_pulse_width_i) > 0)) then
                            coinc_type  <=std_logic_vector(to_signed(COINC_TYPE_PIPELINE, coinc_type'length));  
                            avg_type  <=std_logic_vector(to_signed(AVG_TYPE_PIPELINE, avg_type'length));
                        else
                            coinc_type  <=std_logic_vector(to_signed(COINC_TYPE_ACC, coinc_type'length));
                            avg_type  <= avg_type_i; 
                        end if;  
                    end if;                
                end if;
				
				-- Transmitter 
                if(unsigned(scan_mode) = SCAN_MODE_MRUT) then
					-- Transmitter Delay																									  
                    TRANSMITTER_DELAY <= transmitter_delay_mrut;
					-- Transmitter Burst Freq																								  
					TRANSMITTER_BURST_FREQUENCY <= transmitter_burst_frequency_i;	
					-- Start cycle					
					TRANSMITTER_START_CYCLE <= transmitter_start_cycle_mrut;
					transmitter_prf_calculator <= transmitter_prf_mrut;
                else
					-- Transmitter Delay									   
                    TRANSMITTER_DELAY <= transmitter_delay_i;
					-- Transmitter Burst Freq									   
					TRANSMITTER_BURST_FREQUENCY <= transmitter_burst_frequency_i;	
					-- Start cycle					
                    TRANSMITTER_START_CYCLE <= transmitter_start_cycle_i;		
                    transmitter_prf_calculator <= transmitter_prf;		
                end if;                    
--                if(unsigned(TRIGGER_TYPE_CONTROL) = TRIGGER_TYPE_ENC) then
--                    transmitter_prf_mrut <= std_logic_vector(unsigned(transmitter_prf) srl 1); 
--                else
--                    transmitter_prf_mrut <= transmitter_prf;    
--                end if;    
                transmitter_prf_mrut <= std_logic_vector(unsigned(transmitter_prf) srl 1);     
                --transmitter_prf_enc  <= std_logic_vector(to_unsigned(1000000, transmitter_prf_enc'length));     
                transmitter_prf_enc  <= std_logic_vector(to_unsigned(250000, transmitter_prf_enc'length));        
                encoder1_trigger_counts_i <= ENCODER1_TRIGGER_COUNTS;                              
                if(signed(ENCODER1_POS) >= signed(encoder1_pos_i)) then
                    encoder1_trigger_diff <= std_logic_vector(signed(ENCODER1_POS) -  signed(encoder1_pos_i));
                else
                    encoder1_trigger_diff <= std_logic_vector(signed(encoder1_pos_i) -  signed(ENCODER1_POS));
                end if;
                -- Delay CONTROL signals
                scan_mode_control_d1 <= SCAN_MODE_CONTROL;    
                start_inspection_control_d1 <= START_INSPECTION_CONTROL;
                trigger_sw_control_d1 <= TRIGGER_SW_CONTROL;
                start_stop_inspection_control_d1 <= START_STOP_INSPECTION_CONTROL;     
                trigger_type_control_d1 <= TRIGGER_TYPE_CONTROL; 
                MAX_OVERSPEED_ALARM_1 <= max_overspeed_alarm_x;
                MAX_OVERSPEED_ALARM_2 <= max_overspeed_alarm_y;
                --MAX_OVERSPEED_ALARM <= max_overspeed_alarm_x or max_overspeed_alarm_y;      
            end if;
        end if;
    end process; 
    
    MAGNET_MODE          <= magnet_mode_i;
    MAGNET_PULSE_WIDTH   <= magnet_pulse_width_i;
    MAGNET_INITIAL_DELAY <= magnet_initial_delay_i;  
    
    LRUT_EN <= transmitter_burst_lrut_on;
    
    lrut_process: process(aclk)    
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                transmitter_burst_lrut_on <= '0';
                transmitter_burst_lrut_on_d1 <= '0';
                lrut_wait_flag <= '0';
                lrut_on_counter <= (others => '0');
            else
                transmitter_burst_lrut_on_d1 <= transmitter_burst_lrut_on;
                if(transmitter_burst_lrut_on_d1 /= transmitter_burst_lrut_on) then
                    lrut_wait_flag <= '1';
                    lrut_on_counter <= std_logic_vector(unsigned(lrut_on_counter)+1);
                else
                    if(lrut_wait_end = '1') then
                        lrut_wait_flag <= '0'; 
                    end if;
                end if;
            end if;
        end if;
    end process;
    
    max_overspeed_process: process(aclk)
        variable var_counter_1 : integer range 0 to 500000000 :=0;
        variable var_counter_2 : integer range 0 to 500000000 :=0;
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                max_overspeed_alarm_x <= '0';
                var_counter_1:=0;
                var_counter_2:=0;
                max_overspeed_alarm_positions <= (others => '0');
                max_overspeed_alarm_y <= '0';
            else
                if(var_counter_1 = 0) then --2 seconds
                    if(max_overspeed_alarm_i ='1') then
                        max_overspeed_alarm_x <= '1';
                        var_counter_1:=200000000;
                    else
                        max_overspeed_alarm_x <= '0';
                    end if;
                else
                    var_counter_1:=var_counter_1-1;    
                end if;  
                if(var_counter_2 = 0) then --1 seconds
                    if((unsigned(ENCODER_MAX_OVERSPEED_ALARM) > 0) and (unsigned(trigger_type) = TRIGGER_TYPE_ENC)) then
                        --var_counter_2:=100000000;    
                        var_counter_2:=33000000;                      
                        if(signed(max_overspeed_alarm_positions) > signed(ENCODER1_POS)) then
                            if((signed(max_overspeed_alarm_positions) - signed(ENCODER1_POS)) > signed(ENCODER_MAX_OVERSPEED_ALARM)) then
                                max_overspeed_alarm_y <= '1';
                            else
                                max_overspeed_alarm_y <= '0'; 
                            end if;
                        else
                            if((signed(ENCODER1_POS) - signed(max_overspeed_alarm_positions)) > signed(ENCODER_MAX_OVERSPEED_ALARM)) then
                                max_overspeed_alarm_y <= '1';
                            else
                                max_overspeed_alarm_y <= '0'; 
                            end if;
                        end if;
                        max_overspeed_alarm_positions <= ENCODER1_POS;  
                    else
                        var_counter_2:=0;
                        max_overspeed_alarm_positions <= ENCODER1_POS;
                        max_overspeed_alarm_y <= '0'; 
                    end if;
                else
                    var_counter_2:=var_counter_2-1;
                end if;             
            end if;
        end if;
    end process;
    
    IDLE_STATE_OUT <= idle_state_out_i;
	
	-- State machine for Pulser/receiver - Coinc - Avg
    general_a_sm: process(aclk) 
		variable var_counter : integer range 0 to 999999999 :=0;
		variable mrut_counter : integer range 0 to 65536 :=0;
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				-- config
				config_update_receiver 	  <= '0';
				config_update_transmitter <= '0';
				config_update_magnet 	  <= '0';
				config_update_dsp_1 	  <= '0';
				config_update_dac 		  <= '0';
				transmitter_start_cycle_mrut <= "00";
				-- update_control
				scan_mode <= (others => '0');
				trigger_type <= (others => '0');
				start_inspection <= '0';
				start_stop_inspection <= '0';
				system_ready <= '0';
				trigger_sw <= '0';
				configure_control_reset_ack <= '0';
				-- prf 
				prf_on		<= '0';
				prf_counter <= (others => '0');
				prf_counter_i <= (others => '0');
				avg_acc_i   <= '0';
				-- disposition
				disposition_on_i <= '0';
				-- external trigger
				encoder1_pos_i <= (others => '0');
				-- trigger SW
				-- tready
				general_signal_input_axis_tready <= '0';
				coincidence_average_window_exit <= '0';
				-- PWM
				PWM_MOTOR_MOVE <= '0';
				PWM_MOTOR_NOT_MOVING <= '0';
				pwm_not_moving_counter <= (others => '0');
				-- MRUT
				mrut_counter_i <= (others => '0');
				transmitter_delay_mrut <= (others => '0');
				-- LRUT
				lrut_wait_end <= '0';
				lrut_on_counter_pic <= (others => '0');
				-- Encoder
				ENCODER_RESET_CHANNEL <= '1';				
				max_overspeed_alarm_i <= '0';
				encoder1_pos_result_i <= (others => '0');
				encoder2_pos_result_i <= (others => '0');
				encoder1_dir_result_i <= (others => '0');
                encoder2_dir_result_i <= (others => '0');
				encoder1_target_pos    <= (others => '0');
				encoder1_target_neg    <= (others => '0');
				encoder1_target_first  <= '1';
				encoder_reset_counter_i<= (others => '0');
				encoder_reset_counter  <= (others => '0');
				encoder_reset_top_d1   <= '1';
				-- channel trigger (indicate to sequencer)
				CHANNEL_TRIGGER <= '0';
				idle_state_out_i <= '0';	
				-- state
				state_general_a_sm <= idle;
        	else
				if(prf_on = '1') then
					prf_counter <= prf_counter + 1;
				else
					prf_counter <= (others => '0');
				end if;
				if(encoder_reset_top_d1 = '0' and ENCODER_RESET_TOP = '1') then
				    encoder_reset_counter <= encoder_reset_counter+1;
				end if;		
				encoder_reset_top_d1 <= ENCODER_RESET_TOP;	
				case state_general_a_sm is
					when idle =>
						var_counter := 0;
						mrut_counter := 0;
						mrut_counter_i <= (others => '0');
						coinc_counter <= (others => '0');
						avg_counter_i <= (others => '0');
						general_signal_input_axis_tready <= '0';
						coincidence_average_window_exit <= '0';
						prf_on	<= '0';	
						disposition_on_i   <= '0';
						transmitter_start_cycle_mrut   <= "00";
						CHANNEL_TRIGGER <= '0';
						if(CONFIGURE_CONTROL = '1' or configure_control_reset = '1') then
						    idle_state_out_i <= '0';
						    ENCODER_RESET_CHANNEL <= '1';
                            state_general_a_sm <= update_control;
                        elsif(config_new_receiver = '1') then
                            idle_state_out_i <= '0';
							state_general_a_sm <= update_receiver;
						elsif(config_new_transmitter = '1') then	
						    idle_state_out_i <= '0';
							state_general_a_sm <= update_transmitter;
						elsif(config_new_magnet = '1') then	
						    idle_state_out_i <= '0';
							state_general_a_sm <= update_magnet;
						elsif(config_new_dsp_1 = '1') then	
						    idle_state_out_i <= '0';
							state_general_a_sm <= update_dsp_1;
						elsif(config_new_dac = '1') then
						    idle_state_out_i <= '0';	
							state_general_a_sm <= update_dac;
--                        elsif(lrut_wait_flag = '1') then
--                            lrut_on_counter_pic <= lrut_on_counter;
--                            state_general_a_sm <= lrut_wait;
						else
						    trigger_sw <= trigger_sw_control_d1;
						    start_inspection <= start_inspection_control_d1;
                            trigger_type <= trigger_type_control_d1;
						    scan_mode <= scan_mode_control_d1;
						    start_stop_inspection <= start_stop_inspection_control_d1;		
						    idle_state_out_i <= '1';	
						    encoder1_pos_result_i <= ENCODER1_POS;		
						    encoder2_pos_result_i <= ENCODER2_POS;	 
						    encoder1_dir_result_i <= ENCODER1_DIR;  
						    encoder2_dir_result_i <= ENCODER2_DIR;
						    system_ready <= '1';
							if(there_is_configuration = '1' and channel_en = '1' and IDLE_STATE_IN = '1' and idle_state_out_i = '1') then
                                if(start_inspection_control_d1 = '1') then
									if(unsigned(trigger_type_control_d1) = TRIGGER_TYPE_NORMAL) then
									    state_general_a_sm <= trigger_start;
									elsif(unsigned(trigger_type_control_d1) = TRIGGER_TYPE_ROBOT) then
									    if(start_stop_inspection_control_d1 = '1') then	
									        --disposition_on_i   <= '1';	
									        --INSPECTION_INPROGESS <= '1';							        
                                            state_general_a_sm <= trigger_start; 
									    end if;
                                    elsif(unsigned(trigger_type_control_d1) = TRIGGER_TYPE_SW) then -- SW trigger
                                        if(trigger_sw_control_d1 = '1') then                                       
                                            state_general_a_sm <= sw_trigger; 
                                        end if;
									elsif(unsigned(trigger_type_control_d1) = TRIGGER_TYPE_SW_PWM) then -- SW trigger + PWM (encoder)	                                          
                                        if(PWM_MOTOR_EN = '1') then                                            
                                            if(encoder1_target_first = '1') then
                                                encoder1_target_first <= '0';
                                                --PWM_MOTOR_MOVE <= '0';
                                                PWM_MOTOR_MOVE <= '1';
                                                encoder1_target_pos <= std_logic_vector(signed(ENCODER1_POS) + signed(encoder1_trigger_counts_i));
                                                encoder1_target_neg <= std_logic_vector(signed(ENCODER1_POS) - signed(encoder1_trigger_counts_i));  
                                                --state_general_a_sm <= wait_stop_motor;  
                                            else
                                                if(   signed(ENCODER1_POS) >= signed(encoder1_target_pos) or 
                                                      signed(ENCODER1_POS) <= signed(encoder1_target_neg)) then 
                                                    PWM_MOTOR_MOVE <= '0';
                                                    encoder1_target_pos <= std_logic_vector(signed(encoder1_target_pos) + signed(encoder1_trigger_counts_i));
                                                    encoder1_target_neg <= std_logic_vector(signed(encoder1_target_neg) - signed(encoder1_trigger_counts_i));   
                                                    state_general_a_sm <= wait_stop_motor;       
                                                else
                                                    PWM_MOTOR_MOVE <= '1';
                                                end if;  
                                            end if;                                               
                                        else
                                            encoder1_target_first <= '1'; 
                                            PWM_MOTOR_MOVE <= '0';
                                        end if;				   
                                    elsif(unsigned(trigger_type_control_d1) = TRIGGER_TYPE_ENC and unsigned(encoder1_trigger_counts_i) > 0) then -- Econder 								
										if(encoder1_target_first = '1' or ENCODER_RESET_TOP = '1' or
										   unsigned(encoder_reset_counter_i) /= unsigned(encoder_reset_counter)) then
										    encoder_reset_counter_i <= encoder_reset_counter;
										    max_overspeed_alarm_i <= '0';
										    encoder1_target_first <= '0';
										    encoder1_target_pos <= std_logic_vector(signed(ENCODER1_POS) + signed(encoder1_trigger_counts_i));
										    encoder1_target_neg <= std_logic_vector(signed(ENCODER1_POS) - signed(encoder1_trigger_counts_i));  
                                        elsif(signed(ENCODER1_POS) >= signed(encoder1_target_pos) or 
                                              signed(ENCODER1_POS) <= signed(encoder1_target_neg)) then 
                                            if(signed(ENCODER1_POS) > signed(encoder1_target_pos) or 
                                                 signed(ENCODER1_POS) < signed(encoder1_target_neg)) then
                                                  max_overspeed_alarm_i <= '1';                                            
                                                  state_general_a_sm <= encoder_alignment; 
										    else
                                                max_overspeed_alarm_i <= '0';
                                                encoder1_target_pos <= std_logic_vector(signed(ENCODER1_POS) + signed(encoder1_trigger_counts_i));
                                                encoder1_target_neg <= std_logic_vector(signed(ENCODER1_POS) - signed(encoder1_trigger_counts_i));
                                                state_general_a_sm <= trigger_start;                                   
										    end if;
										else
										    max_overspeed_alarm_i <= '0';	
										end if;			   
                                    end if;
                                else
									if(unsigned(trigger_type_control_d1) = TRIGGER_TYPE_ROBOT) then
                                        if(start_stop_inspection_control_d1 = '1') then
                                            state_general_a_sm <= trigger_start; 
                                        end if;
                                    elsif(unsigned(trigger_type_control_d1) = TRIGGER_TYPE_SW ) then -- SW trigger
                                        if(trigger_sw_control_d1 = '1') then  
                                            state_general_a_sm <= sw_trigger; 
                                        end if;
									elsif(unsigned(trigger_type_control_d1) = TRIGGER_TYPE_SW_PWM) then -- SW trigger + PWM (encoder)
                                        if(PWM_MOTOR_EN = '1') then                                            
                                            if(encoder1_target_first = '1') then
                                                encoder1_target_first <= '0';
                                                --PWM_MOTOR_MOVE <= '0';
                                                PWM_MOTOR_MOVE <= '1';
                                                encoder1_target_pos <= std_logic_vector(signed(ENCODER1_POS) + signed(encoder1_trigger_counts_i));
                                                encoder1_target_neg <= std_logic_vector(signed(ENCODER1_POS) - signed(encoder1_trigger_counts_i));  
                                                --state_general_a_sm <= wait_stop_motor;  
                                            else
                                                if(   signed(ENCODER1_POS) >= signed(encoder1_target_pos) or 
                                                  signed(ENCODER1_POS) <= signed(encoder1_target_neg)) then 
                                                    PWM_MOTOR_MOVE <= '0';
                                                    encoder1_target_pos <= std_logic_vector(signed(encoder1_target_pos) + signed(encoder1_trigger_counts_i));
                                                    encoder1_target_neg <= std_logic_vector(signed(encoder1_target_neg) - signed(encoder1_trigger_counts_i));   
                                                    state_general_a_sm <= wait_stop_motor;       
                                                else
                                                    PWM_MOTOR_MOVE <= '1';
                                                end if;  
                                            end if;                                               
                                        else
                                            encoder1_target_first <= '1'; 
                                            PWM_MOTOR_MOVE <= '0';
                                        end if;
								    elsif(unsigned(trigger_type_control_d1) = TRIGGER_TYPE_ROBOT_ENC and unsigned(encoder1_trigger_counts_i) > 0 and
								          start_stop_inspection_control_d1 = '1') then -- Econder 
								        if(encoder1_target_first = '1' or ENCODER_RESET_TOP = '1' or
                                        unsigned(encoder_reset_counter_i) /= unsigned(encoder_reset_counter)) then
                                            encoder_reset_counter_i <= encoder_reset_counter;
                                            max_overspeed_alarm_i <= '0';
                                            encoder1_target_first <= '0';
                                            encoder1_target_pos <= std_logic_vector(signed(ENCODER1_POS) + signed(encoder1_trigger_counts_i));
                                            encoder1_target_neg <= std_logic_vector(signed(ENCODER1_POS) - signed(encoder1_trigger_counts_i));  
                                        elsif(signed(ENCODER1_POS) >= signed(encoder1_target_pos) or 
                                              signed(ENCODER1_POS) <= signed(encoder1_target_neg)) then 
                                            if(signed(ENCODER1_POS) > signed(encoder1_target_pos) or 
                                               signed(ENCODER1_POS) < signed(encoder1_target_neg)) then
                                                max_overspeed_alarm_i <= '1';                                            
                                                state_general_a_sm <= encoder_alignment; 
                                            else
                                                max_overspeed_alarm_i <= '0';
                                                encoder1_target_pos <= std_logic_vector(signed(ENCODER1_POS) + signed(encoder1_trigger_counts_i));
                                                encoder1_target_neg <= std_logic_vector(signed(ENCODER1_POS) - signed(encoder1_trigger_counts_i));
                                                state_general_a_sm <= trigger_start;                                   
                                            end if;
                                        else
                                            max_overspeed_alarm_i <= '0';    
                                        end if;											    
									end if;
								end if;
							else
							    idle_state_out_i <= '1';							
							end if;
						end if;
					when update_control => 
					   scan_mode <= SCAN_MODE_CONTROL;
                       trigger_type <= TRIGGER_TYPE_CONTROL;
                       start_inspection <= START_INSPECTION_CONTROL;
                       start_stop_inspection <= START_STOP_INSPECTION_CONTROL;
                       --trigger_sw <= TRIGGER_SW_CONTROL; 
                       max_overspeed_alarm_i <= '0';  
                       ENCODER_RESET_CHANNEL <= '0';      
                       encoder1_target_first <= '1';   
                       configure_control_reset_ack <= configure_control_reset;                                 
					   if(CONFIGURE_CONTROL = '0' and configure_control_reset = '0') then
					       state_general_a_sm <= idle;
					   end if;
					when wait_state =>
						if(var_counter >= 128) then
							state_general_a_sm <= idle;		
						else
							var_counter := var_counter + 1;	
						end if;
				    when lrut_wait =>
				        if(var_counter >= LRUT_WAIT_CYCLES) then
				            if(lrut_wait_flag = '0') then
				                lrut_wait_end <= '0';
				                state_general_a_sm <= idle;
				            else
				                if(lrut_on_counter_pic = lrut_on_counter) then
				                    lrut_wait_end <= '1';
				                else
				                    lrut_on_counter_pic <= lrut_on_counter;
				                    var_counter := 0;    
				                end if;
				            end if;
				        else
				            var_counter := var_counter + 1;
				        end if;			    
                    when prf_control =>
                        if(prf_counter >= unsigned(transmitter_prf)) then
                            prf_counter_i <= std_logic_vector(prf_counter);
                            if(avg_acc_i = '1') then
                                prf_on	<= '0';
                                state_general_a_sm <= wait_clear_avg_acc;
                            else
                                state_general_a_sm <= idle;
                            end if;
                        else
                            prf_counter_i <= transmitter_prf; 
                        end if;
                    when prf_control_mrut =>
                        if(prf_counter >= unsigned(transmitter_prf_mrut)) then
                            prf_counter_i <= std_logic_vector(prf_counter);
                            if(mrut_counter >= 2) then
                                state_general_a_sm <= idle;
                            else
                                prf_on	<= '0';	
                                state_general_a_sm <= wait_clear_avg_acc;
                            end if;
                        else
                            prf_counter_i <= transmitter_prf_mrut;    
                        end if;
                    when prf_control_enc =>
                        prf_counter_i <= transmitter_prf;
                        if(prf_counter >= unsigned(transmitter_prf_enc)) then
                            if(avg_acc_i = '1') then
                                prf_on    <= '0';
                                state_general_a_sm <= wait_clear_avg_acc;
                            else
                                state_general_a_sm <= idle;
                            end if;
                        end if;
                    when wait_clear_avg_acc =>
                        prf_on	<= '1';
                        if(avg_acc = '0') then
                            state_general_a_sm <= sync_channels;
                        end if;
                    when sync_channels =>                   
                        if(IDLE_STATE_IN = '1' and idle_state_out_i = '1') then
                            state_general_a_sm <= trigger_start;
                        else
                            idle_state_out_i <= '1';
                        end if;
                    when encoder_alignment =>
                        if((signed(ENCODER1_POS) <= signed(encoder1_target_pos) and signed(ENCODER1_POS) >= signed(encoder1_target_neg)) or
                           (ENCODER_RESET_TOP = '1') or
                           (unsigned(encoder_reset_counter_i) /= unsigned(encoder_reset_counter))) then
                            state_general_a_sm <= idle;    
                        else
                            if(signed(ENCODER1_POS) > signed(encoder1_target_pos)) then
                                encoder1_target_neg <= encoder1_target_pos;
                                encoder1_target_pos <= std_logic_vector(signed(encoder1_target_pos) + signed(encoder1_trigger_counts_i));
                            else
                                encoder1_target_pos <= encoder1_target_neg;
                                encoder1_target_neg <= std_logic_vector(signed(encoder1_target_neg) - signed(encoder1_trigger_counts_i));
                            end if;                        
                        end if;                    
					when sw_trigger =>
					   trigger_sw <= trigger_sw_control_d1;		    
					   if(trigger_sw_control_d1 = '0') then  
					       state_general_a_sm <= trigger_start;
					   end if;
                    when wait_stop_motor =>
                        idle_state_out_i <= '0';
                        -- 100 ms
						if(var_counter >= MOTOR_STOP_CYCLES) then  
							var_counter := 0;								   
                            state_general_a_sm <= trigger_start;        
                        else
                            var_counter := var_counter + 1;    
                        end if;
				    when trigger_start =>  				       	  
				       system_ready <= '0';  
                       prf_on    <= '1';                   
                       if(unsigned(scan_mode) = SCAN_MODE_MRUT and avg_acc_i = '0') then
                           if(CHANNEL_ID = 0) then
                               if(mrut_counter = 0) then
                                   transmitter_delay_mrut <= (others => '0');
                                   transmitter_start_cycle_mrut   <= "00";
                               else
                                   transmitter_delay_mrut <= transmitter_delay_i;
                                   transmitter_start_cycle_mrut   <= "01";
                               end if;
                           else
                               if(mrut_counter = 0) then
                                   transmitter_delay_mrut <= transmitter_delay_i;
                                   transmitter_start_cycle_mrut   <= "01";
                               else
                                   transmitter_delay_mrut <= (others => '0');
                                   transmitter_start_cycle_mrut   <= "00";
                               end if;
                           end if;    
                       end if;
                       coinc_counter <= (others => '0');
                       avg_counter_i <= (others => '0');
                       idle_state_out_i <= '0';                        
                       if(var_counter >= 8) then
                           var_counter := 0;
                           general_signal_input_axis_tready <= '0';
                           coincidence_average_window_exit <= '0';
                           CHANNEL_TRIGGER <= '1';
						   state_general_a_sm <= coincidence_average_window;
                       else
                           var_counter := var_counter + 1;    
                       end if;                                                     
					when trigger_external =>
					    prf_on	<= '1';
						if(signed(encoder1_trigger_diff) >= signed(encoder1_trigger_counts_i)) then
						    coinc_counter <= (others => '0');
						    avg_counter_i <= (others => '0');
							general_signal_input_axis_tready <= '0';
							coincidence_average_window_exit <= '0';
							state_general_a_sm <= coincidence_average_window;
						end if;
					when trigger_external_clear =>
					    start_stop_inspection <= start_stop_inspection_control_d1;
					    prf_counter_i <= std_logic_vector(prf_counter);
						if(start_stop_inspection_control_d1 = '0' or CONFIGURE_CONTROL = '0') then
							prf_on	<= '0';
							disposition_on_i   <= '0';
							state_general_a_sm <= idle;
						end if;  
					when coincidence_average_window =>                         
						-- Average
						if(avg_acc = '1' or coinc_acc = '1') then
						    avg_acc_i <= '1';
						    general_signal_input_axis_tready <= '0';
						    coincidence_average_window_exit <= '1';
                            if(unsigned(scan_mode) = SCAN_MODE_MRUT) then
                                state_general_a_sm <= prf_control_mrut;
                            else
                                state_general_a_sm <= prf_control;
                            end if;	
						elsif((fir_signal_input_axis_tlast = '1' and coincidence_average_window_exit = '1' and dsp_fir_delay_on = '0') or
						      (fir_signal_input_axis_tlast = '1' and coincidence_average_window_exit = '1' and dsp_fir_delay_on = '1' and dsp_fir_delay_state = "01")) then				    
						    avg_acc_i <= '0';
							general_signal_input_axis_tready <= '0';
							coincidence_average_window_exit <= '1';
							if(unsigned(trigger_type) = TRIGGER_TYPE_NORMAL) then
							    if(unsigned(scan_mode) = SCAN_MODE_MRUT) then
							        mrut_counter := mrut_counter + 1;
							        mrut_counter_i <= std_logic_vector(unsigned(mrut_counter_i)+1);
                                    state_general_a_sm <= prf_control_mrut;
                                else
                                    state_general_a_sm <= prf_control;
                                end if;
                            elsif(unsigned(trigger_type) = TRIGGER_TYPE_ENC) then
							    if(unsigned(scan_mode) = SCAN_MODE_MRUT) then
                                    mrut_counter := mrut_counter + 1;
                                    mrut_counter_i <= std_logic_vector(unsigned(mrut_counter_i)+1);
                                    state_general_a_sm <= prf_control_mrut;
                                else
                                    --state_general_a_sm <= prf_control;
                                    state_general_a_sm <= prf_control_enc;
                                end if;
                            elsif((unsigned(trigger_type) = TRIGGER_TYPE_SW) or
                                  (unsigned(trigger_type) = TRIGGER_TYPE_SW_PWM) ) then
                                if(unsigned(scan_mode) = SCAN_MODE_MRUT) then
                                    mrut_counter := mrut_counter + 1;
                                    mrut_counter_i <= std_logic_vector(unsigned(mrut_counter_i)+1);
                                    state_general_a_sm <= prf_control_mrut;
                                else
                                    prf_counter_i <= std_logic_vector(prf_counter);
                                    --state_general_a_sm <= idle;
                                    state_general_a_sm <= prf_control;
                                end if;
                            elsif((unsigned(trigger_type) = TRIGGER_TYPE_ROBOT) or
                                  (unsigned(trigger_type) = TRIGGER_TYPE_ROBOT_ENC)) then
                                state_general_a_sm <= prf_control;    
                            end if;
                        else
                            if(fir_signal_input_axis_tvalid = '1' or coincidence_average_window_exit = '1') then
                                general_signal_input_axis_tready <= '0'; 
                            else
                                general_signal_input_axis_tready <= digital_amplifier_tready;
                            end if;
                            if(avg_coinc_pos = '0') then  
                                if(unsigned(avg_type) = AVG_TYPE_NORMAL) then
                                    if(avg_result_axis_tlast = '1') then
                                        general_signal_input_axis_tready <= '0';
                                        coincidence_average_window_exit <= '1';
                                    end if;
                                else
                                    if(coinc_result_axis_tlast = '1') then
                                        general_signal_input_axis_tready <= '0';
                                        coincidence_average_window_exit <= '1';
                                    end if;
                                end if;  
                            else
                                if(avg_result_axis_tlast = '1') then
                                    general_signal_input_axis_tready <= '0';
                                    coincidence_average_window_exit <= '1';
                                end if;
                            end if; 
						end if;
					when update_receiver =>
						if(config_new_receiver = '0') then
							config_update_receiver <= '0';	
							state_general_a_sm <= idle;											  
						else
							config_update_receiver <= '1';												  
						end if;	
					when update_transmitter =>
						if(config_new_transmitter = '0') then
							config_update_transmitter <= '0';	
							state_general_a_sm <= idle;											  
						else
							config_update_transmitter <= '1';												  
						end if;		
					when update_magnet =>
						if(config_new_magnet = '0') then
							config_update_magnet <= '0';	
							state_general_a_sm <= idle;											  
						else
							config_update_magnet <= '1';												  
						end if;		
					when update_dsp_1 =>
						if(config_new_dsp_1 = '0') then
							config_update_dsp_1 <= '0';	
							state_general_a_sm <= idle;											  
						else
							config_update_dsp_1 <= '1';												  
						end if;
					when update_dac =>
						if(config_new_dac = '0') then
							config_update_dac <= '0';	
							state_general_a_sm <= idle;											  
						else
							config_update_dac <= '1';												  
						end if;		
					when others =>
						null;
				end case;
			end if;
         end if;
    end process;
                    			 
    fir_signal_input_axis_tready_i <= fir_signal_input_axis_tready and fir_signal_input_axis_tready_x;		         
                       			 		 			 
	-- State machine for FIR/CORR - GATES
    general_b_sm: process(aclk) 
		variable var_counter : integer range 0 to 65536 :=0;
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				-- Disposition
				DISPOSITION_BITS		<= (others => '0');
				DISPOSITION				<= '0';
				-- Read gates
				read_gates_result		<= '0';
				-- Config update
				config_update_dsp_2 	<= '0';
				config_update_fir 		<= '0';
				config_update_gates 	<= '0';
				-- Store result
                store_gates_flag       <= '0';
				-- Read ascan
				read_ascan_flag   		<= '0';
				-- FIR tready
				fir_signal_input_axis_tready_x <= '0';			
				read_fir_result_type <= '0';
				-- Decimation
				read_fir_dec_result  <= '0';
				-- Encoder
				encoder1_pos_result <= (others => '0'); 
                encoder2_pos_result <= (others => '0');   
				encoder1_dir_result <= (others => '0'); 
                encoder2_dir_result <= (others => '0');       
                mrut_counter_result <= (others => '0'); 
                -- Decimation
                dsp_dec_type <= (others => '0'); 
                dsp_dec_level <= (others => '0'); 
                dsp_dec_window <= (others => '0'); 
				-- Data_compression
                ga_reset <= '1';
                dsp_compression_mode_i <= (others => '0'); 
                dsp_compression_level_i <= (others => '0'); 
				-- state
				state_general_b_sm <= idle;
        	else
				case state_general_b_sm is
					when idle =>
					    encoder1_pos_result <= encoder1_pos_result_i;			    
                        encoder2_pos_result <= encoder2_pos_result_i;
 					    encoder1_dir_result <= encoder1_dir_result_i;
                        encoder2_dir_result <= encoder2_dir_result_i;
                        mrut_counter_result <= mrut_counter_i;
                        dsp_dec_type <= dsp_dec_type_i;
                        dsp_dec_level <= dsp_dec_level_i;
                        dsp_dec_window <= dsp_dec_window_i;
                        ga_reset <= '0';
                        dsp_compression_mode_i <= dsp_compression_mode;
                        dsp_compression_level_i <= dsp_compression_level;
					    if(fir_signal_input_axis_tlast = '1') then
					        fir_signal_input_axis_tready_x <= '0';
                            state_general_b_sm <= wait_fir_end;
                        elsif(fir_signal_input_axis_tvalid = '1') then
                            fir_signal_input_axis_tready_x <= '1';
                            state_general_b_sm <= wait_input_fir_end;
						elsif(config_new_dsp_2 = '1') then
						    fir_signal_input_axis_tready_x <= '0';
							state_general_b_sm <= update_dsp_2;
						elsif(config_new_fir = '1') then
						    fir_signal_input_axis_tready_x <= '0';
							state_general_b_sm <= update_FIR;
						elsif(config_new_gates = '1') then
						    fir_signal_input_axis_tready_x <= '0';
							state_general_b_sm <= update_gates;
						else
							fir_signal_input_axis_tready_x <= '1';
						end if;	
				    when wait_input_fir_end =>
                        if(fir_signal_input_axis_tlast = '1') then
                            fir_signal_input_axis_tready_x <= '0';
                            state_general_b_sm <= wait_fir_end;            
                        end if;    	    
					when wait_fir_end =>
						if(fir_end = '1') then
							fir_signal_input_axis_tready_x <= '0';
							state_general_b_sm <= wait_gates_end;	
					    else
                            if(dsp_fir_delay_on = '1') then
                               fir_signal_input_axis_tready_x <= '1';
                            end if; 
						end if;			    
					when wait_gates_end =>
						if(gates_en = '1') then
							if(gates_end = '1') then
								read_gates_result	<= '1';
								state_general_b_sm <= wait_decimation_end;
							end if;
						else
							state_general_b_sm <= wait_decimation_end;	
						end if;
				    when wait_decimation_end => 
						if(dec_en = '1') then
						    read_fir_result_type <= '1';
						    read_fir_dec_result  <= '1';
                            if(dec_end = '1') then
                                if(disposition_on_i = '1') then
                                    state_general_b_sm <= data_disposition;    
                                else
                                    state_general_b_sm <= store_gates;    
                                end if;
                            else
                                state_general_b_sm <= wait_decimation_end;	
                            end if;
                        else
                            read_fir_result_type <= '0';
                            read_fir_dec_result  <= '0';
                            if(disposition_on_i = '1') then
                                state_general_b_sm <= data_disposition;    
                            else
                                state_general_b_sm <= read_result;    
                            end if;    
                        end if; 
					when data_disposition =>
						if(gates_en = '0') then
							DISPOSITION_BITS <= std_logic_vector(to_unsigned(DISPOSITION_NODET, DISPOSITION_BITS'length));
						else
							if(unsigned(GATE2_TOF) >= unsigned(GATE2_TOF_ALARM_MIN_VALUE) and unsigned(GATE2_TOF) <= unsigned(GATE2_TOF_ALARM_MAX_VALUE)) then
								DISPOSITION_BITS <= std_logic_vector(to_unsigned(DISPOSITION_PASS, DISPOSITION_BITS'length));
							else
								DISPOSITION_BITS <= std_logic_vector(to_unsigned(DISPOSITION_FAIL, DISPOSITION_BITS'length));	
							end if;
						end if;
						state_general_b_sm <= data_disposition_flag;																			 
					when data_disposition_flag =>																
						--if(disposition_on_i = '0') then
							--DISPOSITION	<= '0';													 
							state_general_b_sm <= read_result;
						--else
							--DISPOSITION	<= '1';													 
						--end if;
				    when store_gates => 
						read_gates_result	<= '0';	
                        read_fir_dec_result <= '0'; 
                        if(store_gates_end = '1') then
                            store_gates_flag <= '0';    
                            state_general_b_sm <= store_gates_check;                                                 
                        else
                            store_gates_flag <= '1';                                                                                                          
                        end if;
				    when store_gates_check =>
                        if(start_stop_inspection = '0') then                           
                            state_general_b_sm <= read_result;
                        else
                            if(unsigned(dsp_compression_mode_i) = COMPRESSION_GATES) then
                                if(unsigned(ga_n_gates) >= unsigned(dsp_compression_level_i)) then
                                    state_general_b_sm <= read_result; 
                                else
                                    state_general_b_sm <= store_result_check_wait;    
                                end if; 
                            elsif(unsigned(dsp_compression_mode_i) = COMPRESSION_ASCAN) then
                                if(unsigned(ga_n_gates) >= unsigned(dsp_compression_level_i)) then
                                    state_general_b_sm <= read_result; 
                                else 
                                    state_general_b_sm <= store_result_check_wait; 
                                end if;   
                            elsif(unsigned(dsp_compression_mode_i) = COMPRESSION_ASCAN_GATES) then
                                if(unsigned(ga_n_gates) >= unsigned(dsp_compression_level_i)) then
                                    state_general_b_sm <= read_result;  
                                else
                                    state_general_b_sm <= store_result_check_wait;
                                end if;
                            else
                                state_general_b_sm <= read_result;
                            end if; 
                        end if; 
                    when store_result_check_wait =>
                        if(var_counter >= 128) then
                            var_counter := 0;
                            state_general_b_sm <= idle;
                        else
                            var_counter := var_counter + 1;
                        end if; 
					when read_result =>															 
						if(read_ascan_end = '1') then
							read_ascan_flag <= '0';	
							state_general_b_sm <= idle;												 
						else
							read_ascan_flag <= '1';													 													 
						end if;																 
					when update_dsp_2 =>
						if(config_new_dsp_2 = '0') then
							config_update_dsp_2 <= '0';	
							state_general_b_sm <= idle;											  
						else
							config_update_dsp_2 <= '1';												  
						end if;	
					when update_FIR =>
						if(config_new_fir = '0') then
							config_update_fir <= '0';	
							state_general_b_sm <= idle;											  
						else
							config_update_fir <= '1';												  
						end if;		
					when update_gates =>
						if(config_new_gates = '0') then
							config_update_gates <= '0';	
							state_general_b_sm <= idle;											  
						else
							config_update_gates <= '1';												  
						end if;			
					when others =>
						null;
				end case;
			end if;
         end if;
    end process;
																			 
	fir_result_axis_tready_i <= fir_result_axis_tready_x and ASCAN_AXIS_TREADY;
	dec_result_axis_tready <= dec_result_axis_tready_x and ASCAN_AXIS_TREADY;
	ga_result_axis_tready <= ASCAN_AXIS_TREADY;
	
	ASCAN_READY <= ascan_ready_i;		
															 																		 																						 
	-- State machine for upload ASCAN
    upload_ascan_sm: process(aclk) 
		variable var_ascan_counter : integer range 0 to 65536 :=0;
		variable var_wait_counter : integer range 0 to 65536 :=0;																			 
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				ascan_ready_i <= '0';	
				ASCAN_SIZE <= (others => '0');															 
				read_fir_result <= '0';
				no_read_fir_result 	<= '0';		
				read_dec_result <= '0';
				no_read_dec_result <= '0';	
				data_window_half_result	<= (others => '0');																		 
				ASCAN_AXIS_TDATA <= (others => '0');
				ASCAN_AXIS_TKEEP <= (others => '0');
				ASCAN_AXIS_TVALID <= '0';
				ASCAN_AXIS_TLAST <= '0';
				fir_result_axis_tready_x <= '0';	
				dec_result_axis_tready_x <= '0';	
				tready_flank_up_flag <= '0';
				ascan_axis_tdata_1 <= (others => '0');	 
				ascan_axis_tdata_2 <= (others => '0');																	 
				ascan_axis_tready_i1 <= '0';
				read_ascan_end <= '0';	 
				ascan_counter <= (others => '0');	
				adquisiton_provided <= '0';	
				gates_acc_counter <= (others => '0');							 
				-- state
				state_read_ascan_sm <= idle;
        	else
				ascan_axis_tready_i1 <= ASCAN_AXIS_TREADY;														 
				case state_read_ascan_sm is
					when idle =>
						fir_result_axis_tready_x <= '0';
						dec_result_axis_tready_x <= '0';
						read_ascan_end <= '0';		
						read_fir_result <= '0';
						no_read_fir_result 	<= '0';	
						read_dec_result <= '0';
						no_read_dec_result <= '0';	
						if(read_fir_result_type = '0') then
						    data_window_half_result <= fir_data_window_half_result;
                            ASCAN_SIZE <= std_logic_vector(to_unsigned(ASCAN_HEADER_SIZE, ASCAN_SIZE'length) + 
                                                       resize(unsigned(fir_data_window_half_result), ASCAN_SIZE'length));				
						else
						    data_window_half_result <= dec_window_half_result;
                            ASCAN_SIZE <= std_logic_vector(to_unsigned(ASCAN_HEADER_SIZE, ASCAN_SIZE'length) + 
                                                       resize(unsigned(dec_window_half_result), ASCAN_SIZE'length));    
						end if;																		 
						if(read_ascan_flag = '1') then			
							state_read_ascan_sm <= read_ascan_start;													 
						end if;	
					when read_ascan_start =>
						var_ascan_counter:= 0;										  
						if(READ_ASCAN = '1') then
							ascan_ready_i <= '0';	
							-- Prepare read from FIR
							if(read_fir_result_type = '0') then
							     read_fir_result <= '1';
							else
							     read_dec_result <= '1';
							end if;
							ASCAN_AXIS_TKEEP <= (others => '1');
							ascan_counter <= (others => '0');
							if(ASCAN_AXIS_TREADY = '1') then
							    state_read_ascan_sm <= upload_general_header;
							end if;
						else
						    if( (unsigned(trigger_type) = TRIGGER_TYPE_SW) or					        
                                (unsigned(trigger_type) = TRIGGER_TYPE_ENC) or
                                (unsigned(trigger_type) = TRIGGER_TYPE_ROBOT_ENC)) then
                                ascan_ready_i <= '1';	
                            elsif( (unsigned(trigger_type) = TRIGGER_TYPE_ROBOT) ) then
                                if(start_stop_inspection = '0') then   
                                    if(ascan_ready_i = '0') then
                                        state_read_ascan_sm <= no_read_result;
                                    end if;
                                else
                                    ascan_ready_i <= '1';
                                end if;
                            else
							    if(start_inspection = '0') then
							        if(ascan_ready_i = '0') then
								        state_read_ascan_sm <= no_read_result;
						            end if;
							    else
								    ascan_ready_i <= '1';										  
							    end if;															 
						    end if;
				        end if;													 
					when no_read_result =>		
						if(var_ascan_counter >= WAIT_CYCLES) then
							if(read_ascan_flag = '0') then
							    no_read_fir_result 	<= '0';		
							    no_read_dec_result 	<= '0';										 
								state_read_ascan_sm <= idle;
						    else
						        read_ascan_end <= '1';    
							end if;
						else
							no_read_fir_result 	<= '1';
							no_read_dec_result 	<= '1';
							var_ascan_counter := var_ascan_counter + 1;
						end if;																 												 
					when upload_general_header =>
						if(ASCAN_AXIS_TREADY = '1') then
						    ascan_counter <= ascan_counter + 1;
							ASCAN_AXIS_TVALID <= '1';													 
							if(var_ascan_counter = 0) then	    -- version
							    -- 4 bytes internal_number.major.minor.revision
							    -- 1.1.0.0
								ASCAN_AXIS_TDATA(7 downto 0)  <= std_logic_vector(to_unsigned(0, ASCAN_AXIS_TDATA(7 downto 0)'length));
								ASCAN_AXIS_TDATA(15 downto 8) <= std_logic_vector(to_unsigned(0, ASCAN_AXIS_TDATA(15 downto 8)'length));
								ASCAN_AXIS_TDATA(23 downto 16)<= std_logic_vector(to_unsigned(1, ASCAN_AXIS_TDATA(23 downto 16)'length));
								ASCAN_AXIS_TDATA(31 downto 24)<= std_logic_vector(to_unsigned(1, ASCAN_AXIS_TDATA(31 downto 24)'length));	
							elsif(var_ascan_counter = 1) then	-- scan mode										 
                                ASCAN_AXIS_TDATA <= scan_mode;
                            elsif(var_ascan_counter = 2) then   -- trigger type
                                ASCAN_AXIS_TDATA <= trigger_type;		
                            elsif(var_ascan_counter = 3) then   -- scan state
                                ASCAN_AXIS_TDATA(0) <= start_inspection;
                                ASCAN_AXIS_TDATA(1) <= start_stop_inspection;
                                ASCAN_AXIS_TDATA(2) <= not PWM_END;
                                ASCAN_AXIS_TDATA(3) <= max_overspeed_alarm_i;
                                ASCAN_AXIS_TDATA(4) <= max_overspeed_alarm_x;
                                ASCAN_AXIS_TDATA(5) <= max_overspeed_alarm_y;
                                ASCAN_AXIS_TDATA(31 downto 6) <= (others => '0');
                            elsif(var_ascan_counter = 4) then   -- Data format (decimation)
                                -- 0 = normal by default
                                if(read_fir_result_type = '0') then
                                    ASCAN_AXIS_TDATA <= std_logic_vector(to_unsigned(DEC_TYPE_NORMAL, ASCAN_AXIS_TDATA'length));
                                else
                                    ASCAN_AXIS_TDATA <= std_logic_vector(to_unsigned(DEC_TYPE_CORDINATES, ASCAN_AXIS_TDATA'length));
                                end if;
                            elsif(var_ascan_counter = 5) then   -- Timestamp
                                ASCAN_AXIS_TDATA <= time_stamp_counter(31 downto 0);
                            elsif(var_ascan_counter = 6) then   -- Timestamp
                                ASCAN_AXIS_TDATA <= time_stamp_counter(63 downto 32);
                            elsif(var_ascan_counter = 7) then   -- Mru counter	
                                ASCAN_AXIS_TDATA <= mrut_counter_result;	
                            elsif(var_ascan_counter = 8) then   -- Voltage status	
                                ASCAN_AXIS_TDATA <= VOLTAGE_STATUS;    
                                		 
							elsif(var_ascan_counter = 14) then	-- number of channels												 
								ASCAN_AXIS_TDATA <= std_logic_vector(to_unsigned(1, ASCAN_AXIS_TDATA'length));
							elsif(var_ascan_counter = 15) then	-- channel offset (var_ascan_counter + 1)
								ASCAN_AXIS_TDATA <= std_logic_vector(to_unsigned(16, ASCAN_AXIS_TDATA'length));
						    else
						        ASCAN_AXIS_TDATA <= (others => '0'); 
							end if;

							if(var_ascan_counter >= 15) then
								var_ascan_counter:= 0;
								state_read_ascan_sm <= upload_ascan_header;													 
							else													 
								var_ascan_counter:= var_ascan_counter+1;		
							end if;															 
						end if;
					when upload_ascan_header =>
						if(ASCAN_AXIS_TREADY = '1') then
						    ascan_counter <= ascan_counter + 1;
							ASCAN_AXIS_TVALID <= '1';													 
							if(var_ascan_counter = 0) then		    -- channel_id
								ASCAN_AXIS_TDATA <= std_logic_vector(to_unsigned(CHANNEL_ID, ASCAN_AXIS_TDATA'length));											 
							elsif(var_ascan_counter = 1) then		-- clock_rate											 
								ASCAN_AXIS_TDATA <= fir_sample_freq_result;
							elsif(var_ascan_counter = 2) then		-- prf
								--ASCAN_AXIS_TDATA <= prf_counter_i;	
								if(unsigned(trigger_type) = TRIGGER_TYPE_NORMAL) then
								    ASCAN_AXIS_TDATA <= transmitter_prf_result;		
								elsif((unsigned(trigger_type) = TRIGGER_TYPE_ENC) or
								      (unsigned(trigger_type) = TRIGGER_TYPE_SW) or
								      (unsigned(trigger_type) = TRIGGER_TYPE_SW_PWM) or
								      (unsigned(trigger_type) = TRIGGER_TYPE_ROBOT) or
								      (unsigned(trigger_type) = TRIGGER_TYPE_ROBOT_ENC) ) then	
								    ASCAN_AXIS_TDATA <= transmitter_prf;									     
								else
								    ASCAN_AXIS_TDATA <= prf_counter_i;		
								end if;																		 													 
							elsif(var_ascan_counter = 3) then		-- ascan size
								--ASCAN_AXIS_TDATA <= data_window_half_result;	
								if(read_fir_result_type = '0') then
								    ASCAN_AXIS_TDATA <= fir_data_window_result;
								else
								    ASCAN_AXIS_TDATA <= dec_window_half_result;
								end if;
								
						    elsif(var_ascan_counter = 4) then		-- Pulser Status
						        ASCAN_AXIS_TDATA(3 downto 0) <= PULSER_STATUS;
                                ASCAN_AXIS_TDATA(15 downto 4) <= (others => '0');
                                ASCAN_AXIS_TDATA(31 downto 16) <= (others => '0');   
						    elsif(var_ascan_counter = 5) then		-- overspeed alarm
                                ASCAN_AXIS_TDATA(0) <= max_overspeed_alarm_i;
                                ASCAN_AXIS_TDATA(1) <= max_overspeed_alarm_x;
                                ASCAN_AXIS_TDATA(2) <= max_overspeed_alarm_y;
                                ASCAN_AXIS_TDATA(31 downto 3) <= (others => '0');
						    elsif(var_ascan_counter = 6) then	    -- temperature	
						        ASCAN_AXIS_TDATA <= TEMPERATURE_STATUS;
						    elsif(var_ascan_counter = 7) then	    -- sensor type (EMAT)
						        ASCAN_AXIS_TDATA <= std_logic_vector(to_unsigned(SENSOR_TYPE, ASCAN_AXIS_TDATA'length));
						    
						    elsif(var_ascan_counter = 15) then		-- ascan offset
								ASCAN_AXIS_TDATA <= std_logic_vector(to_unsigned(ASCAN_HEADER_SIZE, ASCAN_AXIS_TDATA'length));
								
							elsif(var_ascan_counter = 16) then		-- gate1 tof												 
								ASCAN_AXIS_TDATA <= gate1_tof;
							elsif(var_ascan_counter = 17) then		-- gate1 tof avg	
								ASCAN_AXIS_TDATA <= gate1_tof_averaged;	
							elsif(var_ascan_counter = 18) then		-- gate1 tof avg
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(gate1_amp_alarm), ASCAN_AXIS_TDATA'length));	
							elsif(var_ascan_counter = 19) then		-- gate1 tof alarm
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(gate1_tof_alarm), ASCAN_AXIS_TDATA'length));	
						    elsif(var_ascan_counter = 20) then		-- gate1 amp max
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(signed(gate1_max), ASCAN_AXIS_TDATA'length));	
							elsif(var_ascan_counter = 21) then	-- gate1 amp max sample
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(gate1_max_sample), ASCAN_AXIS_TDATA'length));
						    elsif(var_ascan_counter = 22) then	-- gate1 amp min		
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(signed(gate1_min), ASCAN_AXIS_TDATA'length));	
							elsif(var_ascan_counter = 23) then	-- gate1 amp min sample	
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(gate1_min_sample), ASCAN_AXIS_TDATA'length));	
							
							elsif(var_ascan_counter = 32) then	-- gate2 tof													 
								ASCAN_AXIS_TDATA <= gate2_tof;	
							elsif(var_ascan_counter = 33) then	-- gate2 tof avg
								ASCAN_AXIS_TDATA <= gate2_tof_averaged;	
							elsif(var_ascan_counter = 34) then	-- gate2 amp alarm
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(gate2_amp_alarm), ASCAN_AXIS_TDATA'length));		
						    elsif(var_ascan_counter = 35) then	-- gate2 tof alarm
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(gate2_tof_alarm), ASCAN_AXIS_TDATA'length));
							elsif(var_ascan_counter = 36) then	-- gate2 amp max	
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(signed(gate2_max), ASCAN_AXIS_TDATA'length));	
							elsif(var_ascan_counter = 37) then	-- gate2 amp max sample
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(gate2_max_sample), ASCAN_AXIS_TDATA'length));	
							elsif(var_ascan_counter = 38) then	-- gate2 amp min	
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(signed(gate2_min), ASCAN_AXIS_TDATA'length));	
							elsif(var_ascan_counter = 39) then	-- gate2 amp min sample
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(gate2_min_sample), ASCAN_AXIS_TDATA'length));	
							elsif(var_ascan_counter = 40) then	-- gate2 amp thr	
                                ASCAN_AXIS_TDATA <= std_logic_vector(resize(signed(gate2_thr), ASCAN_AXIS_TDATA'length));    
                            elsif(var_ascan_counter = 41) then  -- gate2 amp thr sample
                                ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(gate2_thr_sample), ASCAN_AXIS_TDATA'length));    
                            elsif(var_ascan_counter = 42) then  -- gate2 fft_max_index
                                ASCAN_AXIS_TDATA <= gate2_fft_max_sample;  
                            elsif(var_ascan_counter = 43) then  -- gate2 fft_max_re
                                --ASCAN_AXIS_TDATA <= gate2_fft_max_value(31 downto 0);  
                                ASCAN_AXIS_TDATA <= gate2_fft_cuadratic_error(31 downto 0);  
                            elsif(var_ascan_counter = 44) then  -- gate2 fft_max_im
                                --ASCAN_AXIS_TDATA <= gate2_fft_max_value(63 downto 32);  
                                ASCAN_AXIS_TDATA <= gate2_fft_cuadratic_error(63 downto 32);
                            elsif(var_ascan_counter = 45) then  -- gate2 cc_max_index
                                ASCAN_AXIS_TDATA <= gate2_cc_max_sample;  
                            elsif(var_ascan_counter = 46) then  -- gate2 cc_max_re
                                ASCAN_AXIS_TDATA <= gate2_cc_max_value(31 downto 0);  
                            elsif(var_ascan_counter = 47) then  -- gate2 cc_max_im
                                ASCAN_AXIS_TDATA <= gate2_cc_max_value(63 downto 32);   
                                							
							elsif(var_ascan_counter = 48) then	-- gate3 tof												 
								ASCAN_AXIS_TDATA <= gate3_tof;	
							elsif(var_ascan_counter = 49) then	-- gate3 tof avg
								ASCAN_AXIS_TDATA <= gate3_tof_averaged;	
							elsif(var_ascan_counter = 50) then	-- gate3 amp alarm
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(gate3_amp_alarm), ASCAN_AXIS_TDATA'length));	
							elsif(var_ascan_counter = 51) then	-- gate3 tof alarm
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(gate3_tof_alarm), ASCAN_AXIS_TDATA'length));
							elsif(var_ascan_counter = 52) then	-- gate3 amp max
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(signed(gate3_max), ASCAN_AXIS_TDATA'length));	
							elsif(var_ascan_counter = 53) then	-- gate3 amp max sample
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(gate3_max_sample), ASCAN_AXIS_TDATA'length));	
							elsif(var_ascan_counter = 54) then	-- gate3 amp min	
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(signed(gate3_min), ASCAN_AXIS_TDATA'length));	
							elsif(var_ascan_counter = 55) then	-- gate3 amp min sample
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(gate3_min_sample), ASCAN_AXIS_TDATA'length));
								
							elsif(var_ascan_counter = 64) then	-- encoder1 position 	
							    -- TEMP
							    if(unsigned(trigger_type) = TRIGGER_TYPE_NORMAL) then
							        ASCAN_AXIS_TDATA <= (others => '0');
							    else
                                    if(signed(encoder1_pos_result) >= 0 and signed(encoder1_pos_result) < signed(encoder1_trigger_counts_i)) then
                                        ASCAN_AXIS_TDATA <= (others => '0');
                                    elsif(signed(encoder1_pos_result) < 0 and signed(encoder1_pos_result) > signed(encoder1_trigger_counts_i)) then 	
                                        ASCAN_AXIS_TDATA <= (others => '0');
                                    else
                                        ASCAN_AXIS_TDATA <= encoder1_pos_result;
                                    end if;
                                end if;			
							    -- FINAL					 
								--ASCAN_AXIS_TDATA <= encoder1_pos_result;
							elsif(var_ascan_counter = 65) then	-- encoder1 direction 
                                ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(encoder1_dir_result), ASCAN_AXIS_TDATA'length));
                            elsif(var_ascan_counter = 66) then  -- encoder1 rpm 
                                ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(ENCODER1_RPM), ASCAN_AXIS_TDATA'length));
								
							elsif(var_ascan_counter = 67) then	-- encoder2 position
								ASCAN_AXIS_TDATA <= encoder2_pos_result;
							elsif(var_ascan_counter = 68) then	-- encoder2 direction
                                ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(encoder2_dir_result), ASCAN_AXIS_TDATA'length));
                            elsif(var_ascan_counter = 69) then  -- encoder2 rpm 
                                ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(ENCODER2_RPM), ASCAN_AXIS_TDATA'length));    
						
						    elsif(var_ascan_counter = 70) then	-- inputs			 																											 
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(IO_INPUTS), ASCAN_AXIS_TDATA'length));	
							elsif(var_ascan_counter = 71) then	-- outputs											
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(IO_OUTPUTS), ASCAN_AXIS_TDATA'length));	
							elsif(var_ascan_counter = 76) then
							    ASCAN_AXIS_TDATA <= (others => '0');	
                                -- Synqcronyze with FIR	
                                if(read_fir_result_type = '0') then                                            
                                    fir_result_axis_tready_x <= '1';
                                else
                                    dec_result_axis_tready_x <= '1';
                                end if;
						    else
						        ASCAN_AXIS_TDATA <= (others => '0');											 													 
							end if;
							
							if(var_ascan_counter >= 79) then													 
								var_ascan_counter:= 0;						
								state_read_ascan_sm <= upload_ascan_data_flank_down;													 
							else													 
								var_ascan_counter:= var_ascan_counter+1;		
							end if;														 
						end if;												 
					when upload_ascan_data_flank_down =>															 														
                        if(ASCAN_AXIS_TREADY = '1') then
                            ascan_counter <= ascan_counter + 1;
							ASCAN_AXIS_TVALID <= '1';													
                            if(tready_flank_up_flag = '1') then
                                ASCAN_AXIS_TDATA <= ascan_axis_tdata_2;
                                tready_flank_up_flag <= '0';
                            else
	                            if(read_fir_result_type = '0') then
                                    ASCAN_AXIS_TDATA <= fir_result_axis_tdata;
                                else
                                    ASCAN_AXIS_TDATA <= dec_result_axis_tdata; 
                                end if;
                            end if;
							if(var_ascan_counter >= to_integer(unsigned(data_window_half_result))-1) then
								ASCAN_AXIS_TLAST <= '1';
								adquisiton_provided <= '1';
								state_read_ascan_sm <= upload_ascan_end;												
							else
								var_ascan_counter := var_ascan_counter + 1;													
							end if;
                        else
                            if(ascan_axis_tready_i1 = '1') then
	                            if(read_fir_result_type = '0') then
                                    ascan_axis_tdata_1 <= fir_result_axis_tdata;
                                else
                                    ascan_axis_tdata_1 <= dec_result_axis_tdata; 
                                end if;
                                var_wait_counter:= 0;
                                state_read_ascan_sm <= upload_ascan_data_flank_up;
                            end if;
                        end if;																														
					when upload_ascan_data_flank_up =>																									
                        if(var_wait_counter = 0) then
	                        if(read_fir_result_type = '0') then
                                ascan_axis_tdata_2 <= fir_result_axis_tdata;
                            else
                                ascan_axis_tdata_2 <= dec_result_axis_tdata; 
                            end if;
                        end if;
                        if(var_wait_counter < 4) then
                            var_wait_counter := var_wait_counter + 1;
                        end if;
                        if(ASCAN_AXIS_TREADY = '1' and ascan_axis_tready_i1 = '0') then
                            ascan_counter <= ascan_counter + 1;
							ASCAN_AXIS_TVALID <= '1';													
                            if(var_wait_counter = 0) then
	                            if(read_fir_result_type = '0') then
                                    ASCAN_AXIS_TDATA <= fir_result_axis_tdata;
                                else
                                    ASCAN_AXIS_TDATA <= dec_result_axis_tdata; 
                                end if;
                            else
                                ASCAN_AXIS_TDATA <= ascan_axis_tdata_1;
                            end if;      
                            tready_flank_up_flag <= '1';
							if(var_ascan_counter >= to_integer(unsigned(data_window_half_result))-1) then
								ASCAN_AXIS_TLAST <= '1';
								adquisiton_provided <= '1';
								state_read_ascan_sm <= upload_ascan_end;												
							else
								var_ascan_counter := var_ascan_counter + 1;		
								state_read_ascan_sm <= upload_ascan_data_flank_down;													
							end if;														
                        end if;											
					when upload_ascan_end => 																 
						ASCAN_AXIS_TLAST <= '0';	
						ASCAN_AXIS_TVALID <= '0';
						ASCAN_AXIS_TKEEP <= (others => '0');
						read_fir_result <= '0';																					
						tready_flank_up_flag <= '0';													
						if(READ_ASCAN = '0' and read_ascan_flag = '0') then
						    read_ascan_end <= '0';
						    adquisiton_provided <= '0';			
							state_read_ascan_sm <= idle;	
							--state_read_ascan_sm <= acc_general_header;
						else
						    read_ascan_end <= '1';												
						end if;	
				    when acc_general_header =>                                                 
                        -- version
                        -- 4 bytes internal_number.major.minor.revision
                        -- 1.1.0.0
                        gates_acc(gates_offset+0)(7 downto 0)  <= std_logic_vector(to_unsigned(0, gates_acc(0)(7 downto 0)'length));
                        gates_acc(gates_offset+0)(15 downto 8) <= std_logic_vector(to_unsigned(0, gates_acc(0)(15 downto 8)'length));
                        gates_acc(gates_offset+0)(23 downto 16)<= std_logic_vector(to_unsigned(1, gates_acc(0)(23 downto 16)'length));
                        gates_acc(gates_offset+0)(31 downto 24)<= std_logic_vector(to_unsigned(1, gates_acc(0)(31 downto 24)'length));    
                        -- scan mode                                         
                        gates_acc(gates_offset+1) <= scan_mode;
                        -- trigger type
                        gates_acc(gates_offset+2) <= trigger_type;        
                        -- scan state
                        gates_acc(gates_offset+3)(0) <= start_inspection;
                        gates_acc(gates_offset+3)(1) <= start_stop_inspection;
                        gates_acc(gates_offset+3)(2) <= not PWM_END;
                        gates_acc(gates_offset+3)(3) <= max_overspeed_alarm_i;
                        gates_acc(gates_offset+3)(4) <= max_overspeed_alarm_x;
                        gates_acc(gates_offset+3)(5) <= max_overspeed_alarm_y;
                        gates_acc(gates_offset+3)(31 downto 6) <= (others => '0');
                        -- Data format (decimation)
                        -- 0 = normal by default
                        if(read_fir_result_type = '0') then
                            gates_acc(gates_offset+4) <= std_logic_vector(to_unsigned(DEC_TYPE_NORMAL, gates_acc(0) 'length));
                        else
                            gates_acc(gates_offset+4) <= std_logic_vector(to_unsigned(DEC_TYPE_CORDINATES, gates_acc(0) 'length));
                        end if;
                        -- Timestamp
                        gates_acc(gates_offset+5) <= time_stamp_counter(31 downto 0);
                        -- Timestamp
                        gates_acc(gates_offset+6) <= time_stamp_counter(63 downto 32);
                        -- Mru counter    
                        gates_acc(gates_offset+7) <= mrut_counter_result;    
                        -- Voltage status    
                        gates_acc(gates_offset+8) <= VOLTAGE_STATUS;  
                        gates_acc(gates_offset+9) <= (others => '0');    
                        gates_acc(gates_offset+10) <= (others => '0');   
                        gates_acc(gates_offset+11) <= (others => '0');   
                        gates_acc(gates_offset+12) <= (others => '0');   
                        gates_acc(gates_offset+13) <= (others => '0');   
                        -- number of channels                                                 
                        gates_acc(gates_offset+14) <= std_logic_vector(to_unsigned(1, gates_acc(0)'length));
                        -- channel offset (var_ascan_counter + 1)
                        gates_acc(gates_offset+15) <= std_logic_vector(to_unsigned(16, gates_acc(0)'length));
                        state_read_ascan_sm <= acc_ascan_header; 
                    when acc_ascan_header =>  
						-- channel_id
                        gates_acc(gates_offset+16) <= std_logic_vector(to_unsigned(CHANNEL_ID, gates_acc(0)'length));                                             
                        -- clock_rate                                             
                        gates_acc(gates_offset+17) <= fir_sample_freq_result;
                        -- prf
                        if(unsigned(trigger_type) = TRIGGER_TYPE_NORMAL) then
                            gates_acc(gates_offset+18) <= transmitter_prf_result;        
                        elsif((unsigned(trigger_type) = TRIGGER_TYPE_ENC) or
                              (unsigned(trigger_type) = TRIGGER_TYPE_SW) or
                              (unsigned(trigger_type) = TRIGGER_TYPE_SW_PWM) or
                              (unsigned(trigger_type) = TRIGGER_TYPE_ROBOT) or
                              (unsigned(trigger_type) = TRIGGER_TYPE_ROBOT_ENC) ) then    
                            gates_acc(gates_offset+18) <= transmitter_prf;                                         
                        else
                            gates_acc(gates_offset+18) <= prf_counter_i;        
                        end if;                                                                                                                              
                        -- ascan size  
                        if(read_fir_result_type = '0') then
                            gates_acc(gates_offset+19) <= fir_data_window_result;
                        else
                            gates_acc(gates_offset+19) <= dec_window_half_result;
                        end if;
                        -- Pulser Status
                        gates_acc(gates_offset+20)(3 downto 0) <= PULSER_STATUS;
                        gates_acc(gates_offset+20)(15 downto 4) <= (others => '0');
                        gates_acc(gates_offset+20)(31 downto 16) <= (others => '0');   
                        -- overspeed alarm
                        gates_acc(gates_offset+21)(0) <= max_overspeed_alarm_i;
                        gates_acc(gates_offset+21)(1) <= max_overspeed_alarm_x;
                        gates_acc(gates_offset+21)(2) <= max_overspeed_alarm_y;
                        gates_acc(gates_offset+21)(31 downto 3) <= (others => '0');
                        -- temperature    
                        gates_acc(gates_offset+22) <= TEMPERATURE_STATUS;
                        -- sensor type (EMAT)
                        gates_acc(gates_offset+23) <= std_logic_vector(to_unsigned(SENSOR_TYPE, gates_acc(0)'length));
                        gates_acc(gates_offset+24) <= (others => '0'); 
                        gates_acc(gates_offset+25) <= (others => '0'); 
                        gates_acc(gates_offset+26) <= (others => '0'); 
                        gates_acc(gates_offset+27) <= (others => '0'); 
                        gates_acc(gates_offset+28) <= (others => '0'); 
                        gates_acc(gates_offset+29) <= (others => '0'); 
                        gates_acc(gates_offset+30) <= (others => '0'); 
                        -- ascan offset
                        gates_acc(gates_offset+31) <= std_logic_vector(to_unsigned(ASCAN_HEADER_SIZE, gates_acc(0)'length));   
                        -- gate1 tof                                                 
                        gates_acc(gates_offset+32) <= gate1_tof;
                        -- gate1 tof avg    
                        gates_acc(gates_offset+33) <= gate1_tof_averaged;    
                        -- gate1 tof avg
                        gates_acc(gates_offset+34) <= std_logic_vector(resize(unsigned(gate1_amp_alarm), gates_acc(0)'length));    
                        -- gate1 tof alarm
                        gates_acc(gates_offset+35) <= std_logic_vector(resize(unsigned(gate1_tof_alarm), gates_acc(0)'length));    
                        -- gate1 amp max
                        gates_acc(gates_offset+36) <= std_logic_vector(resize(signed(gate1_max), gates_acc(0)'length));    
                        -- gate1 amp max sample
                        gates_acc(gates_offset+37) <= std_logic_vector(resize(unsigned(gate1_max_sample), gates_acc(0)'length));
                        -- gate1 amp min        
                        gates_acc(gates_offset+38) <= std_logic_vector(resize(signed(gate1_min), gates_acc(0)'length));    
                        -- gate1 amp min sample    
                        gates_acc(gates_offset+39) <= std_logic_vector(resize(unsigned(gate1_min_sample), gates_acc(0)'length));  
                        gates_acc(gates_offset+40) <= (others => '0'); 
                        gates_acc(gates_offset+41) <= (others => '0'); 
                        gates_acc(gates_offset+42) <= (others => '0'); 
                        gates_acc(gates_offset+43) <= (others => '0'); 
                        gates_acc(gates_offset+44) <= (others => '0'); 
                        gates_acc(gates_offset+45) <= (others => '0');  
                        gates_acc(gates_offset+46) <= (others => '0'); 
                        gates_acc(gates_offset+47) <= (others => '0'); 
                        -- gate2 tof                                                     
                        gates_acc(gates_offset+48) <=  gate2_tof;    
                        -- gate2 tof avg
                        gates_acc(gates_offset+49) <= gate2_tof_averaged;    
                        -- gate2 amp alarm
                        gates_acc(gates_offset+50) <= std_logic_vector(resize(unsigned(gate2_amp_alarm), gates_acc(0)'length));        
                        -- gate2 tof alarm
                        gates_acc(gates_offset+51) <= std_logic_vector(resize(unsigned(gate2_tof_alarm), gates_acc(0)'length));
                        -- gate2 amp max    
                        gates_acc(gates_offset+52) <= std_logic_vector(resize(signed(gate2_max), gates_acc(0)'length));    
                        -- gate2 amp max sample
                        gates_acc(gates_offset+53) <= std_logic_vector(resize(unsigned(gate2_max_sample), gates_acc(0)'length));    
                        -- gate2 amp min    
                        gates_acc(gates_offset+54) <= std_logic_vector(resize(signed(gate2_min), gates_acc(0)'length));    
                        -- gate2 amp min sample
                        gates_acc(gates_offset+55) <= std_logic_vector(resize(unsigned(gate2_min_sample), gates_acc(0)'length));    
                        -- gate2 amp thr    
                        gates_acc(gates_offset+56) <= std_logic_vector(resize(signed(gate2_thr), gates_acc(0)'length));    
                        -- gate2 amp thr sample
                        gates_acc(gates_offset+57) <= std_logic_vector(resize(unsigned(gate2_thr_sample), gates_acc(0)'length));    
                        -- gate2 fft_max_index
                        gates_acc(gates_offset+58) <= gate2_fft_max_sample;  
                        -- gate2 fft_max_re
                        gates_acc(gates_offset+59) <= gate2_fft_cuadratic_error(31 downto 0);  
                        -- gate2 fft_max_im 
                        gates_acc(gates_offset+60) <= gate2_fft_cuadratic_error(63 downto 32);
                        -- gate2 cc_max_index
                        gates_acc(gates_offset+61) <= gate2_cc_max_sample;  
                        -- gate2 cc_max_re
                        gates_acc(gates_offset+62) <= gate2_cc_max_value(31 downto 0);  
                        -- gate2 cc_max_im
                        gates_acc(gates_offset+63) <= gate2_cc_max_value(63 downto 32);   
                                                        
                        -- gate3 tof                                                 
                        gates_acc(gates_offset+64) <= gate3_tof;    
                        -- gate3 tof avg
                        gates_acc(gates_offset+65) <= gate3_tof_averaged;    
                        -- gate3 amp alarm
                        gates_acc(gates_offset+66) <= std_logic_vector(resize(unsigned(gate3_amp_alarm), gates_acc(0)'length));    
                        -- gate3 tof alarm
                        gates_acc(gates_offset+67) <= std_logic_vector(resize(unsigned(gate3_tof_alarm), gates_acc(0)'length));
                        -- gate3 amp max
                        gates_acc(gates_offset+68) <= std_logic_vector(resize(signed(gate3_max), gates_acc(0)'length));    
                        -- gate3 amp max sample
                        gates_acc(gates_offset+69) <= std_logic_vector(resize(unsigned(gate3_max_sample), gates_acc(0)'length));    
                        -- gate3 amp min    
                        gates_acc(gates_offset+70) <= std_logic_vector(resize(signed(gate3_min), gates_acc(0)'length));    
                        -- gate3 amp min sample
                        gates_acc(gates_offset+71) <= std_logic_vector(resize(unsigned(gate3_min_sample), gates_acc(0)'length));
                        gates_acc(gates_offset+72) <= (others => '0'); 
                        gates_acc(gates_offset+73) <= (others => '0'); 
                        gates_acc(gates_offset+74) <= (others => '0'); 
                        gates_acc(gates_offset+75) <= (others => '0'); 
                        gates_acc(gates_offset+76) <= (others => '0'); 
                        gates_acc(gates_offset+77) <= (others => '0');  
                        gates_acc(gates_offset+78) <= (others => '0'); 
                        gates_acc(gates_offset+79) <= (others => '0'); 
                            
                        -- encoder1 position     
                        if(unsigned(trigger_type) = TRIGGER_TYPE_NORMAL) then
                            gates_acc(gates_offset+80) <= (others => '0');
                        else
                            if(signed(encoder1_pos_result) >= 0 and signed(encoder1_pos_result) < signed(encoder1_trigger_counts_i)) then
                                gates_acc(gates_offset+80) <= (others => '0');
                            elsif(signed(encoder1_pos_result) < 0 and signed(encoder1_pos_result) > signed(encoder1_trigger_counts_i)) then     
                                gates_acc(gates_offset+80) <= (others => '0');
                            else
                                gates_acc(gates_offset+80) <= encoder1_pos_result;
                            end if;
                        end if;            
                        -- encoder1 direction 
                        gates_acc(gates_offset+81) <= std_logic_vector(resize(unsigned(encoder1_dir_result), gates_acc(0)'length));
                        -- encoder1 rpm 
                        gates_acc(gates_offset+82) <= std_logic_vector(resize(unsigned(ENCODER1_RPM), gates_acc(0)'length));    
                        -- encoder2 position
                        gates_acc(gates_offset+83) <= encoder2_pos_result;
                        -- encoder2 direction
                        gates_acc(gates_offset+84) <= std_logic_vector(resize(unsigned(encoder2_dir_result), gates_acc(0)'length));
                        -- encoder2 rpm 
                        gates_acc(gates_offset+85) <= std_logic_vector(resize(unsigned(ENCODER2_RPM), gates_acc(0)'length));    
                        -- inputs                                                                                                                          
                        gates_acc(gates_offset+86) <= std_logic_vector(resize(unsigned(IO_INPUTS), gates_acc(0)'length));    
                        -- outputs                                            
                        gates_acc(gates_offset+87) <= std_logic_vector(resize(unsigned(IO_OUTPUTS), gates_acc(0)'length));  
                        gates_acc(gates_offset+88) <= (others => '0'); 
                        gates_acc(gates_offset+89) <= (others => '0');  
                        gates_acc(gates_offset+90) <= (others => '0'); 
                        gates_acc(gates_offset+91) <= (others => '0'); 
                        gates_acc(gates_offset+92) <= (others => '0');   
                        gates_acc(gates_offset+93) <= (others => '0');
                        gates_acc(gates_offset+94) <= (others => '0');
                        gates_acc(gates_offset+95) <= (others => '0');   
                        gates_offset <= gates_offset + ASCAN_HEADER_SIZE;    
                        state_read_ascan_sm <= idle;                                                											
					when others =>
						null;
				end case;
			end if;
         end if;
    end process;
    
    compression_reset <= compression_reset_i or ga_reset;
    
	-- PRF CALCULATOR

    prf_calculator_inst: prf_calculator 
        generic map(
            REGISTER_W32  => REGISTER_W32,
            REGISTER_W16  => REGISTER_W16,
            AVG_LEVEL_MIN => 2,
            AVG_LEVEL_MAX => 32
        )
        port map(
            -- Sync
            aclk         => aclk,
            aresetn      => aresetn,
            
            -- Control
            START_INSPECTION          => start_inspection,
            ACQUISITION_PROVIDED      => adquisiton_provided,
            TRANSMITTER_PRF           => transmitter_prf_calculator,
            AVG_LEVEL                 => prf_calculator_avg,
            
            -- Output
            PRF_RESULT                => transmitter_prf_result
        );
        
    prf_calculator_avg <= std_logic_vector(to_unsigned(1, prf_calculator_avg'length));
    
    -- TIME STAMP
       
    time_stamp_process: process(aclk) 
        variable var_counter : integer range 0 to 8192 :=0;                                                                         
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                var_counter := 0;
                time_stamp_counter <= (others => '0');
            else
                if(time_stamp_reset = '0') then
                    if(var_counter >= 99) then
                        var_counter := 0;  
                        time_stamp_counter <= std_logic_vector(unsigned(time_stamp_counter)+1);                                         
                    else
                        var_counter := var_counter + 1;                    
                    end if;    
                else
                    var_counter := 0;
                    time_stamp_counter <= (others => '0');                        
                end if;          
            end if;
        end if;
    end process;
    
    -- COINCIDENCE / AVERAGE POSITION
       
    coinc_avg_process: process(aclk) 
        variable var_counter : integer range 0 to 8192 :=0;                                                                         
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then

                signal_output_axis_tready_i <= '0';
                coinc_result_axis_tready <= '0';
                avg_result_axis_tready <= '0';
                dsp_result_axis_tready <= '0';

                coinc_signal_input_axis_tvalid <= '0';
                coinc_signal_input_axis_tlast <= '0';
                coinc_signal_input_axis_tdata <= (others => '0');
                
                avg_signal_input_axis_tvalid <= '0';
                avg_signal_input_axis_tlast <= '0';
                avg_signal_input_axis_tdata <= (others => '0');
                
                dsp_signal_input_axis_tvalid <= '0';
                dsp_signal_input_axis_tlast <= '0';
                dsp_signal_input_axis_tdata <= (others => '0');
                
                fir_signal_input_axis_tvalid <= '0';
                fir_signal_input_axis_tlast <= '0';
                fir_signal_input_axis_tdata <= (others => '0');
                
                avg_coinc_pos <= '0';
                
                coinc_fir_on <= '0';
                avg_fir_on <= '0';
            else
                if(unsigned(transmitter_n_burst_i) >= MIN_COINC_LEVEL or dsp_avg_coinc_order = '1') then
                    avg_coinc_pos <= '0';
                
                    signal_output_axis_tready_i <= coinc_signal_input_axis_tready;
                    
                    coinc_signal_input_axis_tvalid <= signal_output_axis_tvalid_i;
                    coinc_signal_input_axis_tlast <= signal_output_axis_tlast_i;
                    coinc_signal_input_axis_tdata <= signal_output_axis_tdata_i;
                    
                    coinc_result_axis_tready <= avg_signal_input_axis_tready;
                    
                    avg_signal_input_axis_tvalid <= coinc_result_axis_tvalid;
                    avg_signal_input_axis_tlast <= coinc_result_axis_tlast;
                    avg_signal_input_axis_tdata <= coinc_result_axis_tdata;
                    
                    coinc_fir_on <= '0';
                    avg_fir_on <= dsp_fir_delay_on;
                    
                    avg_result_axis_tready <= dsp_signal_input_axis_tready;
                    
                    dsp_signal_input_axis_tvalid <= avg_result_axis_tvalid;
                    dsp_signal_input_axis_tlast <= avg_result_axis_tlast;
                    dsp_signal_input_axis_tdata <= avg_result_axis_tdata;
                else
                    avg_coinc_pos <= '1';
                
                    signal_output_axis_tready_i <= avg_signal_input_axis_tready;
                
                    avg_signal_input_axis_tvalid <= signal_output_axis_tvalid_i;
                    avg_signal_input_axis_tlast <= signal_output_axis_tlast_i;
                    avg_signal_input_axis_tdata <= signal_output_axis_tdata_i;
                    
                    avg_fir_on <= '0';
                    coinc_fir_on <= dsp_fir_delay_on;
                    
                    avg_result_axis_tready <= coinc_signal_input_axis_tready;
                    
                    coinc_signal_input_axis_tvalid <= avg_result_axis_tvalid;
                    coinc_signal_input_axis_tlast <= avg_result_axis_tlast;
                    coinc_signal_input_axis_tdata <= avg_result_axis_tdata;
                    
                    coinc_result_axis_tready <= dsp_signal_input_axis_tready;
                    
                    dsp_signal_input_axis_tvalid <= coinc_result_axis_tvalid;
                    dsp_signal_input_axis_tlast <= coinc_result_axis_tlast;
                    dsp_signal_input_axis_tdata <= coinc_result_axis_tdata;
                end if;
                dsp_result_axis_tready <= fir_signal_input_axis_tready_i;
                fir_signal_input_axis_tvalid <= dsp_result_axis_tvalid;
                fir_signal_input_axis_tlast <= dsp_result_axis_tlast;
                fir_signal_input_axis_tdata <= dsp_result_axis_tdata;
            end if;
        end if;
    end process;
          																		
	-- DIGITAL AMPLIFIER																		
	digital_amplifier_inst: digital_amplifier
	generic map(
		REGISTER_W48        => REGISTER_W48,
        REGISTER_W32        => REGISTER_W32,
        REGISTER_W16        => REGISTER_W16,
		DIVIDER_FACTOR      => 20,
		MULTIPLIER_DELAY    => 4
    )
    port map(
        aclk         => aclk,
        aresetn      => aresetn,
		
		-- Gain
		DIGITAL_AMPLIFIER_GAIN	=> receiver_digital_gain_i,
		
		-- Signal in
		SIGNAL_INPUT_AXIS_TREADY 	=> digital_amplifier_tready,
		SIGNAL_INPUT_AXIS_TDATA 	=> SIGNAL_INPUT_AXIS_TDATA,
		SIGNAL_INPUT_AXIS_TLAST		=> SIGNAL_INPUT_AXIS_TLAST,
		SIGNAL_INPUT_AXIS_TVALID  	=> SIGNAL_INPUT_AXIS_TVALID,

		-- Signal out
		SIGNAL_OUTPUT_AXIS_TREADY 	=> signal_output_axis_tready_i,
		SIGNAL_OUTPUT_AXIS_TDATA	=> signal_output_axis_tdata_i,
		SIGNAL_OUTPUT_AXIS_TLAST	=> signal_output_axis_tlast_i,
		SIGNAL_OUTPUT_AXIS_TVALID   => signal_output_axis_tvalid_i		
    );
																				 			 						
	-- COINCIDENCE FILTER
    coincidence_filter_inst: coincidence_filter
    generic map(
        REGISTER_W32    => REGISTER_W32,
        REGISTER_W16    => REGISTER_W16,
        REGISTER_W8     => REGISTER_W8,
        COINC_TYPE_NORMAL       => COINC_TYPE_NORMAL,
        COINC_TYPE_PIPELINE     => COINC_TYPE_PIPELINE,
        COINC_TYPE_PIPELINE_ACC => COINC_TYPE_PIPELINE_ACC,
        COINC_TYPE_ACC          => COINC_TYPE_ACC,
        TRIGGER_WAIT    => TRIGGER_WAIT,
        MIN_COINC_LEVEL => MIN_COINC_LEVEL
    )
    port map(
        -- Sync
        aclk         => aclk,
        aresetn      => aresetn,
    
        -- Config
        COINC_LEVEL        => dsp_noise_reduction_filter,
        COINC_RESET     => coinc_reset,
        COINC_RESET_ACK => coinc_reset_ack,
        COINC_END       => open,
        
        COINC_TYPE => coinc_type,
        COINC_ACC  => coinc_acc,
        
        PHASE_SHIFT_LEVEL     => dsp_phase_shift,
        PHASE_SHIFT           => transmitter_phase_shift,
        N_BURST               => transmitter_n_burst_i,
        
        DATA_WINDOW        => RECEIVER_DATA_WINDOW_i,
        SAMPLE_FREQ     => receiver_sampling_frequency_i,
        
        DATA_WINDOW_RESULT    => coinc_data_window_result,
        SAMPLE_FREQ_RESULT  => coinc_sample_freq_result,
        COINC_TYPE_RESULT   => coinc_type_result,
        COINC_LEVEL_RESULT  => coinc_level_result, 
        COINC_COUNTER   => signal_coinc_counter_i,
        COINC_WINDOW    => SIGNAL_COINC_WINDOW, 
        
        FIR_DELAY       => dsp_fir_delay_result,
        FIR_DELAY_ON    => coinc_fir_on,

        -- Signal input
        SIGNAL_INPUT_AXIS_TREADY  => coinc_signal_input_axis_tready,
        SIGNAL_INPUT_AXIS_TDATA      => coinc_signal_input_axis_tdata,
        SIGNAL_INPUT_AXIS_TLAST      => coinc_signal_input_axis_tlast,
        SIGNAL_INPUT_AXIS_TVALID  => coinc_signal_input_axis_tvalid,

        -- Coincidence result
        COINC_RESULT_AXIS_TVALID  => coinc_result_axis_tvalid,
        COINC_RESULT_AXIS_TDATA      => coinc_result_axis_tdata,
        COINC_RESULT_AXIS_TLAST      => coinc_result_axis_tlast,
        COINC_RESULT_AXIS_TREADY  => coinc_result_axis_tready
    );
		
	SIGNAL_COINC_COUNTER <= signal_coinc_counter_i;
	
	DSP_COINC_LEVEL <= coinc_level_result(REGISTER_W16-1 downto 0);
	TRANSMITTER_N_BURST <= transmitter_n_burst_i;																			
																																							
	-- AVERAGE FILTER
	average_filter_inst: average_filter
	generic map(
        REGISTER_W32    => REGISTER_W32, 
        REGISTER_W16    => REGISTER_W16,
        REGISTER_W8     => REGISTER_W8,
        REGISTER_W14    => REGISTER_W14,
        BRAM_ADDR_WIDTH => BRAM_ADDR_WIDTH,
        BRAM_DATA_WIDTH => BRAM_DATA_WIDTH,       		
		AVG_TYPE_NORMAL => AVG_TYPE_NORMAL,
		AVG_TYPE_PIPELINE  => AVG_TYPE_PIPELINE,
		AVG_TYPE_PIPELINE_ACC => AVG_TYPE_PIPELINE_ACC,
		AVG_TYPE_ACC    => AVG_TYPE_ACC,
        TRIGGER_WAIT    => TRIGGER_WAIT,
        MIN_AVG_LEVEL   => MIN_AVG_LEVEL
	)
	port map(
	    -- Sync
	    aclk         => aclk,
        aresetn      => aresetn,
	
	    -- Config
	    AVG_LEVEL	    	=> dsp_average,
	    DATA_WINDOW	    	=> coinc_data_window_result,
	    SAMPLE_FREQ         => coinc_sample_freq_result,
		DATA_WINDOW_RESULT	=> avg_data_window_result,
		SAMPLE_FREQ_RESULT  => avg_sample_freq_result,
		AVG_COUNTER         => avg_counter, 
		AVG_WINDOW			=> SIGNAL_AVG_WINDOW, 
		
		AVG_TYPE			=> avg_type,
		AVG_ACC             => avg_acc,
		AVG_END             => avg_end,
		
		AVG_TYPE_RESULT		=> avg_type_result,
		AVG_LEVEL_RESULT    => avg_level_result,
		AVG_RESET			=> avg_reset, 
		AVG_RESET_ACK		=> avg_reset_ack, 
		
		FIR_DELAY           => dsp_fir_delay_result,
		FIR_DELAY_ON        => avg_fir_on,
		
		-- Signal input
		SIGNAL_INPUT_AXIS_TREADY  => avg_signal_input_axis_tready,
		SIGNAL_INPUT_AXIS_TDATA	  => avg_signal_input_axis_tdata,
		SIGNAL_INPUT_AXIS_TLAST	  => avg_signal_input_axis_tlast,
		SIGNAL_INPUT_AXIS_TVALID  => avg_signal_input_axis_tvalid,

		-- Average result
		AVG_RESULT_AXIS_TVALID    => avg_result_axis_tvalid,
		AVG_RESULT_AXIS_TDATA	  => avg_result_axis_tdata,
		AVG_RESULT_AXIS_TLAST	  => avg_result_axis_tlast,
		AVG_RESULT_AXIS_TREADY    => avg_result_axis_tready
	);
																
	DSP_AVG_LEVEL <= avg_level_result(REGISTER_W16-1 downto 0);																			
	SIGNAL_AVG_COUNTER <= avg_counter;
	DSP_AVG_TYPE <= avg_type_result;
	
    avg_reset_process: process(aclk)
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                avg_reset <= '0';
                coinc_reset <= '0';
                time_stamp_reset <= '1';
                configure_control_reset <= '0';
                start_inspection_d1 <= '0';
                start_stop_inspection_d1 <= '0';
                scan_mode_d1 <= (others => '0');
                trigger_type_d1 <= (others => '0');
            else
                start_inspection_d1 <= start_inspection;
                start_stop_inspection_d1 <= start_stop_inspection;
                scan_mode_d1 <= scan_mode;
                trigger_type_d1 <= trigger_type;
                if((start_inspection /= start_inspection_d1) or 
                   (start_stop_inspection /= start_stop_inspection_d1) or
                   (scan_mode /= scan_mode_d1) or
                   (trigger_type /= trigger_type_d1)                
                  ) then
                    avg_reset <= '1';
                    coinc_reset <= '1';
                    time_stamp_reset <= '1';                   
                else
                    if(avg_reset = avg_reset_ack) then
                        avg_reset <= '0';    
                    end if;
                    if(coinc_reset = coinc_reset_ack) then
                        coinc_reset <= '0';   
                    end if;
                    time_stamp_reset <= '0';
                end if; 
                if((start_inspection /= start_inspection_control_d1) or 
                   (start_stop_inspection /= start_stop_inspection_control_d1) or
                   (scan_mode /= scan_mode_control_d1) or
                   (trigger_type /= trigger_type_control_d1)                
                  ) then
                    configure_control_reset <= '1';
                else
                    if(configure_control_reset = configure_control_reset_ack) then
                        configure_control_reset <= '0';   
                    end if;
                end if;
            end if;
        end if;
    end process;
    
    dsp_filter_inst: dsp_filter 
    generic map(
        REGISTER_W16        => REGISTER_W16,
        REGISTER_W2         => REGISTER_W2,
        RECTIFICATION_DISABLED    => RECTIFICATION_DISABLED,
        RECTIFICATION_MED   => RECTIFICATION_MED,
        RECTIFICATION_FULL  => RECTIFICATION_FULL
    )
    port map(
            -- Sync
        aclk         => aclk,
        aresetn      => aresetn,
        
            -- Control
        DSP_INVERSION              => dsp_inversion,
        DSP_RECTIFICATION          => dsp_rectification,
                
        -- Signal input
        SIGNAL_INPUT_AXIS_TREADY  => dsp_signal_input_axis_tready,
        SIGNAL_INPUT_AXIS_TDATA   => dsp_signal_input_axis_tdata,
        SIGNAL_INPUT_AXIS_TLAST   => dsp_signal_input_axis_tlast,
        SIGNAL_INPUT_AXIS_TVALID  => dsp_signal_input_axis_tvalid,
    
        -- Dsp result
        DSP_RESULT_AXIS_TVALID    => dsp_result_axis_tvalid,
        DSP_RESULT_AXIS_TDATA     => dsp_result_axis_tdata,
        DSP_RESULT_AXIS_TLAST     => dsp_result_axis_tlast,
        DSP_RESULT_AXIS_TREADY    => dsp_result_axis_tready
    );
												
		
	-- FIR FILTER + CORRELTION
	fir_correlation_inst: fir_correlation
	generic map(
	    REGISTER_W48        => REGISTER_W48,
        REGISTER_W32        => REGISTER_W32,
        REGISTER_W16        => REGISTER_W16,
        TAP_BRAM_ADDR_WIDTH => 11,
        N_TAPS              => N_TAPS,
        N_SETS_TAP          => N_SETS_TAP,
        OVERSAMPLING        => OVERSAMPLING,
        RECTIFICATION_DISABLED	=> RECTIFICATION_DISABLED,
        RECTIFICATION_MED   => RECTIFICATION_MED,
        RECTIFICATION_FULL  => RECTIFICATION_FULL,
        WAIT_CYCLES         => WAIT_CYCLES
    )
    port map(
        aclk         => aclk,
        aresetn      => aresetn,
        
		NO_READ_FIR_RESULT			=> no_read_fir_result,
        READ_FIR_RESULT             => read_fir_result,
        READ_DEC_RESULT             => read_fir_dec_result,
        READ_FIR_RESULT_TYPE        => read_fir_result_type,
        DATA_WINDOW                 => avg_data_window_result,
        SAMPLE_FREQ                 => avg_sample_freq_result,
        DATA_WINDOW_RESULT          => fir_data_window_result,
        SAMPLE_FREQ_RESULT          => fir_sample_freq_result,
		DATA_WINDOW_HALF_RESULT	    => fir_data_window_half_result,
		FIR_END                     => fir_end,
        
		-- FIR config
        BAND_PASS_FILTER_EN         => dsp_bandpass_en,
        BAND_PASS_FILTER_L_DIVIDER  => dsp_bandpass_l,
        GAUSSIAN_FILTER_EN          => dsp_gaussian_en,
        GAUSSIAN_FILTER_L_DIVIDER   => dsp_gaussian_l,
        GAUSSIAN_FILTER_DELAY       => dsp_gaussian_delay,
        ENVELOPE_FILTER_EN          => dsp_envelope_en,
        ENVELOPE_FILTER_L_DIVIDER   => dsp_envelope_l,
        FIR_DELAY                   => dsp_fir_delay,
        FIR_DELAY_ON                => dsp_fir_delay_on,
        FIR_DELAY_RESULT            => dsp_fir_delay_result,
        FIR_DELAY_STATE             => dsp_fir_delay_state,
        DSP_RECTIFICATION           => dsp_rectification,
        
        FIR_TAPS_AXIS_TREADY        => fir_taps_axis_tready,
        FIR_TAPS_AXIS_TDATA         => config_fir_axis_tdata,
        FIR_TAPS_AXIS_TLAST         => config_fir_axis_tlast,
        FIR_TAPS_AXIS_TVALID        => config_fir_axis_tvalid,
     
		-- Signal Input
        SIGNAL_INPUT_AXIS_TREADY    => fir_signal_input_axis_tready,
        SIGNAL_INPUT_AXIS_TDATA     => fir_signal_input_axis_tdata,
        SIGNAL_INPUT_AXIS_TLAST     => fir_signal_input_axis_tlast,
        SIGNAL_INPUT_AXIS_TVALID    => fir_signal_input_axis_tvalid,
        
		-- Gates Config
        GATES_EN                    => gates_en,
        GATES_SAMPLE_START          => gates_sample_start,
        GATES_SAMPLE_END            => gates_sample_end,
        
		-- Gates Result
        GATES_AXIS_TVALID           => gates_axis_tvalid,
        GATES_AXIS_TDATA            => gates_axis_tdata,
        GATES_AXIS_TLAST            => gates_axis_tlast,
        GATES_AXIS_TREADY           => gates_axis_tready,
        
		-- Correlation Config
        CORRELATION_EN                  => correlation_en,
        CORRELATION_GATE_SAMPLE_START   => correlation_gate_sample_start,
        CORRELATION_GATE_SAMPLE_END     => correlation_gate_sample_end,
        
		-- Correlation Result
        CORRELATION_AXIS_TVALID     => correlation_axis_tvalid,
        CORRELATION_AXIS_TDATA      => correlation_axis_tdata,
        CORRELATION_AXIS_TLAST      => correlation_axis_tlast,
        CORRELATION_AXIS_TREADY     => correlation_axis_tready,
        
        -- FIR Result for decimation
        DEC_AXIS_TVALID      => dec_axis_tvalid,
        DEC_AXIS_TDATA       => dec_axis_tdata,
        DEC_AXIS_TMAX        => dec_axis_tmax,
        DEC_AXIS_TMIN        => dec_axis_tmin,
        DEC_AXIS_TACC        => dec_axis_tacc,
        DEC_AXIS_TLAST       => dec_axis_tlast,
        DEC_AXIS_TREADY      => dec_axis_tready,
        
		-- FIR Result
        FIR_RESULT_AXIS_TVALID      => fir_result_axis_tvalid,
        FIR_RESULT_AXIS_TDATA       => fir_result_axis_tdata,
        FIR_RESULT_AXIS_TLAST       => fir_result_axis_tlast,
        FIR_RESULT_AXIS_TREADY      => fir_result_axis_tready_i
    );
    
    -- Debug
    correlation_counter_inst: process(aclk) 
        variable var_counter : integer range 0 to 65536 :=0;
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                correlation_counter <= (others => '0'); 
            else
                if(CORRELATION_AXIS_TVALID = '1') then 
                    if(CORRELATION_AXIS_TLAST = '1') then 
                        correlation_counter <= (others => '0'); 
                    else
                        correlation_counter <= correlation_counter + 1;
                    end if;
                end if;
            end if;
        end if;
    end process;
    
    -- Debug
    tx_delay_process: process(aclk) 
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                correlation_counter <= (others => '0'); 
            else
                if(CORRELATION_AXIS_TVALID = '1') then 
                    if(CORRELATION_AXIS_TLAST = '1') then 
                        correlation_counter <= (others => '0'); 
                    else
                        correlation_counter <= correlation_counter + 1;
                    end if;
                end if;
            end if;
        end if;
    end process;
	
	-- GATES PROCESSOR
	gates_processor_inst: gates_processor 
	generic map(
		REGISTER_W64  => REGISTER_W64,
		REGISTER_W48  => REGISTER_W48,
        REGISTER_W32  => REGISTER_W32,
        REGISTER_W16  => REGISTER_W16,
        REGISTER_W8   => REGISTER_W8,
        REGISTER_W4   => REGISTER_W4,
        REGISTER_W2   => REGISTER_W2
	)
	port map(
	    --sync
	    aclk           => aclk,
        aresetn        => aresetn,  
	    
	    -- GENERAL    
--	    RX_DELAY       => receiver_delay_i,  
	    RX_DELAY       => receiver_delay_gate, 
	    TX_DELAY       => transmitter_delay_gate, 

        -- GATE1
		GATE1_EN                          => gate1_en,  
		GATE1_START                       => gate1_start,
		GATE1_WIDTH                       => gate1_width,
        GATE1_AMP_THRESHOLD               => gate1_amp_threshold,
        GATE1_AMP_THRESHOLD_ALARM_EN      => gate1_amp_threshold_alarm_en,
        GATE1_AMP_THRESHOLD_ALARM_CROSSING=> gate1_amp_threshold_alarm_crossing,
        GATE1_TOF_ALGORITHM               => gate1_tof_algorithm,
        GATE1_TOF_AVG                     => gate1_tof_avg,
        GATE1_TOF_MIN_THICKNESS           => gate1_tof_min_thickness,
        GATE1_TOF_ALARM_EN                => gate1_tof_alarm_en,
        GATE1_TOF_ALARM_TYPE              => gate1_tof_alarm_type,
        GATE1_TOF_ALARM_CROSSING          => gate1_tof_alarm_crossing,
        GATE1_TOF_ALARM_MIN_VALUE         => gate1_tof_alarm_min_value,
        GATE1_TOF_ALARM_MAX_VALUE         => gate1_tof_alarm_max_value,
        GATE1_TOF                         => gate1_tof,
        GATE1_TOF_AVERAGED                => gate1_tof_averaged,
        GATE1_AMP_ALARM                   => gate1_amp_alarm,
        GATE1_TOF_ALARM                   => gate1_tof_alarm,
        GATE1_MAX                         => gate1_max,
        GATE1_MAX_SAMPLE                  => gate1_max_sample,
        GATE1_MIN                         => gate1_min,
        GATE1_MIN_SAMPLE                  => gate1_min_sample,
        
        -- GATE2
		GATE2_EN                          => gate2_en,  
        GATE2_START                       => gate2_start,
        GATE2_WIDTH                       => gate2_width,
        GATE2_AMP_THRESHOLD               => gate2_amp_threshold,
        GATE2_AMP_THRESHOLD_ALARM_EN      => gate2_amp_threshold_alarm_en,
        GATE2_AMP_THRESHOLD_ALARM_CROSSING=> gate2_amp_threshold_alarm_crossing,
        GATE2_TOF_ALGORITHM               => gate2_tof_algorithm,
        GATE2_TOF_AVG                     => gate2_tof_avg,
        GATE2_TOF_MIN_THICKNESS           => gate2_tof_min_thickness,
        GATE2_TOF_ALARM_EN                => gate2_tof_alarm_en,
        GATE2_TOF_ALARM_TYPE              => gate2_tof_alarm_type,
        GATE2_TOF_ALARM_CROSSING          => gate2_tof_alarm_crossing,
        GATE2_TOF_ALARM_MIN_VALUE         => gate2_tof_alarm_min_value,
        GATE2_TOF_ALARM_MAX_VALUE         => gate2_tof_alarm_max_value,
        GATE2_TOF                         => gate2_tof,
        GATE2_TOF_AVERAGED                => gate2_tof_averaged,
        GATE2_AMP_ALARM                   => gate2_amp_alarm,
        GATE2_TOF_ALARM                   => gate2_tof_alarm,
        GATE2_MAX                         => gate2_max,
        GATE2_MAX_SAMPLE                  => gate2_max_sample,
        GATE2_MIN                         => gate2_min,
        GATE2_MIN_SAMPLE                  => gate2_min_sample,
        GATE2_THR                         => gate2_thr,
        GATE2_THR_SAMPLE                  => gate2_thr_sample,
        GATE2_FFT_EN                      => gate2_fft_en,
        GATE2_CC_EN                       => gate2_cc_en,
        GATE2_FFT_SEGMENT                 => gate2_fft_segment,
        GATE2_FFT_NFFT                    => gate2_fft_nfft,
        GATE2_FFT_WIDTH                   => gate2_width,
        GATE2_FFT_MAX_VALUE               => gate2_fft_max_value,
        GATE2_FFT_MAX_SAMPLE              => gate2_fft_max_sample,
        GATE2_CC_MAX_VALUE                => gate2_cc_max_value,
        GATE2_CC_MAX_SAMPLE               => gate2_cc_max_sample,
        GATE2_FFT_CUADRATIC_ERROR         => gate2_fft_cuadratic_error,
        GATE2_CC_CONFIG_ON_ACK            => CC_CONFIG_ON_ACK,
        GATE2_CC_CONFIG_ON                => CC_CONFIG_ON,      
        GATE2_CC_CONFIG_ADDR              => CC_CONFIG_ADDR,
        GATE2_CC_CONFIG_DATA              => CC_CONFIG_DATA,
        
        -- GATE3
		GATE3_EN                          => gate3_en,  
        GATE3_START                       => gate3_start,
        GATE3_WIDTH                       => gate3_width,
        GATE3_AMP_THRESHOLD               => gate3_amp_threshold,
        GATE3_AMP_THRESHOLD_ALARM_EN      => gate3_amp_threshold_alarm_en,
        GATE3_AMP_THRESHOLD_ALARM_CROSSING=> gate3_amp_threshold_alarm_crossing,
        GATE3_TOF_ALGORITHM               => gate3_tof_algorithm,
        GATE3_TOF_AVG                     => gate3_tof_avg,
        GATE3_TOF_MIN_THICKNESS           => gate3_tof_min_thickness,
        GATE3_TOF_ALARM_EN                => gate3_tof_alarm_en,
        GATE3_TOF_ALARM_TYPE              => gate3_tof_alarm_type,
        GATE3_TOF_ALARM_CROSSING          => gate3_tof_alarm_crossing,
        GATE3_TOF_ALARM_MIN_VALUE         => gate3_tof_alarm_min_value,
        GATE3_TOF_ALARM_MAX_VALUE         => gate3_tof_alarm_max_value,
        GATE3_TOF                         => gate3_tof,
        GATE3_TOF_AVERAGED                => gate3_tof_averaged,
        GATE3_AMP_ALARM                   => gate3_amp_alarm,
        GATE3_TOF_ALARM                   => gate3_tof_alarm,
        GATE3_MAX                         => gate3_max,
        GATE3_MAX_SAMPLE                  => gate3_max_sample,
        GATE3_MIN                         => gate3_min,
        GATE3_MIN_SAMPLE                  => gate3_min_sample,

		--GATES DATA
		GATES_CONFIG      => config_update_gates,
		GATES_SAMPLE_START=> gates_sample_start,
        GATES_SAMPLE_END  => gates_sample_end,
        GATES_EN          => gates_en,
        GATES_END         => gates_end,
        READ_GATES_RESULT => read_gates_result,
        
		GATES_AXIS_TREADY => gates_axis_tready,
		GATES_AXIS_TDATA  => gates_axis_tdata,
		GATES_AXIS_TLAST  => gates_axis_tlast,
		GATES_AXIS_TVALID => gates_axis_tvalid,
		
		-- CORRELATION
        CORRELATION_EN              	=> correlation_en,
        CORRELATION_GATE_SAMPLE_START   => correlation_gate_sample_start,
        CORRELATION_GATE_SAMPLE_END     => correlation_gate_sample_end,
        CORRELATION_AXIS_TREADY     	=> correlation_axis_tready,
        CORRELATION_AXIS_TDATA      	=> correlation_axis_tdata,
        CORRELATION_AXIS_TLAST      	=> correlation_axis_tlast,
        CORRELATION_AXIS_TVALID     	=> correlation_axis_tvalid
	);
	
    -- DATA COMPRESSOR
    
    gates_accumulator_inst: gates_accumulator 
    generic map(
        N_CHANNELS        => 1,
        REGISTER_W32      => REGISTER_W32,
        REGISTER_W16      => REGISTER_W16,
        REGISTER_W14      => REGISTER_W14,
        REGISTER_W8       => REGISTER_W8,
        BRAM_ADDR_WIDTH   => DC_BRAM_ADDR_WIDTH,
        BRAM_DATA_WIDTH   => DC_BRAM_DATA_WIDTH,
        WAIT_CYCLES       => WAIT_CYCLES,
        ASCAN_HEADER_SIZE => ASCAN_HEADER_SIZE,
        COMPRESSION_NO    => COMPRESSION_NO,
        COMPRESSION_ASCAN => COMPRESSION_ASCAN,
        COMPRESSION_GATES => COMPRESSION_GATES,
        COMPRESSION_ASCAN_GATES => COMPRESSION_ASCAN_GATES,      
        COMPRESSION_BURST => COMPRESSION_BURST,
        COMPRESSION_ASCAN_DEC => COMPRESSION_ASCAN_DEC
    )
    port map(
        -- Sync
        aclk         => aclk,
        aresetn      => aresetn,
        
        -- Control
        GA_RESET  => compression_reset,           
        NO_READ_GA_RESULT  => no_read_ga_result,
        READ_GA_RESULT     => read_ga_result,
        DC_MODE            => dsp_compression_mode,
        
        GA_WINDOW_RESULT   => ga_window_result,
        GA_N_GATES         => ga_n_gates,
                          
        -- GATES input
        GATES_INPUT_AXIS_TREADY  => gates_input_axis_tready,
        GATES_INPUT_AXIS_TDATA   => gates_input_axis_tdata,
        GATES_INPUT_AXIS_TLAST   => gates_input_axis_tlast,
        GATES_INPUT_AXIS_TVALID  => gates_input_axis_tvalid,
    
        -- Decimation result
        GA_RESULT_AXIS_TVALID    => ga_result_axis_tvalid,
        GA_RESULT_AXIS_TDATA     => ga_result_axis_tdata,
        GA_RESULT_AXIS_TLAST     => ga_result_axis_tlast,
        GA_RESULT_AXIS_TREADY    => ga_result_axis_tready
    );
		
	RECEIVER_DELAY <= receiver_delay_i;
	RECEIVER_DATA_WINDOW <= receiver_data_window_i;
	RECEIVER_SAMPLING_FREQUENCY <= receiver_sampling_frequency_i;
	RECEIVER_DIGITAL_GAIN <= receiver_digital_gain_i;	
	
	TRANSMITTER_DELTA_DELAY <= transmitter_delta_delay_i;
	
    -- DECIMATION FILTER
    
    decimator_filter_inst: decimator_filter 
    generic map(
        REGISTER_W32        => REGISTER_W32,
        REGISTER_W16        => REGISTER_W16,
        REGISTER_W14        => REGISTER_W14,
        BRAM_ADDR_WIDTH     => 12,
        WAIT_CYCLES         => WAIT_CYCLES,
        DEC_TYPE_NONE       => DEC_TYPE_NONE,
        DEC_TYPE_SIMPLE     => DEC_TYPE_SIMPLE,
        DEC_TYPE_MINMAX     => DEC_TYPE_MINMAX,
        DEC_TYPE_ALOK       => DEC_TYPE_ALOK
    )
    port map(
        -- Sync
        aclk         => aclk,
        aresetn      => aresetn,
        
        -- Control
        NO_READ_DEC_RESULT  => no_read_dec_result,
        READ_DEC_RESULT     => read_dec_result,
        DEC_END             => dec_end,
        DEC_EN              => dec_en,
        
        DEC_TYPE    => dsp_dec_type,  
        DEC_LEVEL   => dsp_dec_level,
        DEC_WINDOW  => dsp_dec_window,
        DATA_WINDOW => fir_data_window_result,
        
        DEC_TYPE_RESULT         => dec_type_result,   
        DEC_LEVEL_RESULT        => dec_level_result,
        DEC_WINDOW_RESULT       => dec_window_result,
        DEC_WINDOW_HALF_RESULT  => dec_window_half_result,  
            
        -- Signal input
        SIGNAL_INPUT_AXIS_TREADY  => dec_axis_tready,
        SIGNAL_INPUT_AXIS_TDATA   => dec_axis_tdata,
        SIGNAL_INPUT_AXIS_TMAX    => dec_axis_tmax,
        SIGNAL_INPUT_AXIS_TLAST   => dec_axis_tlast,
        SIGNAL_INPUT_AXIS_TVALID  => dec_axis_tvalid,

        -- Decimation result
        DEC_RESULT_AXIS_TVALID    => dec_result_axis_tvalid,
        DEC_RESULT_AXIS_TDATA     => dec_result_axis_tdata,
        DEC_RESULT_AXIS_TLAST     => dec_result_axis_tlast,
        DEC_RESULT_AXIS_TREADY    => dec_result_axis_tready
    );
																
	-- CHANNEL CONFIGURATION	
	channel_configuration_inst: channel_configuration
	generic map(
		CHANNEL_ID		=> CHANNEL_ID,
		BRAM_ADDR_WIDTH => 11,
        REGISTER_W32    => REGISTER_W32,
        REGISTER_W16    => REGISTER_W16,
		REGISTER_W8     => REGISTER_W8,
		REGISTER_W4     => REGISTER_W4,
		REGISTER_W2     => REGISTER_W2
	)
	port map(
	    -- Sync
	    aclk         => aclk, 
        aresetn      => aresetn,
		
		-- Control
		CONFIG_UPDATE_RECEIVER  	=> config_update_receiver,
		CONFIG_NEW_RECEIVER   		=> config_new_receiver,
		
		CONFIG_UPDATE_TRANSMITTER  	=> config_update_transmitter,
		CONFIG_NEW_TRANSMITTER  	=> config_new_transmitter,
		
		CONFIG_UPDATE_MAGNET 	 	=> config_update_magnet,
		CONFIG_NEW_MAGNET  			=> config_new_magnet,
		
		CONFIG_UPDATE_DSP_1  		=> config_update_dsp_1,
		CONFIG_NEW_DSP_1  			=> config_new_dsp_1,
		
		CONFIG_UPDATE_DSP_2  		=> config_update_dsp_2,
		CONFIG_NEW_DSP_2  			=> config_new_dsp_2,
		
		CONFIG_UPDATE_FIR  	 		=> config_update_fir,
		CONFIG_NEW_FIR  			=> config_new_fir,
		
		CONFIG_UPDATE_GATES  		=> config_update_gates,
		CONFIG_NEW_GATES  			=> config_new_gates,
		
		CONFIG_UPDATE_DAC  	 		=> config_update_dac,
		CONFIG_NEW_DAC  			=> config_new_dac,
		
		THERE_IS_CONFIGURATION		=> there_is_configuration,
		
		-- Channel enable
		CHANNEL_EN   	=> channel_en,
				
		-- Channel HW
		CHANNEL_HW      => CHANNEL_HW,
	
		-- Config input
		CONFIG_INPUT_LOAD	  		=> CONFIG_INPUT_LOAD,
		CONFIG_INPUT_AXIS_TREADY 	=> CONFIG_INPUT_AXIS_TREADY,
		CONFIG_INPUT_AXIS_TDATA	  	=> CONFIG_INPUT_AXIS_TDATA,
		CONFIG_INPUT_AXIS_TLAST	  	=> CONFIG_INPUT_AXIS_TLAST,
		CONFIG_INPUT_AXIS_TVALID  	=> CONFIG_INPUT_AXIS_TVALID,
		
		-- Config output receiver
		RECEIVER_SAMPLING_FREQUENCY         => receiver_sampling_frequency_i,  
        RECEIVER_DATA_WINDOW                => receiver_data_window_i, 
        RECEIVER_DELAY                      => receiver_delay_i,
        RECEIVER_ANALOG_GAIN                => RECEIVER_ANALOG_GAIN, 
        RECEIVER_DIGITAL_GAIN               => receiver_digital_gain_i,
        RECEIVER_EXTERNAL_MULTIPLEXER_EN    => RECEIVER_EXTERNAL_MULTIPLEXER_EN,
		
		-- Config output transmitter
		TRANSMITTER_VOLTAGE                 => TRANSMITTER_VOLTAGE,  
        TRANSMITTER_BURST_FREQUENCY         => transmitter_burst_frequency_i, 
        TRANSMITTER_N_CYCLES                => TRANSMITTER_N_CYCLES, 
        TRANSMITTER_N_BURST                 => transmitter_n_burst_i,
        TRANSMITTER_PRF                     => transmitter_prf,
        TRANSMITTER_DELAY                   => transmitter_delay_i,
        TRANSMITTER_DELTA_DELAY             => transmitter_delta_delay_i,
        TRANSMITTER_DELTA_ADD               => TRANSMITTER_DELTA_ADD,
        TRANSMITTER_DIRECTIONAL_PHASING     => TRANSMITTER_DIRECTIONAL_PHASING, 
        TRANSMITTER_PHASE_SHIFT             => transmitter_phase_shift,
        TRANSMITTER_START_CYCLE             => transmitter_start_cycle_i,
		
		-- Config output magnet
		MAGNET_MODE                         => magnet_mode_i,
        MAGNET_PULSE_WIDTH                  => magnet_pulse_width_i,
        MAGNET_INITIAL_DELAY                => magnet_initial_delay_i, 
        MAGNET_RAMP_UP_VOLTAGE              => MAGNET_RAMP_UP_VOLTAGE,
        MAGNET_VOLTAGE                      => MAGNET_VOLTAGE,         
		
		-- Config output DSP
		DSP_AVERAGE                         => dsp_average,
		DSP_AVERAGE_TYPE                    => avg_type_i,
        DSP_NOISE_REDUCTION_FILTER          => dsp_noise_reduction_filter,  
        DSP_PHASE_SHIFT                     => dsp_phase_shift,
        DSP_BANDPASS_EN  					=> dsp_bandpass_en,
		DSP_BANDPASS_N_TAPS  				=> dsp_bandpass_n_taps,
		DSP_BANDPASS_L  					=> dsp_bandpass_l,
        DSP_GAUSSIAN_EN  					=> dsp_gaussian_en,
		DSP_GAUSSIAN_N_TAPS  				=> dsp_gaussian_n_taps,
		DSP_GAUSSIAN_L  					=> dsp_gaussian_l,
		DSP_ENVELOPE_EN  					=> dsp_envelope_en,
		DSP_ENVELOPE_N_TAPS  				=> dsp_envelope_n_taps,
		DSP_ENVELOPE_L  					=> dsp_envelope_l,
		DSP_ANALOG_FILTER					=> DSP_ANALOG_FILTER,
		DSP_DEC_TYPE					    => dsp_dec_type_i,
		DSP_DEC_LEVEL					    => dsp_dec_level_i,
		DSP_DEC_WINDOW					    => dsp_dec_window_i,
		DSP_GAUSSIAN_DELAY                  => dsp_gaussian_delay,
		DSP_INVERSION                       => dsp_inversion,
		DSP_RECTIFICATION                   => dsp_rectification,
		DSP_FIR_DELAY                       => dsp_fir_delay,
		DSP_COMPRESSION_MODE                => dsp_compression_mode,
		DSP_COMPRESSION_LEVEL               => dsp_compression_level,
		DSP_AVG_COINC_ORDER                 => dsp_avg_coinc_order,
				
		-- Config output Gates
		GATE1_EN                          => gate1_en,  
		GATE1_START                       => gate1_start,
		GATE1_WIDTH                       => gate1_width,	
        GATE1_AMP_THRESHOLD               => gate1_amp_threshold,
		GATE1_TOF_ALGORITHM               => gate1_tof_algorithm,
		GATE1_TOF_AVG                     => gate1_tof_avg,
		GATE1_TOF_MIN_THICKNESS           => gate1_tof_min_thickness,
        GATE1_AMP_THRESHOLD_ALARM_EN      => gate1_amp_threshold_alarm_en,
        GATE1_AMP_THRESHOLD_ALARM_CROSSING=> gate1_amp_threshold_alarm_crossing,
        GATE1_TOF_ALARM_EN                => gate1_tof_alarm_en,
        GATE1_TOF_ALARM_TYPE              => gate1_tof_alarm_type,
        GATE1_TOF_ALARM_CROSSING          => gate1_tof_alarm_crossing,
        GATE1_TOF_ALARM_MIN_VALUE         => gate1_tof_alarm_min_value,
        GATE1_TOF_ALARM_MAX_VALUE         => gate1_tof_alarm_max_value,	
		
		GATE2_EN                          => gate2_en,  
        GATE2_START                       => gate2_start,
        GATE2_WIDTH                       => gate2_width,    
        GATE2_AMP_THRESHOLD               => gate2_amp_threshold,
        GATE2_TOF_ALGORITHM               => gate2_tof_algorithm,
        GATE2_TOF_AVG                     => gate2_tof_avg,
        GATE2_TOF_MIN_THICKNESS           => gate2_tof_min_thickness,
        GATE2_AMP_THRESHOLD_ALARM_EN      => gate2_amp_threshold_alarm_en,
        GATE2_AMP_THRESHOLD_ALARM_CROSSING=> gate2_amp_threshold_alarm_crossing,
        GATE2_TOF_ALARM_EN                => gate2_tof_alarm_en,
        GATE2_TOF_ALARM_TYPE              => gate2_tof_alarm_type,
        GATE2_TOF_ALARM_CROSSING          => gate2_tof_alarm_crossing,
        GATE2_TOF_ALARM_MIN_VALUE         => gate2_tof_alarm_min_value,
        GATE2_TOF_ALARM_MAX_VALUE         => gate2_tof_alarm_max_value,    
        GATE2_FFT_EN                      => gate2_fft_en,
        GATE2_CC_EN                       => gate2_cc_en,
        GATE2_FFT_SEGMENT                 => gate2_fft_segment,
        GATE2_FFT_NFFT                    => gate2_fft_nfft,
		
		GATE3_EN                          => gate3_en,  
        GATE3_START                       => gate3_start,
        GATE3_WIDTH                       => gate3_width,    
        GATE3_AMP_THRESHOLD               => gate3_amp_threshold,
        GATE3_TOF_ALGORITHM               => gate3_tof_algorithm,
        GATE3_TOF_AVG                     => gate3_tof_avg,
        GATE3_TOF_MIN_THICKNESS           => gate3_tof_min_thickness,
        GATE3_AMP_THRESHOLD_ALARM_EN      => gate3_amp_threshold_alarm_en,
        GATE3_AMP_THRESHOLD_ALARM_CROSSING=> gate3_amp_threshold_alarm_crossing,
        GATE3_TOF_ALARM_EN                => gate3_tof_alarm_en,
        GATE3_TOF_ALARM_TYPE              => gate3_tof_alarm_type,
        GATE3_TOF_ALARM_CROSSING          => gate3_tof_alarm_crossing,
        GATE3_TOF_ALARM_MIN_VALUE         => gate3_tof_alarm_min_value,
        GATE3_TOF_ALARM_MAX_VALUE         => gate3_tof_alarm_max_value,    
		
		-- Config output FIR
		CONFIG_FIR_AXIS_TREADY    => fir_taps_axis_tready,
		CONFIG_FIR_AXIS_TDATA	  => config_fir_axis_tdata,
		CONFIG_FIR_AXIS_TLAST	  => config_fir_axis_tlast,
		CONFIG_FIR_AXIS_TVALID    => config_fir_axis_tvalid,
		
		-- DAC output
		CONFIG_DAC_AXIS_TREADY    => CONFIG_DAC_AXIS_TREADY,
		CONFIG_DAC_AXIS_TDATA	  => CONFIG_DAC_AXIS_TDATA,
		CONFIG_DAC_AXIS_TLAST	  => CONFIG_DAC_AXIS_TLAST,
		CONFIG_DAC_AXIS_TVALID    => CONFIG_DAC_AXIS_TVALID
	);	
	
	channel_state_process: process(aclk)
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                CHANNEL_STATE_A <= std_logic_vector(to_unsigned(0, CHANNEL_STATE_A'length));
                CHANNEL_STATE_B <= std_logic_vector(to_unsigned(0, CHANNEL_STATE_B'length));
                CHANNEL_STATE_C <= std_logic_vector(to_unsigned(0, CHANNEL_STATE_C'length));
            else
                if(state_general_a_sm = idle) then
                    CHANNEL_STATE_A <= std_logic_vector(to_unsigned(1, CHANNEL_STATE_A'length)); 
                elsif(state_general_a_sm = wait_state) then
                    CHANNEL_STATE_A <= std_logic_vector(to_unsigned(2, CHANNEL_STATE_A'length)); 
                elsif(state_general_a_sm = prf_control) then
                    CHANNEL_STATE_A <= std_logic_vector(to_unsigned(3, CHANNEL_STATE_A'length)); 
                elsif(state_general_a_sm = trigger_external) then
                    CHANNEL_STATE_A <= std_logic_vector(to_unsigned(4, CHANNEL_STATE_A'length)); 
                elsif(state_general_a_sm = trigger_external_clear) then
                    CHANNEL_STATE_A <= std_logic_vector(to_unsigned(5, CHANNEL_STATE_A'length)); 
                elsif(state_general_a_sm = wait_stop_motor) then
                    CHANNEL_STATE_A <= std_logic_vector(to_unsigned(6, CHANNEL_STATE_A'length));   
                elsif(state_general_a_sm = sw_trigger) then
                    CHANNEL_STATE_A <= std_logic_vector(to_unsigned(7, CHANNEL_STATE_A'length));  
                elsif(state_general_a_sm = trigger_encoder) then
                    CHANNEL_STATE_A <= std_logic_vector(to_unsigned(8, CHANNEL_STATE_A'length)); 
                elsif(state_general_a_sm = trigger_start) then
                    CHANNEL_STATE_A <= std_logic_vector(to_unsigned(9, CHANNEL_STATE_A'length)); 
                elsif(state_general_a_sm = prf_control_mrut) then
                    CHANNEL_STATE_A <= std_logic_vector(to_unsigned(10, CHANNEL_STATE_A'length)); 
                elsif(state_general_a_sm = coincidence_average_window) then
                    CHANNEL_STATE_A <= std_logic_vector(to_unsigned(11, CHANNEL_STATE_A'length)); 
                elsif(state_general_a_sm = update_receiver) then
                    CHANNEL_STATE_A <= std_logic_vector(to_unsigned(12, CHANNEL_STATE_A'length)); 
                elsif(state_general_a_sm = update_transmitter) then
                    CHANNEL_STATE_A <= std_logic_vector(to_unsigned(13, CHANNEL_STATE_A'length)); 
                elsif(state_general_a_sm = update_magnet) then
                    CHANNEL_STATE_A <= std_logic_vector(to_unsigned(14, CHANNEL_STATE_A'length)); 
                elsif(state_general_a_sm = update_dsp_1) then
                    CHANNEL_STATE_A <= std_logic_vector(to_unsigned(15, CHANNEL_STATE_A'length)); 
                elsif(state_general_a_sm = update_dac) then
                    CHANNEL_STATE_A <= std_logic_vector(to_unsigned(16, CHANNEL_STATE_A'length)); 
                elsif(state_general_a_sm = update_control) then
                    CHANNEL_STATE_A <= std_logic_vector(to_unsigned(17, CHANNEL_STATE_A'length)); 
                elsif(state_general_a_sm = wait_clear_avg_acc) then
                    CHANNEL_STATE_A <= std_logic_vector(to_unsigned(18, CHANNEL_STATE_A'length)); 
                elsif(state_general_a_sm = sync_channels) then
                    CHANNEL_STATE_A <= std_logic_vector(to_unsigned(19, CHANNEL_STATE_A'length)); 
                elsif(state_general_a_sm = encoder_alignment) then
                    CHANNEL_STATE_A <= std_logic_vector(to_unsigned(20, CHANNEL_STATE_A'length)); 
                elsif(state_general_a_sm = lrut_wait) then
                    CHANNEL_STATE_A <= std_logic_vector(to_unsigned(21, CHANNEL_STATE_A'length)); 
                else
                    CHANNEL_STATE_A <= std_logic_vector(to_unsigned(0, CHANNEL_STATE_A'length)); 
                end if;
                
                if(state_general_b_sm = idle) then
                    CHANNEL_STATE_B <= std_logic_vector(to_unsigned(1, CHANNEL_STATE_B'length)); 
                elsif(state_general_b_sm = wait_input_fir_end) then
                    CHANNEL_STATE_B <= std_logic_vector(to_unsigned(2, CHANNEL_STATE_B'length)); 
                elsif(state_general_b_sm = wait_fir_end) then
                    CHANNEL_STATE_B <= std_logic_vector(to_unsigned(3, CHANNEL_STATE_B'length)); 
                elsif(state_general_b_sm = wait_fir_delay_end) then
                    CHANNEL_STATE_B <= std_logic_vector(to_unsigned(4, CHANNEL_STATE_B'length)); 
                elsif(state_general_b_sm = wait_gates_end) then
                    CHANNEL_STATE_B <= std_logic_vector(to_unsigned(5, CHANNEL_STATE_B'length)); 
                elsif(state_general_b_sm = wait_decimation_end) then
                    CHANNEL_STATE_B <= std_logic_vector(to_unsigned(6, CHANNEL_STATE_B'length));   
                elsif(state_general_b_sm = data_disposition) then
                    CHANNEL_STATE_B <= std_logic_vector(to_unsigned(7, CHANNEL_STATE_B'length));  
                elsif(state_general_b_sm = data_disposition_flag) then
                    CHANNEL_STATE_B <= std_logic_vector(to_unsigned(8, CHANNEL_STATE_B'length)); 
                elsif(state_general_b_sm = read_result) then
                    CHANNEL_STATE_B <= std_logic_vector(to_unsigned(9, CHANNEL_STATE_B'length)); 
                elsif(state_general_b_sm = read_result_end) then
                    CHANNEL_STATE_B <= std_logic_vector(to_unsigned(10, CHANNEL_STATE_B'length)); 
                elsif(state_general_b_sm = update_dsp_2) then
                    CHANNEL_STATE_B <= std_logic_vector(to_unsigned(11, CHANNEL_STATE_B'length)); 
                elsif(state_general_b_sm = update_FIR) then
                    CHANNEL_STATE_B <= std_logic_vector(to_unsigned(12, CHANNEL_STATE_B'length)); 
                elsif(state_general_b_sm = update_gates) then
                    CHANNEL_STATE_B <= std_logic_vector(to_unsigned(13, CHANNEL_STATE_B'length)); 
                else
                    CHANNEL_STATE_B <= std_logic_vector(to_unsigned(0, CHANNEL_STATE_B'length)); 
                end if;
                
                if(state_read_ascan_sm = idle) then
                    CHANNEL_STATE_C <= std_logic_vector(to_unsigned(1, CHANNEL_STATE_C'length)); 
                elsif(state_read_ascan_sm = no_read_result) then
                    CHANNEL_STATE_C <= std_logic_vector(to_unsigned(2, CHANNEL_STATE_C'length)); 
                elsif(state_read_ascan_sm = read_ascan_start) then
                    CHANNEL_STATE_C <= std_logic_vector(to_unsigned(3, CHANNEL_STATE_C'length)); 
                elsif(state_read_ascan_sm = upload_general_header) then
                    CHANNEL_STATE_C <= std_logic_vector(to_unsigned(4, CHANNEL_STATE_C'length)); 
                elsif(state_read_ascan_sm = upload_ascan_header) then
                    CHANNEL_STATE_C <= std_logic_vector(to_unsigned(5, CHANNEL_STATE_C'length)); 
                elsif(state_read_ascan_sm = upload_ascan_data_flank_down) then
                    CHANNEL_STATE_C <= std_logic_vector(to_unsigned(6, CHANNEL_STATE_C'length));   
                elsif(state_read_ascan_sm = upload_ascan_data_flank_up) then
                    CHANNEL_STATE_C <= std_logic_vector(to_unsigned(7, CHANNEL_STATE_C'length));  
                elsif(state_read_ascan_sm = upload_ascan_end) then
                    CHANNEL_STATE_C <= std_logic_vector(to_unsigned(8, CHANNEL_STATE_C'length)); 
                else
                    CHANNEL_STATE_C <= std_logic_vector(to_unsigned(0, CHANNEL_STATE_C'length)); 
                end if;
            end if;       
        end if;
    end process;
				           
end arch_imp;
