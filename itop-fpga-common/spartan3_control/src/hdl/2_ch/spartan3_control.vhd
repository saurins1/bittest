library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity spartan3_control is
	generic (
        REGISTER_W32        : INTEGER := 32;
        REGISTER_W4         : INTEGER := 4
	);
	port (
			aclk					: in std_logic;
			areset					: in std_logic;
		
			-- SPARTAN 3 Version
			spartan3_ver			: out STD_LOGIC_VECTOR(REGISTER_W32-1 DOWNTO 0);

			-- GAIN
			CH0_RECEIVER_GAIN       : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
			CH1_RECEIVER_GAIN       : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
		
			-- DAC CURVES
			CH0_CONFIG_DAC_AXIS_TREADY    : out STD_LOGIC;
			CH0_CONFIG_DAC_AXIS_TDATA	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			CH0_CONFIG_DAC_AXIS_TLAST	  : in STD_LOGIC;
			CH0_CONFIG_DAC_AXIS_TVALID    : in STD_LOGIC;
		
			CH1_CONFIG_DAC_AXIS_TREADY    : out STD_LOGIC;
			CH1_CONFIG_DAC_AXIS_TDATA	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			CH1_CONFIG_DAC_AXIS_TLAST	  : in STD_LOGIC;
			CH1_CONFIG_DAC_AXIS_TVALID    : in STD_LOGIC;
		
			-- ANALOG GAIN
			CH0_DSP_ANALOG_FILTER  	: in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
			CH1_DSP_ANALOG_FILTER  	: in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
					
			-- RECEIVER COMMUNICATION
			RCVR_FPGA_SCLK			: out STD_LOGIC;
			RCVR_FPGA_CS			: out STD_LOGIC;
			RCVR_FPGA_SDO			: in STD_LOGIC;
			RCVR_FPGA_SDI			: out STD_LOGIC
		);
end spartan3_control;

architecture spartan3_control_arch of spartan3_control is

	COMPONENT spartan3_gain_filt_dac is
		port (
				CLOCK					: in std_logic;
				RESET					: in std_logic;

				CH0_RECEIVER_GAIN       : in STD_LOGIC_VECTOR(31 downto 0);  
				CH1_RECEIVER_GAIN       : in STD_LOGIC_VECTOR(31 downto 0);  
				CH0_DSP_ANALOG_FILTER  	: in STD_LOGIC_VECTOR(3 downto 0);
				CH1_DSP_ANALOG_FILTER  	: in STD_LOGIC_VECTOR(3 downto 0);
				CH0_CONFIG_DAC_AXIS_TREADY    : out STD_LOGIC;
				CH0_CONFIG_DAC_AXIS_TDATA	  : in STD_LOGIC_VECTOR(31 downto 0);
				CH0_CONFIG_DAC_AXIS_TLAST	  : in STD_LOGIC;
				CH0_CONFIG_DAC_AXIS_TVALID    : in STD_LOGIC;
				CH1_CONFIG_DAC_AXIS_TREADY    : out STD_LOGIC;
				CH1_CONFIG_DAC_AXIS_TDATA	  : in STD_LOGIC_VECTOR(31 downto 0);
				CH1_CONFIG_DAC_AXIS_TLAST	  : in STD_LOGIC;
				CH1_CONFIG_DAC_AXIS_TVALID    : in STD_LOGIC;

				spartan3_ver			: out STD_LOGIC_VECTOR(31 DOWNTO 0);

				RCVR_FPGA_SCLK			: out STD_LOGIC;
				RCVR_FPGA_CS			: out STD_LOGIC;
				RCVR_FPGA_SDO			: in STD_LOGIC;
				RCVR_FPGA_SDI			: out STD_LOGIC
			);
	end COMPONENT spartan3_gain_filt_dac;
 
begin
	
	spartan3_gain_filt_dac_inst: spartan3_gain_filt_dac
		port map(
				CLOCK					=> aclk,
				RESET					=> areset,

				CH0_RECEIVER_GAIN       => CH0_RECEIVER_GAIN, 
				CH1_RECEIVER_GAIN       => CH1_RECEIVER_GAIN, 
				CH0_DSP_ANALOG_FILTER  	=> CH0_DSP_ANALOG_FILTER,
				CH1_DSP_ANALOG_FILTER   => CH1_DSP_ANALOG_FILTER,
				CH0_CONFIG_DAC_AXIS_TREADY    => CH0_CONFIG_DAC_AXIS_TREADY,
				CH0_CONFIG_DAC_AXIS_TDATA	  => CH0_CONFIG_DAC_AXIS_TDATA,
				CH0_CONFIG_DAC_AXIS_TLAST	  => CH0_CONFIG_DAC_AXIS_TLAST,
				CH0_CONFIG_DAC_AXIS_TVALID    => CH0_CONFIG_DAC_AXIS_TVALID,
				CH1_CONFIG_DAC_AXIS_TREADY    => CH1_CONFIG_DAC_AXIS_TREADY,
				CH1_CONFIG_DAC_AXIS_TDATA	  => CH1_CONFIG_DAC_AXIS_TDATA,
				CH1_CONFIG_DAC_AXIS_TLAST	  => CH1_CONFIG_DAC_AXIS_TLAST,
				CH1_CONFIG_DAC_AXIS_TVALID	  => CH1_CONFIG_DAC_AXIS_TVALID,

				spartan3_ver			=> spartan3_ver,

				RCVR_FPGA_SCLK			=> RCVR_FPGA_SCLK,
				RCVR_FPGA_CS			=> RCVR_FPGA_CS,
				RCVR_FPGA_SDO			=> RCVR_FPGA_SDO,
				RCVR_FPGA_SDI			=> RCVR_FPGA_SDI
			);
	
end spartan3_control_arch;