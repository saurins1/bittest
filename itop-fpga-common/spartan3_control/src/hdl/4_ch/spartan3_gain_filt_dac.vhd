-- spartan3 gain filter download (DAC also download from here)
--	gain parameters ( -23dB to 73dB hardware limit )
-- -23db		41							0.04v
-- -20db		71 "00 0100 0111"		0.07v
-- 0db		276 "01 0001 0100"	0.27v
-- 20db		480 "01 1110 0000"	0.47v
-- 40db		684 "10 1010 1100"	0.67v
-- 73db		1021						1.00v
-- 1db		30.64/3 					0.01v
-- db to DAC code : db * 10 + 276. (accurate at 0db, +/-0.5db@-23db/25db -1.5db@73db)
--
-- 02/22/2018 new gain module, db to DAC code: (db * 12.64 + 252.8) range -20db to 60.93db equation used in FPGA: db * 809/64 + 253
-- DAC curve data 100MHz not change, Spartan3 side decimate, new module play at 50MHz
--
-- filter parameters
-- "000"		0.05MHz to 25MHz
-- "001"		0.0625MHz to 0.3125MHz
-- "010"		0.125MHz to 0.625MHz
-- "011"		0.25MHz to 1.25MHz
-- "100"		0.5MHz to 2.5MHz
-- "101"		1MHz to 5MHz
-- "110"		2MHz to 10MHz
-- "111"		4MHz to 20MHz

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity spartan3_gain_filt_dac is
	port (
			CLOCK					: in std_logic;
			RESET					: in std_logic;

			CH0_RECEIVER_GAIN       : in STD_LOGIC_VECTOR(31 downto 0);  
			CH1_RECEIVER_GAIN       : in STD_LOGIC_VECTOR(31 downto 0);  
			CH0_DSP_ANALOG_FILTER  	: in STD_LOGIC_VECTOR(3 downto 0);
			CH1_DSP_ANALOG_FILTER  	: in STD_LOGIC_VECTOR(3 downto 0);
			CH0_CONFIG_DAC_AXIS_TREADY    : out STD_LOGIC;
			CH0_CONFIG_DAC_AXIS_TDATA	  : in STD_LOGIC_VECTOR(31 downto 0);
			CH0_CONFIG_DAC_AXIS_TLAST	  : in STD_LOGIC;
			CH0_CONFIG_DAC_AXIS_TVALID    : in STD_LOGIC;		
			CH1_CONFIG_DAC_AXIS_TREADY    : out STD_LOGIC;
			CH1_CONFIG_DAC_AXIS_TDATA	  : in STD_LOGIC_VECTOR(31 downto 0);
			CH1_CONFIG_DAC_AXIS_TLAST	  : in STD_LOGIC;
			CH1_CONFIG_DAC_AXIS_TVALID    : in STD_LOGIC;	
			
			spartan3_ver			: out STD_LOGIC_VECTOR(31 DOWNTO 0);
			
			RCVR_FPGA_SCLK			: out STD_LOGIC;
			RCVR_FPGA_CS			: out STD_LOGIC;
			RCVR_FPGA_SDO			: in STD_LOGIC;
			RCVR_FPGA_SDI			: out STD_LOGIC
		);
end spartan3_gain_filt_dac;

architecture spartan3_gain_filt_dac_arch of spartan3_gain_filt_dac is

  type Spartan3_StateEnum_t is (
						   Spartan3_Init_c,
						   Spartan3_Readid_c,
						   Spartan3_WaitReadidACK_c,
						   Spartan3_WaitReadiddone_c,
						   Spartan3_Readver_c,
						   Spartan3_WaitReadverACK_c,
						   Spartan3_WaitReadverdone_c,
						   Spartan3_ReadLoopback_c,
						   Spartan3_WaitReadLoopbackACK_c,
						   Spartan3_WaitReadLoopbackdone_c,
						   Spartan3_Idle_c,
						   Spartan3_Send_CH0_Gain_Filter_c,
						   Spartan3_Send_CH1_Gain_Filter_c,
						   Spartan3_ConvGain1_c,
						   Spartan3_ConvGain2_c,
						   Spartan3_SendCurrentFrame_c,
						   Spartan3_WaitCurrentFrameACK_c,
						   Spartan3_SendGainFilt_c,
						   Spartan3_WaitGainFiltACK_c,
						   
						   Spartan3_SendDACch0_c,
						   Spartan3_waitSendDACch0_header,
						   wait_send_DAC_points_ch0,
						   wait_send_DAC_init_gain_ch0,
						   skip_first_steps_ch0,
						   skip_first_steps1_ch0,
						   wait_send_first_steps_ch0,
						   wait_send_first_delta_ch0,
						   send_DAC_pairs_ch0,
						   wait_send_DAC_pairs_steps_ch0,
						   wait_send_DAC_pairs_delta_ch0,
						   
						   Spartan3_SendDACch1_c,
						   Spartan3_waitSendDACch1_header,
                           wait_send_DAC_points_ch1,
                           wait_send_DAC_init_gain_ch1,
                           skip_first_steps_ch1,
                           skip_first_steps1_ch1,
                           wait_send_first_steps_ch1,
                           wait_send_first_delta_ch1,
                           send_DAC_pairs_ch1,
                           wait_send_DAC_pairs_steps_ch1,
                           wait_send_DAC_pairs_delta_ch1,
						   
						   Send_DAC_tail,
						   wait_send_DAC_tail
						   );
						   
  signal Spartan3_State_s 	:	Spartan3_StateEnum_t;
  signal Spartan3_next_s 	:	Spartan3_StateEnum_t;
  
	signal m_Spartan3_ver	: STD_LOGIC_VECTOR(31 DOWNTO 0) := (others => '1');		-- "FFFFFFFF" no valid Spartan3 firmware
	signal Spartan3_valid	: std_logic := '0';										-- "1" Spartan3 has valid firmware
	signal m_t_count        : STD_LOGIC_VECTOR(21 DOWNTO 0) := (others => '0');

	signal CH0_Gain_Filter_Reg		: STD_LOGIC_VECTOR(35 DOWNTO 0) := (others => '0');
	signal CH1_Gain_Filter_Reg		: STD_LOGIC_VECTOR(35 DOWNTO 0) := (others => '0');
	
	signal Conv_Gain				: signed(20 DOWNTO 0);
    signal Conv_Gain1				: STD_LOGIC_VECTOR(20 DOWNTO 0);
	
	signal Channel_Gain				: signed(31 DOWNTO 0);
	signal Channel_Filter			: STD_LOGIC_VECTOR(2 DOWNTO 0);
	signal Channel_Address			: STD_LOGIC_VECTOR(13 DOWNTO 0);
	
	signal Current_Frame			: STD_LOGIC_VECTOR(7 DOWNTO 0) := "00000001";
	signal Current_Frame_Reg		: STD_LOGIC_VECTOR(7 DOWNTO 0) := (others => '0');
	
	constant POR_Gain0db			: STD_LOGIC_VECTOR(9 DOWNTO 0) := "0100010100";		--0db POR gain
	constant POR_filt				: STD_LOGIC_VECTOR(2 DOWNTO 0) := "000";			--25MHz (bandwidth) POR filter	
	constant Max_Gain				: signed(31 DOWNTO 0) := to_signed(73*1024, 32);		--Max Gain +73dB
	constant Min_Gain				: signed(31 DOWNTO 0) := to_signed( (-23)*1024, 32);	--Min Gain -23dB
	--

    component spartan3_control_multiplier is
      Port (
          A : IN STD_LOGIC_VECTOR(17 DOWNTO 0);
          P : OUT STD_LOGIC_VECTOR(20 DOWNTO 0)
      );
    end component spartan3_control_multiplier;
    
    component spi_master_spartan3 is
      Generic (   
          N : positive := 32;                                             -- 32bit serial word length is default
          CPOL : std_logic := '0';                                        -- SPI mode selection (mode 0 default)
          CPHA : std_logic := '0';                                        -- CPOL = clock polarity, CPHA = clock phase.
          PREFETCH : positive := 2;                                       -- prefetch lookahead cycles
          SPI_2X_CLK_DIV : positive := 5);                                -- for a 100MHz sclk_i, yields a 10MHz SCK
      Port (  
          sclk_i : in std_logic := 'X';                                   -- high-speed serial interface system clock
          pclk_i : in std_logic := 'X';                                   -- high-speed parallel interface system clock
          rst_i : in std_logic := 'X';                                    -- reset core
          ---- serial interface ----
          spi_ssel_o : out std_logic;                                     -- spi bus slave select line
          spi_sck_o : out std_logic;                                      -- spi bus sck
          spi_mosi_o : out std_logic;                                     -- spi bus mosi output
          spi_miso_i : in std_logic := 'X';                               -- spi bus spi_miso_i input
          ---- parallel interface ----
          di_req_o : out std_logic;                                       -- preload lookahead data request line
          di_i : in  std_logic_vector (N-1 downto 0) := (others => 'X');  -- parallel data in (clocked on rising spi_clk after last bit)
          wren_i : in std_logic := 'X';                                   -- user data write enable, starts transmission when interface is idle
          wr_ack_o : out std_logic;                                       -- write acknowledge
          do_valid_o : out std_logic;                                     -- do_o data valid signal, valid during one spi_clk rising edge.
          do_o : out  std_logic_vector (N-1 downto 0)                     -- parallel output (clocked on rising spi_clk after last bit)
          --- debug ports: can be removed or left unconnected for the application circuit ---
--          sck_ena_o : out std_logic;                                      -- debug: internal sck enable signal
--          sck_ena_ce_o : out std_logic;                                   -- debug: internal sck clock enable signal
--          do_transfer_o : out std_logic;                                  -- debug: internal transfer driver
--          wren_o : out std_logic;                                         -- debug: internal state of the wren_i pulse stretcher
--          rx_bit_reg_o : out std_logic;                                   -- debug: internal rx bit
--          state_dbg_o : out std_logic_vector (3 downto 0);                -- debug: internal state register
--          core_clk_o : out std_logic;
--          core_n_clk_o : out std_logic;
--          core_ce_o : out std_logic;
--          core_n_ce_o : out std_logic
--          sh_reg_dbg_o : out std_logic_vector (N-1 downto 0)              -- debug: internal shift register
      );                      
  end component spi_master_spartan3;
  
  SIGNAL m_wren_i				: STD_LOGIC;
  SIGNAL m_do_valid_o           : STD_LOGIC;
  SIGNAL m_di_req_o             : STD_LOGIC;
  SIGNAL m_wr_ack_o             : STD_LOGIC;
  SIGNAL m_wr_ack_o_d1          : STD_LOGIC;
  
--  SIGNAL m_do_transfer_o        : STD_LOGIC;
--  SIGNAL m_wren_o               : STD_LOGIC;
--  SIGNAL m_rx_bit_reg_o         : STD_LOGIC;
--  SIGNAL m_core_clk_o           : STD_LOGIC;
--  SIGNAL m_core_n_clk_o         : STD_LOGIC;
--  SIGNAL m_state_dbg_o          : STD_LOGIC_VECTOR(3 DOWNTO 0);
  
  SIGNAL m_di_i,m_do_o          : STD_LOGIC_VECTOR(31 DOWNTO 0);
  
  COMPONENT spartan3_control_BRAM
    PORT (
    clka : in STD_LOGIC;
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    addra : in STD_LOGIC_VECTOR ( 9 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    clkb : in STD_LOGIC;
    addrb : in STD_LOGIC_VECTOR ( 9 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 31 downto 0 )
    );
  END COMPONENT;
  
  -- signal BRAM port a
  signal ch0_dac_bram_wea      : STD_LOGIC_VECTOR(0 DOWNTO 0);
  signal ch0_dac_bram_addra    : STD_LOGIC_VECTOR(9 DOWNTO 0);
  signal ch0_dac_bram_dina     : STD_LOGIC_VECTOR(31 DOWNTO 0);
  -- signal BRAM port b
  signal ch0_dac_bram_addrb    : STD_LOGIC_VECTOR(9 DOWNTO 0);
  signal ch0_dac_bram_doutb    : STD_LOGIC_VECTOR(31 DOWNTO 0);  
  signal ch0_dac_bram_saved    : STD_LOGIC;
  signal ch0_dac_bram_saved_i  : STD_LOGIC;
  signal ch0_dac_bram_upd      : STD_LOGIC;
  signal dac_bram_upd_ack      : STD_LOGIC;
  
  -- signal BRAM port a
  signal ch1_dac_bram_wea      : STD_LOGIC_VECTOR(0 DOWNTO 0);
  signal ch1_dac_bram_addra    : STD_LOGIC_VECTOR(9 DOWNTO 0);
  signal ch1_dac_bram_dina     : STD_LOGIC_VECTOR(31 DOWNTO 0);
  -- signal BRAM port b
  signal ch1_dac_bram_addrb    : STD_LOGIC_VECTOR(9 DOWNTO 0);
  signal ch1_dac_bram_doutb    : STD_LOGIC_VECTOR(31 DOWNTO 0);  
  signal ch1_dac_bram_saved    : STD_LOGIC;
  signal ch1_dac_bram_saved_i  : STD_LOGIC;
  signal ch1_dac_bram_upd      : STD_LOGIC;

  signal ch0_config_dac_axis_tlast_d1 : STD_LOGIC;
  signal ch0_config_dac_axis_tlast_d2 : STD_LOGIC;
  signal ch0_n_taps : STD_LOGIC_VECTOR(31 DOWNTO 0);

  signal ch1_config_dac_axis_tlast_d1 : STD_LOGIC;
  signal ch1_config_dac_axis_tlast_d2 : STD_LOGIC;
  signal ch1_n_taps : STD_LOGIC_VECTOR(31 DOWNTO 0);

  type states_bram_sm is (idle, update_header, store_dac_curve, read_dac_curve); 
  signal states_bram_sm_a : states_bram_sm;
  signal states_bram_sm_b : states_bram_sm;
  
  signal total_points         : unsigned( 7 downto 0);
  signal count_points         : unsigned( 7 downto 0);
  signal first_steps          : STD_LOGIC_VECTOR(26 DOWNTO 0);
    
begin
	
	spartan3_ver <= m_Spartan3_ver when Spartan3_valid = '1' else
					x"FFFFFFFF";
	process ( RESET, CLOCK )
	begin
	if RESET = '1' then
	   m_t_count <= ( others => '0' );
	elsif rising_edge(CLOCK) then
	   m_t_count <= m_t_count + 1;
	end if;
	END PROCESS;
	
	process(CLOCK)
	begin 
		if rising_edge(CLOCK) then
			if ( RESET = '1' ) then
				m_wr_ack_o_d1 <= '0';
				m_wren_i <= '0';
				Spartan3_valid <= '0';
				ch0_dac_bram_saved_i <= '0';
				ch1_dac_bram_saved_i <= '0';
				ch0_dac_bram_addrb <= (others => '0');
				ch1_dac_bram_addrb <= (others => '0');
				Channel_Gain <= (others => '0');
				Conv_Gain <= (others => '0');
				Channel_Filter <= (others => '0');
				Channel_Address <= (others => '0');
				Spartan3_State_s <= Spartan3_Init_c;
			else
				m_wr_ack_o_d1 <= m_wr_ack_o;
				case Spartan3_State_s is 
					
					when Spartan3_Init_c =>
						Spartan3_State_s <= Spartan3_Readid_c;
					
					when Spartan3_Readid_c =>
						Spartan3_State_s <= Spartan3_WaitReadidACK_c;
						m_wren_i <= '1';
						m_di_i <= '1' & '0' & std_logic_vector(to_unsigned(9,14)) & "00000000" & "00000000";
					
					when Spartan3_WaitReadidACK_c =>
						m_wren_i <= '0';
						if (m_wr_ack_o = '1' and m_wr_ack_o_d1 = '0') then
							Spartan3_State_s <= Spartan3_WaitReadiddone_c;
						else
							Spartan3_State_s <= Spartan3_WaitReadidACK_c;
						end if;	

					when Spartan3_WaitReadiddone_c =>
						if (m_do_valid_o = '1' ) then
							Spartan3_State_s <= Spartan3_Readver_c;
						else
							Spartan3_State_s <= Spartan3_WaitReadiddone_c;
						end if;					
						
					when Spartan3_Readver_c =>
						Spartan3_State_s <= Spartan3_WaitReadverACK_c;
						m_wren_i <= '1';
						m_di_i <= '1' & '0' & std_logic_vector(to_unsigned(11,14)) & "00000000" & "00000000";
					
					when Spartan3_WaitReadverACK_c =>
						m_wren_i <= '0';
						if (m_wr_ack_o = '1' and m_wr_ack_o_d1 = '0') then
							Spartan3_State_s <= Spartan3_WaitReadverdone_c;
						else
							Spartan3_State_s <= Spartan3_WaitReadverACK_c;
						end if;							
					
					when Spartan3_WaitReadverdone_c =>
						if (m_do_valid_o = '1' ) then
							if ( m_do_o = x"a55a5aa5") then
								Spartan3_valid <= '1';
							end if;
							Spartan3_State_s <= Spartan3_ReadLoopback_c;
						else
							Spartan3_State_s <= Spartan3_WaitReadverdone_c;
						end if;					

					when Spartan3_ReadLoopback_c =>
						Spartan3_State_s <= Spartan3_WaitReadLoopbackACK_c;
						m_wren_i <= '1';
						m_di_i <= '1' & '0' & std_logic_vector(to_unsigned(10,14)) & "00000000" & "00000000";
					
					when Spartan3_WaitReadLoopbackACK_c =>
						m_wren_i <= '0';
						if (m_wr_ack_o = '1' and m_wr_ack_o_d1 = '0') then
							Spartan3_State_s <= Spartan3_WaitReadLoopbackdone_c;
						else
							Spartan3_State_s <= Spartan3_WaitReadLoopbackACK_c;
						end if;							
					
					when Spartan3_WaitReadLoopbackdone_c =>
						if (m_do_valid_o = '1' ) then
							m_Spartan3_ver <= m_do_o;
							Spartan3_State_s <= Spartan3_Idle_c;
						else
							Spartan3_State_s <= Spartan3_WaitReadLoopbackdone_c;
						end if;											
					
					when Spartan3_Idle_c =>	
--						m_wren_i <= '0';
--						if ( m_t_count = 0 ) then
--						    Spartan3_State_s <= Spartan3_Send_CH0_Gain_Filter_c;
--						else
--					        Spartan3_State_s <= Spartan3_Idle_c;
--						end if;

						if ( ch0_dac_bram_upd = '1' or ch1_dac_bram_upd = '1') then
						    dac_bram_upd_ack <='0';
						end if;
						if ( (CH0_DSP_ANALOG_FILTER & CH0_RECEIVER_GAIN) /= CH0_Gain_Filter_Reg ) then
							CH0_Gain_Filter_Reg <= CH0_DSP_ANALOG_FILTER & CH0_RECEIVER_GAIN;
							Spartan3_State_s <= Spartan3_Send_CH0_Gain_Filter_c;
						elsif ( (CH1_DSP_ANALOG_FILTER&CH1_RECEIVER_GAIN) /= CH1_Gain_Filter_Reg ) then
							CH1_Gain_Filter_Reg <= CH1_DSP_ANALOG_FILTER & CH1_RECEIVER_GAIN;
							Spartan3_State_s <= Spartan3_Send_CH1_Gain_Filter_c;
						elsif ( Current_Frame /= Current_Frame_Reg ) then
							Spartan3_State_s <= Spartan3_SendCurrentFrame_c;
						elsif ( ch0_dac_bram_upd = '1' or ch1_dac_bram_upd = '1') then
						    Spartan3_State_s <= Spartan3_SendDACch0_c;
						else
							Spartan3_State_s <= Spartan3_Idle_c;
						end if;

					when Spartan3_Send_CH0_Gain_Filter_c => 
						Spartan3_State_s <= Spartan3_ConvGain1_c;
						Spartan3_next_s <= Spartan3_Idle_c;
						Channel_Address <=  STD_LOGIC_VECTOR(to_unsigned(101, 14));
						Channel_Gain <=  signed(CH0_RECEIVER_GAIN);
						Channel_Filter <= CH0_DSP_ANALOG_FILTER(2 downto 0);

					when Spartan3_Send_CH1_Gain_Filter_c => 
						Spartan3_State_s <= Spartan3_ConvGain1_c;
						Spartan3_next_s <= Spartan3_Idle_c;
						Channel_Address <=  STD_LOGIC_VECTOR(to_unsigned(201, 14));
						Channel_Gain <=  signed(CH1_RECEIVER_GAIN);
						Channel_Filter <= CH1_DSP_ANALOG_FILTER(2 downto 0);
						
					when Spartan3_ConvGain1_c =>
						Spartan3_State_s <= Spartan3_ConvGain2_c;
						if ( Channel_Gain > Max_Gain ) then
							Channel_Gain <= Max_Gain;
						elsif (  Channel_Gain < Min_Gain ) then
							Channel_Gain <= Min_Gain;
						else
							Channel_Gain <= Channel_Gain;
						end if;
							
					when Spartan3_ConvGain2_c =>
						Spartan3_State_s <= Spartan3_SendGainFilt_c;
						Conv_Gain <= signed(Conv_Gain1) + to_signed(253*1024, 21);	-- dB * 809/64 (12.64) + 253 (multiply fixed 809/64)
					
					when Spartan3_SendGainFilt_c =>
						Spartan3_State_s <= Spartan3_WaitGainFiltACK_c;
						m_wren_i <= '1';
						m_di_i <= '0' & '0' & Channel_Address & "000" & Channel_Filter & Std_logic_vector( Conv_Gain(19 downto 10) );
					
					when Spartan3_WaitGainFiltACK_c =>
						m_wren_i <= '0';
						if (m_wr_ack_o = '1' and m_wr_ack_o_d1 = '0') then
							Spartan3_State_s <= Spartan3_next_s;
						else
							Spartan3_State_s <= Spartan3_WaitGainFiltACK_c;
						end if;					
					
					when Spartan3_SendCurrentFrame_c =>
						Spartan3_State_s <= Spartan3_WaitCurrentFrameACK_c;
						m_wren_i <= '1';
						m_di_i <= '0' & '0' & std_logic_vector(to_unsigned(6,14)) & "00000000" & Current_Frame;
						
					when Spartan3_WaitCurrentFrameACK_c =>
						m_wren_i <= '0';
						if (m_wr_ack_o = '1' and m_wr_ack_o_d1 = '0') then
							Current_Frame_Reg <= Current_Frame;
							Spartan3_State_s <= Spartan3_Idle_c;
						else
							Spartan3_State_s <= Spartan3_WaitCurrentFrameACK_c;
						end if;						
                    
                    when Spartan3_SendDACch0_c =>
                        ch0_dac_bram_saved_i <= ch0_dac_bram_saved;
                        ch1_dac_bram_saved_i <= ch1_dac_bram_saved;
                        if ( ch0_dac_bram_saved = '1' ) then
                            m_wren_i <= '1';
                            m_di_i <= '0' & '0' & std_logic_vector(to_unsigned(0,14)) & "000000" & "0001" & "0001" & "10";        -- control reg (address 0) for DAC update
                            Spartan3_State_s <= Spartan3_waitSendDACch0_header;
                            ch0_dac_bram_addrb <= (others => '0');
                        else
                            Spartan3_State_s <= Spartan3_SendDACch1_c;
                        end if;    
                            
                    when Spartan3_waitSendDACch0_header =>                                                        -- first data in DAC mem is points, using high "111" to mark start of DAC curve
                        if (m_wr_ack_o = '1' and m_wr_ack_o_d1 = '0') then
                            ch0_dac_bram_addrb <= ch0_dac_bram_addrb + std_logic_vector (to_unsigned(2,10));        -- set init gain address(+1 from head), next data
                            total_points <= unsigned( ch0_n_taps(7 downto 0) );
                            count_points <= to_unsigned(0,8);
                            m_wren_i <= '1';
                            m_di_i <= '0' & '1' & "111" & ch0_n_taps(26 downto 0);
                            Spartan3_State_s <= wait_send_DAC_points_ch0;
                        else
                            m_wren_i <= '0';
                            Spartan3_State_s <= Spartan3_waitSendDACch0_header;
                        end if;                    
    
                    when wait_send_DAC_points_ch0 =>
                        if (m_wr_ack_o = '1' and m_wr_ack_o_d1 = '0') then
                            ch0_dac_bram_addrb <= ch0_dac_bram_addrb + 1;                                        -- set first steps address, if first steps = 0 do not count this point
                            m_wren_i <= '1';
                            m_di_i <= '0' & '1' & "000" & ch0_dac_bram_doutb(26 downto 0);
                            Spartan3_State_s <= wait_send_DAC_init_gain_ch0;
                        else
                            m_wren_i <= '0';
                            Spartan3_State_s <= wait_send_DAC_points_ch0;
                        end if;                    
        
                    when wait_send_DAC_init_gain_ch0 =>
                        if (m_wr_ack_o = '1' and m_wr_ack_o_d1 = '0') then
                            ch0_dac_bram_addrb <= ch0_dac_bram_addrb + std_logic_vector (to_unsigned(2,10));
                            if ( first_steps = std_logic_vector ( to_unsigned(0,27) )) then            -- skip first pair (0,0)    first gain delta is always '0'
                                m_wren_i <= '0';
                                Spartan3_State_s <= skip_first_steps_ch0;
                            else
                                m_wren_i <= '1';
                                m_di_i <= '0' & '1' & "000" & first_steps;
                                Spartan3_State_s <= wait_send_first_steps_ch0;
                            end if;
                        else
                            first_steps <= ch0_dac_bram_doutb(26 downto 0);
                            m_wren_i <= '0';
                            Spartan3_State_s <= wait_send_DAC_init_gain_ch0;
                        end if;                    
    
                    when skip_first_steps_ch0 =>                                                    -- wait BRAM latency 
                        Spartan3_State_s <= skip_first_steps1_ch0;
        
                    when skip_first_steps1_ch0 =>
                        Spartan3_State_s <= send_DAC_pairs_ch0;
    
                    when wait_send_first_steps_ch0 =>
                        if (m_wr_ack_o = '1' and m_wr_ack_o_d1 = '0') then
                            m_wren_i <= '1';
                            m_di_i <= '0' & '1' & "000" & std_logic_vector(to_signed(0,27));        -- first delta
                            Spartan3_State_s <= wait_send_first_delta_ch0;    
                        else
                            m_wren_i <= '0';
                            Spartan3_State_s <= wait_send_first_steps_ch0;
                        end if;                    
    
                    when wait_send_first_delta_ch0 =>
                        m_wren_i <= '0';
                        if (m_wr_ack_o = '1' and m_wr_ack_o_d1 = '0') then
                            count_points <= count_points + 1;
                            Spartan3_State_s <= send_DAC_pairs_ch0;    
                        else                            
                            Spartan3_State_s <= wait_send_first_delta_ch0;
                        end if;    
    
                    when send_DAC_pairs_ch0 =>
                        if ( count_points < total_points ) then
                            count_points <= count_points + 1;
                            ch0_dac_bram_addrb <= ch0_dac_bram_addrb + 1;
                            m_wren_i <= '1';
                            m_di_i <= '0' & '1' & "000" & ch0_dac_bram_doutb(26 downto 0);
                            Spartan3_State_s <= wait_send_DAC_pairs_steps_ch0;
                        else
                            m_wren_i <= '0';
                            Spartan3_State_s <= Spartan3_SendDACch1_c;        -- DAC curve done, return to channel loop
                        end if;    
    
                    when wait_send_DAC_pairs_steps_ch0 =>
                        if (m_wr_ack_o = '1' and m_wr_ack_o_d1 = '0') then
                            ch0_dac_bram_addrb <= ch0_dac_bram_addrb + 1;
                            m_wren_i <= '1';
                            m_di_i <= '0' & '1' & "000" & ch0_dac_bram_doutb(26 downto 0);
                            Spartan3_State_s <= wait_send_DAC_pairs_delta_ch0;    
                        else
                            m_wren_i <= '0';
                            Spartan3_State_s <= wait_send_DAC_pairs_steps_ch0;
                        end if;                    
    
                    when wait_send_DAC_pairs_delta_ch0 =>
                        m_wren_i <= '0';
                        if (m_wr_ack_o = '1' and m_wr_ack_o_d1 = '0') then
                            Spartan3_State_s <= send_DAC_pairs_ch0;    
                        else
                            Spartan3_State_s <= wait_send_DAC_pairs_delta_ch0;
                        end if;                    

                    when Spartan3_SendDACch1_c =>                     
                        if ( ch1_dac_bram_saved = '1' ) then
                            m_wren_i <= '1';
                            m_di_i <= '0' & '0' & std_logic_vector(to_unsigned(0,14)) & "000000" & "0010" & "0001" & "10";        -- control reg (address 0) for DAC update
                            Spartan3_State_s <= Spartan3_waitSendDACch1_header;
                            ch1_dac_bram_addrb <= (others => '0');
                        else
                            Spartan3_State_s <= send_DAC_tail;
                        end if;    
                            
                    when Spartan3_waitSendDACch1_header =>                                                        -- first data in DAC mem is points, using high "111" to mark start of DAC curve
                        if (m_wr_ack_o = '1' and m_wr_ack_o_d1 = '0') then
                            ch1_dac_bram_addrb <= ch1_dac_bram_addrb + std_logic_vector (to_unsigned(2,10));        -- set init gain address(+1 from head), next data
                            total_points <= unsigned( ch1_n_taps(7 downto 0) );
                            count_points <= to_unsigned(0,8);
                            m_wren_i <= '1';
                            m_di_i <= '0' & '1' & "111" & ch1_n_taps(26 downto 0);
                            Spartan3_State_s <= wait_send_DAC_points_ch1;
                        else
                            m_wren_i <= '0';
                            Spartan3_State_s <= Spartan3_waitSendDACch1_header;
                        end if;                    
    
                    when wait_send_DAC_points_ch1 =>
                        if (m_wr_ack_o = '1' and m_wr_ack_o_d1 = '0') then
                            ch1_dac_bram_addrb <= ch1_dac_bram_addrb + 1;                                        -- set first steps address, if first steps = 0 do not count this point
                            m_wren_i <= '1';
                            m_di_i <= '0' & '1' & "000" & ch1_dac_bram_doutb(26 downto 0);
                            Spartan3_State_s <= wait_send_DAC_init_gain_ch1;
                        else
                            m_wren_i <= '0';
                            Spartan3_State_s <= wait_send_DAC_points_ch1;
                        end if;                    
        
                    when wait_send_DAC_init_gain_ch1 =>
                        if (m_wr_ack_o = '1' and m_wr_ack_o_d1 = '0') then
                            ch1_dac_bram_addrb <= ch1_dac_bram_addrb + std_logic_vector (to_unsigned(2,10));
                            if ( first_steps = std_logic_vector ( to_unsigned(0,27) )) then            -- skip first pair (0,0)    first gain delta is always '0'
                                m_wren_i <= '0';
                                Spartan3_State_s <= skip_first_steps_ch1;
                            else
                                m_wren_i <= '1';
                                m_di_i <= '0' & '1' & "000" & first_steps;
                                Spartan3_State_s <= wait_send_first_steps_ch1;
                            end if;
                        else
                            first_steps <= ch1_dac_bram_doutb(26 downto 0);
                            m_wren_i <= '0';
                            Spartan3_State_s <= wait_send_DAC_init_gain_ch1;
                        end if;                    
    
                    when skip_first_steps_ch1 =>                                                    -- wait BRAM latency 
                        Spartan3_State_s <= skip_first_steps1_ch1;
        
                    when skip_first_steps1_ch1 =>
                        Spartan3_State_s <= send_DAC_pairs_ch1;
    
                    when wait_send_first_steps_ch1 =>
                        if (m_wr_ack_o = '1' and m_wr_ack_o_d1 = '0') then
                            m_wren_i <= '1';
                            m_di_i <= '0' & '1' & "000" & std_logic_vector(to_signed(0,27));        -- first delta
                            Spartan3_State_s <= wait_send_first_delta_ch1;    
                        else
                            m_wren_i <= '0';
                            Spartan3_State_s <= wait_send_first_steps_ch1;
                        end if;                    
    
                    when wait_send_first_delta_ch1 =>
                        m_wren_i <= '0';
                        if (m_wr_ack_o = '1' and m_wr_ack_o_d1 = '0') then
                            count_points <= count_points + 1;
                            Spartan3_State_s <= send_DAC_pairs_ch1;    
                        else                            
                            Spartan3_State_s <= wait_send_first_delta_ch1;
                        end if;    
    
                    when send_DAC_pairs_ch1 =>
                        if ( count_points < total_points ) then
                            count_points <= count_points + 1;
                            ch1_dac_bram_addrb <= ch1_dac_bram_addrb + 1;
                            m_wren_i <= '1';
                            m_di_i <= '0' & '1' & "000" & ch1_dac_bram_doutb(26 downto 0);
                            Spartan3_State_s <= wait_send_DAC_pairs_steps_ch1;
                        else
                            m_wren_i <= '0';
                            Spartan3_State_s <= send_DAC_tail;        -- DAC curve ch1 done, return to tail
                        end if;    
    
                    when wait_send_DAC_pairs_steps_ch1 =>
                        if (m_wr_ack_o = '1' and m_wr_ack_o_d1 = '0') then
                            ch1_dac_bram_addrb <= ch1_dac_bram_addrb + 1;
                            m_wren_i <= '1';
                            m_di_i <= '0' & '1' & "000" & ch1_dac_bram_doutb(26 downto 0);
                            Spartan3_State_s <= wait_send_DAC_pairs_delta_ch1;    
                        else
                            m_wren_i <= '0';
                            Spartan3_State_s <= wait_send_DAC_pairs_steps_ch1;
                        end if;                    
    
                    when wait_send_DAC_pairs_delta_ch1 =>
                        m_wren_i <= '0';
                        if (m_wr_ack_o = '1' and m_wr_ack_o_d1 = '0') then
                            Spartan3_State_s <= send_DAC_pairs_ch1;    
                        else
                            Spartan3_State_s <= wait_send_DAC_pairs_delta_ch1;
                        end if;                    

                    when send_DAC_tail =>
                        m_wren_i <= '1';
                        dac_bram_upd_ack <='1';
                        --if ( ch0_dac_bram_saved = '1' or ch1_dac_bram_saved = '1' ) then
                        if ( ch0_dac_bram_saved_i = '1' or ch1_dac_bram_saved_i = '1' ) then
                            m_di_i <= '0' & '0' & std_logic_vector(to_unsigned(0,14)) & std_logic_vector(to_unsigned(1,16));
                        else
                            m_di_i <= '0' & '0' & std_logic_vector(to_unsigned(0,14)) & std_logic_vector(to_unsigned(0,16));
                        end if;
                        Spartan3_State_s <= wait_send_DAC_tail;    
    
                    when wait_send_DAC_tail =>
                        m_wren_i <= '0';
                        if (m_wr_ack_o = '1' and m_wr_ack_o_d1 = '0') then
                            Spartan3_State_s <= Spartan3_Idle_c;    
                        else
                            Spartan3_State_s <= wait_send_DAC_tail;
                        end if;
                            
				end case;
			end if;
		end if;
	end process; 

        Inst_mult_gen_0: spartan3_control_multiplier
          Port map(
              A => STD_LOGIC_VECTOR(Channel_Gain(17 DOWNTO 0)),
              P => Conv_Gain1
          );
     
     Inst_spi_master_spartan3: spi_master_spartan3
        generic map (N => 32, CPOL => '0', CPHA => '0', PREFETCH => 3, SPI_2X_CLK_DIV => 1)
        port map( 
            sclk_i => CLOCK,                      -- system clock is used for serial and parallel ports
            pclk_i => CLOCK,
            rst_i => RESET,
            spi_ssel_o => RCVR_FPGA_CS,
            spi_sck_o => RCVR_FPGA_SCLK,
            spi_mosi_o => RCVR_FPGA_SDI,
            spi_miso_i => RCVR_FPGA_SDO,
            di_req_o => m_di_req_o,
            di_i => m_di_i,
            wren_i => m_wren_i,
            wr_ack_o => m_wr_ack_o,
            do_valid_o => m_do_valid_o,
            do_o => m_do_o
            ----- debug -----
--            do_transfer_o => m_do_transfer_o,
--            wren_o => m_wren_o,
--            rx_bit_reg_o => m_rx_bit_reg_o,
--            state_dbg_o => m_state_dbg_o,
--            core_clk_o => m_core_clk_o,
--            core_n_clk_o => m_core_n_clk_o
--            sh_reg_dbg_o => m_sh_reg_dbg_o
        );  
        
    -- Write DAC curve config
    ch0_dac_BRAM : process(CLOCK) 
        variable var_counter : integer range 0 to 32768 :=0; 
    begin
        if (rising_edge(CLOCK)) then
            if (RESET = '1') then
                ch0_dac_bram_wea      <= (others => '0');
                ch0_dac_bram_addra    <= (others => '0');
                ch0_dac_bram_dina     <= (others => '0');
				ch0_dac_bram_upd 	  <= '0';
				ch0_dac_bram_saved 	  <= '0';
                ch0_config_dac_axis_tlast_d1 <= '0';
                ch0_config_dac_axis_tlast_d2 <= '0';
				CH0_CONFIG_DAC_AXIS_TREADY   <= '0';
				ch0_n_taps 	      <= (others => '0');
				var_counter       := 0;
                -- State
                states_bram_sm_a <= idle;
            else     
                ch0_config_dac_axis_tlast_d1 <= CH0_CONFIG_DAC_AXIS_TLAST; 
                ch0_config_dac_axis_tlast_d2 <= ch0_config_dac_axis_tlast_d1; 
                case states_bram_sm_a is        
                    when idle =>                  
                        ch0_dac_bram_addra <= (others => '0');     
						ch0_dac_bram_wea   <= (others => '0');
						CH0_CONFIG_DAC_AXIS_TREADY <= '1';
						var_counter       := 0;
                        if(CH0_CONFIG_DAC_AXIS_TVALID = '1') then
                            ch0_dac_bram_saved <= '0';
							ch0_n_taps <= CH0_CONFIG_DAC_AXIS_TDATA;
                            states_bram_sm_a <= update_header;
                        elsif( dac_bram_upd_ack = '1' ) then
                            ch0_dac_bram_upd <= '0';
                        end if;
					when update_header => 
						if(CH0_CONFIG_DAC_AXIS_TVALID = '1') then						
							-- Data discarded (reserved)
							states_bram_sm_a <= store_dac_curve;
					    else
					       if(unsigned(ch0_n_taps) = 0) then
					           --states_bram_sm_a <= idle;
					           states_bram_sm_a <= store_dac_curve;
					       end if;	
						end if;
                    when store_dac_curve =>
                        if(CH0_CONFIG_DAC_AXIS_TVALID = '1' and unsigned(ch0_n_taps) < 256) then
							ch0_dac_bram_addra <= std_logic_vector(unsigned(ch0_dac_bram_addra)+1);	
                            ch0_dac_bram_dina  <= CH0_CONFIG_DAC_AXIS_TDATA;
                            if(unsigned(ch0_n_taps) > 0) then                               
                                ch0_dac_bram_saved <= '1'; 
                            end if;
                            ch0_dac_bram_wea   <= (others => '1'); 
                        else
                            if(ch0_config_dac_axis_tlast_d2 = '1' or unsigned(ch0_n_taps) = 0) then
                                ch0_dac_bram_upd <= '1';
                                states_bram_sm_a <= idle;
                            end if;                  
                        end if;  
                    when others => 
                        null;
                end case;                                 
            end if;
        end if;
    end process;
			
    -- Write DAC curve config
    ch1_dac_BRAM : process(CLOCK) 
        variable var_counter : integer range 0 to 32768 :=0; 
    begin
        if (rising_edge(CLOCK)) then
            if (RESET = '1') then
                ch1_dac_bram_wea      <= (others => '0');
                ch1_dac_bram_addra    <= (others => '0');
                ch1_dac_bram_dina     <= (others => '0');
                ch1_dac_bram_upd 	  <= '0';
                ch1_dac_bram_saved    <= '0';
                ch1_config_dac_axis_tlast_d1 <= '0';
                ch1_config_dac_axis_tlast_d2 <= '0';
                CH1_CONFIG_DAC_AXIS_TREADY   <= '0';
                ch1_n_taps        <= (others => '0');
                var_counter       := 0;
                -- State
                states_bram_sm_b <= idle;
            else     
                ch1_config_dac_axis_tlast_d1 <= CH1_CONFIG_DAC_AXIS_TLAST; 
                ch1_config_dac_axis_tlast_d2 <= ch1_config_dac_axis_tlast_d1; 
                case states_bram_sm_b is        
                    when idle =>                  
                        ch1_dac_bram_addra <= (others => '0');     
                        ch1_dac_bram_wea   <= (others => '0');                    
                        CH1_CONFIG_DAC_AXIS_TREADY <= '1';
                        if(CH1_CONFIG_DAC_AXIS_TVALID = '1') then
                            ch1_dac_bram_saved <= '0';
                            ch1_n_taps <= CH1_CONFIG_DAC_AXIS_TDATA;
                            states_bram_sm_b <= update_header;
                        elsif( dac_bram_upd_ack = '1' ) then
                            ch1_dac_bram_upd <= '0';
                        end if;
                        var_counter       := 0;
                    when update_header => 
                        if(CH1_CONFIG_DAC_AXIS_TVALID = '1') then                        
                            -- Data discarded (reserved)
                            states_bram_sm_b <= store_dac_curve;
                        else
                           if(unsigned(ch1_n_taps) = 0) then
                               states_bram_sm_b <= store_dac_curve;
                           end if;    
                        end if;
                    when store_dac_curve =>
                        if(CH1_CONFIG_DAC_AXIS_TVALID = '1' and unsigned(ch1_n_taps) < 256) then
                            ch1_dac_bram_addra <= std_logic_vector(unsigned(ch1_dac_bram_addra)+1);    
                            ch1_dac_bram_dina  <= CH1_CONFIG_DAC_AXIS_TDATA;
                            if(unsigned(ch1_n_taps) > 0) then                             
                                ch1_dac_bram_saved <= '1'; 
                            end if;
                            ch1_dac_bram_wea   <= (others => '1');  
                        else
                            if(ch1_config_dac_axis_tlast_d2 = '1' or unsigned(ch1_n_taps) = 0) then
                                ch1_dac_bram_upd <= '1';
                                states_bram_sm_b <= idle;
                            end if;                  
                        end if;  
                    when others => 
                        null;
                end case;                                 
            end if;
        end if;
    end process;
			
    ch0_spartan3_control_BRAM_inst : spartan3_control_BRAM
        port map (
            clka => CLOCK,
            wea => ch0_dac_bram_wea,
            addra => ch0_dac_bram_addra,
            dina => ch0_dac_bram_dina,
            clkb => CLOCK,
            addrb => ch0_dac_bram_addrb,
            doutb => ch0_dac_bram_doutb
        );
        
    ch1_spartan3_control_BRAM_inst : spartan3_control_BRAM
        port map (
            clka => CLOCK,
            wea => ch1_dac_bram_wea,
            addra => ch1_dac_bram_addra,
            dina => ch1_dac_bram_dina,
            clkb => CLOCK,
            addrb => ch1_dac_bram_addrb,
            doutb => ch1_dac_bram_doutb
        );        
  
end spartan3_gain_filt_dac_arch;