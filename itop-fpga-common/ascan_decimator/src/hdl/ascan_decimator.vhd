----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 09/25/2018 10:07:23 AM
-- Design Name: 
-- Module Name: ascan decimator - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ascan_decimator is
	generic (
        REGISTER_W32        : INTEGER := 32;
        REGISTER_W16        : INTEGER := 16;
        REGISTER_W14        : INTEGER := 14;
		WAIT_CYCLES        	: INTEGER := 32;
		N_INPUT_DELAY       : INTEGER := 8;
		N_OUTPUT_DELAY      : INTEGER := 4;
		DEC_TYPE_NONE    	: INTEGER := 0;
        DEC_TYPE_SIMPLE     : INTEGER := 1;
        DEC_TYPE_MINMAX     : INTEGER := 2;
		DEC_TYPE_ALOK     	: INTEGER := 3
	);
	port (
	    -- Sync
	    aclk         : in STD_LOGIC;
        aresetn      : in STD_LOGIC;
	
	    -- Control
	    DEC_TYPE	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);    
	    DEC_LEVEL	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	    DEC_WINDOW	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	    DATA_WINDOW	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	    DEC_EN          : out STD_LOGIC;
	    		
	    DEC_TYPE_RESULT	    : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);    
	    DEC_LEVEL_RESULT	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	    DEC_WINDOW_RESULT	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		-- Signal input
		SIGNAL_INPUT_AXIS_TREADY  : out STD_LOGIC;
		SIGNAL_INPUT_AXIS_TDATA	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		SIGNAL_INPUT_AXIS_TMAX	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		SIGNAL_INPUT_AXIS_TLAST	  : in STD_LOGIC;
		SIGNAL_INPUT_AXIS_TVALID  : in STD_LOGIC;

		-- Average result
		DEC_RESULT_AXIS_TVALID    : out STD_LOGIC;
		DEC_RESULT_AXIS_TDATA	  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		DEC_RESULT_AXIS_X	      : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		DEC_RESULT_AXIS_TLAST	  : out STD_LOGIC;
		DEC_RESULT_AXIS_TREADY    : in STD_LOGIC
	);
end ascan_decimator;

architecture arch_imp of ascan_decimator is

    -- Delayed signal input
    type signal_input_axis_tdata_array is array ( 0 to N_INPUT_DELAY-1 ) of STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal signal_input_axis_tdata_i    : signal_input_axis_tdata_array;
    signal signal_input_axis_tlast_i    : STD_LOGIC_VECTOR(N_INPUT_DELAY-1 downto 0);
    signal signal_input_axis_tvalid_i   : STD_LOGIC_VECTOR(N_INPUT_DELAY-1 downto 0);

    -- Signals output
    type dec_result_axis_tdata_array is array ( 0 to N_OUTPUT_DELAY-1 ) of STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal dec_result_axis_tdata_i    : dec_result_axis_tdata_array;
    signal dec_result_axis_tlast_i    : STD_LOGIC_VECTOR(N_OUTPUT_DELAY-1 downto 0);
    signal dec_result_axis_tvalid_i   : STD_LOGIC_VECTOR(N_OUTPUT_DELAY-1 downto 0);
        
    --Decimation state machine
    type states_general_sm is (idle, by_pass_state, decimation_simple, decimation_minmax, decimation_alok, decimation_end); 
    signal state_general_sm : states_general_sm;
   
    signal first_flag  : STD_LOGIC;

	signal by_pass  : STD_LOGIC;

	-- Aux Signals	
	signal dec_type_i	 	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal dec_level_i	 	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal dec_window_i 	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal dec_window_counter 	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal data_window_i 	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

	-- Min Max
	signal min_x : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal min_y : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal max_x : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal max_y : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);

	signal min_x_temp : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal min_y_temp : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal max_x_temp : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal max_y_temp : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);

	signal x_counter : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal min_max_result_flag  : STD_LOGIC;
	signal min_max_result_last  : STD_LOGIC;
	signal min_max_result_last_i  : STD_LOGIC_VECTOR(7 downto 0);

	-- Min Max
	signal result_x1 : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal result_y1 : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal result_x2 : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal result_y2 : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	
	-- ALOK
	signal alok_threshold : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal alok_threshold_1 : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal alok_threshold_2 : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal alok_result_flag  : STD_LOGIC;
	signal alok_result_last  : STD_LOGIC;

begin
						      						
    aux_process: process(aclk) 
		variable var_counter  : integer range 0 to 65536 :=0;
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				DEC_TYPE_RESULT  	<= (others => '0');
				DEC_LEVEL_RESULT    <= (others => '0');
				DEC_WINDOW_RESULT   <= (others => '0');	
				signal_input_axis_tdata_i(0) 	<= (others => '0');	
				signal_input_axis_tlast_i(0) 	<= '0';
				signal_input_axis_tvalid_i(0) 	<= '0';
				DEC_RESULT_AXIS_TDATA 	<= (others => '0');
				DEC_RESULT_AXIS_X 	    <= (others => '0');	
				DEC_RESULT_AXIS_TLAST 	<= '0';
				DEC_RESULT_AXIS_TVALID 	<= '0';
				alok_threshold_1        <= (others => '0');
				alok_threshold_2        <= (others => '0');
				min_max_result_last_i(0) <= '0';					
        	else
				DEC_TYPE_RESULT 	<= dec_type_i;			
    			DEC_LEVEL_RESULT 	<= dec_level_i;	
				DEC_WINDOW_RESULT 	<= dec_window_i;	
				signal_input_axis_tdata_i(0) 	<= SIGNAL_INPUT_AXIS_TDATA;
				signal_input_axis_tlast_i(0) 	<= SIGNAL_INPUT_AXIS_TLAST;
				signal_input_axis_tvalid_i(0) 	<= SIGNAL_INPUT_AXIS_TVALID;
				min_max_result_last_i(0)		<= min_max_result_last;		
				alok_threshold_1 <= std_logic_vector(unsigned(SIGNAL_INPUT_AXIS_TMAX) srl 7); -- 0.78125% 	
				alok_threshold_2 <= std_logic_vector(unsigned(SIGNAL_INPUT_AXIS_TMAX) srl 9); -- 0.1953% 	 	 																   
				if(by_pass = '1') then
					DEC_RESULT_AXIS_TVALID 	<= signal_input_axis_tvalid_i(0);
					DEC_RESULT_AXIS_TLAST 	<= signal_input_axis_tlast_i(0);
					DEC_RESULT_AXIS_TDATA 	<= signal_input_axis_tdata_i(0);																		
				else
					if(unsigned(dec_type_i) = DEC_TYPE_SIMPLE) then
						DEC_RESULT_AXIS_TVALID 	<= dec_result_axis_tvalid_i(0) or dec_result_axis_tvalid_i(1);
						if(dec_result_axis_tvalid_i(0) = '1') then
						    DEC_RESULT_AXIS_TDATA <= std_logic_vector(unsigned(x_counter) - 1);
						elsif(dec_result_axis_tvalid_i(1) = '1') then
						    DEC_RESULT_AXIS_TDATA <= dec_result_axis_tdata_i(1);
						else
						    DEC_RESULT_AXIS_TDATA <= dec_result_axis_tdata_i(0);
						end if;
--						DEC_RESULT_AXIS_TLAST 	<= dec_result_axis_tlast_i(0);	
						DEC_RESULT_AXIS_TLAST 	<= dec_result_axis_tlast_i(1);																			   
					elsif(unsigned(dec_type_i) = DEC_TYPE_MINMAX ) then
						if((min_max_result_flag = '1' and min_max_result_last = '0') or min_max_result_last_i(4) = '1') then
							DEC_RESULT_AXIS_TVALID 	<= '1';		
							if(min_x < max_x) then
								result_x1 <= min_x;			
								result_y1 <= min_y;	
								result_x2 <= max_x;	
								result_y2 <= max_y;	
								DEC_RESULT_AXIS_TDATA <= min_x;											   
							else
								result_x1 <= max_x;			
								result_y1 <= max_y;	
								result_x2 <= min_x;	
								result_y2 <= min_y;	
								DEC_RESULT_AXIS_TDATA <= max_x;														   
							end if;
							var_counter:= 1;																		   
						else
							if(var_counter = 1) then
								DEC_RESULT_AXIS_TVALID 	<= '1';										   
								DEC_RESULT_AXIS_TDATA <= result_y1;		
								var_counter:= var_counter+1;											   
							elsif(var_counter = 2) then
								DEC_RESULT_AXIS_TVALID 	<= '1';										   
								DEC_RESULT_AXIS_TDATA <= result_x2;											   
								var_counter:= var_counter+1;											   
							elsif(var_counter = 3) then	
								DEC_RESULT_AXIS_TVALID 	<= '1';										   
								DEC_RESULT_AXIS_TDATA <= result_y2;		
								if(min_max_result_last_i(7) = '1') then
									DEC_RESULT_AXIS_TLAST 	<= '1';											   
								end if;
								var_counter:= var_counter+1;																		   
							else 											   
								-- NOTHING		
								DEC_RESULT_AXIS_TVALID 	<= '0';	
								DEC_RESULT_AXIS_TLAST 	<= '0';											   
							end if;												   
						end if;	
					elsif(unsigned(dec_type_i) = DEC_TYPE_ALOK ) then
						if(alok_result_flag = '1') then
                            DEC_RESULT_AXIS_TVALID <= '1'; 
                            DEC_RESULT_AXIS_X <= std_logic_vector(unsigned(x_counter) - 1);
                            if(signed(signal_input_axis_tdata_i(4)) >= 0) then
                                DEC_RESULT_AXIS_TDATA <= signal_input_axis_tdata_i(4); 
                            else
                                DEC_RESULT_AXIS_TDATA <= std_logic_vector(-signed(signal_input_axis_tdata_i(4))); 
                            end if;
                            if(alok_result_last = '1') then
                                DEC_RESULT_AXIS_TLAST <= '1';
                            else
                                DEC_RESULT_AXIS_TLAST <= '0';
                            end if;  
                        else
                            DEC_RESULT_AXIS_TVALID <= '0';
                            DEC_RESULT_AXIS_TLAST <= '0';  
                        end if;    
					end if;														   
				end if;
			end if;
		end if;
	end process;
																		   
    -- Delay signal input     
    delay_min_max_result_last : for i in 1 to 7 generate
        process(aclk) 
        begin
           if (rising_edge(aclk)) then
                if (aresetn = '0') then
                    min_max_result_last_i(i)    <= '0';
                else
                    min_max_result_last_i(i) <= min_max_result_last_i(i-1);
                end if;
           end if;
        end process;
    end generate;															   
																		   																				  												 																    														
    -- Decimation SM
    decimation_inst: process(aclk) 
        variable var_counter  : integer range 0 to 65536 :=0;
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				-- tready														   
				SIGNAL_INPUT_AXIS_TREADY <= '0';
				DEC_EN  <= '0';
				-- flags														   
				first_flag	<= '0';	
				by_pass		<= '0';
				-- Aux														   
				dec_type_i	<= (others => '0');
				dec_level_i	<= (others => '0');
				dec_window_i<= (others => '0');	
				dec_window_counter <= (others => '0');	
				-- Result														   
				dec_result_axis_tdata_i(0) 	<= (others => '0');	
				dec_result_axis_tlast_i(0) <= '0';
				dec_result_axis_tvalid_i(0) <= '0';	
				-- Min Max
				min_x_temp	<= (others => '0');	
				min_y_temp	<= (others => '0');	
				max_x_temp	<= (others => '0');	
				max_y_temp	<= (others => '0');
				x_counter	<= (others => '0');	
				min_max_result_flag <= '0';	
				min_max_result_last	<= '0';
				-- ALOK
				alok_result_flag <= '0';
				alok_result_last <= '1';
				alok_threshold  <= (others => '0');														   
				-- State														   
				state_general_sm	<= idle;
        	else
				case state_general_sm is        
					when idle =>
						var_counter := 0;										  
						dec_type_i <= DEC_TYPE;
						dec_level_i <= DEC_LEVEL;
						--dec_window_i <= DEC_WINDOW;
						data_window_i <= (others => '0');	
						dec_window_counter <= (others => '0');		
						if(unsigned(DEC_TYPE) = DEC_TYPE_NONE) then													   
							SIGNAL_INPUT_AXIS_TREADY <= '0';	
							DEC_EN  <= '0';
						else									    
							SIGNAL_INPUT_AXIS_TREADY <= DEC_RESULT_AXIS_TREADY;
							if(unsigned(DEC_TYPE) = DEC_TYPE_SIMPLE and unsigned(DEC_LEVEL) >= 2) then
							    DEC_EN  <= '1';
							elsif(unsigned(DEC_TYPE) = DEC_TYPE_MINMAX and unsigned(DEC_LEVEL) >= 4) then
							    DEC_EN  <= '1';
						    elsif(unsigned(DEC_TYPE) = DEC_TYPE_ALOK) then
						        DEC_EN  <= '1';
							else
							    SIGNAL_INPUT_AXIS_TREADY <= '0';
							    DEC_EN  <= '0';
							end if;
						end if;													   
						by_pass	<= '0';
						first_flag <= '1';	
						dec_result_axis_tdata_i(0) 	<= (others => '0');	
						dec_result_axis_tlast_i(0) <= '0';
						dec_result_axis_tvalid_i(0) <= '0';	
						min_max_result_flag <= '0';	
						min_max_result_last	<= '0';		
						alok_result_flag <= '0';
						alok_result_last <= '0';
						-- threshol = 1% MAX
						alok_threshold <= std_logic_vector(signed(alok_threshold_1)+signed(alok_threshold_2));                                   
						x_counter <= (others => '0');																   
						if(SIGNAL_INPUT_AXIS_TVALID = '1') then
							if(unsigned(DEC_TYPE) = DEC_TYPE_SIMPLE and unsigned(DEC_LEVEL) >= 2) then
								state_general_sm <= decimation_simple;								
							elsif(unsigned(DEC_TYPE) = DEC_TYPE_MINMAX and unsigned(DEC_LEVEL) >= 4) then																			   
								state_general_sm <= decimation_minmax;	
						    elsif(unsigned(DEC_TYPE) = DEC_TYPE_ALOK) then
						        state_general_sm <= decimation_alok;
							else
								state_general_sm <= by_pass_state;		
							end if;							
						end if;
                    when by_pass_state => 
                        SIGNAL_INPUT_AXIS_TREADY <= DEC_RESULT_AXIS_TREADY;
						by_pass	<= '1';	
                        if(SIGNAL_INPUT_AXIS_TLAST = '1') then
                        	state_general_sm <= decimation_end;    
                        end if; 
					when decimation_simple =>
						SIGNAL_INPUT_AXIS_TREADY <= DEC_RESULT_AXIS_TREADY;													   
						if(DEC_RESULT_AXIS_TREADY = '1' and signal_input_axis_tvalid_i(0) = '1') then
							if(first_flag = '1') then
								first_flag <= '0';
								dec_result_axis_tvalid_i(0) <= '1';	
								dec_result_axis_tdata_i(0) 	<= signal_input_axis_tdata_i(0);		
								dec_window_counter <= std_logic_vector(to_unsigned(1, dec_window_counter'length));										   
							else						    
								if(var_counter >= (unsigned(dec_level_i)-1)) then
									var_counter:= 0;
									dec_result_axis_tvalid_i(0) <= '1';	
									dec_result_axis_tdata_i(0) 	<= signal_input_axis_tdata_i(0);
									dec_window_counter <= std_logic_vector(unsigned(dec_window_counter) + 1);
									if(signal_input_axis_tlast_i(0) = '1') then	
									   dec_result_axis_tlast_i(0)  <= '1';
									   state_general_sm <= decimation_end;   
									end if;	
							    elsif(signal_input_axis_tlast_i(0) = '1') then		
									dec_result_axis_tvalid_i(0) <= '1';	
                                    dec_result_axis_tlast_i(0)  <= '1';
                                    dec_result_axis_tdata_i(0)  <= signal_input_axis_tdata_i(0);                              
                                    state_general_sm <= decimation_end;      										  
								else
									dec_result_axis_tvalid_i(0) <= '0';										   
									var_counter:= var_counter + 1;											   
								end if;								
							end if;
							x_counter <= std_logic_vector(unsigned(x_counter) + 1);							
						end if;
					when decimation_minmax =>
						SIGNAL_INPUT_AXIS_TREADY <= DEC_RESULT_AXIS_TREADY;													   
						if(DEC_RESULT_AXIS_TREADY = '1' and signal_input_axis_tvalid_i(0) = '1') then			
							if(first_flag = '1') then
								first_flag <= '0';
								min_x_temp	<= x_counter;	
								min_y_temp	<= signal_input_axis_tdata_i(0);
								max_x_temp	<= x_counter;	
								max_y_temp	<= signal_input_axis_tdata_i(0);	
								var_counter := 1;												  
							else
								if(var_counter >= (unsigned(dec_level_i))) then
									var_counter := 1;	
									min_x	<= min_x_temp;	
									min_y	<= min_y_temp;
									max_x	<= max_x_temp;
									max_y	<= max_y_temp;										  
									min_x_temp	<= x_counter;	
									min_y_temp	<= signal_input_axis_tdata_i(0);
									max_x_temp	<= x_counter;	
									max_y_temp	<= signal_input_axis_tdata_i(0);
									if(signal_input_axis_tlast_i(0) = '1') then	
									   min_max_result_last	<= '1';
									   state_general_sm <= decimation_end;
									end if;								
									min_max_result_flag <= '1';
									dec_window_counter <= std_logic_vector(unsigned(dec_window_counter) + 1);		
								else
								    if(signal_input_axis_tlast_i(0) = '1') then
                                        min_max_result_flag <= '1';	
                                        min_max_result_last	<= '1';
                                        dec_window_counter <= std_logic_vector(unsigned(dec_window_counter) + 1);											  
                                        if(signed(signal_input_axis_tdata_i(0)) < signed(min_y_temp)) then
                                            min_x <= x_counter;
                                            min_y <= signal_input_axis_tdata_i(0);       
                                        else
									       min_x  <= min_x_temp;	
                                           min_y  <= min_y_temp;                       
                                        end if;
                                        if(signed(signal_input_axis_tdata_i(0)) > signed(max_y_temp)) then
                                            max_x <= x_counter;
                                            max_y <= signal_input_axis_tdata_i(0);    
                                        else
									        max_x <= max_x_temp;
                                            max_y <= max_y_temp;                            
                                        end if;                                          
                                        state_general_sm <= decimation_end;  
								    else
                                        min_max_result_flag <= '0';												  
                                        if(signed(signal_input_axis_tdata_i(0)) < signed(min_y_temp)) then
                                            min_x_temp <= x_counter;
                                            min_y_temp <= signal_input_axis_tdata_i(0);								  
                                        end if;
                                        if(signed(signal_input_axis_tdata_i(0)) > signed(max_y_temp)) then
                                            max_x_temp <= x_counter;
                                            max_y_temp <= signal_input_axis_tdata_i(0);								  
                                        end if;										  
                                        var_counter:= var_counter + 1;		
                                    end if;								  
								end if;
							end if;	
							x_counter <= std_logic_vector(unsigned(x_counter) + 1);														  
						end if;
				    when decimation_alok =>
				        SIGNAL_INPUT_AXIS_TREADY <= DEC_RESULT_AXIS_TREADY;	
				        if(DEC_RESULT_AXIS_TREADY = '1' and signal_input_axis_tvalid_i(3) = '1') then
				            if(first_flag = '1') then
				                first_flag <= '0';
				                var_counter:= 0;
				            else
				                if((var_counter >= 3) and
				                   (var_counter < unsigned(DATA_WINDOW)-3) and
				                   (signed(signal_input_axis_tdata_i(5)) <= signed(signal_input_axis_tdata_i(3))) and
				                   (signed(signal_input_axis_tdata_i(4)) <= signed(signal_input_axis_tdata_i(3))) and
				                   (signed(signal_input_axis_tdata_i(6)) <  signed(signal_input_axis_tdata_i(3))) and
				                   --(signed(signal_input_axis_tdata_i(6)) <  signed(signal_input_axis_tdata_i(3))) and
				                   (signed(signal_input_axis_tdata_i(0)) <= signed(signal_input_axis_tdata_i(3))) and
				                   (signed(signal_input_axis_tdata_i(2)) <= signed(signal_input_axis_tdata_i(3))) and
				                   (signed(signal_input_axis_tdata_i(3)) > signed(alok_threshold)) 
				                   ) then
				                    alok_result_flag <= '1';   
				                    dec_window_counter <= std_logic_vector(unsigned(dec_window_counter) + 1);
				                else
				                    alok_result_flag <= '0';   
				                end if;   
				                if( signal_input_axis_tlast_i(3) = '1' or var_counter >= unsigned(DATA_WINDOW)-3) then	
				                    alok_result_last <= '1';
				                    alok_result_flag <= '1';
                                    state_general_sm <= decimation_end;
                                end if;    
				                var_counter:= var_counter + 1;
				            end if;
				            x_counter <= std_logic_vector(unsigned(x_counter) + 1);
				        end if;		          			    
					when decimation_end =>
						SIGNAL_INPUT_AXIS_TREADY <= '0';
						dec_result_axis_tvalid_i(0) <= '0';	
                        dec_result_axis_tlast_i(0)  <= '0';	
                        min_max_result_last	<= '0';		
                        min_max_result_flag <= '0';	
                        alok_result_flag <= '0';
                        alok_result_last <= '0';
						if(unsigned(DEC_TYPE) = DEC_TYPE_SIMPLE) then
                            dec_window_i <= std_logic_vector(unsigned(dec_window_counter) sll 1);                             
                        elsif(unsigned(DEC_TYPE) = DEC_TYPE_MINMAX) then
                            dec_window_i <= std_logic_vector(unsigned(dec_window_counter) sll 2);
						elsif(unsigned(DEC_TYPE) = DEC_TYPE_ALOK) then
                            dec_window_i <= std_logic_vector(unsigned(dec_window_counter) sll 1); 
                        else
                            dec_window_i <= dec_window_counter;            
                        end if;    																  
						if(var_counter >= (WAIT_CYCLES)) then
							state_general_sm <= idle;														  
						else
							var_counter:= var_counter + 1;													  
						end if;
					when others =>
						null;
				end case;
			end if;
         end if;
    end process;
			           
    -- Delay signal input     
    delay_signal_input : for i in 1 to N_INPUT_DELAY-1 generate
        process(aclk) 
        begin
           if (rising_edge(aclk)) then
                if (aresetn = '0') then
                    signal_input_axis_tdata_i(i)    <= (others => '0');
                    signal_input_axis_tlast_i(i)    <= '0';
                    signal_input_axis_tvalid_i(i)   <= '0';
                else
                    signal_input_axis_tdata_i(i) <= signal_input_axis_tdata_i(i-1);
                    signal_input_axis_tlast_i(i) <= signal_input_axis_tlast_i(i-1);
                    signal_input_axis_tvalid_i(i) <= signal_input_axis_tvalid_i(i-1);
                end if;
           end if;
        end process;
    end generate;
																			  
    -- Delay signal output     
    delay_signal_output : for i in 1 to N_OUTPUT_DELAY-1 generate
        process(aclk) 
        begin
           if (rising_edge(aclk)) then
                if (aresetn = '0') then
                    dec_result_axis_tdata_i(i)    <= (others => '0');
                    dec_result_axis_tlast_i(i)    <= '0';
                    dec_result_axis_tvalid_i(i)   <= '0';
                else
                    dec_result_axis_tdata_i(i) <= dec_result_axis_tdata_i(i-1);
                    dec_result_axis_tlast_i(i) <= dec_result_axis_tlast_i(i-1);
                    dec_result_axis_tvalid_i(i) <= dec_result_axis_tvalid_i(i-1);
                end if;
           end if;
        end process;
    end generate;																  																			  
																			      	   
end arch_imp;
