----------------------------------------------------------------------------------
-- Company: Innerspec Technologies
-- Engineers: Swapna Muralidhar
--				  Zubin Kumar	
--				Wei Jiang
-- 
-- Design Name: 
-- Module Name: TX_OUTPUTS - Behavioral 
-- Project Name:
-- Target Devices: Virtex 5 xc5vsx50t-1ff1136
-- Tool versions: 
-- Description: Swapna Muralidhar - Generate TTL outputs for TX signal from 200 MHz clock
--					Zubin Kumar - Reduced dead time between positive and negative cycles
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Revision 0.02 - Rewrite to get rid of divider, wei jiang
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE; 
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_SIGNED.ALL;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity mrut_tx_outputs is
	generic (
		REGISTER_W30    : INTEGER := 30;
		REGISTER_W8     : INTEGER := 8;
		REGISTER_W2		: INTEGER := 2 
	);
	port (
		-- Sync
		aclk		: in STD_LOGIC;
		aresetn		: in STD_LOGIC;
		
		-- Inputs
		TX_ENABLE		: in STD_LOGIC;
		START_CALC		: in STD_LOGIC;
		START_TX		: in STD_LOGIC;
		HALF_CYCLE_FREQ	: in STD_LOGIC_VECTOR(REGISTER_W30-1 downto 0);
		NO_HALF_CYCLES	: in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
		START_CYCLE		: in STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
		
		-- Outputs
		TX_PLUS			: out STD_LOGIC;
		TX_MINUS		: out STD_LOGIC
	);
end mrut_tx_outputs;

architecture Behavioral of mrut_tx_outputs is

signal half_cycle_peroid_ps : STD_LOGIC_VECTOR(REGISTER_W30-1 downto 0) := (others => '0');
signal fpga_txclk_period_ps : STD_LOGIC_VECTOR(REGISTER_W30-1 downto 0) := (others => '0');
signal half_cycle_deadtime_ps: STD_LOGIC_VECTOR(REGISTER_W30-1 downto 0) := (others => '0');

type states is (idle_state, plus_state, minus_state, end_state, 
					dead_time_state1, dead_time_state2, dead_time_state3);
signal sm_state : states;

signal count_down_timer : STD_LOGIC_VECTOR(REGISTER_W30-1 downto 0) := (others => '0');
signal count_cycles : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0) := (others => '0');

signal tx_plus1_minus0_i : STD_LOGIC := '0';

begin

	half_cycle_peroid_ps <= HALF_CYCLE_FREQ;

	fpga_txclk_period_ps <= "000000000000000010011100010000";	-- 10,000 (10 ns x 1000) 100 MHz clock
	--half_cycle_deadtime_ps <= "000000000000000100111000100000";	-- 30,000 ( 20 ns total deadtime) 
	half_cycle_deadtime_ps <= "000000000000000111010100110000";	-- 30,000 ( 20 ns total deadtime) 
	
	process (aclk)
	begin
		if (rising_edge(aclk)) then
			if (aresetn = '0') then
			    TX_PLUS <= '0';
                TX_MINUS <= '0';
				sm_state <= idle_state;
			else
				case sm_state is
					when idle_state =>

						count_cycles <= (others => '0');
						TX_PLUS <= '0';
						TX_MINUS <= '0';
						if (TX_ENABLE = '1' and START_TX = '1') then
							if (START_CYCLE = "01") then
								-- 1 = start from negative
								sm_state <= minus_state;
							else
								sm_state <= plus_state;
							end if;
							count_down_timer <= half_cycle_peroid_ps;
						else
							sm_state <= idle_state;
						end if;

					when plus_state =>

						tx_plus1_minus0_i <= '1';
						TX_PLUS <= '1';
						TX_MINUS <= '0';
						count_down_timer <= count_down_timer - fpga_txclk_period_ps;
						if ( count_down_timer > half_cycle_deadtime_ps ) then
							sm_state <= plus_state;
						else
							sm_state <= dead_time_state1;
						end if;

					when dead_time_state1 =>

						TX_PLUS <= '0';
						TX_MINUS <= '0';
						count_down_timer <= count_down_timer - fpga_txclk_period_ps;
						if ( count_down_timer > '0' & fpga_txclk_period_ps(REGISTER_W30-1 downto 1) ) then
							sm_state <= dead_time_state1;
						else
							sm_state <= dead_time_state2;
							--sm_state <= dead_time_state3;
							count_cycles <= count_cycles + 1;
						end if;
						
				    when dead_time_state2 =>
                        sm_state <= dead_time_state3;
                        
					when dead_time_state3 =>

						TX_PLUS <= '0';
						TX_MINUS <= '0';
						count_down_timer <= count_down_timer + half_cycle_peroid_ps;
						if (count_cycles < NO_HALF_CYCLES) then
							if (tx_plus1_minus0_i = '1') then
								sm_state <= minus_state;
							else
								sm_state <= plus_state;
							end if;
						else
							sm_state <= end_state;
						end if;

					when minus_state =>

						tx_plus1_minus0_i <= '0';
						TX_PLUS <= '0';
						TX_MINUS <= '1';
						count_down_timer <= count_down_timer - fpga_txclk_period_ps;
						if ( count_down_timer > half_cycle_deadtime_ps ) then
							sm_state <= minus_state;
						else
							sm_state <= dead_time_state1;
						end if;

					when end_state =>

						TX_PLUS <= '0';
						TX_MINUS <= '0';
						if (START_TX = '1') then
							sm_state <= end_state;
						else
							sm_state <= idle_state;
						end if;

					when others =>

						TX_PLUS <= '0';
						TX_MINUS <= '0';				
						sm_state <= idle_state;

				end case;
			end if;
		end if;
	end process;	
	

-----------------------------------------------------------------------------
end Behavioral;

