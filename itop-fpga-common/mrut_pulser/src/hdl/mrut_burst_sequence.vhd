----------------------------------------------------------------------------------
-- Company: Innerspec Technologies
-- Engineers: Wei Jiang
-- 
-- Design Name: 
-- Module Name: BURST_SEQUENCE - BURST_SEQUENCE_arch 
-- Project Name: ITOP Rollamte
-- Target Devices: 
-- Tool versions: 
-- Description: Generate Data_window for RX
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Revision 0.02 - DAC Delay add, need pass parameters
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity mrut_burst_sequence is
	generic (
	    REGISTER_W32    : INTEGER := 32;
	    REGISTER_W16    : INTEGER := 16;
	    REGISTER_W8     : INTEGER := 8   
	);
	port (	
		-- Sync
		aclk		: in STD_LOGIC;
		aresetn		: in STD_LOGIC;
		
        -- Control
       	BURST_LEVEL             : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        BURST_COUNTER           : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
            
        -- Magnet  
        MAGNET_MODE             : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        MAGNET_PULSE_WIDTH      : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        MAGNET_INITIAL_DELAY    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            
        MAG_PULSE               : out STD_LOGIC;
		
		-- Inputs
		CH_TRIG					: in STD_LOGIC;
		TX_DATA_DELAY			: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);		--delays in 100MHz samples
		TX_DELTA_DELAY			: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);		--delays in 100MHz samples
        TX_DELTA_ADD            : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);     --delays in 100MHz samples
		RX_SAMPLING_FREQUENCY  	: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  	--100 MHz divider for sample freq
		RX_DATA_DELAY			: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);		--delays in 100MHz samples
		RX_DATA_WINDOW			: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);		--data window in sample freq samples
			
		-- Outputs
		TX_TRIGGER				: out STD_LOGIC;
		RX_DATA_VALID			: out STD_LOGIC;
		RX_DATA_WINDOW_ON		: out STD_LOGIC
	);
end mrut_burst_sequence;

architecture BURST_SEQUENCE_arch of mrut_burst_sequence is

type burst_seq_states is (idle_state, init_delay, tx_delay, delta_delay, delta_add, burst_seq_end,
		init_data_delay_state, data_window_state, data_window_dcm_state);
signal burst_sm_state : burst_seq_states;

type states_receiver is (idle, rx_delay, rx_on, receiver_end); 
signal state_receiver : states_receiver; 
signal start_receiver : STD_LOGIC := '0';

signal rx_sampling_frequency_i : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
signal rx_data_delay_i : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
signal rx_data_window_i : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
signal count_freq : UNSIGNED(REGISTER_W32-1 downto 0);
signal count_data_window : UNSIGNED(REGISTER_W32-1 downto 0);
signal count_data_delay : UNSIGNED(REGISTER_W32-1 downto 0);
signal count_delta_delay : UNSIGNED(REGISTER_W32-1 downto 0);
signal count_delta_add : UNSIGNED(REGISTER_W32-1 downto 0);
signal count_burst : UNSIGNED(REGISTER_W16-1 downto 0);

signal delta_delay_flag : STD_LOGIC;
signal delta_add_flag : STD_LOGIC;
signal tx_delay_flag : STD_LOGIC;
signal init_delay_flag : STD_LOGIC;
signal rx_delay_flag : STD_LOGIC;

signal tx_data_delay_i	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);		--delays in 100MHz samples
signal tx_delta_delay_i	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);		--delays in 100MHz samples
signal tx_delta_add_i   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);        --delays in 100MHz samples

signal magnet_init_delay_i  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); --delays in 100MHz samples
signal magnet_pulse_width_i : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); --in 100MHz samples
signal magnet_pulse_count   : UNSIGNED(REGISTER_W32-1 downto 0); 
signal magnet_pulse_i : STD_LOGIC;
signal magnet_pulse_i2 : STD_LOGIC;

signal start_trigger : STD_LOGIC;
signal start_trigger_ack : STD_LOGIC;

begin
	
	-- Pulse width control
	magnet_process:process (aclk)
	begin
		if (rising_edge(aclk)) then
			if (aresetn = '0' ) then
				magnet_pulse_count <= (others => '0');
				magnet_pulse_i2 <= '0';
			else
				if (magnet_pulse_i = '1') then
					if(magnet_pulse_count >= unsigned(magnet_pulse_width_i)) then
						magnet_pulse_i2 <= '0';
					else
						magnet_pulse_i2 <= '1';
						magnet_pulse_count <= magnet_pulse_count + 1;
					end if;
				else
					magnet_pulse_i2 <= '0';
					magnet_pulse_count <= (others => '0');	
				end if;
			end if;	
		end if;
	end process;	
			
	MAG_PULSE <= magnet_pulse_i and magnet_pulse_i2;
		
	pulser_process:process (aclk)
	begin
		if (rising_edge(aclk)) then
            if (aresetn = '0') then             
                count_data_delay <= (others => '0');            
                TX_TRIGGER <= '0';
				start_trigger <= '0';
				start_trigger_ack <= '0';
				magnet_pulse_i <= '0';
				start_receiver <= '0';
				init_delay_flag <= '0';
				tx_delay_flag <= '0';
				delta_delay_flag <= '0';
				delta_add_flag <= '0';
                burst_sm_state <= idle_state;
            else
                if (CH_TRIG = '0') then
					start_trigger <= '1';
				else
					if (start_trigger_ack = '1') then
						start_trigger <= '0';	
					end if;
				end if;
                case burst_sm_state is  
                    when idle_state =>
                        if (CH_TRIG = '1' and start_trigger = '1') then
							start_trigger_ack <= '1';
																			
							if((unsigned(MAGNET_MODE) /= 0) and (unsigned(MAGNET_INITIAL_DELAY) > 0) and (unsigned(count_burst) = 0 or unsigned(BURST_LEVEL) < 2)) then
								burst_sm_state <= init_delay;
                            elsif((unsigned(TX_DATA_DELAY) > 0) and (unsigned(BURST_COUNTER) = 0 or unsigned(BURST_LEVEL) < 2)) then
                                burst_sm_state <= tx_delay;
                            else
                                if(unsigned(count_burst) > 0) then
                                    if(unsigned(TX_DELTA_DELAY) > 0) then
                                        burst_sm_state <= delta_delay;	
                                    elsif(unsigned(TX_DELTA_DELAY) > 0 and unsigned(count_burst) > 1) then
                                        burst_sm_state <= delta_add;
                                    else
                                        burst_sm_state <= burst_seq_end;	
                                    end if;	
                                else
                                    burst_sm_state <= burst_seq_end;
                                end if;
                            end if;
                        else
                            burst_sm_state <= idle_state;
                        end if;
                    
                        if(unsigned(BURST_COUNTER) = 0) then
							magnet_pulse_i <= '0';
                        end if;
    
                        count_data_delay <= (others => '0');
                        count_delta_delay <= (others => '0');
                        count_delta_add <= (others => '0');
                        tx_data_delay_i <= TX_DATA_DELAY;
                        tx_delta_delay_i <= TX_DELTA_DELAY;
                        tx_delta_add_i <= TX_DELTA_ADD; 
						magnet_init_delay_i <= MAGNET_INITIAL_DELAY;
						magnet_pulse_width_i <= MAGNET_PULSE_WIDTH;
                        TX_TRIGGER <= '0';
                        start_receiver <= '0';
				
                    when init_delay =>
                        if((unsigned(MAGNET_MODE) /= 0) and (unsigned(MAGNET_PULSE_WIDTH) > 0)) then
                            magnet_pulse_i <= '1';
                        end if;
						start_trigger_ack <= '0';
                        if (count_data_delay >= (unsigned(magnet_init_delay_i)-1)) then
                            init_delay_flag <= '0';
                            count_data_delay <= (others => '0');
                            start_receiver <= '1';
							if(unsigned(tx_data_delay_i) > 0) then
								burst_sm_state <= tx_delay;	
							else
                            	burst_sm_state <= burst_seq_end;
							end if;
                        else
                            init_delay_flag <= '1';
                            count_data_delay <= count_data_delay + 1;
                        end if;
                        
                    when tx_delay =>
                        if((unsigned(MAGNET_MODE) /= 0) and (unsigned(MAGNET_PULSE_WIDTH) > 0)) then
                            magnet_pulse_i <= '1';
                        end if;
						start_trigger_ack <= '0';
						start_receiver <= '1';
                        if (count_data_delay >= (unsigned(tx_data_delay_i)-1)) then
                            tx_delay_flag <= '0';
                            count_data_delay <= (others => '0');                          
                            burst_sm_state <= burst_seq_end;
                        else
                            tx_delay_flag <= '1';
                            count_data_delay <= count_data_delay + 1;
                        end if;
                            
                    when delta_delay =>
                        if((unsigned(MAGNET_MODE) /= 0) and (unsigned(MAGNET_PULSE_WIDTH) > 0)) then
                            magnet_pulse_i <= '1';
                        end if;
						start_trigger_ack <= '0';
						--start_receiver <= '1';
                        if (count_delta_delay >= (unsigned(tx_delta_delay_i)-1)) then
                            delta_delay_flag <= '0';
                            if(unsigned(tx_delta_add_i) > 0 and unsigned(count_burst) > 1) then
                                count_delta_delay <= (others => '0');
                                burst_sm_state <= delta_add;	
                            else
                                burst_sm_state <= burst_seq_end;	
                            end if;
                        else
                            delta_delay_flag <= '1';
                            count_delta_delay <= count_delta_delay + 1;
                        end if;
                            
                    when delta_add =>
						start_trigger_ack <= '0';
						--start_receiver <= '1';
                        if (count_delta_delay >= unsigned(tx_delta_add_i)) then
                            count_delta_delay <= (others => '0');
                            if(count_delta_add >= (unsigned(count_burst)-2)) then
                                delta_add_flag <= '0';
                                burst_sm_state <= burst_seq_end;		
                            else
                                count_delta_add <= count_delta_add + 1;	
                            end if;
                        else
                            delta_add_flag <= '1';
                            count_delta_delay <= count_delta_delay + 1;
                        end if;
                        
                    when burst_seq_end =>
                        if((unsigned(MAGNET_MODE) /= 0) and (unsigned(MAGNET_PULSE_WIDTH) > 0)) then
                            magnet_pulse_i <= '1';
                        end if;
                        TX_TRIGGER <= '1';
                        start_trigger_ack <= '0';
                        if(state_receiver = receiver_end) then
                            burst_sm_state <= idle_state; 
                            start_receiver <= '0';
                        else
                            start_receiver <= '1';  
                        end if; 
                                                                               
                    when others =>
                        burst_sm_state <= idle_state;
                        
                end case;
            end if;
		end if;
	end process;
	
    receiver_process: process(aclk)
        variable dat_counter : integer range 0 to 65536 :=0;
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then    
                RX_DATA_VALID <= '0';
                RX_DATA_WINDOW_ON <= '0';
                count_data_window <= (others => '0');
                count_burst <= (others => '0');
                count_freq  <= (others => '0');
                rx_sampling_frequency_i <= (others => '0');
                rx_data_delay_i <= (others => '0');
                rx_data_window_i <= (others => '0');
                rx_delay_flag <= '0';
                state_receiver <= idle;
            else
                case state_receiver is        
                    when idle =>  
                        RX_DATA_VALID <= '0';
                        RX_DATA_WINDOW_ON <= '0';
                        count_data_window <= (others => '0');
                        count_freq  <= unsigned(RX_SAMPLING_FREQUENCY);
                        if(unsigned(BURST_COUNTER) = 0) then
                            count_burst <= (others => '0');
                        end if;
                        rx_sampling_frequency_i <= RX_SAMPLING_FREQUENCY;
                        rx_data_delay_i <= RX_DATA_DELAY;
                        rx_data_window_i <= RX_DATA_WINDOW;
                        if(start_receiver = '1') then
                            if(unsigned(RX_DATA_DELAY) > 0) then
                                state_receiver <= rx_delay;
                            else
                                if(unsigned(RX_DATA_WINDOW) > 0) then
                                    state_receiver <= rx_on; 
                                else
                                    state_receiver <= receiver_end;
                                end if;
                            end if;
                        end if;
                    when rx_delay =>
                        if (count_data_window >= unsigned(rx_data_delay_i)) then
                            count_data_window <= (others => '0');
                            rx_delay_flag <= '0';
                            if(unsigned(rx_data_window_i) > 0) then
                                state_receiver <= rx_on;
                            else
                                state_receiver <= receiver_end;
                            end if;
                        else
                            rx_delay_flag <= '1';
                            count_data_window <= count_data_window+1; 
                        end if;
                    when rx_on =>   
                        if(count_freq >= unsigned(rx_sampling_frequency_i)) then
                            count_freq  <= to_unsigned(1, count_freq'length);
                            if (count_data_window >= unsigned(rx_data_window_i)) then
                                RX_DATA_VALID <= '0';
                                RX_DATA_WINDOW_ON <= '0';   
                                count_burst <= count_burst + 1;
                                state_receiver <= receiver_end;     
                            else
                                RX_DATA_VALID <= '1';
                                RX_DATA_WINDOW_ON <= '1';
                                count_data_window <= count_data_window + 1;
                            end if; 
                        else
                            if (count_data_window >= unsigned(rx_data_window_i)) then
                                RX_DATA_VALID <= '0';
                                RX_DATA_WINDOW_ON <= '0';   
                                count_burst <= count_burst + 1;
                                state_receiver <= receiver_end;  
                            else
                                RX_DATA_VALID <= '0';
                                RX_DATA_WINDOW_ON <= '1';
                            end if;
                            count_freq <= count_freq + 1;
                        end if;    
                        
                    when receiver_end =>  
                        if(burst_sm_state = burst_seq_end) then
                            state_receiver <= idle;
                        end if;
                    
                    when others =>   
                        null;
                end case;
            end if;
        end if;
    end process;
                              
-----------------------------------------------------------------------------
end BURST_SEQUENCE_arch;

