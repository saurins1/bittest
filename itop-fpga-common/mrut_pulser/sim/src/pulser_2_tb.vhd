library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity pulser_2_tb is
	generic (
  	    REGISTER_W32    : INTEGER := 32;
  	    REGISTER_W16    : INTEGER := 16;
  		REGISTER_W30    : INTEGER := 30;
  	    REGISTER_W8     : INTEGER := 8;
  	    REGISTER_W2     : INTEGER := 2    
  	);
end;

architecture bench of pulser_2_tb is

  component mrut_pulser
  	generic (
  	    REGISTER_W32    : INTEGER := 32;
  	    REGISTER_W16    : INTEGER := 16;
  		REGISTER_W30    : INTEGER := 30;
  	    REGISTER_W8     : INTEGER := 8;
  	    REGISTER_W2     : INTEGER := 2    
  	);
  	port (
  	    aclk 	: in STD_LOGIC;
        aresetn	: in STD_LOGIC;
		
		-- Control
       	BURST_LEVEL             : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        BURST_COUNTER           : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        
        -- Magnet  
        MAGNET_MODE	            : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        MAGNET_PULSE_WIDTH	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        MAGNET_INITIAL_DELAY    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        
        MAG_PULSE               : out STD_LOGIC;
		
  		CH_TRIG					: in STD_LOGIC;
  		TX_DATA_DELAY			: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		TX_DELTA_DELAY			: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);		--delays in 100MHz samples
        TX_DELTA_ADD            : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);        --delays in 100MHz samples
  		RX_SAMPLING_FREQUENCY  	: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		RX_DATA_DELAY			: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		RX_DATA_WINDOW			: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		RX_DATA_VALID			: out STD_LOGIC;
  		RX_DATA_WINDOW_ON		: out STD_LOGIC;
  		HALF_CYCLE_FREQ	: in STD_LOGIC_VECTOR(REGISTER_W30-1 downto 0);
  		NO_HALF_CYCLES	: in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
  		START_CYCLE		: in STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
  		TXp			: out STD_LOGIC;
  		TXn		: out STD_LOGIC              
  	);
  end component;

  signal aclk: std_logic:= '0';
  signal aresetn: std_logic:= '0';

  signal BURST_LEVEL: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0');
  signal BURST_COUNTER: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0');
  signal MAGNET_MODE	        : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0'); 
  signal MAGNET_PULSE_WIDTH	    : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0'); 
  signal MAGNET_INITIAL_DELAY   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0'); 
  signal MAG_PULSE              : STD_LOGIC;
  signal CH_TRIG: STD_LOGIC:= '0';
  signal TX_DATA_DELAY_1: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0'); 
  signal TX_DATA_DELAY_2: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');
  signal TX_DELTA_DELAY: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0'); 
  signal TX_DELTA_ADD: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0'); 
  signal RX_SAMPLING_FREQUENCY: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0'); 
  signal RX_DATA_DELAY: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0'); 
  signal RX_DATA_WINDOW: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0'); 
  signal RX_DATA_VALID_1: STD_LOGIC;
  signal RX_DATA_WINDOW_ON_1: STD_LOGIC;
  signal RX_DATA_VALID_2: STD_LOGIC;
  signal RX_DATA_WINDOW_ON_2: STD_LOGIC;

  signal HALF_CYCLE_FREQ: STD_LOGIC_VECTOR(REGISTER_W30-1 downto 0):= (others => '0'); 
  signal NO_HALF_CYCLES: STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0):= (others => '0'); 
  signal START_CYCLE_1: STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0):= (others => '0'); 
  signal START_CYCLE_2: STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0):= (others => '0');

  signal TXp_1: STD_LOGIC;
  signal TXn_1: STD_LOGIC;
  signal TXp_2: STD_LOGIC;
  signal TXn_2: STD_LOGIC;

  constant aclk_period: time := 10 ns;

  type states_pulser_sm is (idle, trigger_pulser, trigger_pulser_end); 
  signal state_pulser_sm : states_pulser_sm; 

  signal pulser_flag: STD_LOGIC:= '0';

  signal RX_DATA_WINDOW_ON_d1: STD_LOGIC:= '0';
  signal RX_DATA_WINDOW_ON_counter: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0'); 

  signal TXp_counter: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0'); 
  signal TXn_counter: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0'); 

  signal N_COINCIDENCE: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0'); 
  signal CH_TRIG_d1: STD_LOGIC:= '0';
  signal coincidence_counter: UNSIGNED(REGISTER_W32-1 downto 0):= (others => '0'); 

begin
	
	pulser_sm_inst: process(aclk, aresetn)  
    variable my_counter  : integer range 0 to 16383 :=0;   
begin 
    if (aresetn = '0') then
        CH_TRIG <= '0';
        RX_DATA_WINDOW_ON_d1 <= '0';
        RX_DATA_WINDOW_ON_counter <= (others => '0');
        TXp_counter <= (others => '0');
        TXp_counter <= (others => '0');
        coincidence_counter <= (others => '0');
        BURST_COUNTER <= (others => '0');
        CH_TRIG_d1 <= '1';
        -- state                                                                              
        state_pulser_sm <= idle;  
    elsif (aclk'event and aclk = '1') then
        RX_DATA_WINDOW_ON_d1 <= RX_DATA_WINDOW_ON_1;
        CH_TRIG_d1 <= CH_TRIG;
        if(RX_DATA_WINDOW_ON_1 = '1') then
            RX_DATA_WINDOW_ON_counter <= std_logic_vector(unsigned(RX_DATA_WINDOW_ON_counter) + 1);
        else
            RX_DATA_WINDOW_ON_counter <= (others => '0'); 
        end if;
        if(TXp_1 = '1') then
            TXp_counter <= std_logic_vector(unsigned(TXp_counter) + 1);
        else
            TXp_counter <= (others => '0'); 
        end if;
        if(TXn_1 = '1') then
            TXn_counter <= std_logic_vector(unsigned(TXn_counter) + 1);
        else
            TXn_counter <= (others => '0'); 
        end if;
        case state_pulser_sm is 
            when idle =>     
                CH_TRIG <= '0';
                if(pulser_flag = '1') then
                    my_counter := 0;
                    state_pulser_sm <= trigger_pulser; 
                end if;
            when trigger_pulser =>
                  CH_TRIG <= '1';                      
                if(RX_DATA_WINDOW_ON_d1 = '1' and RX_DATA_WINDOW_ON_1 = '0') then
                    my_counter := 0;
                    state_pulser_sm <= trigger_pulser_end; 
                    if(unsigned(BURST_COUNTER) /= 0) then
                        if((coincidence_counter >= unsigned(N_COINCIDENCE)-1) or unsigned(N_COINCIDENCE) < 2) then
                            BURST_COUNTER <= (others => '0');
                            coincidence_counter <= (others => '0');
                        else
                            coincidence_counter <= coincidence_counter + 1;    
                        end if;
                    end if;
                else
                    if(RX_DATA_WINDOW_ON_d1 = '0' and RX_DATA_WINDOW_ON_1 = '1') then
                        BURST_COUNTER <= std_logic_vector(unsigned(BURST_COUNTER)+1);
                    end if;
                end if;
            when trigger_pulser_end =>
                CH_TRIG <= '0';                
                if(my_counter >= 8) then
                    state_pulser_sm <= idle;         
                else
                    my_counter := my_counter + 1;            
                end if;    
            when others =>
                null;                   
        end case;                                                                                            
    end if;
end process; 
									
	stimulus: process
begin
    aresetn <= '0';
    pulser_flag <= '0';
    wait for aclk_period*10;
                
    MAGNET_MODE <= std_logic_vector(to_unsigned(0, MAGNET_MODE'length));
    --MAGNET_MODE <= std_logic_vector(to_unsigned(1, MAGNET_MODE'length));
                                                
    --MAGNET_INITIAL_DELAY <= std_logic_vector(to_unsigned(0, MAGNET_INITIAL_DELAY'length));
    --MAGNET_INITIAL_DELAY <= std_logic_vector(to_unsigned(500, MAGNET_INITIAL_DELAY'length));
    MAGNET_INITIAL_DELAY <= std_logic_vector(to_unsigned(60000, MAGNET_INITIAL_DELAY'length));
                                                         
    --MAGNET_PULSE_WIDTH <= std_logic_vector(to_unsigned(0, MAGNET_PULSE_WIDTH'length));
    --MAGNET_PULSE_WIDTH <= std_logic_vector(to_unsigned(5000, MAGNET_PULSE_WIDTH'length));
    --MAGNET_PULSE_WIDTH <= std_logic_vector(to_unsigned(300000, MAGNET_PULSE_WIDTH'length));            
    --MAGNET_PULSE_WIDTH <= std_logic_vector(to_unsigned(400000, MAGNET_PULSE_WIDTH'length));        
    MAGNET_PULSE_WIDTH <= std_logic_vector(to_unsigned(90000, MAGNET_PULSE_WIDTH'length));                                                   
        
    --TX_DATA_DELAY  <= std_logic_vector(to_unsigned(0, TX_DATA_DELAY'length));
    --TX_DATA_DELAY  <= std_logic_vector(to_unsigned(4000, TX_DATA_DELAY'length));
    TX_DATA_DELAY_1  <= std_logic_vector(to_unsigned(0, TX_DATA_DELAY_1'length));
    TX_DATA_DELAY_2  <= std_logic_vector(to_unsigned(25, TX_DATA_DELAY_1'length));
    
    TX_DELTA_DELAY  <= std_logic_vector(to_unsigned(0, TX_DELTA_DELAY'length));
    --TX_DELTA_DELAY  <= std_logic_vector(to_unsigned(500, TX_DELTA_DELAY'length));
    --TX_DELTA_DELAY  <= std_logic_vector(to_unsigned(1000, TX_DELTA_DELAY'length));
                                                    
    TX_DELTA_ADD  <= std_logic_vector(to_unsigned(0, TX_DELTA_ADD'length));    
    --TX_DELTA_ADD  <= std_logic_vector(to_unsigned(1000, TX_DELTA_ADD'length));                                                        
                                                   
    --RX_SAMPLING_FREQUENCY  <= std_logic_vector(to_unsigned(1, RX_SAMPLING_FREQUENCY'length));
    RX_SAMPLING_FREQUENCY  <= std_logic_vector(to_unsigned(2, RX_SAMPLING_FREQUENCY'length));
    --RX_SAMPLING_FREQUENCY  <= std_logic_vector(to_unsigned(4, RX_SAMPLING_FREQUENCY'length));
    
    RX_DATA_DELAY  <= std_logic_vector(to_unsigned(0, RX_DATA_DELAY'length));
    --RX_DATA_DELAY  <= std_logic_vector(to_unsigned(1500, RX_DATA_DELAY'length));
    
    --RX_DATA_WINDOW  <= std_logic_vector(to_unsigned(8192, RX_DATA_WINDOW'length));
    RX_DATA_WINDOW  <= std_logic_vector(to_unsigned(4000, RX_DATA_WINDOW'length));
        
    --N_COINCIDENCE <= std_logic_vector(to_unsigned(9, N_COINCIDENCE'length));        
    N_COINCIDENCE <= std_logic_vector(to_unsigned(0, N_COINCIDENCE'length));        
    --N_COINCIDENCE <= std_logic_vector(to_unsigned(3, N_COINCIDENCE'length));                                            
                                                                                                    
    --HALF_CYCLE_FREQ  <= std_logic_vector(to_unsigned(500000, HALF_CYCLE_FREQ'length)); -- 1000 khz to half cycle picosecond    
    --HALF_CYCLE_FREQ  <= std_logic_vector(to_unsigned(38500, HALF_CYCLE_FREQ'length)); -- 8000 khz to half cycle picosecond    
    --HALF_CYCLE_FREQ  <= std_logic_vector(to_unsigned(45000, HALF_CYCLE_FREQ'length)); -- 8000 khz to half cycle picosecond    
    --HALF_CYCLE_FREQ  <= std_logic_vector(to_unsigned(62500, HALF_CYCLE_FREQ'length)); -- 8000 khz to half cycle picosecond    
    --HALF_CYCLE_FREQ  <= std_logic_vector(to_unsigned(100000, HALF_CYCLE_FREQ'length));
    --HALF_CYCLE_FREQ  <= std_logic_vector(to_unsigned(50000, HALF_CYCLE_FREQ'length));
    --HALF_CYCLE_FREQ  <= std_logic_vector(to_unsigned(25000, HALF_CYCLE_FREQ'length));
    --HALF_CYCLE_FREQ  <= std_logic_vector(to_unsigned(0, HALF_CYCLE_FREQ'length));
    --HALF_CYCLE_FREQ  <= std_logic_vector(to_unsigned(125000, HALF_CYCLE_FREQ'length));
    --HALF_CYCLE_FREQ  <= std_logic_vector(to_unsigned(166666, HALF_CYCLE_FREQ'length));
    --HALF_CYCLE_FREQ  <= std_logic_vector(to_unsigned(192308, HALF_CYCLE_FREQ'length));
    --HALF_CYCLE_FREQ  <= std_logic_vector(to_unsigned(250000, HALF_CYCLE_FREQ'length));
    --NO_HALF_CYCLES  <= std_logic_vector(to_unsigned(2, NO_HALF_CYCLES'length));
    --NO_HALF_CYCLES  <= std_logic_vector(to_unsigned(4, NO_HALF_CYCLES'length));
		HALF_CYCLE_FREQ  <= std_logic_vector(to_unsigned(500000, HALF_CYCLE_FREQ'length));
    --HALF_CYCLE_FREQ  <= std_logic_vector(to_unsigned(250000, HALF_CYCLE_FREQ'length));
    --HALF_CYCLE_FREQ  <= std_logic_vector(to_unsigned(71428, HALF_CYCLE_FREQ'length));
    --HALF_CYCLE_FREQ  <= std_logic_vector(to_unsigned(72500, HALF_CYCLE_FREQ'length));
    NO_HALF_CYCLES  <= std_logic_vector(to_unsigned(6, NO_HALF_CYCLES'length));
    START_CYCLE_1  <= std_logic_vector(to_unsigned(0, START_CYCLE_2'length));        
    START_CYCLE_2  <= std_logic_vector(to_unsigned(1, START_CYCLE_2'length));    
    --START_CYCLE_2  <= std_logic_vector(to_unsigned(0, START_CYCLE_2'length));                                                    
    aresetn <= '1';
                                                 
    wait for aclk_period*10;
    pulser_flag <= '1';
                                                 
    wait;
end process;
	
	BURST_LEVEL <= N_COINCIDENCE;
	
	-- aclk process 
    aclk_process :process
    begin
        aclk <= not aclk;
        wait for aclk_period/2;
        aclk <= not aclk;
        wait for aclk_period/2;
    end process;

  -- Insert values for generic parameters !!
  uut1: mrut_pulser generic map ( REGISTER_W32          => REGISTER_W32,
                              REGISTER_W16          => REGISTER_W16,
                              REGISTER_W30          => REGISTER_W30,
                              REGISTER_W8           => REGISTER_W8,
                              REGISTER_W2           => REGISTER_W2)
                   port map ( aclk                  => aclk,
                              aresetn               => aresetn,
                              BURST_LEVEL           => BURST_LEVEL,
                              BURST_COUNTER         => BURST_COUNTER,
                              MAGNET_MODE             => MAGNET_MODE,
                              MAGNET_PULSE_WIDTH    => MAGNET_PULSE_WIDTH,
                              MAGNET_INITIAL_DELAY  => MAGNET_INITIAL_DELAY,
                              MAG_PULSE             => MAG_PULSE,
                              CH_TRIG               => CH_TRIG,
                              TX_DATA_DELAY         => TX_DATA_DELAY_1,
                                 TX_DELTA_DELAY           => TX_DELTA_DELAY,
                              TX_DELTA_ADD          => TX_DELTA_ADD,
                              RX_SAMPLING_FREQUENCY => RX_SAMPLING_FREQUENCY,
                              RX_DATA_DELAY         => RX_DATA_DELAY,
                              RX_DATA_WINDOW        => RX_DATA_WINDOW,
                              RX_DATA_VALID         => RX_DATA_VALID_1,
                              RX_DATA_WINDOW_ON     => RX_DATA_WINDOW_ON_1,
                              HALF_CYCLE_FREQ       => HALF_CYCLE_FREQ,
                              NO_HALF_CYCLES        => NO_HALF_CYCLES,
                              START_CYCLE           => START_CYCLE_1,
                              TXp               => TXp_1,
                              TXn              => TXn_1 );
                              
  uut2: mrut_pulser generic map ( REGISTER_W32          => REGISTER_W32,
                                                                                  REGISTER_W16          => REGISTER_W16,
                                                                                  REGISTER_W30          => REGISTER_W30,
                                                                                  REGISTER_W8           => REGISTER_W8,
                                                                                  REGISTER_W2           => REGISTER_W2)
                                                                       port map ( aclk                  => aclk,
                                                                                  aresetn               => aresetn,
                                                                                  BURST_LEVEL           => BURST_LEVEL,
                                                                                  BURST_COUNTER         => BURST_COUNTER,
                                                                                  MAGNET_MODE           => MAGNET_MODE,
                                                                                  MAGNET_PULSE_WIDTH    => MAGNET_PULSE_WIDTH,
                                                                                  MAGNET_INITIAL_DELAY  => MAGNET_INITIAL_DELAY,
                                                                                  MAG_PULSE             => MAG_PULSE,
                                                                                  CH_TRIG               => CH_TRIG,
                                                                                  TX_DATA_DELAY         => TX_DATA_DELAY_2,
                                                                                  TX_DELTA_DELAY        => TX_DELTA_DELAY,
                                                                                  TX_DELTA_ADD          => TX_DELTA_ADD,
                                                                                  RX_SAMPLING_FREQUENCY => RX_SAMPLING_FREQUENCY,
                                                                                  RX_DATA_DELAY         => RX_DATA_DELAY,
                                                                                  RX_DATA_WINDOW        => RX_DATA_WINDOW,
                                                                                  RX_DATA_VALID         => RX_DATA_VALID_2,
                                                                                  RX_DATA_WINDOW_ON     => RX_DATA_WINDOW_ON_2,
                                                                                  HALF_CYCLE_FREQ       => HALF_CYCLE_FREQ,
                                                                                  NO_HALF_CYCLES        => NO_HALF_CYCLES,
                                                                                  START_CYCLE           => START_CYCLE_2,
                                                                                  TXp               => TXp_2,
                                                                                  TXn              => TXn_2 );

end;