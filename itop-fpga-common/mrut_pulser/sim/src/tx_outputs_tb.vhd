library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity tx_outputs_tb is
	generic (
  		REGISTER_W30    : INTEGER := 30;
  		REGISTER_W8     : INTEGER := 8;
  		REGISTER_W2		: INTEGER := 2 
  	);
end;

architecture bench of tx_outputs_tb is

  component tx_outputs
  	generic (
  		REGISTER_W30    : INTEGER := 30;
  		REGISTER_W8     : INTEGER := 8;
  		REGISTER_W2		: INTEGER := 2 
  	);
  	port (
  		aclk		: in std_logic;
  		aresetn		: in std_logic;
  		TX_ENABLE		: in std_logic;
  		START_CALC		: in std_logic;
  		START_TX		: in std_logic;
  		HALF_CYCLE_FREQ	: in std_logic_vector(REGISTER_W30-1 downto 0);
  		NO_HALF_CYCLES	: in std_logic_vector(REGISTER_W8-1 downto 0);
  		START_CYCLE		: in std_logic_vector(REGISTER_W2-1 downto 0);
  		TX_PLUS			: out std_logic;
  		TX_MINUS		: out std_logic
  	);
  end component;

  signal aclk: std_logic;
  signal aresetn: std_logic;
  signal TX_ENABLE: std_logic;
  signal START_CALC: std_logic;
  signal START_TX: std_logic;
  signal HALF_CYCLE_FREQ: std_logic_vector(REGISTER_W30-1 downto 0);
  signal NO_HALF_CYCLES: std_logic_vector(REGISTER_W8-1 downto 0);
  signal START_CYCLE: std_logic_vector(REGISTER_W2-1 downto 0);
  signal TX_PLUS: std_logic;
  signal TX_MINUS: std_logic ;

  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;

begin

  -- Insert values for generic parameters !!
  uut: tx_outputs generic map ( REGISTER_W30    => REGISTER_W30,
                                REGISTER_W8     => REGISTER_W8,
                                REGISTER_W2     => REGISTER_W2 )
                     port map ( aclk            => aclk,
                                aresetn         => aresetn,
                                TX_ENABLE       => TX_ENABLE,
                                START_CALC      => START_CALC,
                                START_TX        => START_TX,
                                HALF_CYCLE_FREQ => HALF_CYCLE_FREQ,
                                NO_HALF_CYCLES  => NO_HALF_CYCLES,
                                START_CYCLE     => START_CYCLE,
                                TX_PLUS         => TX_PLUS,
                                TX_MINUS        => TX_MINUS );

  stimulus: process
  begin
  
    -- Put initialisation code here


    -- Put test bench stimulus code here

    stop_the_clock <= true;
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      aclk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;