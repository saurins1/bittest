library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity burst_sequence_tb is
	generic (
  	    REGISTER_W32    : INTEGER := 32;
  	    REGISTER_W8     : INTEGER := 8   
  	);
end;

architecture bench of burst_sequence_tb is

  component burst_sequence
  	generic (
  	    REGISTER_W32    : INTEGER := 32;
  	    REGISTER_W8     : INTEGER := 8   
  	);
  	port (
  		aclk		: in std_logic;
  		aresetn		: in std_logic;
  		CH_TRIG					: in std_logic;
  		TX_DATA_DELAY			: in std_logic_vector(REGISTER_W32-1 downto 0);
  		RX_SAMPLING_FREQUENCY  	: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  		RX_DATA_DELAY			: in std_logic_vector(REGISTER_W32-1 downto 0);
  		RX_DATA_WINDOW			: in std_logic_vector(REGISTER_W32-1 downto 0);
  		CH_ACTIVE				: out std_logic;
  		TX_TRIGGER				: out std_logic;
  		RX_DATA_VALID			: out std_logic;
  		RX_DATA_WINDOW_ON		: out std_logic
  	);
  end component;

  signal aclk: std_logic;
  signal aresetn: std_logic;
  signal CH_TRIG: std_logic;
  signal TX_DATA_DELAY: std_logic_vector(REGISTER_W32-1 downto 0);
  signal RX_SAMPLING_FREQUENCY: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal RX_DATA_DELAY: std_logic_vector(REGISTER_W32-1 downto 0);
  signal RX_DATA_WINDOW: std_logic_vector(REGISTER_W32-1 downto 0);
  signal CH_ACTIVE: std_logic;
  signal TX_TRIGGER: std_logic;
  signal RX_DATA_VALID: std_logic;
  signal RX_DATA_WINDOW_ON: std_logic ;

  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;

begin

  -- Insert values for generic parameters !!
  uut: burst_sequence generic map ( REGISTER_W32          => REGISTER_W32,
                                    REGISTER_W8           => REGISTER_W8 )
                         port map ( aclk                  => aclk,
                                    aresetn               => aresetn,
                                    CH_TRIG               => CH_TRIG,
                                    TX_DATA_DELAY         => TX_DATA_DELAY,
                                    RX_SAMPLING_FREQUENCY => RX_SAMPLING_FREQUENCY,
                                    RX_DATA_DELAY         => RX_DATA_DELAY,
                                    RX_DATA_WINDOW        => RX_DATA_WINDOW,
                                    CH_ACTIVE             => CH_ACTIVE,
                                    TX_TRIGGER            => TX_TRIGGER,
                                    RX_DATA_VALID         => RX_DATA_VALID,
                                    RX_DATA_WINDOW_ON     => RX_DATA_WINDOW_ON );

  stimulus: process
  begin
  
    -- Put initialisation code here


    -- Put test bench stimulus code here

    stop_the_clock <= true;
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      aclk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;