LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
use ieee.std_logic_signed.all;


entity fir_compiler_512_tb is
end;

architecture bench of fir_compiler_512_tb is

  COMPONENT fir_compiler_512_FIR
    PORT (
      aresetn : IN STD_LOGIC;
      aclk : IN STD_LOGIC;
      aclken : IN STD_LOGIC;
      s_axis_data_tvalid : IN STD_LOGIC;
      s_axis_data_tready : OUT STD_LOGIC;
      s_axis_data_tlast : IN STD_LOGIC;
      s_axis_data_tdata : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      s_axis_config_tvalid : IN STD_LOGIC;
      s_axis_config_tready : OUT STD_LOGIC;
      s_axis_config_tdata : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      s_axis_reload_tvalid : IN STD_LOGIC;
      s_axis_reload_tready : OUT STD_LOGIC;
      s_axis_reload_tlast : IN STD_LOGIC;
      s_axis_reload_tdata : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
      m_axis_data_tvalid : OUT STD_LOGIC;
      m_axis_data_tready : IN STD_LOGIC;
      m_axis_data_tlast : OUT STD_LOGIC;
      m_axis_data_tdata : OUT STD_LOGIC_VECTOR(47 DOWNTO 0);
      event_s_reload_tlast_missing : OUT STD_LOGIC;
      event_s_reload_tlast_unexpected : OUT STD_LOGIC
    );
  END COMPONENT;
  
    signal M_AXIS_DATA_tdata: STD_LOGIC_VECTOR ( 47 downto 0 );
    signal M_AXIS_DATA_tlast: STD_LOGIC;
    signal M_AXIS_DATA_tready: STD_LOGIC := '0';
    signal M_AXIS_DATA_tvalid: STD_LOGIC;
    signal S_AXIS_CONFIG_tdata: STD_LOGIC_VECTOR ( 7 downto 0 ) := (others => '0');
    signal S_AXIS_CONFIG_tready: STD_LOGIC;
    signal S_AXIS_CONFIG_tvalid: STD_LOGIC := '0';
    signal S_AXIS_DATA_tdata: STD_LOGIC_VECTOR ( 15 downto 0 ) := (others => '0');
    signal S_AXIS_DATA_tlast: STD_LOGIC := '0';
    signal S_AXIS_DATA_tready: STD_LOGIC;
    signal S_AXIS_DATA_tvalid: STD_LOGIC := '0';
    signal S_AXIS_RELOAD_tdata: STD_LOGIC_VECTOR ( 23 downto 0 ) := (others => '0');
    signal S_AXIS_RELOAD_tlast: STD_LOGIC := '0';
    signal S_AXIS_RELOAD_tready: STD_LOGIC;
    signal S_AXIS_RELOAD_tvalid: STD_LOGIC := '0';
    signal mclk : std_logic := '1';
    signal aclk: STD_LOGIC := '0';
    signal aclken: STD_LOGIC := '0';
    signal aresetn: STD_LOGIC := '0';
    signal event_s_reload_tlast_missing: STD_LOGIC;
    signal event_s_reload_tlast_unexpected: STD_LOGIC ;
    
    -- signal filtered  
    signal M_AXIS_DATA_tdata_filtered1: STD_LOGIC_VECTOR ( 15 downto 0 ) := (others => '0');
    signal M_AXIS_DATA_tdata_counter: UNSIGNED(31 downto 0 ) := (others => '0');
    
    type states_filtered_output is (idle, s0, s1, s2, s3, s4); 
    signal state_filtered_output : states_filtered_output; 

    -- Clock period definitions
    constant mclk_period : time := 10 ns;
  
    -- Clock period definitions
    constant aclk_period : time := 5 ns;
  
    	-- STATE MACHINE STATES
    type states_reload is (iddle, reload_coeff, reload_coeff1, reload_wait1, reload_wait2); 
    signal state_reload : states_reload; 
    
    signal start_reload : std_logic := '0';
    signal reload_counter : unsigned(15 downto 0) := (others => '0'); 
    signal reload_counter_wait : unsigned(15 downto 0) := (others => '0');
    constant reload_counter_wait_max : unsigned(15 downto 0) := "0000000001000000"; --64
    
    type states_config is (iddle, config, config1, config_wait1, config_wait2); 
    signal state_config : states_config; 
    
    signal start_config : std_logic := '0';
    signal config_counter_wait : unsigned(9 downto 0) := (others => '0');
    constant config_counter_wait_max : unsigned(9 downto 0) := "0001000000"; --64
    
    type states_filter is (iddle, filter, filter1, filter2, filter3, filter4,filterx,filter_wait); 
    signal state_filter : states_filter; 
    
    signal start_filter : std_logic := '0';
    signal filter_counter : unsigned(15 downto 0) := (others => '0'); 
    signal filter_counter_wait : unsigned(9 downto 0) := (others => '0');
    constant filter_counter_wait_max : unsigned(9 downto 0) := "1000000000"; --256
    
    signal filter_tvalid_counter: unsigned(31 downto 0) := (others => '0');
    
    type result_array is array ( 0 to 9999 ) of std_logic_vector(47 downto 0);
    signal result_filter : result_array;
    
    type states_result is (idle, store_result, show_result); 
    signal state_result : states_result; 
      
    signal result_filter_counter : unsigned(15 downto 0) := (others => '0');
    signal result_filter_data: STD_LOGIC_VECTOR ( 15 downto 0 ) := (others => '0');
    signal result_filter_counter2 : unsigned(15 downto 0) := (others => '0');

begin

  uut: fir_compiler_512_FIR port map ( m_axis_data_tdata               => M_AXIS_DATA_tdata,
                                       m_axis_data_tlast               => M_AXIS_DATA_tlast,
                                       m_axis_data_tready              => M_AXIS_DATA_tready,
                                       m_axis_data_tvalid              => M_AXIS_DATA_tvalid,
                                       s_axis_config_tdata             => S_AXIS_CONFIG_tdata,
                                       s_axis_config_tready            => S_AXIS_CONFIG_tready,
                                       s_axis_config_tvalid            => S_AXIS_CONFIG_tvalid,
                                       s_axis_data_tdata               => S_AXIS_DATA_tdata,
                                       s_axis_data_tlast               => S_AXIS_DATA_tlast,
                                       s_axis_data_tready              => S_AXIS_DATA_tready,
                                       s_axis_data_tvalid              => S_AXIS_DATA_tvalid,
                                       s_axis_reload_tdata             => S_AXIS_RELOAD_tdata,
                                       s_axis_reload_tlast             => S_AXIS_RELOAD_tlast,
                                       s_axis_reload_tready            => S_AXIS_RELOAD_tready,
                                       s_axis_reload_tvalid            => S_AXIS_RELOAD_tvalid,
                                       aclk                            => aclk,
                                       aclken                          => aclken,
                                       aresetn                         => aresetn,
                                       event_s_reload_tlast_missing    => event_s_reload_tlast_missing,
                                       event_s_reload_tlast_unexpected => event_s_reload_tlast_unexpected );
                                       
     -- mclk process 
    mclk_process :process
    begin
         mclk <= not mclk;
         wait for mclk_period/2;
         mclk <= not mclk;
         wait for mclk_period/2;
    end process;
    
       -- aclk process 
    aclk_process :process
    begin
         aclk <= not aclk;
         wait for aclk_period/2;
         aclk <= not aclk;
         wait for aclk_period/2;
    end process;
  
  
    stimulus: process
    begin
       -- hold reset state for 100 ns.
        wait for 100 ns;    
        aresetn <= '0';  
        wait for aclk_period*10;
        aresetn <= '1';  
        M_AXIS_DATA_tready <= '1';
        aclken <= '1';   
        wait for aclk_period*10;
        start_reload <= '1';
        wait for aclk_period*2;
        start_reload <= '0';

        wait;
    end process;
        
	--PERFORM RELOAD
    reload_inst: process(aclk, aresetn)
    
        type COEFile is file of integer;
        file coe_file: COEFile is in "coefficients_512.dat";
        variable coe_in: integer; 
    
    begin 
        if (aresetn = '0') then
            -- dividend signals
            S_AXIS_RELOAD_tvalid <= '0';
            S_AXIS_RELOAD_tlast <= '0';
            S_AXIS_RELOAD_tdata <= (others => '0');
            state_reload <= iddle;  
            reload_counter <= (others => '0');
            reload_counter_wait <= (others => '0');
            start_config <= '0';
        elsif (aclk'event and aclk = '1') then
            case state_reload is        
                when iddle =>
                    start_config <= '0';
                    if(start_reload = '1') then
                        state_reload <= reload_coeff;  
                        reload_counter <= (others => '0'); 
                    end if;
                when reload_coeff =>
                    if(S_AXIS_RELOAD_tready = '1') then                 
                        S_AXIS_RELOAD_tvalid <= '1';
                        S_AXIS_RELOAD_tlast <= '0';
                        -- read coeff
                        if not endfile(coe_file) then
                            read (coe_file, coe_in);
                            S_AXIS_RELOAD_tdata<=std_logic_vector(to_signed(coe_in, S_AXIS_RELOAD_tdata'length));
                        end if;
                        if not endfile(coe_file) then
                            read (coe_file, coe_in);
                        end if;
                        --
                        if(reload_counter >= 511) then
                            S_AXIS_RELOAD_tlast <= '1';  
                            reload_counter_wait <= (others => '0');
                            state_reload <= reload_wait1; 
                        end if;
                        reload_counter <= reload_counter + 1;
                    else
                        S_AXIS_RELOAD_tvalid <= '0';  
                    end if;                     
                when reload_coeff1 =>  
                    S_AXIS_RELOAD_tvalid <= '0';
                    state_reload <= reload_coeff;  
                when reload_wait1 =>
                    S_AXIS_RELOAD_tvalid <= '0';
                    S_AXIS_RELOAD_tlast <= '0';                 
                    if(reload_counter_wait >= reload_counter_wait_max) then
                        start_config <= '1';
                        state_reload <= reload_wait2; 
                    end if;
                    reload_counter_wait <= reload_counter_wait + 1;
                when reload_wait2 =>                  
                    state_reload <= iddle;  
                when others =>
                    null;                   
            end case;                                                                                            
        end if;
    end process;
    
 	--PERFORM CONFIG
    config_inst: process(aclk, aresetn)
    
    begin 
        if (aresetn = '0') then
            -- dividend signals
            S_AXIS_CONFIG_tvalid <= '0';
            S_AXIS_CONFIG_tdata <= (others => '0');
            state_config <= iddle;  
            start_filter <= '0';
            config_counter_wait <= (others => '0');
        elsif (aclk'event and aclk = '1') then
            case state_config is        
                when iddle =>
                    start_filter <= '0';
                    if(start_config = '1') then
                        state_config <= config;  
                    end if;
                when config =>
                    if(S_AXIS_CONFIG_tready = '1') then
                        S_AXIS_CONFIG_tvalid <= '1';
                        S_AXIS_CONFIG_tdata <= (others => '0');
                        config_counter_wait <= (others => '0');
                        state_config <= config1; 
                    else
                        s_axis_config_tvalid <= '0';  
                    end if;  
                when config1 =>
                        if(S_AXIS_CONFIG_tready = '1') then
                            S_AXIS_CONFIG_tvalid <= '1';
                            S_AXIS_CONFIG_tdata <= (others => '0');
                            config_counter_wait <= (others => '0');
                            state_config <= config_wait1; 
                        else
                            s_axis_config_tvalid <= '0';  
                        end if;  
                when config_wait1 =>
                    S_AXIS_CONFIG_tvalid <= '0';
                    if(config_counter_wait >= reload_counter_wait_max) then
                        start_filter <= '1';
                        state_config <= config_wait2; 
                    end if;
                    config_counter_wait <= config_counter_wait + 1;
                when config_wait2 =>                  
                    state_config <= iddle;  
                when others =>
                    null;                   
            end case;                                                                                            
        end if;
    end process; 
     
    --PERFORM FILTER
    filter_inst: process(aclk, aresetn)
    
        type SIGNALFile is file of integer;
        file signal_file: SIGNALFile is in "signal_to_filt.dat";
        variable signal_in: integer; 
    
    begin 
        if (aresetn = '0') then
            -- dividend signals
            S_AXIS_DATA_tvalid <= '0';
            S_AXIS_DATA_tlast <= '0';
            S_AXIS_DATA_tdata <= (others => '0');
            state_filter <= iddle;  
            filter_counter <= (others => '0');
            filter_counter_wait <= (others => '0');
            filter_tvalid_counter <= (others => '0');
        elsif (aclk'event and aclk = '1') then
            case state_filter is        
                when iddle =>
                    if(start_filter = '1') then
                        state_filter<= filter;  
                        filter_counter <= (others => '0'); 
                    end if;
                    if(M_AXIS_DATA_tvalid = '1' ) then
                        filter_tvalid_counter <= filter_tvalid_counter + 1;
                    else 
                        filter_tvalid_counter <= (others => '0');
                    end if;                   
                when filter =>
--                    if(M_AXIS_DATA_tvalid = '1' ) then
--                        filter_tvalid_counter <= filter_tvalid_counter + 1;
--                    else 
--                        filter_tvalid_counter <= (others => '0');
--                    end if; 
                    if(S_AXIS_DATA_tready = '1') then 
                        filter_tvalid_counter <= filter_tvalid_counter + 1;                     
                        S_AXIS_DATA_tvalid <= '1';
                        S_AXIS_DATA_tlast <= '0';
                        -- read coeff
                        if(filter_counter <= 1023) then
                            if not endfile(signal_file) then
                                read (signal_file, signal_in);
                                S_AXIS_DATA_tdata <= std_logic_vector(to_signed(signal_in, S_AXIS_DATA_tdata'length));
                            end if;
                            if not endfile(signal_file) then
                                read (signal_file, signal_in);
                            end if;
                        else
                            S_AXIS_DATA_tdata <= (others => '0');
                        end if;
                        --
                        if(filter_counter >= 1279) then
                            S_AXIS_DATA_tlast <= '1';  
                            filter_counter_wait <= (others => '0');
                            state_filter <= filter2;
                        else
                            state_filter<= filter1;
                            --state_filter<= filterx;
                        end if;
                        filter_counter <= filter_counter + 1;
                    else
                        S_AXIS_DATA_tvalid <= '0';  
                    end if;
                when filterx =>
                    if(S_AXIS_DATA_tready = '0') then 
                        S_AXIS_DATA_tvalid <= '0';
                        state_filter<= filter;
                    end if;
                when filter1 =>
                    S_AXIS_DATA_tvalid <= '0';
                    state_filter<= filter3; 
                when filter3 =>
                    S_AXIS_DATA_tvalid <= '0';
                    state_filter<= filter4; 
                when filter4 =>
                    state_filter<= filter;     
                when filter2 =>
                    S_AXIS_DATA_tvalid <= '0';
                    S_AXIS_DATA_tlast <= '0'; 
                    state_filter<= filter_wait;            
                when filter_wait =>           
                    if(filter_counter_wait >= filter_counter_wait_max) then
                        state_filter <= iddle; 
                    end if;
                    filter_counter_wait <= filter_counter_wait + 1;
                    if(M_AXIS_DATA_tvalid = '1' ) then
                        filter_tvalid_counter <= filter_tvalid_counter + 1;
                    else 
                        filter_tvalid_counter <= (others => '0');
                    end if; 
                when others =>
                    null;                   
            end case;                                                                                            
        end if;
    end process;  
    
     -- M_AXIS_DATA_tvalid_counter process
     output_filter: process(aclk, aresetn)
     begin
        if (aresetn = '0') then
            -- reset counter
            M_AXIS_DATA_tdata_filtered1 <= (others => '0');
            M_AXIS_DATA_tdata_counter <= (others => '0');
            state_filtered_output <= idle; 
        elsif (aclk'event and aclk = '1') then
            case state_filtered_output is        
                when idle =>
                    if(M_AXIS_DATA_tvalid = '1') then 
                        M_AXIS_DATA_tdata_filtered1 <= M_AXIS_DATA_tdata(34 downto 19);                 
                        state_filtered_output <= s0;    
                    end if;     
               when s0 =>
                    if(M_AXIS_DATA_tvalid = '1') then
                        M_AXIS_DATA_tdata_filtered1 <= M_AXIS_DATA_tdata(34 downto 19); 
                        M_AXIS_DATA_tdata_counter  <= M_AXIS_DATA_tdata_counter + 1; 
                        state_filtered_output <= s1; 
                    end if;
                    if(M_AXIS_DATA_tlast = '1') then  
                        state_filtered_output <= idle; 
                    end if;             
               when others =>             
                    null;  
           end case;  
        end if;
     end process;
     
     result_filter_process: process(aclk, aresetn)                                                  
     begin 
       if (aresetn = '0') then
           state_result <= idle;  
           result_filter <= (others => (others => '0'));
           result_filter_counter <= (others => '0');
           result_filter_counter2 <= (others => '0');
           result_filter_data <= (others => '0');
       elsif (aclk'event and aclk = '1') then
           case state_result is  
              when idle =>   
                   if(M_AXIS_DATA_tvalid = '1') then
                       state_result <= store_result; 
                       result_filter(to_integer(result_filter_counter)) <= M_AXIS_DATA_tdata;
                       result_filter_counter <= result_filter_counter + 1;
                       result_filter_counter2 <= (others => '0');
                   end if;
               when store_result =>
                   if(M_AXIS_DATA_tvalid = '1') then
                        if(result_filter_counter >= 256) then
                            result_filter_counter2 <= result_filter_counter2 + 1;
                            result_filter(to_integer(result_filter_counter2)) <= M_AXIS_DATA_tdata;
                        end if;
                        result_filter_counter <= result_filter_counter + 1;
                   end if; 
                   if(M_AXIS_DATA_tlast = '1') then
                        state_result <= show_result; 
                        result_filter_counter <= result_filter_counter2;
                        result_filter_counter2 <= (others => '0');
                   end if;
               when show_result => 
                   if(result_filter_counter2 <= result_filter_counter) then
                       result_filter_data <= result_filter(to_integer(result_filter_counter2))(34 downto 19);
                       result_filter_counter2 <= result_filter_counter2 + 1;
                   else  
                       state_result <= idle;
                   end if;  
               when others =>
                   null;                   
           end case;                                                                                            
       end if;
     end process;

end;
