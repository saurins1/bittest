clear all;close all;

signal_size = 16;
n_coeff = 4;
n_dsp = 2;

signal = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16];
coefficients = [1 2 2 1];

signal_size_in = signal_size+n_coeff;

signal_in = int64([signal zeros(1,n_coeff)]);
signal_filtered = int64(zeros(1,signal_size_in));
%signal_filtered_x = int64(zeros(1,1024+256));



for n = 1:signal_size_in
    nx=n;
    for i = 1:n_coeff
        if(nx == 0)
            signal_filtered(n) = signal_filtered(n) + 0;
        else
            signal_filtered(n) = signal_filtered(n) + signal_in(nx)*coefficients(i); 
            nx = nx - 1;       
        end
    end
    %signal_filtered_x(n) = bitsra(int64(signal_filtered(n)),L);
end

figure(1);
plot(signal);

figure(2);
plot(signal_in);

figure(3);
plot(signal_filtered);

figure(4);
plot(signal_filtered(n_coeff+1:signal_size_in));