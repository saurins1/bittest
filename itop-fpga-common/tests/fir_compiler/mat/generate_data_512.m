clear all;close all;

Fs = 100000000;
%tones = [100000 200000 300000 400000 500000 1000000 2000000 2500000 6000000 12000000 13000000 135000000 120000000]';
tones = [100000 1000000 4000000 10000000]';

t=[0:1/Fs:8191/Fs];
signal = 0;
for i=1:size(tones);
    signal = signal+sin(2*pi*tones(i)*t);
end  
figure(1);
plot(signal);
%Normalize signal between 4095 and -4095
to_normalize = 2047;
signal=signal/max(abs(signal));
signal = signal*to_normalize;
signal = round(signal);
figure(1);
plot(signal);

load("band_filter_512.mat")

a=1

band_filter_512.Arithmetic = 'fixed'
band_filter_512.coeffautoScale = false

a=2

band_filter_512.CoeffWordLength = 16
band_filter_512.NumFracLength = 15

band_filter_512.FilterInternals = 'SpecifyPrecision'

a=3

band_filter_512.RoundMode = 'round'
band_filter_512.OverflowMode = 'saturate'

band_filter_512.InputWordLength = 16
band_filter_512.InputFracLength = 15

band_filter_512.OutputWordLength = 16
band_filter_512.OutputFracLength = 15

band_filter_512.ProductWordLength = 32
band_filter_512.ProductFracLength = 31

band_filter_512.AccumWordLength = 32
band_filter_512.AccumFracLength = 31

output_filter = filter(band_filter_512,signal);
figure(2);
plot(output_filter);

coeffs_512 = coeffs(band_filter_512)
