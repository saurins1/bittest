clear all;close all;

load("band_filter_256.mat");
load("band_filter_512.mat");
load("band_filter_1024.mat");

load("coeff_256.mat");
load("coeff_512.mat");
load("coeff_1024.mat");

Fs = 100000000;
%tones = [100000 200000 300000 400000 500000 1000000 2000000 2500000 6000000 12000000 13000000 135000000 120000000]';
tones = [100000 1000000 4000000 10000000]';

t=[0:1/Fs:8191/Fs];
signal = 0
for i=1:size(tones);
    signal = signal+sin(2*pi*tones(i)*t);
end  
figure(1);
plot(signal);
%Normalize signal between 4095 and -4095
to_normalize = 2047;
signal=signal/max(abs(signal));
signal = signal*to_normalize;
signal = int64(round(signal));
figure(2);
plot(signal);

file_save_1 = fopen('signal_to_filt.dat','w');
fwrite(file_save_1,signal,'int64');
fclose(file_save_1);

%Coefficients

%https://www.mathworks.com/help/dsp/ug/create-an-fir-filter-using-integer-coefficients.html

%256
B=16
L=floor(log2((2^(B-1)-1)/max(coeff_256)))
new_coeff_256 = int64(floor(coeff_256*2^L));

figure(3);
plot(new_coeff_256);

file_save_256 = fopen('coefficients_256.dat','w');
fwrite(file_save_256,new_coeff_256,'int64');
fclose(file_save_256);

%512
L=floor(log2((2^(B-1)-1)/max(coeff_512)))
new_coeff_512 = int64(floor(coeff_512*2^L));

figure(4);
plot(new_coeff_512);

file_save_512 = fopen('coefficients_512.dat','w');
fwrite(file_save_512,new_coeff_512,'int64');
fclose(file_save_512);

%1024
L=floor(log2((2^(B-1)-1)/max(coeff_1024)))
new_coeff_1024 = int64(floor(coeff_1024*2^L));

figure(5);
plot(new_coeff_1024);

file_save_1024 = fopen('coefficients_1024.dat','w');
fwrite(file_save_1024,new_coeff_1024,'int64');
fclose(file_save_1024);

%filter

% get(band_filter_256);
% specifyall(band_filter_256);
% signal_256 = filter(band_filter_256,signal);
% figure(6);
% plot(signal_256);
% 
% get(band_filter_512);
% specifyall(band_filter_512);
% signal_512 = filter(band_filter_512,signal);
% figure(7);
% plot(signal_512);
% 
% get(band_filter_1024);
% specifyall(band_filter_1024);
% signal_1024 = filter(band_filter_1024,signal);
% figure(8);
% plot(signal_1024);

signal_256 = int64(zeros(1,8191));
signal_256_x = int64(zeros(1,8191));

signal_x = signal;
new_coeff_256_x = new_coeff_256;

for n = 1:8191
    nx=n;
    for i = 1:256
        if(nx == 0)
            signal_256(n) = signal_256(n) + 0;
        else
            signal_256(n) = signal_256(n) + signal(nx)*new_coeff_256(i); 
            nx = nx - 1;
        end
    end
    signal_256_x(n) = bitsra(int64(signal_256(n)),L);
end

%figure(6);
%plot(signal_256);

signal_new = int64([signal(1:1024) zeros(1,256)]);
signal_512 = int64(zeros(1,1024+256));
signal_512_x = int64(zeros(1,1024+256));

new_n = 1024+256;
n_coeff = 512;

for n = 1:new_n
    nx=n;
    for i = 1:n_coeff
        if(nx == 0)
            signal_512(n) = signal_512(n) + 0;
        else
            signal_512(n) = signal_512(n) + signal_new(nx)*new_coeff_512(i); 
            nx = nx - 1;       
        end
    end
    signal_512_x(n) = bitsra(int64(signal_512(n)),L);
end

signal_512 = signal_512(257:1280);
signal_512_x_1 = signal_512_x(257:1280);
signal_512_x = signal_512_x(257:1280);


figure(6);
plot(signal_512_x);

signal_512 = [signal_512(1:1024) zeros(1,256)];
signal_512_x = [signal_512_x(1:1024) zeros(1,256)];

signal_512_2 = int64(zeros(1,1024+256));
signal_512_x_2 = int64(zeros(1,1024+256));

for n = 1:new_n
    nx=n;
    for i = 1:n_coeff
        if(nx == 0)
            signal_512_2(n) = signal_512_2(n) + 0;
        else
            signal_512_2(n) = signal_512_2(n) + signal_512_x(nx)*new_coeff_512(i); 
            nx = nx - 1;
        end
    end
    signal_512_x_2(n) = bitsra(int64(signal_512_2(n)),L);
end

signal_512_2 = signal_512_2(257:1280);
signal_512_x_2 = signal_512_x_2(257:1280);

figure(7);
plot(signal_512_x_2);

figure(8);
%plot([signal_512_x_2(257:1280)', signal_512_x(1:1024)']);
plot([signal_512_x_2', signal_512_x_1']);
%plot([signal_512_x_2(1:1023)', signal_512_x_1(2:1024)']);

