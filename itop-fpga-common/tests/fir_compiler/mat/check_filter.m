get(band_filter);
specifyall(band_filter);

Fs = 100000000;
%tones = [100 1000 10000 100000 1000000 3500000 35000000 350000000]';
tones = [1000 3500000 30000000 60000000]';

t=[0:1/Fs:8191/Fs];
signal = 0
for i=1:size(tones);
    signal = signal+sin(2*pi*tones(i)*t);
end  
n=size(tones);
signal = signal/n(1);
signal = signal*4095;
signal2 = filter(band_filter,signal);
figure(1);
plot(signal);
figure(2);
plot(signal2);