----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/23/2017 05:30:41 AM
-- Design Name: 
-- Module Name: configuration_register - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity configuration_register is
	generic (
	    REGISTER_WIDTH    : integer    := 32;
        -- Users to add parameters here
        GATE_RANGE                      : integer    := 32;
        GATE_ALARM_CROSSING             : integer    := 2;
        GATE_TOF_ALGORITHM_RANGE        : integer    := 4;
        GATE_TOF_AVG_RANGE              : integer    := 8;
        GATE_TOF_RANGE                  : integer    := 32;
        GATE_AXIS_RANGE                 : integer    := 48
    );
    Port ( 
        aclk                                : in std_logic;
        aresetn                             : in std_logic;
        CONFIG_EN                           : in std_logic;   
        LOAD_CONFIG                         : in std_logic;
        CONFIG_INDEX                        : in std_logic_vector(REGISTER_WIDTH-1 downto 0);   
        CONFIG_VALUE                        : in std_logic_vector(REGISTER_WIDTH-1 downto 0); 
        -- CONFIG VALUES
        -- transmission
        CHANNEL_EN                          : out std_logic;  
        TRANSMITTER_VOLTAGE                 : out std_logic_vector(15 downto 0);  
        TRANSMITTER_BURST_FREQUENCY         : out std_logic_vector(GATE_RANGE-1 downto 0);  
        TRANSMITTER_N_CYCLES                : out std_logic_vector(7 downto 0);  
        TRANSMITTER_PRF                     : out std_logic_vector(GATE_RANGE-1 downto 0);
        TRANSMITTER_DELAY                   : out std_logic_vector(GATE_RANGE-1 downto 0); 
        TRANSMITTER_DIRECTIONAL_PHASING     : out std_logic_vector(15 downto 0); 
        -- reception
        RECEIVER_SAMPLING_FREQUENCY         : out std_logic_vector(GATE_RANGE-1 downto 0);  
        RECEIVER_DATA_WINDOW                : out std_logic_vector(GATE_RANGE-1 downto 0);  
        RECEIVER_DELAY                      : out std_logic_vector(GATE_RANGE-1 downto 0);  
        RECEIVER_GAIN                       : out std_logic_vector(GATE_RANGE-1 downto 0);  
        RECEIVER_EXTERNAL_MULTIPLEXER_EN    : out std_logic;  
        -- magnet
        MAGNET_MODE                         : out std_logic_vector(7 downto 0); 
        MAGNET_PULSE_WIDTH                  : out std_logic_vector(GATE_RANGE-1 downto 0); 
        MAGNET_INITIAL_DELAY                : out std_logic_vector(GATE_RANGE-1 downto 0); 
        -- GATE1
		GATE1_EN                            : out std_logic;  
        GATE1_START                         : out std_logic_vector(GATE_RANGE-1 downto 0);
        GATE1_WIDTH                         : out std_logic_vector(GATE_RANGE-1 downto 0);    
        GATE1_AMP_THRESHOLD                 : out std_logic_vector(GATE_RANGE-1 downto 0);
        GATE1_TOF_ALGORITHM                 : out std_logic_vector(GATE_TOF_ALGORITHM_RANGE-1 downto 0);
        GATE1_TOF_AVG                       : out std_logic_vector(GATE_TOF_AVG_RANGE-1 downto 0);
        GATE1_TOF_MIN_THICKNESS             : out std_logic_vector(GATE_TOF_RANGE-1 downto 0);
        GATE1_AMP_THRESHOLD_ALARM_EN        : out std_logic; 
        GATE1_AMP_THRESHOLD_ALARM_CROSSING  : out std_logic_vector(GATE_ALARM_CROSSING-1 downto 0);   
        GATE1_TOF_ALARM_EN                  : out std_logic; 
        GATE1_TOF_ALARM_TYPE                : out std_logic;
        GATE1_TOF_ALARM_CROSSING            : out std_logic_vector(GATE_ALARM_CROSSING-1 downto 0);
        GATE1_TOF_ALARM_MIN_VALUE           : out std_logic_vector(GATE_TOF_RANGE-1 downto 0);
        GATE1_TOF_ALARM_MAX_VALUE           : out std_logic_vector(GATE_TOF_RANGE-1 downto 0);
        GATE1_TRACK_GATE_3                  : out std_logic; 
        GATE1_NORMALIZE_TO_AMP_GATE_1       : out std_logic; 
        -- GATE2
		GATE2_EN                            : out std_logic;  
        GATE2_START                         : out std_logic_vector(GATE_RANGE-1 downto 0);
        GATE2_WIDTH                         : out std_logic_vector(GATE_RANGE-1 downto 0);    
        GATE2_AMP_THRESHOLD                 : out std_logic_vector(GATE_RANGE-1 downto 0);
        GATE2_TOF_ALGORITHM                 : out std_logic_vector(GATE_TOF_ALGORITHM_RANGE-1 downto 0);
        GATE2_TOF_AVG                       : out std_logic_vector(GATE_TOF_AVG_RANGE-1 downto 0);
        GATE2_TOF_MIN_THICKNESS             : out std_logic_vector(GATE_TOF_RANGE-1 downto 0);
        GATE2_AMP_THRESHOLD_ALARM_EN        : out std_logic; 
        GATE2_AMP_THRESHOLD_ALARM_CROSSING  : out std_logic_vector(GATE_ALARM_CROSSING-1 downto 0);   
        GATE2_TOF_ALARM_EN                  : out std_logic; 
        GATE2_TOF_ALARM_TYPE                : out std_logic;
        GATE2_TOF_ALARM_CROSSING            : out std_logic_vector(GATE_ALARM_CROSSING-1 downto 0);
        GATE2_TOF_ALARM_MIN_VALUE           : out std_logic_vector(GATE_TOF_RANGE-1 downto 0);
        GATE2_TOF_ALARM_MAX_VALUE           : out std_logic_vector(GATE_TOF_RANGE-1 downto 0);
        GATE2_TRACK_GATE_3                  : out std_logic; 
        GATE2_NORMALIZE_TO_AMP_GATE_1       : out std_logic; 
        -- GATE3
		GATE3_EN                            : out std_logic;  
        GATE3_START                         : out std_logic_vector(GATE_RANGE-1 downto 0);
        GATE3_WIDTH                         : out std_logic_vector(GATE_RANGE-1 downto 0);    
        GATE3_AMP_THRESHOLD                 : out std_logic_vector(GATE_RANGE-1 downto 0);
        GATE3_TOF_ALGORITHM                 : out std_logic_vector(GATE_TOF_ALGORITHM_RANGE-1 downto 0);
        GATE3_TOF_AVG                       : out std_logic_vector(GATE_TOF_AVG_RANGE-1 downto 0);
        GATE3_TOF_MIN_THICKNESS             : out std_logic_vector(GATE_TOF_RANGE-1 downto 0);
        GATE3_AMP_THRESHOLD_ALARM_EN        : out std_logic; 
        GATE3_AMP_THRESHOLD_ALARM_CROSSING  : out std_logic_vector(GATE_ALARM_CROSSING-1 downto 0);   
        GATE3_TOF_ALARM_EN                  : out std_logic; 
        GATE3_TOF_ALARM_TYPE                : out std_logic;
        GATE3_TOF_ALARM_CROSSING            : out std_logic_vector(GATE_ALARM_CROSSING-1 downto 0);
        GATE3_TOF_ALARM_MIN_VALUE           : out std_logic_vector(GATE_TOF_RANGE-1 downto 0);
        GATE3_TOF_ALARM_MAX_VALUE           : out std_logic_vector(GATE_TOF_RANGE-1 downto 0);
        GATE3_TRACK_GATE_3                  : out std_logic; 
        GATE3_NORMALIZE_TO_AMP_GATE_1       : out std_logic; 
        -- dsp
        DSP_AVERAGE                         : out std_logic_vector(GATE_RANGE-1 downto 0);
        DSP_NOISE_REDUCTION_FILTER          : out std_logic_vector(GATE_RANGE-1 downto 0);  
        DSP_AUTOMATIC_FREQUENCY_FILTER_EN   : out std_logic;
        DSP_BAND_PASS_FILTER_EN             : out std_logic; 
        --DSP_BAND_PASS_FILTER_LOWER_CUT_FREQ : out std_logic_vector(REGISTER_WIDTH-1 downto 0); 
        --DSP_BAND_PASS_FILTER_UPPER_CUT_FREQ : out std_logic_vector(REGISTER_WIDTH-1 downto 0); 
        -- velocity
        VELOCITY_PROBE_ZERO                 : out std_logic_vector(GATE_RANGE-1 downto 0);
        VELOCITY_MEASURED_TEMPERATURE       : out std_logic_vector(GATE_RANGE-1 downto 0);
        VELOCITY_REFERENCE_TEMPERATURE      : out std_logic_vector(GATE_RANGE-1 downto 0);
        VELOCITY_TEMPERATURE_LOCK           : out std_logic_vector(GATE_RANGE-1 downto 0);
        VELOCITY_TEMPERATURE_CORRECTION     : out std_logic_vector(GATE_RANGE-1 downto 0);
        VELOCITY_REFERENCE_VELOCITY         : out std_logic_vector(GATE_RANGE-1 downto 0);
        VELOCITY_MEASURED_VELOCITY          : out std_logic_vector(GATE_RANGE-1 downto 0)
    );
end configuration_register;

architecture Behavioral of configuration_register is
     
    type configuration_array is array ( 0 to 127 ) of std_logic_vector(REGISTER_WIDTH-1 downto 0);
    signal config : configuration_array;

begin

	-- Reload coefficients into the local matrix
    config_inst: process(aclk, aresetn)
    begin 
        if (aresetn = '0') then
            -- Coefficient matrix
            config <= ((others=> (others=>'0')));                           
        elsif (aclk'event and aclk = '1') then
            if(LOAD_CONFIG = '1') then
                config(to_integer(unsigned(CONFIG_INDEX))) <= CONFIG_VALUE(REGISTER_WIDTH-1 downto 0);
            end if;                                                                                         
        end if;
    end process;

    -- update config
    update_config_process: process(aclk, aresetn)
    begin 
        if (aresetn = '0') then
            CHANNEL_EN                          <= '0'; 
            -- transmission
            TRANSMITTER_VOLTAGE                 <= (others => '0');
            TRANSMITTER_BURST_FREQUENCY         <= (others => '0');  
            TRANSMITTER_N_CYCLES                <= (others => '0'); 
            TRANSMITTER_PRF                     <= (others => '0');
            TRANSMITTER_DELAY                   <= (others => '0'); 
            TRANSMITTER_DIRECTIONAL_PHASING     <= (others => '0'); 
            -- reception
            RECEIVER_SAMPLING_FREQUENCY         <= (others => '0'); 
            RECEIVER_DATA_WINDOW                <= (others => '0');  
            RECEIVER_DELAY                      <= (others => '0');  
            RECEIVER_GAIN                       <= (others => '0'); 
            RECEIVER_EXTERNAL_MULTIPLEXER_EN    <= '0'; 
            -- magnet
            MAGNET_MODE                         <= (others => '0');
            MAGNET_PULSE_WIDTH                  <= (others => '0'); 
            MAGNET_INITIAL_DELAY                <= (others => '0');
            -- GATE1
            GATE1_EN                            <= '0';
            GATE1_START                         <= (others => '0');
            GATE1_WIDTH                         <= (others => '0');
            GATE1_AMP_THRESHOLD                 <= (others => '0');
            GATE1_TOF_ALGORITHM                 <= (others => '0');
            GATE1_TOF_AVG                       <= (others => '0');
            GATE1_TOF_MIN_THICKNESS             <= (others => '0');
            GATE1_AMP_THRESHOLD_ALARM_EN        <= '0';
            GATE1_AMP_THRESHOLD_ALARM_CROSSING  <= (others => '0');
            GATE1_TOF_ALARM_EN                  <= '0';
            GATE1_TOF_ALARM_TYPE                <= '0';
            GATE1_TOF_ALARM_CROSSING            <= (others => '0');
            GATE1_TOF_ALARM_MIN_VALUE           <= (others => '0');  
            GATE1_TOF_ALARM_MAX_VALUE           <= (others => '0');
            GATE1_TRACK_GATE_3                  <= '0';
            GATE1_NORMALIZE_TO_AMP_GATE_1       <= '0';
            -- GATE2
            GATE2_EN                            <= '0';
            GATE2_START                         <= (others => '0');
            GATE2_WIDTH                         <= (others => '0');
            GATE2_AMP_THRESHOLD                 <= (others => '0');
            GATE2_TOF_ALGORITHM                 <= (others => '0');
            GATE2_TOF_AVG                       <= (others => '0');
            GATE2_TOF_MIN_THICKNESS             <= (others => '0');
            GATE2_AMP_THRESHOLD_ALARM_EN        <= '0';
            GATE2_AMP_THRESHOLD_ALARM_CROSSING  <= (others => '0');
            GATE2_TOF_ALARM_EN                  <= '0';
            GATE2_TOF_ALARM_TYPE                <= '0';
            GATE2_TOF_ALARM_CROSSING            <= (others => '0');
            GATE2_TOF_ALARM_MIN_VALUE           <= (others => '0');  
            GATE2_TOF_ALARM_MAX_VALUE           <= (others => '0');
            GATE2_TRACK_GATE_3                  <= '0';
            GATE2_NORMALIZE_TO_AMP_GATE_1       <= '0';
            -- GATE3
            GATE3_EN                            <= '0';
            GATE3_START                         <= (others => '0');
            GATE3_WIDTH                         <= (others => '0');
            GATE3_AMP_THRESHOLD                 <= (others => '0');
            GATE3_TOF_ALGORITHM                 <= (others => '0');
            GATE3_TOF_AVG                       <= (others => '0');
            GATE3_TOF_MIN_THICKNESS             <= (others => '0');
            GATE3_AMP_THRESHOLD_ALARM_EN        <= '0';
            GATE3_AMP_THRESHOLD_ALARM_CROSSING  <= (others => '0');
            GATE3_TOF_ALARM_EN                  <= '0';
            GATE3_TOF_ALARM_TYPE                <= '0';
            GATE3_TOF_ALARM_CROSSING            <= (others => '0');
            GATE3_TOF_ALARM_MIN_VALUE           <= (others => '0');  
            GATE3_TOF_ALARM_MAX_VALUE           <= (others => '0');
            GATE3_TRACK_GATE_3                  <= '0';
            GATE3_NORMALIZE_TO_AMP_GATE_1       <= '0';
            -- dsp
            DSP_AVERAGE                         <= (others => '0');
            DSP_NOISE_REDUCTION_FILTER          <= (others => '0'); 
            DSP_AUTOMATIC_FREQUENCY_FILTER_EN   <= '0';
            DSP_BAND_PASS_FILTER_EN             <= '0';
            --DSP_BAND_PASS_FILTER_LOWER_CUT_FREQ : out std_logic_vector(REGISTER_WIDTH-1 downto 0); 
            --DSP_BAND_PASS_FILTER_UPPER_CUT_FREQ : out std_logic_vector(REGISTER_WIDTH-1 downto 0); 
            -- velocity
            VELOCITY_PROBE_ZERO                 <= (others => '0');
            VELOCITY_MEASURED_TEMPERATURE       <= (others => '0');
            VELOCITY_REFERENCE_TEMPERATURE      <= (others => '0');
            VELOCITY_TEMPERATURE_LOCK           <= (others => '0');
            VELOCITY_TEMPERATURE_CORRECTION     <= (others => '0');
            VELOCITY_REFERENCE_VELOCITY         <= (others => '0');
            VELOCITY_MEASURED_VELOCITY          <= (others => '0');
       elsif (aclk'event and aclk = '1') then
            if(CONFIG_EN = '1') then
                -- transmission
                CHANNEL_EN                          <= config(0)(0);
                TRANSMITTER_VOLTAGE                 <= config(1)(15 downto 0);
                TRANSMITTER_BURST_FREQUENCY         <= config(2)(GATE_RANGE-1 downto 0);
                TRANSMITTER_N_CYCLES                <= config(3)(7 downto 0);
                TRANSMITTER_PRF                     <= config(4)(GATE_RANGE-1 downto 0);
                TRANSMITTER_DELAY                    <= config(5)(GATE_RANGE-1 downto 0);
                TRANSMITTER_DIRECTIONAL_PHASING     <= config(6)(15 downto 0);
                -- reception
                RECEIVER_SAMPLING_FREQUENCY         <= config(7)(GATE_RANGE-1 downto 0); -- integer divider of max freq = 100 Mhz
                RECEIVER_DATA_WINDOW                <= config(8)(GATE_RANGE-1 downto 0);
                RECEIVER_DELAY                      <= config(9)(GATE_RANGE-1 downto 0);  
                RECEIVER_GAIN                       <= config(10)(GATE_RANGE-1 downto 0); 
                RECEIVER_EXTERNAL_MULTIPLEXER_EN    <= config(11)(0);
                -- magnet
                MAGNET_MODE                         <= config(12)(7 downto 0);
                MAGNET_PULSE_WIDTH                  <= config(13)(GATE_RANGE-1 downto 0);
                MAGNET_INITIAL_DELAY                <= config(14)(GATE_RANGE-1 downto 0);
                -- GATE1 
                --GATE1_ID                            <= config(15)(GATE_RANGE-1 downto 0);              
                GATE1_EN                            <= config(16)(0);
                GATE1_START                         <= config(17)(GATE_RANGE-1 downto 0); 
                GATE1_WIDTH                         <= config(18)(GATE_RANGE-1 downto 0); 
                GATE1_AMP_THRESHOLD                 <= config(19)(GATE_RANGE-1 downto 0); 
                GATE1_TOF_ALGORITHM                 <= config(20)(GATE_TOF_ALGORITHM_RANGE-1 downto 0); 
                GATE1_TOF_AVG                       <= config(21)(GATE_TOF_AVG_RANGE-1 downto 0); 
                GATE1_TOF_MIN_THICKNESS             <= config(22)(GATE_TOF_RANGE-1 downto 0); 
                GATE1_AMP_THRESHOLD_ALARM_EN        <= config(23)(0);
                GATE1_AMP_THRESHOLD_ALARM_CROSSING  <= config(24)(GATE_ALARM_CROSSING-1 downto 0);
                GATE1_TOF_ALARM_EN                  <= config(25)(0);
                GATE1_TOF_ALARM_TYPE                <= config(26)(0);
                GATE1_TOF_ALARM_CROSSING            <= config(27)(GATE_ALARM_CROSSING-1 downto 0);
                GATE1_TOF_ALARM_MIN_VALUE           <= config(28)(GATE_TOF_RANGE-1 downto 0); 
                GATE1_TOF_ALARM_MAX_VALUE           <= config(29)(GATE_TOF_RANGE-1 downto 0); 
                GATE1_TRACK_GATE_3                  <= config(30)(0);
                GATE1_NORMALIZE_TO_AMP_GATE_1       <= config(31)(0);
                -- GATE2
                --GATE2_ID                            <= config(32)(GATE_RANGE-1 downto 0);  
                GATE2_EN                            <= config(33)(0);
                GATE2_START                         <= config(34)(GATE_RANGE-1 downto 0); 
                GATE2_WIDTH                         <= config(35)(GATE_RANGE-1 downto 0); 
                GATE2_AMP_THRESHOLD                 <= config(36)(GATE_RANGE-1 downto 0); 
                GATE2_TOF_ALGORITHM                 <= config(37)(GATE_TOF_ALGORITHM_RANGE-1 downto 0); 
                GATE2_TOF_AVG                       <= config(38)(GATE_TOF_AVG_RANGE-1 downto 0); 
                GATE2_TOF_MIN_THICKNESS             <= config(39)(GATE_TOF_RANGE-1 downto 0); 
                GATE2_AMP_THRESHOLD_ALARM_EN        <= config(40)(0);
                GATE2_AMP_THRESHOLD_ALARM_CROSSING  <= config(41)(GATE_ALARM_CROSSING-1 downto 0);
                GATE2_TOF_ALARM_EN                  <= config(42)(0);
                GATE2_TOF_ALARM_TYPE                <= config(43)(0);
                GATE2_TOF_ALARM_CROSSING            <= config(44)(GATE_ALARM_CROSSING-1 downto 0);
                GATE2_TOF_ALARM_MIN_VALUE           <= config(45)(GATE_TOF_RANGE-1 downto 0); 
                GATE2_TOF_ALARM_MAX_VALUE           <= config(46)(GATE_TOF_RANGE-1 downto 0); 
                GATE2_TRACK_GATE_3                  <= config(47)(0);
                GATE2_NORMALIZE_TO_AMP_GATE_1       <= config(48)(0);
                -- GATE3
                --GATE3_ID                            <= config(49)(GATE_RANGE-1 downto 0);  
                GATE3_EN                            <= config(50)(0);
                GATE3_START                         <= config(51)(GATE_RANGE-1 downto 0); 
                GATE3_WIDTH                         <= config(52)(GATE_RANGE-1 downto 0); 
                GATE3_AMP_THRESHOLD                 <= config(53)(GATE_RANGE-1 downto 0); 
                GATE3_TOF_ALGORITHM                 <= config(54)(GATE_TOF_ALGORITHM_RANGE-1 downto 0); 
                GATE3_TOF_AVG                       <= config(55)(GATE_TOF_AVG_RANGE-1 downto 0); 
                GATE3_TOF_MIN_THICKNESS             <= config(56)(GATE_TOF_RANGE-1 downto 0); 
                GATE3_AMP_THRESHOLD_ALARM_EN        <= config(57)(0);
                GATE3_AMP_THRESHOLD_ALARM_CROSSING  <= config(58)(GATE_ALARM_CROSSING-1 downto 0);
                GATE3_TOF_ALARM_EN                  <= config(59)(0);
                GATE3_TOF_ALARM_TYPE                <= config(60)(0);
                GATE3_TOF_ALARM_CROSSING            <= config(61)(GATE_ALARM_CROSSING-1 downto 0);
                GATE3_TOF_ALARM_MIN_VALUE           <= config(62)(GATE_TOF_RANGE-1 downto 0); 
                GATE3_TOF_ALARM_MAX_VALUE           <= config(63)(GATE_TOF_RANGE-1 downto 0); 
                GATE3_TRACK_GATE_3                  <= config(64)(0);
                GATE3_NORMALIZE_TO_AMP_GATE_1       <= config(65)(0);
                -- dsp
                DSP_AVERAGE                         <= config(66)(GATE_RANGE-1 downto 0); 
                DSP_NOISE_REDUCTION_FILTER          <= config(67)(GATE_RANGE-1 downto 0); 
                DSP_AUTOMATIC_FREQUENCY_FILTER_EN   <= config(68)(0);
                DSP_BAND_PASS_FILTER_EN             <= config(69)(0);
                --DSP_BAND_PASS_FILTER_LOWER_CUT_FREQ : out std_logic_vector(REGISTER_WIDTH-1 downto 0); 
                --DSP_BAND_PASS_FILTER_UPPER_CUT_FREQ : out std_logic_vector(REGISTER_WIDTH-1 downto 0); 
                -- velocity
                VELOCITY_PROBE_ZERO                 <= config(72)(GATE_RANGE-1 downto 0);
                VELOCITY_MEASURED_TEMPERATURE       <= config(73)(GATE_RANGE-1 downto 0);
                VELOCITY_REFERENCE_TEMPERATURE      <= config(74)(GATE_RANGE-1 downto 0);
                VELOCITY_TEMPERATURE_LOCK           <= config(75)(GATE_RANGE-1 downto 0);
                VELOCITY_TEMPERATURE_CORRECTION     <= config(76)(GATE_RANGE-1 downto 0);
                VELOCITY_REFERENCE_VELOCITY         <= config(77)(GATE_RANGE-1 downto 0);
                VELOCITY_MEASURED_VELOCITY          <= config(78)(GATE_RANGE-1 downto 0);
            end if;
       end if;
   end process;

end Behavioral;
