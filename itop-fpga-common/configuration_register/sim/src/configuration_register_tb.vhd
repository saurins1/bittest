----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/03/2017 07:22:28 AM
-- Design Name: 
-- Module Name: channel_configuration_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity configuration_register_tb is
  	generic (
        REGISTER_WIDTH    : integer    := 32;
        GATE_RANGE                      : integer    := 32;
        GATE_ALARM_CROSSING             : integer    := 2;
        GATE_TOF_ALGORITHM_RANGE        : integer    := 4;
        GATE_TOF_AVG_RANGE              : integer    := 8;
        GATE_TOF_RANGE                  : integer    := 32;
        GATE_AXIS_RANGE                 : integer    := 48
    );
end;

architecture bench of configuration_register_tb is

  component configuration_register
  	generic (
  	    REGISTER_WIDTH    : integer    := 32;
          GATE_RANGE                      : integer    := 32;
          GATE_ALARM_CROSSING             : integer    := 2;
          GATE_TOF_ALGORITHM_RANGE        : integer    := 4;
          GATE_TOF_AVG_RANGE              : integer    := 8;
          GATE_TOF_RANGE                  : integer    := 32;
          GATE_AXIS_RANGE                 : integer    := 48
      );
      Port ( 
          aclk                                : in std_logic;
          aresetn                             : in std_logic;
          CONFIG_EN                           : in std_logic;   
          LOAD_CONFIG                         : in std_logic;
          CONFIG_INDEX                        : in std_logic_vector(REGISTER_WIDTH-1 downto 0);   
          CONFIG_VALUE                        : in std_logic_vector(REGISTER_WIDTH-1 downto 0);
          -- CONFIG VALUES
          -- transmission
          CHANNEL_EN                          : out std_logic;  
          TRANSMITTER_VOLTAGE                 : out std_logic_vector(15 downto 0);  
          TRANSMITTER_BURST_FREQUENCY         : out std_logic_vector(GATE_RANGE-1 downto 0);  
          TRANSMITTER_N_CYCLES                : out std_logic_vector(7 downto 0);  
          TRANSMITTER_PRF                     : out std_logic_vector(GATE_RANGE-1 downto 0);
          TRANSMITTER_DELAY                   : out std_logic_vector(GATE_RANGE-1 downto 0); 
          TRANSMITTER_DIRECTIONAL_PHASING     : out std_logic_vector(15 downto 0);
          -- reception
          RECEIVER_SAMPLING_FREQUENCY         : out std_logic_vector(GATE_RANGE-1 downto 0);  
          RECEIVER_DATA_WINDOW                : out std_logic_vector(GATE_RANGE-1 downto 0);  
          RECEIVER_DELAY                      : out std_logic_vector(GATE_RANGE-1 downto 0);  
          RECEIVER_GAIN                       : out std_logic_vector(GATE_RANGE-1 downto 0);  
          RECEIVER_EXTERNAL_MULTIPLEXER_EN    : out std_logic;
          -- magnet
          MAGNET_MODE                         : out std_logic_vector(7 downto 0); 
          MAGNET_PULSE_WIDTH                  : out std_logic_vector(GATE_RANGE-1 downto 0); 
          MAGNET_INITIAL_DELAY                : out std_logic_vector(GATE_RANGE-1 downto 0);
          -- GATE1
  		  GATE1_EN                            : out std_logic;  
          GATE1_START                         : out std_logic_vector(GATE_RANGE-1 downto 0);
          GATE1_WIDTH                         : out std_logic_vector(GATE_RANGE-1 downto 0);    
          GATE1_AMP_THRESHOLD                 : out std_logic_vector(GATE_RANGE-1 downto 0);
          GATE1_TOF_ALGORITHM                 : out std_logic_vector(GATE_TOF_ALGORITHM_RANGE-1 downto 0);
          GATE1_TOF_AVG                       : out std_logic_vector(GATE_TOF_AVG_RANGE-1 downto 0);
          GATE1_TOF_MIN_THICKNESS             : out std_logic_vector(GATE_TOF_RANGE-1 downto 0);
          GATE1_AMP_THRESHOLD_ALARM_EN        : out std_logic; 
          GATE1_AMP_THRESHOLD_ALARM_CROSSING  : out std_logic_vector(GATE_ALARM_CROSSING-1 downto 0);   
          GATE1_TOF_ALARM_EN                  : out std_logic; 
          GATE1_TOF_ALARM_TYPE                : out std_logic;
          GATE1_TOF_ALARM_CROSSING            : out std_logic_vector(GATE_ALARM_CROSSING-1 downto 0);
          GATE1_TOF_ALARM_MIN_VALUE           : out std_logic_vector(GATE_TOF_RANGE-1 downto 0);
          GATE1_TOF_ALARM_MAX_VALUE           : out std_logic_vector(GATE_TOF_RANGE-1 downto 0);
          GATE1_TRACK_GATE_3                  : out std_logic; 
          GATE1_NORMALIZE_TO_AMP_GATE_1       : out std_logic;
          -- GATE2
  		  GATE2_EN                            : out std_logic;  
          GATE2_START                         : out std_logic_vector(GATE_RANGE-1 downto 0);
          GATE2_WIDTH                         : out std_logic_vector(GATE_RANGE-1 downto 0);    
          GATE2_AMP_THRESHOLD                 : out std_logic_vector(GATE_RANGE-1 downto 0);
          GATE2_TOF_ALGORITHM                 : out std_logic_vector(GATE_TOF_ALGORITHM_RANGE-1 downto 0);
          GATE2_TOF_AVG                       : out std_logic_vector(GATE_TOF_AVG_RANGE-1 downto 0);
          GATE2_TOF_MIN_THICKNESS             : out std_logic_vector(GATE_TOF_RANGE-1 downto 0);
          GATE2_AMP_THRESHOLD_ALARM_EN        : out std_logic; 
          GATE2_AMP_THRESHOLD_ALARM_CROSSING  : out std_logic_vector(GATE_ALARM_CROSSING-1 downto 0);   
          GATE2_TOF_ALARM_EN                  : out std_logic; 
          GATE2_TOF_ALARM_TYPE                : out std_logic;
          GATE2_TOF_ALARM_CROSSING            : out std_logic_vector(GATE_ALARM_CROSSING-1 downto 0);
          GATE2_TOF_ALARM_MIN_VALUE           : out std_logic_vector(GATE_TOF_RANGE-1 downto 0);
          GATE2_TOF_ALARM_MAX_VALUE           : out std_logic_vector(GATE_TOF_RANGE-1 downto 0);
          GATE2_TRACK_GATE_3                  : out std_logic; 
          GATE2_NORMALIZE_TO_AMP_GATE_1       : out std_logic;
          -- GATE3
  		  GATE3_EN                            : out std_logic;  
          GATE3_START                         : out std_logic_vector(GATE_RANGE-1 downto 0);
          GATE3_WIDTH                         : out std_logic_vector(GATE_RANGE-1 downto 0);    
          GATE3_AMP_THRESHOLD                 : out std_logic_vector(GATE_RANGE-1 downto 0);
          GATE3_TOF_ALGORITHM                 : out std_logic_vector(GATE_TOF_ALGORITHM_RANGE-1 downto 0);
          GATE3_TOF_AVG                       : out std_logic_vector(GATE_TOF_AVG_RANGE-1 downto 0);
          GATE3_TOF_MIN_THICKNESS             : out std_logic_vector(GATE_TOF_RANGE-1 downto 0);
          GATE3_AMP_THRESHOLD_ALARM_EN        : out std_logic; 
          GATE3_AMP_THRESHOLD_ALARM_CROSSING  : out std_logic_vector(GATE_ALARM_CROSSING-1 downto 0);   
          GATE3_TOF_ALARM_EN                  : out std_logic; 
          GATE3_TOF_ALARM_TYPE                : out std_logic;
          GATE3_TOF_ALARM_CROSSING            : out std_logic_vector(GATE_ALARM_CROSSING-1 downto 0);
          GATE3_TOF_ALARM_MIN_VALUE           : out std_logic_vector(GATE_TOF_RANGE-1 downto 0);
          GATE3_TOF_ALARM_MAX_VALUE           : out std_logic_vector(GATE_TOF_RANGE-1 downto 0);
          GATE3_TRACK_GATE_3                  : out std_logic; 
          GATE3_NORMALIZE_TO_AMP_GATE_1       : out std_logic;
          -- dsp
          DSP_AVERAGE                         : out std_logic_vector(GATE_RANGE-1 downto 0);
          DSP_NOISE_REDUCTION_FILTER          : out std_logic_vector(GATE_RANGE-1 downto 0);  
          DSP_AUTOMATIC_FREQUENCY_FILTER_EN   : out std_logic;
          DSP_BAND_PASS_FILTER_EN             : out std_logic;
          --DSP_BAND_PASS_FILTER_LOWER_CUT_FREQ : out std_logic_vector(REGISTER_WIDTH-1 downto 0); 
          --DSP_BAND_PASS_FILTER_UPPER_CUT_FREQ : out std_logic_vector(REGISTER_WIDTH-1 downto 0); 
          -- velocity
          VELOCITY_PROBE_ZERO                 : out std_logic_vector(GATE_RANGE-1 downto 0);
          VELOCITY_MEASURED_TEMPERATURE       : out std_logic_vector(GATE_RANGE-1 downto 0);
          VELOCITY_REFERENCE_TEMPERATURE      : out std_logic_vector(GATE_RANGE-1 downto 0);
          VELOCITY_TEMPERATURE_LOCK           : out std_logic_vector(GATE_RANGE-1 downto 0);
          VELOCITY_TEMPERATURE_CORRECTION     : out std_logic_vector(GATE_RANGE-1 downto 0);
          VELOCITY_REFERENCE_VELOCITY         : out std_logic_vector(GATE_RANGE-1 downto 0);
          VELOCITY_MEASURED_VELOCITY          : out std_logic_vector(GATE_RANGE-1 downto 0)
      );
  end component;

  signal aclk: std_logic:= '0';
  signal aresetn: std_logic:= '0';
  signal CONFIG_EN: std_logic:= '0';
  signal LOAD_CONFIG: std_logic:= '0';
  signal CONFIG_INDEX: std_logic_vector(REGISTER_WIDTH-1 downto 0):= (others => '0');
  signal CONFIG_VALUE: std_logic_vector(REGISTER_WIDTH-1 downto 0):= (others => '0');
  signal CHANNEL_EN: std_logic;
  signal TRANSMITTER_VOLTAGE: std_logic_vector(15 downto 0);
  signal TRANSMITTER_BURST_FREQUENCY: std_logic_vector(GATE_RANGE-1 downto 0);
  signal TRANSMITTER_N_CYCLES: std_logic_vector(7 downto 0);
  signal TRANSMITTER_PRF: std_logic_vector(GATE_RANGE-1 downto 0);
  signal TRANSMITTER_DELAY: std_logic_vector(GATE_RANGE-1 downto 0);
  signal TRANSMITTER_DIRECTIONAL_PHASING: std_logic_vector(15 downto 0);
  signal RECEIVER_SAMPLING_FREQUENCY: std_logic_vector(GATE_RANGE-1 downto 0);
  signal RECEIVER_DATA_WINDOW: std_logic_vector(GATE_RANGE-1 downto 0);
  signal RECEIVER_DELAY: std_logic_vector(GATE_RANGE-1 downto 0);
  signal RECEIVER_GAIN: std_logic_vector(GATE_RANGE-1 downto 0);
  signal RECEIVER_EXTERNAL_MULTIPLEXER_EN: std_logic;
  signal MAGNET_MODE: std_logic_vector(7 downto 0);
  signal MAGNET_PULSE_WIDTH: std_logic_vector(GATE_RANGE-1 downto 0);
  signal MAGNET_INITIAL_DELAY: std_logic_vector(GATE_RANGE-1 downto 0);
  signal GATE1_EN: std_logic;
  signal GATE1_START: std_logic_vector(GATE_RANGE-1 downto 0);
  signal GATE1_WIDTH: std_logic_vector(GATE_RANGE-1 downto 0);
  signal GATE1_AMP_THRESHOLD: std_logic_vector(GATE_RANGE-1 downto 0);
  signal GATE1_TOF_ALGORITHM: std_logic_vector(GATE_TOF_ALGORITHM_RANGE-1 downto 0);
  signal GATE1_TOF_AVG: std_logic_vector(GATE_TOF_AVG_RANGE-1 downto 0);
  signal GATE1_TOF_MIN_THICKNESS: std_logic_vector(GATE_TOF_RANGE-1 downto 0);
  signal GATE1_AMP_THRESHOLD_ALARM_EN: std_logic;
  signal GATE1_AMP_THRESHOLD_ALARM_CROSSING: std_logic_vector(GATE_ALARM_CROSSING-1 downto 0);
  signal GATE1_TOF_ALARM_EN: std_logic;
  signal GATE1_TOF_ALARM_TYPE: std_logic;
  signal GATE1_TOF_ALARM_CROSSING: std_logic_vector(GATE_ALARM_CROSSING-1 downto 0);
  signal GATE1_TOF_ALARM_MIN_VALUE: std_logic_vector(GATE_TOF_RANGE-1 downto 0);
  signal GATE1_TOF_ALARM_MAX_VALUE: std_logic_vector(GATE_TOF_RANGE-1 downto 0);
  signal GATE1_TRACK_GATE_3: std_logic;
  signal GATE1_NORMALIZE_TO_AMP_GATE_1: std_logic;
  signal GATE2_EN: std_logic;
  signal GATE2_START: std_logic_vector(GATE_RANGE-1 downto 0);
  signal GATE2_WIDTH: std_logic_vector(GATE_RANGE-1 downto 0);
  signal GATE2_AMP_THRESHOLD: std_logic_vector(GATE_RANGE-1 downto 0);
  signal GATE2_TOF_ALGORITHM: std_logic_vector(GATE_TOF_ALGORITHM_RANGE-1 downto 0);
  signal GATE2_TOF_AVG: std_logic_vector(GATE_TOF_AVG_RANGE-1 downto 0);
  signal GATE2_TOF_MIN_THICKNESS: std_logic_vector(GATE_TOF_RANGE-1 downto 0);
  signal GATE2_AMP_THRESHOLD_ALARM_EN: std_logic;
  signal GATE2_AMP_THRESHOLD_ALARM_CROSSING: std_logic_vector(GATE_ALARM_CROSSING-1 downto 0);
  signal GATE2_TOF_ALARM_EN: std_logic;
  signal GATE2_TOF_ALARM_TYPE: std_logic;
  signal GATE2_TOF_ALARM_CROSSING: std_logic_vector(GATE_ALARM_CROSSING-1 downto 0);
  signal GATE2_TOF_ALARM_MIN_VALUE: std_logic_vector(GATE_TOF_RANGE-1 downto 0);
  signal GATE2_TOF_ALARM_MAX_VALUE: std_logic_vector(GATE_TOF_RANGE-1 downto 0);
  signal GATE2_TRACK_GATE_3: std_logic;
  signal GATE2_NORMALIZE_TO_AMP_GATE_1: std_logic;
  signal GATE3_EN: std_logic;
  signal GATE3_START: std_logic_vector(GATE_RANGE-1 downto 0);
  signal GATE3_WIDTH: std_logic_vector(GATE_RANGE-1 downto 0);
  signal GATE3_AMP_THRESHOLD: std_logic_vector(GATE_RANGE-1 downto 0);
  signal GATE3_TOF_ALGORITHM: std_logic_vector(GATE_TOF_ALGORITHM_RANGE-1 downto 0);
  signal GATE3_TOF_AVG: std_logic_vector(GATE_TOF_AVG_RANGE-1 downto 0);
  signal GATE3_TOF_MIN_THICKNESS: std_logic_vector(GATE_TOF_RANGE-1 downto 0);
  signal GATE3_AMP_THRESHOLD_ALARM_EN: std_logic;
  signal GATE3_AMP_THRESHOLD_ALARM_CROSSING: std_logic_vector(GATE_ALARM_CROSSING-1 downto 0);
  signal GATE3_TOF_ALARM_EN: std_logic;
  signal GATE3_TOF_ALARM_TYPE: std_logic;
  signal GATE3_TOF_ALARM_CROSSING: std_logic_vector(GATE_ALARM_CROSSING-1 downto 0);
  signal GATE3_TOF_ALARM_MIN_VALUE: std_logic_vector(GATE_TOF_RANGE-1 downto 0);
  signal GATE3_TOF_ALARM_MAX_VALUE: std_logic_vector(GATE_TOF_RANGE-1 downto 0);
  signal GATE3_TRACK_GATE_3: std_logic;
  signal GATE3_NORMALIZE_TO_AMP_GATE_1: std_logic;
  signal DSP_AVERAGE: std_logic_vector(GATE_RANGE-1 downto 0);
  signal DSP_NOISE_REDUCTION_FILTER: std_logic_vector(GATE_RANGE-1 downto 0);
  signal DSP_AUTOMATIC_FREQUENCY_FILTER_EN: std_logic;
  signal DSP_BAND_PASS_FILTER_EN: std_logic;
  signal VELOCITY_PROBE_ZERO: std_logic_vector(GATE_RANGE-1 downto 0);
  signal VELOCITY_MEASURED_TEMPERATURE: std_logic_vector(GATE_RANGE-1 downto 0);
  signal VELOCITY_REFERENCE_TEMPERATURE: std_logic_vector(GATE_RANGE-1 downto 0);
  signal VELOCITY_TEMPERATURE_LOCK: std_logic_vector(GATE_RANGE-1 downto 0);
  signal VELOCITY_TEMPERATURE_CORRECTION: std_logic_vector(GATE_RANGE-1 downto 0);
  signal VELOCITY_REFERENCE_VELOCITY: std_logic_vector(GATE_RANGE-1 downto 0);
  signal VELOCITY_MEASURED_VELOCITY: std_logic_vector(GATE_RANGE-1 downto 0) ;

  constant aclk_period: time := 10 ns;
  
  signal begin_load: STD_LOGIC := '0'; 
  signal load_input_flag: STD_LOGIC := '0'; 

  type states_load is (idle, load_wait, load_input_1, load_input_2, wait_result); 
  signal state_load : states_load; 
  signal load_counter : unsigned(31 downto 0) := (others => '0');   
  signal CONFIG_LENGHT : unsigned(31 downto 0) := (others => '0');   
  
  type configuration_array is array ( 0 to 127 ) of std_logic_vector(REGISTER_WIDTH-1 downto 0);
  signal config_1 : configuration_array;
  signal config_2 : configuration_array;
  signal inspecting_counter : unsigned(31 downto 0) := (others => '0');  

begin

       -- aclk process 
   aclk_period_process :process
   begin
    aclk <= not aclk;
    wait for aclk_period/2;
    aclk <= not aclk;
    wait for aclk_period/2;
   end process;

  -- Insert values for generic parameters !!
  uut: configuration_register generic map ( REGISTER_WIDTH                     => REGISTER_WIDTH,
                                           GATE_RANGE                         => GATE_RANGE,
                                           GATE_ALARM_CROSSING                => GATE_ALARM_CROSSING,
                                           GATE_TOF_ALGORITHM_RANGE           => GATE_TOF_ALGORITHM_RANGE,
                                           GATE_TOF_AVG_RANGE                 => GATE_TOF_AVG_RANGE,
                                           GATE_TOF_RANGE                     => GATE_TOF_RANGE,
                                           GATE_AXIS_RANGE                    => GATE_AXIS_RANGE )
                                port map ( aclk                               => aclk,
                                           aresetn                            => aresetn,
                                           CONFIG_EN                          => CONFIG_EN,
                                           LOAD_CONFIG                        => LOAD_CONFIG,
                                           CONFIG_INDEX                       => CONFIG_INDEX,
                                           CONFIG_VALUE                       => CONFIG_VALUE,
                                           CHANNEL_EN                         => CHANNEL_EN,
                                           TRANSMITTER_VOLTAGE                => TRANSMITTER_VOLTAGE,
                                           TRANSMITTER_BURST_FREQUENCY        => TRANSMITTER_BURST_FREQUENCY,
                                           TRANSMITTER_N_CYCLES               => TRANSMITTER_N_CYCLES,
                                           TRANSMITTER_PRF                    => TRANSMITTER_PRF,
                                           TRANSMITTER_DELAY                  => TRANSMITTER_DELAY,
                                           TRANSMITTER_DIRECTIONAL_PHASING    => TRANSMITTER_DIRECTIONAL_PHASING,
                                           RECEIVER_SAMPLING_FREQUENCY        => RECEIVER_SAMPLING_FREQUENCY,
                                           RECEIVER_DATA_WINDOW               => RECEIVER_DATA_WINDOW,
                                           RECEIVER_DELAY                     => RECEIVER_DELAY,
                                           RECEIVER_GAIN                      => RECEIVER_GAIN,
                                           RECEIVER_EXTERNAL_MULTIPLEXER_EN   => RECEIVER_EXTERNAL_MULTIPLEXER_EN,
                                           MAGNET_MODE                        => MAGNET_MODE,
                                           MAGNET_PULSE_WIDTH                 => MAGNET_PULSE_WIDTH,
                                           MAGNET_INITIAL_DELAY               => MAGNET_INITIAL_DELAY,
                                           GATE1_EN                           => GATE1_EN,
                                           GATE1_START                        => GATE1_START,
                                           GATE1_WIDTH                        => GATE1_WIDTH,
                                           GATE1_AMP_THRESHOLD                => GATE1_AMP_THRESHOLD,
                                           GATE1_TOF_ALGORITHM                => GATE1_TOF_ALGORITHM,
                                           GATE1_TOF_AVG                      => GATE1_TOF_AVG,
                                           GATE1_TOF_MIN_THICKNESS            => GATE1_TOF_MIN_THICKNESS,
                                           GATE1_AMP_THRESHOLD_ALARM_EN       => GATE1_AMP_THRESHOLD_ALARM_EN,
                                           GATE1_AMP_THRESHOLD_ALARM_CROSSING => GATE1_AMP_THRESHOLD_ALARM_CROSSING,
                                           GATE1_TOF_ALARM_EN                 => GATE1_TOF_ALARM_EN,
                                           GATE1_TOF_ALARM_TYPE               => GATE1_TOF_ALARM_TYPE,
                                           GATE1_TOF_ALARM_CROSSING           => GATE1_TOF_ALARM_CROSSING,
                                           GATE1_TOF_ALARM_MIN_VALUE          => GATE1_TOF_ALARM_MIN_VALUE,
                                           GATE1_TOF_ALARM_MAX_VALUE          => GATE1_TOF_ALARM_MAX_VALUE,
                                           GATE1_TRACK_GATE_3                 => GATE1_TRACK_GATE_3,
                                           GATE1_NORMALIZE_TO_AMP_GATE_1      => GATE1_NORMALIZE_TO_AMP_GATE_1,
                                           GATE2_EN                           => GATE2_EN,
                                           GATE2_START                        => GATE2_START,
                                           GATE2_WIDTH                        => GATE2_WIDTH,
                                           GATE2_AMP_THRESHOLD                => GATE2_AMP_THRESHOLD,
                                           GATE2_TOF_ALGORITHM                => GATE2_TOF_ALGORITHM,
                                           GATE2_TOF_AVG                      => GATE2_TOF_AVG,
                                           GATE2_TOF_MIN_THICKNESS            => GATE2_TOF_MIN_THICKNESS,
                                           GATE2_AMP_THRESHOLD_ALARM_EN       => GATE2_AMP_THRESHOLD_ALARM_EN,
                                           GATE2_AMP_THRESHOLD_ALARM_CROSSING => GATE2_AMP_THRESHOLD_ALARM_CROSSING,
                                           GATE2_TOF_ALARM_EN                 => GATE2_TOF_ALARM_EN,
                                           GATE2_TOF_ALARM_TYPE               => GATE2_TOF_ALARM_TYPE,
                                           GATE2_TOF_ALARM_CROSSING           => GATE2_TOF_ALARM_CROSSING,
                                           GATE2_TOF_ALARM_MIN_VALUE          => GATE2_TOF_ALARM_MIN_VALUE,
                                           GATE2_TOF_ALARM_MAX_VALUE          => GATE2_TOF_ALARM_MAX_VALUE,
                                           GATE2_TRACK_GATE_3                 => GATE2_TRACK_GATE_3,
                                           GATE2_NORMALIZE_TO_AMP_GATE_1      => GATE2_NORMALIZE_TO_AMP_GATE_1,
                                           GATE3_EN                           => GATE3_EN,
                                           GATE3_START                        => GATE3_START,
                                           GATE3_WIDTH                        => GATE3_WIDTH,
                                           GATE3_AMP_THRESHOLD                => GATE3_AMP_THRESHOLD,
                                           GATE3_TOF_ALGORITHM                => GATE3_TOF_ALGORITHM,
                                           GATE3_TOF_AVG                      => GATE3_TOF_AVG,
                                           GATE3_TOF_MIN_THICKNESS            => GATE3_TOF_MIN_THICKNESS,
                                           GATE3_AMP_THRESHOLD_ALARM_EN       => GATE3_AMP_THRESHOLD_ALARM_EN,
                                           GATE3_AMP_THRESHOLD_ALARM_CROSSING => GATE3_AMP_THRESHOLD_ALARM_CROSSING,
                                           GATE3_TOF_ALARM_EN                 => GATE3_TOF_ALARM_EN,
                                           GATE3_TOF_ALARM_TYPE               => GATE3_TOF_ALARM_TYPE,
                                           GATE3_TOF_ALARM_CROSSING           => GATE3_TOF_ALARM_CROSSING,
                                           GATE3_TOF_ALARM_MIN_VALUE          => GATE3_TOF_ALARM_MIN_VALUE,
                                           GATE3_TOF_ALARM_MAX_VALUE          => GATE3_TOF_ALARM_MAX_VALUE,
                                           GATE3_TRACK_GATE_3                 => GATE3_TRACK_GATE_3,
                                           GATE3_NORMALIZE_TO_AMP_GATE_1      => GATE3_NORMALIZE_TO_AMP_GATE_1,
                                           DSP_AVERAGE                        => DSP_AVERAGE,
                                           DSP_NOISE_REDUCTION_FILTER         => DSP_NOISE_REDUCTION_FILTER,
                                           DSP_AUTOMATIC_FREQUENCY_FILTER_EN  => DSP_AUTOMATIC_FREQUENCY_FILTER_EN,
                                           DSP_BAND_PASS_FILTER_EN            => DSP_BAND_PASS_FILTER_EN,
                                           VELOCITY_PROBE_ZERO                => VELOCITY_PROBE_ZERO,
                                           VELOCITY_MEASURED_TEMPERATURE      => VELOCITY_MEASURED_TEMPERATURE,
                                           VELOCITY_REFERENCE_TEMPERATURE     => VELOCITY_REFERENCE_TEMPERATURE,
                                           VELOCITY_TEMPERATURE_LOCK          => VELOCITY_TEMPERATURE_LOCK,
                                           VELOCITY_TEMPERATURE_CORRECTION    => VELOCITY_TEMPERATURE_CORRECTION,
                                           VELOCITY_REFERENCE_VELOCITY        => VELOCITY_REFERENCE_VELOCITY,
                                           VELOCITY_MEASURED_VELOCITY         => VELOCITY_MEASURED_VELOCITY );

  stimulus: process
  begin
    aresetn <= '0';
    wait for aclk_period*20;  
    aresetn <= '1';
    wait for aclk_period*20;  
    begin_load <= '1';
    wait for aclk_period*20;  
    begin_load <= '0';
    wait;
  end process;
  
  update_config_process: process(aclk, aresetn)
  begin 
      if (aresetn = '0') then
        CONFIG_EN <= '0';
        inspecting_counter <= (others => '0');
      elsif (aclk'event and aclk = '1') then
        if(inspecting_counter >= 29) then
            CONFIG_EN <= not CONFIG_EN;
        else
            inspecting_counter <= inspecting_counter + 1;
        end if;
      end if;
  end process;
      
  load_inst: process(aclk, aresetn)      
    type dataFile is file of integer;
    file data_file: dataFile is in "configuration1.dat";
    variable data_in_x: integer;                                               
  begin 
    if (aresetn = '0') then
        load_input_flag <= '0';
        LOAD_CONFIG <= '0';
        CONFIG_INDEX <= (others => '0');
        CONFIG_VALUE <= (others => '0');
        state_load <= idle;  
        load_counter <= (others => '0');
    elsif (aclk'event and aclk = '1') then
        case state_load is  
           when idle =>   
                CONFIG_LENGHT <= "00000000000000000000000001001111";
                if(begin_load = '1') then
                    state_load <= load_input_1;  
                end if;
           when load_wait =>
                LOAD_CONFIG <= '0';
                if(load_counter >= 128) then
                    load_counter <= (others => '0'); 
                    if(load_input_flag = '0') then
                        load_input_flag <= '1';
                    else
                        load_input_flag <= '0';
                    end if;
                    state_load <= load_input_2; 
                else
                    load_counter <= load_counter + 1;
                end if;
            when load_input_1 =>
                load_counter <= load_counter + 1;
                if not endfile(data_file) then
                    read (data_file, data_in_x);
                    config_1(to_integer(load_counter))<=std_logic_vector(to_signed(data_in_x, CONFIG_VALUE'length));
                    config_2(to_integer(CONFIG_LENGHT)-to_integer(load_counter)-1)<=std_logic_vector(to_signed(data_in_x, CONFIG_VALUE'length));
                end if;
                if not endfile(data_file) then
                    read (data_file, data_in_x);
                end if;
                if(load_counter >= (CONFIG_LENGHT-1)) then
                    state_load <= load_input_2; 
                    load_counter <= (others => '0');
                end if; 
            when load_input_2 =>
                load_counter <= load_counter + 1;
                CONFIG_INDEX <= std_logic_vector(load_counter);
                LOAD_CONFIG <= '1';
                if(load_input_flag = '0') then
                    CONFIG_VALUE<=config_1(to_integer(load_counter));
                else
                    CONFIG_VALUE<=config_2(to_integer(load_counter));
                end if;
                if(load_counter >= (CONFIG_LENGHT-1)) then
                    state_load <= load_wait; 
                    load_counter <= (others => '0');
                end if; 
            when others =>
                null;                   
        end case;                                                                                            
    end if;
  end process;
  
end;
