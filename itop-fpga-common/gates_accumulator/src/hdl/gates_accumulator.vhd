----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 09/26/2018 10:07:23 AM
-- Design Name: 
-- Module Name: decimator filter- Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity gates_accumulator is
	generic (
	    N_CHANNELS		  : INTEGER := 4;
        REGISTER_W32      : INTEGER := 32;
        REGISTER_W16      : INTEGER := 16;
        REGISTER_W14      : INTEGER := 14;
        REGISTER_W8       : INTEGER := 8;
        BRAM_ADDR_WIDTH   : INTEGER := 11;
        BRAM_DATA_WIDTH   : INTEGER := 32;
		WAIT_CYCLES       : INTEGER := 32;
		ASCAN_HEADER_SIZE : INTEGER := 96;
		COMPRESSION_NO    : INTEGER := 0;
        COMPRESSION_ASCAN : INTEGER := 1;
        COMPRESSION_GATES : INTEGER := 2;
        COMPRESSION_ASCAN_GATES : INTEGER := 3;
        COMPRESSION_BURST : INTEGER := 4;
        COMPRESSION_ASCAN_DEC : INTEGER := 5
	);
	port (
	    -- Sync
	    aclk         : in STD_LOGIC;
        aresetn      : in STD_LOGIC;
	
	    -- Control
        GA_RESET           : in STD_LOGIC;	    
        NO_READ_GA_RESULT  : in STD_LOGIC;
        READ_GA_RESULT     : in STD_LOGIC;
        DC_MODE            : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        
        GA_WINDOW_RESULT   : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GA_N_GATES         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	         	
		-- GATES input
		GATES_INPUT_AXIS_TREADY  : out STD_LOGIC;
		GATES_INPUT_AXIS_TDATA	 : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		GATES_INPUT_AXIS_TLAST	 : in STD_LOGIC;
		GATES_INPUT_AXIS_TVALID  : in STD_LOGIC;
		
		-- Decimation result
		GA_RESULT_AXIS_TVALID    : out STD_LOGIC;
		GA_RESULT_AXIS_TDATA     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		GA_RESULT_AXIS_TLAST     : out STD_LOGIC;
		GA_RESULT_AXIS_TREADY    : in STD_LOGIC
	);
end gates_accumulator;

architecture arch_imp of gates_accumulator is

    -- BRAM
    COMPONENT gates_accumulator_BRAM
      PORT (
        clka : IN STD_LOGIC;
        ena : IN STD_LOGIC;
        wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addra : IN STD_LOGIC_VECTOR(BRAM_ADDR_WIDTH-1 DOWNTO 0);
        dina : IN STD_LOGIC_VECTOR(BRAM_DATA_WIDTH-1 DOWNTO 0);
        douta : OUT STD_LOGIC_VECTOR(BRAM_DATA_WIDTH-1 DOWNTO 0);
        clkb : IN STD_LOGIC;
        enb : IN STD_LOGIC;
        web : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addrb : IN STD_LOGIC_VECTOR(BRAM_ADDR_WIDTH-1 DOWNTO 0);
        dinb : IN STD_LOGIC_VECTOR(BRAM_DATA_WIDTH-1 DOWNTO 0);
        doutb : OUT STD_LOGIC_VECTOR(BRAM_DATA_WIDTH-1 DOWNTO 0)
      );
    END COMPONENT;
    
    -- gates_accumulator BRAM port a
    signal gates_accumulator_bram_addra   : STD_LOGIC_VECTOR(BRAM_ADDR_WIDTH-1 downto 0);
    signal gates_accumulator_bram_dina    : STD_LOGIC_VECTOR(BRAM_DATA_WIDTH-1 downto 0);
    signal gates_accumulator_bram_douta   : STD_LOGIC_VECTOR(BRAM_DATA_WIDTH-1 downto 0);
    signal gates_accumulator_bram_ena     : STD_LOGIC;
    signal gates_accumulator_bram_wea     : STD_LOGIC_VECTOR(0 downto 0);

    signal gates_accumulator_bram_addra_first : STD_LOGIC;
    
    -- gates_accumulator BRAM port b
    signal gates_accumulator_bram_addrb   : STD_LOGIC_VECTOR(BRAM_ADDR_WIDTH-1 downto 0);
    signal gates_accumulator_bram_dinb    : STD_LOGIC_VECTOR(BRAM_DATA_WIDTH-1 downto 0);
    signal gates_accumulator_bram_doutb   : STD_LOGIC_VECTOR(BRAM_DATA_WIDTH-1 downto 0);
    signal gates_accumulator_bram_enb     : STD_LOGIC;
    signal gates_accumulator_bram_web     : STD_LOGIC_VECTOR(0 downto 0);

    signal gates_accumulator_bram_addrb_first : STD_LOGIC;
    
    signal gates_counter  : UNSIGNED(REGISTER_W16-1 downto 0);
    
    type states_general_sm is (idle, store_gates, store_ascan, non_store_ascan, provide_ga_result, provide_ga_result_finish); 
    signal state_general_sm : states_general_sm;
    
    signal ga_result_axis_tvalid_i : STD_LOGIC_VECTOR(1 downto 0);
    signal ga_result_axis_tlast_i  : STD_LOGIC_VECTOR(1 downto 0);
    
    signal ga_window_result_counter   : UNSIGNED(REGISTER_W16-1 downto 0);
      
begin

    GA_N_GATES <= std_logic_vector(gates_counter);     

    -- GENERAL STATE MACHINE
    general_sm: process(aclk)
        variable var_counter_wait      : integer range 0 to 8192 :=0;
        variable var_counter_result      : integer range 0 to 65535 :=0;
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0' or GA_RESET = '1') then
                gates_accumulator_bram_ena   <= '1';
                gates_accumulator_bram_wea   <= (others => '0');
                gates_accumulator_bram_addra <= (others => '0');
                gates_accumulator_bram_dina  <= (others => '0');
                gates_accumulator_bram_enb   <= '1';
                gates_accumulator_bram_web   <= (others => '0');
                gates_accumulator_bram_addrb <= (others => '0');
                gates_accumulator_bram_dinb  <= (others => '0');
                GATES_INPUT_AXIS_TREADY <= '0';
                gates_accumulator_bram_addra_first <= '1';
                gates_accumulator_bram_addrb_first <= '1';
                ga_result_axis_tvalid_i     <= (others => '0');
                ga_result_axis_tlast_i      <= (others => '0');
                GA_RESULT_AXIS_TVALID       <= '0';
                GA_RESULT_AXIS_TLAST        <= '0';
                GA_WINDOW_RESULT            <= (others => '0');
                gates_counter               <= (others => '0');
                ga_window_result_counter    <= (others => '0');
                -- State
                state_general_sm <= idle;
            else     
                ga_result_axis_tvalid_i(1) <= ga_result_axis_tvalid_i(0);   
                GA_RESULT_AXIS_TVALID      <= ga_result_axis_tvalid_i(1);    
                ga_result_axis_tlast_i(1)  <= ga_result_axis_tlast_i(0);    
                GA_RESULT_AXIS_TLAST       <= ga_result_axis_tlast_i(1); 
                case state_general_sm is        
                    when idle =>      
                        var_counter_wait := 0;
                        var_counter_result := 0;  
                        gates_accumulator_bram_addrb <= (others => '0');       
                        GA_WINDOW_RESULT <= std_logic_vector(ga_window_result_counter);   
                        GATES_INPUT_AXIS_TREADY <= '1';                                                         
                        if(GATES_INPUT_AXIS_TVALID = '1') then
                            gates_accumulator_bram_wea <= (others => '1');
                            gates_accumulator_bram_dina  <= GATES_INPUT_AXIS_TDATA; 
                            if(unsigned(gates_accumulator_bram_addra) > 0) then
                                gates_accumulator_bram_addra <= std_logic_vector(unsigned(gates_accumulator_bram_addra)+1);
                            end if;
                            GATES_INPUT_AXIS_TREADY <= '1';
                            ga_window_result_counter <= ga_window_result_counter + 1;
                            state_general_sm <= store_gates;
                        elsif(READ_GA_RESULT = '1') then 
                            GATES_INPUT_AXIS_TREADY <= '0';
                            gates_accumulator_bram_addrb_first <= '1';
                            state_general_sm <= provide_ga_result; 
                        elsif(NO_READ_GA_RESULT = '1') then 
                            GATES_INPUT_AXIS_TREADY <= '0';
                        else
                            gates_accumulator_bram_wea <= (others => '0');
                            GATES_INPUT_AXIS_TREADY <= '1';
                        end if;
                    when store_gates => 
                        if(GATES_INPUT_AXIS_TVALID = '1') then
                            gates_accumulator_bram_dina  <= GATES_INPUT_AXIS_TDATA; 
                            gates_accumulator_bram_addra <= std_logic_vector(unsigned(gates_accumulator_bram_addra)+1);
                            if(GATES_INPUT_AXIS_TLAST = '1') then
                                gates_counter <= gates_counter + 1;
                                state_general_sm <= idle; 
                            end if;
                            ga_window_result_counter <= ga_window_result_counter + 1;
                        end if;
                    when provide_ga_result =>
                        if(READ_GA_RESULT = '1') then
                            if(GA_RESULT_AXIS_TREADY = '1') then
                                if(unsigned(gates_accumulator_bram_addrb) >= (unsigned(ga_window_result_counter)-1)) then
                                    ga_result_axis_tvalid_i(0) <= '0';    
                                    ga_result_axis_tlast_i(0)  <= '0';  
                                    state_general_sm <= provide_ga_result_finish;
                                else
                                    ga_result_axis_tvalid_i(0) <= '1';
                                    if(gates_accumulator_bram_addra_first = '1') then
                                        gates_accumulator_bram_addra_first <= '0';
                                        gates_accumulator_bram_addrb <= (others => '0');
                                    else
                                        gates_accumulator_bram_addrb <= std_logic_vector(unsigned(gates_accumulator_bram_addrb)+1); 
                                    end if;
                                    if(unsigned(gates_accumulator_bram_addrb) >= unsigned(ga_window_result_counter)-2) then
                                        ga_result_axis_tlast_i(0) <= '1';
                                    end if;
                                end if;
                            else
                                ga_result_axis_tvalid_i(0)  <= '0';    
                                ga_result_axis_tlast_i(0)  <= '0';                                                      
                            end if;  
                        else
                            ga_result_axis_tvalid_i(0)  <= '0';
                            ga_result_axis_tlast_i(0)   <= '0';
                            state_general_sm <= provide_ga_result_finish;
                        end if; 
                    when provide_ga_result_finish =>
                        ga_result_axis_tvalid_i(0)  <= '0';
                        ga_result_axis_tlast_i(0)   <= '0';  
                        gates_accumulator_bram_addra <= (others => '0');
                        if(READ_GA_RESULT = '0') then 
                            gates_counter    <= (others => '0');
                            state_general_sm <= idle; 
                        end if;                              
                    when others => 
                        null;
                end case;                                 
            end if;
        end if;
    end process;
    
    GA_RESULT_AXIS_TDATA <= gates_accumulator_bram_doutb;
   
    gates_accumulator_BRAM_inst : gates_accumulator_BRAM
        port map (
            clka => aclk,
            ena => gates_accumulator_bram_ena,
            wea => gates_accumulator_bram_wea,
            addra => gates_accumulator_bram_addra,
            dina => gates_accumulator_bram_dina,
            douta => gates_accumulator_bram_douta,
            clkb => aclk,
            enb => gates_accumulator_bram_enb,
            web => gates_accumulator_bram_web,
            addrb => gates_accumulator_bram_addrb,
            dinb => gates_accumulator_bram_dinb,
            doutb => gates_accumulator_bram_doutb
        );
	      																					  																			  																		      	   
end arch_imp;
