----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/14/2017 11:51:43 AM
-- Design Name: 
-- Module Name: VOLTA_main - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity main is
	generic (
		-- General
		N_CHANNELS		: INTEGER := 2;
		REGISTER_W64    : INTEGER := 64;
		REGISTER_W48    : INTEGER := 48;
	    REGISTER_W32    : INTEGER := 32;
	    REGISTER_W21    : INTEGER := 21;
        REGISTER_W16    : INTEGER := 16;
		REGISTER_W14    : INTEGER := 14;
		REGISTER_W8     : INTEGER := 8;
		REGISTER_W6     : INTEGER := 6;
		REGISTER_W4     : INTEGER := 4;
		REGISTER_W2     : INTEGER := 2;
		
		-- external trigger
		DISPOSITION_PASS  : INTEGER := 1;
		DISPOSITION_FAIL  : INTEGER := 2;
		DISPOSITION_NODET : INTEGER := 0;
		
		-- Coinc
        TRIGGER_WAIT    : INTEGER := 32;
        MIN_COINC_LEVEL : INTEGER := 2;
		
		-- Avg
		MIN_AVG_LEVEL   : INTEGER := 2;
		
		-- Ascan header size
		ASCAN_HEADER_SIZE : INTEGER := 43;
		
		-- FIR CORR
        N_TAPS              : INTEGER    := 512;
        N_SETS_TAP          : INTEGER    := 3;
        OVERSAMPLING        : INTEGER    := 2;
        WAIT_CYCLES         : INTEGER    := 4
	);
port (
    -- sync
    aclk                    : in STD_LOGIC; 
    aresetn                 : in STD_LOGIC; 
    
    -- DEBUG
    CONFIGURATION_SIZE      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CONFIGURATION_ON        : out STD_LOGIC;
    
    CONFIGURATION_HEADER_SIZE      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CONFIGURATION_CHANNELS         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CONFIGURATION_CHANNEL0_OFFSET  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CONFIGURATION_CHANNEL1_OFFSET  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CONFIGURATION_ENCODER_OFFSET   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CONFIGURATION_DIGITALIO_OFFSET : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CONFIGURATION_CHANNEL0_SIZE    : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CONFIGURATION_CHANNEL1_SIZE    : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CONFIGURATION_ENCODER_ENABLED  : out STD_LOGIC;
    CONFIGURATION_CHANNEL0_ENABLED : out STD_LOGIC;
    CONFIGURATION_CHANNEL1_ENABLED : out STD_LOGIC;
    CONFIGURATION_CHANNEL0_ON      : out STD_LOGIC;
    CONFIGURATION_CHANNEL1_ON      : out STD_LOGIC;
    
    -- CONTROL REGISTERS
    DMA_UPLOAD_CONTROL      : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);   
    DMA_DOWNLOAD_CONTROL    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    START_INSPECTION_CONTROL: in STD_LOGIC; 
    SCAN_MODE_CONTROL       : in STD_LOGIC; 
    CSCAN_MODE_CONTROL      : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);  
    TRIGGER_TYPE_CONTROL    : in STD_LOGIC_VECTOR(REGISTER_W6-1 downto 0);
    RESET_CONTROL           : in STD_LOGIC;
    ENABLE_CONTROL          : in STD_LOGIC;
    -- STATUS REGISTERS
    DMA_UPLOAD_STATUS       : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);   
    DMA_DOWNLOAD_STATUS     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    START_INSPECTION_STATUS : out STD_LOGIC; 
    SCAN_MODE_STATUS        : out STD_LOGIC; 
    CSCAN_MODE_STATUS       : out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);  
    TRIGGER_TYPE_STATUS     : out STD_LOGIC_VECTOR(REGISTER_W6-1 downto 0);
    RESET_STATUS            : out STD_LOGIC;
    ENABLE_STATUS           : out STD_LOGIC;
    
    -- ENCODER
    ENCODER1_DIR     : in STD_LOGIC_VECTOR (REGISTER_W2-1 downto 0);  
    ENCODER1_COUNT   : in STD_LOGIC_VECTOR (REGISTER_W32-1 downto 0);  
    
    ENCODER2_DIR     : in STD_LOGIC_VECTOR (REGISTER_W2-1 downto 0);  
    ENCODER2_COUNT   : in STD_LOGIC_VECTOR (REGISTER_W32-1 downto 0);  
	
	-- DIO
	IO_INPUTS   			: in  STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	IO_OUTPUTS  			: out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	
	---------------------------------------------- CH0 ----------------------------------------------
	
	-- Config output receiver
	CH0_RECEIVER_SAMPLING_FREQUENCY         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
    CH0_RECEIVER_DATA_WINDOW                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
    CH0_RECEIVER_DELAY                      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
    CH0_RECEIVER_GAIN                       : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
    CH0_RECEIVER_EXTERNAL_MULTIPLEXER_EN    : out STD_LOGIC;
			
	-- Config output transmitter
	CH0_TRANSMITTER_VOLTAGE                 : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
    CH0_TRANSMITTER_BURST_FREQUENCY         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
    CH0_TRANSMITTER_N_CYCLES                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
    CH0_TRANSMITTER_DELAY                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CH0_TRANSMITTER_DELTA_DELAY             : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CH0_TRANSMITTER_DELTA_ADD               : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CH0_TRANSMITTER_DIRECTIONAL_PHASING     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
		
	-- Config output magnet
	CH0_MAGNET_MODE                         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    CH0_MAGNET_PULSE_WIDTH                  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    CH0_MAGNET_INITIAL_DELAY                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
	-- Config analog filters
	CH0_DSP_ANALOG_FILTER  					: out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
		
	-- Config DAC curves
	CH0_CONFIG_DAC_AXIS_TREADY    : in STD_LOGIC;
	CH0_CONFIG_DAC_AXIS_TDATA	  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	CH0_CONFIG_DAC_AXIS_TLAST	  : out STD_LOGIC;
	CH0_CONFIG_DAC_AXIS_TVALID    : out STD_LOGIC;
			
	-- Signal input
	CH0_SIGNAL_COINC_WINDOW  	  : out STD_LOGIC;
	CH0_SIGNAL_AVG_WINDOW  		  : out STD_LOGIC;
		
	CH0_SIGNAL_INPUT_AXIS_TREADY  : out STD_LOGIC;
	CH0_SIGNAL_INPUT_AXIS_TDATA	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	CH0_SIGNAL_INPUT_AXIS_TLAST	  : in STD_LOGIC;
	CH0_SIGNAL_INPUT_AXIS_TVALID  : in STD_LOGIC;
	
	---------------------------------------------- CH1 ----------------------------------------------
	
		-- Config output receiver
	CH1_RECEIVER_SAMPLING_FREQUENCY         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
    CH1_RECEIVER_DATA_WINDOW                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
    CH1_RECEIVER_DELAY                      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
    CH1_RECEIVER_GAIN                       : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
    CH1_RECEIVER_EXTERNAL_MULTIPLEXER_EN    : out STD_LOGIC;
			
	-- Config output transmitter
	CH1_TRANSMITTER_VOLTAGE                 : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
    CH1_TRANSMITTER_BURST_FREQUENCY         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
    CH1_TRANSMITTER_N_CYCLES                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
    CH1_TRANSMITTER_DELAY                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CH1_TRANSMITTER_DELTA_DELAY             : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CH1_TRANSMITTER_DELTA_ADD               : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    CH1_TRANSMITTER_DIRECTIONAL_PHASING     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
		
	-- Config output magnet
	CH1_MAGNET_MODE                         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    CH1_MAGNET_PULSE_WIDTH                  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    CH1_MAGNET_INITIAL_DELAY                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
	-- Config analog filters
	CH1_DSP_ANALOG_FILTER  					: out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
		
	-- Config DAC curves
	CH1_CONFIG_DAC_AXIS_TREADY    : in STD_LOGIC;
	CH1_CONFIG_DAC_AXIS_TDATA	  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	CH1_CONFIG_DAC_AXIS_TLAST	  : out STD_LOGIC;
	CH1_CONFIG_DAC_AXIS_TVALID    : out STD_LOGIC;
			
	-- Signal input
	CH1_SIGNAL_COINC_WINDOW  	  : out STD_LOGIC;
	CH1_SIGNAL_AVG_WINDOW  		  : out STD_LOGIC;
		
	CH1_SIGNAL_INPUT_AXIS_TREADY  : out STD_LOGIC;
	CH1_SIGNAL_INPUT_AXIS_TDATA	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	CH1_SIGNAL_INPUT_AXIS_TLAST	  : in STD_LOGIC;
	CH1_SIGNAL_INPUT_AXIS_TVALID  : in STD_LOGIC;
	
    -- SYSTEM SIGNALS
    SW_RESET                : out STD_LOGIC;
    -- DMA INPUT
    DMA_S_axis_tready       : out STD_LOGIC;
    DMA_S_axis_tkeep        : in STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0);
    DMA_S_axis_tdata        : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    DMA_S_axis_tlast        : in STD_LOGIC;
    DMA_S_axis_tvalid       : in STD_LOGIC;
    -- DMA OUTPUT
    DMA_M_axis_tvalid       : out STD_LOGIC;
    DMA_M_axis_tdata        : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    DMA_M_axis_tlast        : out STD_LOGIC;
    DMA_M_axis_tkeep        : out STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0);
    DMA_M_axis_tready       : in STD_LOGIC
);
end main;

architecture Behavioral of main is
	
	---------------------------------------------- CHANNEL ----------------------------------------------
	COMPONENT channel IS
	generic (
		-- General
		CHANNEL_ID		: INTEGER := 0;
		REGISTER_W64    : INTEGER := 64;
		REGISTER_W48    : INTEGER := 48;
	    REGISTER_W32    : INTEGER := 32;
	    REGISTER_W21    : INTEGER := 21;
        REGISTER_W16    : INTEGER := 16;
		REGISTER_W14    : INTEGER := 14;
		REGISTER_W8     : INTEGER := 8;
		REGISTER_W4     : INTEGER := 4;
		REGISTER_W2     : INTEGER := 2;
		
		-- external trigger
		DISPOSITION_PASS  : INTEGER := 1;
		DISPOSITION_FAIL  : INTEGER := 2;
		DISPOSITION_NODET : INTEGER := 0;
		
		-- Coinc
        TRIGGER_WAIT    : INTEGER := 32;
        MIN_COINC_LEVEL : INTEGER := 2;
		
		-- Avg
		MIN_AVG_LEVEL   : INTEGER := 2;
		
		-- Ascan header size
		ASCAN_HEADER_SIZE : INTEGER := 43;
		
		-- FIR CORR
        N_TAPS              : INTEGER    := 512;
        N_SETS_TAP          : INTEGER    := 3;
        OVERSAMPLING        : INTEGER    := 2;
        WAIT_CYCLES         : INTEGER    := 4
	);
	port (
	    -- Sync
	    aclk         : in STD_LOGIC;
        aresetn      : in STD_LOGIC;
		
		-- Control
		START_INSPECTION  : in STD_LOGIC;
		TRIGGER_TYPE  	  : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
		READ_ASCAN  	  : in STD_LOGIC;
		ASCAN_READY  	  : out STD_LOGIC;
		ASCAN_SIZE	      : out STD_LOGIC_VECTOR(REGISTER_W21-1 downto 0);
		CHANNEL_ENABLED   : out STD_LOGIC;
		CONFIG_ON         : out STD_LOGIC;
		
		START_STOP_INSPECTION  : in STD_LOGIC;
		INSPECTION_INPROGESS   : out STD_LOGIC; 
		DISPOSITION		  : out STD_LOGIC; 
		DISPOSITION_BITS  : out STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
		
		-- Signal input
		SIGNAL_COINC_WINDOW  	  : out STD_LOGIC;
		SIGNAL_AVG_WINDOW  		  : out STD_LOGIC;
		
		SIGNAL_INPUT_AXIS_TREADY  : out STD_LOGIC;
		SIGNAL_INPUT_AXIS_TDATA	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		SIGNAL_INPUT_AXIS_TLAST	  : in STD_LOGIC;
		SIGNAL_INPUT_AXIS_TVALID  : in STD_LOGIC;
		
		-- Config
		CONFIG_INPUT_LOAD	  	  : in STD_LOGIC;
		CONFIG_INPUT_AXIS_TREADY  : out STD_LOGIC;
		CONFIG_INPUT_AXIS_TDATA	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		CONFIG_INPUT_AXIS_TLAST	  : in STD_LOGIC;
		CONFIG_INPUT_AXIS_TVALID  : in STD_LOGIC;
		
		-- ENCODERS
		ENCODER1_POS  	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		ENCODER1_DIR  	  : in STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
		ENCODER1_RPM  	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		ENCODER1_TRIGGER_COUNTS : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		ENCODER2_POS  	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		ENCODER2_DIR  	  : in STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
		ENCODER2_RPM  	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		ENCODER2_TRIGGER_COUNTS : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		ENCODER3_POS  	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		ENCODER3_DIR  	  : in STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
		ENCODER3_RPM  	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		ENCODER3_TRIGGER_COUNTS : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		-- IO
		IO_INPUTS  	  	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		IO_OUTPUTS  	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		
		-- Config output receiver
		RECEIVER_SAMPLING_FREQUENCY         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        RECEIVER_DATA_WINDOW                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        RECEIVER_DELAY                      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        RECEIVER_GAIN                       : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        RECEIVER_EXTERNAL_MULTIPLEXER_EN    : out STD_LOGIC;
		
		-- Config output transmitter
		TRANSMITTER_VOLTAGE                 : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        TRANSMITTER_BURST_FREQUENCY         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        TRANSMITTER_N_CYCLES                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        TRANSMITTER_DELAY                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_DELTA_DELAY             : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_DELTA_ADD               : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        TRANSMITTER_DIRECTIONAL_PHASING     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
		
		-- Config output magnet
		MAGNET_MODE                         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        MAGNET_PULSE_WIDTH                  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        MAGNET_INITIAL_DELAY                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		-- Condig analog filters
		DSP_ANALOG_FILTER  					: out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
		
		-- Config DAC curves
		CONFIG_DAC_AXIS_TREADY    : in STD_LOGIC;
		CONFIG_DAC_AXIS_TDATA	  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		CONFIG_DAC_AXIS_TLAST	  : out STD_LOGIC;
		CONFIG_DAC_AXIS_TVALID    : out STD_LOGIC;
					
		-- ASCAN
		ASCAN_AXIS_TREADY  : in STD_LOGIC;
		ASCAN_AXIS_TDATA   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		ASCAN_AXIS_TLAST   : out STD_LOGIC;
		ASCAN_AXIS_TVALID  : out STD_LOGIC;
		ASCAN_AXIS_TKEEP   : out std_logic_vector((REGISTER_W32/8)-1 downto 0)
	);
	END COMPONENT channel;

	---------------------------------------------- CH0 ----------------------------------------------	
	
	-- Control
	signal CH0_START_INSPECTION  	: STD_LOGIC;
	signal CH0_TRIGGER_TYPE  	  	: STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
	signal CH0_READ_ASCAN  	  		: STD_LOGIC;
	signal CH0_ASCAN_READY  	  	: STD_LOGIC;
	signal CH0_ASCAN_SIZE	      	: STD_LOGIC_VECTOR(REGISTER_W21-1 downto 0);

	-- External trigger - robot
	signal CH0_START_STOP_INSPECTION  	: STD_LOGIC;
	signal CH0_INSPECTION_INPROGESS   	: STD_LOGIC; 
	signal CH0_DISPOSITION		  		: STD_LOGIC; 
	signal CH0_DISPOSITION_BITS  		: STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
				
	-- Config
	signal CH0_CONFIG_INPUT_LOAD	  	  	: STD_LOGIC;
	signal CH0_CONFIG_INPUT_AXIS_TREADY  	: STD_LOGIC;
	signal CH0_CONFIG_INPUT_AXIS_TDATA	  	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH0_CONFIG_INPUT_AXIS_TLAST	  	: STD_LOGIC;
	signal CH0_CONFIG_INPUT_AXIS_TVALID  	: STD_LOGIC;
		
	-- ENCODERS
	signal CH0_ENCODER1_POS  	  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH0_ENCODER1_DIR  	  : STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
	signal CH0_ENCODER1_RPM  	  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH0_ENCODER1_TRIGGER_COUNTS : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
	signal CH0_ENCODER2_POS  	  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH0_ENCODER2_DIR  	  : STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
	signal CH0_ENCODER2_RPM  	  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH0_ENCODER2_TRIGGER_COUNTS : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
	signal CH0_ENCODER3_POS  	  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH0_ENCODER3_DIR  	  : STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
	signal CH0_ENCODER3_RPM  	  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH0_ENCODER3_TRIGGER_COUNTS : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
	-- IO
	signal CH0_IO_INPUTS  	  	: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal CH0_IO_OUTPUTS  	  	: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
						
	-- ASCAN
	signal CH0_ASCAN_AXIS_TREADY  : STD_LOGIC;
	signal CH0_ASCAN_AXIS_TDATA   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH0_ASCAN_AXIS_TLAST   : STD_LOGIC;
	signal CH0_ASCAN_AXIS_TVALID  : STD_LOGIC;
	signal CH0_ASCAN_AXIS_TKEEP   : STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0);

	---------------------------------------------- CH1 ----------------------------------------------

	-- Control
	signal CH1_START_INSPECTION  	: STD_LOGIC;
	signal CH1_TRIGGER_TYPE  	  	: STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
	signal CH1_READ_ASCAN  	  		: STD_LOGIC;
	signal CH1_ASCAN_READY  	  	: STD_LOGIC;
	signal CH1_ASCAN_SIZE	      	: STD_LOGIC_VECTOR(REGISTER_W21-1 downto 0);

	-- External trigger - robot
	signal CH1_START_STOP_INSPECTION  	: STD_LOGIC;
	signal CH1_INSPECTION_INPROGESS   	: STD_LOGIC; 
	signal CH1_DISPOSITION		  		: STD_LOGIC; 
	signal CH1_DISPOSITION_BITS  		: STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
				
	-- Config
	signal CH1_CONFIG_INPUT_LOAD	  	  	: STD_LOGIC;
	signal CH1_CONFIG_INPUT_AXIS_TREADY  	: STD_LOGIC;
	signal CH1_CONFIG_INPUT_AXIS_TDATA	  	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH1_CONFIG_INPUT_AXIS_TLAST	  	: STD_LOGIC;
	signal CH1_CONFIG_INPUT_AXIS_TVALID  	: STD_LOGIC;
			
	-- ENCODERS
	signal CH1_ENCODER1_POS  	  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH1_ENCODER1_DIR  	  : STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
	signal CH1_ENCODER1_RPM  	  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH1_ENCODER1_TRIGGER_COUNTS : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
	signal CH1_ENCODER2_POS  	  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH1_ENCODER2_DIR  	  : STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
	signal CH1_ENCODER2_RPM  	  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH1_ENCODER2_TRIGGER_COUNTS : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
	signal CH1_ENCODER3_POS  	  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH1_ENCODER3_DIR  	  : STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
	signal CH1_ENCODER3_RPM  	  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH1_ENCODER3_TRIGGER_COUNTS : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
	-- IO
	signal CH1_IO_INPUTS  	  	: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal CH1_IO_OUTPUTS  	  	: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
						
	-- ASCAN
	signal CH1_ASCAN_AXIS_TREADY  : STD_LOGIC;
	signal CH1_ASCAN_AXIS_TDATA   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal CH1_ASCAN_AXIS_TLAST   : STD_LOGIC;
	signal CH1_ASCAN_AXIS_TVALID  : STD_LOGIC;
	signal CH1_ASCAN_AXIS_TKEEP   : STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0);
	
	---------------------------------------------- MAIN ----------------------------------------------
 
    signal enable_control_i             : std_logic;
    signal enable_status_i             : std_logic;
    signal reset_control_i              : std_logic;
    signal reset_status_i              : std_logic;
    signal start_inspection_control_i   : std_logic;
    signal start_inspection_status_i    : std_logic;
    signal scan_mode_control_i          : std_logic;
    signal scan_mode_status_i          : std_logic;
    signal cscan_mode_control_i         : std_logic_vector(REGISTER_W4-1 downto 0); 
    signal cscan_mode_status_i         : std_logic_vector(REGISTER_W4-1 downto 0); 
    signal trigger_type_control_i       : std_logic_vector(REGISTER_W6-1 downto 0);  
    signal trigger_type_status_i       : std_logic_vector(REGISTER_W6-1 downto 0);  
    
    -- dma state machine
    type volta_dma_states is (idle, prepare_write_configuration, write_configuration, write_configuration_ack, 
                              read_configuration, read_configuration_end, 
                              prepare_read_capability_1, prepare_read_capability_2, read_capability, read_capability_end,
                              start_send_ascan, send_ascan_ch0, send_ascan_ch1, send_ascan_channel0, send_ascan_channel1, stop_send_ascan); 
    signal volta_dma_state : volta_dma_states; 
    
    -- UPLOAD
    signal dma_upload_control_ascan_request     : std_logic;
    signal dma_upload_control_cscan_request     : std_logic;
    signal dma_upload_control_config_request    : std_logic;
    signal dma_upload_control_capability_request: std_logic;
    signal dma_upload_control_ascan_availability: std_logic;
    signal dma_upload_control_cscan_availability: std_logic;
    signal dma_upload_control_scan_ready        : std_logic;
    signal dma_upload_control_config_ready      : std_logic;
    signal dma_upload_control_capability_ready  : std_logic;
    signal dma_upload_control_data_size         : std_logic_vector(REGISTER_W21-1 downto 0);
        
    signal dma_upload_status_ascan_request      : std_logic;
    signal dma_upload_status_cscan_request      : std_logic;
    signal dma_upload_status_config_request     : std_logic;
    signal dma_upload_status_capability_request : std_logic;
    signal dma_upload_status_ascan_availability : std_logic;
    signal dma_upload_status_cscan_availability : std_logic;
    signal dma_upload_status_scan_ready         : std_logic;
    signal dma_upload_status_config_ready       : std_logic;
    signal dma_upload_status_capability_ready   : std_logic;
    signal dma_upload_status_data_size          : std_logic_vector(REGISTER_W21-1 downto 0);
    
    signal previous_ascan_channel_sent  : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal channel_selected_for_upload  : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
       
    -- DOWNLOAD
    signal dma_download_control_config_request      : std_logic;
    signal dma_download_control_ascan_request       : std_logic;
    signal dma_download_control_data_size           : std_logic_vector(REGISTER_W21-1 downto 0);
        
    signal dma_download_status_config_request       : std_logic;
    signal dma_download_status_ascan_request        : std_logic;
    signal dma_download_status_data_size            : std_logic_vector(REGISTER_W21-1 downto 0);
         
    signal sw_reset_i                   : std_logic;

	---------------------------------------------- CONFIGURATION ----------------------------------------------
	
    signal config_header_size        : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal config_channels           : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal config_channel0_offset    : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal config_channel1_offset    : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal config_encoder_offset     : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal config_digitalio_offset   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal config_channel0_size      : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal config_channel1_size      : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal config_channel0_configuration_size_flag: STD_LOGIC:= '0';
	signal config_channel1_configuration_size_flag: STD_LOGIC:= '0';

	signal config_encoder_enabled: STD_LOGIC;

	signal config_encoder_x_axis_enabled: STD_LOGIC;
	signal config_encoder_x_axis_start: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal config_encoder_x_axis_stopt: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal config_encoder_x_axis_trigger_counts: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

	signal config_encoder_y_axis_enabled: STD_LOGIC;
	signal config_encoder_y_axis_start: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal config_encoder_y_axis_stopt: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal config_encoder_y_axis_trigger_counts: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

	signal config_digitalio: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	
	signal config_counter : UNSIGNED(REGISTER_W32-1 downto 0);
	
    COMPONENT main_configuration IS
        generic (
            REGISTER_W32            : INTEGER := 32;
            BRAM_TREADY_SYNC        : INTEGER := 3
        );
        port (
            -- Sync
            aclk         : in STD_LOGIC;
            aresetn      : in STD_LOGIC;
            
            -- Control
            READ_CONFIGURATION  : in STD_LOGIC;
            CONFIGURATION_READY : out STD_LOGIC;
            CONFIGURATION_SIZE  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            
            -- Input
            CONFIGURATION_IN_AXIS_TREADY  : out STD_LOGIC;
            CONFIGURATION_IN_AXIS_TDATA   : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            CONFIGURATION_IN_AXIS_TLAST   : in STD_LOGIC;
            CONFIGURATION_IN_AXIS_TVALID  : in STD_LOGIC;
            
            -- Output
            CONFIGURATION_OUT_AXIS_TREADY  : in STD_LOGIC;
            CONFIGURATION_OUT_AXIS_TDATA   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            CONFIGURATION_OUT_AXIS_TLAST   : out STD_LOGIC;
            CONFIGURATION_OUT_AXIS_TVALID  : out STD_LOGIC;
            CONFIGURATION_OUT_AXIS_TKEEP   : out STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0)
        );
    END COMPONENT main_configuration;
    
    signal main_configuration_ready    : STD_LOGIC;
    signal main_configuration_size     : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    
    signal main_configuration_in_axis_tready   : STD_LOGIC;
    signal main_configuration_in_axis_tdata    : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal main_configuration_in_axis_tlast    : STD_LOGIC;
    signal main_configuration_in_axis_tvalid   : STD_LOGIC;
    
    signal main_configuration_out_axis_tready  : STD_LOGIC;
    signal main_configuration_out_axis_tdata   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal main_configuration_out_axis_tlast   : STD_LOGIC;
    signal main_configuration_out_axis_tvalid  : STD_LOGIC;
    signal main_configuration_out_axis_tkeep   : STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0);
    
    signal read_configuration_flag : STD_LOGIC;
    signal write_configuration_flag : STD_LOGIC;
      
    -- tkeep
    constant tkeep_all_bytes : STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0):= (others => '1');
    
    ---------------------------------------------- CAPABILITIES ----------------------------------------------
    
    COMPONENT capabilities IS
        generic (
            RAM_ADDR_LENGTH         : INTEGER := 10;
            REGISTER_W32            : INTEGER := 32;
            BRAM_BRAM_TREADY_SYNC   : INTEGER := 2;
            WAIT_FOR_BRAM           : INTEGER := 16
        );
        port (
            -- Sync
            aclk         : in STD_LOGIC;
            aresetn      : in STD_LOGIC;
            
            -- Control
            READ_CAPABILITIES  : in STD_LOGIC;
            CAPABILITIES_READY : out STD_LOGIC;
            CAPABILITIES_SIZE  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
                            
            -- Capabilities
            CAPABILITIES_AXIS_TREADY  : in STD_LOGIC;
            CAPABILITIES_AXIS_TDATA   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            CAPABILITIES_AXIS_TLAST   : out STD_LOGIC;
            CAPABILITIES_AXIS_TVALID  : out STD_LOGIC;
            CAPABILITIES_AXIS_TKEEP   : out STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0)
        );
    END COMPONENT capabilities;
    
    signal read_capability_flag : STD_LOGIC;
    
    signal read_capabilities  : STD_LOGIC;
    signal capabilities_ready  : STD_LOGIC;
    signal capabilities_size  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
                    
    signal capabilities_axis_tready  : STD_LOGIC;
    signal capabilities_axis_tdata   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal capabilities_axis_tlast   : STD_LOGIC;
    signal capabilities_axis_tvalid  : STD_LOGIC;
    signal capabilities_axis_tkeep   : STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0);
    
    ---------------------------------------------- CHANNEL MUX ----------------------------------------------
    
    COMPONENT channel_multiplexer is
        generic (
            N_CHANNELS        : INTEGER := 2;
            REGISTER_W16    : INTEGER := 16
        );
        port (
            -- Sync
            aclk         : in STD_LOGIC;
            aresetn      : in STD_LOGIC;
            
            -- Control
            CHANNEL_MULTIPLEXER_EN : in STD_LOGIC;
            
            -- Tready
            TREADY_IN  : in STD_LOGIC_VECTOR(N_CHANNELS-1 downto 0);
            TREADY_OUT  : out STD_LOGIC_VECTOR(N_CHANNELS-1 downto 0)
        );
    END COMPONENT channel_multiplexer;
    
    signal CHANNEL_MULTIPLEXER_EN : STD_LOGIC;
    signal TREADY_IN  : STD_LOGIC_VECTOR(N_CHANNELS-1 downto 0);
    signal TREADY_OUT  : STD_LOGIC_VECTOR(N_CHANNELS-1 downto 0);
   
	---------------------------------------------- I/O ----------------------------------------------
	
	signal IO_OUTPUTS_i : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal IO_INPUTS_i  : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	
begin

    CONFIGURATION_HEADER_SIZE      <= config_header_size;
    CONFIGURATION_CHANNELS         <= config_channels;
    CONFIGURATION_CHANNEL0_OFFSET  <= config_channel0_offset;
    CONFIGURATION_CHANNEL1_OFFSET  <= config_channel1_offset;
    CONFIGURATION_ENCODER_OFFSET   <= config_encoder_offset;
    CONFIGURATION_DIGITALIO_OFFSET <= config_digitalio_offset;
    CONFIGURATION_CHANNEL0_SIZE    <= config_channel0_size;
    CONFIGURATION_CHANNEL1_SIZE    <= config_channel0_size;
    CONFIGURATION_ENCODER_ENABLED  <= config_encoder_enabled;

    IO_INPUTS_i <= IO_INPUTS;
	  
    -- STATUS
    status_process: process(aclk, aresetn)
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				enable_status_i            <= '0';    
				reset_status_i             <= '0';  
				start_inspection_status_i  <= '0'; 
				scan_mode_status_i         <= '0'; 
				cscan_mode_status_i        <= (others => '0');  
				trigger_type_status_i      <= (others => '0'); 
				sw_reset_i <= '0';            
        	else
				enable_status_i <= ENABLE_CONTROL;
				sw_reset_i <= RESET_CONTROL or (not ENABLE_CONTROL);
				reset_status_i <= RESET_CONTROL;
				--if( sw_reset_i = '0') then
					start_inspection_status_i <= START_INSPECTION_CONTROL;
					scan_mode_status_i <= SCAN_MODE_CONTROL;
					cscan_mode_status_i <= CSCAN_MODE_CONTROL;
					trigger_type_status_i <= TRIGGER_TYPE_CONTROL;
				--else
					--start_inspection_status_i  <= '0'; 
					--scan_mode_status_i         <= '0'; 
					--cscan_mode_status_i        <= (others => '0');  
					--trigger_type_status_i      <= (others => '0');
				--end if;
			end if;
        end if;
    end process;
    
    -- CONTROL
    control_process: process(aclk, aresetn)
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				-- CONTROL 
				enable_control_i            <= '0';    
				reset_control_i             <= '0';  
				start_inspection_control_i  <= '0'; 
				scan_mode_control_i         <= '0'; 
				cscan_mode_control_i        <= (others => '0');  
				trigger_type_control_i      <= (others => '0'); 
				-- STATUS  
				ENABLE_STATUS <= '0';
				RESET_STATUS <= '0';
				START_INSPECTION_STATUS <= '0';
				SCAN_MODE_STATUS  <= '0';
				CSCAN_MODE_STATUS <= (others => '0'); 
				TRIGGER_TYPE_STATUS <= (others => '0'); 
				SW_RESET <= '0';
        	else
				-- CONTROL 
				enable_control_i            <= ENABLE_CONTROL;
				reset_control_i             <= RESET_CONTROL;  
				start_inspection_control_i  <= START_INSPECTION_CONTROL;  
				scan_mode_control_i         <= SCAN_MODE_CONTROL;   
				cscan_mode_control_i        <= CSCAN_MODE_CONTROL;
				trigger_type_control_i      <= TRIGGER_TYPE_CONTROL; 
				-- STATUS  
				ENABLE_STATUS <= enable_status_i;  
				RESET_STATUS <= reset_status_i; 
				START_INSPECTION_STATUS <= start_inspection_status_i; 
				SCAN_MODE_STATUS <= scan_mode_status_i; 
				CSCAN_MODE_STATUS <= cscan_mode_status_i; 
				TRIGGER_TYPE_STATUS <= trigger_type_status_i;   
				SW_RESET <= sw_reset_i;  
			end if;
        end if;
    end process;
   					
    -- UPLOAD DMA PORT (master) source selection
	DMA_M_axis_tvalid <= capabilities_axis_tvalid when read_capability_flag = '1' else
	                     main_configuration_out_axis_tvalid when read_configuration_flag = '1' else
	                     CH1_ASCAN_AXIS_TVALID when unsigned(channel_selected_for_upload) = 2 else 
						 CH0_ASCAN_AXIS_TVALID when unsigned(channel_selected_for_upload) = 1 else 
						 '0';
		
	DMA_M_axis_tdata <= capabilities_axis_tdata when read_capability_flag = '1' else
	                    main_configuration_out_axis_tdata when read_configuration_flag = '1' else
	                    CH1_ASCAN_AXIS_TDATA when unsigned(channel_selected_for_upload) = 2 else 
						CH0_ASCAN_AXIS_TDATA when unsigned(channel_selected_for_upload) = 1 else 
						(others =>'0');
		
	DMA_M_axis_tlast <= capabilities_axis_tlast when read_capability_flag = '1' else
	                    main_configuration_out_axis_tlast when read_configuration_flag = '1' else
	                    CH1_ASCAN_AXIS_TLAST when unsigned(channel_selected_for_upload) = 2 else 
						CH0_ASCAN_AXIS_TLAST when unsigned(channel_selected_for_upload) = 1 else 
						'0';
						
	DMA_M_axis_tkeep <= capabilities_axis_tkeep when read_capability_flag = '1' else
	                    main_configuration_out_axis_tkeep when read_configuration_flag = '1' else
                        CH1_ASCAN_AXIS_TKEEP when unsigned(channel_selected_for_upload) = 2 else 
                        CH0_ASCAN_AXIS_TKEEP when unsigned(channel_selected_for_upload) = 1 else 
                        (others =>'0');
	
	CH1_ASCAN_AXIS_TREADY <= DMA_M_axis_tready when unsigned(channel_selected_for_upload) = 2 else
							'0';
		
	CH0_ASCAN_AXIS_TREADY <= DMA_M_axis_tready when unsigned(channel_selected_for_upload) = 1 else
							'0';
							
	main_configuration_out_axis_tready  <= DMA_M_axis_tready when read_configuration_flag = '1' else
	                       '0';
	                       
	capabilities_axis_tready  <= DMA_M_axis_tready when read_capability_flag = '1' else
                           '0'; 
                           
    -- MAIN configuration input enable (DMA_S) when  write_configuration_flag = '1'                                                 
    main_configuration_in_axis_tdata <= DMA_S_axis_tdata when write_configuration_flag = '1' else
                                        (others => '0'); 
                                        
    main_configuration_in_axis_tlast <= DMA_S_axis_tlast when write_configuration_flag = '1' else
                                        '0';          
                                        
    main_configuration_in_axis_tvalid <= DMA_S_axis_tvalid when write_configuration_flag = '1' else
                                        '0';  
                                                      	                       	
    -- DMA   
    dma_process: process(aclk, aresetn)
		variable var_counter : integer range 0 to 65536 :=0;			
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				-- DOWNLOAD
				dma_download_status_config_request <= '0';
				dma_download_status_ascan_request <= '0';
				-- UPLOAD
				dma_upload_status_ascan_request <= '0';
				dma_upload_status_cscan_request <= '0';
				dma_upload_status_config_request <= '0';
				dma_upload_status_capability_request <= '0';
				dma_upload_status_ascan_availability <= '0';
				dma_upload_status_cscan_availability <= '0';
				dma_upload_status_scan_ready <= '0';
				dma_upload_status_config_ready <= '0';
				dma_upload_status_capability_ready <= '0';
				dma_upload_status_data_size <= (others => '0');
				CH0_READ_ASCAN <= '0';	
				CH1_READ_ASCAN <= '0';		
				previous_ascan_channel_sent <= (others => '0');
				channel_selected_for_upload <= (others => '0');
				-- CONFIG
				CH0_CONFIG_INPUT_LOAD <= '0';
				CH0_CONFIG_INPUT_AXIS_TDATA <= (others => '0');
				CH0_CONFIG_INPUT_AXIS_TLAST <= '0';
				CH0_CONFIG_INPUT_AXIS_TVALID <= '0';
				CH1_CONFIG_INPUT_LOAD <= '0';
				CH1_CONFIG_INPUT_AXIS_TDATA <= (others => '0');
				CH1_CONFIG_INPUT_AXIS_TLAST <= '0';
				CH1_CONFIG_INPUT_AXIS_TVALID <= '0';

				config_header_size <= (others => '0');
				config_channels <= (others => '0');
				config_channel0_offset <= (others => '0');
				config_channel1_offset <= (others => '0');
				config_encoder_offset <= (others => '0');
				config_digitalio_offset <= (others => '0');
				config_channel0_size <= (others => '0');
				config_channel1_size <= (others => '0');

				config_encoder_enabled <= '0';
				config_encoder_x_axis_enabled <= '0';
				config_encoder_x_axis_start <= (others => '0');
				config_encoder_x_axis_stopt <= (others => '0');
				config_encoder_x_axis_trigger_counts <= (others => '0');
				config_encoder_y_axis_enabled <= '0';
				config_encoder_y_axis_start <= (others => '0');
				config_encoder_y_axis_stopt <= (others => '0');
				config_encoder_y_axis_trigger_counts <= (others => '0');

				config_digitalio <= (others => '0');
				
				CONFIGURATION_ON <= '0';
				CONFIGURATION_SIZE <= (others => '0');
				config_counter <= (others => '0');
                
                write_configuration_flag <= '0';
                read_configuration_flag <= '0';
                read_capability_flag <= '0';
                
				-- state
				volta_dma_state <= idle;            
        	else
				case volta_dma_state is        
					when idle =>
						DMA_S_axis_tready <= '1';
						var_counter := 0;
						if(dma_upload_control_capability_ready = '1') then
						    volta_dma_state <= prepare_read_capability_1;
						elsif(dma_download_control_config_request = '1') then
						    dma_upload_status_config_ready <= '0';
							volta_dma_state <= prepare_write_configuration;  
					    elsif(dma_upload_control_config_request = '1') then
					        volta_dma_state <= read_configuration;
					    elsif(dma_upload_control_ascan_request = '1') then
					       if(unsigned(channel_selected_for_upload) = 1) then
					           volta_dma_state <= send_ascan_ch0;
					       elsif(unsigned(channel_selected_for_upload) = 2) then
					           volta_dma_state <= send_ascan_ch1;
					       end if;
					    else            
                            if(CH0_ASCAN_READY = '1' or CH1_ASCAN_READY = '1') then
                                volta_dma_state <= start_send_ascan; 
                            else
                                dma_upload_status_scan_ready <= '0'; 
                            end if;
                        end if;
                    -- Handshake
                    when prepare_read_capability_1 =>
                        if(capabilities_ready = '1') then
                            dma_upload_status_capability_ready <= capabilities_ready;
                            dma_upload_status_data_size <= capabilities_size(REGISTER_W21-1 downto 0);
                            volta_dma_state <= prepare_read_capability_2;
                        end if;
                    -- Handshake
                    when prepare_read_capability_2 =>
                        if(capabilities_ready = '0') then
                            dma_upload_status_capability_ready <= '0';
                            volta_dma_state <= read_capability;
                        end if;
                    -- Handshake
                    when read_capability =>
                        if(dma_upload_control_capability_request = '1') then
                            dma_upload_status_capability_request <= '1';
                            read_capability_flag <= '1';
                            volta_dma_state <= read_capability_end;
                        end if;
                    -- Handshake   
                    when read_capability_end =>
                        if(dma_upload_control_capability_request = '0') then
                            dma_upload_status_capability_request <= '0';
                            read_capability_flag <= '0';
                            volta_dma_state <= idle;
                        end if;
                    -- Handshake
					when read_configuration =>
					    if(dma_upload_control_config_request = '1') then
					       if(main_configuration_ready = '1') then
					           read_configuration_flag <= '1';
					           dma_upload_status_config_request <= '1';
					           dma_upload_status_data_size <= main_configuration_size(REGISTER_W21-1 downto 0);
					           volta_dma_state  <= read_configuration_end;
					       end if;
                        end if;		
                    -- Handshake
                    when read_configuration_end => 
                        if(dma_upload_control_config_request = '0') then
                            dma_upload_status_config_request <= '0';
                            read_configuration_flag <= '0';
                            volta_dma_state <= idle;
                        end if;
                    -- Handshake                                       			   					  
					when prepare_write_configuration =>
					    if(CH0_CONFIG_INPUT_AXIS_TREADY = '1' and CH1_CONFIG_INPUT_AXIS_TREADY = '1'
					       and main_configuration_in_axis_tready = '1') then
					       	config_header_size <= (others => '1');
                           	config_channels <= (others => '1');
                           	config_channel0_offset <= (others => '1');
                           	config_channel1_offset <= (others => '1');
                           	config_encoder_offset <= (others => '1');
                           	config_digitalio_offset <= (others => '1');
                           	config_channel0_size <= (others => '1');
                           	config_channel1_size <= (others => '1'); 
							dma_download_status_config_request <= '1';
							config_counter <= (others => '0');
							write_configuration_flag <= '1';
							volta_dma_state <= write_configuration;  
					    end if;
					-- Write config (split by channels & store in main_configuration the original one)
					when write_configuration =>
						if(DMA_S_axis_tvalid = '1' and DMA_S_axis_tkeep = tkeep_all_bytes) then
						    config_counter <= config_counter + 1;
							if(var_counter = 0) then
								config_header_size <= DMA_S_axis_tdata;	
							elsif(var_counter = 1) then
								config_channels <= DMA_S_axis_tdata;	
							elsif(var_counter = 2) then
								config_channel0_offset <= DMA_S_axis_tdata;	
							elsif(var_counter = 3) then
								config_channel1_offset <= DMA_S_axis_tdata;	
							elsif(var_counter = 4) then
								config_encoder_offset <= DMA_S_axis_tdata;		
							elsif(var_counter = 5) then
								config_digitalio_offset <= DMA_S_axis_tdata;
						    elsif(var_counter = unsigned(config_channel0_offset)) then
						        config_channel0_size <= DMA_S_axis_tdata;
						        CH0_CONFIG_INPUT_AXIS_TDATA <= DMA_S_axis_tdata;	
                                CH0_CONFIG_INPUT_AXIS_TVALID <= '1';
                                CH0_CONFIG_INPUT_LOAD <= '1';
							elsif(	(var_counter > unsigned(config_channel0_offset)) and 
									(var_counter <= (unsigned(config_channel0_offset)+unsigned(config_channel0_size)-1)) 	)then
								CH0_CONFIG_INPUT_AXIS_TDATA <= DMA_S_axis_tdata;	
								CH0_CONFIG_INPUT_AXIS_TVALID <= '1';
								CH0_CONFIG_INPUT_LOAD <= '1';
								if(var_counter = (unsigned(config_channel0_offset)+unsigned(config_channel0_size)-1)) then
									CH0_CONFIG_INPUT_AXIS_TLAST <= '1';	
								end if;
						    elsif(var_counter = (unsigned(config_channel0_offset)+unsigned(config_channel0_size))) then
						        config_channel1_size <= DMA_S_axis_tdata;
						        CH0_CONFIG_INPUT_AXIS_TVALID <= '0';
                                CH0_CONFIG_INPUT_AXIS_TLAST  <= '0';
                                CH0_CONFIG_INPUT_LOAD        <= '0';
                                CH1_CONFIG_INPUT_AXIS_TDATA  <= DMA_S_axis_tdata;	
                                CH1_CONFIG_INPUT_AXIS_TVALID <= '1';
                                CH1_CONFIG_INPUT_LOAD        <= '1';
							elsif(	(var_counter > unsigned(config_channel1_offset)) and 
									(var_counter <= (unsigned(config_channel1_offset)+unsigned(config_channel1_size)-1)) 	)then
								CH1_CONFIG_INPUT_AXIS_TDATA  <= DMA_S_axis_tdata;	
								CH1_CONFIG_INPUT_AXIS_TVALID <= '1';
								CH1_CONFIG_INPUT_LOAD 		 <= '1';
								if(var_counter = (unsigned(config_channel1_offset)+unsigned(config_channel1_size)-1)) then
									CH1_CONFIG_INPUT_AXIS_TLAST <= '1';	
								end if;	
							elsif(	(var_counter > (unsigned(config_channel1_offset)+unsigned(config_channel1_size)-1)) and
									(var_counter < (unsigned(config_digitalio_offset))) ) then 
								CH1_CONFIG_INPUT_AXIS_TVALID <= '0';
								CH1_CONFIG_INPUT_AXIS_TLAST  <= '0';
								CH1_CONFIG_INPUT_LOAD 		 <= '0';
								if(var_counter = unsigned(config_encoder_offset))then
									config_encoder_enabled	<= DMA_S_axis_tdata(0);
								elsif(var_counter = unsigned(config_encoder_offset)+1)then
									config_encoder_x_axis_enabled	<= DMA_S_axis_tdata(0);
								elsif(var_counter = unsigned(config_encoder_offset)+2)then
									config_encoder_x_axis_start	<= DMA_S_axis_tdata;
								elsif(var_counter = unsigned(config_encoder_offset)+3)then
									config_encoder_x_axis_stopt	<= DMA_S_axis_tdata;
								elsif(var_counter = unsigned(config_encoder_offset)+4)then
									config_encoder_x_axis_trigger_counts <= DMA_S_axis_tdata;
								elsif(var_counter = unsigned(config_encoder_offset)+4)then
									config_encoder_y_axis_enabled <= DMA_S_axis_tdata(0);
								elsif(var_counter = unsigned(config_encoder_offset)+4)then
									config_encoder_y_axis_start <= DMA_S_axis_tdata;
								elsif(var_counter = unsigned(config_encoder_offset)+4)then
									config_encoder_y_axis_stopt <= DMA_S_axis_tdata;
								elsif(var_counter = unsigned(config_encoder_offset)+4)then
									config_encoder_y_axis_trigger_counts <= DMA_S_axis_tdata;				
								end if;
							elsif(var_counter = (unsigned(config_digitalio_offset))) then 	
								config_digitalio <= DMA_S_axis_tdata;	
							end if;
							var_counter := var_counter + 1;
						else
							CH0_CONFIG_INPUT_AXIS_TVALID <= '0';	
							CH1_CONFIG_INPUT_AXIS_TVALID <= '0';
							CH0_CONFIG_INPUT_AXIS_TLAST  <= '0';
							CH1_CONFIG_INPUT_AXIS_TLAST  <= '0';	
						end if;
						if(DMA_S_axis_tlast = '1') then
							volta_dma_state <= write_configuration_ack; 
						end if;
				    -- Handshake
					when write_configuration_ack =>
						CH0_CONFIG_INPUT_AXIS_TVALID <= '0';	
						CH1_CONFIG_INPUT_AXIS_TVALID <= '0';
						CH0_CONFIG_INPUT_AXIS_TLAST  <= '0';
						CH1_CONFIG_INPUT_AXIS_TLAST  <= '0';
						if(dma_download_control_config_request = '0') then
							dma_download_status_config_request <= '0';
							CONFIGURATION_ON <= '1';
                            CONFIGURATION_SIZE <= std_logic_vector(config_counter);
                            dma_upload_status_config_ready <= '1';
                            write_configuration_flag <= '0';
							volta_dma_state <= idle; 
						end if;
				    -- Upload ASCAN
					when start_send_ascan =>
					    dma_upload_status_scan_ready <= '1'; 
						if(CH0_ASCAN_READY = '1' and CH1_ASCAN_READY = '0') then
						    dma_upload_status_data_size <= CH0_ASCAN_SIZE;
						    channel_selected_for_upload	<= std_logic_vector(to_unsigned(1, channel_selected_for_upload'length));					 
						elsif(CH0_ASCAN_READY = '0' and CH1_ASCAN_READY = '1') then
						    dma_upload_status_data_size <= CH1_ASCAN_SIZE;
						    channel_selected_for_upload	<= std_logic_vector(to_unsigned(2, channel_selected_for_upload'length));
						else
							if(unsigned(previous_ascan_channel_sent) = 0 or unsigned(previous_ascan_channel_sent) = 2) then
							    dma_upload_status_data_size <= CH0_ASCAN_SIZE;
							    channel_selected_for_upload	<= std_logic_vector(to_unsigned(1, channel_selected_for_upload'length));		
							else
							    dma_upload_status_data_size <= CH1_ASCAN_SIZE;
							    channel_selected_for_upload	<= std_logic_vector(to_unsigned(2, channel_selected_for_upload'length));
							end if;
						end if;
						volta_dma_state <= idle;	
				    -- Handshake
					when send_ascan_ch0 => 
						previous_ascan_channel_sent <= channel_selected_for_upload;												
						if(dma_upload_control_ascan_request = '1' and (unsigned(CH0_ASCAN_SIZE) = unsigned(dma_upload_control_data_size))) then
							dma_upload_status_ascan_request <= '1';
							dma_upload_status_scan_ready <= '0';
							CH0_READ_ASCAN	<= '1';																															
						end if;
						if(CH0_ASCAN_AXIS_TLAST = '1') then
							volta_dma_state <= stop_send_ascan;																
						end if;	
					-- Handshake																														
					when send_ascan_ch1 => 
						previous_ascan_channel_sent <= channel_selected_for_upload;															
						if(dma_upload_control_ascan_request = '1' and (unsigned(CH1_ASCAN_SIZE) = unsigned(dma_upload_control_data_size))) then
							dma_upload_status_ascan_request <= '1';
							dma_upload_status_scan_ready <= '0';
							CH1_READ_ASCAN	<= '1';																															
						end if;	
						if(CH1_ASCAN_AXIS_TLAST = '1') then
							volta_dma_state <= stop_send_ascan;																
						end if;		
					-- Handshake																
					when stop_send_ascan =>
						CH0_READ_ASCAN	<= '0';		
						CH1_READ_ASCAN	<= '0';																	
						if(dma_upload_control_ascan_request = '0') then
							dma_upload_status_ascan_request <= '0';
							volta_dma_state <= idle; 
						end if;              
					when others =>
						null;                   
				end case;  
			end if;
        end if;
    end process;
    
    -- DMA Control signal management
    DMA_signals: process(aclk, aresetn)
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				-- CONTROL 
				dma_upload_control_ascan_request        <= '0';    
				dma_upload_control_cscan_request        <= '0';
				dma_upload_control_config_request       <= '0'; 
				dma_upload_control_capability_request   <= '0'; 
				dma_upload_control_scan_ready           <= '0'; 
				dma_upload_control_config_ready         <= '0'; 
				dma_upload_control_capability_ready     <= '0';   
				dma_upload_control_data_size            <= (others => '0');  
				-- STATUS  
				dma_download_control_config_request     <= '0';    
				dma_download_control_ascan_request      <= '0';
				dma_download_control_data_size          <= (others => '0'); 
        	else
				-- CONTROL 
				dma_upload_control_ascan_request <= DMA_UPLOAD_CONTROL(0);
				dma_upload_control_cscan_request <= DMA_UPLOAD_CONTROL(1);
				dma_upload_control_config_request <= DMA_UPLOAD_CONTROL(2);
				dma_upload_control_capability_request <= DMA_UPLOAD_CONTROL(3);
				dma_upload_control_ascan_availability <= DMA_UPLOAD_CONTROL(4);
				dma_upload_control_Cscan_availability <= DMA_UPLOAD_CONTROL(5);
				dma_upload_control_scan_ready <= DMA_UPLOAD_CONTROL(6);
				dma_upload_control_config_ready <= DMA_UPLOAD_CONTROL(7);
				dma_upload_control_capability_ready <= DMA_UPLOAD_CONTROL(8);
				dma_upload_control_data_size <= DMA_UPLOAD_CONTROL(31 downto 11);
				-- STATUS  
				dma_download_control_config_request <= DMA_DOWNLOAD_CONTROL(0);
				dma_download_control_ascan_request <= DMA_DOWNLOAD_CONTROL(1);
				dma_download_control_data_size <= DMA_DOWNLOAD_CONTROL(31 downto 11);  
			end if;
        end if;
    end process;
    
    dma_download_status_data_size <= dma_download_control_data_size;

    DMA_DOWNLOAD_STATUS <= dma_download_status_data_size & "000000000" & dma_download_status_ascan_request & dma_download_status_config_request;
        
    DMA_UPLOAD_STATUS <= dma_upload_status_data_size & "00" & dma_upload_status_capability_ready & dma_upload_status_config_ready & 
        dma_upload_status_scan_ready & dma_upload_status_cscan_availability & dma_upload_status_ascan_availability & 
        dma_upload_status_capability_request &  dma_upload_status_config_request & dma_upload_status_cscan_request & dma_upload_status_ascan_request;
        
        
    ------------------------------------------ MAIN CONFIGURATION ------------------------------------------      
        
    main_configuration_inst: main_configuration
        generic map(
            REGISTER_W32     =>  REGISTER_W32
        )
        port map(
            -- Sync
            aclk         => aclk,
            aresetn      => aresetn,
            
            -- Control
            READ_CONFIGURATION  => read_configuration_flag,
            CONFIGURATION_READY => main_configuration_ready,
            CONFIGURATION_SIZE  => main_configuration_size,
                
            -- Input
            CONFIGURATION_IN_AXIS_TREADY  => main_configuration_in_axis_tready,
            CONFIGURATION_IN_AXIS_TDATA   => main_configuration_in_axis_tdata,
            CONFIGURATION_IN_AXIS_TLAST   => main_configuration_in_axis_tlast,
            CONFIGURATION_IN_AXIS_TVALID  => main_configuration_in_axis_tvalid,
                
            -- Output
            CONFIGURATION_OUT_AXIS_TREADY  => main_configuration_out_axis_tready,
            CONFIGURATION_OUT_AXIS_TDATA   => main_configuration_out_axis_tdata,
            CONFIGURATION_OUT_AXIS_TLAST   => main_configuration_out_axis_tlast,
            CONFIGURATION_OUT_AXIS_TVALID  => main_configuration_out_axis_tvalid,
            CONFIGURATION_OUT_AXIS_TKEEP   => main_configuration_out_axis_tkeep
        );
         
    ------------------------------------------ CAPABILITIES ------------------------------------------
    
    capabilities_inst: capabilities
        generic map (
            REGISTER_W32     =>  REGISTER_W32
        )
        port map(
            -- Sync
            aclk         => aclk,
            aresetn      => aresetn,
                
            -- Control
            READ_CAPABILITIES  => read_capability_flag,
            CAPABILITIES_READY => capabilities_ready,
            CAPABILITIES_SIZE  => capabilities_size,
                                
            -- Capabilities
            CAPABILITIES_AXIS_TREADY  => capabilities_axis_tready,
            CAPABILITIES_AXIS_TDATA   => capabilities_axis_tdata,
            CAPABILITIES_AXIS_TLAST   => capabilities_axis_tlast,
            CAPABILITIES_AXIS_TVALID  => capabilities_axis_tvalid,
            CAPABILITIES_AXIS_TKEEP   => capabilities_axis_tkeep
        );
        
    ------------------------------------------ CHANNEL MUX ------------------------------------------      
        
    channel_multiplexer_inst: channel_multiplexer
        generic map(
            N_CHANNELS      => N_CHANNELS,
            REGISTER_W16    => REGISTER_W16
        )
        port map(
            -- Sync
            aclk         => aclk,
            aresetn      => aresetn,
                
            -- Control
            CHANNEL_MULTIPLEXER_EN => CHANNEL_MULTIPLEXER_EN,
                
            -- Tready
            TREADY_IN   => TREADY_IN,
            TREADY_OUT  => TREADY_OUT
        );
        
    CH0_SIGNAL_INPUT_AXIS_TREADY <= TREADY_OUT(0);
    CH1_SIGNAL_INPUT_AXIS_TREADY <= TREADY_OUT(1);
    
    -- HARD coded until there is option included in configuration
    CHANNEL_MULTIPLEXER_EN <= '0';
      
	---------------------------------------------- CH0 ----------------------------------------------

	CH0_START_INSPECTION <= start_inspection_status_i;
	CH0_TRIGGER_TYPE <= (others => '0') when config_encoder_enabled = '0' else
	                    (others => '1');
	
	CH0_START_STOP_INSPECTION <= IO_INPUTS_i(0);
	
	--IO_OUTPUTS_i(0) <= CH0_INSPECTION_INPROGESS and CH1_INSPECTION_INPROGESS;
    -- CHANNEL 0 and CHANNEL 1 syncronized for inspection in progress
    inspection_in_progress_process: process(aclk, aresetn)
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				IO_OUTPUTS_i(0) <= '0';
        	else
				if(CH0_INSPECTION_INPROGESS = CH1_INSPECTION_INPROGESS) then
					IO_OUTPUTS_i(0) <= CH0_INSPECTION_INPROGESS;																	
				end if;
			end if;
        end if;
    end process;

	IO_OUTPUTS_i(15 downto 1) <= (others => '0');
	IO_OUTPUTS <= IO_OUTPUTS_i;
																																																																																	
	CH0_ENCODER1_POS <= ENCODER1_COUNT;
	CH0_ENCODER1_DIR <= ENCODER1_DIR;	
	CH0_ENCODER1_RPM <= ENCODER1_COUNT;
	CH0_ENCODER1_TRIGGER_COUNTS	<= config_encoder_x_axis_trigger_counts;																				
	
	CH0_ENCODER2_POS <= (others => '0');
	CH0_ENCODER2_DIR <= (others => '0');
	CH0_ENCODER2_RPM <= (others => '0');
	CH0_ENCODER2_TRIGGER_COUNTS	<= (others => '0');																				
	
	CH0_ENCODER3_POS <= (others => '0');
	CH0_ENCODER3_DIR <= (others => '0');
	CH0_ENCODER3_RPM <= (others => '0');
	CH0_ENCODER3_TRIGGER_COUNTS	<= (others => '0');																				
	
	channel_0_inst: channel
	generic map(
		-- General
		CHANNEL_ID		=> 0,
		REGISTER_W64    => REGISTER_W64,
		REGISTER_W48    => REGISTER_W48,
	    REGISTER_W32    => REGISTER_W32,
	    REGISTER_W21    => REGISTER_W21,
        REGISTER_W16    => REGISTER_W16,
		REGISTER_W14    => REGISTER_W14,
		REGISTER_W8     => REGISTER_W8,
		REGISTER_W4     => REGISTER_W4,
		REGISTER_W2     => REGISTER_W2,
		
		-- external trigger
		DISPOSITION_PASS  => DISPOSITION_PASS,
		DISPOSITION_FAIL  => DISPOSITION_FAIL,
		DISPOSITION_NODET => DISPOSITION_NODET,
		
		-- Coinc
        TRIGGER_WAIT    => TRIGGER_WAIT,
        MIN_COINC_LEVEL => MIN_COINC_LEVEL,
		
		-- Avg
		MIN_AVG_LEVEL   => MIN_AVG_LEVEL,
		
		-- Ascan header size
		ASCAN_HEADER_SIZE => ASCAN_HEADER_SIZE,
		
		-- FIR CORR
        N_TAPS              => N_TAPS,
        N_SETS_TAP          => N_SETS_TAP,
        OVERSAMPLING        => OVERSAMPLING,
        WAIT_CYCLES         => WAIT_CYCLES
	)
	port map(
	    -- Sync
	    aclk         => aclk,
        aresetn      => aresetn,
		
		-- Control
		START_INSPECTION  => CH0_START_INSPECTION, 
		TRIGGER_TYPE  	  => CH0_TRIGGER_TYPE, 
		READ_ASCAN  	  => CH0_READ_ASCAN, 
		ASCAN_READY  	  => CH0_ASCAN_READY, 
		ASCAN_SIZE	      => CH0_ASCAN_SIZE, 
		CHANNEL_ENABLED   => CONFIGURATION_CHANNEL0_ENABLED,
		CONFIG_ON         => CONFIGURATION_CHANNEL0_ON,
		
		START_STOP_INSPECTION  => CH0_START_STOP_INSPECTION, 
		INSPECTION_INPROGESS   => CH0_INSPECTION_INPROGESS, 
		DISPOSITION		  => CH0_DISPOSITION,
		DISPOSITION_BITS  => CH0_DISPOSITION_BITS,
		
		-- Signal input
		SIGNAL_COINC_WINDOW  	  => CH0_SIGNAL_COINC_WINDOW, 
		SIGNAL_AVG_WINDOW  		  => CH0_SIGNAL_AVG_WINDOW, 
		
		--SIGNAL_INPUT_AXIS_TREADY  => CH0_SIGNAL_INPUT_AXIS_TREADY, 
		SIGNAL_INPUT_AXIS_TREADY  => TREADY_IN(0), 
		SIGNAL_INPUT_AXIS_TDATA	  => CH0_SIGNAL_INPUT_AXIS_TDATA, 
		SIGNAL_INPUT_AXIS_TLAST	  => CH0_SIGNAL_INPUT_AXIS_TLAST, 
		SIGNAL_INPUT_AXIS_TVALID  => CH0_SIGNAL_INPUT_AXIS_TVALID, 
		
		-- Config
		CONFIG_INPUT_LOAD	  	  => CH0_CONFIG_INPUT_LOAD, 
		CONFIG_INPUT_AXIS_TREADY  => CH0_CONFIG_INPUT_AXIS_TREADY, 
		CONFIG_INPUT_AXIS_TDATA	  => CH0_CONFIG_INPUT_AXIS_TDATA, 
		CONFIG_INPUT_AXIS_TLAST	  => CH0_CONFIG_INPUT_AXIS_TLAST, 
		CONFIG_INPUT_AXIS_TVALID  => CH0_CONFIG_INPUT_AXIS_TVALID, 
		
		-- ENCODERS
		ENCODER1_POS  	  => CH0_ENCODER1_POS, 
		ENCODER1_DIR  	  => CH0_ENCODER1_DIR, 
		ENCODER1_RPM  	  => CH0_ENCODER1_RPM, 
		ENCODER1_TRIGGER_COUNTS => CH0_ENCODER1_TRIGGER_COUNTS,
		
		ENCODER2_POS  	  => CH0_ENCODER2_POS,
		ENCODER2_DIR  	  => CH0_ENCODER2_DIR,
		ENCODER2_RPM  	  => CH0_ENCODER2_RPM,
		ENCODER2_TRIGGER_COUNTS => CH0_ENCODER2_TRIGGER_COUNTS,
		
		ENCODER3_POS  	  => CH0_ENCODER3_POS,
		ENCODER3_DIR  	  => CH0_ENCODER3_DIR,
		ENCODER3_RPM  	  => CH0_ENCODER3_RPM,
		ENCODER3_TRIGGER_COUNTS => CH0_ENCODER3_TRIGGER_COUNTS,
		
		-- IO
		IO_INPUTS  	  	  => IO_INPUTS_i,
		IO_OUTPUTS  	  => IO_OUTPUTS_i,
		
		-- Config output receiver
		RECEIVER_SAMPLING_FREQUENCY         => CH0_RECEIVER_SAMPLING_FREQUENCY, 
        RECEIVER_DATA_WINDOW                => CH0_RECEIVER_DATA_WINDOW,  
        RECEIVER_DELAY                      => CH0_RECEIVER_DELAY,  
        RECEIVER_GAIN                       => CH0_RECEIVER_GAIN,  
        RECEIVER_EXTERNAL_MULTIPLEXER_EN    => CH0_RECEIVER_EXTERNAL_MULTIPLEXER_EN,
		
		-- Config output transmitter
		TRANSMITTER_VOLTAGE                 => CH0_TRANSMITTER_VOLTAGE, 
        TRANSMITTER_BURST_FREQUENCY         => CH0_TRANSMITTER_BURST_FREQUENCY,
        TRANSMITTER_N_CYCLES                => CH0_TRANSMITTER_N_CYCLES,
        TRANSMITTER_DELAY                   => CH0_TRANSMITTER_DELAY,
        TRANSMITTER_DELTA_DELAY             => CH0_TRANSMITTER_DELTA_DELAY,
        TRANSMITTER_DELTA_ADD               => CH0_TRANSMITTER_DELTA_ADD ,
        TRANSMITTER_DIRECTIONAL_PHASING     => CH0_TRANSMITTER_DIRECTIONAL_PHASING, 
		
		-- Config output magnet
		MAGNET_MODE                         => CH0_MAGNET_MODE,
        MAGNET_PULSE_WIDTH                  => CH0_MAGNET_PULSE_WIDTH, 
        MAGNET_INITIAL_DELAY                => CH0_MAGNET_INITIAL_DELAY,
		
		-- Condig analog filters
		DSP_ANALOG_FILTER  					=> CH0_DSP_ANALOG_FILTER,
		
		-- Config DAC curves
		CONFIG_DAC_AXIS_TREADY    => CH0_CONFIG_DAC_AXIS_TREADY,
		CONFIG_DAC_AXIS_TDATA	  => CH0_CONFIG_DAC_AXIS_TDATA,
		CONFIG_DAC_AXIS_TLAST	  => CH0_CONFIG_DAC_AXIS_TLAST,
		CONFIG_DAC_AXIS_TVALID    => CH0_CONFIG_DAC_AXIS_TVALID,
					
		-- ASCAN
		ASCAN_AXIS_TREADY  => CH0_ASCAN_AXIS_TREADY,
		ASCAN_AXIS_TDATA   => CH0_ASCAN_AXIS_TDATA,
		ASCAN_AXIS_TLAST   => CH0_ASCAN_AXIS_TLAST,
		ASCAN_AXIS_TVALID  => CH0_ASCAN_AXIS_TVALID,
		ASCAN_AXIS_TKEEP   => CH0_ASCAN_AXIS_TKEEP
	);
		
	---------------------------------------------- CH1 ----------------------------------------------
	
	CH1_START_INSPECTION <= start_inspection_status_i;
	CH1_TRIGGER_TYPE <= (others => '0') when config_encoder_enabled = '0' else
                        (others => '1');
    
    CH1_START_STOP_INSPECTION <= IO_INPUTS_i(0);
    
	CH1_ENCODER1_POS <= ENCODER1_COUNT;
    CH1_ENCODER1_DIR <= ENCODER1_DIR;    
    CH1_ENCODER1_RPM <= ENCODER1_COUNT;
    CH1_ENCODER1_TRIGGER_COUNTS    <= config_encoder_x_axis_trigger_counts; 																			
      
    CH1_ENCODER2_POS <= (others => '0');
    CH1_ENCODER2_DIR <= (others => '0');
    CH1_ENCODER2_RPM <= (others => '0');
	CH1_ENCODER2_TRIGGER_COUNTS	<= (others => '0');																				
    
    CH1_ENCODER3_POS <= (others => '0');
    CH1_ENCODER3_DIR <= (others => '0');
    CH1_ENCODER3_RPM <= (others => '0');
	CH1_ENCODER3_TRIGGER_COUNTS	<= (others => '0');																				
	
			
	channel_1_inst: channel
	generic map(
		-- General
		CHANNEL_ID		=> 1,
		REGISTER_W64    => REGISTER_W64,
		REGISTER_W48    => REGISTER_W48,
	    REGISTER_W32    => REGISTER_W32,
	    REGISTER_W21    => REGISTER_W21,
        REGISTER_W16    => REGISTER_W16,
		REGISTER_W14    => REGISTER_W14,
		REGISTER_W8     => REGISTER_W8,
		REGISTER_W4     => REGISTER_W4,
		REGISTER_W2     => REGISTER_W2,
		
		-- external trigger
		DISPOSITION_PASS  => DISPOSITION_PASS,
		DISPOSITION_FAIL  => DISPOSITION_FAIL,
		DISPOSITION_NODET => DISPOSITION_NODET,
		
		-- Coinc
        TRIGGER_WAIT    => TRIGGER_WAIT,
        MIN_COINC_LEVEL => MIN_COINC_LEVEL,
		
		-- Avg
		MIN_AVG_LEVEL   => MIN_AVG_LEVEL,
		
		-- Ascan header size
		ASCAN_HEADER_SIZE => ASCAN_HEADER_SIZE,
		
		-- FIR CORR
        N_TAPS              => N_TAPS,
        N_SETS_TAP          => N_SETS_TAP,
        OVERSAMPLING        => OVERSAMPLING,
        WAIT_CYCLES         => WAIT_CYCLES
	)
	port map(
	    -- Sync
	    aclk         => aclk,
        aresetn      => aresetn,
		
		-- Control
		START_INSPECTION  => CH1_START_INSPECTION,
		TRIGGER_TYPE  	  => CH1_TRIGGER_TYPE,
		READ_ASCAN  	  => CH1_READ_ASCAN,
		ASCAN_READY  	  => CH1_ASCAN_READY,
		ASCAN_SIZE	      => CH1_ASCAN_SIZE,
		CHANNEL_ENABLED   => CONFIGURATION_CHANNEL1_ENABLED,
		CONFIG_ON         => CONFIGURATION_CHANNEL1_ON,
		
		START_STOP_INSPECTION  => CH1_START_STOP_INSPECTION,
		INSPECTION_INPROGESS   => CH1_INSPECTION_INPROGESS,
		DISPOSITION		  => CH1_DISPOSITION,
		DISPOSITION_BITS  => CH1_DISPOSITION_BITS,
		
		-- Signal input
		SIGNAL_COINC_WINDOW  	  => CH1_SIGNAL_COINC_WINDOW,
		SIGNAL_AVG_WINDOW  		  => CH1_SIGNAL_AVG_WINDOW,
		
		--SIGNAL_INPUT_AXIS_TREADY  => CH1_SIGNAL_INPUT_AXIS_TREADY, 
		SIGNAL_INPUT_AXIS_TREADY  => TREADY_IN(1), 
		SIGNAL_INPUT_AXIS_TDATA	  => CH1_SIGNAL_INPUT_AXIS_TDATA, 
		SIGNAL_INPUT_AXIS_TLAST	  => CH1_SIGNAL_INPUT_AXIS_TLAST, 
		SIGNAL_INPUT_AXIS_TVALID  => CH1_SIGNAL_INPUT_AXIS_TVALID, 
		
		-- Config
		CONFIG_INPUT_LOAD	  	  => CH1_CONFIG_INPUT_LOAD,
		CONFIG_INPUT_AXIS_TREADY  => CH1_CONFIG_INPUT_AXIS_TREADY,
		CONFIG_INPUT_AXIS_TDATA	  => CH1_CONFIG_INPUT_AXIS_TDATA,
		CONFIG_INPUT_AXIS_TLAST	  => CH1_CONFIG_INPUT_AXIS_TLAST,
		CONFIG_INPUT_AXIS_TVALID  => CH1_CONFIG_INPUT_AXIS_TVALID,
		
		-- ENCODERS
		ENCODER1_POS  	  => CH1_ENCODER1_POS,
		ENCODER1_DIR  	  => CH1_ENCODER1_DIR,
		ENCODER1_RPM  	  => CH1_ENCODER1_RPM,
		ENCODER1_TRIGGER_COUNTS => CH1_ENCODER1_TRIGGER_COUNTS,
		
		ENCODER2_POS  	  => CH1_ENCODER2_POS,
		ENCODER2_DIR  	  => CH1_ENCODER2_DIR,
		ENCODER2_RPM  	  => CH1_ENCODER2_RPM,
		ENCODER2_TRIGGER_COUNTS => CH1_ENCODER2_TRIGGER_COUNTS,
		
		ENCODER3_POS  	  => CH1_ENCODER3_POS,
		ENCODER3_DIR  	  => CH1_ENCODER3_DIR,
		ENCODER3_RPM  	  => CH1_ENCODER3_RPM,
		ENCODER3_TRIGGER_COUNTS => CH1_ENCODER3_TRIGGER_COUNTS,
		
		-- IO
		IO_INPUTS  	  	  => IO_INPUTS_i,
		IO_OUTPUTS  	  => IO_OUTPUTS_i,
		
		-- Config output receiver
		RECEIVER_SAMPLING_FREQUENCY         => CH1_RECEIVER_SAMPLING_FREQUENCY,
        RECEIVER_DATA_WINDOW                => CH1_RECEIVER_DATA_WINDOW, 
        RECEIVER_DELAY                      => CH1_RECEIVER_DELAY,  
        RECEIVER_GAIN                       => CH1_RECEIVER_GAIN,  
        RECEIVER_EXTERNAL_MULTIPLEXER_EN    => CH1_RECEIVER_EXTERNAL_MULTIPLEXER_EN,
		
		-- Config output transmitter
		TRANSMITTER_VOLTAGE                 => CH1_TRANSMITTER_VOLTAGE, 
        TRANSMITTER_BURST_FREQUENCY         => CH1_TRANSMITTER_BURST_FREQUENCY,
        TRANSMITTER_N_CYCLES                => CH1_TRANSMITTER_N_CYCLES,
        TRANSMITTER_DELAY                   => CH1_TRANSMITTER_DELAY,
        TRANSMITTER_DELTA_DELAY             => CH1_TRANSMITTER_DELTA_DELAY,
        TRANSMITTER_DELTA_ADD               => CH1_TRANSMITTER_DELTA_ADD ,
        TRANSMITTER_DIRECTIONAL_PHASING     => CH1_TRANSMITTER_DIRECTIONAL_PHASING, 
		
		-- Config output magnet
		MAGNET_MODE                         => CH1_MAGNET_MODE,
        MAGNET_PULSE_WIDTH                  => CH1_MAGNET_PULSE_WIDTH, 
        MAGNET_INITIAL_DELAY                => CH1_MAGNET_INITIAL_DELAY,
		
		-- Condig analog filters
		DSP_ANALOG_FILTER  					=> CH1_DSP_ANALOG_FILTER,
		
		-- Config DAC curves
		CONFIG_DAC_AXIS_TREADY    => CH1_CONFIG_DAC_AXIS_TREADY,
		CONFIG_DAC_AXIS_TDATA	  => CH1_CONFIG_DAC_AXIS_TDATA,
		CONFIG_DAC_AXIS_TLAST	  => CH1_CONFIG_DAC_AXIS_TLAST,
		CONFIG_DAC_AXIS_TVALID    => CH1_CONFIG_DAC_AXIS_TVALID,
					
		-- ASCAN
		ASCAN_AXIS_TREADY  => CH1_ASCAN_AXIS_TREADY, 
		ASCAN_AXIS_TDATA   => CH1_ASCAN_AXIS_TDATA,
		ASCAN_AXIS_TLAST   => CH1_ASCAN_AXIS_TLAST,
		ASCAN_AXIS_TVALID  => CH1_ASCAN_AXIS_TVALID,
		ASCAN_AXIS_TKEEP   => CH1_ASCAN_AXIS_TKEEP
	);
 
end Behavioral;