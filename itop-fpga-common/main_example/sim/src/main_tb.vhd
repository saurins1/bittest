library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity main_tb is
  	generic (
  		N_CHANNELS		: INTEGER := 2;
  		REGISTER_W64    : INTEGER := 64;
  		REGISTER_W48    : INTEGER := 48;
  	    REGISTER_W32    : INTEGER := 32;
  	    REGISTER_W21    : INTEGER := 21;
        REGISTER_W16    : INTEGER := 16;
  		REGISTER_W14    : INTEGER := 14;
  		REGISTER_W8     : INTEGER := 8;
  		REGISTER_W6     : INTEGER := 6;
  		REGISTER_W4     : INTEGER := 4;
  		REGISTER_W2     : INTEGER := 2;
  		DISPOSITION_PASS  : INTEGER := 1;
  		DISPOSITION_FAIL  : INTEGER := 2;
  		DISPOSITION_NODET : INTEGER := 0;
        TRIGGER_WAIT    : INTEGER := 32;
        MIN_COINC_LEVEL : INTEGER := 2;
  		MIN_AVG_LEVEL   : INTEGER := 2;
  		ASCAN_HEADER_SIZE : INTEGER := 43;
        N_TAPS              : INTEGER    := 512;
        N_SETS_TAP          : INTEGER    := 3;
        OVERSAMPLING        : INTEGER    := 2;
        WAIT_CYCLES         : INTEGER    := 4
  	);
end;

architecture bench of main_tb is

  component main
  	generic (
  		N_CHANNELS		: INTEGER := 2;
  		REGISTER_W64    : INTEGER := 64;
  		REGISTER_W48    : INTEGER := 48;
  	    REGISTER_W32    : INTEGER := 32;
  	    REGISTER_W21    : INTEGER := 21;
        REGISTER_W16    : INTEGER := 16;
  		REGISTER_W14    : INTEGER := 14;
  		REGISTER_W8     : INTEGER := 8;
  		REGISTER_W6     : INTEGER := 6;
  		REGISTER_W4     : INTEGER := 4;
  		REGISTER_W2     : INTEGER := 2;
  		DISPOSITION_PASS  : INTEGER := 1;
  		DISPOSITION_FAIL  : INTEGER := 2;
  		DISPOSITION_NODET : INTEGER := 0;
        TRIGGER_WAIT    : INTEGER := 32;
        MIN_COINC_LEVEL : INTEGER := 2;
  		MIN_AVG_LEVEL   : INTEGER := 2;
  		ASCAN_HEADER_SIZE : INTEGER := 43;
        N_TAPS              : INTEGER    := 512;
        N_SETS_TAP          : INTEGER    := 3;
        OVERSAMPLING        : INTEGER    := 2;
        WAIT_CYCLES         : INTEGER    := 4
  	);
  port (
      aclk                    : in STD_LOGIC; 
      aresetn                 : in STD_LOGIC;
      CONFIGURATION_SIZE      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
      CONFIGURATION_ON        : out STD_LOGIC;
      DMA_UPLOAD_CONTROL      : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);   
      DMA_DOWNLOAD_CONTROL    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
      START_INSPECTION_CONTROL: in STD_LOGIC; 
      SCAN_MODE_CONTROL       : in STD_LOGIC; 
      CSCAN_MODE_CONTROL      : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);  
      TRIGGER_TYPE_CONTROL    : in STD_LOGIC_VECTOR(REGISTER_W6-1 downto 0);
      RESET_CONTROL           : in STD_LOGIC;
      ENABLE_CONTROL          : in STD_LOGIC;
      DMA_UPLOAD_STATUS       : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);   
      DMA_DOWNLOAD_STATUS     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
      START_INSPECTION_STATUS : out STD_LOGIC; 
      SCAN_MODE_STATUS        : out STD_LOGIC; 
      CSCAN_MODE_STATUS       : out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);  
      TRIGGER_TYPE_STATUS     : out STD_LOGIC_VECTOR(REGISTER_W6-1 downto 0);
      RESET_STATUS            : out STD_LOGIC;
      ENABLE_STATUS           : out STD_LOGIC;
      ENCODER1_DIR     : in STD_LOGIC_VECTOR (REGISTER_W2-1 downto 0);  
      ENCODER1_COUNT   : in STD_LOGIC_VECTOR (REGISTER_W32-1 downto 0);  
      ENCODER2_DIR     : in STD_LOGIC_VECTOR (REGISTER_W2-1 downto 0);  
      ENCODER2_COUNT   : in STD_LOGIC_VECTOR (REGISTER_W32-1 downto 0);
  	  IO_INPUTS   			: in  STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  	  IO_OUTPUTS  			: out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  	  CH0_RECEIVER_SAMPLING_FREQUENCY         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
      CH0_RECEIVER_DATA_WINDOW                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
      CH0_RECEIVER_DELAY                      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
      CH0_RECEIVER_GAIN                       : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
      CH0_RECEIVER_EXTERNAL_MULTIPLEXER_EN    : out STD_LOGIC;
  	  CH0_TRANSMITTER_VOLTAGE                 : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
      CH0_TRANSMITTER_BURST_FREQUENCY         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
      CH0_TRANSMITTER_N_CYCLES                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
      CH0_TRANSMITTER_DELAY                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
      CH0_TRANSMITTER_DIRECTIONAL_PHASING     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  	  CH0_MAGNET_MODE                         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
      CH0_MAGNET_PULSE_WIDTH                  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
      CH0_MAGNET_INITIAL_DELAY                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  	  CH0_DSP_ANALOG_FILTER  					: out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
  	  CH0_CONFIG_DAC_AXIS_TREADY    : in STD_LOGIC;
  	  CH0_CONFIG_DAC_AXIS_TDATA	  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  	  CH0_CONFIG_DAC_AXIS_TLAST	  : out STD_LOGIC;
  	  CH0_CONFIG_DAC_AXIS_TVALID    : out STD_LOGIC;
  	  CH0_SIGNAL_COINC_WINDOW  	  : out STD_LOGIC;
  	  CH0_SIGNAL_AVG_WINDOW  		  : out STD_LOGIC;
  	  CH0_SIGNAL_INPUT_AXIS_TREADY  : out STD_LOGIC;
  	  CH0_SIGNAL_INPUT_AXIS_TDATA	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  	  CH0_SIGNAL_INPUT_AXIS_TLAST	  : in STD_LOGIC;
  	  CH0_SIGNAL_INPUT_AXIS_TVALID  : in STD_LOGIC;
  	  CH1_RECEIVER_SAMPLING_FREQUENCY         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
      CH1_RECEIVER_DATA_WINDOW                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
      CH1_RECEIVER_DELAY                      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
      CH1_RECEIVER_GAIN                       : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
      CH1_RECEIVER_EXTERNAL_MULTIPLEXER_EN    : out STD_LOGIC;
  	  CH1_TRANSMITTER_VOLTAGE                 : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
      CH1_TRANSMITTER_BURST_FREQUENCY         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
      CH1_TRANSMITTER_N_CYCLES                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
      CH1_TRANSMITTER_DELAY                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
      CH1_TRANSMITTER_DIRECTIONAL_PHASING     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  	  CH1_MAGNET_MODE                         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
      CH1_MAGNET_PULSE_WIDTH                  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
      CH1_MAGNET_INITIAL_DELAY                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  	  CH1_DSP_ANALOG_FILTER  					: out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
  	  CH1_CONFIG_DAC_AXIS_TREADY    : in STD_LOGIC;
  	  CH1_CONFIG_DAC_AXIS_TDATA	  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  	  CH1_CONFIG_DAC_AXIS_TLAST	  : out STD_LOGIC;
  	  CH1_CONFIG_DAC_AXIS_TVALID    : out STD_LOGIC;
  	  CH1_SIGNAL_COINC_WINDOW  	  : out STD_LOGIC;
  	  CH1_SIGNAL_AVG_WINDOW  		  : out STD_LOGIC;
  	  CH1_SIGNAL_INPUT_AXIS_TREADY  : out STD_LOGIC;
  	  CH1_SIGNAL_INPUT_AXIS_TDATA	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  	  CH1_SIGNAL_INPUT_AXIS_TLAST	  : in STD_LOGIC;
  	  CH1_SIGNAL_INPUT_AXIS_TVALID  : in STD_LOGIC;
      SW_RESET                : out std_logic;
    -- DMA INPUT
      DMA_S_axis_tready       : out STD_LOGIC;
      DMA_S_axis_tkeep        : in STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0);
      DMA_S_axis_tdata        : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
      DMA_S_axis_tlast        : in STD_LOGIC;
      DMA_S_axis_tvalid       : in STD_LOGIC;
      -- DMA OUTPUT
      DMA_M_axis_tvalid       : out STD_LOGIC;
      DMA_M_axis_tdata        : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
      DMA_M_axis_tlast        : out STD_LOGIC;
      DMA_M_axis_tkeep        : out STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0);
      DMA_M_axis_tready       : in STD_LOGIC
  );
  end component;

  signal aclk: STD_LOGIC:= '0';
  signal aresetn: STD_LOGIC:= '0';
  
  signal CONFIGURATION_SIZE      : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal CONFIGURATION_ON        : STD_LOGIC;

  signal DMA_UPLOAD_CONTROL: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');
  signal DMA_DOWNLOAD_CONTROL: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');
  signal START_INSPECTION_CONTROL: STD_LOGIC:= '0';
  signal SCAN_MODE_CONTROL: STD_LOGIC:= '0';
  signal CSCAN_MODE_CONTROL: STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0):= (others => '0');
  signal TRIGGER_TYPE_CONTROL: STD_LOGIC_VECTOR(REGISTER_W6-1 downto 0):= (others => '0');
  signal RESET_CONTROL: STD_LOGIC:= '0';
  signal ENABLE_CONTROL: STD_LOGIC:= '0';

  signal DMA_UPLOAD_STATUS: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal DMA_DOWNLOAD_STATUS: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal START_INSPECTION_STATUS: STD_LOGIC;
  signal SCAN_MODE_STATUS: STD_LOGIC;
  signal CSCAN_MODE_STATUS: STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
  signal TRIGGER_TYPE_STATUS: STD_LOGIC_VECTOR(REGISTER_W6-1 downto 0);
  signal RESET_STATUS: STD_LOGIC;
  signal ENABLE_STATUS: STD_LOGIC;

  signal ENCODER1_DIR: STD_LOGIC_VECTOR (REGISTER_W2-1 downto 0):= (others => '0');
  signal ENCODER1_COUNT: STD_LOGIC_VECTOR (REGISTER_W32-1 downto 0):= (others => '0');
  signal ENCODER2_DIR: STD_LOGIC_VECTOR (REGISTER_W2-1 downto 0):= (others => '0');
  signal ENCODER2_COUNT: STD_LOGIC_VECTOR (REGISTER_W32-1 downto 0):= (others => '0');

  signal IO_INPUTS: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0):= (others => '0');
  signal IO_OUTPUTS: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);

  signal CH0_RECEIVER_SAMPLING_FREQUENCY: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal CH0_RECEIVER_DATA_WINDOW: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal CH0_RECEIVER_DELAY: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal CH0_RECEIVER_GAIN: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal CH0_RECEIVER_EXTERNAL_MULTIPLEXER_EN: STD_LOGIC;
  signal CH0_TRANSMITTER_VOLTAGE: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal CH0_TRANSMITTER_BURST_FREQUENCY: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal CH0_TRANSMITTER_N_CYCLES: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal CH0_TRANSMITTER_DELAY: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal CH0_TRANSMITTER_DIRECTIONAL_PHASING: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal CH0_MAGNET_MODE: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal CH0_MAGNET_PULSE_WIDTH: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal CH0_MAGNET_INITIAL_DELAY: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal CH0_DSP_ANALOG_FILTER: STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
  signal CH0_CONFIG_DAC_AXIS_TREADY: STD_LOGIC;
  signal CH0_CONFIG_DAC_AXIS_TDATA: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal CH0_CONFIG_DAC_AXIS_TLAST: STD_LOGIC;
  signal CH0_CONFIG_DAC_AXIS_TVALID: STD_LOGIC;
  signal CH0_SIGNAL_COINC_WINDOW: STD_LOGIC;
  signal CH0_SIGNAL_AVG_WINDOW: STD_LOGIC;

  signal CH0_SIGNAL_INPUT_AXIS_TREADY: STD_LOGIC:= '0';
  signal CH0_SIGNAL_INPUT_AXIS_TDATA: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  signal CH0_SIGNAL_INPUT_AXIS_TLAST: STD_LOGIC;
  signal CH0_SIGNAL_INPUT_AXIS_TVALID: STD_LOGIC;

  signal CH1_RECEIVER_SAMPLING_FREQUENCY: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal CH1_RECEIVER_DATA_WINDOW: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal CH1_RECEIVER_DELAY: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal CH1_RECEIVER_GAIN: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal CH1_RECEIVER_EXTERNAL_MULTIPLEXER_EN: STD_LOGIC;
  signal CH1_TRANSMITTER_VOLTAGE: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal CH1_TRANSMITTER_BURST_FREQUENCY: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal CH1_TRANSMITTER_N_CYCLES: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal CH1_TRANSMITTER_DELAY: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal CH1_TRANSMITTER_DIRECTIONAL_PHASING: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal CH1_MAGNET_MODE: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal CH1_MAGNET_PULSE_WIDTH: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal CH1_MAGNET_INITIAL_DELAY: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal CH1_DSP_ANALOG_FILTER: STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
  signal CH1_CONFIG_DAC_AXIS_TREADY: STD_LOGIC;
  signal CH1_CONFIG_DAC_AXIS_TDATA: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
  signal CH1_CONFIG_DAC_AXIS_TLAST: STD_LOGIC;
  signal CH1_CONFIG_DAC_AXIS_TVALID: STD_LOGIC;
  signal CH1_SIGNAL_COINC_WINDOW: STD_LOGIC;
  signal CH1_SIGNAL_AVG_WINDOW: STD_LOGIC;

  signal CH1_SIGNAL_INPUT_AXIS_TREADY: STD_LOGIC:= '0';
  signal CH1_SIGNAL_INPUT_AXIS_TDATA: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
  signal CH1_SIGNAL_INPUT_AXIS_TLAST: STD_LOGIC;
  signal CH1_SIGNAL_INPUT_AXIS_TVALID: STD_LOGIC;

  signal SW_RESET: std_logic;

  signal DMA_S_axis_tready: std_logic;
  signal DMA_S_axis_tdata: std_logic_vector(REGISTER_W32-1 downto 0):= (others => '0');
  signal DMA_S_axis_tlast: std_logic:= '0';
  signal DMA_S_axis_tvalid: std_logic:= '0';
  signal DMA_S_axis_tkeep: STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0):= (others => '0');

  signal DMA_M_axis_tvalid: std_logic;
  signal DMA_M_axis_tdata: std_logic_vector(REGISTER_W32-1 downto 0);
  signal DMA_M_axis_tlast: std_logic;
  signal DMA_M_axis_tready: std_logic:= '0';
  signal DMA_M_axis_tkeep: STD_LOGIC_VECTOR((REGISTER_W32/8)-1 downto 0);

  constant aclk_period: time := 10 ns;

    -- UPLOAD
  signal dma_upload_control_ascan_request     : std_logic;
  signal dma_upload_control_cscan_request     : std_logic;
  signal dma_upload_control_config_request    : std_logic;
  signal dma_upload_control_capability_request: std_logic;
  signal dma_upload_control_ascan_availability: std_logic;
  signal dma_upload_control_cscan_availability: std_logic;
  signal dma_upload_control_scan_ready        : std_logic;
  signal dma_upload_control_config_ready      : std_logic;
  signal dma_upload_control_capability_ready  : std_logic;
  signal dma_upload_control_data_size         : std_logic_vector(REGISTER_W21-1 downto 0);
        
  signal dma_upload_status_ascan_request      : std_logic;
  signal dma_upload_status_cscan_request      : std_logic;
  signal dma_upload_status_config_request     : std_logic;
  signal dma_upload_status_capability_request : std_logic;
  signal dma_upload_status_ascan_availability : std_logic;
  signal dma_upload_status_cscan_availability : std_logic;
  signal dma_upload_status_scan_ready         : std_logic;
  signal dma_upload_status_config_ready       : std_logic;
  signal dma_upload_status_capability_ready   : std_logic;
  signal dma_upload_status_data_size          : std_logic_vector(REGISTER_W21-1 downto 0);
           
  -- DOWNLOAD
  signal dma_download_control_config_request      : std_logic;
  signal dma_download_control_ascan_request       : std_logic;
  signal dma_download_control_data_size           : std_logic_vector(REGISTER_W21-1 downto 0);
        
  signal dma_download_status_config_request       : std_logic;
  signal dma_download_status_ascan_request        : std_logic;
  signal dma_download_status_data_size            : std_logic_vector(REGISTER_W21-1 downto 0);

  type states_config_sm is (idle, config_ready, open_file, load_config, load_config_wait, close_file); 
  signal state_config_sm : states_config_sm; 

  signal load_config_flag: STD_LOGIC:= '0'; 

  type states_signal_sm is (idle, open_file, load_signal, load_signal_wait, close_file); 
  signal state_signal_sm_ch0 : states_signal_sm; 
  signal state_signal_sm_ch1 : states_signal_sm;

  signal load_signal_flag_ch0: STD_LOGIC:= '0';
  signal load_signal_flag_ch1: STD_LOGIC:= '0';

  signal trigger_type: STD_LOGIC:= '0';

  type states_general_sm is (idle, initial_config, start_inspection_on, get_ascan_1, get_ascan_2,get_ascan_3); 
  signal state_general_sm : states_general_sm; 
  
  signal start_general_sm: STD_LOGIC:= '0';
  
  signal ascan_counter: UNSIGNED(REGISTER_W32-1 downto 0);
  signal CH0_START_STOP_INSPECTION : STD_LOGIC:= '0';
  signal CH1_START_STOP_INSPECTION : STD_LOGIC:= '0';
  
  signal config_sample: std_logic_vector(REGISTER_W32-1 downto 0);
  signal config_counter: UNSIGNED(REGISTER_W32-1 downto 0);
  
  signal config_encoder_offset: std_logic_vector(REGISTER_W32-1 downto 0);

begin

    tready_inst: process(aclk, aresetn)
        variable var_counter : integer range 0 to 65536 :=0;
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                DMA_M_axis_tready <= '0';
                var_counter := 0;
            else
                if(var_counter >= 8) then
                    DMA_M_axis_tready <= not DMA_M_axis_tready;
                    var_counter := 0;
                else
                    var_counter := var_counter + 1;
                end if;
            end if;
        end if;
    end process;

    config_counter_inst: process(aclk, aresetn) 
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                config_counter <= (others => '0'); 
            else
                if(DMA_S_axis_tvalid = '1') then
                    config_counter <= config_counter + 1;
                else
                    config_counter <= (others => '0');
                end if;
            end if;
        end if;
    end process;
	
	-- aclk process 
    aclk_process :process
    begin
        aclk <= not aclk;
        wait for aclk_period/2;
        aclk <= not aclk;
        wait for aclk_period/2;
    end process;
			
	stimulus: process
	begin
		aresetn <= '0';
		wait for aclk_period*10;
		aresetn <= '1';
		wait for aclk_period*10;
		start_general_sm <= '1';
		wait for aclk_period*10;
		start_general_sm <= '0';
		wait;
	end process;

    trigger_inst: process(aclk, aresetn) 
        variable var_counter : integer range 0 to 65536 :=0;
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                ENCODER1_COUNT <= (others => '0'); 
				ENCODER2_COUNT <= (others => '0');  
                var_counter := 0;  
            else
                if(trigger_type /= '0') then
                    if(var_counter >= 2) then
                        ENCODER1_COUNT <= std_logic_vector(signed(ENCODER1_COUNT)-1); 
                        --ENCODER1_COUNT <= std_logic_vector(signed(ENCODER1_COUNT)+1); 
                        var_counter := 0;
                    else    
                        var_counter := var_counter + 1;
                    end if;  
                else
                    ENCODER1_COUNT <= (others => '0');  
                    var_counter := 0;  
                end if;
            end if;
        end if;
    end process;
			
	IO_INPUTS <= "000000000000000" & CH0_START_STOP_INSPECTION;

	-- General SM
    general_sm_inst: process(aclk, aresetn)  
        variable my_counter  : integer range 0 to 16383 :=0;    
    begin 
        if (aresetn = '0') then
			-- config
			START_INSPECTION_CONTROL <= '0';
			TRIGGER_TYPE_CONTROL <= (others => '0');
			SCAN_MODE_CONTROL <= '0';
			CSCAN_MODE_CONTROL <= (others => '0');
			RESET_CONTROL <= '0';
			ENABLE_CONTROL <= '0';
			CH0_START_STOP_INSPECTION <= '0';
			CH1_START_STOP_INSPECTION <= '0';
			trigger_type <= '0';
			dma_upload_control_ascan_request <= '0';
		    dma_upload_control_data_size <= (others => '0');
		    dma_upload_control_capability_ready <= '0';
		    dma_upload_control_config_ready <= '0';
		    dma_upload_control_scan_ready <= '0';
		    dma_upload_control_cscan_availability <= '0';
		    dma_upload_control_ascan_availability <= '0';
		    dma_upload_control_capability_request <= '0';
		    dma_upload_control_config_request <= '0';
		    dma_upload_control_cscan_request <= '0';
			load_signal_flag_ch0 <= '0';
            load_signal_flag_ch1 <= '1';
			-- state																		  	
            state_general_sm <= idle;  
        elsif (aclk'event and aclk = '1') then
            case state_general_sm is  
                when idle =>     
					--trigger_type <= '1';	
					trigger_type <= '0';										 
                    if(start_general_sm = '1') then
                        my_counter := 0;
                        state_general_sm <= initial_config;        
                    end if;
                when initial_config => 
                    load_config_flag <= '1';
					if(DMA_S_axis_tlast ='1') then
						state_general_sm <= start_inspection_on;													 
					end if;												 
                when start_inspection_on =>
                    CH0_START_STOP_INSPECTION <= '1';
					START_INSPECTION_CONTROL <= '1';
					load_config_flag <= '0';															 
					if(my_counter >= 16) then
						load_signal_flag_ch0 <= '1';
						load_signal_flag_ch1 <= '1';
						state_general_sm <= get_ascan_1;										 
					else
						my_counter := my_counter + 1;											 
					end if;												 
				when get_ascan_1 =>										  
					if(dma_upload_status_scan_ready = '1') then
						dma_upload_control_ascan_request <= '1';
						dma_upload_control_data_size <= dma_upload_status_data_size;														
						state_general_sm <= get_ascan_2;											 
					end if;												 
                when get_ascan_2 =>												 
					if(DMA_M_axis_tlast = '1') then
						dma_upload_control_ascan_request <= '0';
						CH0_START_STOP_INSPECTION <= '0';		
						state_general_sm <= get_ascan_3;									 
					end if;
				when get_ascan_3 =>
					if(dma_upload_status_ascan_request = '0') then															
						state_general_sm <= start_inspection_on;
					end if;
                when others =>
                    null;                   
            end case;                                                                                            
        end if;
    end process; 
    
    dma_upload_status_ascan_request <= DMA_UPLOAD_STATUS(0);
    dma_upload_status_cscan_request <= DMA_UPLOAD_STATUS(1);
    dma_upload_status_config_request <= DMA_UPLOAD_STATUS(2);
    dma_upload_status_capability_request <= DMA_UPLOAD_STATUS(3);
    dma_upload_status_ascan_availability <= DMA_UPLOAD_STATUS(4);
    dma_upload_status_Cscan_availability <= DMA_UPLOAD_STATUS(5);
    dma_upload_status_scan_ready <= DMA_UPLOAD_STATUS(6);
    dma_upload_status_config_ready <= DMA_UPLOAD_STATUS(7);
    dma_upload_status_capability_ready <= DMA_UPLOAD_STATUS(8);
    dma_upload_status_data_size <= DMA_UPLOAD_STATUS(31 downto 11);
    
    dma_download_status_config_request <= DMA_DOWNLOAD_STATUS(0);
    dma_download_status_ascan_request <= DMA_DOWNLOAD_STATUS(1);
    dma_download_status_data_size <= DMA_DOWNLOAD_STATUS(31 downto 11);  
																			
    DMA_UPLOAD_CONTROL <= dma_upload_control_data_size & "00" & dma_upload_control_capability_ready & dma_upload_control_config_ready & 
        dma_upload_control_scan_ready & dma_upload_control_cscan_availability & dma_upload_control_ascan_availability & 
        dma_upload_control_capability_request &  dma_upload_control_config_request & dma_upload_control_cscan_request & dma_upload_control_ascan_request;
																			
	DMA_DOWNLOAD_CONTROL <= dma_download_control_data_size & "000000000" & dma_download_control_ascan_request & dma_download_control_config_request;																		

	-- Configuration SM
    configuration_input_inst: process(aclk, aresetn)  
        variable my_counter  : integer range 0 to 16383 :=0;    

        type configDataFile is file of integer;
        file config_data_file: configDataFile;
        variable config_data: integer;     
        variable config_data_file_status: file_open_status;
    begin 
        if (aresetn = '0') then
			-- config_input
			DMA_S_axis_tdata <= (others => '0');
			DMA_S_axis_tkeep <= (others => '0');
			DMA_S_axis_tlast <= '0';
			DMA_S_axis_tvalid <= '0';
			-- config params
			dma_download_control_config_request <= '0';
			dma_download_control_data_size <= (others => '0');
			dma_download_control_ascan_request <= '0';
			-- state																		  	
            state_config_sm <= idle;  
        elsif (aclk'event and aclk = '1') then
            case state_config_sm is  
                when idle =>         
                    if(load_config_flag = '1') then
                        my_counter := 0;
                        state_config_sm <= config_ready;        
                    end if;
				when config_ready =>
					if(dma_download_status_config_request = '1') then
						state_config_sm <= open_file; 	
					else
						dma_download_control_config_request <= '1';
						dma_download_control_data_size <= std_logic_vector(to_unsigned(6, dma_download_control_data_size'length) + 
																		   to_unsigned(1618, dma_download_control_data_size'length)+
																		   to_unsigned(1618, dma_download_control_data_size'length)+
																		   to_unsigned(10, dma_download_control_data_size'length));	
					end if;
                when open_file =>
					if(my_counter >= 128) then
						file_open(config_data_file_status, config_data_file, "configuration.dat", read_mode);
						my_counter := 0; 
						config_encoder_offset <= (others => '1');
						state_config_sm <= load_config;  
					else
						my_counter:= my_counter + 1;	
					end if;	
                when load_config =>
                    if(DMA_S_axis_tready= '1') then
						-- tdata
						if not endfile(config_data_file) then
                            read (config_data_file, config_data);
                        end if; 
                     	if not endfile(config_data_file) then
							if(my_counter = (unsigned(config_encoder_offset)) ) then
								if(trigger_type = '1') then														   
									DMA_S_axis_tdata <= std_logic_vector(to_unsigned(1, DMA_S_axis_tdata'length));
								else															 
									DMA_S_axis_tdata <= std_logic_vector(to_unsigned(0, DMA_S_axis_tdata'length));
								end if;																	 
							else													   
                            	DMA_S_axis_tdata<=std_logic_vector(to_signed(config_data, DMA_S_axis_tdata'length));
							end if;
							config_sample<=std_logic_vector(to_signed(config_data, config_sample'length));
                        end if;
                        if(my_counter = 4 ) then
                            config_encoder_offset <=std_logic_vector(to_signed(config_data, config_encoder_offset'length));
                        end if;
						if not endfile(config_data_file) then
                            read (config_data_file, config_data);
                        end if;
						-- tvalid
						DMA_S_axis_tvalid <= '1';
						DMA_S_axis_tkeep <= (others => '1');	
						-- tlast
						if(my_counter >= unsigned(dma_download_control_data_size)-1) then
							my_counter := 0; 	
							DMA_S_axis_tlast <= '1';											 
							state_config_sm <= load_config_wait;											  
						else
							-- counter										  
							my_counter := my_counter + 1;										  
						end if; 
					else
				        DMA_S_axis_tkeep <= (others => '0');
                    end if;																																																														
				when load_config_wait =>										  
					DMA_S_axis_tdata <= (others => '0');
					DMA_S_axis_tkeep <= (others => '0');
					DMA_S_axis_tlast <= '0';
					DMA_S_axis_tvalid <= '0';
					dma_download_control_config_request <= '0';													  
					if(my_counter >= 16) then
						if(dma_download_status_config_request = '0') then	
							my_counter := 0;											 
							state_config_sm <= close_file;	
						end if;
					else
						my_counter := my_counter + 1;										  
					end if;														  
                when close_file =>
                    if(my_counter >= 32) then
						if(load_config_flag = '0') then													
							file_close(config_data_file);
							state_config_sm <= idle;
						end if;														
                    else
                        my_counter := my_counter + 1;
                    end if;   
                when others =>
                    null;                   
            end case;                                                                                            
        end if;
    end process;
																																											 
	-- Signal ch0 SM
    signal_ch0_input_inst: process(aclk, aresetn)  
        variable my_counter  : integer range 0 to 16383 :=0;    

        type signalDataFile is file of integer;
        file signal_data_file: signalDataFile;
        variable signal_data: integer;     
        variable signal_data_file_status: file_open_status;
    begin 
        if (aresetn = '0') then
			-- signal input
			CH0_SIGNAL_INPUT_AXIS_TDATA <= (others => '0');
			CH0_SIGNAL_INPUT_AXIS_TLAST <= '0';
			CH0_SIGNAL_INPUT_AXIS_TVALID <= '0';
			-- state																		  	
            state_signal_sm_ch0 <= idle;  
        elsif (aclk'event and aclk = '1') then
            case state_signal_sm_ch0 is  
                when idle =>         
                    if(load_signal_flag_ch0 = '1') then
                        my_counter := 0;
                        state_signal_sm_ch0 <= open_file;        
                    end if;
                when open_file => 
                    file_open(signal_data_file_status, signal_data_file, "ascan.dat", read_mode);
                   	my_counter := 0; 
                    state_signal_sm_ch0 <= load_signal;  													
                when load_signal =>
                    if(CH0_SIGNAL_INPUT_AXIS_TREADY = '1') then
						-- tdata
                     	if not endfile(signal_data_file) then
                        	read (signal_data_file, signal_data);
                            CH0_SIGNAL_INPUT_AXIS_TDATA<=std_logic_vector(to_signed(signal_data, CH0_SIGNAL_INPUT_AXIS_TDATA'length));
                        end if;
						-- tvalid																										
						CH0_SIGNAL_INPUT_AXIS_TVALID <= '1';	
						if not endfile(signal_data_file) then
                            read (signal_data_file, signal_data);
                        end if;	
						-- tlast											  
						if(my_counter >= 4999) then
							CH0_SIGNAL_INPUT_AXIS_TLAST <= '1';
							my_counter := 0; 										  
							state_signal_sm_ch0 <= load_signal_wait;											  
						else
							-- counter										  
							my_counter := my_counter + 1;										  
						end if;  
                    end if;																																																												
				when load_signal_wait =>										  
					CH0_SIGNAL_INPUT_AXIS_TDATA <= (others => '0');
					CH0_SIGNAL_INPUT_AXIS_TLAST <= '0';
					CH0_SIGNAL_INPUT_AXIS_TVALID <= '0';													  
					if(my_counter >= 16) then
						my_counter := 0; 
						state_signal_sm_ch0 <= close_file;													
					else
						my_counter := my_counter + 1;										  
					end if;														  
                when close_file =>
                    if(my_counter >= 32) then
						--if(load_config_flag = '0') then													
							file_close(signal_data_file);
							state_signal_sm_ch0 <= open_file;
						--end if;														
                    else
                        my_counter := my_counter + 1;
                    end if;   
                when others =>
                    null;                   
            end case;                                                                                            
        end if;
    end process;	
																					
	-- Signal ch1 SM
    signal_ch1_input_inst: process(aclk, aresetn)  
        variable my_counter  : integer range 0 to 16383 :=0;    

        type signalDataFile is file of integer;
        file signal_data_file: signalDataFile;
        variable signal_data: integer;     
        variable signal_data_file_status: file_open_status;
    begin 
        if (aresetn = '0') then
			-- signal input
			CH1_SIGNAL_INPUT_AXIS_TDATA <= (others => '0');
			CH1_SIGNAL_INPUT_AXIS_TLAST <= '0';
			CH1_SIGNAL_INPUT_AXIS_TVALID <= '0';
			-- state																		  	
            state_signal_sm_ch1 <= idle;  
        elsif (aclk'event and aclk = '1') then
            case state_signal_sm_ch1 is  
                when idle =>         
                    if(load_signal_flag_ch1 = '1') then
                        my_counter := 0;
                        state_signal_sm_ch1 <= open_file;        
                    end if;
                when open_file => 
                    file_open(signal_data_file_status, signal_data_file, "ascan.dat", read_mode);
                   	my_counter := 0; 
                    state_signal_sm_ch1 <= load_signal;  													
                when load_signal =>
                    if(CH1_SIGNAL_INPUT_AXIS_TREADY = '1') then
						-- tdata
                     	if not endfile(signal_data_file) then
                        	read (signal_data_file, signal_data);
                            CH1_SIGNAL_INPUT_AXIS_TDATA<=std_logic_vector(to_signed(signal_data, CH0_SIGNAL_INPUT_AXIS_TDATA'length));
                        end if;
						-- tvalid																										
						CH1_SIGNAL_INPUT_AXIS_TVALID <= '1';	
						if not endfile(signal_data_file) then
                            read (signal_data_file, signal_data);
                        end if;	
						-- tlast											  
						if(my_counter >= 2499) then
							CH1_SIGNAL_INPUT_AXIS_TLAST <= '1';
							my_counter := 0; 										  
							state_signal_sm_ch1 <= load_signal_wait;											  
						else
							-- counter										  
							my_counter := my_counter + 1;										  
						end if;  
                    end if;																																																												
				when load_signal_wait =>										  
					CH1_SIGNAL_INPUT_AXIS_TDATA <= (others => '0');
					CH1_SIGNAL_INPUT_AXIS_TLAST <= '0';
					CH1_SIGNAL_INPUT_AXIS_TVALID <= '0';													  
					if(my_counter >= 16) then
						my_counter := 0; 
						state_signal_sm_ch1 <= close_file;													
					else
						my_counter := my_counter + 1;										  
					end if;														  
                when close_file =>
                    if(my_counter >= 32) then
						--if(load_config_flag = '0') then													
							file_close(signal_data_file);
							state_signal_sm_ch1 <= open_file;
						--end if;														
                    else
                        my_counter := my_counter + 1;
                    end if;   
                when others =>
                    null;                   
            end case;                                                                                            
        end if;
    end process;																	

  -- Insert values for generic parameters !!
  uut: main generic map ( N_CHANNELS                           => N_CHANNELS,
                          REGISTER_W64                         => REGISTER_W64,
                          REGISTER_W48                         => REGISTER_W48,
                          REGISTER_W32                         => REGISTER_W32,
                          REGISTER_W21                         => REGISTER_W21,
                          REGISTER_W16                         => REGISTER_W16,
                          REGISTER_W14                         => REGISTER_W14,
                          REGISTER_W8                          => REGISTER_W8,
                          REGISTER_W6                          => REGISTER_W6,
                          REGISTER_W4                          => REGISTER_W4,
                          REGISTER_W2                          => REGISTER_W2,
                          DISPOSITION_PASS                     => DISPOSITION_PASS,
                          DISPOSITION_FAIL                     => DISPOSITION_FAIL,
                          DISPOSITION_NODET                    => DISPOSITION_NODET,
                          TRIGGER_WAIT                         => TRIGGER_WAIT,
                          MIN_COINC_LEVEL                      => MIN_COINC_LEVEL,
                          MIN_AVG_LEVEL                        => MIN_AVG_LEVEL,
                          ASCAN_HEADER_SIZE                    => ASCAN_HEADER_SIZE,
                          N_TAPS                               => N_TAPS,
                          N_SETS_TAP                           => N_SETS_TAP,
                          OVERSAMPLING                         => OVERSAMPLING,
                          WAIT_CYCLES                          => WAIT_CYCLES )
               port map ( aclk                                 => aclk,
                          aresetn                              => aresetn,
                          CONFIGURATION_SIZE                   => CONFIGURATION_SIZE,
                          CONFIGURATION_ON                     => CONFIGURATION_ON,
                          DMA_UPLOAD_CONTROL                   => DMA_UPLOAD_CONTROL,
                          DMA_DOWNLOAD_CONTROL                 => DMA_DOWNLOAD_CONTROL,
                          START_INSPECTION_CONTROL             => START_INSPECTION_CONTROL,
                          SCAN_MODE_CONTROL                    => SCAN_MODE_CONTROL,
                          CSCAN_MODE_CONTROL                   => CSCAN_MODE_CONTROL,
                          TRIGGER_TYPE_CONTROL                 => TRIGGER_TYPE_CONTROL,
                          RESET_CONTROL                        => RESET_CONTROL,
                          ENABLE_CONTROL                       => ENABLE_CONTROL,
                          DMA_UPLOAD_STATUS                    => DMA_UPLOAD_STATUS,
                          DMA_DOWNLOAD_STATUS                  => DMA_DOWNLOAD_STATUS,
                          START_INSPECTION_STATUS              => START_INSPECTION_STATUS,
                          SCAN_MODE_STATUS                     => SCAN_MODE_STATUS,
                          CSCAN_MODE_STATUS                    => CSCAN_MODE_STATUS,
                          TRIGGER_TYPE_STATUS                  => TRIGGER_TYPE_STATUS,
                          RESET_STATUS                         => RESET_STATUS,
                          ENABLE_STATUS                        => ENABLE_STATUS,
                          ENCODER1_DIR                         => ENCODER1_DIR,
                          ENCODER1_COUNT                       => ENCODER1_COUNT,
                          ENCODER2_DIR                         => ENCODER2_DIR,
                          ENCODER2_COUNT                       => ENCODER2_COUNT,
                          IO_INPUTS                            => IO_INPUTS,
                          IO_OUTPUTS                           => IO_OUTPUTS,
                          CH0_RECEIVER_SAMPLING_FREQUENCY      => CH0_RECEIVER_SAMPLING_FREQUENCY,
                          CH0_RECEIVER_DATA_WINDOW             => CH0_RECEIVER_DATA_WINDOW,
                          CH0_RECEIVER_DELAY                   => CH0_RECEIVER_DELAY,
                          CH0_RECEIVER_GAIN                    => CH0_RECEIVER_GAIN,
                          CH0_RECEIVER_EXTERNAL_MULTIPLEXER_EN => CH0_RECEIVER_EXTERNAL_MULTIPLEXER_EN,
                          CH0_TRANSMITTER_VOLTAGE              => CH0_TRANSMITTER_VOLTAGE,
                          CH0_TRANSMITTER_BURST_FREQUENCY      => CH0_TRANSMITTER_BURST_FREQUENCY,
                          CH0_TRANSMITTER_N_CYCLES             => CH0_TRANSMITTER_N_CYCLES,
                          CH0_TRANSMITTER_DELAY                => CH0_TRANSMITTER_DELAY,
                          CH0_TRANSMITTER_DIRECTIONAL_PHASING  => CH0_TRANSMITTER_DIRECTIONAL_PHASING,
                          CH0_MAGNET_MODE                      => CH0_MAGNET_MODE,
                          CH0_MAGNET_PULSE_WIDTH               => CH0_MAGNET_PULSE_WIDTH,
                          CH0_MAGNET_INITIAL_DELAY             => CH0_MAGNET_INITIAL_DELAY,
                          CH0_DSP_ANALOG_FILTER                => CH0_DSP_ANALOG_FILTER,
                          CH0_CONFIG_DAC_AXIS_TREADY           => CH0_CONFIG_DAC_AXIS_TREADY,
                          CH0_CONFIG_DAC_AXIS_TDATA            => CH0_CONFIG_DAC_AXIS_TDATA,
                          CH0_CONFIG_DAC_AXIS_TLAST            => CH0_CONFIG_DAC_AXIS_TLAST,
                          CH0_CONFIG_DAC_AXIS_TVALID           => CH0_CONFIG_DAC_AXIS_TVALID,
                          CH0_SIGNAL_COINC_WINDOW              => CH0_SIGNAL_COINC_WINDOW,
                          CH0_SIGNAL_AVG_WINDOW                => CH0_SIGNAL_AVG_WINDOW,
                          CH0_SIGNAL_INPUT_AXIS_TREADY         => CH0_SIGNAL_INPUT_AXIS_TREADY,
                          CH0_SIGNAL_INPUT_AXIS_TDATA          => CH0_SIGNAL_INPUT_AXIS_TDATA,
                          CH0_SIGNAL_INPUT_AXIS_TLAST          => CH0_SIGNAL_INPUT_AXIS_TLAST,
                          CH0_SIGNAL_INPUT_AXIS_TVALID         => CH0_SIGNAL_INPUT_AXIS_TVALID,
                          CH1_RECEIVER_SAMPLING_FREQUENCY      => CH1_RECEIVER_SAMPLING_FREQUENCY,
                          CH1_RECEIVER_DATA_WINDOW             => CH1_RECEIVER_DATA_WINDOW,
                          CH1_RECEIVER_DELAY                   => CH1_RECEIVER_DELAY,
                          CH1_RECEIVER_GAIN                    => CH1_RECEIVER_GAIN,
                          CH1_RECEIVER_EXTERNAL_MULTIPLEXER_EN => CH1_RECEIVER_EXTERNAL_MULTIPLEXER_EN,
                          CH1_TRANSMITTER_VOLTAGE              => CH1_TRANSMITTER_VOLTAGE,
                          CH1_TRANSMITTER_BURST_FREQUENCY      => CH1_TRANSMITTER_BURST_FREQUENCY,
                          CH1_TRANSMITTER_N_CYCLES             => CH1_TRANSMITTER_N_CYCLES,
                          CH1_TRANSMITTER_DELAY                => CH1_TRANSMITTER_DELAY,
                          CH1_TRANSMITTER_DIRECTIONAL_PHASING  => CH1_TRANSMITTER_DIRECTIONAL_PHASING,
                          CH1_MAGNET_MODE                      => CH1_MAGNET_MODE,
                          CH1_MAGNET_PULSE_WIDTH               => CH1_MAGNET_PULSE_WIDTH,
                          CH1_MAGNET_INITIAL_DELAY             => CH1_MAGNET_INITIAL_DELAY,
                          CH1_DSP_ANALOG_FILTER                => CH1_DSP_ANALOG_FILTER,
                          CH1_CONFIG_DAC_AXIS_TREADY           => CH1_CONFIG_DAC_AXIS_TREADY,
                          CH1_CONFIG_DAC_AXIS_TDATA            => CH1_CONFIG_DAC_AXIS_TDATA,
                          CH1_CONFIG_DAC_AXIS_TLAST            => CH1_CONFIG_DAC_AXIS_TLAST,
                          CH1_CONFIG_DAC_AXIS_TVALID           => CH1_CONFIG_DAC_AXIS_TVALID,
                          CH1_SIGNAL_COINC_WINDOW              => CH1_SIGNAL_COINC_WINDOW,
                          CH1_SIGNAL_AVG_WINDOW                => CH1_SIGNAL_AVG_WINDOW,
                          CH1_SIGNAL_INPUT_AXIS_TREADY         => CH1_SIGNAL_INPUT_AXIS_TREADY,
                          CH1_SIGNAL_INPUT_AXIS_TDATA          => CH1_SIGNAL_INPUT_AXIS_TDATA,
                          CH1_SIGNAL_INPUT_AXIS_TLAST          => CH1_SIGNAL_INPUT_AXIS_TLAST,
                          CH1_SIGNAL_INPUT_AXIS_TVALID         => CH1_SIGNAL_INPUT_AXIS_TVALID,
                          SW_RESET                             => SW_RESET,
                          DMA_S_axis_tready                    => DMA_S_axis_tready,
                          DMA_S_axis_tdata                     => DMA_S_axis_tdata,
                          DMA_S_axis_tlast                     => DMA_S_axis_tlast,
                          DMA_S_axis_tvalid                    => DMA_S_axis_tvalid,
                          DMA_S_axis_tkeep                     => DMA_S_axis_tkeep,
                          DMA_M_axis_tvalid                    => DMA_M_axis_tvalid,
                          DMA_M_axis_tdata                     => DMA_M_axis_tdata,
                          DMA_M_axis_tlast                     => DMA_M_axis_tlast,
                          DMA_M_axis_tready                    => DMA_M_axis_tready,
                          DMA_M_axis_tkeep                     => DMA_M_axis_tkeep );


end;