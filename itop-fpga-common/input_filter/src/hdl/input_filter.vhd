----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 12/04/2019 10:07:23 AM
-- Design Name: 
-- Module Name: input_filter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity input_filter is
	generic (
		REGISTER_W32     : INTEGER := 32
	);
	port (
	    -- Sync
	    aclk 	: in STD_LOGIC;
        aresetn	: in STD_LOGIC;
        
		FILTER_ENABLE   : in STD_LOGIC;
		
		FILTER_PERCENT_WINDOW  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);			
		FILTER_PERCENT_COUNTS  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		FILTER_PERCENT_RESET   : in STD_LOGIC;
			
		PULSE_QUALIFIER        : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		PULSE_QUALIFIER_RESET  : in STD_LOGIC;
		
		DEBOUNCE        : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);	
		DEBOUNCE_RESET  : in STD_LOGIC;
		
        INPUT  : in STD_LOGIC;
        INPUT_FILTERED : out STD_LOGIC       
	);
end input_filter;

architecture arch_imp of input_filter is
	
	signal filter_percent_counter      : UNSIGNED(REGISTER_W32-1 downto 0);
	signal filter_percent_counter_one  : UNSIGNED(REGISTER_W32-1 downto 0);	
	signal filter_percent_counter_zero : UNSIGNED(REGISTER_W32-1 downto 0);	
	signal input_filter_percent : STD_LOGIC;
	signal input_filter_percent_d1 : STD_LOGIC;
	
	signal pulse_qualifier_counter  : UNSIGNED(REGISTER_W32-1 downto 0);
	signal input_pulse_qualifier : STD_LOGIC;
	signal input_pulse_qualifier_d1 : STD_LOGIC;
	
	signal debounce_counter  : UNSIGNED(REGISTER_W32-1 downto 0);
	signal input_debounce : STD_LOGIC;
	       
begin

	result_process: process(aclk)
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                INPUT_FILTERED <= '0';
            else
                if(FILTER_ENABLE = '1') then
                    INPUT_FILTERED <= input_debounce; 
                else
                    INPUT_FILTERED <= INPUT;
                end if;                                              
            end if;
        end if;
    end process;

	delay_process: process(aclk)
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                input_filter_percent_d1 <= '0';
                input_pulse_qualifier_d1 <= '0';
            else
                input_filter_percent_d1 <= input_filter_percent;     
                input_pulse_qualifier_d1 <= input_pulse_qualifier;                                                
            end if;
        end if;
    end process;
    
	filter_percent_process: process(aclk)
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                input_filter_percent <= '0';
                filter_percent_counter <= (others => '0');
                filter_percent_counter_one <= (others => '0');
                filter_percent_counter_zero <= (others => '0');
            else
                if(unsigned(FILTER_PERCENT_WINDOW) > 0 and unsigned(FILTER_PERCENT_COUNTS) > 0 
                   and FILTER_PERCENT_RESET = '0') then
                    if(filter_percent_counter >= unsigned(FILTER_PERCENT_WINDOW)) then
                        if(INPUT = '1') then         
                            if(filter_percent_counter_one < unsigned(FILTER_PERCENT_WINDOW)) then                 
                                filter_percent_counter_one <= filter_percent_counter_one + 1;
                            end if;
                            if(filter_percent_counter_zero > 0) then
                                filter_percent_counter_zero <= filter_percent_counter_zero - 1; 
                            end if;
                        else
                            if(filter_percent_counter_zero < unsigned(FILTER_PERCENT_WINDOW)) then  
                                filter_percent_counter_zero <= filter_percent_counter_zero + 1;  
                            end if;
                            if(filter_percent_counter_one > 0) then
                                filter_percent_counter_one <= filter_percent_counter_one - 1;
                            end if;
                        end if;
                        if(filter_percent_counter_one >= unsigned(FILTER_PERCENT_COUNTS)) then
                            input_filter_percent <= '1';
                        elsif(filter_percent_counter_zero >= unsigned(FILTER_PERCENT_COUNTS)) then
                            input_filter_percent <= '0';
                        else
                            input_filter_percent <= input_filter_percent; 
                        end if;
                    else
                        if(INPUT = '1') then
                            filter_percent_counter_one <= filter_percent_counter_one + 1;
                        else
                            filter_percent_counter_zero <= filter_percent_counter_zero + 1;  
                        end if;
                        filter_percent_counter <= filter_percent_counter + 1;
                    end if;    
                else
                    input_filter_percent <= INPUT;
                    filter_percent_counter <= (others => '0');
                    filter_percent_counter_one <= (others => '0');
                    filter_percent_counter_zero <= (others => '0');
                end if;                                                  
            end if;
        end if;
    end process;
		
	pulse_qualifier_process: process(aclk)
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                input_pulse_qualifier <= '0';
                pulse_qualifier_counter <= (others => '0');
            else
                if(unsigned(PULSE_QUALIFIER) > 0 and PULSE_QUALIFIER_RESET = '0') then
                    if(input_filter_percent_d1 = input_filter_percent) then
                        if(pulse_qualifier_counter >= unsigned(PULSE_QUALIFIER)) then
                            input_pulse_qualifier <= input_filter_percent;
                        else
                            pulse_qualifier_counter <= pulse_qualifier_counter + 1;
                        end if;
                    else
                        pulse_qualifier_counter <= (others => '0');
                    end if;    
                else
                    input_pulse_qualifier <= input_filter_percent;
                    pulse_qualifier_counter <= (others => '0');
                end if;                                               
            end if;
        end if;
    end process;
    
	debounce_process: process(aclk)
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                input_debounce <= '0';
                debounce_counter <= (others => '0');
            else
                if(unsigned(DEBOUNCE) > 0 and DEBOUNCE_RESET = '0') then
                    if(unsigned(debounce_counter) = 0) then
                        input_debounce <= input_pulse_qualifier;
                        if(input_pulse_qualifier_d1 /= input_pulse_qualifier) then                          
                            debounce_counter <= unsigned(DEBOUNCE);
                        end if;
                    else
                        debounce_counter <= debounce_counter-1;
                    end if;
                else
                    input_debounce <= input_pulse_qualifier;
                    debounce_counter <= (others => '0');
                end if;                                                  
            end if;
        end if;
    end process;
                      																				   					     					      									 	   			                 
end arch_imp;
