----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 01/09/2017 10:07:23 AM
-- Design Name: 
-- Module Name: temperature sensor - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity temperature_sensor is
	generic (
		REGISTER_W32       : INTEGER := 32;
	    REGISTER_W16       : INTEGER := 16;
	    TEMP_MEASURE_CYCLE : INTEGER := 10000; -- in usecs 
		TEMP_WORD_WIDTH    : INTEGER := 16;
		SPI_CS_SCK_CYCLES  : INTEGER := 2;
		SPI_CLK_DIV        : INTEGER := 100       
	);
	port (
	    -- Sync
	    aclk         : in STD_LOGIC;
        aresetn      : in STD_LOGIC;
	     
		TEMPERATURE   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
        TEMP_CSn      : out STD_LOGIC;
        TEMP_SCK      : out STD_LOGIC;
        TEMP_SDO      : in STD_LOGIC      
	);
end temperature_sensor;

architecture arch_imp of temperature_sensor is
   	
	-- SPI
	signal TEMP_CSn_i   : STD_LOGIC;
	signal TEMP_SCK_i   : STD_LOGIC;
	signal TEMP_SDO_i   : STD_LOGIC;

	signal TEMP_SCK_i_d1 : STD_LOGIC;
	   		
    -- Read Temp SM
    type states_temp_sm is (idle, read_temperature_start, read_temperature); 
    signal state_temp_sm : states_temp_sm;

	signal temperature_i   		: STD_LOGIC_VECTOR(TEMP_WORD_WIDTH-1 downto 0);
	signal read_temperature_on  : STD_LOGIC;

    signal nsec_counter : INTEGER range 0 to 100000000;
    signal usec_counter : INTEGER range 0 to 100000000;

    type states_generate_spi_sm is (idle, csn_on, sck_on, csn_off, spi_end); 
    signal state_generate_spi_sm : states_generate_spi_sm;
         
begin
          								
	-- Read Temp SM
    temp_sm: process(aclk, aresetn) 
		variable temp_counter  : integer range 0 to 65536 :=0; 
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				-- counters
				nsec_counter <= 0;
				usec_counter <= 0;
				-- temperature result
				TEMPERATURE   <= (others => '0');
				temperature_i <= (others => '0');
				TEMP_SCK_i_d1 <= '0';
				-- state
				state_temp_sm <= idle;
        	else			
				TEMP_SCK_i_d1 <= TEMP_SCK_i;
				case state_temp_sm is
					when idle =>
						read_temperature_on <= '0';
						if(nsec_counter >= 100) then
							state_temp_sm <= read_temperature_start;	
							if(usec_counter >= TEMP_MEASURE_CYCLE) then
								state_temp_sm <= read_temperature_start;	
							else
								usec_counter <= usec_counter + 1;	
							end if;
						else
							nsec_counter <= nsec_counter + 1;
						end if;
					when read_temperature_start =>
			  			read_temperature_on <= '1';
						if(TEMP_CSn_i = '0') then
							temp_counter  := 1;
							state_temp_sm <= read_temperature;		
						end if;
			  		when read_temperature =>
						if(TEMP_CSn_i = '0') then
							if(TEMP_SCK_i = '1' and TEMP_SCK_i_d1 = '0') then
								if(temp_counter <= TEMP_WORD_WIDTH) then
									temperature_i(TEMP_WORD_WIDTH-temp_counter) <= TEMP_SDO_i;
									temp_counter := temp_counter + 1;
								end if;
							end if;
						else
							nsec_counter <= 0;
							usec_counter <= 0;
							TEMPERATURE <= 	"00000000000000000000" & temperature_i(14 downto 3);
							state_temp_sm <= idle;		
						end if;														  
					when others =>
						null;
				end case;
			end if;
         end if;
    end process;
			 
	-- Generate CSn and SCK
    spi_sm: process(aclk, aresetn) 
		variable spi_counter  : integer range 0 to 16384 :=0; 
		variable sck_counter  : integer range 0 to 16384 :=0;
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				TEMP_SCK_i <= '0';
				TEMP_CSn_i <= '1';
				-- state
				state_generate_spi_sm <= idle;
        	else			
				case state_generate_spi_sm is
					when idle =>
						TEMP_SCK_i <= '0';
						TEMP_CSn_i <= '1';
						spi_counter := 0;
						if(read_temperature_on = '1') then
							state_generate_spi_sm <= csn_on;	
						end if;
					when csn_on =>
			  			TEMP_CSn_i <= '0';
						if(spi_counter >= SPI_CLK_DIV*SPI_CS_SCK_CYCLES) then
							spi_counter := 0;
							sck_counter := 0;
							state_generate_spi_sm <= sck_on;
						else
							spi_counter := spi_counter + 1;	
						end if;
			  		when sck_on =>
						if(spi_counter >= SPI_CLK_DIV/2) then
							spi_counter := 0;							
							if(sck_counter >= (TEMP_WORD_WIDTH*2)) then
								state_generate_spi_sm <= csn_off;	
							else
							    TEMP_SCK_i <= not TEMP_SCK_i;
								sck_counter := sck_counter + 1;	
							end if;
						else
							spi_counter := spi_counter + 1;
						end if;	
					when csn_off =>
						if(spi_counter >= SPI_CLK_DIV*SPI_CS_SCK_CYCLES) then
							TEMP_CSn_i <= '1';
							state_generate_spi_sm <= spi_end;
						else
							spi_counter := spi_counter + 1;	
						end if;
					when spi_end =>
						if(read_temperature_on = '0') then
							state_generate_spi_sm <= idle;	
						end if;
					when others =>
						null;
				end case;
			end if;
         end if;
    end process;
    
    TEMP_CSn <= TEMP_CSn_i;
    TEMP_SCK <= TEMP_SCK_i;
    TEMP_SDO_i <= TEMP_SDO;
			 	  																					   																				   					     					      									 	   			                 
end arch_imp;
