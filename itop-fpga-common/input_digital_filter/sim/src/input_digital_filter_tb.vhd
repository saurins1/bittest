library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity input_digital_filter_tb is
  	generic (
  		FILTER_LEVEL		: INTEGER := 4
  	);
end;

architecture bench of input_digital_filter_tb is

  component input_digital_filter
  	generic (
  		FILTER_LEVEL		: INTEGER := 4
  	);
      Port (
    	    aclk     : in STD_LOGIC;
          aresetn  : in STD_LOGIC;
          DIN 	: in STD_LOGIC; 
          CE 		: in STD_LOGIC; 
          DOUT 	: out STD_LOGIC
  	);
  end component;

  signal aclk: STD_LOGIC:= '0';
  signal aresetn: STD_LOGIC:= '0';

  signal DIN: STD_LOGIC:= '0';
  signal CE: STD_LOGIC:= '0';
  signal DOUT: STD_LOGIC ;

  constant aclk_period: time := 10 ns;

  signal filter_on: STD_LOGIC:= '0';

  signal din_counter: STD_LOGIC_VECTOR(31 downto 0);

begin
	
	-- aclk process 
    aclk_process :process
    begin
        aclk <= not aclk;
        wait for aclk_period/2;
        aclk <= not aclk;
        wait for aclk_period/2;
    end process;

  -- Insert values for generic parameters !!
  uut: input_digital_filter generic map ( FILTER_LEVEL => FILTER_LEVEL)
                               port map ( aclk         => aclk,
                                          aresetn      => aresetn,
                                          DIN          => din_counter(4),
                                          CE           => CE,
                                          DOUT         => DOUT );

	stimulus: process
	begin
		aresetn <= '0';
		wait for aclk_period*10;
		aresetn <= '1';
		wait for aclk_period*10;
		filter_on <= '1';
		wait for aclk_period*10;
		--filter_on <= '0';
		wait;
	end process;


	process(aclk) 
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                din_counter <= (others => '0');
				CE <= '0';
            else  
				if(filter_on = '1') then
		      		din_counter <= std_logic_vector(unsigned(din_counter)+1);
					CE <= '1';
				else
					CE <= '0';	
		   		end if; 				
			end if;
		end if;
	end process;

end;