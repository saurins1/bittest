----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 10/21/2017 10:57:53 AM
-- Design Name: 
-- Module Name: fir_correlation - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity input_digital_filter is
	generic (
		FILTER_LEVEL		: INTEGER := 4
	);
    Port ( 
		-- sync
  	    aclk     : in STD_LOGIC;
        aresetn  : in STD_LOGIC;
		
		-- input/output
        DIN 	: in STD_LOGIC; 
        CE 		: in STD_LOGIC; 
        DOUT 	: out STD_LOGIC
	);
end input_digital_filter;

architecture Behavioral of input_digital_filter is

    signal tap 		    : STD_LOGIC_VECTOR(FILTER_LEVEL-1 downto 0);
	constant all_zeros 	: STD_LOGIC_VECTOR(FILTER_LEVEL-1 downto 0):= (others => '0');
	constant all_ones  	: STD_LOGIC_VECTOR(FILTER_LEVEL-1 downto 0):= (others => '1');

begin
	
	-- TAP
	process(aclk) 
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                tap      <= (others => '0');
            else  
				if(CE = '1') then
		      		tap <= tap(FILTER_LEVEL-2 downto 0) & DIN;
		   		end if; 				
			end if;
		end if;
	end process;
		
	-- DOUT				
	process(aclk) 
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                DOUT      <= '0';
            else  
           		if(tap = all_ones) then
              		DOUT <= '1';
           		elsif(tap = all_zeros) then
              		DOUT <= '0';
           		end if; 				
			end if;
		end if;
	end process;
    
end Behavioral;
