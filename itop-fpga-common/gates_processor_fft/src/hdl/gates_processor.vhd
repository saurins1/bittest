library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity gates_processor is
	generic (
		REGISTER_W64  : INTEGER    := 64;
		REGISTER_W48  : INTEGER    := 48;
        REGISTER_W32  : INTEGER    := 32;
        REGISTER_W24  : INTEGER    := 24;
        REGISTER_W16  : INTEGER    := 16;
        REGISTER_W8   : INTEGER    := 8;
        REGISTER_W4   : INTEGER    := 4;
        REGISTER_W2   : INTEGER    := 2
	);
	port (
	    --sync
	    aclk           : in STD_LOGIC;
        aresetn        : in STD_LOGIC;  
	    
	    -- GENERAL    
	    RX_DELAY       : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	    TX_DELAY       : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);     

        -- GATE1
		GATE1_EN                          : in STD_LOGIC;  
		GATE1_START                       : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		GATE1_WIDTH                       : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);	
        GATE1_AMP_THRESHOLD               : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE1_AMP_THRESHOLD_ALARM_EN      : in STD_LOGIC; 
        GATE1_AMP_THRESHOLD_ALARM_CROSSING: in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE1_TOF_ALGORITHM               : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE1_TOF_AVG                     : in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
        GATE1_TOF_MIN_THICKNESS           : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE1_TOF_ALARM_EN                : in STD_LOGIC; 
        GATE1_TOF_ALARM_TYPE              : in STD_LOGIC;
        GATE1_TOF_ALARM_CROSSING          : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE1_TOF_ALARM_MIN_VALUE         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE1_TOF_ALARM_MAX_VALUE         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE1_TOF                         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE1_TOF_AVERAGED                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE1_AMP_ALARM                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        GATE1_TOF_ALARM                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE1_AMP_ALARM_DIO               : out STD_LOGIC; --NEW ADDER FOR DIO (Bolt load)
        GATE1_TOF_ALARM_DIO               : out STD_LOGIC; --NEW ADDED FOR DIO (Bolt load)
        GATE1_MAX                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE1_MAX_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE1_MIN                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE1_MIN_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE1_THR                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);   --NEW ADDER (Bolt load)
        GATE1_THR_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);   --NEW ADDER (Bolt load)
        
        -- GATE2
        GATE2_EN                          : in STD_LOGIC;  
        GATE2_START                       : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE2_WIDTH                       : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);    
        GATE2_AMP_THRESHOLD               : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE2_AMP_THRESHOLD_ALARM_EN      : in STD_LOGIC; 
        GATE2_AMP_THRESHOLD_ALARM_CROSSING: in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE2_TOF_ALGORITHM               : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE2_TOF_AVG                     : in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
        GATE2_TOF_MIN_THICKNESS           : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE2_TOF_ALARM_EN                : in STD_LOGIC; 
        GATE2_TOF_ALARM_TYPE              : in STD_LOGIC;
        GATE2_TOF_ALARM_CROSSING          : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE2_TOF_ALARM_MIN_VALUE         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE2_TOF_ALARM_MAX_VALUE         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE2_TOF                         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE2_TOF_AVERAGED                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE2_AMP_ALARM                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE2_TOF_ALARM                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE2_AMP_ALARM_DIO               : out STD_LOGIC; --NEW ADDER FOR DIO (Bolt load)
        GATE2_TOF_ALARM_DIO               : out STD_LOGIC; --NEW ADDED FOR DIO (Bolt load)
        GATE2_MAX                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE2_MAX_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE2_MIN                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE2_MIN_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE2_THR                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE2_THR_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE2_FFT_EN                      : in STD_LOGIC;
        GATE2_CC_EN                       : in STD_LOGIC;
        GATE2_FFT_SEGMENT                 : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE2_FFT_NFFT                    : in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
        GATE2_FFT_WIDTH                   : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE2_FFT_MAX_VALUE               : out STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
        GATE2_FFT_MAX_SAMPLE              : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE2_CC_MAX_VALUE                : out STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
        GATE2_CC_MAX_SAMPLE               : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        --GATE2_FFT_CUADRATIC_ERROR         : out STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
        GATE2_FFT_COHERENCE               : out STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
        GATE2_CC_CONFIG_ON_ACK            : out STD_LOGIC;
        GATE2_CC_CONFIG_ON                : in STD_LOGIC;        
        GATE2_CC_CONFIG_ADDR              : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE2_CC_CONFIG_DATA              : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);  
        
        -- GATE3
        GATE3_EN                          : in STD_LOGIC;  
        GATE3_START                       : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE3_WIDTH                       : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);    
        GATE3_AMP_THRESHOLD               : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE3_AMP_THRESHOLD_ALARM_EN      : in STD_LOGIC; 
        GATE3_AMP_THRESHOLD_ALARM_CROSSING: in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE3_TOF_ALGORITHM               : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE3_TOF_AVG                     : in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
        GATE3_TOF_MIN_THICKNESS           : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE3_TOF_ALARM_EN                : in STD_LOGIC; 
        GATE3_TOF_ALARM_TYPE              : in STD_LOGIC;
        GATE3_TOF_ALARM_CROSSING          : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE3_TOF_ALARM_MIN_VALUE         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE3_TOF_ALARM_MAX_VALUE         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE3_TOF                         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE3_TOF_AVERAGED                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE3_AMP_ALARM                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        GATE3_TOF_ALARM                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE3_AMP_ALARM_DIO               : out STD_LOGIC; --NEW ADDER FOR DIO (Bolt load)
        GATE3_TOF_ALARM_DIO               : out STD_LOGIC; --NEW ADDED FOR DIO (Bolt load)
        GATE3_MAX                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE3_MAX_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE3_MIN                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE3_MIN_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE3_THR                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0); --NEW ADDER (Bolt load)
        GATE3_THR_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0); --NEW ADDER (Bolt load)

		--GATES DATA
		GATES_CONFIG : in STD_LOGIC;
		GATES_SAMPLE_START: out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATES_SAMPLE_END  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATES_EN          : out STD_LOGIC;
        GATES_END         : out STD_LOGIC;
        READ_GATES_RESULT : in STD_LOGIC;
        
		GATES_AXIS_TREADY : out STD_LOGIC;
		GATES_AXIS_TDATA  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		GATES_AXIS_TLAST  : in STD_LOGIC;
		GATES_AXIS_TVALID : in STD_LOGIC;
		
		-- CORRELATION
        CORRELATION_EN              	: out STD_LOGIC;
        CORRELATION_GATE_SAMPLE_START   : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        CORRELATION_GATE_SAMPLE_END     : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        CORRELATION_AXIS_TREADY     	: out STD_LOGIC;
        CORRELATION_AXIS_TDATA      	: in STD_LOGIC_VECTOR(REGISTER_W48-1 downto 0);
        CORRELATION_AXIS_TLAST      	: in STD_LOGIC;
        CORRELATION_AXIS_TVALID     	: in STD_LOGIC
	);
	
end gates_processor;

architecture arch_imp of gates_processor is

    -- GATE 2
    component gate2_compute is
	generic (
	    REGISTER_W64                    : INTEGER    := 64;
	    REGISTER_W48                    : INTEGER    := 48;
        REGISTER_W32                    : INTEGER    := 32;
        REGISTER_W24                    : INTEGER    := 24;
        REGISTER_W16                    : INTEGER    := 16;
        REGISTER_W8                     : INTEGER    := 8;
        REGISTER_W4                     : INTEGER    := 4;
        REGISTER_W2                     : INTEGER    := 2
    );
    Port ( 
      -- sync
      aclk                             : in STD_LOGIC;
      aresetn                          : in STD_LOGIC; 
                        
      -- GATE PARAMETERS
      GATE_EN                          : in STD_LOGIC;  
      GATE_ON                          : out STD_LOGIC;
      GATE_START                       : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
      GATE_WIDTH                       : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);   
      GATE_RX_DELAY                    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
      GATE_TX_DELAY                    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
      GATE_AMP_THRESHOLD               : in STD_LOGIC_VECTOR(REGISTER_W48-1 downto 0);
      GATE_AMP_THRESHOLD_ALARM_EN      : in STD_LOGIC; 
      GATE_AMP_THRESHOLD_ALARM_CROSSING: in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
      GATE_TOF_ALGORITHM               : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
      GATE_TOF_AVG                     : in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
      GATE_TOF_MIN_THICKNESS           : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
      GATE_TOF_ALARM_EN                : in STD_LOGIC; 
      GATE_TOF_ALARM_TYPE              : in STD_LOGIC;
      GATE_TOF_ALARM_CROSSING          : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
      GATE_TOF_ALARM_MIN_VALUE         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
      GATE_TOF_ALARM_MAX_VALUE         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
      -- GATE OUTPUT
      READ_GATE                        : in STD_LOGIC;
      GATE_END                         : out STD_LOGIC; 
      GATE_TOF                         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
      GATE_TOF_AVERAGED                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
      GATE_AMP_ALARM                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
      GATE_TOF_ALARM                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
      GATE_AMP_ALARM_DIO               : out STD_LOGIC; --NEW ADDER FOR DIO (Bolt load)
      GATE_TOF_ALARM_DIO               : out STD_LOGIC; --NEW ADDED FOR DIO (Bolt load)
      GATE_MAX                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
      GATE_MAX_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
      GATE_MIN                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
      GATE_MIN_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
      GATE_THR                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
      GATE_THR_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
      -- GATE DATA
      GATE_TREADY                      : out STD_LOGIC;
      GATE_TDATA                       : in STD_LOGIC_VECTOR(REGISTER_W48-1 downto 0);
      GATE_TLAST                       : in STD_LOGIC;
      GATE_TVALID                      : in STD_LOGIC;
      -- CORRELATION
      CORRELATION_EN                   : out STD_LOGIC;
      GATE_CORRELATION_TREADY          : out STD_LOGIC;
      GATE_CORRELATION_TDATA           : in STD_LOGIC_VECTOR(REGISTER_W48-1 downto 0);
      GATE_CORRELATION_TLAST           : in STD_LOGIC;
      GATE_CORRELATION_TVALID          : in STD_LOGIC;
      -- FFT 
      FFT_EN                           : in STD_LOGIC;
      CC_EN                            : in STD_LOGIC;
      FFT_SEGMENT                      : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
      FFT_NFFT                         : in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
      FFT_WIDTH                        : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
      FFT_MAX_VALUE                    : out STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
      FFT_MAX_SAMPLE                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
      CC_MAX_VALUE                     : out STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
      CC_MAX_SAMPLE                    : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
      --FFT_CUADRATIC_ERROR              : out STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
      FFT_COHERENCE                    : out STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
      CC_CONFIG_ON_ACK                 : out STD_LOGIC;
      CC_CONFIG_ON                     : in STD_LOGIC;        
      CC_CONFIG_ADDR                   : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
      CC_CONFIG_DATA                   : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0)  
    );
    end component gate2_compute;

    -- GATE 1 and 3
    component gatex_compute is
	generic (
        -- Users to add parameters here
        REGISTER_W64                    : INTEGER    := 64;
        REGISTER_W48                    : INTEGER    := 48;
        REGISTER_W32                    : INTEGER    := 32;
        REGISTER_W16                    : INTEGER    := 16;
        REGISTER_W8                     : INTEGER    := 8;
        REGISTER_W4                     : INTEGER    := 4;
        REGISTER_W2                     : INTEGER    := 2
    );
    Port ( 
      -- sync
      aclk                             : in STD_LOGIC;
      aresetn                          : in STD_LOGIC;                   
      -- GATE PARAMETERS
      GATE_EN                          : in STD_LOGIC;  
      GATE_ON                          : out STD_LOGIC;
      GATE_START                       : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
      GATE_WIDTH                       : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
      GATE_RX_DELAY                    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
      GATE_TX_DELAY                    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
      GATE_AMP_THRESHOLD               : in STD_LOGIC_VECTOR(REGISTER_W48-1 downto 0);
      GATE_AMP_THRESHOLD_ALARM_EN      : in STD_LOGIC; 
      GATE_AMP_THRESHOLD_ALARM_CROSSING: in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
      GATE_TOF_ALGORITHM               : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
      GATE_TOF_AVG                     : in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
      GATE_TOF_MIN_THICKNESS           : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
      GATE_TOF_ALARM_EN                : in STD_LOGIC; 
      GATE_TOF_ALARM_TYPE              : in STD_LOGIC;
      GATE_TOF_ALARM_CROSSING          : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
      GATE_TOF_ALARM_MIN_VALUE         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
      GATE_TOF_ALARM_MAX_VALUE         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
      -- GATE output
      READ_GATE                        : in STD_LOGIC;
      GATE_END                         : out STD_LOGIC; 
      GATE_TOF                         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
      GATE_TOF_AVERAGED                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
      GATE_AMP_ALARM                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
      GATE_TOF_ALARM                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
      GATE_AMP_ALARM_DIO               : out STD_LOGIC; --NEW ADDER FOR DIO (Bolt load)
      GATE_TOF_ALARM_DIO               : out STD_LOGIC; --NEW ADDED FOR DIO (Bolt load)
      GATE_MAX                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
      GATE_MAX_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
      GATE_MIN                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
      GATE_MIN_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
      GATE_THR                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);  --NEW ADDER (Bolt load)
      GATE_THR_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);  --NEW ADDER (Bolt load)
      -- AXI SLAVE PORT
      GATE_TREADY                      : out STD_LOGIC;
      GATE_TDATA                       : in STD_LOGIC_VECTOR(REGISTER_W48-1 downto 0);
      GATE_TLAST                       : in STD_LOGIC;
      GATE_TVALID                      : in STD_LOGIC;
      
      -- CORRELATION
      CORRELATION_EN                   : out STD_LOGIC;
      GATE_CORRELATION_TREADY          : out STD_LOGIC;
      GATE_CORRELATION_TDATA           : in STD_LOGIC_VECTOR(REGISTER_W48-1 downto 0);
      GATE_CORRELATION_TLAST           : in STD_LOGIC;
      GATE_CORRELATION_TVALID          : in STD_LOGIC
    );
    end component gatex_compute;
    
    signal gate1_on : STD_LOGIC;
    signal gate2_on : STD_LOGIC;
    signal gate3_on : STD_LOGIC;
    
    -- Gates data input
    signal gate1_axis_tready    : STD_LOGIC;
    signal gate1_axis_tdata     : STD_LOGIC_VECTOR(REGISTER_W48-1 downto 0);
    signal gate1_axis_tlast     : STD_LOGIC;
    signal gate1_axis_tvalid    : STD_LOGIC;
	signal gate1_input_counter  : UNSIGNED(REGISTER_W16-1 downto 0);

    signal gate2_axis_tready    : STD_LOGIC;
    signal gate2_axis_tdata     : STD_LOGIC_VECTOR(REGISTER_W48-1 downto 0);
    signal gate2_axis_tlast     : STD_LOGIC;
    signal gate2_axis_tvalid    : STD_LOGIC;
	signal gate2_input_counter  : UNSIGNED(REGISTER_W16-1 downto 0);

    signal gate3_axis_tready    : STD_LOGIC;
    signal gate3_axis_tdata     : STD_LOGIC_VECTOR(REGISTER_W48-1 downto 0);
    signal gate3_axis_tlast     : STD_LOGIC;
    signal gate3_axis_tvalid    : STD_LOGIC;
	signal gate3_input_counter  : UNSIGNED(REGISTER_W16-1 downto 0);
    
    -- Read Gate
    signal read_gate1    : STD_LOGIC;
    signal read_gate2    : STD_LOGIC;
    signal read_gate3    : STD_LOGIC;
    
    -- Gate end
    signal gate1_end    : STD_LOGIC;
    signal gate2_end    : STD_LOGIC;
    signal gate3_end    : STD_LOGIC;

	-- Gates limits
	signal gates_sample_start_i  : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal gates_sample_end_i    : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);

	signal gate1_wait   : UNSIGNED(REGISTER_W16-1 downto 0);
	signal gate2_wait   : UNSIGNED(REGISTER_W16-1 downto 0);
	signal gate3_wait   : UNSIGNED(REGISTER_W16-1 downto 0);

	signal gate1_top   : UNSIGNED(REGISTER_W16-1 downto 0);
	signal gate2_top   : UNSIGNED(REGISTER_W16-1 downto 0);
	signal gate3_top   : UNSIGNED(REGISTER_W16-1 downto 0);

    -- Gate_end = gate_start+gate_width
    signal gate1_sample_end    : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal gate2_sample_end    : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal gate3_sample_end    : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	
    -- gate2_amp_threshold_i
    signal gate2_amp_threshold_i  : STD_LOGIC_VECTOR(REGISTER_W48-1 downto 0);
    signal gate1_amp_threshold_i  : STD_LOGIC_VECTOR(REGISTER_W48-1 downto 0);
    signal gate3_amp_threshold_i  : STD_LOGIC_VECTOR(REGISTER_W48-1 downto 0);

	type states_gates_config is (idle, prepare_update, 
								update_gates_1, update_gates_2, update_gates_3, update_gates_4,
								update_gates_5, update_gates_6, update_gates_7, update_gates_8,
								update_gates_9, update_gates_10, update_gates_11); 
    signal state_gates_config : states_gates_config; 

	signal gate_main_axis_tready : STD_LOGIC;
	
	signal CORRELATION_AXIS_TREADY_G1 : std_logic;
	signal CORRELATION_AXIS_TREADY_G2 : std_logic;
	signal CORRELATION_AXIS_TREADY_G3 : std_logic;
	
	signal CORRELATION_EN_G1 : std_logic;
	signal CORRELATION_EN_G2 : std_logic;
	signal CORRELATION_EN_G3 : std_logic;
	
	--signal GATE_THR_1                        : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    --signal GATE_THR_SAMPLE_1                  : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0); 
	--signal GATE_THR_2                        : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    --signal GATE_THR_SAMPLE_2                  : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	--signal GATE_THR_3                        : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    --signal GATE_THR_SAMPLE_3                  : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    
begin
	
	-- GATES TREADY
    GATES_AXIS_TREADY <= (gate1_axis_tready or gate2_axis_tready or gate3_axis_tready) and gate_main_axis_tready;
	
	gates_process:process(aclk)   
		variable var_counter_wait  : integer range 0 to 1024 :=0; 
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then 
                gate_main_axis_tready <= '0';
				gates_sample_start_i <= (others => '0');  
				gate1_sample_end <= (others => '0');  
				gate2_sample_end <= (others => '0');  
				gate3_sample_end <= (others => '0');
				gates_sample_end_i <= (others => '0');
				CORRELATION_GATE_SAMPLE_START <= (others => '0');
				CORRELATION_GATE_SAMPLE_END <= (others => '0');	
				gate1_wait <= (others => '0');  
				gate2_wait <= (others => '0');  
				gate3_wait <= (others => '0');
				gate1_top <= (others => '0');  
				gate2_top <= (others => '0');  
				gate3_top <= (others => '0');
                -- state
                state_gates_config <= idle;  
            else
                case state_gates_config is                    
                    when idle=>
						if(GATES_CONFIG = '1') then
							gate_main_axis_tready <= '0';
							var_counter_wait := 0;
							state_gates_config <= prepare_update;  
						else
							gate_main_axis_tready <= '1';	
						end if;
					when prepare_update=>
						if(GATES_CONFIG = '0') then
							if(var_counter_wait >= 16) then
								state_gates_config <= update_gates_1; 	
							else
								var_counter_wait := var_counter_wait + 1;	
							end if;
						end if;	
					when update_gates_1 =>		
						gate1_sample_end <= std_logic_vector(unsigned(GATE1_START) + unsigned(GATE1_WIDTH) - 1);
    					gate2_sample_end <= std_logic_vector(unsigned(GATE2_START) + unsigned(GATE2_WIDTH) - 1);
    					gate3_sample_end <= std_logic_vector(unsigned(GATE3_START) + unsigned(GATE3_WIDTH) - 1);
						gates_sample_start_i <= (others => '1');
						gates_sample_end_i <= (others => '0');  
						state_gates_config <= update_gates_2;	
					when update_gates_2 =>			
						if(gate1_on = '1') then
							gates_sample_start_i <= GATE1_START; 	
							gates_sample_end_i <= gate1_sample_end; 
						end if;
						state_gates_config <= update_gates_3;
					when update_gates_3 =>			
						if(gate2_on = '1' and (unsigned(gate2_sample_end) >= unsigned(gates_sample_end_i))) then
							gates_sample_end_i <= gate2_sample_end; 
						end if;
						if(gate2_on = '1' and (unsigned(GATE2_START) <= unsigned(gates_sample_start_i))) then
							gates_sample_start_i <= GATE2_START; 	 
						end if;	
						state_gates_config <= update_gates_4;
					when update_gates_4 =>			
						if(gate3_on = '1' and (unsigned(gate3_sample_end) >= unsigned(gates_sample_end_i))) then
							gates_sample_end_i <= gate3_sample_end; 
						end if;
						if(gate3_on = '1' and (unsigned(GATE3_START) <= unsigned(gates_sample_start_i))) then
							gates_sample_start_i <= GATE3_START; 	 
						end if;	
						state_gates_config <= update_gates_5;
					when update_gates_5 =>	
						if(gate1_on = '0' and gate2_on = '0' and gate3_on = '0') then
							gates_sample_start_i <= (others => '0');	
						end if;
						state_gates_config <= update_gates_6;
					when update_gates_6 =>                                             -- new added 09/25/2020 (Bolt load)
						if(gate1_on = '1') then
							CORRELATION_GATE_SAMPLE_START <= GATE1_START;	
							CORRELATION_GATE_SAMPLE_END	  <= gate1_sample_end;
						else
							CORRELATION_GATE_SAMPLE_START <= (others => '0');	
							CORRELATION_GATE_SAMPLE_END <= (others => '0');	
						end if;
						state_gates_config <= update_gates_7;
					when update_gates_7 =>                                             -- new added 09/25/2020 (previously added at update_gates_6
                            if(gate2_on = '1') then
                                CORRELATION_GATE_SAMPLE_START <= GATE2_START;    
                                CORRELATION_GATE_SAMPLE_END      <= gate2_sample_end;
                            else
                                CORRELATION_GATE_SAMPLE_START <= (others => '0');    
                                CORRELATION_GATE_SAMPLE_END <= (others => '0');    
                            end if;
                            state_gates_config <= update_gates_8;
					when update_gates_8 =>                                             -- new added 09/25/2020 (Bolt load)
                                if(gate3_on = '1') then
                                    CORRELATION_GATE_SAMPLE_START <= GATE3_START;    
                                    CORRELATION_GATE_SAMPLE_END      <= gate3_sample_end;
                                else
                                    CORRELATION_GATE_SAMPLE_START <= (others => '0');    
                                    CORRELATION_GATE_SAMPLE_END <= (others => '0');    
                                end if;
                                state_gates_config <= update_gates_9;                            						
					when update_gates_9 =>
						gate1_wait <= unsigned(GATE1_START) - unsigned(gates_sample_start_i);
						gate2_wait <= unsigned(GATE2_START) - unsigned(gates_sample_start_i);
						gate3_wait <= unsigned(GATE3_START) - unsigned(gates_sample_start_i);
						state_gates_config <= update_gates_10;
					when update_gates_10 =>
						gate1_top <= gate1_wait + unsigned(GATE1_WIDTH);
						gate2_top <= gate2_wait + unsigned(GATE2_WIDTH);
						gate3_top <= gate3_wait + unsigned(GATE3_WIDTH);
						state_gates_config <= update_gates_11;
					when update_gates_11 =>
						--if(GATES_CONFIG = '0') then
							state_gates_config <= idle;	
						--end if;	
					when others =>
						null;
				end case;
			end if;
		end if;
	end process;
    
	-- GATES OFF
    GATES_EN <= '0' when ((gate1_on = '0') and (gate2_on = '0') and (gate3_on = '0')) or
                          (unsigned(gates_sample_start_i) >= unsigned(gates_sample_end_i)) else '1';		
	-- GATES SAMPLE START					   
	GATES_SAMPLE_START <= gates_sample_start_i;				   
            
	-- GATES SAMPLE END             		
    GATES_SAMPLE_END <= gates_sample_end_i;
                 					 						 
    -- GATES DATA
	process(aclk)   
    begin
		if (rising_edge(aclk)) then
        	if (aresetn = '0') then 
				-- GATE AXIS signals
				gate1_axis_tdata  <= (others => '0');
				gate1_axis_tlast  <= '0';
				gate1_axis_tvalid <= '0';
				-- Counter variable
				gate1_input_counter <= (others => '0');
            else
				if(GATES_AXIS_TVALID = '1') then
					-- GATE 1
					if(gate1_on = '1') then
						if(gate1_input_counter >= gate1_wait and gate1_input_counter <= gate1_top) then
							gate1_axis_tdata  <= std_logic_vector(resize(signed(GATES_AXIS_TDATA), gate1_axis_tdata'length));
							gate1_axis_tvalid <= '1';
							if(gate1_input_counter = (gate1_top-1)) then
							--if(gate1_input_counter = (gate1_top)) then
								gate1_axis_tlast <= '1';	
							end if;
						else
							gate1_axis_tlast  <= '0';
							gate1_axis_tvalid <= '0';		
						end if;
					end if;
					gate1_input_counter <= gate1_input_counter + 1;
				else
					gate1_axis_tlast  <= '0';
					gate1_axis_tvalid <= '0';
					gate1_input_counter <= (others => '0');
				end if;
            end if;
        end if;
    end process;
			
	process(aclk)   
    begin
		if (rising_edge(aclk)) then
        	if (aresetn = '0') then 
				-- GATE AXIS signals
				gate2_axis_tdata  <= (others => '0');
				gate2_axis_tlast  <= '0';
				gate2_axis_tvalid <= '0';
				-- Counter variable
				gate2_input_counter <= (others => '0');
            else
				if(GATES_AXIS_TVALID = '1') then
					-- GATE 2
					if(gate2_on = '1') then
						if(gate2_input_counter >= gate2_wait and gate2_input_counter <= gate2_top) then
							gate2_axis_tdata  <= std_logic_vector(resize(signed(GATES_AXIS_TDATA), gate2_axis_tdata'length)); 
							gate2_axis_tvalid <= '1';
							if(gate2_input_counter = (gate2_top-1)) then
							--if(gate2_input_counter = (gate2_top)) then
								gate2_axis_tlast <= '1';	
							end if;
						else
							gate2_axis_tlast  <= '0';
							gate2_axis_tvalid <= '0';		
						end if;
					end if;
					gate2_input_counter <= gate2_input_counter + 1;
				else
					gate2_axis_tlast  <= '0';
					gate2_axis_tvalid <= '0';
					gate2_input_counter <= (others => '0');
				end if;
            end if;
        end if;
    end process;	
																		 
	process(aclk)   
    begin
		if (rising_edge(aclk)) then
        	if (aresetn = '0') then 
				-- GATE AXIS signals
				gate3_axis_tdata  <= (others => '0');
				gate3_axis_tlast  <= '0';
				gate3_axis_tvalid <= '0';
				-- Counter variable
				gate3_input_counter <= (others => '0');
            else
				if(GATES_AXIS_TVALID = '1') then
					-- GATE 3
					if(gate3_on = '1') then
						if(gate3_input_counter >= gate3_wait and gate3_input_counter <= gate3_top) then
							gate3_axis_tdata  <= std_logic_vector(resize(signed(GATES_AXIS_TDATA), gate3_axis_tdata'length)); 
							gate3_axis_tvalid <= '1';
							if(gate3_input_counter = (gate3_top-1)) then
							--if(gate3_input_counter = (gate3_top)) then
								gate3_axis_tlast <= '1';	
							end if;
						else
							gate3_axis_tlast  <= '0';
							gate3_axis_tvalid <= '0';		
						end if;
					end if;
					gate3_input_counter <= gate3_input_counter + 1;
				else
					gate3_axis_tlast  <= '0';
					gate3_axis_tvalid <= '0';
					gate3_input_counter <= (others => '0');
				end if;
            end if;
        end if;
    end process;														 
			
    -- GATES_END 
	process(aclk)   
    begin
		if (rising_edge(aclk)) then
        	if (aresetn = '0') then 
            	GATES_END <= '0'; 
            	
            else
                -- GATES END
			    if( ((gate1_on = '1' and gate1_end = '1') or (gate1_on = '0')) and
			        ((gate2_on = '1' and gate2_end = '1') or (gate2_on = '0')) and  
			        ((gate3_on = '1' and gate3_end = '1') or (gate3_on = '0')) ) then
			    	GATES_END <= '1';    
			    else
			        GATES_END <= '0';    
			    end if;
			    
            end if;
        end if;
    end process;
			
    -- READ_GATES_RESULT
	process(aclk)   
    begin
		if (rising_edge(aclk)) then
        	if (aresetn = '0') then 
				read_gate1 <= '0';
            else
	           -- READ GATES		     
			    if(READ_GATES_RESULT = '1') then
					-- GATE 1
					if(gate1_on = '1') then
			        	read_gate1 <= '1';
			        else
			            read_gate1 <= '0';
			        end if;
			    else
			        read_gate1 <= '0';
			    end if;
            end if;
        end if;
    end process;
			
	process(aclk)   
    begin
		if (rising_edge(aclk)) then
        	if (aresetn = '0') then 
				read_gate2 <= '0';
            else
	           -- READ GATES		     
			    if(READ_GATES_RESULT = '1') then
					-- GATE 2
					if(gate2_on = '1') then
			        	read_gate2 <= '1';
			        else
			            read_gate2 <= '0';
			        end if;
			    else
			        read_gate2 <= '0';
			    end if;
            end if;
        end if;
    end process;
			
	process(aclk)   
    begin
		if (rising_edge(aclk)) then
        	if (aresetn = '0') then 
				read_gate3 <= '0';
            else
	           -- READ GATES		     
			    if(READ_GATES_RESULT = '1') then
					-- GATE 3
					if(gate3_on = '1') then
			        	read_gate3 <= '1';
			        else
			            read_gate3 <= '0';
			        end if;
			    else
			        read_gate3 <= '0';
			    end if;
            end if;
        end if;
    end process;
	
	gate1_amp_threshold_i <= std_logic_vector(resize(signed(GATE1_AMP_THRESHOLD), gate1_amp_threshold_i'length));		   
    gate2_amp_threshold_i <= std_logic_vector(resize(signed(GATE2_AMP_THRESHOLD), gate2_amp_threshold_i'length));
    gate3_amp_threshold_i <= std_logic_vector(resize(signed(GATE3_AMP_THRESHOLD), gate3_amp_threshold_i'length));
    
    CORRELATION_EN <= CORRELATION_EN_G1 OR CORRELATION_EN_G2 OR CORRELATION_EN_G3; --new added for bolt load (algorithm)
    
    CORRELATION_AXIS_TREADY <= CORRELATION_AXIS_TREADY_G1 OR CORRELATION_AXIS_TREADY_G2 OR CORRELATION_AXIS_TREADY_G3; --new added for bolt load (algorithm)
                                                     
    gate2_compute_inst: gate2_compute
    generic map(
        REGISTER_W64    => REGISTER_W64,
        REGISTER_W48    => REGISTER_W48,
        REGISTER_W32    => REGISTER_W32,
        REGISTER_W24    => REGISTER_W24,
        REGISTER_W16    => REGISTER_W16,
        REGISTER_W8     => REGISTER_W8,
        REGISTER_W4     => REGISTER_W4,
        REGISTER_W2     => REGISTER_W2
    )
    Port map( 
      -- sync
      aclk                             => aclk,
      aresetn                          => aresetn, 
                        
      -- GATE PARAMETERS
      GATE_EN                          => GATE2_EN,
      GATE_ON                          => gate2_on,
      GATE_START                       => GATE2_START,
      GATE_WIDTH                       => GATE2_WIDTH,   
      GATE_RX_DELAY                    => RX_DELAY, 
      GATE_TX_DELAY                    => TX_DELAY,
      GATE_AMP_THRESHOLD               => gate2_amp_threshold_i,
      GATE_AMP_THRESHOLD_ALARM_EN      => GATE2_AMP_THRESHOLD_ALARM_EN,
      GATE_AMP_THRESHOLD_ALARM_CROSSING=> GATE2_AMP_THRESHOLD_ALARM_CROSSING,
      GATE_TOF_ALGORITHM               => GATE2_TOF_ALGORITHM,
      GATE_TOF_AVG                     => GATE2_TOF_AVG,
      GATE_TOF_MIN_THICKNESS           => GATE2_TOF_MIN_THICKNESS,
      GATE_TOF_ALARM_EN                => GATE2_TOF_ALARM_EN, 
      GATE_TOF_ALARM_TYPE              => GATE2_TOF_ALARM_TYPE,
      GATE_TOF_ALARM_CROSSING          => GATE2_TOF_ALARM_CROSSING,
      GATE_TOF_ALARM_MIN_VALUE         => GATE2_TOF_ALARM_MIN_VALUE,
      GATE_TOF_ALARM_MAX_VALUE         => GATE2_TOF_ALARM_MAX_VALUE,
      -- GATE OUTPUT
      READ_GATE                        => read_gate2,
      GATE_END                         => gate2_end,
      GATE_TOF                         => GATE2_TOF,
      GATE_TOF_AVERAGED                => GATE2_TOF_AVERAGED,
      GATE_AMP_ALARM                   => GATE2_AMP_ALARM,
      GATE_TOF_ALARM                   => GATE2_TOF_ALARM,
      GATE_AMP_ALARM_DIO               => GATE2_AMP_ALARM_DIO, -- NEW ADDED (Bolt load)
      GATE_TOF_ALARM_DIO               => GATE2_TOF_ALARM_DIO, -- NEW ADDED (Bolt load)
      GATE_MAX                         => GATE2_MAX,
      GATE_MAX_SAMPLE                  => GATE2_MAX_SAMPLE,
      GATE_MIN                         => GATE2_MIN,
      GATE_MIN_SAMPLE                  => GATE2_MIN_SAMPLE,
      GATE_THR                         => GATE2_THR,            
      GATE_THR_SAMPLE                  => GATE2_THR_SAMPLE,
      -- GATE DATA
      GATE_TREADY                      => gate2_axis_tready,
      GATE_TDATA                       => gate2_axis_tdata,
      GATE_TLAST                       => gate2_axis_tlast,
      GATE_TVALID                      => gate2_axis_tvalid,
      -- CORRELATION
      CORRELATION_EN                   => CORRELATION_EN_G2,
      GATE_CORRELATION_TREADY          => CORRELATION_AXIS_TREADY_G2,
      GATE_CORRELATION_TDATA           => CORRELATION_AXIS_TDATA,
      GATE_CORRELATION_TLAST           => CORRELATION_AXIS_TLAST,
      GATE_CORRELATION_TVALID          => CORRELATION_AXIS_TVALID,
      -- FFT 
      FFT_EN                           => GATE2_FFT_EN,
      CC_EN                            => GATE2_CC_EN,
      FFT_SEGMENT                      => GATE2_FFT_SEGMENT,
      FFT_NFFT                         => GATE2_FFT_NFFT,
      FFT_WIDTH                        => GATE2_FFT_WIDTH,
      FFT_MAX_VALUE                    => GATE2_FFT_MAX_VALUE,
      FFT_MAX_SAMPLE                   => GATE2_FFT_MAX_SAMPLE,
      CC_MAX_VALUE                     => GATE2_CC_MAX_VALUE,
      CC_MAX_SAMPLE                    => GATE2_CC_MAX_SAMPLE,
      --FFT_CUADRATIC_ERROR              => GATE2_FFT_CUADRATIC_ERROR,
      FFT_COHERENCE                    => GATE2_FFT_COHERENCE,
      CC_CONFIG_ON_ACK                 => GATE2_CC_CONFIG_ON_ACK,
      CC_CONFIG_ON                     => GATE2_CC_CONFIG_ON,      
      CC_CONFIG_ADDR                   => GATE2_CC_CONFIG_ADDR,
      CC_CONFIG_DATA                   => GATE2_CC_CONFIG_DATA
    );  

    gate1_compute_inst: gatex_compute
    generic map(
        REGISTER_W64    => REGISTER_W64,
        REGISTER_W48    => REGISTER_W48,
        REGISTER_W32    => REGISTER_W32,
        REGISTER_W16    => REGISTER_W16,
        REGISTER_W8     => REGISTER_W8,
        REGISTER_W4     => REGISTER_W4,
        REGISTER_W2     => REGISTER_W2
    )
    Port map( 
      -- sync
      aclk                             => aclk,
      aresetn                          => aresetn, 
                        
      -- GATE PARAMETERS
      GATE_EN                          => GATE1_EN,
      GATE_ON                          => gate1_on,
      GATE_START                       => GATE1_START,
      GATE_WIDTH                       => GATE1_WIDTH,   
      GATE_RX_DELAY                    => RX_DELAY, 
      GATE_TX_DELAY                    => TX_DELAY,
      GATE_AMP_THRESHOLD               => gate1_amp_threshold_i,
      GATE_AMP_THRESHOLD_ALARM_EN      => GATE1_AMP_THRESHOLD_ALARM_EN,
      GATE_AMP_THRESHOLD_ALARM_CROSSING=> GATE1_AMP_THRESHOLD_ALARM_CROSSING,
      GATE_TOF_ALGORITHM               => GATE1_TOF_ALGORITHM,
      GATE_TOF_AVG                     => GATE1_TOF_AVG,
      GATE_TOF_MIN_THICKNESS           => GATE1_TOF_MIN_THICKNESS,
      GATE_TOF_ALARM_EN                => GATE1_TOF_ALARM_EN, 
      GATE_TOF_ALARM_TYPE              => GATE1_TOF_ALARM_TYPE,
      GATE_TOF_ALARM_CROSSING          => GATE1_TOF_ALARM_CROSSING,
      GATE_TOF_ALARM_MIN_VALUE         => GATE1_TOF_ALARM_MIN_VALUE,
      GATE_TOF_ALARM_MAX_VALUE         => GATE1_TOF_ALARM_MAX_VALUE,
      -- GATE OUTPUT
      READ_GATE                        => read_gate1,
      GATE_END                         => gate1_end,
      GATE_TOF                         => GATE1_TOF,
      GATE_TOF_AVERAGED                => GATE1_TOF_AVERAGED,
      GATE_AMP_ALARM                   => GATE1_AMP_ALARM,
      GATE_TOF_ALARM                   => GATE1_TOF_ALARM,
      GATE_AMP_ALARM_DIO               => GATE1_AMP_ALARM_DIO, -- NEW ADDED (Bolt load)
      GATE_TOF_ALARM_DIO               => GATE1_TOF_ALARM_DIO, -- NEW ADDED (Bolt load)
      GATE_MAX                         => GATE1_MAX,
      GATE_MAX_SAMPLE                  => GATE1_MAX_SAMPLE,
      GATE_MIN                         => GATE1_MIN,
      GATE_MIN_SAMPLE                  => GATE1_MIN_SAMPLE,
      GATE_THR                         => GATE1_THR,            --new added for bolt load (algorithm)
      GATE_THR_SAMPLE                  => GATE1_THR_SAMPLE,     --new added for bolt load (algorithm)
      -- GATE DATA
      GATE_TREADY                      => gate1_axis_tready,
      GATE_TDATA                       => gate1_axis_tdata,
      GATE_TLAST                       => gate1_axis_tlast,
      GATE_TVALID                      => gate1_axis_tvalid,
      
      -- CORRELATION
      CORRELATION_EN                   => CORRELATION_EN_G1,            --new added for bolt load (algorithm)
      GATE_CORRELATION_TREADY          => CORRELATION_AXIS_TREADY_G1,   --new added for bolt load (algorithm)
      GATE_CORRELATION_TDATA           => CORRELATION_AXIS_TDATA,       --new added for bolt load (algorithm)
      GATE_CORRELATION_TLAST           => CORRELATION_AXIS_TLAST,       --new added for bolt load (algorithm)
      GATE_CORRELATION_TVALID          => CORRELATION_AXIS_TVALID       --new added for bolt load (algorithm)
    );  

    gate3_compute_inst: gatex_compute
    generic map(
        REGISTER_W64    => REGISTER_W64,
        REGISTER_W48    => REGISTER_W48,
        REGISTER_W32    => REGISTER_W32,
        REGISTER_W16    => REGISTER_W16,
        REGISTER_W8     => REGISTER_W8,
        REGISTER_W4     => REGISTER_W4,
        REGISTER_W2     => REGISTER_W2
    )
    Port map( 
      -- sync
      aclk                             => aclk,
      aresetn                          => aresetn, 
                        
      -- GATE PARAMETERS
      GATE_EN                          => GATE3_EN,
      GATE_ON                          => gate3_on,
      GATE_START                       => GATE3_START,
      GATE_WIDTH                       => GATE3_WIDTH,   
      GATE_RX_DELAY                    => RX_DELAY, 
      GATE_TX_DELAY                    => TX_DELAY, 
      GATE_AMP_THRESHOLD               => gate3_amp_threshold_i,
      GATE_AMP_THRESHOLD_ALARM_EN      => GATE3_AMP_THRESHOLD_ALARM_EN,
      GATE_AMP_THRESHOLD_ALARM_CROSSING=> GATE3_AMP_THRESHOLD_ALARM_CROSSING,
      GATE_TOF_ALGORITHM               => GATE3_TOF_ALGORITHM,
      GATE_TOF_AVG                     => GATE3_TOF_AVG,
      GATE_TOF_MIN_THICKNESS           => GATE3_TOF_MIN_THICKNESS,
      GATE_TOF_ALARM_EN                => GATE3_TOF_ALARM_EN, 
      GATE_TOF_ALARM_TYPE              => GATE3_TOF_ALARM_TYPE,
      GATE_TOF_ALARM_CROSSING          => GATE3_TOF_ALARM_CROSSING,
      GATE_TOF_ALARM_MIN_VALUE         => GATE3_TOF_ALARM_MIN_VALUE,
      GATE_TOF_ALARM_MAX_VALUE         => GATE3_TOF_ALARM_MAX_VALUE,
      -- GATE OUTPUT
      READ_GATE                        => read_gate3,
      GATE_END                         => gate3_end,
      GATE_TOF                         => GATE3_TOF,
      GATE_TOF_AVERAGED                => GATE3_TOF_AVERAGED,
      GATE_AMP_ALARM                   => GATE3_AMP_ALARM,
      GATE_TOF_ALARM                   => GATE3_TOF_ALARM,
      GATE_AMP_ALARM_DIO               => GATE3_AMP_ALARM_DIO, -- NEW ADDED (Bolt load)
      GATE_TOF_ALARM_DIO               => GATE3_TOF_ALARM_DIO, -- NEW ADDED (Bolt load)
      GATE_MAX                         => GATE3_MAX,
      GATE_MAX_SAMPLE                  => GATE3_MAX_SAMPLE,
      GATE_MIN                         => GATE3_MIN,
      GATE_MIN_SAMPLE                  => GATE3_MIN_SAMPLE,
      GATE_THR                         => GATE3_THR,            --new added for bolt load (algorithm)
      GATE_THR_SAMPLE                  => GATE3_THR_SAMPLE,     --new added for bolt load (algorithm)
      -- GATE DATA
      GATE_TREADY                      => gate3_axis_tready,
      GATE_TDATA                       => gate3_axis_tdata,
      GATE_TLAST                       => gate3_axis_tlast,
      GATE_TVALID                      => gate3_axis_tvalid,
      
      -- CORRELATION
      CORRELATION_EN                   => CORRELATION_EN_G3,
      GATE_CORRELATION_TREADY          => CORRELATION_AXIS_TREADY_G3,   --new added for bolt load (algorithm)
      GATE_CORRELATION_TDATA           => CORRELATION_AXIS_TDATA,       --new added for bolt load (algorithm)
      GATE_CORRELATION_TLAST           => CORRELATION_AXIS_TLAST,       --new added for bolt load (algorithm)
      GATE_CORRELATION_TVALID          => CORRELATION_AXIS_TVALID       --new added for bolt load (algorithm)
    );  
    
end arch_imp;