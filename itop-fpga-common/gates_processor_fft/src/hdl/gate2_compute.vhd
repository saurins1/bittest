----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- Engineer:    Moktik Rohatgi
-- 
-- Create Date: 04/03/2017 05:02:22 AM
-- Recreate Date: 09/28/2020
-- Design Name: 
-- Module Name: gate2_compute - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity gate2_compute is
	generic (
	    REGISTER_W64                    : INTEGER    := 64;
	    REGISTER_W48                    : INTEGER    := 48;
        REGISTER_W32                    : INTEGER    := 32;
        REGISTER_W24                    : INTEGER    := 24;
        REGISTER_W16                    : INTEGER    := 16;
        REGISTER_W8                     : INTEGER    := 8;
        REGISTER_W4                     : INTEGER    := 4;
        REGISTER_W2                     : INTEGER    := 2;
        PEAK_PEAK_COUNTER               : INTEGER    := 3;
        TOF_ALGORITHM_PEAK              : INTEGER    := 1;
        TOF_ALGORITHM_FIRST_PEAK        : INTEGER    := 2;
        TOF_ALGORITHM_PEAK_PEAK         : INTEGER    := 3;
--        TOF_ALGORITHM_PEAK_PEAK         : INTEGER    := 5;
        TOF_ALGORITHM_FLANK             : INTEGER    := 4; 
        TOF_ALGORITHM_PEAK_PEAK_LIGHT   : INTEGER    := 5; 
--        TOF_ALGORITHM_PEAK_PEAK_LIGHT   : INTEGER    := 3; 
        ALARM_CROSSING_NONE             : INTEGER    := 0;
        ALARM_CROSSING_ABOVE            : INTEGER    := 1;
        ALARM_CROSSING_BELOW            : INTEGER    := 2;
        ALARM_CROSSING_BOTH             : INTEGER    := 3;
        ALARM_CROSSING_RELATIVE         : INTEGER    := 4;
        FFT_MAX                         : INTEGER := 8192;
        FFT_MAX_LOOP                    : INTEGER := 14;
        FFT_BRAM_ADDR_WIDTH             : INTEGER := 12;
        FFT_BRAM_DATA_WIDTH             : INTEGER := 32
  );
  Port ( 
      -- sync
      aclk                             : in STD_LOGIC;
      aresetn                          : in STD_LOGIC; 
                        
      -- GATE PARAMETERS
      GATE_EN                          : in STD_LOGIC;  
      GATE_ON                          : out STD_LOGIC;
      GATE_START                       : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
      GATE_WIDTH                       : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);   
      GATE_RX_DELAY                    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
      GATE_TX_DELAY                    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
      GATE_AMP_THRESHOLD               : in STD_LOGIC_VECTOR(REGISTER_W48-1 downto 0);
      GATE_AMP_THRESHOLD_ALARM_EN      : in STD_LOGIC; 
      GATE_AMP_THRESHOLD_ALARM_CROSSING: in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
      GATE_TOF_ALGORITHM               : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
      GATE_TOF_AVG                     : in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
      GATE_TOF_MIN_THICKNESS           : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
      GATE_TOF_ALARM_EN                : in STD_LOGIC; 
      GATE_TOF_ALARM_TYPE              : in STD_LOGIC;
      GATE_TOF_ALARM_CROSSING          : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
      GATE_TOF_ALARM_MIN_VALUE         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
      GATE_TOF_ALARM_MAX_VALUE         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
      -- GATE OUTPUT
      READ_GATE                        : in STD_LOGIC;
      GATE_END                         : out STD_LOGIC; 
      GATE_TOF                         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
      GATE_TOF_AVERAGED                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
      GATE_AMP_ALARM                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
      GATE_TOF_ALARM                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
      GATE_AMP_ALARM_DIO               : out STD_LOGIC; --NEW ADDER FOR DIO
      GATE_TOF_ALARM_DIO               : out STD_LOGIC; --NEW ADDED FOR DIO
      GATE_MAX                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
      GATE_MAX_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
      GATE_MIN                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
      GATE_MIN_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
      GATE_THR                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
      GATE_THR_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
      -- GATE DATA
      GATE_TREADY                      : out STD_LOGIC;
      GATE_TDATA                       : in STD_LOGIC_VECTOR(REGISTER_W48-1 downto 0);
      GATE_TLAST                       : in STD_LOGIC;
      GATE_TVALID                      : in STD_LOGIC;
      -- CORRELATION
      CORRELATION_EN                   : out STD_LOGIC;
      GATE_CORRELATION_TREADY          : out STD_LOGIC;
      GATE_CORRELATION_TDATA           : in STD_LOGIC_VECTOR(REGISTER_W48-1 downto 0);
      GATE_CORRELATION_TLAST           : in STD_LOGIC;
      GATE_CORRELATION_TVALID          : in STD_LOGIC;
      -- FFT 
      FFT_EN                           : in STD_LOGIC;
      CC_EN                            : in STD_LOGIC;
      FFT_SEGMENT                      : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
      FFT_NFFT                         : in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
      FFT_WIDTH	                       : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
      FFT_MAX_VALUE                    : out STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
      FFT_MAX_SAMPLE                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
      CC_MAX_VALUE                     : out STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
      CC_MAX_SAMPLE                    : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
      --FFT_CUADRATIC_ERROR              : out STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
      FFT_COHERENCE                    : out STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
      CC_CONFIG_ON_ACK                 : out STD_LOGIC;
      CC_CONFIG_ON                     : in STD_LOGIC;        
      CC_CONFIG_ADDR                   : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
      CC_CONFIG_DATA                   : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0)    
  );
end gate2_compute;

architecture Behavioral of gate2_compute is

    component fft_gate is
        generic (
            REGISTER_W64        : INTEGER := 64;
            REGISTER_W48        : INTEGER := 48;
            REGISTER_W32        : INTEGER := 32;
            REGISTER_W24        : INTEGER := 24;
            REGISTER_W16        : INTEGER := 16;
            REGISTER_W8         : INTEGER := 8;
            FFT_MAX             : INTEGER := 8192;
            FFT_MAX_LOOP        : INTEGER := 14;
            BRAM_ADDR_WIDTH     : INTEGER := 12;
            BRAM_DATA_WIDTH     : INTEGER := 32
        );
        port (
            -- Sync
            aclk         : in STD_LOGIC;
            aresetn      : in STD_LOGIC;
        
            -- Control
            FFT_TEST     : in STD_LOGIC;
            FFT_EN       : in STD_LOGIC;  
            CC_EN        : in STD_LOGIC; 
            
            FFT_WIDTH	 : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	        CO_FFT_SEGMENT : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
            CO_FFT_NFFT : in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
    
            FFT_MAX_VALUE  : out STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
            FFT_MAX_SAMPLE : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            CC_MAX_VALUE   : out STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
            CC_MAX_SAMPLE  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            FFT_GATE_END   : out STD_LOGIC;  
            FFT_GATE_END_ACK  : in STD_LOGIC;  
            --FFT_CUADRATIC_ERROR   : out STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
            FFT_COHERENCE   : out STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
                
            -- Signal input
            FFT_INPUT_AXIS_TREADY  : out STD_LOGIC;
            FFT_INPUT_AXIS_TDATA   : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
            FFT_INPUT_AXIS_TLAST   : in STD_LOGIC;
            FFT_INPUT_AXIS_TVALID  : in STD_LOGIC;
            
            CC_CONFIG_ON_ACK : out STD_LOGIC;
            CC_CONFIG_ON     : in STD_LOGIC;	    
            CC_CONFIG_ADDR   : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
            CC_CONFIG_DATA   : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
            
            FFT_STATUS_AXIS_TDATA  : out STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
            FFT_STATUS_AXIS_TUSER  : out STD_LOGIC_VECTOR(REGISTER_W24-1 downto 0);
            FFT_STATUS_AXIS_TLAST  : out STD_LOGIC;
            FFT_STATUS_AXIS_TVALID : out STD_LOGIC
        );
    end component fft_gate;
    
    signal fft_en_i               : STD_LOGIC;
    signal cc_en_i                : STD_LOGIC;
    signal fft_gate_end           : STD_LOGIC;
    signal fft_gate_end_ack       : STD_LOGIC;
    signal fft_input_axis_tready  : STD_LOGIC;
    signal fft_status_axis_tdata  : STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
    signal fft_status_axis_tuser  : STD_LOGIC_VECTOR(REGISTER_W24-1 downto 0);
    signal fft_status_axis_tlast  : STD_LOGIC;
    signal fft_status_axis_tvalid : STD_LOGIC;

    signal gate_max_i           : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal gate_min_i           : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal gate_max_min_end     : STD_LOGIC;
    
    signal gate_threshold       : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal gate_threshold_sample: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);

    -- gate data
    signal gate_tdata_rect      : STD_LOGIC_VECTOR (REGISTER_W48-1 downto 0);
    signal gate_tdata_i         : STD_LOGIC_VECTOR (REGISTER_W48-1 downto 0);
    signal gate_tdata_i1        : STD_LOGIC_VECTOR (REGISTER_W48-1 downto 0);
    signal gate_tdata_i2        : STD_LOGIC_VECTOR (REGISTER_W48-1 downto 0);
    signal gate_tvalid_i        : STD_LOGIC;
    signal gate_tvalid_i1       : STD_LOGIC;
    signal gate_tlast_i         : STD_LOGIC;
    signal gate_tlast_i1        : STD_LOGIC;
    
    signal peak_plus_flag       : STD_LOGIC;
    
    -- Peak points
    signal peak_i               : STD_LOGIC_VECTOR (REGISTER_W48-1 downto 0);
    signal peak_plus_i          : STD_LOGIC_VECTOR (REGISTER_W48-1 downto 0);
    signal peak_minus_i         : STD_LOGIC_VECTOR (REGISTER_W48-1 downto 0);
    signal peak_tof_i           : STD_LOGIC_VECTOR (REGISTER_W16-1 downto 0);
    signal tof_counter_sample   : UNSIGNED (REGISTER_W16-1 downto 0);
    signal corr_counter         : UNSIGNED (REGISTER_W16-1 downto 0);
    
    signal peak_peak_first_flag  : STD_LOGIC;
    signal peak_i2               : STD_LOGIC_VECTOR (REGISTER_W48-1 downto 0);
    signal peak_plus_i2          : STD_LOGIC_VECTOR (REGISTER_W48-1 downto 0);
    signal peak_minus_i2         : STD_LOGIC_VECTOR (REGISTER_W48-1 downto 0);
    signal peak_tof_i2           : STD_LOGIC_VECTOR (REGISTER_W16-1 downto 0);
    signal gate_width_half       : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0); 
    
    -- Flank cross points
    signal ef_y1                : STD_LOGIC_VECTOR(REGISTER_W48-1 downto 0) := (others => '0');
    signal ef_y2                : STD_LOGIC_VECTOR(REGISTER_W48-1 downto 0) := (others => '0');
    
    -- peak divider params
    signal peak_plus_divisor    : STD_LOGIC_VECTOR (REGISTER_W64-1 downto 0);
    signal peak_plus_dividend   : STD_LOGIC_VECTOR (REGISTER_W64-1 downto 0);
    signal peak_minus_divisor   : STD_LOGIC_VECTOR (REGISTER_W64-1 downto 0);
    signal peak_minus_dividend  : STD_LOGIC_VECTOR (REGISTER_W64-1 downto 0);  
    
    -- flank divider params
    signal y1_dividend          : STD_LOGIC_VECTOR (REGISTER_W64-1 downto 0);
    signal threshold_dividend   : STD_LOGIC_VECTOR (REGISTER_W64-1 downto 0);
    signal y1_divisor           : STD_LOGIC_VECTOR (REGISTER_W64-1 downto 0);
    signal y2_divisor           : STD_LOGIC_VECTOR (REGISTER_W64-1 downto 0);
    
    -- general divider prams
   	signal dividend 		: STD_LOGIC_VECTOR ( REGISTER_W64-1 downto 0 );
   	signal divisor 			: STD_LOGIC_VECTOR ( REGISTER_W64-1 downto 0 );
	signal quotient 		: STD_LOGIC_VECTOR ( REGISTER_W64-1 downto 0 );
   	signal divition_result 	: signed ( REGISTER_W32-1 downto 0 );
    
    -- TOF SM   
    type states_tof is (idle, peak_detect, first_peak_detect, peak_to_peak_detect, fe_detect, peak_to_peak_light_detect,
						-- peak
						peak_check_dividend, peak_prepare_dividend_1, peak_prepare_dividend_2, peak_prepare_dividend_3,
						peak_prepare_divisor_1, peak_prepare_divisor_2, peak_check_divisor,				
						-- flank
						flank_check_dividend, flank_prepare_dividend_1, flank_prepare_dividend_2, flank_prepare_dividend_3,
						flank_prepare_divisor_1, flank_prepare_divisor_2, flank_check_divisor,		
						-- divider
        				divide, divide_result, prepare_tof_1, prepare_tof_2, calculate_tof, calculate_tof_alarm, set_result, finish, finish_read); 
    signal state_tof : states_tof; 
    
    -- Amplitude alarm SM   
    type states_amplitude_alarm is (idle, alarm, set_alarm,finish); 
    signal state_amplitude_alarm : states_amplitude_alarm; 
    
    -- Amplitude GATE_TREADY  
    type states_gate_tready is (idle, wait_tvalid, wait_tlast, wait_end); 
    signal state_gate_tready : states_gate_tready; 
    
    -- Divider  
	component divisor_complement_64_64 is
		generic (
			wait_cycles   : integer    := 8
		);
		port (
			aclk            : in std_logic;
			aresetn         : in std_logic;
			DIVIDEND        : in std_logic_vector(63 downto 0);
			DIVISOR         : in std_logic_vector(63 downto 0);
			DIVISION_START  : in std_logic;
			DIVISION_VALID  : out std_logic;
			DIVISION_RESULT : out std_logic_vector(63 downto 0)
		);
	end component divisor_complement_64_64;
    
	signal division_start : STD_LOGIC;
	signal division_valid : STD_LOGIC; 
    
    -- tof
    signal tof_i                    : SIGNED ( REGISTER_W32-1 downto 0 );
      
    -- amp alarm
    signal gate_amp_valid           : STD_LOGIC; 
    signal gate_amplitude_alarm_crossed : STD_LOGIC;
      
    -- correlation
    signal gate_correlation_tdata_i1   : STD_LOGIC_VECTOR (REGISTER_W48-1 downto 0);
    signal gate_correlation_tlast_i1   : STD_LOGIC;
    signal gate_correlation_tvalid_i1  : STD_LOGIC;
    
    signal gate_correlation_tdata_i2   : STD_LOGIC_VECTOR (REGISTER_W48-1 downto 0);
    signal gate_correlation_tlast_i2   : STD_LOGIC;
    signal gate_correlation_tvalid_i2  : STD_LOGIC;
    
    signal gate_on_i           : STD_LOGIC;
    
    -- tof alarm min max value
    signal gate_tof_alarm_min_value_i : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal gate_tof_alarm_max_value_i : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

	signal gate_start_i 	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal gate_rx_delay_i 	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal gate_tx_delay_i 	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal gate_peak_tof_i 	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	
	signal peak_corr_counter 	: UNSIGNED(REGISTER_W32-1 downto 0);

begin

	-- Rect negative value of DATA_IN
	process(aclk)   
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then 
                gate_tvalid_i <= '0';
                gate_tlast_i <= '0';
                gate_tdata_rect <= (others => '0');
                gate_tdata_i <= (others => '0');
            else
                gate_tvalid_i <= GATE_TVALID;
                gate_tlast_i <= GATE_TLAST;
                if(gate_tvalid = '1') then
                    if (GATE_TDATA(47) = '0') then
                        gate_tdata_rect <= GATE_TDATA;
                    else
                        gate_tdata_rect <= std_logic_vector(not signed(GATE_TDATA) + 1);
                    end if;
                    gate_tdata_i <= GATE_TDATA;
                else
                    gate_tdata_rect <= (others => '0');
                    gate_tdata_i <= (others => '0');
                end if;
            end if;
        end if;
    end process;
	
	-- Delay GATE_DATA_IN_TDATA_RECT and GATE_DATA_IN_TVALID_i
	process(aclk)   
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then 
                gate_tdata_i1 <= (others => '0');
                gate_tdata_i2 <= (others => '0');
                gate_tvalid_i1 <= '0';
                gate_tlast_i1 <= '0';
                tof_counter_sample <= (others => '0');
            else
                if(unsigned(GATE_TOF_ALGORITHM) /= TOF_ALGORITHM_FLANK and 
                   unsigned(GATE_TOF_ALGORITHM) /= TOF_ALGORITHM_PEAK_PEAK and 
                   unsigned(GATE_TOF_ALGORITHM) /= TOF_ALGORITHM_FIRST_PEAK) then
                    gate_tdata_i1 <= gate_tdata_rect;
                else
                    gate_tdata_i1 <= gate_tdata_i;
                end if;
                gate_tdata_i2 <= gate_tdata_i1;
                gate_tvalid_i1 <= gate_tvalid_i;
                gate_tlast_i1 <= gate_tlast_i;
                if(gate_tvalid_i1 = '1') then
                    tof_counter_sample <= tof_counter_sample + 1;
                else
                    tof_counter_sample <= (others => '0');
                end if; 
            end if;
        end if;
    end process;
    
	-- tready
    tready_process:process(aclk)   
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then 
                -- General
                GATE_TREADY <= '0';
                -- state
                state_gate_tready <= idle;  
            else
                case state_gate_tready is                    
                    when idle=>
                        if(gate_on_i = '1') then
                            GATE_TREADY <= '1';
                            if(GATE_TVALID = '1') then
                                state_gate_tready <= wait_tlast;
                            end if;
                        else
                            GATE_TREADY <= '0';
                        end if;
                    when wait_tlast=>
                        if(GATE_TLAST = '1') then
                            state_gate_tready <= wait_end; 
                        end if;
                    when wait_end=>
                        GATE_TREADY <= '0';
                        if(state_tof = idle) then
                            state_gate_tready <= idle;
                        end if;
                    when others =>
                        null;
                end case;
            end if;
        end if;
    end process;
                   
                   
    CORRELATION_EN <= '1' when (GATE_EN = '1' and unsigned(GATE_TOF_ALGORITHM) = TOF_ALGORITHM_PEAK_PEAK and  (unsigned(GATE_WIDTH) > 0)) else
                      '0' ; 
                      
    gate_on_i <= '1' when GATE_EN = '1' and unsigned(GATE_WIDTH) > 0 else '0';
    GATE_ON <= gate_on_i;
    
    gate_tof_alarm_min_value_i  <= GATE_TOF_ALARM_MIN_VALUE(REGISTER_W32-1 downto 10) & "0000000000"; 
    gate_tof_alarm_max_value_i  <= GATE_TOF_ALARM_MAX_VALUE(REGISTER_W32-1 downto 10) & "0000000000"; 
      
	-- Peak Detector SM
	main_process:process(aclk)   
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then 
                -- General
                GATE_END                <= '0';
                GATE_TOF                <= (others => '0');
                GATE_TOF_AVERAGED       <= (others => '0');
                GATE_TOF_ALARM          <= std_logic_vector(to_unsigned(ALARM_CROSSING_NONE, GATE_TOF_ALARM'length)); 
                GATE_TOF_ALARM_DIO <= '0'; -- NEW ADDED
                GATE_CORRELATION_TREADY <= '0';
                gate_threshold          <= (others => '0');
                gate_threshold_sample   <= (others => '0');
                -- divider
                division_start <= '0';
                -- fft
                fft_en_i <= '0';
                cc_en_i <= '0';
                fft_gate_end_ack <= '0';
                -- state
                state_tof <= idle;  
            else
                case state_tof is                    
                    when idle=>  
                        fft_en_i <= FFT_EN;  
                        cc_en_i <= CC_EN;                
                        if(gate_on_i = '1') then
                            if(GATE_TVALID = '1') then
                                peak_i <= (others => '0');
                                peak_plus_i <= (others => '0');
                                peak_minus_i <= (others => '0');
                                peak_tof_i <= (others => '0');
                                ef_y1 <= (others => '0');
                                ef_y2 <= (others => '0');
                                corr_counter <= (others => '0');
                                peak_corr_counter <= (others => '0');
                                peak_plus_flag <= '0';
                                fft_gate_end_ack <= '0';
                                if(unsigned(GATE_TOF_ALGORITHM) = TOF_ALGORITHM_PEAK) then -- peak
                                    state_tof <= peak_detect;
                                elsif(unsigned(GATE_TOF_ALGORITHM) = TOF_ALGORITHM_FLANK) then -- flank edge
                                    state_tof <= fe_detect;
                                elsif(unsigned(GATE_TOF_ALGORITHM) = TOF_ALGORITHM_PEAK_PEAK) then -- peak to peak
                                    state_tof <= peak_to_peak_detect; 
                                elsif(unsigned(GATE_TOF_ALGORITHM) = TOF_ALGORITHM_FIRST_PEAK) then -- first peak
                                    state_tof <= first_peak_detect; 
                                elsif(unsigned(GATE_TOF_ALGORITHM) = TOF_ALGORITHM_PEAK_PEAK_LIGHT) then
                                    peak_peak_first_flag <= '1';
                                    peak_i2 <= (others => '0');
                                    peak_plus_i2 <= (others => '0');
                                    peak_minus_i2 <= (others => '0');
                                    peak_tof_i2 <= (others => '0');
                                    gate_width_half <= std_logic_vector(unsigned(GATE_WIDTH) srl 1);                                
                                    state_tof <= peak_to_peak_light_detect;
                                else
                                    state_tof <= peak_detect;
                                end if;
                            end if;
                        else
                            GATE_TOF            <= (others => '0');
                            GATE_TOF_AVERAGED   <= (others => '0');
                            GATE_TOF_ALARM      <= std_logic_vector(to_unsigned(ALARM_CROSSING_NONE, GATE_TOF_ALARM'length)); 
                            GATE_TOF_ALARM_DIO <= '0'; -- NEW ADDED
                        end if;
                    when peak_detect =>
                        if(gate_tvalid_i1 = '1') then
                          	if (unsigned(gate_tdata_i1) > unsigned(peak_i)) then
                            	peak_i <= gate_tdata_i1;
                                peak_minus_i <= gate_tdata_i2;
                                peak_plus_i <= gate_tdata_rect;
                                peak_tof_i <= std_logic_vector(tof_counter_sample);   
                                gate_threshold <= gate_tdata_i1(REGISTER_W16-1 downto 0);
                                gate_threshold_sample <= std_logic_vector(unsigned(GATE_START)+tof_counter_sample-
                                    unsigned(GATE_TX_DELAY(15 downto 0))+unsigned(GATE_RX_DELAY(15 downto 0)));
                            end if;                                                    
                            if(gate_tlast_i1 = '1' or tof_counter_sample >= (unsigned(GATE_WIDTH)-1)) then
                               state_tof <= peak_check_dividend;
                            end if;  
                        else
                            state_tof <= idle;                
                        end if;  
                    when peak_to_peak_light_detect => 
                        if(gate_tvalid_i1 = '1') then
                            if(peak_peak_first_flag = '1') then
                                if (unsigned(gate_tdata_i1) > unsigned(peak_i)) then
                                    peak_i <= gate_tdata_i1;
                                    peak_minus_i <= gate_tdata_i2;
                                    peak_plus_i <= gate_tdata_rect;
                                    peak_tof_i <= std_logic_vector(tof_counter_sample);    
                                    gate_threshold <= gate_tdata_i1(REGISTER_W16-1 downto 0);
                                    gate_threshold_sample <= std_logic_vector(unsigned(GATE_START)+tof_counter_sample-
                                            unsigned(GATE_TX_DELAY(15 downto 0))+unsigned(GATE_RX_DELAY(15 downto 0)));
                                end if;   
                                if(tof_counter_sample >= (unsigned(gate_width_half)-1)) then
                                    peak_peak_first_flag <= '0';
                                end if;  
                            else
                                if (unsigned(gate_tdata_i1) > unsigned(peak_i2)) then
                                    peak_i2 <= gate_tdata_i1;
                                    peak_minus_i2 <= gate_tdata_i2;
                                    peak_plus_i2 <= gate_tdata_rect;
                                    peak_tof_i2 <= std_logic_vector(tof_counter_sample);    
                                end if;                            
                            end if;                                                 
                            if(gate_tlast_i1 = '1' or tof_counter_sample >= (unsigned(GATE_WIDTH)-1)) then
                                peak_peak_first_flag <= '1';
                                state_tof <= peak_check_dividend;
                            end if;  
                        else
                            state_tof <= idle;                
                        end if;                  
                    when first_peak_detect =>
                        if(GATE_TVALID_i1 = '1') then 
                            if (signed(GATE_TDATA_i1) > signed(GATE_AMP_THRESHOLD)) then
                                if (signed(GATE_TDATA_i1) >= signed(GATE_TDATA_i2) and (signed(GATE_TDATA_i1) >= signed(GATE_TDATA_i))) then
                                    peak_i <= GATE_TDATA_i1;
                                    peak_minus_i <= GATE_TDATA_i2;
                                    peak_plus_i <= GATE_TDATA_i;
                                    peak_tof_i <= std_logic_vector(tof_counter_sample);  
                                    gate_threshold <= GATE_TDATA_i1(REGISTER_W16-1 downto 0);
                                    gate_threshold_sample <= std_logic_vector(unsigned(GATE_START)+tof_counter_sample-
                                            unsigned(GATE_TX_DELAY(15 downto 0))+unsigned(GATE_RX_DELAY(15 downto 0)));
                                    state_tof <= peak_check_dividend;  
                                end if; 
                            end if; 
                            if(GATE_TLAST_i1 = '1' or tof_counter_sample >= (unsigned(GATE_WIDTH)-1)) then
                                state_tof <= peak_check_dividend;
                            end if;  
                        else
                            state_tof <= idle;               
                        end if; 
                    when peak_to_peak_detect =>
                        GATE_CORRELATION_TREADY <= '1';  
                        if(gate_correlation_tvalid_i1 = '1') then
                            corr_counter <= corr_counter + 1;
                            if(peak_plus_flag = '1' ) then
                                peak_plus_flag <= '0';
                                peak_plus_i <= gate_correlation_tdata_i1;
                            end if;
                        end if;
                        if ((signed(gate_correlation_tdata_i1) >= 0) and 
                            (signed(gate_correlation_tdata_i1) > signed(gate_correlation_tdata_i2)) and 
                            (signed(gate_correlation_tdata_i1) > signed(gate_correlation_tdata))
                            ) then
                            peak_corr_counter <= peak_corr_counter + 1; 
                        end if;
                        if((corr_counter >= unsigned(GATE_TOF_MIN_THICKNESS)) and
                           (peak_corr_counter >= PEAK_PEAK_COUNTER)
                           ) then
                            if (signed(gate_correlation_tdata_i1) > signed(peak_i)) then
                                peak_i       <= gate_correlation_tdata_i1;
                                peak_minus_i <= gate_correlation_tdata_i2;
                                peak_plus_i  <= gate_correlation_tdata;
                                peak_tof_i <= std_logic_vector(corr_counter);
                            end if;
                            if(GATE_CORRELATION_TVALID = '0' and gate_correlation_tvalid_i1 = '1') then
                                peak_plus_flag <= '1';
                            else
                                peak_plus_flag <= '0';
                            end if;
                           -- debug
                           --peak_minus <= CORRELATION_TDATA;
                           --peak_plus <= CORRELATION_TDATA_i2;
                        end if;
                        if(gate_correlation_tlast_i1 = '1') then
                            state_tof <= peak_check_dividend;                    
                        end if;													
					when peak_check_dividend =>
						GATE_CORRELATION_TREADY <= '0';													   
                        if(peak_minus_i = peak_plus_i) then
                            divition_result <= (others => '0'); 
							state_tof <= prepare_tof_1;											  
                        else
							state_tof <= peak_prepare_dividend_1;											  
                        end if;													   
					when peak_prepare_dividend_1 =>	
						peak_minus_dividend <= std_logic_vector(resize(signed(peak_minus_i), peak_minus_dividend'length));   
						peak_plus_dividend <= std_logic_vector(resize(signed(peak_plus_i), peak_plus_dividend'length)); 	
						state_tof <= peak_prepare_dividend_2;		
					when peak_prepare_dividend_2 =>
						peak_minus_dividend <= peak_minus_dividend(REGISTER_W64-1-9 downto 0) & "000000000";	
						peak_plus_dividend <= peak_plus_dividend(REGISTER_W64-1-9 downto 0) & "000000000";	
						state_tof <= peak_prepare_dividend_3;											  
					when peak_prepare_dividend_3	=>
                        dividend <= std_logic_vector(signed(peak_minus_dividend) - signed(peak_plus_dividend));
						state_tof <= peak_prepare_divisor_1;											  
					when peak_prepare_divisor_1 =>
						peak_minus_divisor <= std_logic_vector(resize(signed(peak_minus_i), peak_minus_divisor'length)); 
						peak_plus_divisor <= std_logic_vector(resize(signed(peak_plus_i), peak_plus_divisor'length)); 	
						state_tof <= peak_prepare_divisor_2;		
					when peak_prepare_divisor_2 =>												 
                        divisor <= std_logic_vector(signed(peak_minus_divisor) + signed(peak_plus_divisor) - signed(peak_i) - signed(peak_i));
						state_tof <= peak_check_divisor;												 
					when peak_check_divisor => 
						if(signed(divisor) = 0) then
                            divition_result <= (others => '0'); 
							state_tof <= prepare_tof_1;											 
						else
							state_tof <= divide;												 
						end if;																							
                    when fe_detect =>
                        if(gate_tvalid_i1 = '1') then 
                            if ((signed(gate_tdata_i) >= signed(GATE_AMP_THRESHOLD)) and (signed(gate_tdata_i1) <= signed(GATE_AMP_THRESHOLD))) then
                                ef_y1 <= gate_tdata_i1;
                                ef_y2 <= gate_tdata_i;  
                                peak_tof_i <= std_logic_vector(tof_counter_sample); 
                                gate_threshold <= gate_tdata_i1(REGISTER_W16-1 downto 0);
                                gate_threshold_sample <= std_logic_vector(unsigned(GATE_START)+tof_counter_sample-
                                    unsigned(GATE_TX_DELAY(15 downto 0))+unsigned(GATE_RX_DELAY(15 downto 0)));
                                state_tof <= flank_check_dividend; 
                             end if; 
                             if(gate_tlast_i1 = '1' or tof_counter_sample >= (unsigned(GATE_WIDTH)-1)) then
                                state_tof <= flank_check_dividend;
                             end if;  
                        else
                            state_tof <= idle;                  
                        end if;   																												 
					when flank_check_dividend =>
						GATE_CORRELATION_TREADY <= '0';													   
                        if(GATE_AMP_THRESHOLD = ef_y1) then
                            divition_result <= (others => '0'); 
							state_tof <= prepare_tof_1;											  
                        else
							state_tof <= flank_prepare_dividend_1;											  
                        end if;													   
					when flank_prepare_dividend_1 =>	
						y1_dividend <= std_logic_vector(resize(signed(ef_y1), y1_dividend'length));   
						threshold_dividend <= std_logic_vector(resize(signed(GATE_AMP_THRESHOLD), threshold_dividend'length)); 	
						state_tof <= flank_prepare_dividend_2;		
					when flank_prepare_dividend_2 =>
						y1_dividend <= y1_dividend(REGISTER_W64-1-10 downto 0) & "0000000000";	
						threshold_dividend <= threshold_dividend(REGISTER_W64-1-10 downto 0) & "0000000000";	
						state_tof <= flank_prepare_dividend_3;											 
					when flank_prepare_dividend_3	=>
                        dividend <= std_logic_vector(signed(threshold_dividend) - signed(y1_dividend));
						state_tof <= flank_prepare_divisor_1;	
					when flank_prepare_divisor_1 =>
						y1_divisor <= std_logic_vector(resize(signed(ef_y1), y1_divisor'length)); 
						y2_divisor <= std_logic_vector(resize(signed(ef_y2), y2_divisor'length)); 
						state_tof <= flank_prepare_divisor_2;		
					when flank_prepare_divisor_2 =>												 
                        divisor <= std_logic_vector(signed(y2_divisor) - signed(y1_divisor));
						state_tof <= flank_check_divisor;												 
					when flank_check_divisor => 
						if(signed(divisor) = 0) then
                            divition_result <= (others => '0'); 
							state_tof <= prepare_tof_1;											 
						else
							state_tof <= divide;												 
						end if;										  									
                    when divide =>
                    	if(division_valid = '1') then
							division_start <= '0';	
							state_tof <= divide_result;										 
						else
							division_start <= '1';											 
						end if;                         
                    when divide_result =>
						divition_result <= signed(quotient(REGISTER_W32-1 downto 0));
						state_tof <= prepare_tof_1;	
					when prepare_tof_1 =>												 
						gate_start_i <= std_logic_vector(resize(unsigned(GATE_START), gate_start_i'length)); 
						gate_rx_delay_i <= std_logic_vector(resize(unsigned(GATE_RX_DELAY), gate_rx_delay_i'length)); 
						gate_tx_delay_i <= std_logic_vector(resize(unsigned(GATE_TX_DELAY), gate_tx_delay_i'length)); 
						gate_peak_tof_i <= std_logic_vector(resize(unsigned(peak_tof_i), gate_peak_tof_i'length)); 										
						state_tof <= prepare_tof_2;	
					when prepare_tof_2 =>	
						gate_start_i <= gate_start_i(REGISTER_W32-1-10 downto 0) & "0000000000";	
						gate_rx_delay_i <= gate_rx_delay_i(REGISTER_W32-1-10 downto 0) & "0000000000";	
						gate_tx_delay_i <= gate_tx_delay_i(REGISTER_W32-1-10 downto 0) & "0000000000";	
						gate_peak_tof_i <= gate_peak_tof_i(REGISTER_W32-1-10 downto 0) & "0000000000";											   
						state_tof <= calculate_tof;								  
                    when calculate_tof =>
                        if(signed(peak_tof_i) = 0) then
                            tof_i <= (others => '0');
                            state_tof <= calculate_tof_alarm;
                        else
                            if(unsigned(GATE_TOF_ALGORITHM) = TOF_ALGORITHM_PEAK_PEAK) then
                                tof_i <= signed(gate_peak_tof_i) + divition_result;
                                state_tof <= calculate_tof_alarm;
                            elsif(unsigned(GATE_TOF_ALGORITHM) = TOF_ALGORITHM_PEAK_PEAK_LIGHT and peak_peak_first_flag = '1') then                             
								tof_i <=  signed(gate_peak_tof_i) + divition_result;  
                                peak_i <= peak_i2;
                                peak_minus_i <= peak_minus_i2;
                                peak_plus_i <= peak_plus_i2;
                                peak_tof_i <= peak_tof_i2;  
                                peak_peak_first_flag <= '0';
                                state_tof <= peak_check_dividend;   
                            elsif(unsigned(GATE_TOF_ALGORITHM) = TOF_ALGORITHM_PEAK_PEAK_LIGHT and peak_peak_first_flag = '0') then 
								tof_i <= signed(gate_peak_tof_i) + divition_result - tof_i;  
								state_tof <= calculate_tof_alarm;               
                            else
								tof_i <= signed(gate_start_i) + 
										 signed(gate_peak_tof_i) +
										 signed(gate_rx_delay_i) -
										 signed(gate_tx_delay_i) +
										 divition_result;
							    state_tof <= calculate_tof_alarm;
                            end if;
                        end if;
                    when calculate_tof_alarm =>
                        if(GATE_TOF_ALARM_EN = '1') then
                            if(unsigned(GATE_TOF_ALARM_CROSSING) = ALARM_CROSSING_ABOVE) then
                                if(tof_i > signed(gate_tof_alarm_max_value_i)) then
                                    GATE_TOF_ALARM <= std_logic_vector(to_unsigned(ALARM_CROSSING_ABOVE, GATE_TOF_ALARM'length));
                                    GATE_TOF_ALARM_DIO <= '1'; -- NEW ADDED 
                                else
                                    GATE_TOF_ALARM_DIO <= '0'; -- NEW ADDED
                                end if;
                            elsif(unsigned(GATE_TOF_ALARM_CROSSING) = ALARM_CROSSING_BELOW) then
                                if(tof_i < signed(gate_tof_alarm_min_value_i)) then
                                    GATE_TOF_ALARM <= std_logic_vector(to_unsigned(ALARM_CROSSING_BELOW, GATE_TOF_ALARM'length));
                                    GATE_TOF_ALARM_DIO <= '1'; -- NEW ADDED
                                else
                                    GATE_TOF_ALARM_DIO <= '0'; -- NEW ADDED
                                end if;    
                            elsif(unsigned(GATE_TOF_ALARM_CROSSING) = ALARM_CROSSING_BOTH) then
                                if(tof_i >= signed(gate_tof_alarm_min_value_i) and tof_i <= signed(gate_tof_alarm_max_value_i)) then
                                    GATE_TOF_ALARM <= std_logic_vector(to_unsigned(ALARM_CROSSING_BOTH, GATE_TOF_ALARM'length));
                                    GATE_TOF_ALARM_DIO <= '0'; -- NEW ADDED
                                else
                                    GATE_TOF_ALARM_DIO <= '1'; -- NEW ADDED
                                end if;
                            else
                                GATE_TOF_ALARM <= std_logic_vector(to_unsigned(ALARM_CROSSING_NONE, GATE_TOF_ALARM'length));
                                GATE_TOF_ALARM_DIO <= '0'; -- NEW ADDED
                            end if; 
                        else
                            GATE_TOF_ALARM <= std_logic_vector(to_unsigned(ALARM_CROSSING_NONE, GATE_TOF_ALARM'length));
                            GATE_TOF_ALARM_DIO <= '0'; -- NEW ADDED
                        end if;   
                        state_tof <= set_result;             
                    when set_result =>
                        GATE_TOF <= std_logic_vector(tof_i);
                        GATE_TOF_AVERAGED <= std_logic_vector(tof_i); -- TODO
                        state_tof <= finish;
                    when finish => 
                        if((GATE_AMP_VALID = '1' or GATE_AMP_THRESHOLD_ALARM_EN = '0') and gate_max_min_end = '1') then
                            if(READ_GATE = '1') then 
                                GATE_END <= '0';
                                fft_gate_end_ack <= '0';
                                state_tof <= idle;
                            else
                                if(fft_en_i = '1' or cc_en_i = '1') then 
                                    if(fft_gate_end = '1') then
                                        GATE_END <= '1';  
                                    end if;
                                    fft_gate_end_ack <= fft_gate_end;                         
                                else
                                    GATE_END <= '1';
                                end if;
                            end if;
                        end if;              
                    when others =>
                        null;                   
                end case;   
            end if;                                                                                         
        end if;
    end process;

    -- signed divider																		   
	divisor_complement_64_64_inst : divisor_complement_64_64
	port map(aclk => aclk,
		aresetn => aresetn, 
		DIVIDEND => dividend,
		DIVISOR => divisor,
		DIVISION_START => division_start,
		-- Outputs
		DIVISION_VALID => division_valid,
		DIVISION_RESULT => quotient
   );	
          
    -- Amplitude alarm
	amplitude_alarm_process:process(aclk)   
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                gate_amplitude_alarm_crossed <= '0';
                -- output
                GATE_AMP_ALARM <= std_logic_vector(to_unsigned(ALARM_CROSSING_NONE, GATE_AMP_ALARM'length));
                GATE_AMP_ALARM_DIO <= '0'; -- NEW ADDED 
                GATE_AMP_VALID <= '0';        
            else
                case state_amplitude_alarm is                    
                    when idle =>   
                        if(GATE_AMP_THRESHOLD_ALARM_EN = '1' and gate_on_i = '1') then
                            if(GATE_TVALID = '1') then
                                state_amplitude_alarm <= alarm;
                                gate_amplitude_alarm_crossed <= '0';
                            end if;
                        else
                            GATE_AMP_ALARM <= std_logic_vector(to_unsigned(ALARM_CROSSING_NONE, GATE_AMP_ALARM'length));
                            GATE_AMP_ALARM_DIO <= '0'; -- NEW ADDED 
                        end if;
                    when alarm=>
                        if(GATE_TVALID_i1 = '1') then
                            if(signed(GATE_TDATA_i1) > signed(GATE_AMP_THRESHOLD)) then
                                gate_amplitude_alarm_crossed <= '1';
                            end if;                 
                            if(GATE_TLAST_i1 = '1' or tof_counter_sample >= (unsigned(GATE_WIDTH)-1)) then
                                state_amplitude_alarm <= set_alarm;
                            end if;                  
                        end if;   
                    when set_alarm => 
                        if(unsigned(GATE_AMP_THRESHOLD_ALARM_CROSSING) = ALARM_CROSSING_ABOVE) then
                            if( gate_amplitude_alarm_crossed = '1') then
                                GATE_AMP_ALARM <= std_logic_vector(to_unsigned(ALARM_CROSSING_ABOVE, GATE_AMP_ALARM'length)); 
                                GATE_AMP_ALARM_DIO <= '1'; -- NEW ADDED 
                            else
                                GATE_AMP_ALARM <= std_logic_vector(to_unsigned(ALARM_CROSSING_NONE, GATE_AMP_ALARM'length));  
                                GATE_AMP_ALARM_DIO <= '0'; -- NEW ADDED
                            end if;
                        elsif(unsigned(GATE_AMP_THRESHOLD_ALARM_CROSSING) = ALARM_CROSSING_BELOW) then
                            if( gate_amplitude_alarm_crossed = '1') then
                                GATE_AMP_ALARM <= std_logic_vector(to_unsigned(ALARM_CROSSING_NONE, GATE_AMP_ALARM'length));  
                                GATE_AMP_ALARM_DIO <= '1'; -- NEW ADDED 
                            else
                                GATE_AMP_ALARM <= std_logic_vector(to_unsigned(ALARM_CROSSING_BELOW, GATE_AMP_ALARM'length)); 
                                GATE_AMP_ALARM_DIO <= '0'; -- NEW ADDED
                            end if;
                        else
                            GATE_AMP_ALARM <= std_logic_vector(to_unsigned(ALARM_CROSSING_NONE, GATE_AMP_ALARM'length));
                            GATE_AMP_ALARM_DIO <= '0'; -- NEW ADDED 
                        end if;
                        state_amplitude_alarm <= finish;       
                    when finish =>
                        GATE_AMP_VALID <= '1';                  
                        if(READ_GATE = '1') then
                            GATE_AMP_VALID <= '0'; 
                            state_amplitude_alarm <= idle;
                        end if;        
                    when others=>
                        null;
                end case;
            end if;
        end if;
    end process;
        
    -- Simple correlator output delay
	process(aclk)   
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then 
                gate_correlation_tdata_i1  <= (others => '0');
                gate_correlation_tlast_i1  <= '0'; 
                gate_correlation_tvalid_i1 <= '0'; 
                gate_correlation_tdata_i2  <= (others => '0');
                gate_correlation_tlast_i2  <= '0'; 
                gate_correlation_tvalid_i2 <= '0';
            else
                gate_correlation_tdata_i1  <= gate_correlation_tdata;
                gate_correlation_tlast_i1  <= gate_correlation_tlast; 
                gate_correlation_tvalid_i1 <= gate_correlation_tvalid; 
                gate_correlation_tdata_i2  <= gate_correlation_tdata_i1;
                gate_correlation_tlast_i2  <= gate_correlation_tlast_i1; 
                gate_correlation_tvalid_i2 <= gate_correlation_tvalid_i1;
            end if;
        end if;
    end process;
             
    GATE_MIN <= gate_min_i;
    GATE_MAX <= gate_max_i;
    GATE_THR <= gate_threshold;
    GATE_THR_SAMPLE <= gate_threshold_sample;
      
    -- Max and min of the gate
	max_min_process:process(aclk)   
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then 
                -- output
                gate_max_i <= (others => '0'); 
                gate_min_i <= (others => '0'); 
                GATE_MAX_SAMPLE  <= (others => '0'); 
                GATE_MIN_SAMPLE  <= (others => '0'); 
                gate_max_min_end <= '0';
            else
                if(gate_on_i = '1') then
                    if(gate_tvalid = '1' and gate_tvalid_i= '0') then
                        gate_max_i <= GATE_TDATA(REGISTER_W16-1 downto 0);
                        gate_min_i <= GATE_TDATA(REGISTER_W16-1 downto 0);
                        gate_max_min_end <= '0';
                    else
                        if(GATE_TVALID_i = '1') then
                            if(signed(gate_tdata_i(REGISTER_W16-1 downto 0)) >  signed(gate_max_i)) then
                                gate_max_i <= gate_tdata_i(REGISTER_W16-1 downto 0);
                                GATE_MAX_SAMPLE <= std_logic_vector(unsigned(GATE_START)+tof_counter_sample);
                            end if;
                            if(signed(gate_tdata_i(REGISTER_W16-1 downto 0)) <  signed(gate_min_i)) then
                                gate_min_i <= gate_tdata_i(REGISTER_W16-1 downto 0);
                                GATE_MIN_SAMPLE <= std_logic_vector(unsigned(GATE_START)+tof_counter_sample);
                            end if;
                            if(GATE_TVALID = '0') then
                                gate_max_min_end <= '1';
                            end if;
                        end if;   
                    end if; 
                else
                    GATE_MAX_SAMPLE  <= (others => '0'); 
                    GATE_MIN_SAMPLE  <= (others => '0'); 
                    gate_max_i <= (others => '0'); 
                    gate_min_i <= (others => '0'); 
                end if;
            end if;
        end if;
    end process;
    
    fft_gate_inst: fft_gate 
        generic map(
            REGISTER_W64        => REGISTER_W64,
            REGISTER_W48        => REGISTER_W48,
            REGISTER_W32        => REGISTER_W32,
            REGISTER_W24        => REGISTER_W24,
            REGISTER_W16        => REGISTER_W16,
            REGISTER_W8         => REGISTER_W8,
            FFT_MAX             => FFT_MAX,
            FFT_MAX_LOOP        => FFT_MAX_LOOP,
            BRAM_ADDR_WIDTH     => FFT_BRAM_ADDR_WIDTH,
            BRAM_DATA_WIDTH     => FFT_BRAM_DATA_WIDTH
        )
        port map(
            -- Sync
            aclk         => aclk,
            aresetn      => aresetn,
        
            -- Control
            FFT_TEST     => '0',
            FFT_EN       => fft_en_i, 
            CC_EN        => cc_en_i, 
            
            FFT_WIDTH     => FFT_WIDTH,
            CO_FFT_SEGMENT => FFT_SEGMENT,
            CO_FFT_NFFT => FFT_NFFT,
    
            FFT_MAX_VALUE  => FFT_MAX_VALUE,
            FFT_MAX_SAMPLE => FFT_MAX_SAMPLE,          
            CC_MAX_VALUE  => CC_MAX_VALUE,
            CC_MAX_SAMPLE => CC_MAX_SAMPLE,
            FFT_GATE_END  => fft_gate_end,
            FFT_GATE_END_ACK => fft_gate_end_ack,
            --FFT_CUADRATIC_ERROR => FFT_CUADRATIC_ERROR,
            FFT_COHERENCE  => FFT_COHERENCE,
                
            -- Signal input
            FFT_INPUT_AXIS_TREADY  => fft_input_axis_tready,
            FFT_INPUT_AXIS_TDATA   => GATE_TDATA(REGISTER_W16-1 downto 0),
            FFT_INPUT_AXIS_TLAST   => GATE_TLAST,
            FFT_INPUT_AXIS_TVALID  => GATE_TVALID,
            
            CC_CONFIG_ON_ACK => CC_CONFIG_ON_ACK,
            CC_CONFIG_ON     => CC_CONFIG_ON,  
            CC_CONFIG_ADDR   => CC_CONFIG_ADDR,
            CC_CONFIG_DATA   => CC_CONFIG_DATA,
            
            FFT_STATUS_AXIS_TDATA  => fft_status_axis_tdata,
            FFT_STATUS_AXIS_TUSER  => fft_status_axis_tuser,
            FFT_STATUS_AXIS_TLAST  => fft_status_axis_tlast,
            FFT_STATUS_AXIS_TVALID => fft_status_axis_tvalid
        );

          
end Behavioral;
