library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity motor_pwm_tb is
end;

architecture bench of motor_pwm_tb is

	component motor_pwm is	
		Port ( 
			-- Sync
			aclk 		: in STD_LOGIC;
			aresetn 	: in STD_LOGIC;

			-- General
			PMOD_5HB_EN : in STD_LOGIC;

			-- PWM inputs 
			PWM_CARRIER_PERIOD 	: in STD_LOGIC_VECTOR (31 downto 0);
			PWM_MOD_VALUE 		: in STD_LOGIC_VECTOR (31 downto 0);
			PWM_MOD_DIR 		: in STD_LOGIC;
			PWM_DEAD_TIME 		: in STD_LOGIC_VECTOR (31 downto 0);

			-- PWM outputs
			PWM_EN 	: out STD_LOGIC;
			PWM_DIR : out STD_LOGIC
		);
	end component motor_pwm;

  signal aclk: STD_LOGIC := '0';
  signal aresetn: STD_LOGIC := '0';

  signal PMOD_5HB_EN: STD_LOGIC := '0';

  signal PWM_CARRIER_PERIOD: STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
  signal PWM_MOD_VALUE: STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
  signal PWM_MOD_DIR: STD_LOGIC := '0';
  signal PWM_DEAD_TIME: STD_LOGIC_VECTOR (31 downto 0) := (others => '0');

  signal PWM_EN: STD_LOGIC := '0';
  signal PWM_DIR: STD_LOGIC := '0';

  constant aclk_period: time := 10 ns;
  signal stop_the_clock: boolean;

begin

  stimulus: process
  begin
    aresetn <= '0';  
    wait for aclk_period*10;
    aresetn <= '1';
	PWM_CARRIER_PERIOD  <= std_logic_vector(to_unsigned(512, PWM_CARRIER_PERIOD'length));
	PWM_MOD_VALUE  <= std_logic_vector(to_unsigned(256, PWM_MOD_VALUE'length));
	PWM_DEAD_TIME  <= std_logic_vector(to_unsigned(128, PWM_DEAD_TIME'length));												
    PWM_MOD_DIR <= '0';
    wait for 100 ns;
    PMOD_5HB_EN <= '1';
    wait for aclk_period*25000;
    PWM_MOD_DIR <= '1';
    wait for aclk_period*25000;
	PWM_MOD_VALUE  <= std_logic_vector(to_unsigned(128, PWM_MOD_VALUE'length));											   
    wait for aclk_period*25000;
	PWM_MOD_VALUE  <= std_logic_vector(to_unsigned(128+256, PWM_MOD_VALUE'length));												   
    wait for aclk_period*25000;
    PWM_MOD_DIR <= '0';
    wait for aclk_period*25000;
    PMOD_5HB_EN <= '0';
    wait for aclk_period*25000;
    PMOD_5HB_EN <= '1';
    wait for aclk_period*25000;
	PWM_MOD_VALUE  <= std_logic_vector(to_unsigned(1024, PWM_MOD_VALUE'length));												   
    wait for aclk_period*25000;
	PWM_MOD_VALUE  <= std_logic_vector(to_unsigned(256, PWM_MOD_VALUE'length));												   
    wait for aclk_period*25000;
    stop_the_clock <= true;
    wait;
  end process;

	-- aclk process 
    aclk_process :process
    begin
        aclk <= not aclk;
        wait for aclk_period/2;
        aclk <= not aclk;
        wait for aclk_period/2;
    end process;
			
  uut: motor_pwm port map ( 
	  						  	aclk          		=> aclk,
                              	aresetn          	=> aresetn,
                              	PMOD_5HB_EN      	=> PMOD_5HB_EN,
                             	PWM_CARRIER_PERIOD	=> PWM_CARRIER_PERIOD,
                              	PWM_MOD_VALUE      	=> PWM_MOD_VALUE,
                              	PWM_MOD_DIR        	=> PWM_MOD_DIR,
                              	PWM_DEAD_TIME      	=> PWM_DEAD_TIME,
                              	PWM_EN         		=> PWM_EN,
                              	PWM_DIR        		=> PWM_DIR 
  							);

end;
