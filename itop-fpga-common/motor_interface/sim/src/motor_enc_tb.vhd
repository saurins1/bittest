library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity motor_enc_tb is
end;

architecture bench of motor_enc_tb is

  component motor_enc is
    Port ( 
		-- Sync
		aclk 		: in STD_LOGIC; 
        aresetn     : in STD_LOGIC; 
	
		-- ENCODER inputs
		ENC_RESET 			: in STD_LOGIC; 
		ENC_POSITION_MAX 	: in STD_LOGIC_VECTOR (31 downto 0);
        ENC_CHA 			: in STD_LOGIC; 
        ENC_CHB     		: in STD_LOGIC; 
		
        -- ENCODER outputs
        ENC_DIR     				: out STD_LOGIC_VECTOR (1 downto 0);
		
        ENC_SPEED_1       			: out STD_LOGIC_VECTOR (31 downto 0);
		ENC_SPEED_2       			: out STD_LOGIC_VECTOR (31 downto 0);
        ENC_POSITION_RELATIVE    	: out STD_LOGIC_VECTOR (31 downto 0);
		ENC_POSITION_ABSOLUTE    	: out STD_LOGIC_VECTOR (31 downto 0)
	);	
  end component motor_enc;

  signal aclk: STD_LOGIC := '0';
  signal aresetn: STD_LOGIC := '0';

  signal PMOD_5HB_EN: STD_LOGIC := '0';

  signal ENC_RESET: STD_LOGIC := '0';
  signal ENC_POSITION_MAX: STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
  signal ENC_CHA: STD_LOGIC := '0';
  signal ENC_CHB: STD_LOGIC := '0';

  signal ENC_DIR: STD_LOGIC_VECTOR (1 downto 0);

  signal ENC_SPEED_1: STD_LOGIC_VECTOR (31 downto 0);
  signal ENC_SPEED_2: STD_LOGIC_VECTOR (31 downto 0);
  signal ENC_POSITION_RELATIVE: STD_LOGIC_VECTOR (31 downto 0);
  signal ENC_POSITION_ABSOLUTE: STD_LOGIC_VECTOR (31 downto 0);

  constant aclk_period: time := 10 ns;
  signal stop_the_clock: boolean;
  
  --constant encoder_period: time := 3600 us;
  --signal encoder_period: time := 360 us;
  signal encoder_period: time := 13020 us; --3255*4
  
  signal dir_control: STD_LOGIC := '0';

begin

  stimulus: process
  begin
    aresetn <= '0';
    PMOD_5HB_EN <= '0';
    dir_control <= '0';
    encoder_period <= 3255 us * 4;
	ENC_POSITION_MAX  <= std_logic_vector(to_unsigned(57*4-1, ENC_POSITION_MAX'length));	
    wait for 100 ns;
    aresetn <= '1';
    wait for aclk_period*25000;
    PMOD_5HB_EN <= '1';
    wait for encoder_period*60;
    PMOD_5HB_EN <= '0';
    ENC_RESET <= '1';
    wait for aclk_period*1;
    ENC_RESET <= '0';
    wait for encoder_period*10;
    PMOD_5HB_EN <= '1';
    encoder_period <= 6510 us * 4;
    wait for encoder_period*60;
    PMOD_5HB_EN <= '0';
    ENC_RESET <= '1';
    wait for aclk_period*1;
    ENC_RESET <= '0';
    wait for encoder_period*60;
    PMOD_5HB_EN <= '1';
    encoder_period <= 1627 us * 4;
    wait for encoder_period*60;
    dir_control <= '1';
    wait;
  end process;

	-- aclk process 
    aclk_process :process
    begin
        aclk <= not aclk;
        wait for aclk_period/2;
        aclk <= not aclk;
        wait for aclk_period/2;
    end process;
  
  cha_generation: process
  begin
    if (PMOD_5HB_EN = '1') then
        if(dir_control = '0') then
            ENC_CHA <= '0';
            wait for encoder_period/4;
            ENC_CHA <= '1';
            wait for encoder_period/2;
            ENC_CHA <= '0';
            wait for encoder_period/4;
        else
            ENC_CHA <= '1';
            wait for encoder_period/2;
            ENC_CHA <= '0';
            wait for encoder_period/2; 
        end if;
    else
        ENC_CHA <= '0';
        wait for encoder_period;
    end if;
  end process;
  
  chb_generation: process
  begin
    if (PMOD_5HB_EN = '1') then
        if(dir_control = '0') then
            ENC_CHB <= '1';
            wait for encoder_period/2;
            ENC_CHB <= '0';
            wait for encoder_period/2;
        else
            ENC_CHB <= '0';
            wait for encoder_period/4;
            ENC_CHB <= '1';
            wait for encoder_period/2;
            ENC_CHB <= '0';
            wait for encoder_period/4;
        end if;
    else
        ENC_CHB <= '0';
        wait for encoder_period;
    end if;
  end process;
		
  uut: motor_enc port map ( 
	  							aclk     				=> aclk,
                             	aresetn     			=> aresetn,
                              	ENC_RESET       		=> ENC_RESET,
                              	ENC_POSITION_MAX 		=> ENC_POSITION_MAX,
                              	ENC_CHA       			=> ENC_CHA,
                              	ENC_CHB     			=> ENC_CHB,
                              	ENC_DIR  				=> ENC_DIR,
	  							ENC_SPEED_1  			=> ENC_SPEED_1,
	  							ENC_SPEED_2  			=> ENC_SPEED_2,
	  							ENC_POSITION_RELATIVE  	=> ENC_POSITION_RELATIVE,
	  							ENC_POSITION_ABSOLUTE  	=> ENC_POSITION_ABSOLUTE
 							);

end;
