library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all; 

entity motor_interface_tb is
end;

architecture bench of motor_interface_tb is

  component motor_interface
      Port (
             clock : in STD_LOGIC;
             reset : in STD_LOGIC;
             en_engine : in STD_LOGIC;
             carrier_period : in STD_LOGIC_VECTOR (23 downto 0);
             mod_value : in STD_LOGIC_VECTOR (23 downto 0);
             mod_dir : in STD_LOGIC;
             dead_time : in STD_LOGIC_VECTOR (15 downto 0);
             pwm_en : out STD_LOGIC;
             pwm_dir : out STD_LOGIC);
  end component;

  signal clock: STD_LOGIC := '0';
  signal reset: STD_LOGIC := '0';
  signal en_engine: STD_LOGIC := '0';
  signal carrier_period: STD_LOGIC_VECTOR (23 downto 0) := (others => '0');
  signal mod_value: STD_LOGIC_VECTOR (23 downto 0) := (others => '0');
  signal mod_dir: STD_LOGIC := '0';
  signal dead_time: STD_LOGIC_VECTOR (15 downto 0) := (others => '0');
  signal pwm_en: STD_LOGIC := '0';
  signal pwm_dir: STD_LOGIC := '0';

  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;

begin

  uut: motor_interface port map ( clock          => clock,
                              reset          => reset,
                              en_engine      => en_engine,
                              carrier_period => carrier_period,
                              mod_value      => mod_value,
                              mod_dir        => mod_dir,
                              dead_time      => dead_time,
                              pwm_en         => pwm_en,
                              pwm_dir        => pwm_dir );

  stimulus: process
  begin
    
    -- Put initialisation code here   
    wait for 100 ns;
    -- Put test bench stimulus code here
    reset <= '1';
    carrier_period <= "000000000000001000000000"; --512
    mod_value <= "000000000000000100000000"; --256
    dead_time <= "0000000010000000"; -- 128
    mod_dir <= '0';
    wait for 100 ns;
    en_engine <= '1';
    wait for clock_period*25000;
    mod_dir <= '1';
    wait for clock_period*25000;
    mod_value <= "000000000000000010000000"; --128
    wait for clock_period*25000;
    mod_value <= "000000000000000110000000"; --256+128
    wait for clock_period*25000;
    mod_dir <= '0';
    wait for clock_period*25000;
    en_engine <= '0';
    wait for clock_period*25000;
    en_engine <= '1';
    wait for clock_period*25000;
    mod_value <= "000000000000010000000000"; --1024
    wait for clock_period*25000;
    mod_value <= "000000000000000100000000"; --256
    wait for clock_period*25000;
    stop_the_clock <= true;
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      clock <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;
