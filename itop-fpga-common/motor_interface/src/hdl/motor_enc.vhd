----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 05/17/2018 10:07:23 AM
-- Design Name: 
-- Module Name: motor enc - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity motor_enc is
    Port ( 
		-- Sync
		aclk 		: in STD_LOGIC; 
        aresetn     : in STD_LOGIC; 
		
		-- ENCODER inputs
		ENC_RESET 			: in STD_LOGIC; 
		ENC_POSITION_MAX 	: in STD_LOGIC_VECTOR (31 downto 0);
        ENC_CHA 			: in STD_LOGIC; 
        ENC_CHB     		: in STD_LOGIC; 
		
        -- ENCODER outputs
        ENC_DIR     				: out STD_LOGIC_VECTOR (1 downto 0);
		
        ENC_SPEED_1       			: out STD_LOGIC_VECTOR (31 downto 0);
		ENC_SPEED_2       			: out STD_LOGIC_VECTOR (31 downto 0);
        ENC_POSITION_RELATIVE    	: out STD_LOGIC_VECTOR (31 downto 0);
		ENC_POSITION_ABSOLUTE    	: out STD_LOGIC_VECTOR (31 downto 0)
	);	
end motor_enc;

architecture Behavioral of motor_enc is
   
	-- Signals Position & Dir
    signal position_relative       : unsigned(31 downto 0) := (others => '0');
    signal position_relative_d1    : unsigned(31 downto 0) := (others => '0');
	signal position_absolute       : signed(31 downto 0) := (others => '0');

    signal dir_i            : std_logic_vector(1 downto 0) := (others => '0');
    signal dir_i_d1         : std_logic_vector(1 downto 0) := (others => '0');

	type states_enc is (s0, s00, s01, s10, s11); 
    signal state_enc : states_enc; 

	-- -- Signals Speed
    signal speed_1        : unsigned(31 downto 0) := (others => '0');
    signal speed_1_cnt    : unsigned(31 downto 0) := (others => '0');

    signal speed_2        : unsigned(31 downto 0) := (others => '0');
    signal speed_2_cnt    : unsigned(31 downto 0) := (others => '0');
	signal speed_2_on     : std_logic := '0';
	signal enc_cha_d1     : std_logic := '0'; 

	type states_enc_speed is (idle, measure_speed); 
    signal state_enc_speed : states_enc_speed; 
  
begin
	
	---- OUTPUTS ----
	
	ENC_DIR 					<= dir_i_d1;

    ENC_POSITION_RELATIVE 		<= std_logic_vector(position_relative);

	ENC_POSITION_ABSOLUTE		<= std_logic_vector(position_absolute);

    ENC_SPEED_1 				<= std_logic_vector(speed_1);

	ENC_SPEED_2 				<= std_logic_vector(speed_2);
	
	---- POSITION & DIR ----

    -- Position & Dir SM
    encoder_pos_dir_SM: process(aclk)
    begin 
		if (rising_edge(aclk)) then
			if (aresetn = '0' or ENC_RESET = '1') then
				state_enc <= s0;                    
				dir_i <= "00";   
				position_relative <= (others => '0');
				position_absolute <= (others => '0');
			else            
			   case state_enc is	
				when s0 =>
					if(ENC_CHA = '0' and ENC_CHB = '0') then
						state_enc <= s00;
					elsif(ENC_CHA = '0' and ENC_CHB = '1') then  
						state_enc <= s01;
					elsif(ENC_CHA = '1' and ENC_CHB = '0') then       
						state_enc <= s10;
					elsif(ENC_CHA = '1' and ENC_CHB = '1') then
						state_enc <= s11;
					end if;                	
				when s00 =>
					if(ENC_CHA = '0' and ENC_CHB = '1') then
						state_enc <= s01;
						if(position_relative = 0) then
                            position_relative <= unsigned(ENC_POSITION_MAX); 
						else
							position_relative <= position_relative - 1;
						end if;
						position_absolute <= position_absolute - 1;		
						dir_i <= "01";
					elsif(ENC_CHA = '1' and ENC_CHB = '0') then  
						state_enc <= s10;
						if(position_relative >= unsigned(ENC_POSITION_MAX)) then
                            position_relative <= (others => '0'); 
						else
							position_relative <= position_relative + 1;
						end if;
						position_absolute <= position_absolute + 1;	
						dir_i <= "00";                   
					end if;
				when s01 =>
					if(ENC_CHA = '1' and ENC_CHB = '1') then
						state_enc <= s11;
						if(position_relative = 0) then
                            position_relative <= unsigned(ENC_POSITION_MAX); 
						else
							position_relative <= position_relative - 1;
						end if;  
						position_absolute <= position_absolute - 1;	
						dir_i <= "01";
					elsif(ENC_CHA = '0' and ENC_CHB = '0') then  
						state_enc <= s00;
						if(position_relative >= unsigned(ENC_POSITION_MAX)) then
                            position_relative <= (others => '0'); 
						else
							position_relative <= position_relative + 1;
						end if;    
						position_absolute <= position_absolute + 1;	
						dir_i <= "00";       
					end if;
				when s10 =>
					 if(ENC_CHA = '0' and ENC_CHB = '0') then
						state_enc <= s00;
						if(position_relative = 0) then
                            position_relative <= unsigned(ENC_POSITION_MAX); 
						else
							position_relative <= position_relative - 1;
						end if;
						position_absolute <= position_absolute - 1;	
						dir_i <= "01";
					elsif(ENC_CHA = '1' and ENC_CHB = '1') then  
						state_enc <= s11;
						if(position_relative >= unsigned(ENC_POSITION_MAX)) then
                            position_relative <= (others => '0'); 
						else
							position_relative <= position_relative + 1;
						end if; 
						position_absolute <= position_absolute + 1;	
						dir_i <= "00";                  
					end if;           
				when s11 =>
					if(ENC_CHA = '1' and ENC_CHB = '0') then
						state_enc <= s10;
						if(position_relative = 0) then
                            position_relative <= unsigned(ENC_POSITION_MAX); 
						else
							position_relative <= position_relative - 1;
						end if; 
						position_absolute <= position_absolute - 1;	
						dir_i <= "01";
					elsif(ENC_CHA = '0' and ENC_CHB = '1') then  
						state_enc <= s01;
						if(position_relative >= unsigned(ENC_POSITION_MAX)) then
                            position_relative <= (others => '0'); 
						else
							position_relative <= position_relative + 1;
						end if;  
						position_absolute <= position_absolute + 1;	
						dir_i <= "00";              
					end if;            
			   end case;
		   	end if;
		end if;
    end process;
						       
    ---- SPEED ----
			
	-- Speed SM
    encoder_speed_1_SM: process(aclk)
    begin 
		if (rising_edge(aclk)) then
			if (aresetn = '0' or ENC_RESET = '1') then
				state_enc_speed <= idle;                      
				speed_1 <= (others => '0');
				position_relative_d1 <= (others => '0');
				speed_1_cnt <= (others => '0');  
				dir_i_d1 <= (others => '0');    
			else
				position_relative_d1 <= position_relative; 
				dir_i_d1 <= dir_i;
				case state_enc_speed is
					when idle =>
						if(position_relative_d1 /= position_relative) then
							speed_1_cnt <= (others => '0');
							state_enc_speed <= measure_speed;    
						end if;               
					when measure_speed =>    
						speed_1_cnt <= speed_1_cnt+1;                       
						if(dir_i /= dir_i_d1) then
							state_enc_speed <= idle;
						elsif(speed_1_cnt = "11111111111111111111111111111111") then
							speed_1_cnt <= (others => '0');
							speed_1 <= (others => '0');
							state_enc_speed <= idle;
						else
							if(position_relative_d1 /= position_relative) then
								speed_1 <= speed_1_cnt+1;  
								speed_1_cnt <= (others => '0');
							end if;
						end if;                           
					when others =>  
						null;
				end case;
			end if;
		end if;
    end process;
			
	-- Calculate speend in function of encoder pulses
    encoder_speed_2: process(aclk)
    begin 
		if (rising_edge(aclk)) then
			if (aresetn = '0' or ENC_RESET = '1') then
				speed_2_on 	<= '0';
				enc_cha_d1 	<= '0';
				speed_2		<= (others => '0');
				speed_2_cnt	<= (others => '0');
			else            
				enc_cha_d1 <= ENC_CHA;
				if(ENC_CHA = '1' and enc_cha_d1 = '0') then
					if(speed_2_on = '0') then
						speed_2_on <= '1';
					else
						speed_2	<= speed_2_cnt+1;
					end if;
					speed_2_cnt	<= (others => '0');	
				else
					speed_2_cnt <= speed_2_cnt + 1;
				end if;
		   	end if;
		end if;
    end process;
			
end Behavioral;
