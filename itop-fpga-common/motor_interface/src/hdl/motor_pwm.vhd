----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 05/17/2018 10:07:23 AM
-- Design Name: 
-- Module Name: motor pwm - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity motor_pwm is	
    Port ( 
		-- Sync
		aclk 		: in STD_LOGIC;
        aresetn 	: in STD_LOGIC;
		
		-- General
        MOTOR_PWM_EN : in STD_LOGIC;
		
		-- PWM inputs 
        PWM_CARRIER_PERIOD 	: in STD_LOGIC_VECTOR (31 downto 0);
        PWM_MOD_VALUE 		: in STD_LOGIC_VECTOR (31 downto 0);
        PWM_MOD_DIR 		: in STD_LOGIC;
        PWM_DEAD_TIME 		: in STD_LOGIC_VECTOR (31 downto 0);
           
		-- PWM outputs
        PWM_EN 	: out STD_LOGIC;
        PWM_DIR : out STD_LOGIC
	);
end motor_pwm;

architecture Behavioral of motor_pwm is

    -- Signals PWM generator
    signal carrier_cnt      : unsigned (31 downto 0) := (others => '0');
    signal pwm              : std_logic := '0';
    signal dir              : std_logic := '0';
    
    -- Signals Dead time control
    signal dir_i1           : std_logic := '0'; 
    signal dir_i2           : std_logic := '0'; 

    signal dt_enable        : std_logic := '0'; 
    signal dead_time_cnt    : unsigned (31 downto 0);  

	-- Dead time SM
    type states_dt is (idle, dead_time_1, dead_time_2); 
    signal state_dt : states_dt; 

	signal pmw_carrier_perior_d1      : STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
    
begin

    ---- PWM GENERATOR ----
         
    -- DEC counter for create carrier_cnt
    carrier_cnt_gen: process(aclk)
	begin
		if (rising_edge(aclk)) then
	   		if (aresetn = '0' or MOTOR_PWM_EN = '0') then
           		carrier_cnt      <= (others => '0');
				pmw_carrier_perior_d1 <= (others => '0');
	   		else 
				pmw_carrier_perior_d1 <= PWM_CARRIER_PERIOD;
				if(PWM_CARRIER_PERIOD /= pmw_carrier_perior_d1) then
					carrier_cnt      <= (others => '0');	
				else
					-- load carrier period when counter reachs '0'    
					if(carrier_cnt = 0) then
						carrier_cnt <= unsigned(PWM_CARRIER_PERIOD);
					else
						carrier_cnt <= carrier_cnt - 1; 
					end if;   
				end if;     
	   		end if;
		end if;
	end process;
			
	-- PWM & DIR internal
    pwm_gen: process(aclk)
    begin
		if (rising_edge(aclk)) then
       		if (aresetn = '0' or MOTOR_PWM_EN = '0') then
           		pwm <= '0';
           		dir <= '0';
       		else
           		dir <= PWM_MOD_DIR;
           		if(unsigned(PWM_MOD_VALUE) >= unsigned(PWM_CARRIER_PERIOD) or unsigned(PWM_MOD_VALUE) = 0) then
                	pwm <= '0';
           		else
                	if(unsigned(PWM_MOD_VALUE) >= carrier_cnt) then
                    	pwm <= '0';
                	else
                    	pwm <= '1';
                	end if;
           		end if;        
       		end if;
		end if;
    end process;
		   
	-- PWM & DIR final
    pwm_final_gen: process(aclk)
    begin 
		if (rising_edge(aclk)) then
       		if (aresetn = '0' or MOTOR_PWM_EN = '0') then
           		PWM_EN  <= '0';
           		PWM_DIR <= '0';
       		else
           		PWM_DIR <= dir_i2;
           		if(dt_enable = '1') then              
               		PWM_EN <= '0';
           		else
               		PWM_EN <= pwm;
           		end if;   
       		end if;
		end if;
    end process;
    
    ---- DEADTIME CONTROL ----

    -- Dead time generator
    dt_enable_gen: process(aclk)
    begin 
		if (rising_edge(aclk)) then
       		if (aresetn = '0' or MOTOR_PWM_EN = '0') then
           		dir_i1 <= '0';
           		dir_i2 <= '0';
           		dead_time_cnt <= (others => '0');
           		state_dt <= idle;
       		else        
           		dir_i1 <= dir;        
           		case state_dt is		
            		when idle =>  
                		if(dir /= dir_i1) then    
                    		dt_enable <='1';                                
                    		dead_time_cnt <= (others => '0');
                    		state_dt <= dead_time_1;
                		end if;            
            		when dead_time_1 =>
                		dead_time_cnt <= dead_time_cnt + 1;    
                		if(dead_time_cnt >= unsigned(PWM_DEAD_TIME)) then             
                    		dead_time_cnt <= (others => '0');
                    		dir_i2 <= dir_i1;
                    		state_dt <= dead_time_2;
               			end if;
            		when dead_time_2 =>
                		dead_time_cnt <= dead_time_cnt + 1;    
                		if(dead_time_cnt >= unsigned(PWM_DEAD_TIME)) then
                    		dead_time_cnt <= (others => '0');
                    		dt_enable <='0';
                    		state_dt <= idle;
                		end if;               
           		end case;
       		end if;
		end if;
    end process;   
  
end Behavioral;
