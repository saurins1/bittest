----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 05/17/2018 10:07:23 AM
-- Design Name: 
-- Module Name: motor interface - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity motor_interface is
    Port ( 
        -- Sync
        aclk    : in STD_LOGIC;
        aresetn : in STD_LOGIC;
        
        -- General
		PMOD_5HB_EN : in STD_LOGIC;
        
        -- PWM inputs      
        PWM_CARRIER_PERIOD  : in STD_LOGIC_VECTOR (31 downto 0);
        PWM_MOD_VALUE       : in STD_LOGIC_VECTOR (31 downto 0);
        PWM_MOD_DIR         : in STD_LOGIC;
        PWM_DEAD_TIME       : in STD_LOGIC_VECTOR (31 downto 0);
        
        -- PWM outputs
        PWM_EN  : out STD_LOGIC;
        PWM_DIR : out STD_LOGIC;
        
        -- ENCODER inputs
		ENC_RESET 			: in STD_LOGIC;
		ENC_POSITION_MAX 	: in STD_LOGIC_VECTOR (31 downto 0);
        ENC_CHA         	: in STD_LOGIC; 
        ENC_CHB         	: in STD_LOGIC; 
        
        -- ENCODER outputs
        ENC_DIR     				: out STD_LOGIC_VECTOR (1 downto 0);	
        ENC_SPEED_1       			: out STD_LOGIC_VECTOR (31 downto 0);
		ENC_SPEED_2       			: out STD_LOGIC_VECTOR (31 downto 0);
        ENC_POSITION_RELATIVE    	: out STD_LOGIC_VECTOR (31 downto 0);
		ENC_POSITION_ABSOLUTE    	: out STD_LOGIC_VECTOR (31 downto 0)
	);       
end motor_interface;
	
architecture Behavioral of motor_interface is

	-- PWM generation
	component motor_pwm is
    Port ( 
		-- Sync
         aclk 		: in STD_LOGIC;
         aresetn 	: in STD_LOGIC;
		
		-- General
        PMOD_5HB_EN : in STD_LOGIC;
		
		-- PWM inputs 
        PWM_CARRIER_PERIOD 	: in STD_LOGIC_VECTOR (31 downto 0);
        PWM_MOD_VALUE 		: in STD_LOGIC_VECTOR (31 downto 0);
        PWM_MOD_DIR 		: in STD_LOGIC;
        PWM_DEAD_TIME 		: in STD_LOGIC_VECTOR (31 downto 0);
           
		-- PWM outputs
        PWM_EN 	: out STD_LOGIC;
        PWM_DIR : out STD_LOGIC
	);
    end component motor_pwm;
    
	-- Encoders reading	
	component motor_enc is
    Port ( 
		-- Sync
		aclk 		: in STD_LOGIC; 
        aresetn     : in STD_LOGIC; 
	
		-- ENCODER inputs
		ENC_RESET 			: in STD_LOGIC; 
		ENC_POSITION_MAX 	: in STD_LOGIC_VECTOR (31 downto 0);
        ENC_CHA 			: in STD_LOGIC; 
        ENC_CHB     		: in STD_LOGIC; 
		
        -- ENCODER outputs
        ENC_DIR     				: out STD_LOGIC_VECTOR (1 downto 0);	
        ENC_SPEED_1       			: out STD_LOGIC_VECTOR (31 downto 0);
		ENC_SPEED_2       			: out STD_LOGIC_VECTOR (31 downto 0);
        ENC_POSITION_RELATIVE    	: out STD_LOGIC_VECTOR (31 downto 0);
		ENC_POSITION_ABSOLUTE    	: out STD_LOGIC_VECTOR (31 downto 0)
	);	
	end component motor_enc;

begin

    motor_pwm_inst: motor_pwm
    Port map( 
		-- Sync
        aclk 		=> aclk,
        aresetn 	=> aresetn,
         
		-- General
		PMOD_5HB_EN => PMOD_5HB_EN,
		
		-- PWM inputs 
        PWM_CARRIER_PERIOD 	=> PWM_CARRIER_PERIOD,
        PWM_MOD_VALUE 		=> PWM_MOD_VALUE,
        PWM_MOD_DIR 		=> PWM_MOD_DIR,
        PWM_DEAD_TIME 		=> PWM_DEAD_TIME,
		
        -- PWM outputs
        PWM_EN 	=> PWM_EN,
        PWM_DIR => PWM_DIR
    );
    
    motor_enc_inst: motor_enc 
    Port map( 
		-- Sync
        aclk  		=> aclk,
        aresetn 	=> aresetn,
				
		-- ENCODER inputs
		ENC_RESET			=> ENC_RESET,
		ENC_POSITION_MAX 	=> ENC_POSITION_MAX,
        ENC_CHA 			=> ENC_CHA,
        ENC_CHB 			=> ENC_CHB,
           
        -- ENCODER outputs
        ENC_DIR 				=> ENC_DIR,
        ENC_SPEED_1 			=> ENC_SPEED_1,
        ENC_SPEED_2 			=> ENC_SPEED_2,
		ENC_POSITION_RELATIVE 	=> ENC_POSITION_RELATIVE,
		ENC_POSITION_ABSOLUTE 	=> ENC_POSITION_ABSOLUTE
    );
	
end Behavioral;
