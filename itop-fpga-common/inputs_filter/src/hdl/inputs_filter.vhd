----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 12/04/2019 10:07:23 AM
-- Design Name: 
-- Module Name: input_filter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity inputs_filter is
	generic (
		REGISTER_W32     : INTEGER := 32;
		N_INPUTS         : INTEGER := 16
	);
	port (
	    -- Sync
	    aclk 	: in STD_LOGIC;
        aresetn	: in STD_LOGIC;
        
		FILTER_ENABLE   : in STD_LOGIC_VECTOR(N_INPUTS-1 downto 0);		
		
		FILTER_PERCENT_WINDOW  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);			
		FILTER_PERCENT_COUNTS  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);			
		PULSE_QUALIFIER        : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);		
		DEBOUNCE        : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);	
		
        INPUTS  : in STD_LOGIC_VECTOR(N_INPUTS-1 downto 0);		
        INPUTS_FILTERED : out STD_LOGIC_VECTOR(N_INPUTS-1 downto 0)    
	);
end inputs_filter;

architecture arch_imp of inputs_filter is
	
    component input_filter is
        generic (
            REGISTER_W32     : INTEGER := 32
        );
        port (
            -- Sync
            aclk 	: in STD_LOGIC;
            aresetn	: in STD_LOGIC;
            
            FILTER_ENABLE   : in STD_LOGIC;
            
            FILTER_PERCENT_WINDOW  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);			
            FILTER_PERCENT_COUNTS  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            FILTER_PERCENT_RESET   : in STD_LOGIC;
                
            PULSE_QUALIFIER        : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            PULSE_QUALIFIER_RESET  : in STD_LOGIC;
            
            DEBOUNCE        : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);	
            DEBOUNCE_RESET  : in STD_LOGIC;
            
            INPUT  : in STD_LOGIC;
            INPUT_FILTERED : out STD_LOGIC       
        );
    end component;
    
    signal filter_percent_window_d1 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);	
    signal filter_percent_counts_d1 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);	
    signal pulse_qualifier_d1 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);	
    signal debounce_d1 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);	
    
    signal filter_percent_reset : STD_LOGIC;
    signal pulse_qualifier_reset : STD_LOGIC;
    signal debounce_reset : STD_LOGIC;
	       
begin

	reset_process: process(aclk)
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                filter_percent_window_d1 <= (others => '0');
                filter_percent_counts_d1 <= (others => '0');
                pulse_qualifier_d1 <= (others => '0');
                debounce_d1 <= (others => '0');
                filter_percent_reset <= '0';
                pulse_qualifier_reset <= '0';
                debounce_reset <= '0';
            else
                filter_percent_window_d1 <= FILTER_PERCENT_WINDOW;
                filter_percent_counts_d1 <= FILTER_PERCENT_COUNTS;
                pulse_qualifier_d1 <= PULSE_QUALIFIER;
                debounce_d1 <= DEBOUNCE;
                if(filter_percent_window_d1 /= FILTER_PERCENT_WINDOW or
                   filter_percent_counts_d1 /= FILTER_PERCENT_COUNTS) then
                    filter_percent_reset <= '1';   
                else
                    filter_percent_reset <= '0';
                end if;
                if(pulse_qualifier_d1 /= PULSE_QUALIFIER) then
                    pulse_qualifier_reset <= '1';   
                else
                    pulse_qualifier_reset <= '0';
                end if;
                if(debounce_d1 /= DEBOUNCE) then
                    debounce_reset <= '1';   
                else
                    debounce_reset <= '0';
                end if;                                              
            end if;
        end if;
    end process;

    input_filter_gen : for i in 0 to (N_INPUTS-1) generate
        input_filter_gen_inst: input_filter
            generic map(
                REGISTER_W32 => REGISTER_W32
            )
            port map(
                aclk => aclk,
                aresetn => aresetn,
                FILTER_ENABLE => FILTER_ENABLE(i),
                FILTER_PERCENT_WINDOW => FILTER_PERCENT_WINDOW,
                FILTER_PERCENT_COUNTS => FILTER_PERCENT_COUNTS,
                FILTER_PERCENT_RESET => filter_percent_reset,
                PULSE_QUALIFIER => PULSE_QUALIFIER,
                PULSE_QUALIFIER_RESET => pulse_qualifier_reset,
                DEBOUNCE => DEBOUNCE,
                DEBOUNCE_RESET => debounce_reset,
                INPUT => INPUTS(i),
                INPUT_FILTERED => INPUTS_FILTERED(i)
            );
    end generate;
                      																				   					     					      									 	   			                 
end arch_imp;
