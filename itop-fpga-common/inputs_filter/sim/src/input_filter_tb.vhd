library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity input_filter_tb is
  	generic (
  		REGISTER_W32     : INTEGER := 32
  	);
end;

architecture bench of input_filter_tb is

  COMPONENT input_filter is
        generic (
            REGISTER_W32     : INTEGER := 8
        );
        port (
            -- Sync
            aclk 	: in STD_LOGIC;
            aresetn	: in STD_LOGIC;
            
            FILTER_ENABLE   : in STD_LOGIC;
            
            FILTER_PERCENT_WINDOW  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);			
            FILTER_PERCENT_COUNTS  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            FILTER_PERCENT_RESET   : in STD_LOGIC;
                
            PULSE_QUALIFIER        : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            PULSE_QUALIFIER_RESET  : in STD_LOGIC;
            
            DEBOUNCE        : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);	
            DEBOUNCE_RESET  : in STD_LOGIC;
            
            INPUT  : in STD_LOGIC;
            INPUT_FILTERED : out STD_LOGIC       
  );
  end COMPONENT;

  signal aclk: std_logic:= '0';
  signal aresetn: std_logic:= '0';
  
  signal FILTER_ENABLE: STD_LOGIC:= '0';

  signal FILTER_PERCENT_WINDOW: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0'); 
  signal FILTER_PERCENT_COUNTS: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0'); 
  signal FILTER_PERCENT_RESET: STD_LOGIC:= '0';

  signal PULSE_QUALIFIER: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0'); 
  signal PULSE_QUALIFIER_RESET: STD_LOGIC:= '0';
  
  signal DEBOUNCE: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0'); 
  signal DEBOUNCE_RESET: STD_LOGIC:= '0';

  signal INPUT: STD_LOGIC:= '0';
  signal INPUT_FILTERED: STD_LOGIC;

  constant aclk_period: time := 10 ns;
  
  type states_signal_sm is (idle, open_file, load_signal, load_signal_wait, close_file); 
  signal state_signal_sm : states_signal_sm; 
  signal load_signal_counter: unsigned(REGISTER_W32-1 downto 0):= (others => '0');
  signal start_sm: STD_LOGIC:= '0';
  signal my_input: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0'); 

begin
	
    update_IO_inst: process(aclk, aresetn)
    begin 
		if (rising_edge(aclk)) then
            if (aresetn = '0') then
   				INPUT <= '0';
            else
				INPUT <= my_input(0);
            end if;
        end if;
    end process;
	
	stimulus: process
	begin
		aresetn <= '0';
		start_sm <= '0';
		wait for aclk_period*10;
		aresetn <= '1';
		start_sm <= '1';
		--FILTER_ENABLE <= '0';
		FILTER_ENABLE <= '1';
		FILTER_PERCENT_WINDOW <= (others => '0');
		--FILTER_PERCENT_WINDOW <= std_logic_vector(to_unsigned(1000, PULSE_QUALIFIER'length));
		FILTER_PERCENT_COUNTS <= (others => '0');
		--FILTER_PERCENT_COUNTS <= std_logic_vector(to_unsigned(600, FILTER_PERCENT_COUNTS'length));
		FILTER_PERCENT_RESET <= '0';
		--PULSE_QUALIFIER <= (others => '0');
		--PULSE_QUALIFIER <= std_logic_vector(to_unsigned(500, PULSE_QUALIFIER'length));	
		PULSE_QUALIFIER <= std_logic_vector(to_unsigned(10000, PULSE_QUALIFIER'length));	
        PULSE_QUALIFIER_RESET <= '0';
		--DEBOUNCE <= (others => '0');
		DEBOUNCE <= std_logic_vector(to_unsigned(500000, DEBOUNCE'length));	
		--DEBOUNCE <= std_logic_vector(to_unsigned(1000000, DEBOUNCE'length));	
        DEBOUNCE_RESET <= '0';
		wait;
	end process;
	
	-- aclk process 
    aclk_process :process
    begin
        aclk <= not aclk;
        wait for aclk_period/2;
        aclk <= not aclk;
        wait for aclk_period/2;
    end process;	
 
    signal_input_inst: process(aclk, aresetn)  
        variable my_counter  : integer range 0 to 1000000000 :=0;   
        
        type signalDataFile is file of integer;
        file signal_data_file: signalDataFile;
        variable signal_data: integer;     
        variable signal_data_file_status: file_open_status;
    begin 
        if (aresetn = '0') then
            my_input <= (others => '0');                                                                                                                                                  
            -- state                                                                              
            state_signal_sm <= idle;  
        elsif (aclk'event and aclk = '1') then
            case state_signal_sm is  
                when idle =>     
                    if(start_sm = '1') then
                        my_counter := 0;
                        load_signal_counter <= (others => '0');    
                        state_signal_sm <= open_file;        
                    end if; 
                when open_file => 
                    file_open(signal_data_file_status, signal_data_file, "signal.dat", read_mode);
                    my_counter := 0; 
                    state_signal_sm <= load_signal;      
                when load_signal =>
                        if not endfile(signal_data_file) then
                            read (signal_data_file, signal_data);
                            my_input <= std_logic_vector(to_unsigned(signal_data, my_input'length));
                        end if;
                        if not endfile(signal_data_file) then
                            read (signal_data_file, signal_data);
                        end if;                                                
                        if(my_counter >= 2499000) then
                            my_counter := 0;                                           
                            state_signal_sm <= load_signal_wait;                                              
                        else
                            -- counter                                          
                            my_counter := my_counter + 1;                                          
                        end if;                                                                         
                when load_signal_wait =>
                    my_input <= (others => '0');                                                      
                    if(my_counter >= 16) then
                        my_counter := 0; 
                        state_signal_sm <= close_file;                                                    
                    else
                        my_counter := my_counter + 1;                                          
                    end if;                                                                                                                                
                when close_file =>
                    if(my_counter >= 100000) then                                               
                        file_close(signal_data_file);
                        state_signal_sm <= idle;                                                   
                    else
                        my_counter := my_counter + 1;
                    end if;     
                when others =>
                    null;                   
            end case;                                                                                            
        end if;
    end process;

  -- Insert values for generic parameters !!
  uut: input_filter 
              generic map ( REGISTER_W32  => REGISTER_W32)
              port map ( aclk         => aclk,
                         aresetn      => aresetn,
                         FILTER_ENABLE  => FILTER_ENABLE,
                         FILTER_PERCENT_WINDOW  => FILTER_PERCENT_WINDOW,
                         FILTER_PERCENT_COUNTS  => FILTER_PERCENT_COUNTS,
                         FILTER_PERCENT_RESET  => FILTER_PERCENT_RESET,
                         PULSE_QUALIFIER  => PULSE_QUALIFIER,
                         PULSE_QUALIFIER_RESET  => PULSE_QUALIFIER_RESET,
                         DEBOUNCE  => DEBOUNCE,
                         DEBOUNCE_RESET  => DEBOUNCE_RESET,
                         INPUT  => INPUT,
                         INPUT_FILTERED  => INPUT_FILTERED
                         );

end;
