----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 05/17/2018 10:07:23 AM
-- Design Name: 
-- Module Name: motor interface - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity motor_sequencer is
	generic (
        REGISTER_W32    : INTEGER := 32;
        REGISTER_W2     : INTEGER := 2;
        WAIT_CYCLES     : INTEGER := 4;
        MAX_SECONDS     : INTEGER := 600
    );
    Port ( 
        -- Sync
        aclk    : in STD_LOGIC;
        aresetn : in STD_LOGIC;
        
        -- General        
		PWM_CONTROL : in STD_LOGIC;
		PWM_STATUS  : out STD_LOGIC;		
		
		PWM_ENCODER_COUNTS : in STD_LOGIC_VECTOR (REGISTER_W32-1 downto 0);
		
		PWM_MOTOR_EN : out STD_LOGIC;
        
        -- PWM inputs      
        PWM_CARRIER_PERIOD  : in STD_LOGIC_VECTOR (REGISTER_W32-1 downto 0);
        PWM_MOD_VALUE       : in STD_LOGIC_VECTOR (REGISTER_W32-1 downto 0);
        PWM_MOD_DIR         : in STD_LOGIC;
        PWM_DEAD_TIME       : in STD_LOGIC_VECTOR (REGISTER_W32-1 downto 0);
        
        -- PWM outputs
        PWM_EN  : out STD_LOGIC;
        PWM_DIR : out STD_LOGIC;
        PWM_END : out STD_LOGIC;
        
        -- ENCODER inputs
        ENCODER_DIR     : in STD_LOGIC_VECTOR (REGISTER_W2-1 downto 0);  
        ENCODER_COUNT   : in STD_LOGIC_VECTOR (REGISTER_W32-1 downto 0); 
        ENCODER_SPEED   : in STD_LOGIC_VECTOR (REGISTER_W32-1 downto 0);
        ENCODER_TRIGGER_COUNTS : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        
        -- ENCODER outputs
        ENCODER_RESET  : out STD_LOGIC
	);       
end motor_sequencer;
	
architecture Behavioral of motor_sequencer is

	-- PWM generation
	component motor_pwm is
    Port ( 
		-- Sync
         aclk 		: in STD_LOGIC;
         aresetn 	: in STD_LOGIC;
		
		-- General
        PWM_MOTOR_EN : in STD_LOGIC;
		
		-- PWM inputs 
        PWM_CARRIER_PERIOD 	: in STD_LOGIC_VECTOR (31 downto 0);
        PWM_MOD_VALUE 		: in STD_LOGIC_VECTOR (31 downto 0);
        PWM_MOD_DIR 		: in STD_LOGIC;
        PWM_DEAD_TIME 		: in STD_LOGIC_VECTOR (31 downto 0);
           
		-- PWM outputs
        PWM_EN 	: out STD_LOGIC;
        PWM_DIR : out STD_LOGIC
	);
    end component motor_pwm;
    
    signal pwm_motor_en_i : std_logic := '0';
    
    -- Motor sequencer state machine
    type states_motor_sequencer_sm is (idle, check_encoder_position, motor_on, motor_off); 
    signal state_motor_sequencer_sm : states_motor_sequencer_sm;
    
    signal encoder_count_abs : std_logic_vector(REGISTER_W32-1 downto 0);
    signal encoder_diff : std_logic_vector(REGISTER_W32-1 downto 0);
    
    signal seconds_counter : std_logic_vector(REGISTER_W32-1 downto 0);
    signal seconds_reset : std_logic := '0';
    
begin

    PWM_MOTOR_EN <= pwm_motor_en_i;

	-- Motor machine 
    motor_sequencer_sm: process(aclk, aresetn) 	
        variable var_counter : integer range 0 to 16383 :=0;																 
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then	
                -- Initialize neccesary signals
                pwm_motor_en_i <= '0'; 	
                PWM_STATUS <= '0';	
                ENCODER_RESET <= '1';	
                PWM_END	<= '0';
                seconds_reset <= '1';						 														 
				-- state
				state_motor_sequencer_sm <= idle;
        	else													 
				case state_motor_sequencer_sm is
					when idle =>
					    PWM_STATUS <= PWM_CONTROL;
					    pwm_motor_en_i <= '0';
					    var_counter := 0;
					    seconds_reset <= '1';
					    PWM_END	<= '0';
					    ENCODER_RESET <= '0';
                        if(PWM_CONTROL = '1') then                         
                            state_motor_sequencer_sm <= check_encoder_position;
                        end if;
					when check_encoder_position =>
					   seconds_reset <= '0';
					   if(var_counter >= WAIT_CYCLES) then
                           if(unsigned(ENCODER_COUNT) = 0) then     
                               pwm_motor_en_i <= '1';
                               ENCODER_RESET <= '0';
                               state_motor_sequencer_sm <= motor_on;
                           end if;		
                        else
                            ENCODER_RESET <= '1'; 
                            var_counter := var_counter + 1;
                        end if;					 											 
					when motor_on =>
					   if(signed(encoder_diff) <= signed(ENCODER_TRIGGER_COUNTS))then
					       PWM_END	<= '1';
					   end if;
					   if((PWM_CONTROL = '0') or
					      (signed(encoder_count_abs) >= signed(PWM_ENCODER_COUNTS)) or
					      (unsigned(seconds_counter) >= MAX_SECONDS)) then
					       pwm_motor_en_i <= '0';
					       PWM_STATUS <= '0';
					       state_motor_sequencer_sm <= motor_off;   
					   end if;
					when motor_off =>
                       if(PWM_CONTROL = '0') then
                           state_motor_sequencer_sm <= idle;
                       end if;																						
					when others =>
						null;
				end case;
			end if;
         end if;
    end process;
    
	-- Motor machine 
    encoder_process: process(aclk, aresetn)
        variable var_counter : integer range 0 to 16383 :=0;																 
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then     
                encoder_count_abs <= (others => '0');
                encoder_diff <= (others => '0');
            else    
                if(signed(ENCODER_COUNT) >= 0) then
                    encoder_diff <= std_logic_vector(signed(PWM_ENCODER_COUNTS) - signed(ENCODER_COUNT));
                    encoder_count_abs <= ENCODER_COUNT;
                else
                    encoder_diff <= std_logic_vector(signed(PWM_ENCODER_COUNTS) + signed(ENCODER_COUNT));
                    encoder_count_abs <= std_logic_vector(-signed(ENCODER_COUNT));
                end if;             
            end if;
        end if;
    end process;
    
    -- Timer process
    timer_process: process(aclk, aresetn)     
        variable cycles_counter : integer range 0 to 999999999 :=0;                                                          
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then    
                cycles_counter:= 0;
                seconds_counter <= (others => '0');
            else
                if(seconds_reset = '1') then
                    cycles_counter:= 0;
                    seconds_counter <= (others => '0');
                else
                    if(cycles_counter >= 100000000) then
                        cycles_counter := 0;
                        seconds_counter <= std_logic_vector(unsigned(seconds_counter)+1);   
                    else
                        cycles_counter := cycles_counter + 1;
                    end if;
                end if;
            end if;
        end if;
    end process;

    motor_pwm_inst: motor_pwm
    Port map( 
		-- Sync
        aclk 		=> aclk,
        aresetn 	=> aresetn,
         
		-- General
		PWM_MOTOR_EN => pwm_motor_en_i,
		
		-- PWM inputs 
        PWM_CARRIER_PERIOD 	=> PWM_CARRIER_PERIOD,
        PWM_MOD_VALUE 		=> PWM_MOD_VALUE,
        PWM_MOD_DIR 		=> PWM_MOD_DIR,
        PWM_DEAD_TIME 		=> PWM_DEAD_TIME,
		
        -- PWM outputs
        PWM_EN 	=> PWM_EN,
        PWM_DIR => PWM_DIR
    );
    	
end Behavioral;
