----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 01/15/2018 15:34:54 PM
-- Design Name: 
-- Module Name: digital_amplifier - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity digital_amplifier is
	generic (
		REGISTER_W48        : INTEGER    := 48;
        REGISTER_W32        : INTEGER    := 32;
        REGISTER_W16        : INTEGER    := 16;
		DIVIDER_FACTOR      : INTEGER    := 20; -- /(1024*1024)
		MULTIPLIER_DELAY    : INTEGER    := 4
    );
    port (
        aclk         : in std_logic;
        aresetn      : in std_logic;
		
		-- Gain
		DIGITAL_AMPLIFIER_GAIN	: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		-- Signal in
		SIGNAL_INPUT_AXIS_TREADY 	: out STD_LOGIC;
		SIGNAL_INPUT_AXIS_TDATA 	: in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		SIGNAL_INPUT_AXIS_TLAST		: in STD_LOGIC;
		SIGNAL_INPUT_AXIS_TVALID  	: in STD_LOGIC;

		-- Signal out
		SIGNAL_OUTPUT_AXIS_TREADY 	: in STD_LOGIC;
		SIGNAL_OUTPUT_AXIS_TDATA	: out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		SIGNAL_OUTPUT_AXIS_TLAST	: out STD_LOGIC;
		SIGNAL_OUTPUT_AXIS_TVALID  : out STD_LOGIC		
    );
end digital_amplifier;

architecture Behavioral of digital_amplifier is

	-- Multiplier
	component digital_amplifier_multiplier_16_32
  	port (
    	CLK : IN STD_LOGIC;
    	A : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    	B : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    	P : OUT STD_LOGIC_VECTOR(47 DOWNTO 0)
  	);
	end component;
	
	signal a : STD_LOGIC_VECTOR(REGISTER_W16-1 DOWNTO 0);
	signal b : STD_LOGIC_VECTOR(REGISTER_W32-1 DOWNTO 0);
	signal p : STD_LOGIC_VECTOR(REGISTER_W48-1 DOWNTO 0);
	
	signal p_i : STD_LOGIC_VECTOR(127 DOWNTO 0);
	
--	constant MSB_zeros  : STD_LOGIC_VECTOR((REGISTER_W48-1) DOWNTO (DIVIDER_FACTOR+REGISTER_W16-1)) := (others => '0');
--	constant Max_positive : STD_LOGIC_VECTOR(REGISTER_W16-1 DOWNTO 0) := ( REGISTER_W16-1 => '0', others => '1' ); 
--	constant Max_negative : STD_LOGIC_VECTOR(REGISTER_W16-1 DOWNTO 0) := ( REGISTER_W16-1 => '1', others => '0' );
    constant Max_positive : STD_LOGIC_VECTOR(REGISTER_W16-1 DOWNTO 0) := "0001111111111111"; 
    constant Max_negative : STD_LOGIC_VECTOR(REGISTER_W16-1 DOWNTO 0) := "1110000000000000"; 

	signal signal_input_axis_tvalid_d : STD_LOGIC_VECTOR(MULTIPLIER_DELAY DOWNTO 0);
	signal signal_input_axis_tlast_d  : STD_LOGIC_VECTOR(MULTIPLIER_DELAY DOWNTO 0);
	       
begin
	
	-- TREADY
	SIGNAL_INPUT_AXIS_TREADY <= SIGNAL_OUTPUT_AXIS_TREADY;

	-- A
	a <= SIGNAL_INPUT_AXIS_TDATA;

	-- B
	b <= DIGITAL_AMPLIFIER_GAIN;
	
	-- Signal control & sync																				   
    adc_config_parameters_inst: process(aclk) 
    begin 
		if (rising_edge(aclk)) then
            if (aresetn = '0') then
   				SIGNAL_OUTPUT_AXIS_TDATA 	<= (others => '0'); 
				SIGNAL_OUTPUT_AXIS_TVALID 	<= '0';	
				SIGNAL_OUTPUT_AXIS_TLAST 	<= '0';
            else    
				if(unsigned(DIGITAL_AMPLIFIER_GAIN) = 0) then
					SIGNAL_OUTPUT_AXIS_TDATA 	<= SIGNAL_INPUT_AXIS_TDATA;
					SIGNAL_OUTPUT_AXIS_TVALID 	<= SIGNAL_INPUT_AXIS_TVALID;	
					SIGNAL_OUTPUT_AXIS_TLAST 	<= SIGNAL_INPUT_AXIS_TLAST;
				else
					-- Divide by 1024 the result (Gain was multiplied originally by 1024 in order to increase accuracy)
--					if ( (p((REGISTER_W48-1) downto (DIVIDER_FACTOR+REGISTER_W16-1))=MSB_zeros) or (p((REGISTER_W48-1) downto (DIVIDER_FACTOR+REGISTER_W16-1))= not MSB_zeros) ) then 
--					   SIGNAL_OUTPUT_AXIS_TDATA 	<= p((DIVIDER_FACTOR+REGISTER_W16-1) downto DIVIDER_FACTOR);
--					elsif ( p(REGISTER_W48-1) = '0' ) then
--					   SIGNAL_OUTPUT_AXIS_TDATA 	<= Max_positive;
--					else
--					   SIGNAL_OUTPUT_AXIS_TDATA 	<= Max_negative;
--					end if;
					if(signed(p_i(REGISTER_W48-1+DIVIDER_FACTOR downto DIVIDER_FACTOR)) > signed(Max_positive)) then
					   SIGNAL_OUTPUT_AXIS_TDATA     <= Max_positive;
					elsif(signed(p_i(REGISTER_W48-1+DIVIDER_FACTOR downto DIVIDER_FACTOR)) < signed(Max_negative)) then
					   SIGNAL_OUTPUT_AXIS_TDATA 	<= Max_negative;
					else
					   SIGNAL_OUTPUT_AXIS_TDATA 	<= p((DIVIDER_FACTOR+REGISTER_W16-1) downto DIVIDER_FACTOR);
					end if;		
					SIGNAL_OUTPUT_AXIS_TVALID 	<= signal_input_axis_tvalid_d(MULTIPLIER_DELAY);
					SIGNAL_OUTPUT_AXIS_TLAST 	<= signal_input_axis_tlast_d(MULTIPLIER_DELAY);
				end if;
            end if;
        end if;
    end process;

    p_i <= std_logic_vector(resize(signed(p), p_i'length));
			
	signal_input_axis_tvalid_d(0) <= SIGNAL_INPUT_AXIS_TVALID;
	signal_input_axis_tlast_d(0) <= SIGNAL_INPUT_AXIS_TLAST;
			
	-- Delay tvalid and tlast for sync multiplier output delay    
    fir_taps_process : for i in 1 to MULTIPLIER_DELAY generate
        process(aclk) 
        begin
           if (rising_edge(aclk)) then
                if (aresetn = '0') then
                    signal_input_axis_tvalid_d(i) 	<= '0';
			   		signal_input_axis_tlast_d(i)  	<= '0';
                else
                    signal_input_axis_tvalid_d(i) 	<= signal_input_axis_tvalid_d(i-1);
					signal_input_axis_tlast_d(i)  	<= signal_input_axis_tlast_d(i-1);
                end if;
           end if;
        end process;
    end generate;
		
	-- Multiplier
	digital_amplifier_multiplier_16_32_inst: digital_amplifier_multiplier_16_32
  	port map (
    	CLK => aclk,
    	A   => a,
    	B   => b,
    	P   => p
  	);

end Behavioral;
