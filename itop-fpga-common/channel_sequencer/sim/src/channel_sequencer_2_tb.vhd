library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

use work.bus_pkg.all;

entity channel_sequencer_2_tb is
	generic (
		SCAN_MODE_NORMAL: INTEGER := 0;
		SCAN_MODE_MRUT	: INTEGER := 10;
		N_CHANNELS		: INTEGER := 2;
		REGISTER_W32    : INTEGER := 32;
		REGISTER_W16    : INTEGER := 16
	);
end;

architecture bench of channel_sequencer_2_tb is

	component channel_sequencer_2 is
		generic (
			SCAN_MODE_NORMAL: INTEGER := 0;
			SCAN_MODE_MRUT	: INTEGER := 10;
			N_CHANNELS		: INTEGER := 2;
			REGISTER_W32    : INTEGER := 32;
			REGISTER_W16    : INTEGER := 16
		);
		port (
			-- Sync
			aclk         : in STD_LOGIC;
			aresetn      : in STD_LOGIC;

			-- Control
			CHANNEL_SEQUENCER_EN : in STD_LOGIC;

			SCAN_MODE     	: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

			COINC_LEVEL 	: in bus_array_2_8;
			COINC_COUNTER   : in bus_array_2_8;

			AVG_LEVEL 		: in bus_array_2_8;
			AVG_COUNTER  	: in bus_array_2_8;

			BURST_LEVEL     : out bus_array_2_8;
			BURST_COUNTER   : out bus_array_2_8;

			-- Tready
			TREADY_IN  : in STD_LOGIC_VECTOR(N_CHANNELS-1 downto 0);
			TREADY_OUT : out STD_LOGIC_VECTOR(N_CHANNELS-1 downto 0)
		);
	end component channel_sequencer_2;

  	signal aclk: STD_LOGIC:= '0';
  	signal aresetn: STD_LOGIC:= '0';

  	signal CHANNEL_SEQUENCER_EN: STD_LOGIC := '0';
  	signal SCAN_MODE: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');
	signal COINC_LEVEL: bus_array_2_8:= (others => (others => '0'));
	signal COINC_COUNTER: bus_array_2_8:= (others => (others => '0'));
	signal AVG_LEVEL: bus_array_2_8:= (others => (others => '0'));
	signal AVG_COUNTER: bus_array_2_8:= (others => (others => '0'));
	signal BURST_LEVEL: bus_array_2_8;
	signal BURST_COUNTER: bus_array_2_8;
  	signal TREADY_IN: STD_LOGIC_VECTOR(N_CHANNELS-1 downto 0):= (others => '0');
  	signal TREADY_OUT: STD_LOGIC_VECTOR(N_CHANNELS-1 downto 0) ;
  
  	signal CHANNEL_SEQUENCER_EN_i: STD_LOGIC := '0';

  	constant aclk_period: time := 10 ns;
  
  	type states_tready_in_sm is (idle, wait_tready_out_on_channel, burst, wait_tready_out_off_channel); 
  	signal state_tready_in_sm_0 : states_tready_in_sm; 
  	signal state_tready_in_sm_1 : states_tready_in_sm; 
  
 	signal channel_tready_on : INTEGER range 0 to 16383;

begin

	CHANNEL_SEQUENCER_EN <= CHANNEL_SEQUENCER_EN_i;


    tready_inst_0: process(aclk, aresetn) 
		variable burst_counter  : integer range 0 to 16383 :=0;
        variable my_counter  : integer range 0 to 16383 :=0;
        variable wait_counter  : integer range 0 to 16383 :=0;
    begin 
        if (aresetn = '0') then
			TREADY_IN(0) <= '0';
			COINC_COUNTER(0) <= (others => '0');
			AVG_COUNTER(0) <= (others => '0');
			my_counter := 0;	
			wait_counter := 0;													  	
            state_tready_in_sm_0 <= idle;  
        elsif (aclk'event and aclk = '1') then
            case state_tready_in_sm_0 is  
                when idle =>
					burst_counter := 0;
					TREADY_IN(0) <= '0';
                    if(wait_counter >= 64) then
                        my_counter := 0; 
                        wait_counter := 0; 
                        state_tready_in_sm_0 <= wait_tready_out_on_channel; 
                    else
                        wait_counter := wait_counter + 1;
                    end if;    
                when wait_tready_out_on_channel => 
					TREADY_IN(0) <= '1';
                    if(TREADY_OUT(0) = '1') then
						if(my_counter = 8) then
							COINC_COUNTER(0) <= std_logic_vector(unsigned(COINC_COUNTER(0))+1);	
							AVG_COUNTER(0) <= std_logic_vector(unsigned(AVG_COUNTER(0))+1);
						end if;
						if(my_counter >= 2500) then
							my_counter := 0; 
							wait_counter := 0;
							burst_counter := burst_counter + 1;
							state_tready_in_sm_0 <= burst;		
						else
							my_counter := my_counter + 1;	
						end if;
					end if;
				when burst => 
					TREADY_IN(0) <= '0';
					if(burst_counter >= unsigned(BURST_LEVEL(0))) then	
						COINC_COUNTER(0) <= (others => '0');
						AVG_COUNTER(0) <= (others => '0');
						if(TREADY_OUT(0) = '0') then
							state_tready_in_sm_0 <= idle; 
							wait_counter := 0;						
						end if;
					else
						if(TREADY_OUT(0) = '0') then
							if(wait_counter >= 8) then
								wait_counter := 0;
								state_tready_in_sm_0 <= wait_tready_out_on_channel; 	
							else
								wait_counter := wait_counter + 1;		
							end if;	
						end if;
					end if;
                when others =>
                    null;                   
            end case;                                                                                            
        end if;
    end process; 

    tready_inst_1: process(aclk, aresetn) 
		variable burst_counter  : integer range 0 to 16383 :=0;
        variable my_counter  : integer range 0 to 16383 :=0;
        variable wait_counter  : integer range 0 to 16383 :=0;
    begin 
        if (aresetn = '0') then
			TREADY_IN(1) <= '0';
			COINC_COUNTER(1) <= (others => '0');
			AVG_COUNTER(1) <= (others => '0');
			my_counter := 0;	
			wait_counter := 0;													  	
            state_tready_in_sm_1 <= idle;  
        elsif (aclk'event and aclk = '1') then
            case state_tready_in_sm_1 is  
                when idle =>
					burst_counter := 0;
					TREADY_IN(1) <= '0';
                    if(wait_counter >= 64) then
                        my_counter := 0; 
                        wait_counter := 0; 
                        state_tready_in_sm_1 <= wait_tready_out_on_channel; 
                    else
                        wait_counter := wait_counter + 1;
                    end if;    
                when wait_tready_out_on_channel => 
					TREADY_IN(1) <= '1';
                    if(TREADY_OUT(1) = '1') then
						if(my_counter = 8) then
							COINC_COUNTER(1) <= std_logic_vector(unsigned(COINC_COUNTER(1))+1);	
							AVG_COUNTER(1) <= std_logic_vector(unsigned(AVG_COUNTER(1))+1);
						end if;
						if(my_counter >= 2500) then
							my_counter := 0; 
							wait_counter := 0;
							burst_counter := burst_counter + 1;
							state_tready_in_sm_1 <= burst;		
						else
							my_counter := my_counter + 1;	
						end if;
					end if;
				when burst => 
					TREADY_IN(1) <= '0';
					if(burst_counter >= unsigned(BURST_LEVEL(1))) then	
						COINC_COUNTER(1) <= (others => '0');
						AVG_COUNTER(1) <= (others => '0');	
						if(TREADY_OUT(1) = '0') then
							state_tready_in_sm_1 <= idle; 
							wait_counter := 0;
						end if;
					else
						if(TREADY_OUT(1) = '0') then
							if(wait_counter >= 8) then
								wait_counter := 0;
								state_tready_in_sm_1 <= wait_tready_out_on_channel; 	
							else
								wait_counter := wait_counter + 1;		
							end if;	
						end if;
					end if;
                when others =>
                    null;                   
            end case;                                                                                            
        end if;
    end process; 
												  
	stimulus: process
	begin
		aresetn <= '0';
		wait for aclk_period*10;
		
		--CHANNEL_SEQUENCER_EN_i <= '0';
		CHANNEL_SEQUENCER_EN_i <= '1';

		SCAN_MODE <= std_logic_vector(to_unsigned(0, SCAN_MODE'length));
		--SCAN_MODE <= std_logic_vector(to_unsigned(10, SCAN_MODE'length));		
												  
		--COINC_LEVEL(0) <= std_logic_vector(to_unsigned(0, COINC_LEVEL(0)'length));
		--COINC_LEVEL(1) <= std_logic_vector(to_unsigned(0, COINC_LEVEL(1)'length));
		
		COINC_LEVEL(0) <= std_logic_vector(to_unsigned(5, COINC_LEVEL(0)'length));
		COINC_LEVEL(1) <= std_logic_vector(to_unsigned(3, COINC_LEVEL(1)'length));
														
		--AVG_LEVEL(0) <= std_logic_vector(to_unsigned(0, AVG_LEVEL(0)'length));
        --AVG_LEVEL(1) <= std_logic_vector(to_unsigned(0, AVG_LEVEL(1)'length));
        
		AVG_LEVEL(0) <= std_logic_vector(to_unsigned(8, AVG_LEVEL(0)'length));
		AVG_LEVEL(1) <= std_logic_vector(to_unsigned(8, AVG_LEVEL(1)'length));
														   
		aresetn <= '1';
		wait;
	end process;
			
	-- aclk process 
    aclk_process :process
    begin
        aclk <= not aclk;
        wait for aclk_period/2;
        aclk <= not aclk;
        wait for aclk_period/2;
    end process;		
												  												 											  
  -- Insert values for generic parameters !!
  uut: channel_sequencer_2 generic map ( 	
	 						SCAN_MODE_NORMAL=> SCAN_MODE_NORMAL,
	  						SCAN_MODE_MRUT 	=> SCAN_MODE_MRUT,
	  						N_CHANNELS 		=> N_CHANNELS,
	  						REGISTER_W32 	=> REGISTER_W32,
                            REGISTER_W16    => REGISTER_W16
  						)
                        port map ( 
							aclk 		=> aclk,
                           	aresetn     => aresetn,
                        	CHANNEL_SEQUENCER_EN 	=> CHANNEL_SEQUENCER_EN,
                     		SCAN_MODE              	=> SCAN_MODE,
							COINC_LEVEL             => COINC_LEVEL,
							COINC_COUNTER           => COINC_COUNTER,
							AVG_LEVEL              	=> AVG_LEVEL,
							AVG_COUNTER             => AVG_COUNTER,
							BURST_LEVEL             => BURST_LEVEL,
							BURST_COUNTER           => BURST_COUNTER,
							TREADY_IN             	=> TREADY_IN,
             				TREADY_OUT             	=> TREADY_OUT );											 
    
end;