library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

use work.bus_pkg.all;

entity channel_sequencer_tb is
	generic (
		SCAN_MODE_NORMAL: INTEGER := 0;
		SCAN_MODE_MRUT	: INTEGER := 10;
		N_CHANNELS		: INTEGER := 2;
        REGISTER_W16    : INTEGER := 16;
		REGISTER_W8     : INTEGER := 8
	);
end;

architecture bench of channel_sequencer_tb is

	component channel_sequencer is
		generic (
			SCAN_MODE_NORMAL: INTEGER := 0;
			SCAN_MODE_MRUT	: INTEGER := 10;
			N_CHANNELS		: INTEGER := 2;
			REGISTER_W16    : INTEGER := 16;
			REGISTER_W8     : INTEGER := 8
		);
		port (
			-- Sync
			aclk         : in STD_LOGIC;
			aresetn      : in STD_LOGIC;

			-- Control
			CHANNEL_SEQUENCER_EN : in STD_LOGIC;

			SCAN_MODE     	: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

			COINC_LEVEL 	: in bus_array(0 to N_CHANNELS-1)(REGISTER_W8-1 downto 0);
			COINC_COUNTER  	: in bus_array(0 to N_CHANNELS-1)(REGISTER_W8-1 downto 0);

			AVG_LEVEL 		: in bus_array(0 to N_CHANNELS-1)(REGISTER_W8-1 downto 0);
			AVG_COUNTER  	: in bus_array(0 to N_CHANNELS-1)(REGISTER_W8-1 downto 0);

			BURST_LEVEL     : out bus_array(0 to N_CHANNELS-1)(REGISTER_W8-1 downto 0);
			BURST_COUNTER   : in bus_array(0 to N_CHANNELS-1)(REGISTER_W8-1 downto 0);

			-- Tready
			TREADY_IN  : in STD_LOGIC_VECTOR(N_CHANNELS-1 downto 0);
			TREADY_OUT : out STD_LOGIC_VECTOR(N_CHANNELS-1 downto 0)
		);
	end component channel_sequencer;

  	signal aclk: STD_LOGIC:= '0';
  	signal aresetn: STD_LOGIC:= '0';

  	signal CHANNEL_SEQUENCER_EN: STD_LOGIC := '0';
  	signal SCAN_MODE: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= (others => '0');
	signal COINC_LEVEL: bus_array(0 to N_CHANNELS-1)(REGISTER_W8-1 downto 0):= (others => (others => '0'));
	signal COINC_COUNTER: bus_array(0 to N_CHANNELS-1)(REGISTER_W8-1 downto 0):= (others => (others => '0'));
	signal AVG_LEVEL: bus_array(0 to N_CHANNELS-1)(REGISTER_W8-1 downto 0):= (others => (others => '0'));
	signal AVG_COUNTER: bus_array(0 to N_CHANNELS-1)(REGISTER_W8-1 downto 0):= (others => (others => '0'));
	signal BURST_LEVEL: bus_array(0 to N_CHANNELS-1)(REGISTER_W8-1 downto 0);
	signal BURST_COUNTER: bus_array(0 to N_CHANNELS-1)(REGISTER_W8-1 downto 0);
  	signal TREADY_IN: STD_LOGIC_VECTOR(N_CHANNELS-1 downto 0):= (others => '0');
  	signal TREADY_OUT: STD_LOGIC_VECTOR(N_CHANNELS-1 downto 0) ;
  
  	signal CHANNEL_SEQUENCER_EN_i: STD_LOGIC := '0';

  	constant aclk_period: time := 10 ns;
  
  	type states_tready_in_sm is (idle, wait_tready_out_on_channel, wait_tready_out_off_channel); 
  	signal state_tready_in_sm : states_tready_in_sm; 
  
 	signal channel_tready_on : INTEGER range 0 to 16383;

begin

	CHANNEL_SEQUENCER_EN <= CHANNEL_SEQUENCER_EN_i;

    tready_inst: process(aclk, aresetn)  
        variable my_counter  : integer range 0 to 16383 :=0;
        variable wait_counter  : integer range 0 to 16383 :=0;
    begin 
        if (aresetn = '0') then
			TREADY_IN <= (others => '0');
			my_counter := 0;	
			wait_counter := 0;													  	
            state_tready_in_sm <= idle;  
        elsif (aclk'event and aclk = '1') then
            case state_tready_in_sm is  
                when idle =>       
                    if(wait_counter >= 32) then
                        my_counter := 0; 
                        wait_counter := 0; 
                        TREADY_IN <= (others => '1');   
                        state_tready_in_sm <= wait_tready_out_on_channel; 
                    else
                        TREADY_IN <= (others => '0');
                        wait_counter := wait_counter + 1;
                    end if;    
                when wait_tready_out_on_channel =>               
                    if(UNSIGNED(TREADY_OUT) > 0) then
                        wait_counter := 0;
                        if(TREADY_OUT(my_counter) = '1') then
                            channel_tready_on <= my_counter; 
                            state_tready_in_sm <= wait_tready_out_off_channel;    
                        else
                            my_counter := my_counter + 1;
                        end if;
                    else
                        if(wait_counter >= 32) then 
                            state_tready_in_sm <= idle; 
                        else
                            wait_counter := wait_counter + 1;
                        end if;       
                    end if;
                when wait_tready_out_off_channel =>
                    if(wait_counter >= 32) then 
                        TREADY_IN(channel_tready_on) <= '0';
                        if(TREADY_OUT(channel_tready_on) = '0') then
                            state_tready_in_sm <= wait_tready_out_on_channel; 
                        end if;
                    else
                        wait_counter := wait_counter + 1;
                    end if;													
                when others =>
                    null;                   
            end case;                                                                                            
        end if;
    end process; 
												  
	stimulus: process
	begin
		aresetn <= '0';
		wait for aclk_period*10;
		
		CHANNEL_SEQUENCER_EN_i <= '0';
		--CHANNEL_SEQUENCER_EN_i <= '1';

		SCAN_MODE <= std_logic_vector(to_unsigned(0, SCAN_MODE'length));
		--SCAN_MODE <= std_logic_vector(to_unsigned(10, SCAN_MODE'length));		
												  
		COINC_LEVEL(0) <= std_logic_vector(to_unsigned(0, COINC_LEVEL(0)'length));
		COINC_LEVEL(1) <= std_logic_vector(to_unsigned(0, COINC_LEVEL(1)'length));
		
--		COINC_LEVEL(0) <= std_logic_vector(to_unsigned(5, COINC_LEVEL(0)'length));
--        COINC_LEVEL(1) <= std_logic_vector(to_unsigned(5, COINC_LEVEL(1)'length));
														
		AVG_LEVEL(0) <= std_logic_vector(to_unsigned(0, AVG_LEVEL(0)'length));
        AVG_LEVEL(1) <= std_logic_vector(to_unsigned(0, AVG_LEVEL(1)'length));
        
--		AVG_LEVEL(0) <= std_logic_vector(to_unsigned(8, AVG_LEVEL(0)'length));
--        AVG_LEVEL(1) <= std_logic_vector(to_unsigned(8, AVG_LEVEL(1)'length));
														   
		aresetn <= '1';
		wait;
	end process;
			
	-- aclk process 
    aclk_process :process
    begin
        aclk <= not aclk;
        wait for aclk_period/2;
        aclk <= not aclk;
        wait for aclk_period/2;
    end process;		
												  												 											  
  -- Insert values for generic parameters !!
  uut: channel_sequencer generic map ( 	
	 						SCAN_MODE_NORMAL=> SCAN_MODE_NORMAL,
	  						SCAN_MODE_MRUT 	=> SCAN_MODE_MRUT,
	  						N_CHANNELS 		=> N_CHANNELS,
	  						REGISTER_W32 	=> REGISTER_W32,
                            REGISTER_W16    => REGISTER_W16,
	  						REGISTER_W8		=> REGISTER_W8
  						)
                        port map ( 
							aclk 		=> aclk,
                           	aresetn     => aresetn,
                        	CHANNEL_SEQUENCER_EN 	=> CHANNEL_SEQUENCER_EN,
                     		SCAN_MODE              	=> SCAN_MODE,
							COINC_LEVEL             => COINC_LEVEL,
							COINC_COUNTER           => COINC_COUNTER,
							AVG_LEVEL              	=> AVG_LEVEL,
							AVG_WINDOW              => AVG_COUNTER,
							BURST_LEVEL             => BURST_LEVEL,
							BURST_COUNTER           => BURST_COUNTER,
							TREADY_IN             	=> TREADY_IN,
             				TREADY_OUT             	=> TREADY_OUT );											 
    
end;