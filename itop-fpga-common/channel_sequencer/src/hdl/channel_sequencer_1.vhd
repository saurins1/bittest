----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 12/12/2017 10:07:23 AM
-- Design Name: 
-- Module Name: channel sequencer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

--library IEEE;
--use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;
--package bus_pkg is
--        type bus_array is array(natural range <>) of std_logic_vector;
--end package;
--use work.bus_pkg.all;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

--use work.bus_pkg.all;

entity channel_sequencer_1 is
	generic (
	    CHANNEL_SEPPARATION	: INTEGER := 8192;
	    HVPS_EN_WAIT	: INTEGER := 200;
		REGISTER_W32    : INTEGER := 32;
        REGISTER_W16    : INTEGER := 16;
		REGISTER_W8     : INTEGER := 8
	);
	port (
	    -- Sync
	    aclk         : in STD_LOGIC;
        aresetn      : in STD_LOGIC;
        
        -- Control
        CHANNEL_SEQUENCER_EN : in STD_LOGIC;
        RECEIVER_DATA_WINDOW : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        HVPS_EN              : out STD_LOGIC;
				     
        -- Tready
        TREADY_IN  : in STD_LOGIC;
        TREADY_OUT : out STD_LOGIC
	);
end channel_sequencer_1;

architecture arch_imp of channel_sequencer_1 is
		 
    -- Channel sequencer state machine
    type states_channel_sequencer_sm is (idle, hvps_enable, check_tready_in, sepparate_channel); 
    signal state_channel_sequencer_sm : states_channel_sequencer_sm;
     
begin
	
	-- State machine for channel multiplexer
    channel_sequencer_sm: process(aclk, aresetn) 
		variable var_sepparate_counter : integer range 0 to 2000000 :=0;																			 
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then	
                -- Initialize neccesary signals 												 														 
				TREADY_OUT <= '0';
				HVPS_EN    <= '0';
				-- state
				state_channel_sequencer_sm <= idle;
        	else														 
				case state_channel_sequencer_sm is
					when idle =>				    
					    if(CHANNEL_SEQUENCER_EN = '1') then
                            TREADY_OUT <= '0';	                         	
                            if(TREADY_IN = '1') then
                                --state_channel_sequencer_sm <= check_tready_in;  
                                var_sepparate_counter := 0;
                                state_channel_sequencer_sm <= hvps_enable;  	
                            end if;	                                
                        else
                            TREADY_OUT <= TREADY_IN;
                        end if;
                    when hvps_enable =>
                        if(var_sepparate_counter >= HVPS_EN_WAIT) then
                            HVPS_EN <= '0';
                            var_sepparate_counter := 0;
                            state_channel_sequencer_sm <= check_tready_in;  
                        else
                            HVPS_EN <= '1';
                            var_sepparate_counter := var_sepparate_counter + 1;	
                        end if;
					when check_tready_in =>
						TREADY_OUT <= TREADY_IN;
					    if(TREADY_IN = '0') then  
							var_sepparate_counter := 0;
							state_channel_sequencer_sm <= sepparate_channel;  
					    end if;				  												 											 
					when sepparate_channel =>
						if(var_sepparate_counter > unsigned(RECEIVER_DATA_WINDOW)) then
						    var_sepparate_counter := 0;
							state_channel_sequencer_sm <= idle;  	
						else
							var_sepparate_counter := var_sepparate_counter + 1;	
						end if;
					when others =>
						null;
				end case;
			end if;
         end if;
    end process;
    					      									 	   			                 
end arch_imp;
