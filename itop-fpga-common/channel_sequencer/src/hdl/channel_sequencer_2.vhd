----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 12/12/2017 10:07:23 AM
-- Design Name: 
-- Module Name: channel sequencer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

--library IEEE;
--use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;
--package bus_pkg is
--        type bus_array is array(natural range <>) of std_logic_vector;
--end package;
--use work.bus_pkg.all;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use work.bus_pkg.all;

entity channel_sequencer_2 is
	generic (
	    SCAN_MODE_NORMAL           : INTEGER := 0;
        SCAN_MODE_MRUT             : INTEGER := 10;
        SCAN_MODE_MRUT_PITCH_CATH  : INTEGER := 11;
        SCAN_MODE_MRUT_DIR         : INTEGER := 12;
	    N_CHANNELS		: INTEGER := 2;
	    CHANNEL_SEPPARATION		: INTEGER := 100;
	    BURST_SEPPARATION		: INTEGER := 256;
		REGISTER_W32    : INTEGER := 32;
        REGISTER_W16    : INTEGER := 16
	);
	port (
	    -- Sync
	    aclk         : in STD_LOGIC;
        aresetn      : in STD_LOGIC;
        
        -- Control
        CHANNEL_SEQUENCER_EN : in STD_LOGIC;
		
		SCAN_MODE     	: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		COINC_LEVEL 	: in bus_array_2_16;
		COINC_COUNTER   : in bus_array_2_16;
		
		AVG_LEVEL 		: in bus_array_2_16;
		AVG_COUNTER  	: in bus_array_2_16;
		
		BURST_LEVEL     : out bus_array_2_16;
        BURST_COUNTER   : out bus_array_2_16;
        
        CHANNEL_EN : in STD_LOGIC_VECTOR(N_CHANNELS-1 downto 0);
        CHANNEL_TRIGGER : in STD_LOGIC_VECTOR(N_CHANNELS-1 downto 0);
        
        -- Tready
        TREADY_IN  : in STD_LOGIC_VECTOR(N_CHANNELS-1 downto 0);
        TREADY_OUT : out STD_LOGIC_VECTOR(N_CHANNELS-1 downto 0)
	);
end channel_sequencer_2;

architecture arch_imp of channel_sequencer_2 is

    -- Aux
    signal tready_in_i  : STD_LOGIC_VECTOR(N_CHANNELS-1 downto 0);
    signal tready_out_i  : STD_LOGIC_VECTOR(N_CHANNELS-1 downto 0);
    
    signal tready_in_burst  : STD_LOGIC_VECTOR(N_CHANNELS-1 downto 0);
    signal tready_in_d1  : STD_LOGIC_VECTOR(N_CHANNELS-1 downto 0);
    signal tready_count  : STD_LOGIC_VECTOR(N_CHANNELS-1 downto 0);

	signal scan_mode_i  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

	signal burst_level_i 	: bus_array_2_16;
	signal burst_counter_i  : bus_array_2_16;
		
    signal channel_selected : INTEGER range 0 to 16383;
    
    constant all_ones  : STD_LOGIC_VECTOR(N_CHANNELS-1 downto 0) := (others => '1');
    constant all_zeros : STD_LOGIC_VECTOR(N_CHANNELS-1 downto 0) := (others => '0');
    constant all_all_zeros : bus_array_2_16 := (others => (others => '0'));
     
    -- Priority
    type priority_array is array ( 0 to N_CHANNELS-1 ) of UNSIGNED(REGISTER_W16-1 downto 0);
    signal channel_priority : priority_array;
    
    signal priority_max : UNSIGNED(REGISTER_W16-1 downto 0);
    
    signal tready_first : STD_LOGIC;
		
    -- Channel sequencer state machine
    type states_channel_sequencer_sm is (idle, at_same_time_trigger, check_tready_in, select_channel, mux_end, sepparate_channel, mrut_sequence); 

    signal state_channel_sequencer_sm : states_channel_sequencer_sm;
    
begin

    -- Mask TREADY_OUT with tready_out_i(temporal)
    TREADY_OUT <= (tready_out_i and TREADY_IN);
    BURST_LEVEL <= burst_level_i;
    BURST_COUNTER <= burst_counter_i;
       
    burst_sel: process(aclk)
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                burst_level_i <= all_all_zeros;
                burst_counter_i <= all_all_zeros;
            else
                if(unsigned(scan_mode_i) = SCAN_MODE_NORMAL) then
                    burst_level_i <= COINC_LEVEL;
                    burst_counter_i <= COINC_COUNTER;
                else
                    --burst_level_i <= AVG_LEVEL;
                    --burst_counter_i <= AVG_COUNTER;
                    burst_level_i <= COINC_LEVEL;
                    burst_counter_i <= COINC_COUNTER;
                end if;
            end if;
        end if;
    end process;
    
    tready_in_bursts: for i in 0 to N_CHANNELS-1 generate
        tready_in_process: process(aclk)
            variable var_counter : integer range 0 to 2000000 :=0;
        begin 
            if (rising_edge(aclk)) then
                if (aresetn = '0') then
                    tready_in_burst(i) <= '0';
                    tready_count (i) <= '0';
                else
                    tready_in_d1(i) <= TREADY_IN(i);
                    if(TREADY_IN(i) = '1' and  tready_in_d1(i) = '0') then
                        tready_in_burst(i) <= '1';  
                        tready_count (i) <= '0';
                    elsif(TREADY_IN(i) = '0' and  tready_in_d1(i) = '1') then
                        var_counter := 0;
                        tready_count (i) <= '1';
                    else
                        if(tready_count (i) = '1') then
                            if(var_counter >= BURST_SEPPARATION) then
                                tready_count (i) <= '0';
                                tready_in_burst(i) <= '0';
                            else
                                var_counter := var_counter + 1;
                            end if;      
                        end if;     
                    end if;
                end if;
            end if;
        end process;
    end generate;
                          
	-- State machine for channel multiplexer
    channel_sequencer_sm: process(aclk) 
		variable var_channel_counter : integer range 0 to 16383 :=0;
		variable var_sepparate_counter : integer range 0 to 2000000 :=0;																			 
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then	
                -- Initialize neccesary signals 												 														 
				tready_out_i <= (others => '0');
				priority_max <= (others => '0');
				channel_priority <= (others => (others => '0'));	
				scan_mode_i <= (others => '0');
				-- state
				state_channel_sequencer_sm <= idle;
        	else														 
				case state_channel_sequencer_sm is
					when idle =>
					    scan_mode_i <= SCAN_MODE;
					    if(CHANNEL_SEQUENCER_EN = '1' and unsigned(SCAN_MODE) = SCAN_MODE_NORMAL) then
                            tready_out_i <= (others => '0');		
                            if(unsigned(TREADY_IN) > 0) then
                                tready_in_i <= TREADY_IN;
                                var_channel_counter := 0;
                                tready_first <= '1';
                                state_channel_sequencer_sm <= check_tready_in;  	
                            else
                                channel_priority <= (others => (others => '0'));	   									 
                            end if;	
                        --elsif(CHANNEL_SEQUENCER_EN = '1' and unsigned(SCAN_MODE) = SCAN_MODE_MRUT) then
                        elsif(unsigned(SCAN_MODE) = SCAN_MODE_MRUT or unsigned(SCAN_MODE) = SCAN_MODE_MRUT_DIR) then 
                            tready_out_i <= (others => '0');  
                            if(TREADY_IN = all_ones) then
                                state_channel_sequencer_sm <= mrut_sequence;
                            end if;                                 
                        else
                            tready_out_i <= (others => '1');  
--                            -- FINAL
--                            tready_out_i <= (others => '0');
--                            tready_in_i <= tready_in_burst or (not CHANNEL_EN);
--                            --tready_in_i <= (TREADY_IN) or (CHANNEL_TRIGGER) or (not CHANNEL_EN);
--                            --tready_in_i <= (CHANNEL_TRIGGER) or (not CHANNEL_EN);  
--                            if(tready_in_i = all_ones) then
--                                state_channel_sequencer_sm <= at_same_time_trigger;
--                            end if;                                                         
                        end if; 
                    when at_same_time_trigger => 
                        tready_in_i <= tready_in_burst and CHANNEL_EN;
                        if(tready_in_i = all_zeros) then   
                            tready_out_i <= (others => '0');
                            state_channel_sequencer_sm <= idle;  
                        else
                            tready_out_i <= (others => '1');
                        end if;                    
					when check_tready_in =>
					    if(var_channel_counter >= N_CHANNELS) then  
					        state_channel_sequencer_sm <= select_channel;
					    else
					        if(tready_in_i(var_channel_counter) = '1') then
					            -- Set as channel selected the first with tready_in activated
					            if(tready_first = '1') then
					                tready_first <= '0';
                                    priority_max <= channel_priority(var_channel_counter);
                                    channel_selected <= var_channel_counter;
                                else
                                    -- Check if channel priority is bigger than the temporal maximum one
                                    if(channel_priority(var_channel_counter) > priority_max) then
                                        priority_max <= channel_priority(var_channel_counter);
                                        channel_selected <= var_channel_counter;     
                                    else
                                        -- Otherwise update current channel priority
                                        channel_priority(var_channel_counter) <= channel_priority(var_channel_counter) + 1;    
                                    end if;
                                end if;
					        end if;
					        var_channel_counter := var_channel_counter + 1;  
					    end if;				  												 											 
					when select_channel =>
					   -- If any channel has been selected
					   if(tready_first = '0') then
					       --tready_out_i(channel_selected) <= '1';
					       channel_priority(channel_selected) <= (others => '0');
					       state_channel_sequencer_sm <= mux_end;
					   else
					       -- Otherwise go to idle to start again
					       state_channel_sequencer_sm <= idle;
					   end if;														 																																											
					when mux_end =>
					   tready_out_i(channel_selected) <= TREADY_IN(channel_selected);
					   -- Wait until tready of channel selected goes to '0'
					   if(TREADY_IN(channel_selected) = '0' and unsigned(burst_counter_i(channel_selected)) = 0) then
					       if(unsigned(TREADY_IN) /= 0) then
					           var_sepparate_counter := 0;
					           state_channel_sequencer_sm <= sepparate_channel;
					       else
					           state_channel_sequencer_sm <= idle;
					       end if;
					   end if;		
					when mrut_sequence =>
					   tready_out_i <= TREADY_IN;
					   if(TREADY_IN = all_zeros and burst_counter_i = all_all_zeros) then
                           var_sepparate_counter := 0;
                           state_channel_sequencer_sm <= sepparate_channel;
					   end if;	
					when sepparate_channel =>
                       if(var_sepparate_counter >= CHANNEL_SEPPARATION) then
                           state_channel_sequencer_sm <= idle;
                       else
                           var_sepparate_counter := var_sepparate_counter + 1;
                       end if;																									
					when others =>
						null;
				end case;
			end if;
         end if;
    end process;
    					      									 	   			                 
end arch_imp;
