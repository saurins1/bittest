----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 05/01/2018 10:07:23 AM
-- Design Name: 
-- Module Name: fft gate - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity fft_gate_coherence is
	generic (
	    REGISTER_W64        : INTEGER := 64;
        REGISTER_W32        : INTEGER := 32;
        REGISTER_W24        : INTEGER := 24;
        REGISTER_W16        : INTEGER := 16;
        REGISTER_W8         : INTEGER := 8
	);
	port (
	    -- Sync
	    aclk         : in STD_LOGIC;
        aresetn      : in STD_LOGIC;
        
        -- Control
        COHERENCE_FFT_WIDTH : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        COHERENCE_NFFT      : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        COHERENCE_FFT_FRAME : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        COHERENCE_CALC      : in STD_LOGIC;
        COHERENCE_END       : out STD_LOGIC;
        
        -- Correlation Bias Calculation
        CORR_SIGNAL_ACC     : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        CORR_SIGNAL_LEN     : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        CORR_SIGNAL_TVALID  : in STD_LOGIC;
        CORR_SIGNAL_TLAST   : in STD_LOGIC;  
        CORR_SIGNAL_BIAS    : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);        
        CORR_SIGNAL_BIAS_TVALID  : out STD_LOGIC;
        CORR_SIGNAL_BIAS_TLAST   : out STD_LOGIC;
      	
		-- Signal input
		FFT_INPUT_AXIS_TREADY  : out STD_LOGIC;
		FFT_INPUT_AXIS_TDATA_X : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		FFT_INPUT_AXIS_TDATA_Y : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		FFT_INPUT_AXIS_TLAST   : in STD_LOGIC;
		FFT_INPUT_AXIS_TVALID  : in STD_LOGIC;
        
        -- Result
        COHERENCE              : out STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
        COHERENCE_TVALID       : out STD_LOGIC
	);
end fft_gate_coherence;

architecture arch_imp of fft_gate_coherence is

    signal fft_input_tvalid : STD_LOGIC_VECTOR(15 DOWNTO 0);
    signal fft_input_tlast  : STD_LOGIC_VECTOR(15 DOWNTO 0);
  
    -- Multiplexer
    COMPONENT fft_MUL_2
      PORT (
        CLK : IN STD_LOGIC;
        A : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        B : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        P : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
      );
    END COMPONENT;

    -- Multiplexer
    COMPONENT fft_MUL_4
      PORT (
        CLK : IN STD_LOGIC;
        A : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        B : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        P : OUT STD_LOGIC_VECTOR(63 DOWNTO 0)
      );
    END COMPONENT;
    
    signal ax : STD_LOGIC_VECTOR(15 DOWNTO 0);
    signal bx : STD_LOGIC_VECTOR(15 DOWNTO 0);
    signal ay : STD_LOGIC_VECTOR(15 DOWNTO 0);
    signal by : STD_LOGIC_VECTOR(15 DOWNTO 0);
    
    signal axax : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal ayay : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal bxbx : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal byby : STD_LOGIC_VECTOR(31 DOWNTO 0);
    
    signal axay : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal axby : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal bxay : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal bxby : STD_LOGIC_VECTOR(31 DOWNTO 0);
    
    signal pxx : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal pyy : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal pxxyy : STD_LOGIC_VECTOR(63 DOWNTO 0);   
    signal pxxyy_d1 : STD_LOGIC_VECTOR(63 DOWNTO 0);  
    signal pxxyy_div : STD_LOGIC_VECTOR(15 DOWNTO 0);  
    signal pxxyy_div_d1 : STD_LOGIC_VECTOR(15 DOWNTO 0); 
      
    signal pxy_re : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal pxy_im : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal apxy : STD_LOGIC_VECTOR(63 DOWNTO 0);
    signal bpxy : STD_LOGIC_VECTOR(63 DOWNTO 0);
    signal pxy : STD_LOGIC_VECTOR(127 DOWNTO 0);
    signal pxy_i : STD_LOGIC_VECTOR(31 DOWNTO 0);

    signal pxxyy_tvalid : STD_LOGIC;
    signal divisor_norm	: INTEGER range 0 to 255 :=0;
    
    COMPONENT fft_divider
      PORT (
        aclk : IN STD_LOGIC;
        aresetn : IN STD_LOGIC;
        s_axis_divisor_tvalid : IN STD_LOGIC;
        s_axis_divisor_tlast : IN STD_LOGIC;
        s_axis_divisor_tdata : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        s_axis_dividend_tvalid : IN STD_LOGIC;
        s_axis_dividend_tlast : IN STD_LOGIC;
        s_axis_dividend_tdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        m_axis_dout_tvalid : OUT STD_LOGIC;
        m_axis_dout_tuser : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        m_axis_dout_tlast : OUT STD_LOGIC;
        m_axis_dout_tdata : OUT STD_LOGIC_VECTOR(47 DOWNTO 0)
      );
    END COMPONENT;
    
    signal fft_divider_divisor : STD_LOGIC_VECTOR(15 DOWNTO 0); 
    signal fft_divider_dividend : STD_LOGIC_VECTOR(31 DOWNTO 0);
    
    signal fft_divider_s_tvalid : STD_LOGIC;
    signal fft_divider_s_tlast : STD_LOGIC;
    
    signal fft_divider_m_tvalid : STD_LOGIC;
    signal fft_divider_m_tlast : STD_LOGIC;
    signal fft_divider_m_tdata : STD_LOGIC_VECTOR(47 DOWNTO 0);
    signal fft_divider_m_tuser : STD_LOGIC_VECTOR(0 DOWNTO 0);
    
    signal fft_divider_m_tvalid_d1 : STD_LOGIC;
    signal fft_divider_m_tlast_d1 : STD_LOGIC;
    signal fft_divider_m_tdata_d1 : STD_LOGIC_VECTOR(31 DOWNTO 0);
    
    COMPONENT fft_coherence_BRAM
      PORT (
        clka : IN STD_LOGIC;
        ena : IN STD_LOGIC;
        wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addra : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        dina : IN STD_LOGIC_VECTOR(71 DOWNTO 0);
        douta : OUT STD_LOGIC_VECTOR(71 DOWNTO 0);
        clkb : IN STD_LOGIC;
        enb : IN STD_LOGIC;
        web : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addrb : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        dinb : IN STD_LOGIC_VECTOR(71 DOWNTO 0);
        doutb : OUT STD_LOGIC_VECTOR(71 DOWNTO 0)
      );
    END COMPONENT;
    
    -- FFT BRAMA port a
    signal brama_coherence_addra   : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal brama_coherence_dina    : STD_LOGIC_VECTOR ( 71 downto 0 );
    signal brama_coherence_douta   : STD_LOGIC_VECTOR ( 71 downto 0 );
    signal brama_coherence_ena     : STD_LOGIC;
    signal brama_coherence_wea     : STD_LOGIC_VECTOR ( 0 to 0 );

    signal brama_coherence_addra_first : STD_LOGIC;
    
    -- FFT BRAMA port b
    signal brama_coherence_addrb   : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal brama_coherence_dinb    : STD_LOGIC_VECTOR ( 71 downto 0 );
    signal brama_coherence_doutb   : STD_LOGIC_VECTOR ( 71 downto 0 );
    signal brama_coherence_enb     : STD_LOGIC;
    signal brama_coherence_web     : STD_LOGIC_VECTOR ( 0 to 0 );
    
    -- FFT BRAMB port a
    signal bramb_coherence_addra   : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal bramb_coherence_dina    : STD_LOGIC_VECTOR ( 71 downto 0 );
    signal bramb_coherence_douta   : STD_LOGIC_VECTOR ( 71 downto 0 );
    signal bramb_coherence_ena     : STD_LOGIC;
    signal bramb_coherence_wea     : STD_LOGIC_VECTOR ( 0 to 0 );

    signal bramb_coherence_addra_first : STD_LOGIC;
    
    -- FFT BRAMB port b
    signal bramb_coherence_addrb   : STD_LOGIC_VECTOR ( 7 downto 0 );
    signal bramb_coherence_dinb    : STD_LOGIC_VECTOR ( 71 downto 0 );
    signal bramb_coherence_doutb   : STD_LOGIC_VECTOR ( 71 downto 0 );
    signal bramb_coherence_enb     : STD_LOGIC;
    signal bramb_coherence_web     : STD_LOGIC_VECTOR ( 0 to 0 );
    
    type states_avg_sm is (idle, avg_acc, avg_div, avg_acc_end, avg_div_end , co_calc, co_calc_acc, co_calc_end); 
    signal state_avg_sm : states_avg_sm;
    
    signal coherence_calc_d : STD_LOGIC_VECTOR(15 DOWNTO 0);
    signal coherence_calc_side : STD_LOGIC;
    
    signal coherence_calc_tvalid : STD_LOGIC;
    signal coherence_calc_tlast : STD_LOGIC;
    signal coherence_calc_tvalid_d : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal coherence_calc_tlast_d : STD_LOGIC_VECTOR ( 15 downto 0 );
    
    signal coherence_acc : STD_LOGIC_VECTOR(127 DOWNTO 0);
    
    signal fft_nfft_int	 : INTEGER range 0 to 255 :=0;
    
    signal fft_frame : UNSIGNED(REGISTER_W16-1 downto 0);
    signal fft_width : UNSIGNED(REGISTER_W16-1 downto 0);
    
    signal test_fft_divider_counter : UNSIGNED(15 DOWNTO 0);
    
    signal brama_coherence_dina_a    : STD_LOGIC_VECTOR ( 35 downto 0 );
    signal brama_coherence_dina_b    : STD_LOGIC_VECTOR ( 35 downto 0 );
    signal bramb_coherence_dina_a    : STD_LOGIC_VECTOR ( 35 downto 0 );
    signal bramb_coherence_dina_b    : STD_LOGIC_VECTOR ( 35 downto 0 );
    
    signal brama_coherence_douta_a   : STD_LOGIC_VECTOR ( 35 downto 0 );
    signal brama_coherence_douta_b   : STD_LOGIC_VECTOR ( 35 downto 0 );
    signal bramb_coherence_douta_a   : STD_LOGIC_VECTOR ( 35 downto 0 );
    signal bramb_coherence_douta_b   : STD_LOGIC_VECTOR ( 35 downto 0 );
    
    signal brama_coherence_doutb_a   : STD_LOGIC_VECTOR ( 35 downto 0 );
    signal brama_coherence_doutb_b   : STD_LOGIC_VECTOR ( 35 downto 0 );
    signal bramb_coherence_doutb_a   : STD_LOGIC_VECTOR ( 35 downto 0 );
    signal bramb_coherence_doutb_b   : STD_LOGIC_VECTOR ( 35 downto 0 );
    
    signal corr_bias_sign : STD_LOGIC;
  
begin

    input_process : process(aclk) 
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                fft_divider_m_tvalid_d1 <= '0';
                fft_divider_m_tlast_d1 <= '0';
                fft_divider_m_tdata_d1 <= (others => '0');        
                fft_input_tvalid(0) <= '0';
                fft_input_tlast(0) <= '0';
                coherence_calc_tvalid_d(0) <= '0';
                coherence_calc_tlast_d(0) <= '0';
                ax  <= (others => '0');
                ay  <= (others => '0');
                bx  <= (others => '0');
                by  <= (others => '0');
                pxx     <= (others => '0');
                pyy     <= (others => '0');
                pxy_re  <= (others => '0');
                pxy_im  <= (others => '0');
                pxy_i   <= (others => '0');
                pxy     <= (others => '0');
                pxxyy_tvalid <= '0';
                pxxyy_d1 <= (others => '0');
                pxxyy_div <= (others => '0');
                pxxyy_div_d1 <= (others => '0');
                fft_divider_divisor <= (others => '0');
                fft_divider_dividend <= (others => '0');
                fft_divider_s_tvalid <= '0';
                fft_divider_s_tlast <= '0';               
                fft_frame <= (others => '0');
                fft_width <= (others => '0');
                fft_nfft_int <= 0;
                corr_bias_sign <= '0';
                CORR_SIGNAL_BIAS <= (others => '0');
                CORR_SIGNAL_BIAS_TVALID <= '0';
                CORR_SIGNAL_BIAS_TLAST <= '0';
            else
                fft_divider_m_tvalid_d1 <= fft_divider_m_tvalid;
                fft_divider_m_tlast_d1 <= fft_divider_m_tlast;
                if(unsigned(fft_divider_m_tuser) > 0) then
                    fft_divider_m_tdata_d1 <= (others => '0'); 
                else
                    fft_divider_m_tdata_d1 <= fft_divider_m_tdata(47 downto 16);
                end if;
                
                fft_input_tvalid(0) <= FFT_INPUT_AXIS_TVALID;
                fft_input_tlast(0)  <= FFT_INPUT_AXIS_TLAST;
                         
                if(FFT_INPUT_AXIS_TVALID = '1') then
                    ax  <= FFT_INPUT_AXIS_TDATA_X(15 downto 0);
                    bx  <= FFT_INPUT_AXIS_TDATA_X(31 downto 16);
                    ay  <= FFT_INPUT_AXIS_TDATA_Y(15 downto 0);
                    by  <= FFT_INPUT_AXIS_TDATA_Y(31 downto 16);
                    if(fft_input_tvalid(0) = '0') then
                        fft_frame <= unsigned(COHERENCE_FFT_FRAME);
                        fft_width <= unsigned(COHERENCE_FFT_WIDTH);
                        fft_nfft_int <= to_integer(unsigned(COHERENCE_NFFT)); 
                    end if;
                else
                    ax  <= (others => '0');
                    ay  <= (others => '0');
                    bx  <= (others => '0');
                    by  <= (others => '0');
                end if; 
                              
                -- axax ayay bxbx axby axay bxby bxay axby fft_input_tvalid(3)
                -- pxx pyy fft_input_tvalid(4)
                -- axax ayay bxbx axby axay bxby bxay axby fft_input_tvalid(3)
                -- pxy_re pxy_im fft_input_tvalid(4)
                if(coherence_calc_side = '0') then
                    pxx     <= std_logic_vector(unsigned(axax) + unsigned(bxbx));
                    pyy     <= std_logic_vector(unsigned(ayay) + unsigned(byby));  
                    pxy_re  <= std_logic_vector(signed(axay) + signed(bxby));   
                    pxy_im  <= std_logic_vector(signed(bxay) - signed(axby));  
                else 
                    -- pxx pyy coherence_calc_tvalid_d(2)
                    pxx     <= std_logic_vector(resize(signed(brama_coherence_douta(35 downto 0)), pxy_re'length));  
                    pyy     <= std_logic_vector(resize(signed(brama_coherence_douta(71 downto 36)), pxy_re'length));   
                    pxy_re  <= std_logic_vector(resize(signed(bramb_coherence_douta(35 downto 0)), pxy_re'length));  
                    pxy_im  <= std_logic_vector(resize(signed(bramb_coherence_douta(71 downto 36)), pxy_im'length)); 
                end if; 
                
                -- apxy bpxy coherence_calc_tvalid_d(8)  
                -- pxy coherence_calc_tvalid_d(9) 
                pxy     <= std_logic_vector(resize(unsigned(apxy), pxy'length)+
                                            resize(unsigned(bpxy), pxy'length));  
                -- pxy coherence_calc_tvalid_d(9)   
                -- pxy_i coherence_calc_tvalid_d(10)                             
                pxy_i   <= pxy(divisor_norm+21 downto divisor_norm) & "0000000000";     
                
                -- pxxyy coherence_calc_tvalid_d(8)     
                pxxyy_tvalid <= coherence_calc_tvalid_d(8); 
                -- pxxyy_d1 coherence_calc_tvalid_d(9)   
                pxxyy_d1 <= pxxyy;
                -- pxxyy_div coherence_calc_tvalid_d(10)   
                pxxyy_div <= pxxyy_d1(divisor_norm+15 downto divisor_norm);  
                        
                if(coherence_calc_side = '1') then
                    CORR_SIGNAL_BIAS_TVALID <= '0';
                    CORR_SIGNAL_BIAS_TLAST <= '0'; 
                    fft_divider_s_tvalid <= coherence_calc_tvalid_d(10);  
                    fft_divider_s_tlast <= coherence_calc_tlast_d(10);
                    fft_divider_divisor <= pxxyy_div;  
                    fft_divider_dividend <= pxy_i;
                    CORR_SIGNAL_BIAS_TVALID <= '0';
                    CORR_SIGNAL_BIAS_TLAST <= '0';
                else
                    if(CORR_SIGNAL_TVALID = '1') then
                        fft_divider_s_tvalid <= CORR_SIGNAL_TVALID;  
                        fft_divider_s_tlast <= CORR_SIGNAL_TLAST; 
                        if(signed(CORR_SIGNAL_ACC) < 0) then
                            fft_divider_dividend <= std_logic_vector(resize(-signed(CORR_SIGNAL_ACC), fft_divider_dividend'length));
                            corr_bias_sign <= '1';
                        else
                            fft_divider_dividend <= std_logic_vector(resize(signed(CORR_SIGNAL_ACC), fft_divider_dividend'length));
                            corr_bias_sign <= '0';
                        end if;
                        fft_divider_divisor <= std_logic_vector(resize(unsigned(CORR_SIGNAL_LEN), fft_divider_divisor'length));
                    else
                        fft_divider_s_tvalid <= '0';  
                        fft_divider_s_tlast <= '0'; 
                    end if;
                    if(fft_divider_m_tvalid = '1') then
                        CORR_SIGNAL_BIAS_TVALID <= fft_divider_m_tvalid;
                        CORR_SIGNAL_BIAS_TLAST <= fft_divider_m_tlast;  
                        if(corr_bias_sign = '0') then
                            CORR_SIGNAL_BIAS <= std_logic_vector(resize(-signed(fft_divider_m_tdata(47 downto 16)), CORR_SIGNAL_BIAS'length));  
                        else
                            CORR_SIGNAL_BIAS <= std_logic_vector(resize(signed(fft_divider_m_tdata(47 downto 16)), CORR_SIGNAL_BIAS'length)); 
                        end if;    
                    else
                        CORR_SIGNAL_BIAS_TVALID <= '0';
                        CORR_SIGNAL_BIAS_TLAST <= '0'; 
                    end if;                   
                end if;             
                coherence_calc_tvalid_d(0) <= coherence_calc_tvalid;
                coherence_calc_tlast_d(0) <= coherence_calc_tlast;
            end if;
        end if;
    end process;
    
    div_norm_loop_process : process(aclk) 
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                divisor_norm <= 0;    
            else
                if(pxxyy_tvalid = '1') then
                    divisor_norm <= 0;  
                    --for I in 8 to 63 loop
                    for I in 16 to 63 loop
                        if(pxxyy(I) = '1') then
                            --div_norm <= I-7;
                            divisor_norm <= I-15;
                        end if;
                    end loop;
                end if;
            end if;
        end if;
    end process;
     
    brama_coherence_dina_a    <= brama_coherence_dina(35 downto 0);
    brama_coherence_dina_b    <= brama_coherence_dina(71 downto 36);
    bramb_coherence_dina_a    <= bramb_coherence_dina(35 downto 0);
    bramb_coherence_dina_b    <= bramb_coherence_dina(71 downto 36);
    
    brama_coherence_douta_a    <= brama_coherence_douta(35 downto 0);
    brama_coherence_douta_b    <= brama_coherence_douta(71 downto 36);
    bramb_coherence_douta_a    <= bramb_coherence_douta(35 downto 0);
    bramb_coherence_douta_b    <= bramb_coherence_douta(71 downto 36);   
    
    brama_coherence_doutb_a    <= brama_coherence_doutb(35 downto 0);
    brama_coherence_doutb_b    <= brama_coherence_doutb(71 downto 36);
    bramb_coherence_doutb_a    <= bramb_coherence_doutb(35 downto 0);
    bramb_coherence_doutb_b    <= bramb_coherence_doutb(71 downto 36);
    
    avg_1_process : process(aclk) 
        variable var_counter  : integer range 0 to 1024 :=0;
        variable fft_counter  : integer range 0 to 1024 :=0;
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                -- bram
                brama_coherence_addra <= (others => '0');
                brama_coherence_addra_first <= '1';
                brama_coherence_dina <= (others => '0');
                brama_coherence_ena <= '1';
                brama_coherence_wea <= (others => '0');    
                bramb_coherence_addra <= (others => '0');
                bramb_coherence_addra_first <= '1';
                bramb_coherence_dina <= (others => '0');
                bramb_coherence_ena <= '1';
                bramb_coherence_wea <= (others => '0');          
                coherence_calc_tvalid <= '0';
                coherence_calc_tlast <= '0';
                COHERENCE <= (others => '0');
                COHERENCE_TVALID <= '0';
                COHERENCE_END <= '0';
                FFT_INPUT_AXIS_TREADY <= '0';
                coherence_calc_d(0) <= '0';
                coherence_calc_side <= '0';
                state_avg_sm <= idle;
            else
                case state_avg_sm is
                    when idle => 
                        COHERENCE_END <= '0';
                        COHERENCE_TVALID <= '0';
                        brama_coherence_addra_first <= '1';
                        var_counter := 0;
                        fft_counter := 0;
                        brama_coherence_wea <= (others => '0');  
                        bramb_coherence_wea <= (others => '0'); 
                        coherence_calc_d(0) <= COHERENCE_CALC; 
                        coherence_calc_side <= '0';
                        if(COHERENCE_CALC = '1') then
                            FFT_INPUT_AXIS_TREADY <= '0';
                            state_avg_sm <= avg_div;
                        else
                            FFT_INPUT_AXIS_TREADY <= '1';
                            if(FFT_INPUT_AXIS_TVALID = '1') then
                                state_avg_sm <= avg_acc;            
                            end if;
                        end if;  
                    when avg_acc => 
                        if(fft_input_tvalid(4) = '1') then
                            brama_coherence_wea <= (others => '1');   
                            bramb_coherence_wea <= (others => '1');  
                            if(brama_coherence_addra_first = '1') then
                                brama_coherence_addra <= (others => '0');
                                bramb_coherence_addra <= (others => '0');
                                brama_coherence_addra_first <= '0';
                            else
                                brama_coherence_addra <= std_logic_vector(unsigned(brama_coherence_addra) +1);
                                bramb_coherence_addra <= std_logic_vector(unsigned(bramb_coherence_addra) +1);
                            end if;  
                            if(fft_frame = 0) then
                                -- pxx pyy
                                brama_coherence_dina(35 downto 0) <=  std_logic_vector(resize(unsigned(pxx), brama_coherence_dina(35 downto 0)'length));
                                brama_coherence_dina(71 downto 36) <=  std_logic_vector(resize(unsigned(pyy), brama_coherence_dina(71 downto 36)'length));
                                -- pxy_im pxy_re
                                bramb_coherence_dina(35 downto 0) <=  std_logic_vector(resize(signed(pxy_re), bramb_coherence_dina(35 downto 0)'length));
                                bramb_coherence_dina(71 downto 36) <=  std_logic_vector(resize(signed(pxy_im), bramb_coherence_dina(71 downto 36)'length));
                            else
                                -- pxx pyy
                                brama_coherence_dina(35 downto 0) <=  std_logic_vector(unsigned(brama_coherence_doutb(35 downto 0))+
                                                                        resize(unsigned(pxx), brama_coherence_dina(35 downto 0)'length));
                                brama_coherence_dina(71 downto 36) <=  std_logic_vector(unsigned(brama_coherence_doutb(71 downto 36))+
                                                                        resize(unsigned(pyy), brama_coherence_dina(71 downto 36)'length));
                                -- pxy_im pxy_re
                                bramb_coherence_dina(35 downto 0) <=  std_logic_vector(signed(bramb_coherence_doutb(35 downto 0))+
                                                                        resize(signed(pxy_re), bramb_coherence_dina(35 downto 0)'length));
                                bramb_coherence_dina(71 downto 36) <=  std_logic_vector(signed(bramb_coherence_doutb(71 downto 36))+
                                                                        resize(signed(pxy_im), bramb_coherence_dina(71 downto 36)'length));
                            end if;
                            if(fft_input_tlast(4) = '1') then
                                var_counter := 0;
                                state_avg_sm <= avg_acc_end;
                            end if;
                        end if;
                    when avg_acc_end =>
                        if(var_counter >= 4) then
                            brama_coherence_wea <= (others => '0'); 
                            bramb_coherence_wea <= (others => '0'); 
                            var_counter := 0;
                            state_avg_sm <= idle;
                        else
                            var_counter := var_counter + 1;
                        end if;
                    when avg_div =>
                        if(coherence_calc_d(3) = '1') then
                            brama_coherence_wea <= (others => '1');   
                            bramb_coherence_wea <= (others => '1');  
                            if(brama_coherence_addra_first = '1') then
                                brama_coherence_addra <= (others => '0');
                                bramb_coherence_addra <= (others => '0');
                                brama_coherence_addra_first <= '0';
                            else
                                brama_coherence_addra <= std_logic_vector(unsigned(brama_coherence_addra) +1);
                                bramb_coherence_addra <= std_logic_vector(unsigned(brama_coherence_addra) +1);
                            end if; 
                            -- pxx pyy div by 8
                            brama_coherence_dina(35 downto 0) <=  brama_coherence_doutb(35) & brama_coherence_doutb(35) & brama_coherence_doutb(35) & 
                                                                  brama_coherence_doutb(35 downto 3);
                            brama_coherence_dina(71 downto 36) <= brama_coherence_doutb(71) & brama_coherence_doutb(71) & brama_coherence_doutb(71) & 
                                                                  brama_coherence_doutb(71 downto 39);
                            -- pxy_im pxy_re div by 8
                            bramb_coherence_dina(35 downto 0) <=  bramb_coherence_doutb(35) & bramb_coherence_doutb(35) & bramb_coherence_doutb(35) & 
                                                                  bramb_coherence_doutb(35 downto 3);
                            bramb_coherence_dina(71 downto 36) <= bramb_coherence_doutb(71) & bramb_coherence_doutb(71) & bramb_coherence_doutb(71) & 
                                                                  bramb_coherence_doutb(71 downto 39);
                            if(fft_counter >= fft_width-1) then
                                fft_counter := 0;
                                var_counter := 0;
                                state_avg_sm <= avg_div_end;
                            else
                                fft_counter := fft_counter + 1;
                            end if;                            
                        end if; 
                    when avg_div_end =>
                        if(var_counter >= 16) then
                            var_counter := 0;
                            state_avg_sm <= co_calc;
                        else
                            if(var_counter >= 4) then
                                brama_coherence_wea <= (others => '0'); 
                                bramb_coherence_wea <= (others => '0'); 
                                brama_coherence_addra_first <= '1';
                                coherence_calc_side <= '1';
                            end if;
                            var_counter := var_counter + 1;
                        end if;
                    when co_calc => 
                        if(brama_coherence_addra_first = '1') then
                            brama_coherence_addra <= (others => '0');
                            bramb_coherence_addra <= (others => '0');
                            brama_coherence_addra_first <= '0';
                        else
                            brama_coherence_addra <= std_logic_vector(unsigned(brama_coherence_addra) +1);
                            bramb_coherence_addra <= std_logic_vector(unsigned(brama_coherence_addra) +1);
                        end if;
                        coherence_calc_tvalid <= '1';
                        if(fft_counter >= fft_width-1) then
                            coherence_calc_tlast <= '1';
                            state_avg_sm <= co_calc_acc;
                        else
                            fft_counter := fft_counter + 1;
                        end if;
                    when co_calc_acc => 
                        coherence_calc_tvalid <= '0';
                        coherence_calc_tlast <= '0';
                        if(fft_divider_m_tlast = '1') then
                            state_avg_sm <= co_calc_end;
                        end if;
                    when co_calc_end => 
                        if(var_counter >= 4) then
                            COHERENCE_END <= '1';
                            COHERENCE_TVALID <= '1';
                            COHERENCE <= coherence_acc(63+fft_nfft_int downto fft_nfft_int);
                            if(COHERENCE_CALC = '0') then                               
                                state_avg_sm <= idle;
                            end if;    
                        else
                            var_counter := var_counter + 1;
                        end if;   
                    when others => 
                        null;
                end case;                 
            end if;
        end if;
    end process;
    
    avg_2_process : process(aclk) 
        variable var_counter  : integer range 0 to 1024 :=0;
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                -- bram
                brama_coherence_addrb <= (others => '0');
                brama_coherence_dinb <= (others => '0');
                brama_coherence_enb <= '1';
                brama_coherence_web <= (others => '0');
                bramb_coherence_addrb <= (others => '0');
                bramb_coherence_dinb <= (others => '0');
                bramb_coherence_enb <= '1';
                bramb_coherence_web <= (others => '0');
                coherence_acc <= (others => '0');
            else
                if(coherence_calc_d(1) = '1') then
                    if(coherence_calc_side = '0') then
                        coherence_acc <= (others => '0');
                        if(coherence_calc_d(0) = '0') then
                            brama_coherence_addrb <= (others => '0');
                            bramb_coherence_addrb <= (others => '0');
                        else
                            brama_coherence_addrb <= std_logic_vector(unsigned(brama_coherence_addrb) +1);
                            bramb_coherence_addrb <= std_logic_vector(unsigned(bramb_coherence_addrb) +1);
                        end if;
                    else
                        if(fft_divider_m_tvalid_d1 = '1') then
                            if(unsigned(fft_divider_m_tdata_d1) > 1024) then
                                coherence_acc <= std_logic_vector(unsigned(coherence_acc)+
                                                --to_unsigned(1024, coherence_acc'length));
                                                to_unsigned(512, coherence_acc'length));
                            else
                                coherence_acc <= std_logic_vector(unsigned(coherence_acc)+
                                                 resize(unsigned(fft_divider_m_tdata_d1), coherence_acc'length));
                            end if;
                        end if;
                    end if;
                else
                    coherence_acc <= (others => '0');
                    if(fft_input_tvalid(2) = '1') then
                        if(fft_input_tvalid(1) = '0') then
                            brama_coherence_addrb <= (others => '0');        
                            bramb_coherence_addrb <= (others => '0');                      
                        else
                            brama_coherence_addrb <= std_logic_vector(unsigned(brama_coherence_addrb) +1);
                            bramb_coherence_addrb <= std_logic_vector(unsigned(bramb_coherence_addrb) +1);
                        end if;
                    end if;   
                end if;  
            end if;
        end if;
    end process;
     
	-- Delay FFT input
    delay_fft_input_process : for i in 1 to 15 generate
        process(aclk) 
        begin
           if (rising_edge(aclk)) then
                if (aresetn = '0') then
                    fft_input_tvalid(i) <= '0';
                    fft_input_tlast(i) <= '0';
                    coherence_calc_d(i) <= '0';
                    coherence_calc_tvalid_d(i) <= '0';
                    coherence_calc_tlast_d(i) <= '0';
                else
                    fft_input_tvalid(i) <= fft_input_tvalid(i-1);
                    fft_input_tlast(i) <= fft_input_tlast(i-1);
                    coherence_calc_d(i) <= coherence_calc_d(i-1);
                    coherence_calc_tvalid_d(i) <= coherence_calc_tvalid_d(i-1);
                    coherence_calc_tlast_d(i) <= coherence_calc_tlast_d(i-1);
                end if;
           end if;
        end process;
    end generate;
      
    axax_inst : fft_MUL_2
      PORT MAP (
        CLK => aclk,
        A => ax,
        B => ax,
        P => axax
      );
          
    bxbx_inst : fft_MUL_2
      PORT MAP (
        CLK => aclk,
        A => bx,
        B => bx,
        P => bxbx
      );
      
    ayay_inst : fft_MUL_2
      PORT MAP (
        CLK => aclk,
        A => ay,
        B => ay,
        P => ayay
      );
      
    byby_inst : fft_MUL_2
      PORT MAP (
        CLK => aclk,
        A => by,
        B => by,
        P => byby
      );
      
    axay_inst : fft_MUL_2
      PORT MAP (
        CLK => aclk,
        A => ax,
        B => ay,
        P => axay
      );  
      
    axby_inst : fft_MUL_2
      PORT MAP (
        CLK => aclk,
        A => ax,
        B => by,
        P => axby
      ); 
      
    bxay_inst : fft_MUL_2
      PORT MAP (
        CLK => aclk,
        A => bx,
        B => ay,
        P => bxay
      ); 
      
    bxby_inst : fft_MUL_2
      PORT MAP (
        CLK => aclk,
        A => bx,
        B => by,
        P => bxby
      );  
      
    apxy_inst : fft_MUL_4
      PORT MAP (
        CLK => aclk,
        A => pxy_re,
        B => pxy_re,
        P => apxy
      );
        
    bpxy_inst : fft_MUL_4
      PORT MAP (
        CLK => aclk,
        A => pxy_im,
        B => pxy_im,
        P => bpxy
      );
      
    pxxyy_inst : fft_MUL_4
      PORT MAP (
        CLK => aclk,
        A => pxx,
        B => pyy,
        P => pxxyy
    );
    
    coherence_div : fft_divider
      PORT MAP (
        aclk => aclk,
        aresetn => aresetn,
        s_axis_divisor_tvalid => fft_divider_s_tvalid,
        s_axis_divisor_tlast => fft_divider_s_tlast,
        s_axis_divisor_tdata => fft_divider_divisor,
        s_axis_dividend_tvalid => fft_divider_s_tvalid,
        s_axis_dividend_tlast => fft_divider_s_tlast,
        s_axis_dividend_tdata => fft_divider_dividend,
        m_axis_dout_tvalid => fft_divider_m_tvalid,
        m_axis_dout_tuser => fft_divider_m_tuser,
        m_axis_dout_tlast => fft_divider_m_tlast,
        m_axis_dout_tdata => fft_divider_m_tdata
      );
      
    brama_coherence_BRAM_inst : fft_coherence_BRAM
        PORT MAP (
          clka => aclk,
          ena => brama_coherence_ena,
          wea => brama_coherence_wea,
          addra => brama_coherence_addra,
          dina => brama_coherence_dina,
          douta => brama_coherence_douta,
          clkb => aclk,
          enb => brama_coherence_enb,
          web => brama_coherence_web,
          addrb => brama_coherence_addrb,
          dinb => brama_coherence_dinb,
          doutb => brama_coherence_doutb
        );
        
    bramb_coherence_BRAM_inst : fft_coherence_BRAM
        PORT MAP (
          clka => aclk,
          ena => bramb_coherence_ena,
          wea => bramb_coherence_wea,
          addra => bramb_coherence_addra,
          dina => bramb_coherence_dina,
          douta => bramb_coherence_douta,
          clkb => aclk,
          enb => bramb_coherence_enb,
          web => bramb_coherence_web,
          addrb => bramb_coherence_addrb,
          dinb => bramb_coherence_dinb,
          doutb => bramb_coherence_doutb
        );
        
    process(aclk) 
        begin
           if (rising_edge(aclk)) then
                if (aresetn = '0') then
                    test_fft_divider_counter <= (others => '0');
                else
                    if(fft_divider_s_tvalid = '1') then
                        test_fft_divider_counter <= test_fft_divider_counter + 1;
                    else
                        if(coherence_calc_tvalid = '1') then
                            test_fft_divider_counter <= (others => '0');
                        end if;
                    end if;
                end if;
           end if;
    end process;
    
end arch_imp;
