----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 05/01/2018 10:07:23 AM
-- Design Name: 
-- Module Name: fft gate - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity fft_gate is
	generic (
	    REGISTER_W64        : INTEGER := 64;
	    REGISTER_W48        : INTEGER := 48;
        REGISTER_W32        : INTEGER := 32;
        REGISTER_W24        : INTEGER := 24;
        REGISTER_W16        : INTEGER := 16;
        REGISTER_W8         : INTEGER := 8;
        FFT_MAX             : INTEGER := 4192;
        BRAM_ADDR_WIDTH     : INTEGER := 12;
        BRAM_DATA_WIDTH     : INTEGER := 32
	);
	port (
	    -- Sync
	    aclk         : in STD_LOGIC;
        aresetn      : in STD_LOGIC;
	
	    -- Control
	    FFT_TEST     : in STD_LOGIC;
	    FFT_EN       : in STD_LOGIC;  
	    CC_EN        : in STD_LOGIC; 
	    
	    FFT_WIDTH	 : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	    CO_FFT_SEGMENT : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	    CO_FFT_NFFT : in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
	    
	    RECEIVER_SAMPLING_FREQUENCY  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);

		FFT_MAX_VALUE  : out STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
		FFT_MAX_SAMPLE : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);		
		CC_MAX_VALUE  : out STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
        CC_MAX_SAMPLE : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        FFT_GATE_END  : out STD_LOGIC;  
        FFT_GATE_END_ACK  : in STD_LOGIC;  
        FFT_COHERENCE   : out STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
        	
		-- Signal input
		FFT_INPUT_AXIS_TREADY  : out STD_LOGIC;
		FFT_INPUT_AXIS_TDATA   : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		FFT_INPUT_AXIS_TLAST   : in STD_LOGIC;
		FFT_INPUT_AXIS_TVALID  : in STD_LOGIC;
		
		CC_CONFIG_ON_ACK : out STD_LOGIC;
		CC_CONFIG_ON     : in STD_LOGIC;	    
        CC_CONFIG_ADDR   : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        CC_CONFIG_DATA   : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        
        FFT_STATUS_AXIS_TDATA  : out STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
        FFT_STATUS_AXIS_TUSER  : out STD_LOGIC_VECTOR(REGISTER_W24-1 downto 0);
        FFT_STATUS_AXIS_TLAST  : out STD_LOGIC;
        FFT_STATUS_AXIS_TVALID : out STD_LOGIC
	);
end fft_gate;

architecture arch_imp of fft_gate is

	signal fft_input_axis_tlast_d   : STD_LOGIC_VECTOR(3 downto 0);
	signal fft_input_axis_valid_d   : STD_LOGIC_VECTOR(3 downto 0);
  	type fft_input_axis_tdata_array is array ( 0 to 3 ) of std_logic_vector(REGISTER_W16-1 downto 0);
    signal fft_input_axis_tdata_d : fft_input_axis_tdata_array;

    -- FFT
    COMPONENT fft_gate_FFT
      PORT (
        aclk : IN STD_LOGIC;
        aresetn : IN STD_LOGIC;
        s_axis_config_tdata : IN STD_LOGIC_VECTOR(39 DOWNTO 0);
        s_axis_config_tvalid : IN STD_LOGIC;
        s_axis_config_tready : OUT STD_LOGIC;
        s_axis_data_tdata : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        s_axis_data_tvalid : IN STD_LOGIC;
        s_axis_data_tready : OUT STD_LOGIC;
        s_axis_data_tlast : IN STD_LOGIC;
        m_axis_data_tdata : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
        m_axis_data_tuser : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
        m_axis_data_tvalid : OUT STD_LOGIC;
        m_axis_data_tready : IN STD_LOGIC;
        m_axis_data_tlast : OUT STD_LOGIC;
        m_axis_status_tdata : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        m_axis_status_tvalid : OUT STD_LOGIC;
        m_axis_status_tready : IN STD_LOGIC;
        event_frame_started : OUT STD_LOGIC;
        event_tlast_unexpected : OUT STD_LOGIC;
        event_tlast_missing : OUT STD_LOGIC;
        event_fft_overflow : OUT STD_LOGIC;
        event_status_channel_halt : OUT STD_LOGIC;
        event_data_in_channel_halt : OUT STD_LOGIC;
        event_data_out_channel_halt : OUT STD_LOGIC
      );
    END COMPONENT;
    
    signal s_axis_config_tdata : STD_LOGIC_VECTOR(39 DOWNTO 0);
    signal s_axis_config_tvalid : STD_LOGIC;
    signal s_axis_config_tready : STD_LOGIC;
    
    signal s_axis_data_tdata : STD_LOGIC_VECTOR(63 DOWNTO 0);
    signal s_axis_data_tdata_re : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal s_axis_data_tdata_im : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal s_axis_data_tvalid : STD_LOGIC;
    signal s_axis_data_tready : STD_LOGIC;
    signal s_axis_data_tlast : STD_LOGIC;
    signal s_axis_data_tvalid_d : STD_LOGIC_VECTOR(2 DOWNTO 0);
    signal s_axis_data_tlast_d  : STD_LOGIC_VECTOR(2 DOWNTO 0);
    
    signal m_axis_data_tdata : STD_LOGIC_VECTOR(63 DOWNTO 0);     
    signal m_axis_data_tuser : STD_LOGIC_VECTOR(23 DOWNTO 0);  
    type m_axis_data_tuser_array is array ( 0 to 3 ) of std_logic_vector(REGISTER_W24-1 downto 0);
    signal m_axis_data_tuser_d : m_axis_data_tuser_array;  
    signal m_axis_data_tvalid : STD_LOGIC;
    signal m_axis_data_tvalid_d : STD_LOGIC_VECTOR(3 DOWNTO 0);
    signal m_axis_data_tready : STD_LOGIC;
    signal m_axis_data_tlast : STD_LOGIC;
    signal m_axis_data_tlast_d : STD_LOGIC_VECTOR(2 DOWNTO 0);
    
    signal m_axis_status_tdata : STD_LOGIC_VECTOR(7 DOWNTO 0);
    signal m_axis_status_tvalid : STD_LOGIC;
    signal m_axis_status_tready : STD_LOGIC;
    
    signal event_frame_started : STD_LOGIC;
    signal event_tlast_unexpected : STD_LOGIC;
    signal event_tlast_missing : STD_LOGIC;
    signal event_fft_overflow : STD_LOGIC;
    signal event_status_channel_halt : STD_LOGIC;
    signal event_data_in_channel_halt : STD_LOGIC;
    signal event_data_out_channel_halt : STD_LOGIC;

    -- BRAM
    COMPONENT fft_gate_BRAM 
      PORT (
        clka : IN STD_LOGIC;
        ena : IN STD_LOGIC;
        wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addra : IN STD_LOGIC_VECTOR(BRAM_ADDR_WIDTH-1 DOWNTO 0);
        dina : IN STD_LOGIC_VECTOR(BRAM_DATA_WIDTH-1 DOWNTO 0);
        douta : OUT STD_LOGIC_VECTOR(BRAM_DATA_WIDTH-1 DOWNTO 0);
        clkb : IN STD_LOGIC;
        enb : IN STD_LOGIC;
        web : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addrb : IN STD_LOGIC_VECTOR(BRAM_ADDR_WIDTH-1 DOWNTO 0);
        dinb : IN STD_LOGIC_VECTOR(BRAM_DATA_WIDTH-1 DOWNTO 0);
        doutb : OUT STD_LOGIC_VECTOR(BRAM_DATA_WIDTH-1 DOWNTO 0)
      );
    END COMPONENT;
    
    -- FFT BRAM port a
    type addr_array is array ( 0 to 3 ) of std_logic_vector(BRAM_ADDR_WIDTH-1 downto 0);
    signal fft_bram_addra : addr_array;
    signal fft_bram_addra_stored   : UNSIGNED ( BRAM_ADDR_WIDTH-1 downto 0 );
    signal fft_bram_dina    : STD_LOGIC_VECTOR ( BRAM_DATA_WIDTH-1 downto 0 );
    signal fft_bram_douta   : STD_LOGIC_VECTOR ( BRAM_DATA_WIDTH-1 downto 0 );
    signal fft_bram_ena     : STD_LOGIC;
    signal fft_bram_wea     : STD_LOGIC_VECTOR ( 0 to 0 );

	signal fft_bram_addra_first : STD_LOGIC;
    
    -- FFT BRAM port b
    signal fft_bram_addrb   : STD_LOGIC_VECTOR ( BRAM_ADDR_WIDTH-1 downto 0 );
    signal fft_bram_dinb    : STD_LOGIC_VECTOR ( BRAM_DATA_WIDTH-1 downto 0 );
    signal fft_bram_doutb   : STD_LOGIC_VECTOR ( BRAM_DATA_WIDTH-1 downto 0 );
    signal fft_bram_enb     : STD_LOGIC;
    signal fft_bram_web     : STD_LOGIC_VECTOR ( 0 to 0 );

	signal fft_bram_addrb_first : STD_LOGIC;
	
	constant gate_bram_addra_last  : STD_LOGIC_VECTOR(BRAM_ADDR_WIDTH-2 downto 0) := (others => '1');
	
    -- FFT BRAM port a
    --signal cc_bram_addra   : STD_LOGIC_VECTOR ( BRAM_ADDR_WIDTH-1 downto 0 );
    signal cc_bram_addra   : addr_array;
    signal cc_bram_dina    : STD_LOGIC_VECTOR ( BRAM_DATA_WIDTH-1 downto 0 );
    signal cc_bram_douta   : STD_LOGIC_VECTOR ( BRAM_DATA_WIDTH-1 downto 0 );
    signal cc_bram_ena     : STD_LOGIC;
    signal cc_bram_wea     : STD_LOGIC_VECTOR ( 0 to 0 );
    
    constant cc_bram_addra_last  : STD_LOGIC_VECTOR(BRAM_ADDR_WIDTH-1 downto 0) := (others => '1');
   
    -- FFT BRAM port b
    signal cc_bram_addrb   : STD_LOGIC_VECTOR ( BRAM_ADDR_WIDTH-1 downto 0 );
    signal cc_bram_dinb    : STD_LOGIC_VECTOR ( BRAM_DATA_WIDTH-1 downto 0 );
    signal cc_bram_doutb   : STD_LOGIC_VECTOR ( BRAM_DATA_WIDTH-1 downto 0 );
    signal cc_bram_enb     : STD_LOGIC;
    signal cc_bram_web     : STD_LOGIC_VECTOR ( 0 to 0 );

    signal cc_bram_addrb_first : STD_LOGIC;
    
    COMPONENT fft_ref_BRAM
      PORT (
        clka : IN STD_LOGIC;
        ena : IN STD_LOGIC;
        wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addra : IN STD_LOGIC_VECTOR(10 DOWNTO 0);
        dina : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        douta : OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
      );
    END COMPONENT;
    
    signal ref_bram_addra   : STD_LOGIC_VECTOR ( BRAM_ADDR_WIDTH-1 downto 0 );
    signal ref_bram_addra_stored   : UNSIGNED ( BRAM_ADDR_WIDTH-1 downto 0 );
    signal ref_bram_dina    : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal ref_bram_douta   : STD_LOGIC_VECTOR ( 15 downto 0 );
    signal ref_bram_ena     : STD_LOGIC;
    signal ref_bram_wea     : STD_LOGIC_VECTOR ( 0 to 0 );
    
    signal padding_cnt   : UNSIGNED ( BRAM_ADDR_WIDTH-1 downto 0 );
    
    signal ref_bram_addra_first : STD_LOGIC;
    
    constant ref_bram_addra_last  : STD_LOGIC_VECTOR(10 downto 0) := (others => '1');
    
    signal fft_segment : UNSIGNED(REGISTER_W16-1 downto 0);
    signal fft_segment_aux : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal fft_segment_i : UNSIGNED(REGISTER_W16-1 downto 0);
    signal fft_segment_half : UNSIGNED(BRAM_ADDR_WIDTH-1 downto 0);
    
    -- DSP
    signal fft_dsp_tvalid_d : STD_LOGIC_VECTOR(2 DOWNTO 0);
    
    -- Multiplexer
    COMPONENT fft_MUL
      PORT (
        CLK : IN STD_LOGIC;
        A : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        B : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        P : OUT STD_LOGIC_VECTOR(63 DOWNTO 0)
      );
    END COMPONENT;
    
    signal fft_mul_a1 : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal fft_mul_b1 : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal fft_mul_p1 : STD_LOGIC_VECTOR(63 DOWNTO 0);
    
    signal fft_mul_a2 : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal fft_mul_b2 : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal fft_mul_p2 : STD_LOGIC_VECTOR(63 DOWNTO 0);
    
    signal fft_mul_sum : UNSIGNED(63 DOWNTO 0);

    -- Complex multiplier
    COMPONENT fft_complex_MUL
      PORT (
        aclk : IN STD_LOGIC;
        s_axis_a_tvalid : IN STD_LOGIC;
        s_axis_a_tlast : IN STD_LOGIC;
        s_axis_a_tdata : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        s_axis_b_tvalid : IN STD_LOGIC;
        s_axis_b_tlast : IN STD_LOGIC;
        s_axis_b_tdata : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        m_axis_dout_tvalid : OUT STD_LOGIC;
        m_axis_dout_tlast : OUT STD_LOGIC;
        m_axis_dout_tdata : OUT STD_LOGIC_VECTOR(127 DOWNTO 0)
      );
    END COMPONENT;

    signal cm_ifft_s_axis_a_tvalid : STD_LOGIC;
    signal cm_ifft_s_axis_a_tlast : STD_LOGIC;
    signal cm_ifft_s_axis_a_tdata : STD_LOGIC_VECTOR(63 DOWNTO 0);
    signal cm_ifft_s_axis_b_tvalid : STD_LOGIC;
    signal cm_ifft_s_axis_b_tlast : STD_LOGIC;
    signal cm_ifft_s_axis_b_tdata : STD_LOGIC_VECTOR(63 DOWNTO 0);
    signal cm_ifft_m_axis_dout_tvalid : STD_LOGIC;
    signal cm_ifft_m_axis_dout_tlast : STD_LOGIC;
    signal cm_ifft_m_axis_dout_tdata : STD_LOGIC_VECTOR(127 DOWNTO 0);
   
    -- FIFO
    COMPONENT fft_gate_FIFO
      PORT (
        clk : IN STD_LOGIC;
        rst : IN STD_LOGIC;
        din : IN STD_LOGIC_VECTOR(79 DOWNTO 0);
        wr_en : IN STD_LOGIC;
        rd_en : IN STD_LOGIC;
        dout : OUT STD_LOGIC_VECTOR(79 DOWNTO 0);
        full : OUT STD_LOGIC;
        empty : OUT STD_LOGIC;
        data_count : OUT STD_LOGIC_VECTOR(8 DOWNTO 0)
      );
    END COMPONENT;
    
    signal fifo_rst : STD_LOGIC;
    
    signal fifo_wr_en : STD_LOGIC;
    signal fifo_din : STD_LOGIC_VECTOR(79 DOWNTO 0);
    
    signal fifo_dout_valid : STD_LOGIC;
    signal fifo_dout_valid_d1 : STD_LOGIC;
    signal fifo_dout : STD_LOGIC_VECTOR(79 DOWNTO 0);
    signal fifo_dout_index : STD_LOGIC_VECTOR(15 DOWNTO 0);
    signal fifo_rd_en : STD_LOGIC;
    signal fifo_rd_en_d1 : STD_LOGIC;
    
    signal fifo_full : STD_LOGIC;
    signal fifo_empty : STD_LOGIC;
    signal fifo_data_count : STD_LOGIC_VECTOR(8 DOWNTO 0);
    signal fifo_data_count_d1 : STD_LOGIC_VECTOR(8 DOWNTO 0);
    
    signal fft_max_mod : UNSIGNED(63 DOWNTO 0);
    signal fft_max_complex : STD_LOGIC_VECTOR(63 DOWNTO 0);
    signal fft_max_index : STD_LOGIC_VECTOR(15 DOWNTO 0);
    
    COMPONENT fft_gate_coherence is
        generic (
            REGISTER_W64        : INTEGER := 64;
            REGISTER_W32        : INTEGER := 32;
            REGISTER_W24        : INTEGER := 24;
            REGISTER_W16        : INTEGER := 16;
            REGISTER_W8         : INTEGER := 8
        );
        port (
            -- Sync
            aclk         : in STD_LOGIC;
            aresetn      : in STD_LOGIC;
            
            -- Control
            COHERENCE_FFT_WIDTH : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
            COHERENCE_NFFT      : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
            COHERENCE_FFT_FRAME : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
            COHERENCE_CALC      : in STD_LOGIC;
            COHERENCE_END       : out STD_LOGIC;
            
            -- Correlation Bias Calculation
            CORR_SIGNAL_ACC     : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            CORR_SIGNAL_LEN     : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
            CORR_SIGNAL_TVALID  : in STD_LOGIC;
            CORR_SIGNAL_TLAST   : in STD_LOGIC;  
            CORR_SIGNAL_BIAS    : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);        
            CORR_SIGNAL_BIAS_TVALID  : out STD_LOGIC;
            CORR_SIGNAL_BIAS_TLAST   : out STD_LOGIC;
              
            -- Signal input
            FFT_INPUT_AXIS_TREADY  : out STD_LOGIC;
            FFT_INPUT_AXIS_TDATA_X : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            FFT_INPUT_AXIS_TDATA_Y : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            FFT_INPUT_AXIS_TLAST   : in STD_LOGIC;
            FFT_INPUT_AXIS_TVALID  : in STD_LOGIC;
            
            -- Result
            COHERENCE              : out STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
            COHERENCE_TVALID       : out STD_LOGIC
        );
    end COMPONENT fft_gate_coherence;
     
    signal coherence_input_tready : STD_LOGIC;
    signal coherence_input_tvalid : STD_LOGIC;
    signal coherence_input_tlast  : STD_LOGIC;
    signal coherence_input_tdata_x : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal coherence_input_tdata_y : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    
	signal coherence_input_tvalid_d : STD_LOGIC_VECTOR(2 DOWNTO 0);
    signal coherence_input_tlast_d : STD_LOGIC_VECTOR(2 DOWNTO 0);
    
    signal coherence_fft_width : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal coherence_fft_width_i : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal coherence_fft_nfft  : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal coherence_fft_frame : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal coherence_calc  : STD_LOGIC;
    signal coherence_end   : STD_LOGIC;
    
    signal coherence : STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
    signal coherence_valid  : STD_LOGIC;
    
    signal corr_signal_acc     : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal corr_signal_len     : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal corr_signal_tvalid  : STD_LOGIC;
    signal corr_signal_tlast   : STD_LOGIC;  
    signal corr_signal_bias    : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);        
    signal corr_signal_bias_tvalid  : STD_LOGIC;
    signal corr_signal_bias_tlast   : STD_LOGIC;
       
    -- FFT-CC sm
    type states_fft_cc_sm is (idle, store_ref_input, start_bias_ref, end_bias_ref, config_ref_fft, perform_ref_fft, wait_ref_fft_end, 
                              store_fft_input, start_bias_fft, end_bias_fft, decide_coherence_cc, config_fft, perform_fft, wait_fft_end,-- fft
                              wait_config_ifft, config_ifft, perform_ifft, wait_ifft_end,               -- ifft
                              config_coherence, start_coherence, perform_coherence_ref, wait_coherence_ref_end, perform_coherence_gate, 
                              wait_coherence_gate_end, wait_for_coherence_check, coherence_check, calculate_coherence, calculate_local_coherence, -- coherence
                              fft_result, fft_cc_end); 
    signal state_fft_cc_sm : states_fft_cc_sm;
    
    signal fft_cc_flag  : STD_LOGIC;
    signal coherence_flag  : STD_LOGIC;
    signal coherence_type  : STD_LOGIC;
    signal store_ref_input_first : STD_LOGIC;
    
    signal ref_input_acc : STD_LOGIC_VECTOR (31 downto 0 );
    signal ref_input_len : STD_LOGIC_VECTOR(15 downto 0);
    signal gate_input_acc : STD_LOGIC_VECTOR (31 downto 0 );
    signal gate_input_len : STD_LOGIC_VECTOR(15 downto 0);
    
    signal ref_bias : STD_LOGIC_VECTOR (15 downto 0 );
    signal gate_bias : STD_LOGIC_VECTOR (15 downto 0 );
    signal xnorm_on  : STD_LOGIC;
    signal xnorm_mul_on  : STD_LOGIC_VECTOR (15 downto 0 );
    signal xnorm : STD_LOGIC_VECTOR (63 downto 0 );
    signal divisor_norm	: INTEGER range 0 to 255 :=0;
    
    signal coherence_read_offset  : STD_LOGIC_VECTOR ( BRAM_ADDR_WIDTH-1 downto 0 );
    
    signal coherence_write_offset  : UNSIGNED ( BRAM_ADDR_WIDTH-1 downto 0 );
    signal coherence_write_offset_x  : UNSIGNED ( BRAM_ADDR_WIDTH-1 downto 0 );
    signal coherence_write_offset_y  : UNSIGNED ( BRAM_ADDR_WIDTH-1 downto 0 );
    
    -- FFT config sm
    type states_fft_config_sm is (idle, capture_params, adjust_params, 
                                  configuration_coherence, 
                                  configuration_start, configuration_end); 
    signal state_fft_config_sm : states_fft_config_sm;
    
    signal configure_fft_module    : STD_LOGIC;
    signal configure_fft_module_end: STD_LOGIC;
    signal fft_width_temp	       : UNSIGNED(REGISTER_W16-1 downto 0);
    signal fft_width_i	           : UNSIGNED(REGISTER_W16-1 downto 0);
    signal fft_width_configured	   : UNSIGNED(REGISTER_W16-1 downto 0);
    signal fft_nfft	               : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0); -- 13 (2^13 = 8192)
    signal fft_nfft_counter	       : UNSIGNED(REGISTER_W8-1 downto 0); -- 13 (2^13 = 8192)
    signal fft_nfft_int	           : INTEGER range 0 to 255 :=0;
    signal fft_fw_inv              : STD_LOGIC_VECTOR(0 downto 0); -- 1 foward 0 inverse
    signal fft_fw_inv_i            : STD_LOGIC_VECTOR(0 downto 0); -- 1 foward 0 inverse
    signal fft_fw_inv_configured   : STD_LOGIC_VECTOR(0 downto 0); -- 1 foward 0 inverse
    signal fft_scale_sch	       : STD_LOGIC_VECTOR(25 downto 0); -- 2 * NFFT
    signal fft_scale_sch_i	       : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); -- 2 * NFFT
    signal fft_gate_width	       : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
      
    type nfft_array is array ( 0 to REGISTER_W16-1 ) of UNSIGNED(16-1 downto 0);
    signal nfft_array_data : nfft_array;
    
    type shift_array is array ( 0 to REGISTER_W16-1 ) of INTEGER range 0 to 255;
    signal shift_ifft_data : shift_array;
    
    signal fft_counter : UNSIGNED(REGISTER_W32-1 downto 0); 
    signal fft_size	   : UNSIGNED(REGISTER_W32-1 downto 0);
    
    signal ifft_result_tdata_i : SIGNED(63 DOWNTO 0);
    signal ifft_result_tdata_i1 : SIGNED(63 DOWNTO 0);
    signal ifft_result_tdata : SIGNED(63 DOWNTO 0);
    signal ifft_result_tvalid : STD_LOGIC;
    signal ifft_result_tlast : STD_LOGIC;
    signal ifft_result_max : SIGNED(63 DOWNTO 0);
    signal ifft_result_max_index : STD_LOGIC_VECTOR(15 DOWNTO 0);
    signal ifft_result_offset_index : UNSIGNED(15 DOWNTO 0);
    
    signal cc_en_i : STD_LOGIC;
    signal fft_en_i : STD_LOGIC;
    
    signal fft_status_axis_tdata_re : STD_LOGIC_VECTOR(63 DOWNTO 0); 
    
    signal test_fft_counter : UNSIGNED(15 DOWNTO 0);
    signal test_fft_result_counter : UNSIGNED(15 DOWNTO 0);
    signal test_fft_coherence_counter : UNSIGNED(15 DOWNTO 0);
    
    signal sampling_freq  : UNSIGNED(REGISTER_W16-1 downto 0);
       
begin

	-- Delay FFT input
    delay_fft_input_process : for i in 1 to 3 generate
        process(aclk) 
        begin
           if (rising_edge(aclk)) then
                if (aresetn = '0') then
                    fft_input_axis_valid_d(i) <= '0';
                    fft_input_axis_tlast_d(i) <= '0';
                    fft_input_axis_tdata_d(i) <= (others => '0');
                else
                    fft_input_axis_valid_d(i) <= fft_input_axis_valid_d(i-1);
                    fft_input_axis_tlast_d(i) <= fft_input_axis_tlast_d(i-1);
                    fft_input_axis_tdata_d(i) <= fft_input_axis_tdata_d(i-1); 
                end if;
           end if;
        end process;
    end generate;
    
    process(aclk) 
    begin
       if (rising_edge(aclk)) then
            if (aresetn = '0') then
                test_fft_counter <= (others => '0');
                test_fft_result_counter <= (others => '0');
                test_fft_coherence_counter <= (others => '0');
            else
                if(s_axis_data_tvalid = '1') then
                    test_fft_counter <= test_fft_counter + 1;
                else
                    if(s_axis_data_tvalid_d(2) = '1') then
                        test_fft_counter <= (others => '0');
                    end if;
                end if;
                if(m_axis_data_tvalid_d(0) = '1') then
                    test_fft_result_counter <= test_fft_result_counter + 1;
                else
                    if(m_axis_data_tvalid = '1') then
                        test_fft_result_counter <= (others => '0');
                    end if;
                end if;           
                if(coherence_input_tvalid = '1') then
                    test_fft_coherence_counter <= test_fft_coherence_counter + 1;
                else
                    if(coherence_input_tvalid_d(1) = '1') then
                        test_fft_coherence_counter <= (others => '0');
                    end if;
                end if;
            end if;
       end if;
    end process;
       
    -- FFT - Cross correlation process
    fft_cc_process: process(aclk) 
        variable var_counter  : integer range 0 to 65536 :=0;
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then	
				var_counter := 0;
				FFT_INPUT_AXIS_TREADY <= '0';
				-- Cross correlation
				CC_CONFIG_ON_ACK	  <= '0';	
				cc_en_i               <= '0';	
				fft_en_i              <= '0';	
				-- CC BRAM
                cc_bram_wea           <= (others => '0');
                cc_bram_addra(0)      <= (others => '0');          
                cc_bram_dina          <= (others => '0');
                cc_bram_ena           <= '1';
				-- FFT BRAM
				fft_bram_wea          <= (others => '0');
                fft_bram_addra        <= (others => (others => '0'));   
                fft_bram_addra_stored <= (others => '0');         
                fft_bram_dina         <= (others => '0');
                fft_bram_ena          <= '0';
                fft_bram_addra_first  <= '1'; 
                -- reference BRAM
				ref_bram_wea           <= (others => '0');
                ref_bram_addra         <= (others => '0');  
                ref_bram_addra_stored  <= (others => '0');             
                ref_bram_dina          <= (others => '0');
                ref_bram_ena           <= '0';
                ref_bram_addra_first   <= '1';
                -- signal input                    
                fft_input_axis_valid_d(0) <= '0';
                fft_input_axis_tlast_d(0) <= '0';
                fft_input_axis_tdata_d(0) <= (others => '0');
                -- FFT configuration
                configure_fft_module  <= '0';
                fft_fw_inv <= (others => '1');
                fft_scale_sch_i <= (others => '0');
                fft_cc_flag <= '0';
                -- FFT input
                s_axis_data_tvalid_d <= (others => '0');
                s_axis_data_tlast_d <= (others => '0');
                m_axis_data_tready <= '0';
                m_axis_status_tready <= '0';
                -- FFT result
                FFT_GATE_END <= '0';
                FFT_MAX_VALUE <= (others => '0');
                FFT_MAX_SAMPLE <= (others => '0');
                CC_MAX_VALUE <= (others => '0');
                CC_MAX_SAMPLE <= (others => '0');
                FFT_COHERENCE <= (others => '0');
                -- DSP
                fft_dsp_tvalid_d <= (others => '0');
                -- Cerror
                coherence_input_tvalid_d <= (others => '0');
                coherence_input_tlast_d <= (others => '0');
                -- FFT features
                fft_counter <= (others => '0');
                fft_size <= (others => '0');
                fft_width_temp <= (others => '0');
                -- Coherence
                coherence_flag <= '0';
                coherence_type <= '0';
                coherence_read_offset <= (others => '0');
                coherence_write_offset <= (others => '0');
                coherence_write_offset_x <= (others => '0');
                coherence_write_offset_y <= (others => '0');
                coherence_fft_width <= (others => '0');
                coherence_fft_width_i <= (others => '0');
                coherence_fft_nfft <= (others => '0');
                coherence_fft_frame <= (others => '0');
                coherence_calc <= '0';
                fft_segment <= (others => '0');
                fft_segment_i <= (others => '0');
                fft_segment_aux <= (others => '0');
                fft_segment_half <= (others => '0');
                fft_gate_width <= (others => '0');
                sampling_freq <= (others => '0');
                corr_signal_acc <= (others => '0');
                corr_signal_len <= (others => '0');
                corr_signal_tvalid <= '0';
                corr_signal_tlast <= '0';
                ref_bias <= (others => '0');
                gate_bias <= (others => '0');
                xnorm_on <= '0';
				state_fft_cc_sm <= idle;
        	else
                fft_input_axis_valid_d(0) <= FFT_INPUT_AXIS_TVALID;
                fft_input_axis_tlast_d(0) <= FFT_INPUT_AXIS_TLAST;
                fft_input_axis_tdata_d(0) <= FFT_INPUT_AXIS_TDATA;
                s_axis_data_tvalid_d(1) <= s_axis_data_tvalid_d(0);
                s_axis_data_tvalid_d(2) <= s_axis_data_tvalid_d(1);
                s_axis_data_tlast_d(1)  <= s_axis_data_tlast_d(0);
                s_axis_data_tlast_d(2)  <= s_axis_data_tlast_d(1);     
                fft_dsp_tvalid_d(1) <= fft_dsp_tvalid_d(0);         
                fft_dsp_tvalid_d(2) <= fft_dsp_tvalid_d(1);
                fft_bram_addra(1) <= fft_bram_addra(0);
                fft_bram_addra(2) <= fft_bram_addra(1);
                cc_bram_addra(1) <= cc_bram_addra(0);
                cc_bram_addra(2) <= cc_bram_addra(1);
                coherence_input_tvalid_d(1) <= coherence_input_tvalid_d(0);
                coherence_input_tvalid_d(2) <= coherence_input_tvalid_d(1);
                coherence_input_tlast_d(1) <= coherence_input_tlast_d(0);
                coherence_input_tlast_d(2) <= coherence_input_tlast_d(1);
                m_axis_status_tready <= '1';
                if(m_axis_data_tvalid = '1') then
                    if(m_axis_data_tlast = '1') then
                        fft_counter <= (others => '0');
                        fft_size <= fft_counter + 1;
                    else
                        fft_counter <= fft_counter + 1; 
                    end if;    
                end if;
				case state_fft_cc_sm is        
					when idle =>
					   CC_CONFIG_ON_ACK <= '0';		
					   var_counter := 0;	
                       cc_bram_wea      <= (others => '0');
                       cc_bram_addra(0) <= (others => '0');          
                       cc_bram_dina     <= (others => '0');
                       cc_bram_ena      <= '1';
				       fft_bram_wea     <= (others => '0');
                       fft_bram_addra(0)<= (others => '0');          
                       fft_bram_dina    <= (others => '0');
                       fft_bram_ena     <= '1';
                       fft_bram_addra_first <= '1';
				       ref_bram_wea     <= (others => '0');
                       ref_bram_addra   <= (others => '0');           
                       ref_bram_dina    <= (others => '0');
                       ref_bram_ena     <= '1';
                       ref_bram_addra_first   <= '1';
                       FFT_GATE_END <= '0';
                       m_axis_data_tready <= '1';    
                       s_axis_data_tvalid_d(0) <= '0';
                       s_axis_data_tlast_d(0)  <= '0'; 
                       coherence_input_tvalid_d(0) <= '0';
                       coherence_input_tlast_d(0)  <= '0'; 
                       if(CC_EN = '1') then
                           if(unsigned(FFT_WIDTH) >= 2048) then
                               fft_width_temp<= to_unsigned(2048, fft_width_temp'length);
                           else
                               if(unsigned(FFT_WIDTH(REGISTER_W16-1 downto 0)) < 16) then
                                    fft_width_temp<= to_unsigned(8, fft_width_temp'length);  
                               elsif(unsigned(FFT_WIDTH(REGISTER_W16-1 downto 0)) >= 16 and 
                                     unsigned(FFT_WIDTH(REGISTER_W16-1 downto 0)) < 32) then
                                    fft_width_temp<= to_unsigned(16, fft_width_temp'length); 
                               elsif(unsigned(FFT_WIDTH(REGISTER_W16-1 downto 0)) >= 32 and 
                                     unsigned(FFT_WIDTH(REGISTER_W16-1 downto 0)) < 64) then
                                    fft_width_temp<= to_unsigned(32, fft_width_temp'length); 
                               elsif(unsigned(FFT_WIDTH(REGISTER_W16-1 downto 0)) >= 64 and 
                                     unsigned(FFT_WIDTH(REGISTER_W16-1 downto 0)) < 128) then
                                    fft_width_temp<= to_unsigned(64, fft_width_temp'length); 
                               elsif(unsigned(FFT_WIDTH(REGISTER_W16-1 downto 0)) >= 128 and 
                                     unsigned(FFT_WIDTH(REGISTER_W16-1 downto 0)) < 256) then
                                    fft_width_temp<= to_unsigned(128, fft_width_temp'length); 
                               elsif(unsigned(FFT_WIDTH(REGISTER_W16-1 downto 0)) >= 256 and 
                                     unsigned(FFT_WIDTH(REGISTER_W16-1 downto 0)) < 512) then
                                    fft_width_temp<= to_unsigned(256, fft_width_temp'length); 
                               elsif(unsigned(FFT_WIDTH(REGISTER_W16-1 downto 0)) >= 512 and 
                                     unsigned(FFT_WIDTH(REGISTER_W16-1 downto 0)) < 1024) then
                                    fft_width_temp<= to_unsigned(512, fft_width_temp'length); 
                               elsif(unsigned(FFT_WIDTH(REGISTER_W16-1 downto 0)) >= 1024 and 
                                     unsigned(FFT_WIDTH(REGISTER_W16-1 downto 0)) < 2048) then
                                    fft_width_temp<= to_unsigned(1024, fft_width_temp'length); 
                               else
                                    fft_width_temp<= to_unsigned(2048, fft_width_temp'length); 
                               end if;
                           end if;
                       else
                           if(unsigned(FFT_WIDTH) >= 4096) then
                               fft_width_temp<= to_unsigned(4096, fft_width_temp'length);
                           else
                               if(unsigned(FFT_WIDTH(REGISTER_W16-1 downto 0)) < 16) then
                                    fft_width_temp<= to_unsigned(8, fft_width_temp'length);  
                               elsif(unsigned(FFT_WIDTH(REGISTER_W16-1 downto 0)) >= 16 and 
                                     unsigned(FFT_WIDTH(REGISTER_W16-1 downto 0)) < 32) then
                                    fft_width_temp<= to_unsigned(16, fft_width_temp'length); 
                               elsif(unsigned(FFT_WIDTH(REGISTER_W16-1 downto 0)) >= 32 and 
                                     unsigned(FFT_WIDTH(REGISTER_W16-1 downto 0)) < 64) then
                                    fft_width_temp<= to_unsigned(32, fft_width_temp'length); 
                               elsif(unsigned(FFT_WIDTH(REGISTER_W16-1 downto 0)) >= 64 and 
                                     unsigned(FFT_WIDTH(REGISTER_W16-1 downto 0)) < 128) then
                                    fft_width_temp<= to_unsigned(64, fft_width_temp'length); 
                               elsif(unsigned(FFT_WIDTH(REGISTER_W16-1 downto 0)) >= 128 and 
                                     unsigned(FFT_WIDTH(REGISTER_W16-1 downto 0)) < 256) then
                                    fft_width_temp<= to_unsigned(128, fft_width_temp'length); 
                               elsif(unsigned(FFT_WIDTH(REGISTER_W16-1 downto 0)) >= 256 and 
                                     unsigned(FFT_WIDTH(REGISTER_W16-1 downto 0)) < 512) then
                                    fft_width_temp<= to_unsigned(256, fft_width_temp'length); 
                               elsif(unsigned(FFT_WIDTH(REGISTER_W16-1 downto 0)) >= 512 and 
                                     unsigned(FFT_WIDTH(REGISTER_W16-1 downto 0)) < 1024) then
                                    fft_width_temp<= to_unsigned(512, fft_width_temp'length); 
                               elsif(unsigned(FFT_WIDTH(REGISTER_W16-1 downto 0)) >= 1024 and 
                                     unsigned(FFT_WIDTH(REGISTER_W16-1 downto 0)) < 2044) then
                                    fft_width_temp<= to_unsigned(1024, fft_width_temp'length); 
                               elsif(unsigned(FFT_WIDTH(REGISTER_W16-1 downto 0)) >= 2048 and 
                                     unsigned(FFT_WIDTH(REGISTER_W16-1 downto 0)) < 4096) then
                                    fft_width_temp<= to_unsigned(2048, fft_width_temp'length); 
                               else
                                    fft_width_temp<= to_unsigned(4096, fft_width_temp'length); 
                               end if;
                           end if;
                       end if;
                       fft_cc_flag <= '1';
                       coherence_flag <= '0';
                       cc_en_i     <= CC_EN;
                       fft_en_i    <= FFT_EN;
                       if(unsigned(CO_FFT_SEGMENT) > 400) then
                          fft_segment_aux <= std_logic_vector(to_unsigned(400, fft_segment_aux'length));
                       else
                          fft_segment_aux <= CO_FFT_SEGMENT; 
                       end if;
--                       fft_segment <= unsigned(CO_FFT_SEGMENT);
--                       fft_segment_half <= "0" & unsigned(CO_FFT_SEGMENT(11 downto 1));
                       fft_segment <= unsigned(fft_segment_aux);
                       fft_segment_half <= "0" & unsigned(fft_segment_aux(11 downto 1));
                       fft_gate_width <= FFT_WIDTH(REGISTER_W16-1 downto 0); 
                       sampling_freq <= unsigned(RECEIVER_SAMPLING_FREQUENCY);
					   if(FFT_INPUT_AXIS_TVALID = '1') then
					       gate_input_acc <= (others => '0');
					       gate_input_len <= (others => '0');
					       state_fft_cc_sm <= store_fft_input;
					   else
					       if(CC_CONFIG_ON = '1') then                              
                               FFT_INPUT_AXIS_TREADY <= '0'; 
                               store_ref_input_first <= '1'; 
                               ref_input_acc <= (others => '0');
                               ref_input_len <= (others => '0');
                               state_fft_cc_sm <= store_ref_input;
                           else   
                               FFT_INPUT_AXIS_TREADY <= FFT_EN or CC_EN; 
                           end if;
					   end if;
					-- Reference signal storage in cc_bram
					when store_ref_input => 
					   if(var_counter >= 1000) then
					       CC_CONFIG_ON_ACK <= CC_CONFIG_ON;
					       if(CC_CONFIG_ON = '1') then
					           ref_bram_wea   <= (others => '1');
					           if(unsigned(CC_CONFIG_ADDR) <= unsigned(ref_bram_addra_last)) then
                                   ref_bram_addra <= std_logic_vector(resize(unsigned(CC_CONFIG_ADDR), ref_bram_addra'length));   
                                   ref_bram_dina  <= std_logic_vector(resize(signed(CC_CONFIG_DATA), ref_bram_dina'length));                                   
                                   if(store_ref_input_first = '1') then
                                       ref_input_acc <= std_logic_vector(resize(signed(CC_CONFIG_DATA), ref_input_acc'length));
                                       store_ref_input_first <= '0';
                                   else
                                       if(unsigned(ref_bram_addra) /= unsigned(CC_CONFIG_ADDR)) then
                                           ref_input_acc <= std_logic_vector(signed(ref_input_acc) + 
                                                                             resize(signed(CC_CONFIG_DATA), ref_input_acc'length));                                       
                                       end if;
                                   end if;
                                   ref_input_len <= std_logic_vector(resize(unsigned(CC_CONFIG_ADDR), ref_input_len'length)+1);
                               end if;
					       else
					           ref_bram_wea   <= (others => '0');
					           ref_bram_addra_stored <= unsigned(ref_bram_addra);		   
                               --state_fft_cc_sm <= config_ref_fft;
                               state_fft_cc_sm <= start_bias_ref;
					       end if;
					   else
					       if(FFT_INPUT_AXIS_TVALID = '1') then				           
					           state_fft_cc_sm <= idle;
					       end if;
					       var_counter := var_counter + 1;
					   end if;
					when start_bias_ref =>
					   corr_signal_tvalid <= '1';
					   corr_signal_tlast <= '1';
					   corr_signal_acc <= ref_input_acc;
					   corr_signal_len <= ref_input_len;
					   state_fft_cc_sm <= end_bias_ref;
					when end_bias_ref =>
					   corr_signal_tvalid <= '0';
                       corr_signal_tlast <= '0';
					   if(corr_signal_bias_tlast = '1') then
					       ref_bias <= corr_signal_bias;
					       state_fft_cc_sm <= config_ref_fft; 
					   end if;
					-- FFT configuration
					when config_ref_fft =>		
					   coherence_flag <= '0';	   
					   fft_fw_inv <= (others => '1');
                       fft_scale_sch_i <= (others => '0');
                       fft_cc_flag <= '0';            
                       if(configure_fft_module_end = '1') then   
                           configure_fft_module  <= '0';                         
                           state_fft_cc_sm <= perform_ref_fft;
                       else
                           configure_fft_module  <= '1';    
                       end if;
                    -- Perform reference signal FFT
					when perform_ref_fft => 
                        if(s_axis_data_tready = '1') then
                            xnorm_on <= '1';
                            if(ref_bram_addra_first = '1') then
                                ref_bram_addra  <= (others => '0'); 
                                ref_bram_addra_first <= '0';
                            else
                                ref_bram_addra <= std_logic_vector(unsigned(ref_bram_addra)+1);
                            end if;  
                            s_axis_data_tvalid_d(0) <= '1';
                            if(unsigned(ref_bram_addra) = (fft_width_configured)-2) then
                                s_axis_data_tlast_d(0)  <= '1'; 
                                state_fft_cc_sm <= wait_ref_fft_end;
                            else
                                s_axis_data_tlast_d(0)  <= '0';
                            end if;
                        else
                            s_axis_data_tvalid_d(0) <= '0';
                            s_axis_data_tlast_d(0)  <= '0';
                        end if;
                    -- Wait for FFT ends
                    when wait_ref_fft_end =>
                       s_axis_data_tvalid_d(0) <= '0';  
                       s_axis_data_tlast_d(0)  <= '0';
                       ref_bram_addra  <= (others => '0'); 
                       ref_bram_addra_first <= '1';
                       if(m_axis_data_tlast = '1') then
                           xnorm_on <= '0';
                           state_fft_cc_sm <= idle;
                       end if;
                    -- Input signal storage in fft_bram
					when store_fft_input =>
					   if(fft_input_axis_valid_d(0) = '1') then
					       fft_bram_wea   <= (others => '1');
					       if(unsigned(fft_bram_addra(0)) <= unsigned(gate_bram_addra_last)) then
                               if(fft_bram_addra_first = '1') then
                                   fft_bram_addra(0) <= (others => '0'); 
                                   fft_bram_addra_first <= '0';
                               else
                                   fft_bram_addra(0) <= std_logic_vector(unsigned(fft_bram_addra(0))+1);					           
                               end if;
                               fft_bram_dina  <= std_logic_vector(resize(signed(fft_input_axis_tdata_d(0)), fft_bram_dina'length)); 
                               gate_input_acc <= std_logic_vector(signed(gate_input_acc) + 
                                                                  resize(signed(fft_input_axis_tdata_d(0)), gate_input_acc'length)); 
                               gate_input_len <= std_logic_vector(unsigned(gate_input_len) + 1);
                           end if;   
					   else
					       fft_bram_addra_first <= '1';
					       fft_bram_addra_stored <= unsigned(fft_bram_addra(0));
					       state_fft_cc_sm <= start_bias_fft;
					       --state_fft_cc_sm <= decide_coherence_cc;
					   end if;
					when start_bias_fft =>
                       corr_signal_tvalid <= '1';
                       corr_signal_tlast <= '1';
                       corr_signal_acc <= gate_input_acc;
                       corr_signal_len <= gate_input_len;
                       state_fft_cc_sm <= end_bias_fft;
                    when end_bias_fft =>
                       corr_signal_tvalid <= '0';
                       corr_signal_tlast <= '0';
                       if(corr_signal_bias_tlast = '1') then
                           gate_bias <= corr_signal_bias;
                           state_fft_cc_sm <= decide_coherence_cc; 
                       end if;
					-- Padding FFT input
					when decide_coherence_cc =>
                       fft_bram_wea <= (others => '0');    
                       if(fft_en_i = '1') then
                           state_fft_cc_sm <= config_coherence;
                       elsif(cc_en_i = '1') then
                           state_fft_cc_sm <= config_fft;
                       else
                           state_fft_cc_sm <= idle;
                       end if;
                    -- Coherence config
                    when config_coherence => 
                       coherence_flag <= '1';
                       fft_fw_inv <= (others => '1');
                       fft_scale_sch_i <= (others => '0');
                       fft_cc_flag <= '1';
                       var_counter := 0;
					   if(configure_fft_module_end = '1') then   
                           configure_fft_module  <= '0';   
                           ref_bram_addra_first <= '1';                             
                           state_fft_cc_sm <= start_coherence;
                       else
                           configure_fft_module  <= '1';    
                       end if;
                    when start_coherence => 
                        coherence_fft_frame <= (others => '0');
                        coherence_fft_width_i <= std_logic_vector((unsigned(fft_width_i) srl 1));  
                        if(sampling_freq = 1) then
                            coherence_fft_width <= std_logic_vector((unsigned(fft_width_i) srl 3));    
                            coherence_fft_nfft <= std_logic_vector(resize(unsigned(fft_nfft), coherence_fft_nfft'length)-3); 
                        elsif(sampling_freq = 2) then
                            coherence_fft_width <= std_logic_vector((unsigned(fft_width_i) srl 2)); 
                            coherence_fft_nfft <= std_logic_vector(resize(unsigned(fft_nfft), coherence_fft_nfft'length)-2); 
                        else
                            coherence_fft_width <= std_logic_vector((unsigned(fft_width_i) srl 1)); 
                            coherence_fft_nfft <= std_logic_vector(resize(unsigned(fft_nfft), coherence_fft_nfft'length)-1); 
                        end if;
                        --coherence_fft_nfft <= std_logic_vector(resize(unsigned(fft_nfft), coherence_fft_nfft'length)-1); 
                        if(unsigned(fft_gate_width) >= fft_segment) then
                            fft_segment_i <= fft_segment;
                        else
                            fft_segment_i <= unsigned(fft_gate_width); 
                        end if; 
                        coherence_read_offset <= (others => '0');
                        state_fft_cc_sm <= perform_coherence_ref;
                    when perform_coherence_ref =>
                        coherence_write_offset <= to_unsigned(2048, coherence_write_offset'length);
                        coherence_write_offset_x <= to_unsigned(2048, coherence_write_offset_x'length);
                        coherence_type <= '0';
                        if(s_axis_data_tready = '1') then
                            if(ref_bram_addra_first = '1') then
                                ref_bram_addra  <= coherence_read_offset(11 downto 0); 
                                ref_bram_addra_first <= '0';
                            else
                                ref_bram_addra <= std_logic_vector(unsigned(ref_bram_addra)+1);
                            end if;  
                            s_axis_data_tvalid_d(0) <= '1';
                            if(var_counter = (fft_width_configured)-1) then
                                s_axis_data_tlast_d(0)  <= '1'; 
                                state_fft_cc_sm <= wait_coherence_ref_end;
                            else
                                var_counter := var_counter + 1;
                                s_axis_data_tlast_d(0)  <= '0';
                            end if;
                        else
                            s_axis_data_tvalid_d(0) <= '0';
                            s_axis_data_tlast_d(0)  <= '0';
                        end if;                
                    when wait_coherence_ref_end =>
                        s_axis_data_tvalid_d(0) <= '0';  
                        s_axis_data_tlast_d(0)  <= '0';
                        ref_bram_addra  <= (others => '0'); 
                        ref_bram_addra_first <= '1';
                        fft_bram_addra_first <= '1';
                        var_counter := 0;
                        if(m_axis_data_tlast = '1') then
                            state_fft_cc_sm <= perform_coherence_gate;
                        end if;        
                    when perform_coherence_gate =>
                       coherence_write_offset <= to_unsigned(3072, coherence_write_offset'length);
                       coherence_write_offset_y <= to_unsigned(3072, coherence_write_offset'length);
                       coherence_type <= '1';
					   if(s_axis_data_tready = '1') then
                            if(fft_bram_addra_first = '1') then
                                fft_bram_addra(0)  <= coherence_read_offset; 
                                fft_bram_addra_first <= '0';
                            else
                                fft_bram_addra(0) <= std_logic_vector(unsigned(fft_bram_addra(0))+1);
                            end if;
                            s_axis_data_tvalid_d(0) <= '1';
                            if(var_counter = (fft_width_configured)-1) then
                                s_axis_data_tlast_d(0)  <= '1'; 
                                state_fft_cc_sm <= wait_coherence_gate_end;
                            else
                                var_counter := var_counter + 1;
                                s_axis_data_tlast_d(0)  <= '0';
                            end if;
                        else
                            s_axis_data_tvalid_d(0) <= '0';
                            s_axis_data_tlast_d(0)  <= '0';
                        end if;
                    when wait_coherence_gate_end => 
                        s_axis_data_tvalid_d(0) <= '0';  
                        s_axis_data_tlast_d(0)  <= '0';
                        fft_bram_addra(0)  <= (others => '0'); 
                        ref_bram_addra_first <= '1';
                        fft_bram_addra_first <= '1';
                        var_counter := 0;
                        if(m_axis_data_tlast = '1') then
                            state_fft_cc_sm <= wait_for_coherence_check;
                        end if; 
                    when wait_for_coherence_check => 
                        if(var_counter >= 64) then
                            var_counter := 0;
                            state_fft_cc_sm <= calculate_local_coherence;
                        else
                            var_counter := var_counter + 1;
                        end if;
                    when calculate_local_coherence =>
                        if(coherence_input_tready = '1') then
                            if(fft_bram_addra_first = '1') then
                                fft_bram_addra(0)  <= std_logic_vector(coherence_write_offset_x);
                                --fft_bram_addra(0)  <= std_logic_vector(coherence_write_offset_x+unsigned(coherence_fft_width_i(BRAM_ADDR_WIDTH-1 downto 0)));
                                fft_bram_addra_first <= '0';
                            else
                                fft_bram_addra(0) <= std_logic_vector(unsigned(fft_bram_addra(0))+1);
                            end if;
                            coherence_input_tvalid_d(0) <= '1';
                            --if(var_counter = (fft_width_configured)-1) then
                            if(var_counter = (unsigned(coherence_fft_width))-1) then
                                var_counter := 0;
                                coherence_input_tlast_d(0)  <= '1'; 
                                coherence_read_offset <= std_logic_vector(unsigned(coherence_read_offset) + fft_segment_half);
                                state_fft_cc_sm <= coherence_check;
                            else
                                var_counter := var_counter + 1;
                                coherence_input_tlast_d(0)  <= '0';
                            end if;
                        else
                            coherence_input_tvalid_d(0) <= '0';
                            coherence_input_tlast_d(0)  <= '0';
                        end if;                 
                    when coherence_check => 
                        coherence_input_tvalid_d(0) <= '0';  
                        coherence_input_tlast_d(0)  <= '0';
                        fft_bram_addra(0)  <= (others => '0'); 
                        fft_bram_addra_first <= '1';
                        if(var_counter >= 64) then
                            var_counter := 0;
                            if(unsigned(coherence_fft_frame) >= 7) then
                                state_fft_cc_sm <= calculate_coherence;
                            else
                                coherence_fft_frame <= std_logic_vector(unsigned(coherence_fft_frame)+1);
                                if(unsigned(coherence_read_offset) < unsigned(fft_gate_width)) then    
                                    if((unsigned(fft_gate_width)-unsigned(coherence_read_offset)) >= fft_segment) then
                                        fft_segment_i <= fft_segment;
                                    else
                                        fft_segment_i <= unsigned(fft_gate_width)-unsigned(coherence_read_offset); 
                                    end if;                     
                                    state_fft_cc_sm <= perform_coherence_ref;
                                else
                                    state_fft_cc_sm <= calculate_coherence;
                                end if;
                            end if;     
                        else
                            var_counter := var_counter + 1;
                        end if;
                    when calculate_coherence =>
                        if(coherence_end = '1') then
                            coherence_fft_frame <= (others => '0');
                            coherence_calc <= '0';
                            state_fft_cc_sm <= config_fft;
                        else
                            coherence_calc <= '1';
                        end if;                                         
					-- FFT configuration
					when config_fft =>	
					   coherence_flag <= '0';				   
					   fft_fw_inv <= (others => '1');
					   fft_scale_sch_i <= (others => '0');
					   fft_cc_flag <= '1';
					   if(configure_fft_module_end = '1') then   
                           configure_fft_module  <= '0';                         
					       state_fft_cc_sm <= perform_fft;
					   else
					       configure_fft_module  <= '1';    
					   end if;
					-- Perform input signal FFT
					when perform_fft => 
					   if(s_axis_data_tready = '1') then
					       if(fft_bram_addra_first = '1') then
					           fft_bram_addra(0)  <= (others => '0'); 
					           fft_bram_addra_first <= '0';
					       else
					           fft_bram_addra(0) <= std_logic_vector(unsigned(fft_bram_addra(0))+1);
					       end if;
					       s_axis_data_tvalid_d(0) <= '1';
                           if(unsigned(fft_bram_addra(0)) = (fft_width_configured)-2) then
                               s_axis_data_tlast_d(0)  <= '1'; 
                               state_fft_cc_sm <= wait_fft_end;
                           else
                               s_axis_data_tlast_d(0)  <= '0';
                           end if;
					   else
					       s_axis_data_tvalid_d(0) <= '0';
					       s_axis_data_tlast_d(0)  <= '0';
					   end if;
					-- Wait for FFT ends
					when wait_fft_end =>
					   s_axis_data_tvalid_d(0) <= '0';  
                       s_axis_data_tlast_d(0)  <= '0';
                       fft_bram_addra(0)  <= (others => '0'); 
					   fft_bram_addra_first <= '1';
					   if(m_axis_data_tlast = '1') then
					       state_fft_cc_sm <= wait_config_ifft;
					   end if;
					when wait_config_ifft =>
					   if(var_counter >= 64) then
					       var_counter := 0;
					       if(cc_en_i = '1') then
					           state_fft_cc_sm <= config_ifft;
					       else
					           state_fft_cc_sm <= fft_result;
					       end if;
					   else
					       var_counter := var_counter + 1;
					   end if;
					-- iFFT configuration for run cross correlation 
			        when config_ifft =>
			            coherence_flag <= '0';
					    fft_fw_inv <= (others => '0');
					    fft_scale_sch_i <= "00000000000000000000000000000000"; 
                        fft_cc_flag <= '1';
                        if(configure_fft_module_end = '1') then   
                            configure_fft_module  <= '0';                         
                            state_fft_cc_sm <= perform_ifft;
                        else
                            configure_fft_module  <= '1';    
                        end if;
                    -- Perform input iFFT(input_signal_FFT.*conj(reference_signal_FFT))
					when perform_ifft =>                      
                        if(s_axis_data_tready = '1') then
                            if(fft_bram_addra_first = '1') then
                                fft_bram_addra(0) <= (others => '0'); 
                                cc_bram_addra(0)  <= (others => '0'); 
                                fft_bram_addra_first <= '0';
                            else
                                fft_bram_addra(0) <= std_logic_vector(unsigned(fft_bram_addra(0))+1);
                                cc_bram_addra(0)  <= std_logic_vector(unsigned(cc_bram_addra(0))+1);
                            end if;   
                            s_axis_data_tvalid_d(0) <= '1';
                            fft_dsp_tvalid_d(0) <= '1';
                            if(unsigned(fft_bram_addra(0)) = (fft_width_configured)-2) then
                                s_axis_data_tlast_d(0)  <= '1'; 
                                state_fft_cc_sm <= wait_ifft_end;
                            else
                                s_axis_data_tlast_d(0)  <= '0';
                            end if; 
                        else
                            s_axis_data_tvalid_d(0) <= '0';
                            s_axis_data_tlast_d(0)  <= '0';
                            fft_dsp_tvalid_d(0) <= '0';
                        end if;
                    -- Wait for iFFT ends
                    when wait_ifft_end =>
                        s_axis_data_tvalid_d(0) <= '0';  
                        s_axis_data_tlast_d(0)  <= '0';
                        fft_dsp_tvalid_d(0)  <= '0';
                        fft_bram_addra(0)  <= (others => '0'); 
                        cc_bram_addra(0)  <= (others => '0'); 
					    fft_bram_addra_first <= '1';
                        if(m_axis_data_tlast = '1') then
                            if(FFT_TEST = '1') then
                                state_fft_cc_sm <= fft_result;
                            else
                                state_fft_cc_sm <= fft_result;
                            end if;
                        end if;
                    -- Provide FFT/Cross correlation results
					when fft_result =>
					   FFT_GATE_END <= '1';
					   FFT_MAX_VALUE <= fft_max_complex;
					   FFT_MAX_SAMPLE <= std_logic_vector(resize(unsigned(fft_max_index), FFT_MAX_SAMPLE'length));
					   FFT_COHERENCE <= coherence;
					   if(cc_en_i = '1') then
--                           CC_MAX_VALUE <= std_logic_vector(ifft_result_max);
                           CC_MAX_VALUE   <= xnorm(divisor_norm+31 downto divisor_norm) & 
                                             std_logic_vector(ifft_result_max(divisor_norm+31 downto divisor_norm)); 
                           CC_MAX_SAMPLE <= std_logic_vector(resize(unsigned(ifft_result_max_index), CC_MAX_SAMPLE'length));
                       else
                           CC_MAX_VALUE <= (others => '0');
                           CC_MAX_SAMPLE <= (others => '0');
                       end if;
                       state_fft_cc_sm <= fft_cc_end;
				    -- End of FFT/Cross correlation process
					when fft_cc_end => 
					   if(FFT_GATE_END_ACK = '1') then
					       FFT_GATE_END <= '0';
					       state_fft_cc_sm <= idle;
					   end if;
					when others =>
					   null;
		        end case;
		    end if;
		end if;
    end process;
    
    -- FFT result storage
    fft_store_process: process(aclk) 
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then    
				cc_bram_web           <= (others => '0');
                cc_bram_addrb         <= (others => '0');          
                cc_bram_dinb          <= (others => '0');
                cc_bram_enb           <= '0';
                fft_bram_web          <= (others => '0');
                fft_bram_addrb        <= (others => '0');          
                fft_bram_dinb         <= (others => '0');
                fft_bram_enb          <= '0';
                cc_bram_addrb_first   <= '1';
                fft_bram_addrb_first  <= '1';
            else
                cc_bram_enb  <= '1';
                fft_bram_enb <= '1';
                if(m_axis_data_tvalid = '1') then
                    -- FFT/iFFT
                    if(fft_cc_flag = '1') then
                        -- FFT
                        if(fft_fw_inv(0) = '1') then
                            fft_bram_web   <= (others => '1');
                            if(coherence_flag = '0') then
                                fft_bram_addrb <= m_axis_data_tuser(BRAM_ADDR_WIDTH-1 downto 0);
                                fft_bram_dinb  <= m_axis_data_tdata(47+fft_nfft_int downto 32+fft_nfft_int) & 
                                                  m_axis_data_tdata(15+fft_nfft_int downto 0+fft_nfft_int);
                            else
--                                fft_bram_addrb <= std_logic_vector(coherence_write_offset+
--                                                                   unsigned(m_axis_data_tuser(BRAM_ADDR_WIDTH-1 downto 0))+
--                                                                   unsigned(m_axis_data_tuser(BRAM_ADDR_WIDTH-1 downto 0)));
                                fft_bram_addrb <= std_logic_vector(coherence_write_offset+unsigned(m_axis_data_tuser(BRAM_ADDR_WIDTH-1 downto 0)));
                                -- Real part of the FFT                                       
                                --fft_bram_dinb  <= m_axis_data_tdata(31 downto 0);
                                
--                                if(fft_nfft_int < 3) then
--                                    fft_bram_dinb  <= m_axis_data_tdata(47+fft_nfft_int downto 32+fft_nfft_int) & 
--                                                      m_axis_data_tdata(15+fft_nfft_int downto 0+fft_nfft_int);
--                                else
--                                    fft_bram_dinb  <= m_axis_data_tdata(47+fft_nfft_int-3 downto 32+fft_nfft_int-3) & 
--                                                      m_axis_data_tdata(15+fft_nfft_int-3 downto 0+fft_nfft_int-3);
--                                end if;
                                
                                fft_bram_dinb  <= m_axis_data_tdata(47 downto 32) & 
                                                  m_axis_data_tdata(15 downto 0);
                                                  
--                                fft_bram_dinb  <= m_axis_data_tdata(47+fft_nfft_int downto 32+fft_nfft_int) & 
--                                                  m_axis_data_tdata(15+fft_nfft_int downto 0+fft_nfft_int);
                            end if;
                        end if;
                    -- Reference signal FFT
                    else
                        -- FFT
                        if(fft_fw_inv(0) = '1') then
                            cc_bram_web   <= (others => '1');
                            cc_bram_dinb  <= m_axis_data_tdata(47+fft_nfft_int downto 32+fft_nfft_int) & 
                                             m_axis_data_tdata(15+fft_nfft_int downto 0+fft_nfft_int);
                            cc_bram_addrb <= m_axis_data_tuser(BRAM_ADDR_WIDTH-1 downto 0);
                        end if;
                    end if; 
                else
                    if(m_axis_data_tvalid_d(1) = '0') then 
                        fft_bram_web <= (others => '0'); 
                        cc_bram_web  <= (others => '0');
                    end if;
                    if(state_fft_cc_sm = calculate_local_coherence) then
                        if(coherence_input_tready = '1') then
                            if(fft_bram_addra_first = '1') then
                                fft_bram_addrb <= std_logic_vector(coherence_write_offset_y);
                                --fft_bram_addrb  <= std_logic_vector(coherence_write_offset_y+unsigned(coherence_fft_width_i(BRAM_ADDR_WIDTH-1 downto 0)));
                            else
                                fft_bram_addrb <= std_logic_vector(unsigned(fft_bram_addrb)+1);
                            end if;
                        end if;
                    else
                        if(m_axis_data_tvalid_d(1) = '0') then 
                            fft_bram_addrb <= (others => '0');
                        end if;
                    end if;
                end if;
            end if;
        end if;
    end process;
    
    s_axis_data_tdata_re <= s_axis_data_tdata(31 downto 0);
    s_axis_data_tdata_im <= s_axis_data_tdata(63 downto 32);
    
    -- FFT input feed
    fft_input_process: process(aclk) 
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then    
                s_axis_data_tvalid  <= '0';                              
                s_axis_data_tlast   <= '0';
                s_axis_data_tdata   <= (others => '0');
                padding_cnt <= (others => '0');
            else
                -- FFT/iFFT
                if(fft_cc_flag = '1') then
                    -- FFT
                    if(fft_fw_inv(0) = '1') then
                        if(s_axis_data_tvalid_d(2) = '1') then
                            if(coherence_flag = '0') then
                                if(padding_cnt >= fft_bram_addra_stored) then
                                    s_axis_data_tdata <= (others => '0'); 
                                else
--                                    s_axis_data_tdata  <= "00000000000000000000000000000000" & 
--                                                  std_logic_vector(resize(signed(fft_bram_douta(REGISTER_W16-1 downto 0)), fft_bram_douta'length));
                                    s_axis_data_tdata  <= "00000000000000000000000000000000" & 
                                                  std_logic_vector(resize(signed(fft_bram_douta(REGISTER_W16-1 downto 0)), fft_bram_douta'length) - 
                                                                   resize(signed(gate_bias), fft_bram_douta'length));
                                    padding_cnt <= padding_cnt + 1;
                                end if;
                            else
                                --if(padding_cnt >= fft_segment) then
                                if(padding_cnt >= fft_segment_i) then
                                    s_axis_data_tdata <= (others => '0'); 
                                else
                                    if(coherence_type = '0') then
                                        s_axis_data_tdata  <= "00000000000000000000000000000000" & 
                                                          std_logic_vector(resize(signed(ref_bram_douta(15 downto 0)), fft_bram_douta'length));
                                    else
                                        s_axis_data_tdata  <= "00000000000000000000000000000000" & 
                                                          std_logic_vector(resize(signed(fft_bram_douta(15 downto 0)), fft_bram_douta'length));  
                                    end if;
                                    padding_cnt <= padding_cnt + 1;
                                end if;
                            end if;
                        else
                            s_axis_data_tdata  <= (others => '0');
                            padding_cnt <= (others => '0'); 
                        end if;
                        s_axis_data_tvalid <= s_axis_data_tvalid_d(2);
                        s_axis_data_tlast  <= s_axis_data_tlast_d(2);        
                    -- iFFF                   
                    else
                        if(FFT_TEST = '1') then
                            s_axis_data_tdata   <= std_logic_vector(resize(signed(fft_bram_douta(31 downto 16)), cm_ifft_s_axis_a_tdata(63 downto 32)'length)) &
                                                   std_logic_vector(resize(signed(fft_bram_douta(15 downto 0)), cm_ifft_s_axis_a_tdata(31 downto 0)'length));   
                            s_axis_data_tvalid <= s_axis_data_tvalid_d(2);
                            s_axis_data_tlast  <= s_axis_data_tlast_d(2);  
                        else
                            s_axis_data_tdata  <= cm_ifft_m_axis_dout_tdata(95 downto 64) & cm_ifft_m_axis_dout_tdata(31 downto 0); 
                            s_axis_data_tvalid <= cm_ifft_m_axis_dout_tvalid;
                            s_axis_data_tlast  <= cm_ifft_m_axis_dout_tlast; 
                        end if;
                    end if;
                -- Reference signal FFT
                else
                    if(s_axis_data_tvalid_d(2) = '1') then
                        if(padding_cnt >= ref_bram_addra_stored) then
                            s_axis_data_tdata <= (others => '0'); 
                        else
--                            s_axis_data_tdata  <= "00000000000000000000000000000000" & 
--                                              std_logic_vector(resize(signed(ref_bram_douta(15 downto 0)), fft_bram_douta'length));
                            s_axis_data_tdata  <= "00000000000000000000000000000000" & 
                                                  std_logic_vector(resize(signed(ref_bram_douta(15 downto 0)), fft_bram_douta'length) - 
                                                                   resize(signed(ref_bias), fft_bram_douta'length));
                            padding_cnt <= padding_cnt + 1;
                        end if;
                    else
                        s_axis_data_tdata  <= (others => '0');
                        padding_cnt <= (others => '0'); 
                    end if;
                    s_axis_data_tvalid <= s_axis_data_tvalid_d(2);
                    s_axis_data_tlast  <= s_axis_data_tlast_d(2);  
                end if;
            end if;
        end if;
    end process;
    
    -- DSP input (calculate FFT module)
    dsp_process: process(aclk) 
        variable tvalid_counter  : integer range 0 to 1024 :=0;
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then    
                fft_max_mod  <= (others => '0');
                fft_max_complex <= (others => '0');
                fft_max_index <= (others => '0');
                m_axis_data_tuser_d <= (others => (others => '0'));
                m_axis_data_tvalid_d  <= (others => '0');
                m_axis_data_tlast_d   <= (others => '0');                                          
                fifo_dout_valid    <= '0'; 
                fifo_dout_valid_d1 <= '0'; 
                tvalid_counter := 0;
                fifo_wr_en <= '0'; 
                fifo_rd_en <= '0';  
                fifo_rd_en_d1 <= '0'; 
                fifo_din  <= (others => '0');
                fifo_data_count_d1 <= (others => '0');
                fft_mul_a1 <= (others => '0');
                fft_mul_b1 <= (others => '0');
                fft_mul_a2 <= (others => '0');
                fft_mul_b2 <= (others => '0');
                fft_mul_sum <= (others => '0');
                xnorm <= (others => '0');
                xnorm_mul_on(0) <= '0';
            else               
                m_axis_data_tuser_d(0) <= m_axis_data_tuser;
                m_axis_data_tuser_d(1) <= m_axis_data_tuser_d(0);
                m_axis_data_tuser_d(2) <= m_axis_data_tuser_d(1);
                m_axis_data_tlast_d(0) <= m_axis_data_tlast;
                m_axis_data_tlast_d(1) <= m_axis_data_tlast_d(0);
                m_axis_data_tlast_d(2) <= m_axis_data_tlast_d(1);
                m_axis_data_tvalid_d(0) <= m_axis_data_tvalid;
                m_axis_data_tvalid_d(1) <= m_axis_data_tvalid_d(0);
                m_axis_data_tvalid_d(2) <= m_axis_data_tvalid_d(1);
                m_axis_data_tvalid_d(3) <= m_axis_data_tvalid_d(2);
                fifo_rd_en_d1 <= fifo_rd_en;
                fifo_dout_valid <= fifo_rd_en_d1;
                fifo_dout_valid_d1 <= fifo_dout_valid;
                fifo_data_count_d1 <= fifo_data_count;
                fft_mul_sum <= unsigned(fft_mul_p1)+unsigned(fft_mul_p2);
                -- Perform DSP (fft_re*fft_re+fft_im*fft_im) when reading the FFT result 
                if(fft_fw_inv(0) = '1' and fft_cc_flag = '1' and coherence_flag = '0') then
                    -- DSP input 
                    fft_mul_a1     <= m_axis_data_tdata(31 downto 0); 
                    fft_mul_b1     <= m_axis_data_tdata(31 downto 0); 
                    fft_mul_a2     <= m_axis_data_tdata(63 downto 32); 
                    fft_mul_b2     <= m_axis_data_tdata(63 downto 32); 
                    fifo_wr_en     <= m_axis_data_tvalid;
                    fifo_din       <= m_axis_data_tuser(REGISTER_W16-1 downto 0) & m_axis_data_tdata;                 
                    if(m_axis_data_tvalid = '1') then
                        if(tvalid_counter >= 5) then
                            fifo_rd_en <= '1';  
                        else     
                            fifo_rd_en <= '0';                 
                            tvalid_counter := tvalid_counter + 1;
                        end if;
                    else
                        if(unsigned(fifo_data_count_d1) = 0) then
                            fifo_rd_en <= '0';
                        end if;
                    end if;              
                    -- DSP result  ==> find the max sample
                    if(fifo_dout_valid = '1') then
                        if(fifo_dout_valid_d1 = '0') then
                            fft_max_mod <= fft_mul_sum;
                            fft_max_complex <= fifo_dout(63 downto 0);
                            fft_max_index <= fifo_dout(79 downto 64);                    
                        else
                            if(fft_mul_sum > fft_max_mod) then
                                fft_max_mod <= fft_mul_sum;
                                fft_max_complex <= fifo_dout(63 downto 0);
                                fft_max_index <= fifo_dout(79 downto 64);       
                            end if; 
                        end if;
                    end if;
                    xnorm_mul_on(0) <= '0';
                else
                    if(xnorm_on = '1') then
                        xnorm_mul_on(0) <= s_axis_data_tvalid;
                        if(s_axis_data_tvalid = '1') then
                            fft_mul_a1 <= s_axis_data_tdata(31 downto 0);
                            fft_mul_b1 <= s_axis_data_tdata(31 downto 0);
                        else
                            fft_mul_a1 <= (others => '0');
                            fft_mul_b1 <= (others => '0'); 
                        end if;
                        if(xnorm_mul_on(5) = '1') then
                            if(xnorm_mul_on(6) = '0') then
                                xnorm <= std_logic_vector(resize(unsigned(fft_mul_p1), xnorm'length));
                            else
                                xnorm <= std_logic_vector(resize(unsigned(fft_mul_p1), xnorm'length) + unsigned(xnorm)); 
                            end if;
                        end if;
                    else
                        xnorm_mul_on(0) <= '0';
                        fft_mul_a1 <= (others => '0');
                        fft_mul_b1 <= (others => '0'); 
                    end if;
                    fft_mul_a2 <= (others => '0');
                    fft_mul_b2 <= (others => '0');
                    tvalid_counter := 0;
                    fifo_wr_en <= '0'; 
                    fifo_rd_en <= '0'; 
                end if;
            end if;
        end if;
    end process;
    
	-- Delay FFT input
    delay_xnorm_mul_on : for i in 1 to 15 generate
        process(aclk) 
        begin
           if (rising_edge(aclk)) then
                if (aresetn = '0') then
                    xnorm_mul_on(i) <= '0';
                else
                    xnorm_mul_on(i) <= xnorm_mul_on(i-1);
                end if;
           end if;
        end process;
    end generate;
    
    div_cc_loop_process : process(aclk) 
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                divisor_norm <= 0;    
            else 
                if(xnorm_mul_on(8) = '1' and xnorm_mul_on(7) = '0') then
                    divisor_norm <= 0;  
                    for I in 31 to 63 loop
                        if(xnorm(I) = '1') then
                            divisor_norm <= I-30;
                        end if;
                    end loop;
                end if;
            end if;
        end if;
    end process;
    
    fifo_dout_index <= fifo_dout(79 downto 64);
    
    -- COMPLEX mul input
    complex_mul_input_process: process(aclk) 
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then    
                cm_ifft_s_axis_a_tvalid  <= '0';                              
                cm_ifft_s_axis_a_tlast   <= '0';
                cm_ifft_s_axis_a_tdata   <= (others => '0');
                cm_ifft_s_axis_b_tvalid  <= '0';                              
                cm_ifft_s_axis_b_tlast   <= '0';
                cm_ifft_s_axis_b_tdata   <= (others => '0');
            else
                -- iFFT selected ==> feed complex multiplier inputs
                if(fft_fw_inv(0) = '0') then 
                    cm_ifft_s_axis_a_tvalid  <= fft_dsp_tvalid_d(2);
                    cm_ifft_s_axis_a_tlast   <= s_axis_data_tlast_d(2); 
                    cm_ifft_s_axis_a_tdata   <= std_logic_vector(resize(signed(fft_bram_douta(31 downto 16)), cm_ifft_s_axis_a_tdata(63 downto 32)'length)) &
                                                std_logic_vector(resize(signed(fft_bram_douta(15 downto 0)), cm_ifft_s_axis_a_tdata(31 downto 0)'length));
                    cm_ifft_s_axis_b_tvalid  <= fft_dsp_tvalid_d(2);
                    cm_ifft_s_axis_b_tlast   <= s_axis_data_tlast_d(2); 
                    cm_ifft_s_axis_b_tdata   <= std_logic_vector(resize(-signed(cc_bram_douta(31 downto 16)), cm_ifft_s_axis_a_tdata(63 downto 32)'length)) &
                                                std_logic_vector(resize(signed(cc_bram_douta(15 downto 0)), cm_ifft_s_axis_a_tdata(31 downto 0)'length));
                else
                    cm_ifft_s_axis_a_tvalid  <= '0';                              
                    cm_ifft_s_axis_a_tlast   <= '0';
                    cm_ifft_s_axis_a_tdata   <= (others => '0');
                    cm_ifft_s_axis_b_tvalid  <= '0';                              
                    cm_ifft_s_axis_b_tlast   <= '0';
                    cm_ifft_s_axis_b_tdata   <= (others => '0');
                end if;
            end if;
        end if;
    end process;
    
    nfft_array_data(0)  <= to_unsigned(8, nfft_array_data(0)'length);
    nfft_array_data(1)  <= to_unsigned(8, nfft_array_data(0)'length);
    nfft_array_data(2)  <= to_unsigned(8, nfft_array_data(0)'length);
    nfft_array_data(3)  <= to_unsigned(8, nfft_array_data(0)'length);
    nfft_array_data(4)  <= to_unsigned(16, nfft_array_data(0)'length);
    nfft_array_data(5)  <= to_unsigned(32, nfft_array_data(0)'length);
    nfft_array_data(6)  <= to_unsigned(64, nfft_array_data(0)'length);
    nfft_array_data(7)  <= to_unsigned(128, nfft_array_data(0)'length);
    nfft_array_data(8)  <= to_unsigned(256, nfft_array_data(0)'length);
    nfft_array_data(9)  <= to_unsigned(512, nfft_array_data(0)'length);
    nfft_array_data(10) <= to_unsigned(1024, nfft_array_data(0)'length);
    nfft_array_data(11) <= to_unsigned(2048, nfft_array_data(0)'length);
    nfft_array_data(12) <= to_unsigned(4096, nfft_array_data(0)'length);
    nfft_array_data(13) <= to_unsigned(8192, nfft_array_data(0)'length);
    nfft_array_data(14) <= to_unsigned(16384, nfft_array_data(0)'length);
    nfft_array_data(15) <= to_unsigned(32768, nfft_array_data(0)'length);
    
    shift_ifft_data(0)  <= 7;
    shift_ifft_data(1)  <= 7;
    shift_ifft_data(2)  <= 7;
    shift_ifft_data(3)  <= 7;
    shift_ifft_data(4)  <= 8;
    shift_ifft_data(5)  <= 9;
    shift_ifft_data(6)  <= 10;
    shift_ifft_data(7)  <= 11;
    shift_ifft_data(8)  <= 12;
    shift_ifft_data(9)  <= 13;
    shift_ifft_data(10) <= 14;
    shift_ifft_data(11) <= 15;
    shift_ifft_data(12) <= 16;
    shift_ifft_data(13) <= 17;
    shift_ifft_data(14) <= 18;
    shift_ifft_data(15) <= 19;
            
    -- FFT configuration process
    fft_config_process: process(aclk) 
        variable var_counter_wait  : integer range 0 to 65536 :=0;
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then    
                var_counter_wait        := 0;
                s_axis_config_tvalid    <= '0';
                s_axis_config_tdata     <= (others => '0');
                fft_nfft                <= (others => '0');              
                fft_nfft_counter        <= (others => '0');
                fft_width_i             <= (others => '0');
                fft_width_configured    <= (others => '0');
                fft_fw_inv_i            <= (others => '0');
                fft_fw_inv_configured   <= (others => '0');
                configure_fft_module_end<= '0';
                fft_nfft_int            <= 0;
                state_fft_config_sm     <= idle;
            else
                fft_nfft_int <= to_integer(unsigned(fft_nfft));
                case state_fft_config_sm is        
                    when idle =>                                                             
                        var_counter_wait := 0;  
                        fft_nfft_counter <= (others => '0');
                        if(s_axis_config_tready = '1') then
                           if(configure_fft_module = '1') then
                                if(coherence_flag = '0')then
                                    state_fft_config_sm  <= capture_params;  
                                else
                                    state_fft_config_sm  <= configuration_coherence;  
                                end if;
                           end if;
                        end if;                  
                    when capture_params =>
                        fft_scale_sch <= fft_scale_sch_i(25 downto 0); 
                        if(var_counter_wait >= 14) then
                            var_counter_wait := 0;  
                            state_fft_config_sm  <= adjust_params;  
                        else
                            if(fft_width_temp >= nfft_array_data(var_counter_wait)) then
                                fft_width_i <= nfft_array_data(var_counter_wait); 
                                fft_nfft    <= std_logic_vector(fft_nfft_counter);
                            end if;   
                            fft_fw_inv_i  <= fft_fw_inv;                                                      
                            fft_nfft_counter <= fft_nfft_counter + 1;
                            var_counter_wait := var_counter_wait + 1;
                        end if;                     
                    when adjust_params => 
--                        if((fft_fw_inv(0) = '1' and fft_cc_flag = '0' and cc_en_i = '1') or 
--                           (fft_fw_inv(0) = '0' and fft_cc_flag = '1') or
--                           (fft_fw_inv(0) = '1' and fft_cc_flag = '1' and cc_en_i = '1')) then
                            -- Reference FFT 2N
                            fft_nfft <= std_logic_vector(unsigned(fft_nfft)+1);   
                            fft_width_i <= (unsigned(fft_width_i) sll 1);   
--                        end if; 
                        state_fft_config_sm  <= configuration_start;   
                    when configuration_coherence =>
                        fft_scale_sch <= fft_scale_sch_i(25 downto 0); 
                        if(fft_segment > 256) then
                            fft_width_i <= to_unsigned(512, fft_width_i'length);
                            fft_nfft <= std_logic_vector(to_unsigned(9, fft_nfft'length));
                        else
                            fft_width_i <= to_unsigned(256, fft_width_i'length);
                            fft_nfft <= std_logic_vector(to_unsigned(8, fft_nfft'length));
                        end if;   
                        fft_fw_inv_i  <= fft_fw_inv;  
                        state_fft_config_sm  <= configuration_start;          
                    when configuration_start =>
                        if(s_axis_config_tready = '1') then
                            fft_width_configured    <= fft_width_i;
                            fft_fw_inv_configured   <= fft_fw_inv_i;
                            s_axis_config_tvalid    <= '1';
                            s_axis_config_tdata     <= "00000" & fft_scale_sch & fft_fw_inv_i & fft_nfft;
                            state_fft_config_sm     <= configuration_end;  
                        else
                            state_fft_config_sm     <= idle;
                        end if; 
                    when configuration_end => 
                        s_axis_config_tvalid <= '0';
                        if(configure_fft_module = '0') then
                            configure_fft_module_end <= '0';
                            state_fft_config_sm  <= idle;
                        else
                            configure_fft_module_end <= '1';
                        end if;   
					when others =>
                       null;
                end case;
            end if;
        end if;
    end process;
    
	-- iFFT result
    ifft_result_process : process(aclk) 
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                ifft_result_tdata_i <= (others => '0');
                ifft_result_tdata_i1 <= (others => '0');
                ifft_result_tdata <= (others => '0');
                ifft_result_tvalid <= '0';
                ifft_result_tlast <= '0';
                ifft_result_max <= (others => '0');
                ifft_result_max_index <= (others => '0');  
                ifft_result_offset_index <= (others => '0');            
            else
                ifft_result_tdata_i <= resize(signed(m_axis_data_tdata(31 downto 0)), ifft_result_tdata_i'length); 
               -- ifft_result_tdata_i1 <= ifft_result_tdata_i;
                ifft_result_tdata_i1 <= shift_left(ifft_result_tdata_i, fft_nfft_int+1);
                if(fft_fw_inv(0) = '0') then
                    ifft_result_tdata <= ifft_result_tdata_i1;
                    ifft_result_tvalid <= m_axis_data_tvalid_d(1);
                    ifft_result_tlast <= m_axis_data_tlast_d(1);
                else
                    ifft_result_tdata <= (others => '0');
                    ifft_result_tvalid <= '0';
                    ifft_result_tlast <= '0';
                end if;
                if(ifft_result_tvalid = '1') then
                    if(m_axis_data_tvalid_d(3) = '0') then
                        ifft_result_max <= ifft_result_tdata;
                        ifft_result_max_index <= std_logic_vector(
                                                    unsigned(m_axis_data_tuser_d(2)(REGISTER_W16-1 downto 0))+
                                                    ifft_result_offset_index
                                                  );             
                    else
                        if(ifft_result_tdata > ifft_result_max) then
                            ifft_result_max <= ifft_result_tdata;
                            ifft_result_max_index <= std_logic_vector(
                                                        unsigned(m_axis_data_tuser_d(2)(REGISTER_W16-1 downto 0))+
                                                        ifft_result_offset_index
                                                     );    
                        end if; 
                    end if;
                end if;
            end if;
        end if;
    end process;
    
	-- Status FFT 
    status_fft_input_process : process(aclk) 
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                FFT_STATUS_AXIS_TVALID <= '0';
                FFT_STATUS_AXIS_TLAST  <= '0';
                FFT_STATUS_AXIS_TDATA  <= (others => '0');
                FFT_STATUS_AXIS_TUSER  <= (others => '0');
                fft_status_axis_tdata_re <= (others => '0');
            else
                FFT_STATUS_AXIS_TVALID <= m_axis_data_tvalid;
                FFT_STATUS_AXIS_TLAST  <= m_axis_data_tlast;            
                FFT_STATUS_AXIS_TUSER  <= m_axis_data_tuser;
                fft_status_axis_tdata_re <= std_logic_vector(resize(signed(m_axis_data_tdata(31 downto 0)), fft_status_axis_tdata_re'length));
                FFT_STATUS_AXIS_TDATA  <= m_axis_data_tdata;         
            end if;
        end if;
    end process;
     
    fft_gate_FFT_inst : fft_gate_FFT
      PORT MAP (
        aclk => aclk,
        aresetn => aresetn,
        s_axis_config_tdata => s_axis_config_tdata,
        s_axis_config_tvalid => s_axis_config_tvalid,
        s_axis_config_tready => s_axis_config_tready,
        s_axis_data_tdata => s_axis_data_tdata,
        s_axis_data_tvalid => s_axis_data_tvalid,
        s_axis_data_tready => s_axis_data_tready,
        s_axis_data_tlast => s_axis_data_tlast,
        m_axis_data_tdata => m_axis_data_tdata,
        m_axis_data_tuser => m_axis_data_tuser,
        m_axis_data_tvalid => m_axis_data_tvalid,
        m_axis_data_tready => m_axis_data_tready,
        m_axis_data_tlast => m_axis_data_tlast,
        m_axis_status_tdata => m_axis_status_tdata,
        m_axis_status_tvalid => m_axis_status_tvalid,
        m_axis_status_tready => m_axis_status_tready,
        event_frame_started => event_frame_started,
        event_tlast_unexpected => event_tlast_unexpected,
        event_tlast_missing => event_tlast_missing,
        event_fft_overflow => event_fft_overflow,
        event_status_channel_halt => event_status_channel_halt,
        event_data_in_channel_halt => event_data_in_channel_halt,
        event_data_out_channel_halt => event_data_out_channel_halt
      );
           
    fft_BRAM_inst : fft_gate_BRAM
      PORT MAP (
        clka => aclk,
        ena => fft_bram_ena,
        wea => fft_bram_wea,
        addra => fft_bram_addra(0),
        dina => fft_bram_dina,
        douta => fft_bram_douta,
        clkb => aclk,
        enb => fft_bram_enb,
        web => fft_bram_web,
        addrb => fft_bram_addrb,
        dinb => fft_bram_dinb,
        doutb => fft_bram_doutb
      );
          
    cc_BRAM_inst : fft_gate_BRAM
      PORT MAP (
        clka => aclk,
        ena => cc_bram_ena,
        wea => cc_bram_wea,
        addra => cc_bram_addra(0),
        dina => cc_bram_dina,
        douta => cc_bram_douta,
        clkb => aclk,
        enb => cc_bram_enb,
        web => cc_bram_web,
        addrb => cc_bram_addrb,
        dinb => cc_bram_dinb,
        doutb => cc_bram_doutb
      );
      
    fft_ref_BRAM_inst : fft_ref_BRAM
        PORT MAP (
          clka => aclk,
          ena => ref_bram_ena,
          wea => ref_bram_wea,
          addra => ref_bram_addra(10 downto 0),
          dina => ref_bram_dina,
          douta => ref_bram_douta
        );
            
    fft_MUL1_inst : fft_MUL
      PORT MAP (
        CLK => aclk,
        A => fft_mul_a1,
        B => fft_mul_b1,
        P => fft_mul_p1
      );
        
    fft_MUL2_inst : fft_MUL
      PORT MAP (
        CLK => aclk,
        A => fft_mul_a2,
        B => fft_mul_b2,
        P => fft_mul_p2
      );
      
    ifft_complex_MUL_inst : fft_complex_MUL
      PORT MAP (
        aclk => aclk,
        s_axis_a_tvalid => cm_ifft_s_axis_a_tvalid,
        s_axis_a_tlast => cm_ifft_s_axis_a_tlast,
        s_axis_a_tdata => cm_ifft_s_axis_a_tdata,
        s_axis_b_tvalid => cm_ifft_s_axis_b_tvalid,
        s_axis_b_tlast => cm_ifft_s_axis_b_tlast,
        s_axis_b_tdata => cm_ifft_s_axis_b_tdata,
        m_axis_dout_tvalid => cm_ifft_m_axis_dout_tvalid,
        m_axis_dout_tlast => cm_ifft_m_axis_dout_tlast,
        m_axis_dout_tdata => cm_ifft_m_axis_dout_tdata
      );
      
    fft_gate_FIFO_inst : fft_gate_FIFO
        PORT MAP (
          clk => aclk,
          rst => fifo_rst,
          din => fifo_din,
          wr_en => fifo_wr_en,
          rd_en => fifo_rd_en,
          dout => fifo_dout,
          full => fifo_full,
          empty => fifo_empty,
          data_count => fifo_data_count
        );
        
    fifo_rst <= (not aresetn);
    
    -- Coherence input process
    coherence_input_process: process(aclk) 
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then    
                coherence_input_tvalid  <= '0';                              
                coherence_input_tlast   <= '0';
                coherence_input_tdata_x   <= (others => '0');
                coherence_input_tdata_y   <= (others => '0');
            else
                coherence_input_tvalid  <= coherence_input_tvalid_d(2);
                coherence_input_tlast   <= coherence_input_tlast_d(2); 
                coherence_input_tdata_x <= fft_bram_douta;
                coherence_input_tdata_y <= fft_bram_doutb;
            end if;
        end if;
    end process;
    
    fft_gate_coherence_inst: fft_gate_coherence 
        generic map(
            REGISTER_W64        => REGISTER_W64,
            REGISTER_W32        => REGISTER_W32,
            REGISTER_W24        => REGISTER_W24,
            REGISTER_W16        => REGISTER_W16
        )
        port map(
            -- Sync
            aclk         => aclk,
            aresetn      => aresetn,
            
            -- Control
            COHERENCE_FFT_WIDTH => coherence_fft_width,
            COHERENCE_NFFT      => coherence_fft_nfft,
            COHERENCE_FFT_FRAME => coherence_fft_frame,
            COHERENCE_CALC  => coherence_calc,
            COHERENCE_END   => coherence_end,
            
            -- Correlation Bias Calculation
            CORR_SIGNAL_ACC     => corr_signal_acc,
            CORR_SIGNAL_LEN     => corr_signal_len,
            CORR_SIGNAL_TVALID  => corr_signal_tvalid,
            CORR_SIGNAL_TLAST   => corr_signal_tlast,
            CORR_SIGNAL_BIAS    => corr_signal_bias,      
            CORR_SIGNAL_BIAS_TVALID  => corr_signal_bias_tvalid,
            CORR_SIGNAL_BIAS_TLAST   => corr_signal_bias_tlast,
                             
            -- Signal input
            FFT_INPUT_AXIS_TREADY  => coherence_input_tready,
            FFT_INPUT_AXIS_TDATA_X => coherence_input_tdata_x,
            FFT_INPUT_AXIS_TDATA_Y => coherence_input_tdata_y,
            FFT_INPUT_AXIS_TLAST   => coherence_input_tlast,
            FFT_INPUT_AXIS_TVALID  => coherence_input_tvalid,
            
            COHERENCE        => coherence,
            COHERENCE_TVALID => coherence_valid
        );
   
end arch_imp;
