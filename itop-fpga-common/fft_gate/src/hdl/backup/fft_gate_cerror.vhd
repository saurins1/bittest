----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 05/01/2018 10:07:23 AM
-- Design Name: 
-- Module Name: fft gate - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity fft_gate_cerror is
	generic (
	    REGISTER_W64        : INTEGER := 64;
        REGISTER_W32        : INTEGER := 32;
        REGISTER_W24        : INTEGER := 24;
        REGISTER_W16        : INTEGER := 16;
        REGISTER_W8         : INTEGER := 8
	);
	port (
	    -- Sync
	    aclk         : in STD_LOGIC;
        aresetn      : in STD_LOGIC;
        
        SHIFT_RESULT	 : in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
	        	
		-- Signal input
		FFT_INPUT_AXIS_TREADY  : out STD_LOGIC;
		FFT_INPUT_AXIS_TDATA_A : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		FFT_INPUT_AXIS_TDATA_B : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		FFT_INPUT_AXIS_TLAST   : in STD_LOGIC;
		FFT_INPUT_AXIS_TVALID  : in STD_LOGIC;
        
        CUADRATIC_ERROR        : out STD_LOGIC_VECTOR(REGISTER_W64-1 downto 0);
        CUADRATIC_ERROR_TVALID : out STD_LOGIC
	);
end fft_gate_cerror;

architecture arch_imp of fft_gate_cerror is

    signal fft_input_tvalid : STD_LOGIC_VECTOR(11 DOWNTO 0);
    signal fft_input_tlast  : STD_LOGIC_VECTOR(11 DOWNTO 0);
  
    -- Multiplexer
    COMPONENT fft_MUL_2
      PORT (
        CLK : IN STD_LOGIC;
        A : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        B : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        P : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
      );
    END COMPONENT;
    
    -- Multiplexer
    COMPONENT fft_MUL_4
      PORT (
        CLK : IN STD_LOGIC;
        A : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        B : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        P : OUT STD_LOGIC_VECTOR(63 DOWNTO 0)
      );
    END COMPONENT;
    
    signal fft_mul_re_a : STD_LOGIC_VECTOR(15 DOWNTO 0);
    signal fft_mul_im_a : STD_LOGIC_VECTOR(15 DOWNTO 0);
    signal fft_mul_re_b : STD_LOGIC_VECTOR(15 DOWNTO 0);
    signal fft_mul_im_b : STD_LOGIC_VECTOR(15 DOWNTO 0);
    
    signal fft_mul_re_p_a : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal fft_mul_im_p_a : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal fft_mul_re_p_b : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal fft_mul_im_p_b : STD_LOGIC_VECTOR(31 DOWNTO 0);
    
    signal fft_sum_a : STD_LOGIC_VECTOR(127 DOWNTO 0);
    signal fft_sum_b : STD_LOGIC_VECTOR(127 DOWNTO 0);
    
    signal fft_sum_a_norm : STD_LOGIC_VECTOR(63 DOWNTO 0);
    signal fft_sum_b_norm : STD_LOGIC_VECTOR(63 DOWNTO 0);
    
    signal error_sub    : STD_LOGIC_VECTOR(63 DOWNTO 0);
    signal error_sub_tvalid : STD_LOGIC;
    signal error_sub_tlast  : STD_LOGIC;
    
    signal error_mul_p  : STD_LOGIC_VECTOR(63 DOWNTO 0);
    signal error_mul_p_tvalid : STD_LOGIC;
    signal error_mul_p_tvalid_d1 : STD_LOGIC;
    signal error_mul_p_tlast  : STD_LOGIC;
    signal error_mul_p_tlast_d1  : STD_LOGIC;
    signal error_mul_p_tlast_d2  : STD_LOGIC;
    signal error_mul_p_tlast_d3  : STD_LOGIC;
    
    signal error_acc : UNSIGNED(127 DOWNTO 0);
    
    signal shift_result_int	: INTEGER range 0 to 255 :=0;
    
begin

	-- Delay FFT input
    delay_fft_input_process : for i in 1 to 11 generate
        process(aclk) 
        begin
           if (rising_edge(aclk)) then
                if (aresetn = '0') then
                    fft_input_tvalid(i) <= '0';
                    fft_input_tlast(i) <= '0';
                else
                    fft_input_tvalid(i) <= fft_input_tvalid(i-1);
                    fft_input_tlast(i) <= fft_input_tlast(i-1);
                end if;
           end if;
        end process;
    end generate;
       
    input_process : process(aclk) 
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                FFT_INPUT_AXIS_TREADY <= '0'; 
                fft_input_tvalid(0) <= '0';
                fft_input_tlast(0) <= '0';
                fft_mul_re_a  <= (others => '0');
                fft_mul_im_a  <= (others => '0');
                fft_mul_re_b  <= (others => '0');
                fft_mul_im_b  <= (others => '0');
                fft_sum_a     <= (others => '0');
                fft_sum_b     <= (others => '0');
                fft_sum_a_norm <= (others => '0');
                fft_sum_b_norm <= (others => '0');
                error_sub     <= (others => '0');
                error_sub_tvalid <= '0';
                error_sub_tlast  <= '0';
                error_mul_p_tvalid <= '0';
                error_mul_p_tlast <= '0';
                error_mul_p_tvalid_d1 <= '0';
                error_mul_p_tlast_d1 <= '0';
                error_mul_p_tlast_d2 <= '0';
                error_mul_p_tlast_d3 <= '0';
            else
                FFT_INPUT_AXIS_TREADY <= '1'; 
                fft_input_tvalid(0) <= FFT_INPUT_AXIS_TVALID;
                fft_input_tlast(0)  <= FFT_INPUT_AXIS_TLAST;
                if(FFT_INPUT_AXIS_TVALID = '1') then
                    fft_mul_re_a  <= FFT_INPUT_AXIS_TDATA_A(15 downto 0);
                    fft_mul_im_a  <= FFT_INPUT_AXIS_TDATA_A(31 downto 16);
                    fft_mul_re_b  <= FFT_INPUT_AXIS_TDATA_B(15 downto 0);
                    fft_mul_im_b  <= FFT_INPUT_AXIS_TDATA_B(31 downto 16);
                else
                    fft_mul_re_a  <= (others => '0');
                    fft_mul_im_a  <= (others => '0');
                    fft_mul_re_b  <= (others => '0');
                    fft_mul_im_b  <= (others => '0');
                end if;               
                -- fft_mul_re_p_a result fft_input_tvalid(3)
                fft_sum_a     <= std_logic_vector(resize(unsigned(fft_mul_re_p_a), fft_sum_a'length) + 
                                                  resize(unsigned(fft_mul_im_p_a), fft_sum_a'length));
                fft_sum_b     <= std_logic_vector(resize(unsigned(fft_mul_re_p_b), fft_sum_a'length) + 
                                                  resize(unsigned(fft_mul_im_p_b), fft_sum_a'length));  
                -- fft_sum_a result fft_input_tvalid(4)                                  
                --fft_sum_a_norm <= fft_sum_a(63+shift_result_int downto shift_result_int);     
                --fft_sum_b_norm <= fft_sum_b(63+shift_result_int downto shift_result_int); 
                fft_sum_a_norm <= fft_sum_a(63 downto 0);     
                fft_sum_b_norm <= fft_sum_b(63 downto 0); 
                -- fft_sum_a_norm result fft_input_tvalid(5) 
                if(signed(fft_sum_a_norm) >= signed(fft_sum_b_norm)) then                                           
                    error_sub    <= std_logic_vector(signed(fft_sum_a_norm) - 
                                                     signed(fft_sum_b_norm));
                else
                    error_sub    <= std_logic_vector(signed(fft_sum_b_norm) - 
                                                     signed(fft_sum_a_norm));
                end if;
                -- error_sub result fft_input_tvalid(6)                                
                error_sub_tvalid <= fft_input_tvalid(6);
                error_sub_tlast  <= fft_input_tlast(6);
                error_mul_p_tvalid <= fft_input_tvalid(9);
                error_mul_p_tlast  <= fft_input_tlast(9);
                error_mul_p_tvalid_d1 <= error_mul_p_tvalid;
                error_mul_p_tlast_d1 <= error_mul_p_tlast;
                error_mul_p_tlast_d2 <= error_mul_p_tlast_d1;
                error_mul_p_tlast_d3 <= error_mul_p_tlast_d2;
            end if;
        end if;
    end process;
    
    result_process : process(aclk) 
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                shift_result_int <= 0;
                error_acc <= (others => '0');
                CUADRATIC_ERROR <= (others => '0');
                CUADRATIC_ERROR_TVALID <= '0';
            else
                shift_result_int <= to_integer(unsigned(SHIFT_RESULT));
                if(error_mul_p_tvalid = '1') then
                    if(error_mul_p_tvalid_d1 = '0') then
                        error_acc <= resize(unsigned(error_mul_p), error_acc'length);  
                    else                  
                        error_acc <= resize(unsigned(error_mul_p), error_acc'length)+error_acc;  
                    end if; 
                    CUADRATIC_ERROR_TVALID <= '0';  
                else
                    if(error_mul_p_tlast_d2 = '1') then 
                        CUADRATIC_ERROR <= std_logic_vector(error_acc(63 downto 0));
                        --CUADRATIC_ERROR <= std_logic_vector(error_acc_i(63+shift_result_int downto shift_result_int));
                        CUADRATIC_ERROR_TVALID <= '1';
                    else              
                        --error_acc <= (others => '0');         
                        CUADRATIC_ERROR_TVALID <= '0';
                    end if;                
                end if;
            end if;
        end if;
    end process;
      
    fft_MUL_A_RE_inst : fft_MUL_2
      PORT MAP (
        CLK => aclk,
        A => fft_mul_re_a,
        B => fft_mul_re_a,
        P => fft_mul_re_p_a
      );
      
    fft_MUL_A_IM_inst : fft_MUL_2
      PORT MAP (
        CLK => aclk,
        A => fft_mul_im_a,
        B => fft_mul_im_a,
        P => fft_mul_im_p_a
      );
      
    fft_MUL_B_RE_inst : fft_MUL_2
      PORT MAP (
        CLK => aclk,
        A => fft_mul_re_b,
        B => fft_mul_re_b,
        P => fft_mul_re_p_b
      );
      
    fft_MUL_B_IM_inst : fft_MUL_2
      PORT MAP (
        CLK => aclk,
        A => fft_mul_im_b,
        B => fft_mul_im_b,
        P => fft_mul_im_p_b
      );
      
    fft_MUL_SUBinst : fft_MUL_4
        PORT MAP (
          CLK => aclk,
          A => error_sub(31 downto 0),
          B => error_sub(31 downto 0),
          P => error_mul_p
        );
    
end arch_imp;
