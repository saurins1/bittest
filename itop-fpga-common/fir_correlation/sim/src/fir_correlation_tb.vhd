library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity fir_correlation_tb is
	generic (
    -- Users to add parameters here
        REGISTER_WIDTH  : integer    := 32;
        REGISTER_HALF_WIDTH  : integer    := 16;
        n_taps          : integer    := 8;
        n_sets_tap      : integer    := 3;
        oversampling    : integer    := 2;
        wait_cycles     : integer    := 8;
        tap_width       : integer    := 16;
        
        CORRELATION_WIDTH  : integer    := 64;
        -- Parameters of Axi Slave Bus Interface S00_AXIS
        C_S00_AXIS_TDATA_WIDTH    : integer    := 16;
    
        -- Parameters of Axi Master Bus Interface M00_AXIS
        C_M00_AXIS_TDATA_WIDTH    : integer    := 16
    );
end;

architecture bench of fir_correlation_tb is
 
  component fir_correlation is
      generic (
          -- Users to add parameters here
          REGISTER_WIDTH  : integer    := 32;
          REGISTER_HALF_WIDTH  : integer    := 16;
          n_taps          : integer    := 4;
          n_sets_tap      : integer    := 3;
          oversampling    : integer    := 1;
          wait_cycles     : integer    := 8;
          tap_width       : integer    := 16;
          
          CORRELATION_WIDTH  : integer    := 64;
          -- Parameters of Axi Slave Bus Interface S00_AXIS
          C_S00_AXIS_TDATA_WIDTH    : integer    := 16;
      
          -- Parameters of Axi Master Bus Interface M00_AXIS
          C_M00_AXIS_TDATA_WIDTH    : integer    := 16
      );
      port (
          aclk         : in std_logic;
          aresetn      : in std_logic;
          
          READ_RESULT                 : in std_logic;
          DATA_WINDOW                 : in std_logic_vector(REGISTER_HALF_WIDTH-1 downto 0); 
          
          BAND_PASS_FILTER_EN         : in std_logic;
          BAND_PASS_FILTER_L_DIVIDER  : in std_logic_vector(REGISTER_HALF_WIDTH-1 downto 0);
          GAUSSIAN_FILTER_EN          : in std_logic;
          GAUSSIAN_FILTER_L_DIVIDER   : in std_logic_vector(REGISTER_HALF_WIDTH-1 downto 0);
          LOW_PASS_FILTER_EN          : in std_logic;
          LOW_PASS_FILTER_L_DIVIDER   : in std_logic_vector(REGISTER_HALF_WIDTH-1 downto 0);
          
          FIR_END                     : out std_logic;
                       
          fir_taps_axis_tready        : out std_logic;
          fir_taps_axis_tdata         : in std_logic_vector(C_S00_AXIS_TDATA_WIDTH-1 downto 0);
          fir_taps_axis_tlast         : in std_logic;
          fir_taps_axis_tvalid        : in std_logic;
       
          signal_input_axis_tready    : out std_logic;
          signal_input_axis_tdata     : in std_logic_vector(C_S00_AXIS_TDATA_WIDTH-1 downto 0);
          signal_input_axis_tlast     : in std_logic;
          signal_input_axis_tvalid    : in std_logic;
          
          GATES_START                 : in std_logic_vector(REGISTER_HALF_WIDTH-1 downto 0);
          GATES_END                   : in std_logic_vector(REGISTER_HALF_WIDTH-1 downto 0);
          
          gates_axis_tvalid           : out std_logic;
          gates_axis_tdata            : out std_logic_vector(C_M00_AXIS_TDATA_WIDTH-1 downto 0);
          gates_axis_tlast            : out std_logic;
          gates_axis_tready           : in std_logic;
          
          CORRELATION_EN              : in std_logic;
          CORRELATION_GATE_START                  : in std_logic_vector(REGISTER_HALF_WIDTH-1 downto 0);
          CORRELATION_GATE_END                    : in std_logic_vector(REGISTER_HALF_WIDTH-1 downto 0);
          
          correlation_axis_tvalid     : out std_logic;
          correlation_axis_tdata      : out std_logic_vector(CORRELATION_WIDTH-1 downto 0);
          correlation_axis_tlast      : out std_logic;
          correlation_axis_tready     : in std_logic;
      
          -- Ports of Axi Master Bus Interface M00_AXIS
          fir_result_axis_tvalid      : out std_logic;
          fir_result_axis_tdata       : out std_logic_vector(C_M00_AXIS_TDATA_WIDTH-1 downto 0);
          fir_result_axis_tlast       : out std_logic;
          fir_result_axis_tready      : in std_logic
      );
  end component;

  signal aclk: std_logic:= '0';
  signal aresetn: std_logic:= '0';
  
  constant aclk_period: time := 10 ns;
  
  signal READ_RESULT: std_logic:= '0';
  signal DATA_WINDOW: std_logic_vector(REGISTER_HALF_WIDTH-1 downto 0):= (others => '0');
  signal BAND_PASS_FILTER_EN: std_logic:= '0';
  signal BAND_PASS_FILTER_L_DIVIDER: std_logic_vector(REGISTER_HALF_WIDTH-1 downto 0):= (others => '0');
  signal GAUSSIAN_FILTER_EN: std_logic:= '0';
  signal GAUSSIAN_FILTER_L_DIVIDER: std_logic_vector(REGISTER_HALF_WIDTH-1 downto 0):= (others => '0');
  signal LOW_PASS_FILTER_EN: std_logic:= '0';
  signal LOW_PASS_FILTER_L_DIVIDER: std_logic_vector(REGISTER_HALF_WIDTH-1 downto 0):= (others => '0');
  signal FIR_END: std_logic;
  signal fir_taps_axis_tready: std_logic;
  signal fir_taps_axis_tdata: std_logic_vector(C_S00_AXIS_TDATA_WIDTH-1 downto 0):= (others => '0');
  signal fir_taps_axis_tlast: std_logic:= '0';
  signal fir_taps_axis_tvalid: std_logic:= '0';
  signal signal_input_axis_tready: std_logic;
  signal signal_input_axis_tdata: std_logic_vector(C_S00_AXIS_TDATA_WIDTH-1 downto 0):= (others => '0');
  signal signal_input_axis_tlast: std_logic:= '0';
  signal signal_input_axis_tvalid: std_logic:= '0';
  signal CORRELATION_EN : std_logic := '0';
  signal GATES_START: std_logic_vector(REGISTER_HALF_WIDTH-1 downto 0):= (others => '0');
  signal GATES_END: std_logic_vector(REGISTER_HALF_WIDTH-1 downto 0):= (others => '0');
  signal gates_axis_tvalid: std_logic;
  signal gates_axis_tdata: std_logic_vector(C_M00_AXIS_TDATA_WIDTH-1 downto 0);
  signal gates_axis_tlast: std_logic;
  signal gates_axis_tready: std_logic:= '0';
  signal CORRELATION_GATE_START: std_logic_vector(REGISTER_HALF_WIDTH-1 downto 0):= (others => '0');
  signal CORRELATION_GATE_END: std_logic_vector(REGISTER_HALF_WIDTH-1 downto 0):= (others => '0');
  signal correlation_axis_tvalid: std_logic;
  signal correlation_axis_tdata: std_logic_vector(CORRELATION_WIDTH-1 downto 0);
  signal correlation_axis_tlast: std_logic;
  signal correlation_axis_tready: std_logic:= '0';
  signal fir_result_axis_tvalid: std_logic;
  signal fir_result_axis_tdata: std_logic_vector(C_M00_AXIS_TDATA_WIDTH-1 downto 0);
  signal fir_result_axis_tlast: std_logic;
  signal fir_result_axis_tready: std_logic:= '0';
  
  type states_sm is (idle, load_taps, load_signal, wait_gates, wait_corr, read_fir_result); 
  signal state_sm : states_sm; 
  
  signal start_sm: std_logic:= '0';
  signal corr_on: std_logic:= '0';

begin

   -- aclk process 
    aclk_process :process
    begin
        aclk <= not aclk;
        wait for aclk_period/2;
        aclk <= not aclk;
        wait for aclk_period/2;
    end process;

  -- Insert values for generic parameters !!
  uut: fir_correlation generic map ( REGISTER_WIDTH             => REGISTER_WIDTH,
                                REGISTER_HALF_WIDTH        => REGISTER_HALF_WIDTH,
                                n_taps                     => n_taps,
                                n_sets_tap                 => n_sets_tap,
                                oversampling               => oversampling,
                                wait_cycles                => wait_cycles,
                                tap_width                  => tap_width,
                                CORRELATION_WIDTH          => CORRELATION_WIDTH,
                                C_S00_AXIS_TDATA_WIDTH     => C_S00_AXIS_TDATA_WIDTH,
                                C_M00_AXIS_TDATA_WIDTH     => C_M00_AXIS_TDATA_WIDTH )
                     port map ( aclk                       => aclk,
                                aresetn                    => aresetn,
                                READ_RESULT                => READ_RESULT,
                                DATA_WINDOW                => DATA_WINDOW,
                                BAND_PASS_FILTER_EN        => BAND_PASS_FILTER_EN,
                                BAND_PASS_FILTER_L_DIVIDER => BAND_PASS_FILTER_L_DIVIDER,
                                GAUSSIAN_FILTER_EN         => GAUSSIAN_FILTER_EN,
                                GAUSSIAN_FILTER_L_DIVIDER  => GAUSSIAN_FILTER_L_DIVIDER,
                                LOW_PASS_FILTER_EN         => LOW_PASS_FILTER_EN,
                                LOW_PASS_FILTER_L_DIVIDER  => LOW_PASS_FILTER_L_DIVIDER,
                                FIR_END                    => FIR_END,
                                fir_taps_axis_tready       => fir_taps_axis_tready,
                                fir_taps_axis_tdata        => fir_taps_axis_tdata,
                                fir_taps_axis_tlast        => fir_taps_axis_tlast,
                                fir_taps_axis_tvalid       => fir_taps_axis_tvalid,
                                signal_input_axis_tready   => signal_input_axis_tready,
                                signal_input_axis_tdata    => signal_input_axis_tdata,
                                signal_input_axis_tlast    => signal_input_axis_tlast,
                                signal_input_axis_tvalid   => signal_input_axis_tvalid,
                                GATES_START                => GATES_START,
                                GATES_END                  => GATES_END,
                                gates_axis_tvalid          => gates_axis_tvalid,
                                gates_axis_tdata           => gates_axis_tdata,
                                gates_axis_tlast           => gates_axis_tlast,
                                gates_axis_tready          => gates_axis_tready,
                                CORRELATION_EN             => CORRELATION_EN,
                                CORRELATION_GATE_START                 => CORRELATION_GATE_START,
                                CORRELATION_GATE_END                   => CORRELATION_GATE_END,
                                correlation_axis_tvalid    => correlation_axis_tvalid,
                                correlation_axis_tdata     => correlation_axis_tdata,
                                correlation_axis_tlast     => correlation_axis_tlast,
                                correlation_axis_tready    => correlation_axis_tready,
                                fir_result_axis_tvalid     => fir_result_axis_tvalid,
                                fir_result_axis_tdata      => fir_result_axis_tdata,
                                fir_result_axis_tlast      => fir_result_axis_tlast,
                                fir_result_axis_tready     => fir_result_axis_tready );

  stimulus: process
  begin
    aresetn <= '0';
    wait for aclk_period*10;
    aresetn <= '1';
    wait for aclk_period*10;
    start_sm <= '1';
    wait for aclk_period*10;
    start_sm <= '0';
    wait;
  end process;
  
  CORRELATION_EN <= corr_on;
  
    process_inst: process(aclk, aresetn)  
        variable my_counter  : integer range 0 to 16383 :=0;
        
        type tapsDataFile is file of integer;
        file taps_data_file: tapsDataFile is in "taps_fir.dat";
        variable taps_fir: integer;   
        
        type signalDataFile is file of integer;
        file signal_data_file: tapsDataFile is in "signal_fir.dat";
        variable signal_fir: integer;                        
    begin 
        if (aresetn = '0') then
            DATA_WINDOW <= (others => '0');
            BAND_PASS_FILTER_EN <= '0';
            BAND_PASS_FILTER_L_DIVIDER <= (others => '0');
            GAUSSIAN_FILTER_EN <= '0';
            GAUSSIAN_FILTER_L_DIVIDER <= (others => '0');
            LOW_PASS_FILTER_EN <= '0';
            LOW_PASS_FILTER_L_DIVIDER <= (others => '0');
            fir_taps_axis_tdata <= (others => '0');
            fir_taps_axis_tvalid <= '0';
            fir_taps_axis_tlast <= '0';
            signal_input_axis_tdata <= (others => '0');
            signal_input_axis_tvalid <= '0';
            signal_input_axis_tlast <= '0';
            gates_axis_tready   <= '0';
            fir_result_axis_tready <= '0';
            GATES_START <= (others => '0');
            GATES_END <= (others => '0');
            CORRELATION_GATE_START <= (others => '0');
            CORRELATION_GATE_END <= (others => '0');
            READ_RESULT <= '0';
            corr_on <= '0';
            correlation_axis_tready <= '0';
            state_sm <= idle;  
        elsif (aclk'event and aclk = '1') then
            case state_sm is  
                when idle =>         
                    DATA_WINDOW <= std_logic_vector(to_unsigned(32, DATA_WINDOW'length));
                    --DATA_WINDOW <= std_logic_vector(to_unsigned(30, DATA_WINDOW'length));
                    --BAND_PASS_FILTER_EN <= '1';
                    BAND_PASS_FILTER_EN <= '1';
                    GAUSSIAN_FILTER_EN  <= '1';
                    LOW_PASS_FILTER_EN  <= '0';
                    BAND_PASS_FILTER_L_DIVIDER  <= std_logic_vector(to_unsigned(1, BAND_PASS_FILTER_L_DIVIDER'length));
                    GAUSSIAN_FILTER_L_DIVIDER   <= std_logic_vector(to_unsigned(1, BAND_PASS_FILTER_L_DIVIDER'length));
                    LOW_PASS_FILTER_L_DIVIDER   <= (others => '0');
                    gates_axis_tready   <= '1';
                    fir_result_axis_tready <= '1';
                    correlation_axis_tready <= '1';
                    --GATES_START <= std_logic_vector(to_unsigned(3, GATES_START'length));
                    GATES_START <= std_logic_vector(to_unsigned(0, GATES_START'length));
                    --GATES_END   <= std_logic_vector(to_unsigned(29, GATES_END'length));
                    GATES_END   <= std_logic_vector(to_unsigned(31, GATES_END'length));
                    corr_on <= '1';
                    --GATE_START <= std_logic_vector(to_unsigned(8, GATE_START'length));
                    CORRELATION_GATE_START <= std_logic_vector(to_unsigned(0, CORRELATION_GATE_START'length));
                    CORRELATION_GATE_END   <= std_logic_vector(to_unsigned(15, CORRELATION_GATE_END'length));
                    if(start_sm = '1') then
                        fir_taps_axis_tdata <= (others => '0');
                        my_counter := 0;
                        state_sm <= load_taps;        
                    end if;
                when load_taps => 
                    if(fir_taps_axis_tready = '1') then
                        if(my_counter <= (n_taps*n_sets_tap)-1) then
                            fir_taps_axis_tvalid <= '1';
                            if not endfile(taps_data_file) then
                                read (taps_data_file, taps_fir);
                                fir_taps_axis_tdata<=std_logic_vector(to_signed(taps_fir, fir_taps_axis_tdata'length));
                            end if;
                            if not endfile(taps_data_file) then
                                read (taps_data_file, taps_fir);
                            end if;
                            if(my_counter >= (n_taps*n_sets_tap)-1) then
                                fir_taps_axis_tlast <= '1';
                            end if;
                            my_counter := my_counter + 1;
                        else
                            fir_taps_axis_tvalid    <= '0';
                            fir_taps_axis_tlast     <= '0';
                            my_counter              := 0;
                            state_sm                <= load_signal;
                        end if;
                    end if;                                                                           
                when load_signal =>
                    if(signal_input_axis_tready = '1') then
                        if(my_counter <= (to_integer(unsigned(DATA_WINDOW))-1)) then
                            signal_input_axis_tvalid <= '1';
                            if not endfile(signal_data_file) then
                                read (signal_data_file, signal_fir);
                                signal_input_axis_tdata<=std_logic_vector(to_signed(signal_fir, signal_input_axis_tdata'length));
                            end if;
                            if not endfile(signal_data_file) then
                                read (signal_data_file, signal_fir);
                            end if;
                            if(my_counter >= (to_integer(unsigned(DATA_WINDOW))-1)) then
                                signal_input_axis_tlast <= '1';
                                my_counter  := 0;
                                state_sm    <= wait_gates;
                            end if;
                            my_counter := my_counter + 1;
                        else
                            signal_input_axis_tvalid    <= '0';
                            signal_input_axis_tlast     <= '0';
                            if(FIR_END = '1') then
                                my_counter  := 0;
                                state_sm    <= wait_gates;
                            end if;             
                        end if;  
                    end if;
                    if(FIR_END = '1') then
                        my_counter  := 0;
                        state_sm    <= wait_gates;
                    end if; 
                when wait_gates =>
                    signal_input_axis_tvalid <= '0';
                    signal_input_axis_tlast <= '0';
                    if(gates_axis_tlast = '1') then
                        if(corr_on = '1') then
                            state_sm    <= wait_corr;
                        else
                            READ_RESULT <= '1';
                            state_sm    <= read_fir_result;
                        end if;    
                    end if;
                when wait_corr =>
                    if(correlation_axis_tlast = '1') then
                        READ_RESULT <= '1';
                        state_sm    <= read_fir_result;
                    end if;
                when read_fir_result => 
                    if(fir_result_axis_tlast = '1') then
                        READ_RESULT <= '0';
                    end if;   
                when others =>
                    null;                   
            end case;                                                                                            
        end if;
   end process; 
   
end;
