clear all;close all;

signal_size = 32;
tap_size = 8;
tap_sets = 3;

signal_fir = zeros(1,signal_size);

taps_fir = zeros(1,tap_size*tap_sets);

for n = 1:signal_size
    signal_fir(n) = n;  
end

file_signal_fir = fopen('signal_fir.dat','w');
fwrite(file_signal_fir,signal_fir,'int64');
fclose(file_signal_fir);

n_aux = 0;
tap_dir = 0;
for n = 1:tap_size*tap_sets
    if(tap_dir == 0)
        if(n_aux >= tap_size/2)
           tap_dir = 1; 
        else
           n_aux = n_aux + 1;
        end
    else
        if(n_aux <= 1)
            tap_dir = 0;
        else
            n_aux = n_aux - 1;
        end
    end
    taps_fir(n) = n_aux;
end

file_taps_fir = fopen('taps_fir.dat','w');
fwrite(file_taps_fir,taps_fir,'int64');
fclose(file_taps_fir);

% FIR oversampling 1

signal_fir_1 = [signal_fir zeros(1,tap_size-1)];
fir_result_1   = zeros(1,signal_size);

for n = 1:signal_size
   %fir_result(n) = signal_fir_x(n:n+tap_size-1) * taps_fir(1:tap_size)';
   fir_result_1(n) = dot(signal_fir_1(n:n+tap_size-1),taps_fir(1:tap_size));
end
fir_result_1 = (fir_result_1/2);
fir_result_1 = floor(fir_result_1);

figure(1);
plot(fir_result_1);

signal_fir_2 = [fir_result_1 zeros(1,tap_size-1)];
fir_result_2   = zeros(1,signal_size);

for n = 1:signal_size
   %fir_result(n) = signal_fir_x(n:n+tap_size-1) * taps_fir(1:tap_size)';
   fir_result_2(n) = dot(signal_fir_2(n:n+tap_size-1),taps_fir(1:tap_size));
end
fir_result_2 = (fir_result_2/2);
fir_result_2 = floor(fir_result_2);
figure(2);
plot(fir_result_2);

signal_fir_3 = [fir_result_2 zeros(1,tap_size-1)];
fir_result_3   = zeros(1,signal_size);

for n = 1:signal_size
   %fir_result(n) = signal_fir_x(n:n+tap_size-1) * taps_fir(1:tap_size)';
   fir_result_3(n) = dot(signal_fir_3(n:n+tap_size-1),taps_fir(1:tap_size));
end
fir_result_3 =(fir_result_3/2);
fir_result_3 =floor(fir_result_3);
figure(3);
plot(fir_result_3);

% CORRELATION

correlation_size = 16;

correlation_result_1 = xcorr(fir_result_2(1:correlation_size));
correlation_result_2 = correlation_result_1(correlation_size:2*correlation_size-1);

for n = 1:correlation_size
    correlation_result_3(n) = fir_result_2(1:correlation_size+1-n)*fir_result_2(n:correlation_size)';
end



