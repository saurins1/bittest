----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 09/27/2017 10:07:23 AM
-- Design Name: 
-- Module Name: fir_module - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity fir_correlation is
	generic (
        REGISTER_WIDTH      : integer    := 32;
        REGISTER_HALF_WIDTH : integer    := 16;
        n_taps              : integer    := 512;
        n_sets_tap          : integer    := 3;
        oversampling        : integer    := 2;
        wait_cycles         : integer    := 4;
        tap_width           : integer    := 16;
        CORRELATION_WIDTH   : integer    := 64;
        C_S00_AXIS_TDATA_WIDTH  : integer    := 16;
        C_M00_AXIS_TDATA_WIDTH  : integer    := 16
    );
    port (
        aclk         : in std_logic;
        aresetn      : in std_logic;
        
        READ_RESULT                 : in std_logic;
        DATA_WINDOW                 : in std_logic_vector(REGISTER_HALF_WIDTH-1 downto 0); 
        
        BAND_PASS_FILTER_EN         : in std_logic;
        BAND_PASS_FILTER_L_DIVIDER  : in std_logic_vector(REGISTER_HALF_WIDTH-1 downto 0);
        GAUSSIAN_FILTER_EN          : in std_logic;
        GAUSSIAN_FILTER_L_DIVIDER   : in std_logic_vector(REGISTER_HALF_WIDTH-1 downto 0);
        LOW_PASS_FILTER_EN          : in std_logic;
        LOW_PASS_FILTER_L_DIVIDER   : in std_logic_vector(REGISTER_HALF_WIDTH-1 downto 0);
        
        FIR_END                     : out std_logic;
                     
        fir_taps_axis_tready        : out std_logic;
        fir_taps_axis_tdata         : in std_logic_vector(C_S00_AXIS_TDATA_WIDTH-1 downto 0);
        fir_taps_axis_tlast         : in std_logic;
        fir_taps_axis_tvalid        : in std_logic;
     
        signal_input_axis_tready    : out std_logic;
        signal_input_axis_tdata     : in std_logic_vector(C_S00_AXIS_TDATA_WIDTH-1 downto 0);
        signal_input_axis_tlast     : in std_logic;
        signal_input_axis_tvalid    : in std_logic;
        
        GATES_START                 : in std_logic_vector(REGISTER_HALF_WIDTH-1 downto 0);
        GATES_END                   : in std_logic_vector(REGISTER_HALF_WIDTH-1 downto 0);
        
        gates_axis_tvalid           : out std_logic;
        gates_axis_tdata            : out std_logic_vector(C_M00_AXIS_TDATA_WIDTH-1 downto 0);
        gates_axis_tlast            : out std_logic;
        gates_axis_tready           : in std_logic;
        
        CORRELATION_EN              : in std_logic;
        CORRELATION_GATE_START      : in std_logic_vector(REGISTER_HALF_WIDTH-1 downto 0);
        CORRELATION_GATE_END        : in std_logic_vector(REGISTER_HALF_WIDTH-1 downto 0);
        
        correlation_axis_tvalid     : out std_logic;
        correlation_axis_tdata      : out std_logic_vector(CORRELATION_WIDTH-1 downto 0);
        correlation_axis_tlast      : out std_logic;
        correlation_axis_tready     : in std_logic;

        fir_result_axis_tvalid      : out std_logic;
        fir_result_axis_tdata       : out std_logic_vector(C_M00_AXIS_TDATA_WIDTH-1 downto 0);
        fir_result_axis_tlast       : out std_logic;
        fir_result_axis_tready      : in std_logic
    );
end fir_correlation;

architecture Behavioral of fir_correlation is

    -- Number of dsps
    constant n_dsp                      : INTEGER range 0 to 16383 := n_taps/oversampling;
    
    -- FIR Window oversampling
    constant window_oversampling        : INTEGER range 0 to 16383 := n_taps + (n_taps/oversampling) - 1;
    
    -- FIR Window oversampling step
    constant window_oversampling_step   : INTEGER range 0 to 16383 := n_dsp;

    signal data_window_int                  : INTEGER range 0 to 65536 :=0;
    
    -- FIR Window oversampling offset
    signal window_oversampling_offset       : UNSIGNED(16 downto 0);
    signal window_oversampling_offset_end   : UNSIGNED(16 downto 0);
    
    -- TAPS registers
    type fir_taps_array is array ( 0 to n_dsp-1 ) of STD_LOGIC_VECTOR(tap_width-1 downto 0);
    signal fir_taps : fir_taps_array;
     
    -- DSP SLICE 48   
    COMPONENT fir_correlation_DSP
      PORT (
        CLK : IN STD_LOGIC;
        SCLR : IN STD_LOGIC;
        A : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
        B : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        P : OUT STD_LOGIC_VECTOR(29 DOWNTO 0)
      );
    END COMPONENT;
    
    -- DSP SCLR 
    signal fir_corr_DSP_SCLR : STD_LOGIC;
    
    -- DSP result array
    type dsp_48_P_array is array ( 0 to n_dsp-1 ) of std_logic_vector(29 downto 0);
    signal dsp_48_P : dsp_48_P_array;
    
    -- ACCUMULATOR
    COMPONENT fir_correlation_ACC
      PORT (
        B : IN STD_LOGIC_VECTOR(29 DOWNTO 0);
        CLK : IN STD_LOGIC;
        BYPASS : IN STD_LOGIC;
        SCLR : IN STD_LOGIC;
        Q : OUT STD_LOGIC_VECTOR(63 DOWNTO 0)
      );
    END COMPONENT;
    
    -- Accumulator Bypass
    signal fir_corr_ACC_BYPASS      : STD_LOGIC_VECTOR(6 downto 0);
    signal fir_corr_ACC_BYPASS_i    : STD_LOGIC_VECTOR(n_dsp-1 downto 0);
    
    -- ACC SCLR 
    signal fir_corr_ACC_SCLR        : STD_LOGIC;
    
    -- ACC result array
    type fir_corr_ACC_Q_array is array ( 0 to n_dsp-1 ) of STD_LOGIC_VECTOR(63 downto 0);
    signal fir_corr_ACC_Q : fir_corr_ACC_Q_array;
    
    -- BRAM stores FIR taps (3 sets)
    COMPONENT fir_correlation_taps_BRAM
      PORT (
        clka : IN STD_LOGIC;
        ena : IN STD_LOGIC;
        wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addra : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
        dina : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        douta : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
        clkb : IN STD_LOGIC;
        enb : IN STD_LOGIC;
        web : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addrb : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
        dinb : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        doutb : OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
      );
    END COMPONENT;
    
    -- FIR taps BRAM port a
    signal fir_taps_BRAM_ena    : STD_LOGIC;
    signal fir_taps_BRAM_wea    : STD_LOGIC_VECTOR(0 DOWNTO 0);
    signal fir_taps_BRAM_addra  : STD_LOGIC_VECTOR(11 DOWNTO 0);
    signal fir_taps_BRAM_dina   : STD_LOGIC_VECTOR(15 DOWNTO 0);
    signal fir_taps_BRAM_douta  : STD_LOGIC_VECTOR(15 DOWNTO 0);
    
    signal fir_taps_axis_tvalid_first   : STD_LOGIC;
    signal fir_taps_axis_tlast_i        : std_logic_vector(1 downto 0);

    -- FIR taps BRAM port b
    signal fir_taps_BRAM_enb    : STD_LOGIC;
    signal fir_taps_BRAM_web    : STD_LOGIC_VECTOR(0 DOWNTO 0);
    signal fir_taps_BRAM_addrb  : STD_LOGIC_VECTOR(11 DOWNTO 0);
    signal fir_taps_BRAM_dinb   : STD_LOGIC_VECTOR(15 DOWNTO 0);
    signal fir_taps_BRAM_doutb  : STD_LOGIC_VECTOR(15 DOWNTO 0);
    
    -- FIR coefficient set select
    signal fir_taps_set  : STD_LOGIC_VECTOR(2 downto 0);
    type fir_taps_offset_array is array ( 0 to n_sets_tap-1 ) of STD_LOGIC_VECTOR(11 downto 0);
    signal fir_taps_offset_aux      : fir_taps_offset_array;
    signal fir_taps_offset_aux_i    : STD_LOGIC_VECTOR(11 downto 0);
    signal fir_taps_offset          : STD_LOGIC_VECTOR(11 downto 0);
    signal fir_l_divider            : INTEGER range 0 to 128;
       
    signal read_fir_taps_first : STD_LOGIC;
   
    -- BRAM stores signal input or FIR result
    COMPONENT fir_correlation_signal_BRAM
      PORT (
        clka : IN STD_LOGIC;
        ena : IN STD_LOGIC;
        wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addra : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        dina : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
        douta : OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
        clkb : IN STD_LOGIC;
        enb : IN STD_LOGIC;
        web : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addrb : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        dinb : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
        doutb : OUT STD_LOGIC_VECTOR(13 DOWNTO 0)
      );
    END COMPONENT;
    
    -- signal BRAM port a
    signal signal_BRAM_ena      : STD_LOGIC;
    signal signal_BRAM_wea      : STD_LOGIC_VECTOR(0 DOWNTO 0);
    signal signal_BRAM_addra    : STD_LOGIC_VECTOR(15 DOWNTO 0);
    signal signal_BRAM_addra_aux: STD_LOGIC_VECTOR(15 DOWNTO 0); 
    signal signal_BRAM_dina     : STD_LOGIC_VECTOR(13 DOWNTO 0);
    signal signal_BRAM_douta    : STD_LOGIC_VECTOR(13 DOWNTO 0);
    
    signal rectified_signal_BRAM_douta    : STD_LOGIC_VECTOR(13 DOWNTO 0);
    
    signal fir_input_axis_tvalid_first  : STD_LOGIC;
    signal signal_input_axis_tlast_i    : std_logic_vector(1 downto 0);
    signal write_fir_result_first       : STD_LOGIC;
    
    -- signal BRAM port b
    signal signal_BRAM_enb      : STD_LOGIC;
    signal signal_BRAM_web      : STD_LOGIC_VECTOR(0 DOWNTO 0);
    signal signal_BRAM_addrb    : STD_LOGIC_VECTOR(16 DOWNTO 0);
    signal signal_BRAM_dinb     : STD_LOGIC_VECTOR(13 DOWNTO 0);
    signal signal_BRAM_doutb    : STD_LOGIC_VECTOR(13 DOWNTO 0);
    
    signal rectified_signal_BRAM_doutb    : STD_LOGIC_VECTOR(13 DOWNTO 0);
    
    -- GENERAL STATE MACHINE
    type states_general_sm is (idle, fir_input, select_fir_tap_set, fir_filtering, provide_gates, check_operation, provide_correlation, provide_fir_result, provide_fir_result_finish); 
    signal state_general_sm : states_general_sm;
    
    signal fir_corr_signal : std_logic_vector(13 downto 0);
    
    signal low_pass_filter_flag    : STD_LOGIC;
    
    -- FIR filtering 
    signal fir_filtering_flag   : STD_LOGIC;
    signal fir_filtering_end    : STD_LOGIC;
    
    -- Provide gates
    signal provide_gates_flag   : STD_LOGIC;
    signal provide_gates_end    : STD_LOGIC;
    
    signal read_gates_first     : STD_LOGIC;
    signal gates_axis_tvalid_i  : STD_LOGIC_VECTOR(1 downto 0);
    signal gates_axis_tlast_i   : STD_LOGIC_VECTOR(1 downto 0);
    
    -- Provide fir result
    signal provide_fir_result_flag  : STD_LOGIC;
    signal provide_fir_result_end   : STD_LOGIC;
    
    signal read_fir_result_first    : STD_LOGIC;
    signal fir_result_axis_tvalid_i : STD_LOGIC_VECTOR(1 downto 0);
    signal fir_result_axis_tlast_i  : STD_LOGIC_VECTOR(1 downto 0);
        
    -- Provide correlation
    signal provide_correlation_flag : STD_LOGIC;
    signal provide_correlation_end  : STD_LOGIC;
    signal provide_correlation_ending  : STD_LOGIC;
    
    type states_write_signal_sm is (idle, write_signal_input, write_fir_result_check, write_fir_result, read_correlation_gate, read_correlation_gate_wait); 
    signal state_write_signal_sm : states_write_signal_sm;
    
    type states_read_fir_signal_sm is (idle, read_fir_filtering_signal, read_gates, read_fir_result, read_correlation_gate, read_correlation_gate_wait); 
    signal state_read_fir_signal_sm : states_read_fir_signal_sm;
    
    -- Correlation
    type states_provide_correlation is (idle, provide_correlation_1, provide_correlation_2, provide_correlation_3, provide_correlation_4); 
    signal state_provide_correlation : states_provide_correlation;
    
    signal correlation_addb_offset_end   : UNSIGNED(16 downto 0);
    
    signal corr_en : STD_LOGIC;
    signal corr_and : STD_LOGIC_VECTOR(13 downto 0);
    
    type corr_and_array is array ( 0 to 1 ) of STD_LOGIC_VECTOR(13 downto 0);
    signal corr_and_i : corr_and_array;
    
    signal corr_out_flag : STD_LOGIC;
    signal corr_out_flag_i : STD_LOGIC_VECTOR(7 downto 0);
    
begin

    fir_taps_offset_aux_i   <= std_logic_vector(to_unsigned(n_taps,fir_taps_offset_aux_i'length));
    fir_taps_offset_aux(0)  <= (others => '0');
    fir_taps_offset_aux(1)  <= fir_taps_offset_aux_i;
    fir_taps_offset_aux(2)  <= fir_taps_offset_aux_i(10 downto 0) & "0";
    
    data_window_int <= to_integer(unsigned(DATA_WINDOW));
    
    -- GENERAL STATE MACHINE
    general_sm: process(aclk)
        variable counter_aux  : integer range 0 to 65536 :=0; 
        variable counter_wait  : integer range 0 to 65536 :=0; 
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then  
                -- FIR taps
                fir_taps_set            <= (others => '0');
                fir_taps_offset         <= (others => '0');
                -- Fir filtering
                fir_filtering_flag      <= '0';
                fir_corr_DSP_SCLR       <= '1';
                fir_corr_ACC_SCLR       <= '1';
                low_pass_filter_flag    <= '0';
                -- Gates    
                provide_gates_flag      <= '0';
                -- FIR result
                provide_fir_result_flag <= '0';
                signal_input_axis_tready<= '0';
                fir_taps_axis_tready    <= '0';
                FIR_END                 <= '0';
                -- Correlation
                provide_correlation_flag<= '0';
                corr_en                 <= '0';
                -- State
                state_general_sm        <= idle;
            else
                fir_corr_DSP_SCLR       <= '0';
                fir_corr_ACC_SCLR       <= '0';
                case state_general_sm is        
                    when idle =>
                        -- waiting for signal input
                        signal_input_axis_tready <= '1';
                        fir_taps_axis_tready     <= '1';
                        FIR_END                  <= '0';
                        fir_corr_DSP_SCLR        <= '0';
                        fir_corr_ACC_SCLR        <= '0';
                        if(signal_input_axis_tvalid = '1') then
                            fir_taps_set        <= (others => '1');
                            corr_en             <= '1';
                            state_general_sm    <= fir_input; 
                        end if;
                    when fir_input =>
                        -- waiting signal input end
                        if(signal_input_axis_tlast_i(1) = '1') then
                            state_general_sm <= select_fir_tap_set;                     
                        end if;
                    when select_fir_tap_set => 
                        -- select FIR coefficients set
                        signal_input_axis_tready <= '0';
                        fir_taps_axis_tready     <= '0';  
                        counter_wait := 0;                            
                        if(BAND_PASS_FILTER_EN = '1' and fir_taps_set(0) = '1') then
                            fir_taps_set(0)     <= '0';
                            fir_taps_offset     <= fir_taps_offset_aux(0);
                            fir_l_divider       <= to_integer(unsigned(BAND_PASS_FILTER_L_DIVIDER));
                            state_general_sm    <= fir_filtering;
                        elsif(GAUSSIAN_FILTER_EN = '1' and fir_taps_set(1) = '1') then
                            fir_taps_set(1)     <= '0';
                            fir_taps_offset     <= fir_taps_offset_aux(1);  
                            fir_l_divider       <= to_integer(unsigned(GAUSSIAN_FILTER_L_DIVIDER));
                            state_general_sm    <= fir_filtering;
                        elsif(LOW_PASS_FILTER_EN = '1' and fir_taps_set(2) = '1') then
                            fir_taps_set(2)     <= '0';
                            fir_taps_offset     <= fir_taps_offset_aux(2);
                            fir_l_divider       <= to_integer(unsigned(LOW_PASS_FILTER_L_DIVIDER));
                            low_pass_filter_flag<= '1';
                            state_general_sm    <= fir_filtering;
                        else
                            state_general_sm    <= provide_gates; 
                        end if;
                    when fir_filtering =>
                        -- FIR filtering
                        if(counter_wait >= wait_cycles) then
                            if(fir_filtering_end = '1') then
                                fir_filtering_flag  <= '0';
                                low_pass_filter_flag<= '0';
                                state_general_sm    <= select_fir_tap_set;
                            else
                                fir_filtering_flag  <= '1';
                            end if; 
                        else
                            counter_wait := counter_wait + 1;
                        end if;                       
                    when provide_gates =>
                        -- Provide gates
                        if(provide_gates_end = '1') then
                            FIR_END <= '1';
                            provide_gates_flag  <= '0';
                            state_general_sm    <= check_operation;
                        else
                            provide_gates_flag  <= '1';
                         end if;
                    when check_operation =>
                        -- Wait for read result
                        if(CORRELATION_EN = '1' and corr_en = '1') then
                            state_general_sm    <= provide_correlation;
                            corr_en             <= '0';         
                            counter_aux         := 0;
                            counter_wait        := 0;
                        else 
                            if(READ_RESULT = '1') then
                                state_general_sm  <= provide_fir_result;
                            end if;
                        end if;
                    when provide_correlation =>
                        -- Provide correlation
                        if(provide_correlation_end = '1') then
                            if(counter_aux >= 16) then
                                provide_correlation_flag <= '0';
                                state_general_sm         <= check_operation;
                            else
                                counter_aux := counter_aux + 1;
                            end if;
                        else
                            provide_correlation_flag <= '1';
                        end if;
                    when provide_fir_result =>
                        -- Provide FIR result
                        if(provide_fir_result_end = '1') then
                            provide_fir_result_flag <= '0';
                            FIR_END                 <= '0';
                            state_general_sm        <= provide_fir_result_finish;
                        else
                            provide_fir_result_flag <= '1';
                        end if;
                    when provide_fir_result_finish =>
                        if(READ_RESULT = '0') then
                            state_general_sm        <= idle;
                        end if;
                    when others =>
                        null;
                end case;
             end if; 
         end if;
    end process;
     
    -- Write signal input
    -- Write FIR result
    -- Read correlation gate
    signal_BRAM_porta: process(aclk) 
        variable counter_aux  : integer range 0 to 65536 :=0;
        variable counter_result  : integer range 0 to 65536 :=0;
        variable counter_wait      : integer range 0 to 8192 :=0;
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                signal_BRAM_ena   <= '1';
                signal_BRAM_wea   <= (others => '0');
                signal_BRAM_addra <= (others => '0');
                signal_BRAM_addra_aux <= (others => '0');
                signal_BRAM_dina  <= (others => '0');
                signal_input_axis_tlast_i <= (others => '0');
                fir_input_axis_tvalid_first <= '1';
                write_fir_result_first      <= '1';
                -- State
                state_write_signal_sm       <= idle;
            else     
                signal_input_axis_tlast_i(0) <= signal_input_axis_tlast;
                signal_input_axis_tlast_i(1) <= signal_input_axis_tlast_i(0);
                case state_write_signal_sm is        
                    when idle =>
                        signal_BRAM_wea <= (others => '0');
                        if(fir_filtering_flag = '1') then 
                            -- flank down active
                            if(fir_corr_ACC_BYPASS(5) = '0' and fir_corr_ACC_BYPASS(6) = '1') then
                                write_fir_result_first  <= '1';
                                counter_result          := 0;
                                state_write_signal_sm   <= write_fir_result_check;
                            end if;    
                        elsif(signal_input_axis_tvalid = '1') then
                            signal_BRAM_wea   <= (others => '1');
                            signal_BRAM_dina  <= signal_input_axis_tdata(13 downto 0); 
                            signal_BRAM_addra <= (others => '0');
                            state_write_signal_sm <= write_signal_input;
                        elsif(provide_correlation_flag = '1') then
                            signal_BRAM_addra           <= CORRELATION_GATE_START;
                            signal_BRAM_addra_aux       <= CORRELATION_GATE_START;
                            state_write_signal_sm       <= read_correlation_gate;
                        end if;
                    when write_signal_input =>
                        if(signal_input_axis_tvalid = '1') then
                            signal_BRAM_dina  <= signal_input_axis_tdata(13 downto 0); 
                            signal_BRAM_addra <= std_logic_vector(unsigned(signal_BRAM_addra)+1);
                        else
                            if(signal_input_axis_tlast_i(1) = '1') then
                                state_write_signal_sm   <= idle;
                            end if;
                        end if;
                    when write_fir_result_check =>
                        -- flank up active
                        if(fir_corr_ACC_BYPASS(5) = '1' and fir_corr_ACC_BYPASS(6) = '0') then  
                            state_write_signal_sm <= write_fir_result;
                            counter_aux := 0;
                        end if;   
                    when write_fir_result =>
                        signal_BRAM_wea   <= (others => '1');
                        if(counter_result >= data_window_int) then
                            if(fir_filtering_flag = '0') then      
                                state_write_signal_sm   <= idle; 
                            end if;   
                        else
                            if(write_fir_result_first = '1') then
                                signal_BRAM_addra <= (others => '0');
                                write_fir_result_first  <= '0';
                            else
                                signal_BRAM_addra <= std_logic_vector(unsigned(signal_BRAM_addra)+1);
                            end if; 
                            signal_BRAM_dina  <= fir_corr_ACC_Q(counter_aux)(C_S00_AXIS_TDATA_WIDTH + fir_l_divider -3 downto fir_l_divider);          
                            if(counter_aux >= n_dsp-1) then
                                if(oversampling > 1) then
                                    state_write_signal_sm <= write_fir_result_check;
                                else
                                    counter_aux := 0;
                                end if;
                            else             
                                counter_aux := counter_aux + 1;       
                            end if;
                            counter_result := counter_result + 1;
                        end if;
                    when read_correlation_gate =>
                        if(provide_correlation_flag = '1') then
                            if(correlation_axis_tready = '1') then
                                if(unsigned(signal_BRAM_addra) >= (unsigned(CORRELATION_GATE_END)) and provide_correlation_ending = '0') then
                                    counter_wait := 0;
                                    state_write_signal_sm <= read_correlation_gate_wait;
                                    signal_BRAM_addra <= std_logic_vector(unsigned(signal_BRAM_addra_aux) + n_dsp);
                                    signal_BRAM_addra_aux <= std_logic_vector(unsigned(signal_BRAM_addra_aux) + n_dsp);
                                else
                                    if(unsigned(signal_BRAM_addra) <= (unsigned(CORRELATION_GATE_END))) then
                                        signal_BRAM_addra <= std_logic_vector(unsigned(signal_BRAM_addra)+1);
                                    end if;
                                end if;
                            end if;
                        else
                            state_write_signal_sm <= idle;
                        end if;
                    when read_correlation_gate_wait =>
                        if(counter_wait >= n_dsp-1) then
                            state_write_signal_sm <= read_correlation_gate;
                        else
                            counter_wait:= counter_wait + 1;
                        end if;
                                                
                    when others => 
                        null;
                end case;                                 
            end if;
        end if;
    end process;
    
    -- Correlation result
    correlation_output_process: process(aclk) 
        variable counter_aux  : integer range 0 to 65536 :=0;
        variable counter_result  : integer range 0 to 65536 :=0;
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                correlation_axis_tvalid <= '0';
                correlation_axis_tdata  <= (others => '0');
                correlation_axis_tlast  <= '0';
                state_provide_correlation <= idle;
            else    
                case state_provide_correlation is
                    when idle =>
                        if(provide_correlation_flag = '1') then
                            if(corr_out_flag_i(4) = '1') then
                                counter_result              := 0;
                                counter_aux                 := 0;
                                state_provide_correlation   <= provide_correlation_2;
                            end if;
                        end if;
                    when provide_correlation_1 =>
                        if(corr_out_flag_i(4) = '1') then 
                            state_provide_correlation <= provide_correlation_2;
                            counter_aux := 0;
                        end if;
                    when provide_correlation_2 =>
                        if(counter_result <= (unsigned(CORRELATION_GATE_END)-unsigned(CORRELATION_GATE_START))) then
                            if(counter_aux >= n_dsp) then
                                correlation_axis_tvalid <= '0';
                                if(counter_result = (unsigned(CORRELATION_GATE_END)-unsigned(CORRELATION_GATE_START))) then
                                --if(counter_result >= (unsigned(GATE_END)-unsigned(GATE_START)-1)) then
                                    state_provide_correlation <= provide_correlation_3;
                                else
                                    correlation_axis_tvalid <= '0';
                                    state_provide_correlation <= provide_correlation_1;
                                end if;
                            else
                                correlation_axis_tvalid <= '1';
                                correlation_axis_tdata  <= fir_corr_ACC_Q(counter_aux)(CORRELATION_WIDTH-1 downto 0);
                                if(counter_result = (unsigned(CORRELATION_GATE_END)-unsigned(CORRELATION_GATE_START))) then
                                    correlation_axis_tlast <= '1';
                                end if;
                                counter_result := counter_result + 1; 
                                counter_aux := counter_aux + 1; 
                            end if;
                        else
                            correlation_axis_tvalid <= '0';
                            correlation_axis_tlast <= '0';
                            if(provide_correlation_flag = '0') then      
                                state_provide_correlation   <= idle; 
                            end if; 
                        end if;
                    when provide_correlation_3 =>
                        correlation_axis_tvalid <= '1';
                        correlation_axis_tdata  <= fir_corr_ACC_Q(0)(CORRELATION_WIDTH-1 downto 0);
                        correlation_axis_tlast <= '1';
                        state_provide_correlation <= provide_correlation_4;
                    when provide_correlation_4 =>
                        correlation_axis_tvalid <= '0';
                        correlation_axis_tlast <= '0';
                        if(provide_correlation_flag = '0') then      
                            state_provide_correlation   <= idle; 
                        end if; 
                    when others =>
                        null;
                end case;
            end if;
        end if;
    end process; 
    
    gates_axis_tdata <= std_logic_vector(resize(signed(signal_BRAM_doutb), gates_axis_tdata'length));
    fir_result_axis_tdata <= std_logic_vector(resize(signed(signal_BRAM_doutb), fir_result_axis_tdata'length));
  
    -- Read signal to filter
    -- Read gates
    -- Read FIR result
    -- Read correlation gate
    signal_BRAM_portb: process(aclk) 
        variable bypass_counter_1  : integer range 0 to 65535 :=0;
        variable bypass_counter_2  : integer range 0 to 65535 :=0;
        variable counter_wait      : integer range 0 to 8192 :=0;
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                signal_BRAM_enb     <= '1';
                signal_BRAM_web     <= (others => '0');
                signal_BRAM_addrb   <= (others => '0');
                signal_BRAM_dinb   <= (others => '0');
                -- FIR filtering
                window_oversampling_offset <= (others => '0');
                window_oversampling_offset_end <= (others => '0');
                fir_filtering_end   <= '0';
                -- Gates
                provide_gates_end   <= '0';
                read_gates_first    <= '1';
                gates_axis_tvalid_i <= (others => '0');
                gates_axis_tlast_i  <= (others => '0');
                gates_axis_tvalid   <= '0';
                gates_axis_tlast    <= '0';
                -- FIR result
                provide_fir_result_end <= '0';
                read_fir_result_first <= '1';
                fir_result_axis_tvalid_i <= (others => '0');
                fir_result_axis_tlast_i  <= (others => '0');
                fir_result_axis_tvalid   <= '0';
                fir_result_axis_tlast    <= '0';
                -- BYPASS
                fir_corr_ACC_BYPASS <= (others => '0');
                -- Correlation
                provide_correlation_end     <= '0';
                provide_correlation_ending  <= '0';   
                correlation_addb_offset_end <= (others => '0');
                corr_and                    <= (others => '0');
                corr_and_i(0)               <= (others => '0');
                corr_and_i(1)               <= (others => '0');
                corr_out_flag               <= '0';
                corr_out_flag_i             <= (others => '0');
                -- State    
                state_read_fir_signal_sm <= idle;
            else
                -- GATES
                gates_axis_tvalid_i(0)      <= gates_axis_tvalid_i(1);
                gates_axis_tvalid           <= gates_axis_tvalid_i(0);
                gates_axis_tlast_i(0)       <= gates_axis_tlast_i(1);
                gates_axis_tlast            <= gates_axis_tlast_i(0);
                -- FIR RESULT
                fir_result_axis_tvalid_i(0) <= fir_result_axis_tvalid_i(1);
                fir_result_axis_tvalid      <= fir_result_axis_tvalid_i(0);
                fir_result_axis_tlast_i(0)  <= fir_result_axis_tlast_i(1);
                fir_result_axis_tlast       <= fir_result_axis_tlast_i(0);
                -- BYPASS
                fir_corr_ACC_BYPASS(1) <= fir_corr_ACC_BYPASS(0);
                fir_corr_ACC_BYPASS(2) <= fir_corr_ACC_BYPASS(1);
                fir_corr_ACC_BYPASS(3) <= fir_corr_ACC_BYPASS(2);
                fir_corr_ACC_BYPASS(4) <= fir_corr_ACC_BYPASS(3);
                fir_corr_ACC_BYPASS(5) <= fir_corr_ACC_BYPASS(4);
                fir_corr_ACC_BYPASS(6) <= fir_corr_ACC_BYPASS(5);
                --Correlation
                corr_and_i(0) <= corr_and;
                corr_and_i(1) <= corr_and_i(0);
                corr_out_flag_i(0) <= corr_out_flag;
                corr_out_flag_i(1) <= corr_out_flag_i(0);
                corr_out_flag_i(2) <= corr_out_flag_i(1);
                corr_out_flag_i(3) <= corr_out_flag_i(2);
                corr_out_flag_i(4) <= corr_out_flag_i(3);
                corr_out_flag_i(5) <= corr_out_flag_i(4);
                -- State machine
                case state_read_fir_signal_sm is 
                    when idle =>
                        fir_corr_ACC_BYPASS(0)  <= '1';
                        if(fir_filtering_flag = '1') then
                            signal_BRAM_addrb <= (others => '0');
                            window_oversampling_offset <= (others => '0');
                            window_oversampling_offset_end <= to_unsigned(window_oversampling-1,window_oversampling_offset_end'length);
                            bypass_counter_1 := 0;
                            bypass_counter_2 := 0;
                            state_read_fir_signal_sm <= read_fir_filtering_signal;
                        elsif(provide_gates_flag = '1') then
                            signal_BRAM_addrb <= (others => '0');
                            state_read_fir_signal_sm <= read_gates;
                        elsif(provide_correlation_flag = '1') then
                            corr_and                    <= (others => '1');
                            signal_BRAM_addrb           <= std_logic_vector(resize(unsigned(CORRELATION_GATE_START), signal_BRAM_addrb'length));
                            correlation_addb_offset_end <= resize(unsigned(CORRELATION_GATE_END), signal_BRAM_addrb'length);
                            state_read_fir_signal_sm <= read_correlation_gate;
                        elsif(provide_fir_result_flag = '1') then  
                            signal_BRAM_addrb <= (others => '0');
                            state_read_fir_signal_sm <= read_fir_result;    
                        end if;
                    when read_fir_filtering_signal =>
                        if(fir_filtering_flag = '1') then
                            if(oversampling = 1) then
                                if(unsigned(signal_BRAM_addrb) >= (data_window_int+n_taps-2)) then
                                    fir_filtering_end <= '1';
                                else
                                    signal_BRAM_addrb <= std_logic_vector(unsigned(signal_BRAM_addrb)+1);
                                end if;
                                if(bypass_counter_1 >= n_dsp-1) then
                                    fir_corr_ACC_BYPASS(0)   <= '1';
                                    bypass_counter_1 := 0;
                                else
                                    fir_corr_ACC_BYPASS(0)   <= '0';
                                    bypass_counter_1 := bypass_counter_1 + 1;
                                end if;
                            else
                                if(unsigned(signal_BRAM_addrb) >= (data_window_int+n_taps-2)) then
                                    fir_filtering_end <= '1';
                                else
                                    if(unsigned(signal_BRAM_addrb) >= window_oversampling_offset_end) then
                                        window_oversampling_offset <= window_oversampling_offset + window_oversampling_step;
                                        signal_BRAM_addrb <= std_logic_vector(window_oversampling_offset + window_oversampling_step);
                                        window_oversampling_offset_end <= window_oversampling_offset + window_oversampling_step + window_oversampling - 1;
                                    else
                                        signal_BRAM_addrb <= std_logic_vector(unsigned(signal_BRAM_addrb)+1);
                                    end if; 
                                end if; 
                                if(bypass_counter_1 >= n_taps-1) then
                                    fir_corr_ACC_BYPASS(0)   <= '1';
                                    if(bypass_counter_2 >= window_oversampling_step-1) then
                                        bypass_counter_1 := 0;
                                        bypass_counter_2 := 0;
                                    else
                                        bypass_counter_2 := bypass_counter_2 + 1;
                                    end if;
                                else
                                    fir_corr_ACC_BYPASS(0)   <= '0';
                                    bypass_counter_1 := bypass_counter_1 + 1;
                                end if;                   
                            end if;
                        else
                            fir_filtering_end <= '0';
                            state_read_fir_signal_sm <= idle;
                        end if;
                    when read_gates =>
                        if(provide_gates_flag = '1') then
                            if(gates_axis_tready = '1') then
                                if(unsigned(signal_BRAM_addrb) >= unsigned(GATES_END)) then
                                    gates_axis_tvalid_i(1)  <= '0';
                                    gates_axis_tlast_i(1)   <= '0';
                                    provide_gates_end       <= '1';
                                else
                                    gates_axis_tvalid_i(1) <= '1';
                                    if(read_gates_first = '1') then
                                        read_gates_first  <= '0';
                                        signal_BRAM_addrb <= std_logic_vector(resize(unsigned(GATES_START), signal_BRAM_addrb'length));
                                    else
                                        signal_BRAM_addrb <= std_logic_vector(unsigned(signal_BRAM_addrb)+1); 
                                    end if;
                                    if(unsigned(signal_BRAM_addrb) >= (unsigned(GATES_END)-1)) then
                                        gates_axis_tlast_i(1) <= '1';
                                    end if;
                                end if;
                            end if;  
                        else
                            provide_gates_end <= '0';
                            read_gates_first  <= '1';
                            state_read_fir_signal_sm <= idle;
                        end if;
                    when read_fir_result =>
                        if(provide_fir_result_flag = '1') then
                            if(fir_result_axis_tready = '1') then
                                if(unsigned(signal_BRAM_addrb) >= (data_window_int-1)) then
                                    fir_result_axis_tvalid_i(1)  <= '0';
                                    fir_result_axis_tlast_i(1)   <= '0';
                                    provide_fir_result_end  <= '1';
                                else
                                    fir_result_axis_tvalid_i(1) <= '1';
                                    if(read_fir_result_first = '1') then
                                        read_fir_result_first <= '0';
                                        signal_BRAM_addrb <= (others => '0');
                                    else
                                        signal_BRAM_addrb <= std_logic_vector(unsigned(signal_BRAM_addrb)+1); 
                                    end if;
                                    if(unsigned(signal_BRAM_addrb) >= (data_window_int-2)) then
                                        fir_result_axis_tlast_i(1) <= '1';
                                    end if;
                                end if;
                            end if;  
                        else
                            provide_fir_result_end <= '0';
                            read_fir_result_first  <= '1';
                            state_read_fir_signal_sm <= idle;
                        end if;
                    when read_correlation_gate =>
                        if(provide_correlation_flag = '1') then
                            if(correlation_axis_tready = '1') then
                                if(unsigned(signal_BRAM_addrb) >= correlation_addb_offset_end and provide_correlation_ending = '0') then
                                    corr_and <= (others => '0');
                                    signal_BRAM_addrb <= std_logic_vector(resize(unsigned(CORRELATION_GATE_START), signal_BRAM_addrb'length));
                                    if(correlation_addb_offset_end = (unsigned(CORRELATION_GATE_START) + n_dsp-1)) then
                                        provide_correlation_ending <= '1';
                                        correlation_addb_offset_end <= resize(unsigned(CORRELATION_GATE_START), correlation_addb_offset_end'length);
                                    elsif(correlation_addb_offset_end > (unsigned(CORRELATION_GATE_START) + n_dsp-1)) then
                                        correlation_addb_offset_end <= correlation_addb_offset_end - n_dsp;
                                    else
                                        correlation_addb_offset_end <= resize(unsigned(CORRELATION_GATE_START), correlation_addb_offset_end'length);
                                        provide_correlation_ending <= '1';
                                    end if;
                                    --fir_corr_ACC_BYPASS(0)   <= '1';
                                    counter_wait    := 0;
                                    corr_out_flag   <= '1';
                                    state_read_fir_signal_sm <= read_correlation_gate_wait;
                                else
                                    corr_and <= (others => '1');
                                    if(unsigned(signal_BRAM_addrb) >= correlation_addb_offset_end) then
                                        fir_corr_ACC_BYPASS(0)   <= '1';
                                        provide_correlation_end <= '1';     
                                    else
                                        fir_corr_ACC_BYPASS(0)   <= '0';
                                        signal_BRAM_addrb <= std_logic_vector(unsigned(signal_BRAM_addrb)+1);
                                    end if;
                                end if;
                            end if;
                        else
                            provide_correlation_end <= '0';
                            state_read_fir_signal_sm <= idle;
                        end if; 
                    when read_correlation_gate_wait =>
                        corr_out_flag   <= '0';
                        if(counter_wait >= n_dsp-1) then
                            fir_corr_ACC_BYPASS(0)   <= '1';
                            corr_and <= (others => '1');
                            state_read_fir_signal_sm <= read_correlation_gate;
                        else
                            counter_wait := counter_wait + 1;
                        end if;                
                    when others => 
                        null;
                end case;     
            end if;
        end if;
    end process;
       
    -- Update TAPS
    write_taps_BRAM: process(aclk) 
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                fir_taps_BRAM_ena   <= '1';
                fir_taps_BRAM_wea   <= (others => '0');
                fir_taps_BRAM_addra <= (others => '0');
                fir_taps_BRAM_dina  <= (others => '0');
                fir_taps_axis_tlast_i <= (others => '0');
                fir_taps_axis_tvalid_first <= '1';
            else
                fir_taps_axis_tlast_i(0) <= fir_taps_axis_tlast;
                fir_taps_axis_tlast_i(1) <= fir_taps_axis_tlast_i(0);
                if(fir_taps_axis_tvalid = '1') then
                    fir_taps_BRAM_wea   <= (others => '1');
                    fir_taps_BRAM_dina  <= fir_taps_axis_tdata;                   
                    if(fir_taps_axis_tvalid_first = '1') then
                        fir_taps_BRAM_addra <= (others => '0');
                        fir_taps_axis_tvalid_first <= '0';
                    else
                        fir_taps_BRAM_addra <= std_logic_vector(unsigned(fir_taps_BRAM_addra)+1);
                    end if;
                else
                    if(fir_taps_axis_tlast_i(0) = '0' and fir_taps_axis_tlast_i(1) = '1') then
                        fir_taps_BRAM_wea   <= (others => '0');
                        fir_taps_axis_tvalid_first <= '1';
                    end if;
                end if;
            end if;
        end if;
    end process;
    
    -- Read TAPs to filtering
    read_taps_BRAM: process(aclk) 
        variable wait_counter  : integer range 0 to 16383 :=0;
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                fir_taps_BRAM_enb   <= '1';
                fir_taps_BRAM_web   <= (others => '0');
                fir_taps_BRAM_addrb <= (others => '0');
                fir_taps_BRAM_dinb  <= (others => '0');
                read_fir_taps_first <= '1';
            else
                if(fir_filtering_flag = '1') then
                    if(read_fir_taps_first = '1') then
                        fir_taps_BRAM_addrb <= fir_taps_offset;
                        read_fir_taps_first <= '0';       
                        wait_counter := 0;
                    else      
                        if(unsigned(fir_taps_BRAM_addrb) >= (unsigned(fir_taps_offset)+(n_taps-1))) then
                            if(oversampling = 1) then
                                fir_taps_BRAM_addrb <= fir_taps_offset;
                            else
                                if(wait_counter >= window_oversampling_step-1) then
                                    fir_taps_BRAM_addrb <= fir_taps_offset;  
                                    wait_counter := 0;   
                                else
                                    wait_counter := wait_counter + 1;
                                end if; 
                            end if;
                        else
                            fir_taps_BRAM_addrb <= std_logic_vector(unsigned(fir_taps_BRAM_addrb)+1);
                        end if;
                    end if;
                else
                    read_fir_taps_first <= '1';
                end if;
            end if;
        end if;
    end process;
   
    --fir_taps(0) <= fir_taps_BRAM_doutb;
    fir_taps(0) <= std_logic_vector(resize(signed(signal_BRAM_doutb), fir_taps(0)'length)) when state_general_sm = provide_correlation  else
                   std_logic_vector(resize(signed(rectified_signal_BRAM_douta), fir_taps(0)'length)) when low_pass_filter_flag = '1' else
                   fir_taps_BRAM_doutb;
                        
    -- select fir corr
    --fir_corr_signal <= signal_BRAM_doutb;
    fir_corr_signal <= (signal_BRAM_douta and corr_and_i(1)) when state_general_sm = provide_correlation else
                       rectified_signal_BRAM_doutb when low_pass_filter_flag = '1' else
                       signal_BRAM_doutb;
                
    -- Rectify signal for envelope
    rectified_signal_BRAM_doutb <= std_logic_vector(-signed(signal_BRAM_doutb)) when signal_BRAM_doutb(13) = '1' else
                                   signal_BRAM_doutb;
                                   
    rectified_signal_BRAM_douta <= std_logic_vector(-signed(signal_BRAM_douta)) when signal_BRAM_douta(13) = '1' else
                                   signal_BRAM_douta;
                    
    -- Delay taps     
    fir_taps_process : for i in 1 to n_dsp-1 generate
        process(aclk) 
        begin
           if (rising_edge(aclk)) then
                if (aresetn = '0') then
                    fir_taps(i) <= (others => '0');
                else
                    fir_taps(i) <= fir_taps(i-1);
                end if;
           end if;
        end process;
    end generate;
    
    -- DSP mult
    fir_correlation_DSPs: for i in 0 to n_dsp-1 generate
        fir_correlation_DSP_inst: fir_correlation_DSP
            port map (
            CLK => aclk,  
            SCLR => fir_corr_DSP_SCLR,
            A   => fir_corr_signal,
            B   => fir_taps(i),     
            P   => dsp_48_P(i)
    );
    end generate;
    
    fir_corr_ACC_BYPASS_i(0) <= fir_corr_ACC_BYPASS(6);
    
    -- Delay ACC BYPASS
    fir_corr_ACC_BYPASS_process : for i in 1 to n_dsp-1 generate
        process(aclk) 
        begin
           if (rising_edge(aclk)) then
                if (aresetn = '0') then
                    fir_corr_ACC_BYPASS_i(i) <= '0';
                else
                    fir_corr_ACC_BYPASS_i(i) <= fir_corr_ACC_BYPASS_i(i-1);    
                end if;
           end if;
        end process;
    end generate;
    
    --Accumulator
    fir_correlation_ACCs: for i in 0 to n_dsp-1 generate
        fir_correlation_ACC_inst: fir_correlation_ACC
            port map (
            B   => dsp_48_P(i), 
            CLK => aclk,  
            BYPASS => fir_corr_ACC_BYPASS_i(i),
            SCLR => fir_corr_ACC_SCLR,   
            Q   => fir_corr_ACC_Q(i)
    );
    end generate;
                   
    -- FIR TAPS BRAM
    fir_correlation_taps_BRAM_inst : fir_correlation_taps_BRAM
        port map (
            clka => aclk,
            ena => fir_taps_BRAM_ena,
            wea => fir_taps_BRAM_wea,
            addra => fir_taps_BRAM_addra,
            dina => fir_taps_BRAM_dina,
            douta => fir_taps_BRAM_douta,
            clkb => aclk,
            enb => fir_taps_BRAM_enb,
            web => fir_taps_BRAM_web,
            addrb => fir_taps_BRAM_addrb,
            dinb => fir_taps_BRAM_dinb,
            doutb => fir_taps_BRAM_doutb
        );
    
    -- FIR SIGNAL BRAM
    fir_correlation_signal_BRAM_inst : fir_correlation_signal_BRAM
        port map (
            clka => aclk,
            ena => signal_BRAM_ena,
            wea => signal_BRAM_wea,
            addra => signal_BRAM_addra,
            dina => signal_BRAM_dina,
            douta => signal_BRAM_douta,
            clkb => aclk,
            enb => signal_BRAM_enb,
            web => signal_BRAM_web,
            addrb => signal_BRAM_addrb(15 downto 0),
            dinb => signal_BRAM_dinb,
            doutb => signal_BRAM_doutb
        );

end Behavioral;
