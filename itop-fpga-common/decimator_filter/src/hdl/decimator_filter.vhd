----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 09/26/2018 10:07:23 AM
-- Design Name: 
-- Module Name: decimator filter- Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity decimator_filter is
	generic (
        REGISTER_W32        : INTEGER := 32;
        REGISTER_W16        : INTEGER := 16;
        REGISTER_W14        : INTEGER := 14;
        BRAM_ADDR_WIDTH     : INTEGER := 13;
        BRAM_DATA_WIDTH     : INTEGER := 16;
		WAIT_CYCLES        	: INTEGER := 32;
		N_INPUT_DELAY       : INTEGER := 8;
		N_OUTPUT_DELAY      : INTEGER := 4;
		DEC_TYPE_NONE    	: INTEGER := 0;
        DEC_TYPE_SIMPLE     : INTEGER := 1;
        DEC_TYPE_MINMAX     : INTEGER := 2;
		DEC_TYPE_ALOK     	: INTEGER := 3
	);
	port (
	    -- Sync
	    aclk         : in STD_LOGIC;
        aresetn      : in STD_LOGIC;
        
	    -- Control
        NO_READ_DEC_RESULT  : in STD_LOGIC;
        READ_DEC_RESULT     : in STD_LOGIC;
        DEC_END             : out STD_LOGIC;
        DEC_EN              : out STD_LOGIC;
	    
	    DEC_TYPE	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);    
	    DEC_LEVEL	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	    DEC_WINDOW	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	    DATA_WINDOW	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
	    DEC_TYPE_RESULT	    : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);    
	    DEC_LEVEL_RESULT	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	    DEC_WINDOW_RESULT	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        DEC_WINDOW_HALF_RESULT	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);	
        	
		-- Signal input
		SIGNAL_INPUT_AXIS_TREADY  : out STD_LOGIC;
		SIGNAL_INPUT_AXIS_TDATA	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		SIGNAL_INPUT_AXIS_TMAX	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		SIGNAL_INPUT_AXIS_TLAST	  : in STD_LOGIC;
		SIGNAL_INPUT_AXIS_TVALID  : in STD_LOGIC;

		-- Decimation result
		DEC_RESULT_AXIS_TVALID    : out STD_LOGIC;
		DEC_RESULT_AXIS_TDATA	  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		DEC_RESULT_AXIS_TLAST	  : out STD_LOGIC;
		DEC_RESULT_AXIS_TREADY    : in STD_LOGIC
	);
end decimator_filter;

architecture arch_imp of decimator_filter is

    signal signal_input_axis_tready_i : STD_LOGIC;
    
    signal dec_type_result_i	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal dec_level_result_i   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal dec_window_result_i  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    
    COMPONENT decimator_filter_BRAM
      PORT (
        clka : IN STD_LOGIC;
        ena : IN STD_LOGIC;
        wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addra : IN STD_LOGIC_VECTOR(BRAM_ADDR_WIDTH-1 DOWNTO 0);
        dina : IN STD_LOGIC_VECTOR(BRAM_DATA_WIDTH-1 DOWNTO 0);
        douta : OUT STD_LOGIC_VECTOR(BRAM_DATA_WIDTH-1 DOWNTO 0);
        clkb : IN STD_LOGIC;
        enb : IN STD_LOGIC;
        web : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addrb : IN STD_LOGIC_VECTOR(BRAM_ADDR_WIDTH-1 DOWNTO 0);
        dinb : IN STD_LOGIC_VECTOR(BRAM_DATA_WIDTH-1 DOWNTO 0);
        doutb : OUT STD_LOGIC_VECTOR(BRAM_DATA_WIDTH-1 DOWNTO 0)
      );
    END COMPONENT;
    
    -- signal BRAM port a
    signal signal_bram_ena      : STD_LOGIC;
    signal signal_bram_wea      : STD_LOGIC_VECTOR(0 DOWNTO 0);
    signal signal_bram_addra    : STD_LOGIC_VECTOR(BRAM_ADDR_WIDTH-1 DOWNTO 0);
    signal signal_bram_dina     : STD_LOGIC_VECTOR(BRAM_DATA_WIDTH-1 DOWNTO 0);
    signal signal_bram_douta    : STD_LOGIC_VECTOR(BRAM_DATA_WIDTH-1 DOWNTO 0);
    
    signal signal_bram_first    : STD_LOGIC;
 
    -- signal BRAM port b
    signal signal_bram_enb      : STD_LOGIC;
    signal signal_bram_web      : STD_LOGIC_VECTOR(0 DOWNTO 0);
    signal signal_bram_addrb    : STD_LOGIC_VECTOR(BRAM_ADDR_WIDTH-1 DOWNTO 0);
    signal signal_bram_dinb     : STD_LOGIC_VECTOR(BRAM_DATA_WIDTH-1 DOWNTO 0);
    signal signal_bram_doutb    : STD_LOGIC_VECTOR(BRAM_DATA_WIDTH-1 DOWNTO 0);
    
    COMPONENT ascan_decimator is
        generic (
            REGISTER_W32        : INTEGER := 32;
            REGISTER_W16        : INTEGER := 16;
            REGISTER_W14        : INTEGER := 14;
            WAIT_CYCLES        	: INTEGER := 32;
            N_INPUT_DELAY       : INTEGER := 8;
            N_OUTPUT_DELAY      : INTEGER := 4;
            DEC_TYPE_NONE    	: INTEGER := 0;
            DEC_TYPE_SIMPLE     : INTEGER := 1;
            DEC_TYPE_MINMAX     : INTEGER := 2;
            DEC_TYPE_ALOK     	: INTEGER := 3
        );
        port (
            -- Sync
            aclk         : in STD_LOGIC;
            aresetn      : in STD_LOGIC;
        
            -- Control
            DEC_TYPE	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);    
            DEC_LEVEL	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            DEC_WINDOW	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            DATA_WINDOW	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            DEC_EN          : out STD_LOGIC;
            
            DEC_TYPE_RESULT	    : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);    
            DEC_LEVEL_RESULT	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            DEC_WINDOW_RESULT	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
            
            -- Signal input
            SIGNAL_INPUT_AXIS_TREADY  : out STD_LOGIC;
            SIGNAL_INPUT_AXIS_TDATA	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
            SIGNAL_INPUT_AXIS_TMAX	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
            SIGNAL_INPUT_AXIS_TLAST	  : in STD_LOGIC;
            SIGNAL_INPUT_AXIS_TVALID  : in STD_LOGIC;
    
            -- Decimation result
            DEC_RESULT_AXIS_TVALID    : out STD_LOGIC;
            DEC_RESULT_AXIS_TDATA	  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
            DEC_RESULT_AXIS_X	      : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
            DEC_RESULT_AXIS_TLAST	  : out STD_LOGIC;
            DEC_RESULT_AXIS_TREADY    : in STD_LOGIC
        );
    end COMPONENT ascan_decimator;
    
    signal dec_result_axis_tvalid_i : STD_LOGIC_VECTOR(2 downto 0);
    signal dec_result_axis_tlast_i  : STD_LOGIC_VECTOR(2 downto 0);
    signal dec_result_axis_tlast_d1  : STD_LOGIC;
    signal dec_result_axis_tlast_d2  : STD_LOGIC;
    signal dec_result_axis_tready_i : STD_LOGIC;
    signal dec_result_axis_tdata_i  : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal dec_result_axis_x_i  : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    
    type states_general_sm is (idle, store_result, check_operation, provide_dec_result, provide_dec_result_finish); 
    signal state_general_sm : states_general_sm;
    
    signal provide_dec_result_flag  : STD_LOGIC;
    signal provide_dec_result_end   : STD_LOGIC;
      
    type states_write_signal_sm is (idle, store_result, provide_dec_result); 
    signal state_write_signal_sm : states_write_signal_sm;
    
    type states_read_signal_sm is (idle, store_result, provide_dec_result); 
    signal state_read_signal_sm : states_read_signal_sm;
    
    signal data_window_half_int     : INTEGER range 0 to 65536 :=0;

begin

    -- GENERAL STATE MACHINE
    general_sm: process(aclk)
        variable var_counter_wait  : integer range 0 to 65536 :=0; 
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then  
                DEC_END <= '0';		
                SIGNAL_INPUT_AXIS_TREADY <= '0';
                provide_dec_result_flag <= '0';
                dec_result_axis_tready_i <= '0';					
                -- State
                state_general_sm <= idle;
            else
                case state_general_sm is        
                    when idle =>
                        var_counter_wait:= 0;
                        SIGNAL_INPUT_AXIS_TREADY <= signal_input_axis_tready_i;
                        dec_result_axis_tready_i <= '1';
                        if(dec_result_axis_tvalid_i(0) = '1') then
                            state_general_sm <= store_result;
                        end if;
                    when store_result =>
                        SIGNAL_INPUT_AXIS_TREADY <= signal_input_axis_tready_i;
                        if(dec_result_axis_tlast_i(0) = '1') then
                            dec_result_axis_tready_i <= '0';
                            state_general_sm <= check_operation;
                        end if;
                    when check_operation =>
                        if(NO_READ_DEC_RESULT = '1') then
                            DEC_END <= '0';
                            state_general_sm  <= idle; 
                        else
                            if(var_counter_wait >= (WAIT_CYCLES-1)) then
                                var_counter_wait:= 0;
                                DEC_END <= '1';
                                if(READ_DEC_RESULT = '1') then
                                    state_general_sm  <= provide_dec_result;
                                end if;
                            else
                                var_counter_wait:= var_counter_wait + 1;
                            end if;
                        end if;                 
                    when provide_dec_result =>
                        if(provide_dec_result_end = '1') then
                            provide_dec_result_flag <= '0';
                            DEC_END                 <= '0';
                            state_general_sm        <= provide_dec_result_finish;
                        else
                            provide_dec_result_flag <= '1';
                        end if;  
                    when provide_dec_result_finish =>
                        if(READ_DEC_RESULT = '0') then
                            state_general_sm  <= idle;
                        end if;                                    
                    when others =>
                        null;
                end case;
             end if; 
         end if;
    end process;
    
    -- Write decimation result
    -- Read decimation result
    signal_BRAM_porta: process(aclk) 
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                signal_bram_ena   <= '1';
                signal_bram_wea   <= (others => '0');
                signal_bram_addra <= (others => '0');
                signal_bram_dina  <= (others => '0');
                dec_result_axis_tlast_d1 <= '0';
                dec_result_axis_tlast_d2 <= '0';
                -- State
                state_write_signal_sm <= idle;
            else     
                dec_result_axis_tlast_d1 <= dec_result_axis_tlast_i(0); 
                dec_result_axis_tlast_d2 <= dec_result_axis_tlast_d1; 
                case state_write_signal_sm is        
                    when idle =>
                        if(unsigned(dec_type_result_i) = DEC_TYPE_ALOK) then
                            signal_bram_addra <= std_logic_vector(to_unsigned(1, signal_bram_addra'length));  
                        else
                            signal_bram_addra <= (others => '0'); 
                        end if;                                                     
                        if(dec_result_axis_tvalid_i(0) = '1') then
                            signal_bram_wea <= (others => '1');
                            signal_bram_dina  <= dec_result_axis_tdata_i; 
                            state_write_signal_sm <= store_result;
                        elsif(provide_dec_result_flag = '1') then  
                            state_write_signal_sm <= provide_dec_result; 
                        else
                            signal_bram_wea <= (others => '0');
                        end if;
                    when store_result =>
                        if(dec_result_axis_tvalid_i(0) = '1') then
                            if(unsigned(dec_type_result_i) = DEC_TYPE_ALOK) then
                                signal_bram_addra <= std_logic_vector(unsigned(signal_bram_addra)+2); 
                            else
                                signal_bram_addra <= std_logic_vector(unsigned(signal_bram_addra)+1);
                            end if;                          
                            signal_bram_dina  <= dec_result_axis_tdata_i;    
                        else
                            if(dec_result_axis_tlast_d2 = '1') then
                                state_write_signal_sm <= idle;
                            end if;                  
                        end if;  
                    when provide_dec_result =>
                        if(provide_dec_result_flag = '1') then
                            if(DEC_RESULT_AXIS_TREADY = '1') then
                                if(signal_bram_first = '1') then
                                    signal_bram_addra <= std_logic_vector(to_unsigned(1, signal_bram_addra'length));       
                                else
                                    signal_bram_addra <= std_logic_vector(unsigned(signal_bram_addra)+2);
                                end if; 
                            end if;    
                        else
                            state_write_signal_sm <= idle;
                        end if;                                                                
                    when others => 
                        null;
                end case;                                 
            end if;
        end if;
    end process;
    
    -- Read decimation result
    signal_BRAM_portb: process(aclk) 
        variable var_counter_wait      : integer range 0 to 8192 :=0;
        variable var_counter_result      : integer range 0 to 65535 :=0;
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                signal_bram_enb     <= '1';
                signal_bram_web     <= (others => '0');
                signal_bram_addrb   <= (others => '0');
                signal_bram_dinb    <= (others => '0');
                signal_bram_first   <= '1';
                provide_dec_result_end <= '0';
                dec_result_axis_tvalid_i(1)  <= '0';
                dec_result_axis_tvalid_i(2)  <= '0';
                dec_result_axis_tlast_i(1)   <= '0';
                dec_result_axis_tlast_i(2)   <= '0';
                DEC_RESULT_AXIS_TVALID       <= '0';
                DEC_RESULT_AXIS_TLAST        <= '0';
                -- State    
                state_read_signal_sm <= idle;
            else          
                dec_result_axis_tvalid_i(2) <= dec_result_axis_tvalid_i(1);   
                DEC_RESULT_AXIS_TVALID      <= dec_result_axis_tvalid_i(2);    
                dec_result_axis_tlast_i(2)  <= dec_result_axis_tlast_i(1);    
                DEC_RESULT_AXIS_TLAST       <= dec_result_axis_tlast_i(2);              
                -- State machine
                case state_read_signal_sm is 
                    when idle =>
                        signal_bram_addrb <= (others => '0');                      
                        signal_bram_first   <= '1';
                        var_counter_result:= 0;
                        if(dec_result_axis_tvalid_i(0) = '1' and unsigned(dec_type_result_i) = DEC_TYPE_ALOK) then
                            signal_bram_web <= (others => '1');
                            signal_bram_dinb  <= dec_result_axis_x_i;
                            state_read_signal_sm <= store_result;
                        elsif(provide_dec_result_flag = '1') then  
                            signal_bram_web <= (others => '0');
                            state_read_signal_sm <= provide_dec_result; 
                        else
                            signal_bram_web <= (others => '0');
                        end if;  
                    when store_result =>
                        if(dec_result_axis_tvalid_i(0) = '1') then
                            signal_bram_addrb <= std_logic_vector(unsigned(signal_bram_addrb)+2);                      
                            signal_bram_dinb  <= dec_result_axis_x_i;    
                        else
                            if(dec_result_axis_tlast_d2 = '1') then
                                state_read_signal_sm <= idle;
                            end if;                  
                        end if;          
                    when provide_dec_result =>
                        if(provide_dec_result_flag = '1') then
                            if(DEC_RESULT_AXIS_TREADY = '1') then
                                if(var_counter_result >= (data_window_half_int)) then
                                    dec_result_axis_tvalid_i(1)  <= '0';
                                    dec_result_axis_tlast_i(1)   <= '0';
                                    provide_dec_result_end  <= '1';
                                else
                                    dec_result_axis_tvalid_i(1) <= '1';
                                    if(signal_bram_first = '1') then
                                        signal_bram_first <= '0';
                                        signal_bram_addrb <= (others => '0');
                                    else
                                        signal_bram_addrb <= std_logic_vector(unsigned(signal_bram_addrb)+2); 
                                    end if;
                                    if(var_counter_result >= (data_window_half_int-1)) then
                                        dec_result_axis_tlast_i(1) <= '1';
                                    end if;
                                    var_counter_result:= var_counter_result + 1;
                                end if;
                            else
                                dec_result_axis_tvalid_i(1)  <= '0';                                                         
                            end if;  
                        else
                            provide_dec_result_end <= '0';
                            signal_bram_first  <= '1';
                            dec_result_axis_tvalid_i(1)  <= '0';
                            dec_result_axis_tlast_i(1)   <= '0';
                            state_read_signal_sm <= idle;
                        end if;                   
                    when others => 
                        null;
                end case;     
            end if;
        end if;
    end process; 
    
    DEC_RESULT_AXIS_TDATA <= std_logic_vector(resize(signed(signal_bram_douta), SIGNAL_INPUT_AXIS_TDATA'length)) &
                             std_logic_vector(resize(signed(signal_bram_doutb), SIGNAL_INPUT_AXIS_TDATA'length));
    
    aux_process: process(aclk) 
    begin
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                DEC_TYPE_RESULT <= (others => '0');
                DEC_LEVEL_RESULT <= (others => '0');
                DEC_WINDOW_HALF_RESULT <= (others => '0');
                DEC_WINDOW_RESULT <= (others => '0');
                data_window_half_int <= 0;
            else 
                DEC_TYPE_RESULT <= dec_type_result_i;
                DEC_LEVEL_RESULT <= dec_level_result_i;
                DEC_WINDOW_RESULT <= dec_window_result_i;
                DEC_WINDOW_HALF_RESULT <= std_logic_vector(to_unsigned(data_window_half_int, DEC_WINDOW_HALF_RESULT'length)); 
                if(dec_window_result_i(0) = '0') then
                    data_window_half_int<= to_integer(unsigned(dec_window_result_i(REGISTER_W32-1 downto 1)));
                else
                    data_window_half_int<= to_integer(unsigned(dec_window_result_i(REGISTER_W32-1 downto 1))) + 1;
                end if;
            end if;
        end if;
    end process; 
    				
    ascan_decimator_inst: ascan_decimator
    generic map(
        REGISTER_W32        => REGISTER_W32,
        REGISTER_W16        => REGISTER_W16,
        REGISTER_W14        => REGISTER_W14,
        WAIT_CYCLES         => WAIT_CYCLES,
        N_INPUT_DELAY       => N_INPUT_DELAY,
        N_OUTPUT_DELAY      => N_OUTPUT_DELAY,
        DEC_TYPE_NONE       => DEC_TYPE_NONE,
        DEC_TYPE_SIMPLE     => DEC_TYPE_SIMPLE,
        DEC_TYPE_MINMAX     => DEC_TYPE_MINMAX,
        DEC_TYPE_ALOK       => DEC_TYPE_ALOK
    )
    port map(
        -- Sync
        aclk         => aclk,
        aresetn      => aresetn,
    
        -- Control
        DEC_TYPE        => DEC_TYPE, 
        DEC_LEVEL       => DEC_LEVEL,
        DEC_WINDOW      => DEC_WINDOW,
        DATA_WINDOW     => DATA_WINDOW,
        DEC_EN          => DEC_EN,
        
        DEC_TYPE_RESULT     => dec_type_result_i, 
        DEC_LEVEL_RESULT    => dec_level_result_i,
        DEC_WINDOW_RESULT   => dec_window_result_i,
        
        -- Signal input
        SIGNAL_INPUT_AXIS_TREADY  => signal_input_axis_tready_i,
        SIGNAL_INPUT_AXIS_TDATA   => SIGNAL_INPUT_AXIS_TDATA,
        SIGNAL_INPUT_AXIS_TMAX    => SIGNAL_INPUT_AXIS_TMAX,
        SIGNAL_INPUT_AXIS_TLAST   => SIGNAL_INPUT_AXIS_TLAST,
        SIGNAL_INPUT_AXIS_TVALID  => SIGNAL_INPUT_AXIS_TVALID,

        -- Average result
        DEC_RESULT_AXIS_TVALID    => dec_result_axis_tvalid_i(0),
        DEC_RESULT_AXIS_TDATA     => dec_result_axis_tdata_i,
        DEC_RESULT_AXIS_X         => dec_result_axis_x_i,
        DEC_RESULT_AXIS_TLAST     => dec_result_axis_tlast_i(0),
        DEC_RESULT_AXIS_TREADY    => dec_result_axis_tready_i
    );
    
    decimator_filter_BRAM_inst : decimator_filter_BRAM
        port map (
            clka => aclk,
            ena => signal_bram_ena,
            wea => signal_bram_wea,
            addra => signal_bram_addra,
            dina => signal_bram_dina,
            douta => signal_bram_douta,
            clkb => aclk,
            enb => signal_bram_enb,
            web => signal_bram_web,
            addrb => signal_bram_addrb,
            dinb => signal_bram_dinb,
            doutb => signal_bram_doutb
        );
	      																					  																			  																		      	   
end arch_imp;
