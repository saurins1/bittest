----------------------------------------------------------------------------------
-- Company:     Innerspec Technologies
-- Engineer:    Carlos Luengo Gomez 
-- 
-- Create Date: 09/27/2017 10:07:23 AM
-- Design Name: 
-- Module Name: channel - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity channel is
	generic (
		-- General
		CHANNEL_ID		: INTEGER := 0;
		REGISTER_W64    : INTEGER := 64;
		REGISTER_W48    : INTEGER := 48;
	    REGISTER_W32    : INTEGER := 32;
	    REGISTER_W21    : INTEGER := 21;
        REGISTER_W16    : INTEGER := 16;
		REGISTER_W14    : INTEGER := 14;
		REGISTER_W8     : INTEGER := 8;
		REGISTER_W4     : INTEGER := 4;
		REGISTER_W2     : INTEGER := 2;
		
		-- Scan mode
	    SCAN_MODE_NORMAL  : INTEGER := 0;
        SCAN_MODE_MRUT    : INTEGER := 10;
        
		-- Trigger type
        TRIGGER_TYPE_NORMAL     : INTEGER := 0;
        TRIGGER_TYPE_SW         : INTEGER := 10;
        TRIGGER_TYPE_SW_PWM     : INTEGER := 11;
        TRIGGER_TYPE_ENC        : INTEGER := 20;
        TRIGGER_TYPE_ROBOT      : INTEGER := 30;
        TRIGGER_TYPE_ROBOT_ENC  : INTEGER := 40;
		
		-- external trigger
		DISPOSITION_PASS  : INTEGER := 1;
		DISPOSITION_FAIL  : INTEGER := 2;
		DISPOSITION_NODET : INTEGER := 0;
		
		-- Coinc
        TRIGGER_WAIT    : INTEGER := 32;
        MIN_COINC_LEVEL : INTEGER := 2;
		
		-- Avg
		MIN_AVG_LEVEL   	: INTEGER := 2;
		AVG_TYPE_NORMAL     : INTEGER := 0;
		AVG_TYPE_PIPELINE   : INTEGER := 1;
		
		-- Motor
		MOTOR_STOP_CYCLES   : INTEGER := 25000000; --250ms
		
		-- LRUT
		LRUT_WAIT_CYCLES   : INTEGER := 700000; --7ms
		
		-- Ascan header size
		ASCAN_HEADER_SIZE : INTEGER := 43;
		
		-- FIR CORR
        N_TAPS              : INTEGER    := 512;
        N_SETS_TAP          : INTEGER    := 3;
        OVERSAMPLING        : INTEGER    := 2;
        WAIT_CYCLES         : INTEGER    := 4
	);
	port (
	    -- Sync
	    aclk         : in STD_LOGIC;
        aresetn      : in STD_LOGIC;
		
		-- Control	
		CONFIGURE_CONTROL  	  : in STD_LOGIC;
		
		-- Start inspection
		START_INSPECTION_CONTROL  : in STD_LOGIC;
		START_INSPECTION_STATUS   : out STD_LOGIC;
		
		-- Start top inspection
        START_STOP_INSPECTION_CONTROL  : in STD_LOGIC;
        START_STOP_INSPECTION_STATUS   : out STD_LOGIC;
        DISPOSITION		  : out STD_LOGIC; 
        DISPOSITION_BITS  : out STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0); --
		
		-- Scan mode
		SCAN_MODE_CONTROL : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        SCAN_MODE_STATUS  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        
        -- Trigger type
        TRIGGER_TYPE_CONTROL : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRIGGER_TYPE_STATUS  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        
        -- Trigger sw
        TRIGGER_SW_CONTROL : in STD_LOGIC;
        TRIGGER_SW_STATUS  : out STD_LOGIC;
		
		-- PWM
		PWM_MOTOR_EN     : in STD_LOGIC;
		PWM_END          : in STD_LOGIC;
		PWM_MOTOR_MOVE   : out STD_LOGIC;
		
		-- LRUT
		LRUT_EN          : out STD_LOGIC;
        
        -- Ascan
        READ_ASCAN  	 : in STD_LOGIC;
        ASCAN_READY      : out STD_LOGIC;
        ASCAN_SIZE       : out STD_LOGIC_VECTOR(REGISTER_W21-1 downto 0);
				
		-- Debug
		CHANNEL_ENABLED  : out STD_LOGIC;
		CONFIG_ON        : out STD_LOGIC;
		CHANNEL_STATE_A	 : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		CHANNEL_STATE_B	 : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		CHANNEL_STATE_C	 : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		
		-- Signal input
		SIGNAL_COINC_WINDOW  	  : out STD_LOGIC;
		SIGNAL_COINC_COUNTER      : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
		SIGNAL_AVG_WINDOW  		  : out STD_LOGIC;
		SIGNAL_AVG_COUNTER        : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
		
		SIGNAL_INPUT_AXIS_TREADY  : out STD_LOGIC;
		SIGNAL_INPUT_AXIS_TDATA	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		SIGNAL_INPUT_AXIS_TLAST	  : in STD_LOGIC;
		SIGNAL_INPUT_AXIS_TVALID  : in STD_LOGIC;
		
		-- Config
		CONFIG_INPUT_LOAD	  	  : in STD_LOGIC;
		CONFIG_INPUT_AXIS_TREADY  : out STD_LOGIC;
		CONFIG_INPUT_AXIS_TDATA	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		CONFIG_INPUT_AXIS_TLAST	  : in STD_LOGIC;
		CONFIG_INPUT_AXIS_TVALID  : in STD_LOGIC;
		
		-- Encoders
		ENCODER1_POS  	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		ENCODER1_DIR  	  : in STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
		ENCODER1_RPM  	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		ENCODER1_TRIGGER_COUNTS : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		ENCODER2_POS  	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		ENCODER2_DIR  	  : in STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
		ENCODER2_RPM  	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		ENCODER2_TRIGGER_COUNTS : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		ENCODER3_POS  	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		ENCODER3_DIR  	  : in STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
		ENCODER3_RPM  	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		ENCODER3_TRIGGER_COUNTS : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		-- IO
		IO_INPUTS  	  	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		IO_OUTPUTS  	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		
		-- Config output receiver
		RECEIVER_SAMPLING_FREQUENCY         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        RECEIVER_DATA_WINDOW                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        RECEIVER_DELAY                      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        RECEIVER_ANALOG_GAIN                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  --db*1024
        RECEIVER_DIGITAL_GAIN               : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  --V*1024*1024  
        RECEIVER_EXTERNAL_MULTIPLEXER_EN    : out STD_LOGIC;
		
		-- Config output transmitter
		TRANSMITTER_VOLTAGE                 : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        TRANSMITTER_BURST_FREQUENCY         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        TRANSMITTER_N_CYCLES                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        TRANSMITTER_N_BURST                 : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_DELAY                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_DELTA_DELAY             : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_DELTA_ADD               : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_DIRECTIONAL_PHASING     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        TRANSMITTER_START_CYCLE	            : out STD_LOGIC_VECTOR(REGISTER_W2-1 downto 0);
		
		-- Config output magnet
		MAGNET_MODE                         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        MAGNET_PULSE_WIDTH                  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        MAGNET_INITIAL_DELAY                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        MAGNET_RAMP_UP_VOLTAGE              : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        MAGNET_VOLTAGE                      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		-- Condig analog filters
		DSP_ANALOG_FILTER  					: out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
		DSP_COINC_LEVEL                     : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
		DSP_AVG_LEVEL                       : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
		DSP_AVG_TYPE                        : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		-- Config DAC curves
		CONFIG_DAC_AXIS_TREADY    : in STD_LOGIC;
		CONFIG_DAC_AXIS_TDATA	  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		CONFIG_DAC_AXIS_TLAST	  : out STD_LOGIC;
		CONFIG_DAC_AXIS_TVALID    : out STD_LOGIC;
					
		-- ASCAN
		ASCAN_AXIS_TREADY  : in STD_LOGIC;
		ASCAN_AXIS_TDATA   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		ASCAN_AXIS_TLAST   : out STD_LOGIC;
		ASCAN_AXIS_TVALID  : out STD_LOGIC;
		ASCAN_AXIS_TKEEP   : out std_logic_vector((REGISTER_W32/8)-1 downto 0)
	);
end channel;

architecture arch_imp of channel is
	
	-- DIGITAL AMPLIFIER
	COMPONENT digital_amplifier is
	generic (
		REGISTER_W48        : INTEGER    := 48;
        REGISTER_W32        : INTEGER    := 32;
        REGISTER_W16        : INTEGER    := 16;
		DIVIDER_FACTOR      : INTEGER    := 20; -- /(1024*1024)
		MULTIPLIER_DELAY    : INTEGER    := 4
    );
    port (
        aclk         : in std_logic;
        aresetn      : in std_logic;
		
		-- Gain
		DIGITAL_AMPLIFIER_GAIN	: in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		-- Signal in
		SIGNAL_INPUT_AXIS_TREADY 	: out STD_LOGIC;
		SIGNAL_INPUT_AXIS_TDATA 	: in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		SIGNAL_INPUT_AXIS_TLAST		: in STD_LOGIC;
		SIGNAL_INPUT_AXIS_TVALID  	: in STD_LOGIC;

		-- Signal out
		SIGNAL_OUTPUT_AXIS_TREADY 	: in STD_LOGIC;
		SIGNAL_OUTPUT_AXIS_TDATA	: out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		SIGNAL_OUTPUT_AXIS_TLAST	: out STD_LOGIC;
		SIGNAL_OUTPUT_AXIS_TVALID  : out STD_LOGIC		
    );
	end COMPONENT digital_amplifier;

	-- signal output = input for coincidence
	signal signal_output_axis_tdata_i	: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal signal_output_axis_tlast_i	: STD_LOGIC;
	signal signal_output_axis_tvalid_i  : STD_LOGIC;	
	
	-- Signal input tready
	signal digital_amplifier_tready  : STD_LOGIC;

	-- COINCIDENCE FILTER
	COMPONENT coincidence_filter is
		generic (
			REGISTER_W32    : INTEGER := 32;
			REGISTER_W16    : INTEGER := 16;
			REGISTER_W8     : INTEGER := 8;
			TRIGGER_WAIT    : INTEGER := 32;
			MIN_COINC_LEVEL : INTEGER := 2
		);
		port (
			-- Sync
			aclk         : in STD_LOGIC;
			aresetn      : in STD_LOGIC;

			-- Config
			COINC_LEVEL	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

			PHASE_SHIFT_LEVEL     : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			PHASE_SHIFT           : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			N_BURST               : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

			DATA_WINDOW	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			SAMPLE_FREQ	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

			DATA_WINDOW_RESULT	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			SAMPLE_FREQ_RESULT	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			COINC_LEVEL_RESULT	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			COINC_COUNTER     : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
			COINC_WINDOW  	  : out STD_LOGIC;

			-- Signal input
			SIGNAL_INPUT_AXIS_TREADY  : out STD_LOGIC;
			SIGNAL_INPUT_AXIS_TDATA	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
			SIGNAL_INPUT_AXIS_TLAST	  : in STD_LOGIC;
			SIGNAL_INPUT_AXIS_TVALID  : in STD_LOGIC;

			-- Coincidence result
			COINC_RESULT_AXIS_TVALID  : out STD_LOGIC;
			COINC_RESULT_AXIS_TDATA	  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
			COINC_RESULT_AXIS_TLAST	  : out STD_LOGIC;
			COINC_RESULT_AXIS_TREADY  : in STD_LOGIC
		);
	end COMPONENT coincidence_filter;

	-- DATA window
	signal coinc_data_window_result	 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal coinc_sample_freq_result	 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

	-- Advanced filter
	signal transmitter_phase_shift : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal dsp_phase_shift	 	   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal transmitter_n_burst_i   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	
	-- Signal input tready
	signal coinc_signal_input_axis_tready  : STD_LOGIC;

	-- Coincidence result
	signal coinc_result_axis_tvalid  : STD_LOGIC;
	signal coinc_result_axis_tdata	 : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal coinc_result_axis_tlast	 : STD_LOGIC;

	signal coinc_level_result : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

	signal coinc_counter	 : UNSIGNED(REGISTER_W32-1 downto 0);

	COMPONENT average_filter is
		generic (
			REGISTER_W32        : INTEGER := 32;
			REGISTER_W16        : INTEGER := 16;
			REGISTER_W8         : INTEGER := 8;
			REGISTER_W14        : INTEGER := 14;
			AVG_TYPE_NORMAL     : INTEGER := 0;
			AVG_TYPE_PIPELINE   : INTEGER := 1;
			TRIGGER_WAIT        : INTEGER := 32;
			MIN_AVG_LEVEL       : INTEGER := 2
		);
		port (
			-- Sync
			aclk         : in STD_LOGIC;
			aresetn      : in STD_LOGIC;

			-- Control
			AVG_LEVEL	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);    
			DATA_WINDOW	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			SAMPLE_FREQ	    : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			DATA_WINDOW_RESULT	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			SAMPLE_FREQ_RESULT	: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			AVG_COUNTER         : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
			AVG_WINDOW  	    : out STD_LOGIC;

			AVG_TYPE	     : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			AVG_RESET        : in STD_LOGIC;
			AVG_RESET_ACK    : out STD_LOGIC;
			AVG_TYPE_RESULT	 : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			AVG_LEVEL_RESULT : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

			-- Signal input
			SIGNAL_INPUT_AXIS_TREADY  : out STD_LOGIC;
			SIGNAL_INPUT_AXIS_TDATA	  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
			SIGNAL_INPUT_AXIS_TLAST	  : in STD_LOGIC;
			SIGNAL_INPUT_AXIS_TVALID  : in STD_LOGIC;

			-- Average result
			AVG_RESULT_AXIS_TVALID    : out STD_LOGIC;
			AVG_RESULT_AXIS_TDATA	  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
			AVG_RESULT_AXIS_TLAST	  : out STD_LOGIC;
			AVG_RESULT_AXIS_TREADY    : in STD_LOGIC
		);
	end COMPONENT average_filter;
	
	-- DATA window
	signal avg_data_window_result	 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal avg_sample_freq_result	 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

	-- Signal input tready
	signal avg_signal_input_axis_tready  : STD_LOGIC;

	-- Average result
	signal avg_result_axis_tvalid   : STD_LOGIC;
	signal avg_result_axis_tdata	: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal avg_result_axis_tlast	: STD_LOGIC;

	signal avg_type	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal avg_type_result	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal avg_level_result	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal avg_reset : STD_LOGIC;
	signal avg_reset_ack : STD_LOGIC;
	signal avg_counter	: STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);

	-- FIR FILTER + CORRELTION
	COMPONENT fir_correlation is
	generic (
	    REGISTER_W48        : INTEGER    := 48;
        REGISTER_W32        : INTEGER    := 32;
        REGISTER_W16        : INTEGER    := 16;
        N_TAPS              : INTEGER    := 512;
        N_SETS_TAP          : INTEGER    := 3;
        OVERSAMPLING        : INTEGER    := 2;
        WAIT_CYCLES         : INTEGER    := 4
    );
    port (
        aclk         : in std_logic;
        aresetn      : in std_logic;
        
		NO_READ_FIR_RESULT          : in STD_LOGIC;
        READ_FIR_RESULT             : in STD_LOGIC;
        DATA_WINDOW                 : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        SAMPLE_FREQ                 : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        DATA_WINDOW_RESULT          : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        SAMPLE_FREQ_RESULT            : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		DATA_WINDOW_HALF_RESULT     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		FIR_END                     : out STD_LOGIC;
        
		-- FIR config
        BAND_PASS_FILTER_EN         : in STD_LOGIC;
        BAND_PASS_FILTER_L_DIVIDER  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GAUSSIAN_FILTER_EN          : in STD_LOGIC;
        GAUSSIAN_FILTER_L_DIVIDER   : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        ENVELOPE_FILTER_EN          : in STD_LOGIC;
        ENVELOPE_FILTER_L_DIVIDER   : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        
        FIR_TAPS_AXIS_TREADY        : out STD_LOGIC;
        FIR_TAPS_AXIS_TDATA         : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        FIR_TAPS_AXIS_TLAST         : in STD_LOGIC;
        FIR_TAPS_AXIS_TVALID        : in STD_LOGIC;
     
		-- Signal Input
        SIGNAL_INPUT_AXIS_TREADY    : out STD_LOGIC;
        SIGNAL_INPUT_AXIS_TDATA     : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        SIGNAL_INPUT_AXIS_TLAST     : in STD_LOGIC;
        SIGNAL_INPUT_AXIS_TVALID    : in STD_LOGIC;
        
		-- Gates Config
        GATES_EN                    : in STD_LOGIC;
        GATES_SAMPLE_START          : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATES_SAMPLE_END            : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        
		-- Gates Result
        GATES_AXIS_TVALID           : out STD_LOGIC;
        GATES_AXIS_TDATA            : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATES_AXIS_TLAST            : out STD_LOGIC;
        GATES_AXIS_TREADY           : in STD_LOGIC;
        
		-- Correlation Config
        CORRELATION_EN                  : in STD_LOGIC;
        CORRELATION_GATE_SAMPLE_START   : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        CORRELATION_GATE_SAMPLE_END     : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        
		-- Correlation Result
        CORRELATION_AXIS_TVALID     : out STD_LOGIC;
        CORRELATION_AXIS_TDATA      : out STD_LOGIC_VECTOR(REGISTER_W48-1 downto 0);
        CORRELATION_AXIS_TLAST      : out STD_LOGIC;
        CORRELATION_AXIS_TREADY     : in STD_LOGIC;

		-- FIR Result
        FIR_RESULT_AXIS_TVALID      : out STD_LOGIC;
        FIR_RESULT_AXIS_TDATA       : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        FIR_RESULT_AXIS_TLAST       : out STD_LOGIC;
        FIR_RESULT_AXIS_TREADY      : in STD_LOGIC
    );
	end COMPONENT fir_correlation;

	-- DATA window
	signal fir_data_window_result	 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal fir_data_window_half_result	 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal fir_sample_freq_result	 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

	-- FIR end
	signal fir_end  : STD_LOGIC;

	-- FIR input tready
	signal fir_taps_axis_tready	: STD_LOGIC;
	
	-- Gates Result
	signal gates_axis_tdata	 	: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal gates_axis_tlast  	: STD_LOGIC;
	signal gates_axis_tvalid 	: STD_LOGIC;

	-- Correlation Result
	signal correlation_axis_tdata	: STD_LOGIC_VECTOR(REGISTER_W48-1 downto 0);
	signal correlation_axis_tlast  	: STD_LOGIC;
	signal correlation_axis_tvalid 	: STD_LOGIC;

	-- FIR Result
	signal fir_result_axis_tdata	: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal fir_result_axis_tlast  	: STD_LOGIC;
	signal fir_result_axis_tvalid 	: STD_LOGIC;

	-- GATES PROCESSOR

	COMPONENT gates_processor is
	generic (
		REGISTER_W64  : INTEGER    := 64;
		REGISTER_W48  : INTEGER    := 48;
        REGISTER_W32  : INTEGER    := 32;
        REGISTER_W16  : INTEGER    := 16;
        REGISTER_W8   : INTEGER    := 8;
        REGISTER_W4   : INTEGER    := 4;
        REGISTER_W2   : INTEGER    := 2
	);
	port (
	    --sync
	    aclk           : in STD_LOGIC;
        aresetn        : in STD_LOGIC;  
	    
	    -- GENERAL    
	    RX_DELAY       : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);    

        -- GATE1
		GATE1_EN                          : in STD_LOGIC;  
		GATE1_START                       : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		GATE1_WIDTH                       : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);	
        GATE1_AMP_THRESHOLD               : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE1_AMP_THRESHOLD_ALARM_EN      : in STD_LOGIC; 
        GATE1_AMP_THRESHOLD_ALARM_CROSSING: in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE1_TOF_ALGORITHM               : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE1_TOF_AVG                     : in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
        GATE1_TOF_MIN_THICKNESS           : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE1_TOF_ALARM_EN                : in STD_LOGIC; 
        GATE1_TOF_ALARM_TYPE              : in STD_LOGIC;
        GATE1_TOF_ALARM_CROSSING          : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE1_TOF_ALARM_MIN_VALUE         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE1_TOF_ALARM_MAX_VALUE         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE1_TOF                         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE1_TOF_AVERAGED                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE1_AMP_ALARM                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        GATE1_TOF_ALARM                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        GATE1_MAX                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE1_MAX_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE1_MIN                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE1_MIN_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        
        -- GATE2
        GATE2_EN                          : in STD_LOGIC;  
        GATE2_START                       : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE2_WIDTH                       : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);    
        GATE2_AMP_THRESHOLD               : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE2_AMP_THRESHOLD_ALARM_EN      : in STD_LOGIC; 
        GATE2_AMP_THRESHOLD_ALARM_CROSSING: in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE2_TOF_ALGORITHM               : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE2_TOF_AVG                     : in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
        GATE2_TOF_MIN_THICKNESS           : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE2_TOF_ALARM_EN                : in STD_LOGIC; 
        GATE2_TOF_ALARM_TYPE              : in STD_LOGIC;
        GATE2_TOF_ALARM_CROSSING          : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE2_TOF_ALARM_MIN_VALUE         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE2_TOF_ALARM_MAX_VALUE         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE2_TOF                         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE2_TOF_AVERAGED                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE2_AMP_ALARM                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE2_TOF_ALARM                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE2_MAX                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE2_MAX_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE2_MIN                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE2_MIN_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        
        -- GATE3
        GATE3_EN                          : in STD_LOGIC;  
        GATE3_START                       : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE3_WIDTH                       : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);    
        GATE3_AMP_THRESHOLD               : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE3_AMP_THRESHOLD_ALARM_EN      : in STD_LOGIC; 
        GATE3_AMP_THRESHOLD_ALARM_CROSSING: in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE3_TOF_ALGORITHM               : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE3_TOF_AVG                     : in STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
        GATE3_TOF_MIN_THICKNESS           : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE3_TOF_ALARM_EN                : in STD_LOGIC; 
        GATE3_TOF_ALARM_TYPE              : in STD_LOGIC;
        GATE3_TOF_ALARM_CROSSING          : in STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE3_TOF_ALARM_MIN_VALUE         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE3_TOF_ALARM_MAX_VALUE         : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE3_TOF                         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE3_TOF_AVERAGED                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE3_AMP_ALARM                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        GATE3_TOF_ALARM                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE3_MAX                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE3_MAX_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE3_MIN                         : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE3_MIN_SAMPLE                  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);

		--GATES DATA
		GATES_CONFIG : in STD_LOGIC;
		GATES_SAMPLE_START: out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATES_SAMPLE_END  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATES_EN          : out STD_LOGIC;
        GATES_END         : out STD_LOGIC;
        READ_GATES_RESULT : in STD_LOGIC;
        
		GATES_AXIS_TREADY : out STD_LOGIC;
		GATES_AXIS_TDATA  : in STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		GATES_AXIS_TLAST  : in STD_LOGIC;
		GATES_AXIS_TVALID : in STD_LOGIC;
		
		-- CORRELATION
        CORRELATION_EN              	: out STD_LOGIC;
        CORRELATION_GATE_SAMPLE_START   : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        CORRELATION_GATE_SAMPLE_END     : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        CORRELATION_AXIS_TREADY     	: out STD_LOGIC;
        CORRELATION_AXIS_TDATA      	: in STD_LOGIC_VECTOR(REGISTER_W48-1 downto 0);
        CORRELATION_AXIS_TLAST      	: in STD_LOGIC;
        CORRELATION_AXIS_TVALID     	: in STD_LOGIC
	);
	end COMPONENT gates_processor;

	-- GATE1 result
	signal gate1_tof			: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal gate1_tof_averaged   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal gate1_amp_alarm      : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
	signal gate1_tof_alarm      : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
	signal gate1_max            : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal gate1_max_sample     : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal gate1_min            : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal gate1_min_sample     : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);

	-- GATE2 result
	signal gate2_tof			: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal gate2_tof_averaged   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal gate2_amp_alarm      : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    signal gate2_tof_alarm      : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    signal gate2_max            : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal gate2_max_sample     : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal gate2_min            : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal gate2_min_sample     : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);

	-- GATE3 result
	signal gate3_tof			: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal gate3_tof_averaged   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal gate3_amp_alarm      : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    signal gate3_tof_alarm      : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
    signal gate3_max            : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal gate3_max_sample     : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal gate3_min            : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal gate3_min_sample     : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);

	-- GATE config for FIR
	signal gates_sample_start	: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal gates_sample_end  	: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal gates_en          	: STD_LOGIC;
	signal gates_end         	: STD_LOGIC;

	signal gates_axis_tready    : STD_LOGIC;

	-- GATE config for CORR
	signal correlation_en              		: STD_LOGIC;
	signal correlation_gate_sample_start   	: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal correlation_gate_sample_end     	: STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);

	signal correlation_axis_tready     		: STD_LOGIC;
	
	signal correlation_counter : UNSIGNED(REGISTER_W32-1 downto 0);

	-- CONFIGURATION

	COMPONENT channel_configuration is
	generic (
		CHANNEL_ID		: INTEGER := 0;
		RECEIVER_SIZE	: INTEGER := 6;
		TRANSMITTER_SIZE: INTEGER := 10;
		MAGNET_SIZE		: INTEGER := 5;
		GATES_SIZE		: INTEGER := 42;
		DSP_SIZE		: INTEGER := 12;
        REGISTER_W32    : INTEGER := 32;
        REGISTER_W16    : INTEGER := 16;
		REGISTER_W8     : INTEGER := 8;
		REGISTER_W4     : INTEGER := 4
	);
	port (
	    -- Sync
	    aclk         : in STD_LOGIC;
        aresetn      : in STD_LOGIC;
		
		-- Control
		CONFIG_UPDATE_RECEIVER  	: in STD_LOGIC;
		CONFIG_NEW_RECEIVER   		: out STD_LOGIC;
		
		CONFIG_UPDATE_TRANSMITTER  		: in STD_LOGIC;
		CONFIG_NEW_TRANSMITTER  		: out STD_LOGIC;
		
		CONFIG_UPDATE_MAGNET 	 	: in STD_LOGIC;
		CONFIG_NEW_MAGNET  			: out STD_LOGIC;
		
		CONFIG_UPDATE_DSP_1  	: in STD_LOGIC;
		CONFIG_NEW_DSP_1  		: out STD_LOGIC;
		
		CONFIG_UPDATE_DSP_2  	: in STD_LOGIC;
		CONFIG_NEW_DSP_2  		: out STD_LOGIC;
		
		CONFIG_UPDATE_FIR  	 	: in STD_LOGIC;
		CONFIG_NEW_FIR  		: out STD_LOGIC;
		
		CONFIG_UPDATE_GATES  	: in STD_LOGIC;
		CONFIG_NEW_GATES  		: out STD_LOGIC;
		
		CONFIG_UPDATE_DAC  	 	: in STD_LOGIC;
		CONFIG_NEW_DAC  		: out STD_LOGIC;
		
		THERE_IS_CONFIGURATION   : out STD_LOGIC;
		
		-- Channel enable
		CHANNEL_EN   : out STD_LOGIC;
	
		-- Config input
		CONFIG_INPUT_LOAD	  : in STD_LOGIC;
		CONFIG_INPUT_AXIS_TREADY  : out STD_LOGIC;
		CONFIG_INPUT_AXIS_TDATA	  : in STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		CONFIG_INPUT_AXIS_TLAST	  : in STD_LOGIC;
		CONFIG_INPUT_AXIS_TVALID  : in STD_LOGIC;
		
		-- Config output receiver
		RECEIVER_SAMPLING_FREQUENCY         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        RECEIVER_DATA_WINDOW                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        RECEIVER_DELAY                      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        RECEIVER_ANALOG_GAIN                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  --db*1024
        RECEIVER_DIGITAL_GAIN               : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  --V*1024*1024  
        RECEIVER_EXTERNAL_MULTIPLEXER_EN    : out STD_LOGIC;
		
		-- Config output transmitter
		TRANSMITTER_VOLTAGE                 : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        TRANSMITTER_BURST_FREQUENCY         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        TRANSMITTER_N_CYCLES                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_N_BURST                 : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        TRANSMITTER_PRF                     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_DELAY                   : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_DELTA_DELAY             : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_DELTA_ADD               : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        TRANSMITTER_DIRECTIONAL_PHASING     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        TRANSMITTER_PHASE_SHIFT             : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		-- Config output magnet
		MAGNET_MODE                         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        MAGNET_PULSE_WIDTH                  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        MAGNET_INITIAL_DELAY                : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        MAGNET_RAMP_UP_VOLTAGE              : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        MAGNET_VOLTAGE                      : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		-- Config output DSP
		DSP_AVERAGE                         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        DSP_NOISE_REDUCTION_FILTER          : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        DSP_PHASE_SHIFT                     : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
        DSP_BANDPASS_EN  					: out STD_LOGIC;
		DSP_BANDPASS_N_TAPS  				: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
		DSP_BANDPASS_L  					: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
        DSP_GAUSSIAN_EN  					: out STD_LOGIC;
		DSP_GAUSSIAN_N_TAPS  				: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
		DSP_GAUSSIAN_L  					: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
		DSP_ENVELOPE_EN  					: out STD_LOGIC;
		DSP_ENVELOPE_N_TAPS  				: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
		DSP_ENVELOPE_L  					: out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		DSP_ANALOG_FILTER  					: out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
				
		-- Config output Gates
		GATE1_EN                          : out STD_LOGIC;  
		GATE1_START                       : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		GATE1_WIDTH                       : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);	
        GATE1_AMP_THRESHOLD               : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		GATE1_TOF_ALGORITHM               : out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
		GATE1_TOF_AVG                     : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
		GATE1_TOF_MIN_THICKNESS           : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE1_AMP_THRESHOLD_ALARM_EN      : out STD_LOGIC; 
        GATE1_AMP_THRESHOLD_ALARM_CROSSING: out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE1_TOF_ALARM_EN                : out STD_LOGIC; 
        GATE1_TOF_ALARM_TYPE              : out STD_LOGIC;
        GATE1_TOF_ALARM_CROSSING          : out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE1_TOF_ALARM_MIN_VALUE         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE1_TOF_ALARM_MAX_VALUE         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
				
		GATE2_EN                          : out STD_LOGIC;  
		GATE2_START                       : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		GATE2_WIDTH                       : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);	
        GATE2_AMP_THRESHOLD               : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		GATE2_TOF_ALGORITHM               : out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
		GATE2_TOF_AVG                     : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
		GATE2_TOF_MIN_THICKNESS           : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE2_AMP_THRESHOLD_ALARM_EN      : out STD_LOGIC; 
        GATE2_AMP_THRESHOLD_ALARM_CROSSING: out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE2_TOF_ALARM_EN                : out STD_LOGIC; 
        GATE2_TOF_ALARM_TYPE              : out STD_LOGIC;
        GATE2_TOF_ALARM_CROSSING          : out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE2_TOF_ALARM_MIN_VALUE         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE2_TOF_ALARM_MAX_VALUE         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);		
		
		GATE3_EN                          : out STD_LOGIC;  
		GATE3_START                       : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		GATE3_WIDTH                       : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);	
        GATE3_AMP_THRESHOLD               : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		GATE3_TOF_ALGORITHM               : out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
		GATE3_TOF_AVG                     : out STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
		GATE3_TOF_MIN_THICKNESS           : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
        GATE3_AMP_THRESHOLD_ALARM_EN      : out STD_LOGIC; 
        GATE3_AMP_THRESHOLD_ALARM_CROSSING: out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE3_TOF_ALARM_EN                : out STD_LOGIC; 
        GATE3_TOF_ALARM_TYPE              : out STD_LOGIC;
        GATE3_TOF_ALARM_CROSSING          : out STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
        GATE3_TOF_ALARM_MIN_VALUE         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
        GATE3_TOF_ALARM_MAX_VALUE         : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
		-- Config output FIR
		CONFIG_FIR_AXIS_TREADY    : in STD_LOGIC;
		CONFIG_FIR_AXIS_TDATA	  : out STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
		CONFIG_FIR_AXIS_TLAST	  : out STD_LOGIC;
		CONFIG_FIR_AXIS_TVALID    : out STD_LOGIC;
		
		-- DAC output
		CONFIG_DAC_AXIS_TREADY    : in STD_LOGIC;
		CONFIG_DAC_AXIS_TDATA	  : out STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		CONFIG_DAC_AXIS_TLAST	  : out STD_LOGIC;
		CONFIG_DAC_AXIS_TVALID    : out STD_LOGIC
	);
	end COMPONENT channel_configuration;

	-- Control
	signal config_new_receiver   	: STD_LOGIC;
	signal config_new_transmitter  	: STD_LOGIC;
	signal config_new_magnet  		: STD_LOGIC;
	signal config_new_dsp_1  		: STD_LOGIC;
	signal config_new_dsp_2  		: STD_LOGIC;
	signal config_new_fir  			: STD_LOGIC;
	signal config_new_gates  		: STD_LOGIC;
	signal config_new_dac  			: STD_LOGIC;

	signal there_is_configuration  	: STD_LOGIC;

	-- Config output receiver		
	signal receiver_delay_i                    : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal receiver_data_window_i              : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
	signal receiver_sampling_frequency_i       : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal receiver_digital_gain_i             : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 

	-- Config output transmitter  
	signal transmitter_prf                     : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal transmitter_prf_mrut                : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal transmitter_delay_mrut              : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal transmitter_delay_i                 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	
	signal transmitter_burst_frequency_i       : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	-- 20 Khz
	constant transmitter_burst_frequency_min   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= std_logic_vector(to_unsigned(25000000, transmitter_burst_frequency_i'length));  
	-- 150 Khz
	constant transmitter_burst_frequency_lrut  : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= std_logic_vector(to_unsigned(3333333, transmitter_burst_frequency_i'length));  
	-- 7Mhz
	constant transmitter_burst_frequency_max   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0):= std_logic_vector(to_unsigned(71428, transmitter_burst_frequency_i'length)); 																													  
	signal transmitter_burst_lrut_on  		   : STD_LOGIC;
	signal transmitter_burst_lrut_on_d1  	   : STD_LOGIC;
	signal lrut_wait_flag  		               : STD_LOGIC;
	signal lrut_wait_end 		               : STD_LOGIC;
	signal lrut_on_counter                     : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal lrut_on_counter_pic                 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
			
	-- Config output DSP
	signal dsp_average             		: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
	signal dsp_noise_reduction_filter   : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);  
	signal dsp_bandpass_en  			: STD_LOGIC;
	signal dsp_bandpass_n_taps  		: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
	signal dsp_bandpass_l  				: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
	signal dsp_gaussian_en  			: STD_LOGIC;
	signal dsp_gaussian_n_taps  		: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
	signal dsp_gaussian_l  				: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
	signal dsp_envelope_en  			: STD_LOGIC;
	signal dsp_envelope_n_taps  		: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
	signal dsp_envelope_l  				: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0); 
				
	-- Config output Gates
	signal gate1_en                          : STD_LOGIC;  
	signal gate1_start                       : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal gate1_width                       : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);	
	signal gate1_amp_threshold               : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal gate1_tof_algorithm               : STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
	signal gate1_tof_avg                     : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
	signal gate1_tof_min_thickness           : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal gate1_amp_threshold_alarm_en      : STD_LOGIC; 
	signal gate1_amp_threshold_alarm_crossing: STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
	signal gate1_tof_alarm_en                : STD_LOGIC; 
	signal gate1_tof_alarm_type              : STD_LOGIC;
	signal gate1_tof_alarm_crossing          : STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
	signal gate1_tof_alarm_min_value         : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal gate1_tof_alarm_max_value         : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);	
		
	signal gate2_en                          : STD_LOGIC;  
    signal gate2_start                       : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal gate2_width                       : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);    
    signal gate2_amp_threshold               : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal gate2_tof_algorithm               : STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
    signal gate2_tof_avg                     : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
    signal gate2_tof_min_thickness           : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
    signal gate2_amp_threshold_alarm_en      : STD_LOGIC; 
    signal gate2_amp_threshold_alarm_crossing: STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
    signal gate2_tof_alarm_en                : STD_LOGIC; 
    signal gate2_tof_alarm_type              : STD_LOGIC;
    signal gate2_tof_alarm_crossing          : STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
    signal gate2_tof_alarm_min_value         : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal gate2_tof_alarm_max_value         : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

	signal gate3_en                          : STD_LOGIC;  
	signal gate3_start                       : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal gate3_width                       : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);	
	signal gate3_amp_threshold               : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal gate3_tof_algorithm               : STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
	signal gate3_tof_avg                     : STD_LOGIC_VECTOR(REGISTER_W8-1 downto 0);
	signal gate3_tof_min_thickness           : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal gate3_amp_threshold_alarm_en      : STD_LOGIC; 
	signal gate3_amp_threshold_alarm_crossing: STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
	signal gate3_tof_alarm_en                : STD_LOGIC; 
	signal gate3_tof_alarm_type              : STD_LOGIC;
	signal gate3_tof_alarm_crossing          : STD_LOGIC_VECTOR(REGISTER_W4-1 downto 0);
	signal gate3_tof_alarm_min_value         : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal gate3_tof_alarm_max_value         : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

	signal config_fir_axis_tdata	 : STD_LOGIC_VECTOR(REGISTER_W16-1 downto 0);
	signal config_fir_axis_tlast	 : STD_LOGIC;
	signal config_fir_axis_tvalid    : STD_LOGIC;

	-- CHANNEL
	signal config_update_receiver  		: STD_LOGIC;
	signal config_update_transmitter	: STD_LOGIC;
	signal config_update_magnet 	 	: STD_LOGIC;
	signal config_update_dsp_1  		: STD_LOGIC;
	signal config_update_dsp_2  		: STD_LOGIC;
	signal config_update_fir  	 		: STD_LOGIC;
	signal config_update_gates  		: STD_LOGIC;
	signal config_update_dac  			: STD_LOGIC;

	-- State machines

	-- State machine A
	type states_general_a_sm is (idle, wait_state, trigger_channel, prf_control, trigger_external, trigger_external_clear, wait_stop_motor,
	                             sw_trigger, trigger_encoder, trigger_start, prf_control_mrut, coincidence_average_window, 
								 update_receiver, update_transmitter, update_magnet, update_dsp_1, update_dac, update_control,
								 lrut_wait); 

    signal state_general_a_sm : states_general_a_sm;

	-- State machine B
	type states_general_b_sm is (idle, wait_avg_end, wait_fir_end, wait_gates_end, data_disposition, data_disposition_flag, read_result, read_result_end, 
								 update_dsp_2, update_FIR, update_gates); 

    signal state_general_b_sm : states_general_b_sm;

	-- Read ASCAN state machine
	type states_read_ascan_sm is (idle, no_read_result, read_ascan_start, upload_general_header, upload_ascan_header, upload_ascan_data_flank_down, upload_ascan_data_flank_up, upload_ascan_end); 

    signal state_read_ascan_sm : states_read_ascan_sm;

	signal fir_result_axis_tready_i	: STD_LOGIC;
	signal fir_result_axis_tready_x	: STD_LOGIC;
	signal general_signal_input_axis_tready	: STD_LOGIC;

	signal read_fir_result		: STD_LOGIC;
	signal no_read_fir_result	: STD_LOGIC;
	signal read_gates_result    : STD_LOGIC;
	signal channel_en   		: STD_LOGIC;
	signal ascan_ready_i  	    : STD_LOGIC;

	signal prf_on     	: STD_LOGIC;
	signal prf_counter	: UNSIGNED(REGISTER_W32-1 downto 0);
	signal prf_counter_i: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);

	signal disposition_on_i   : STD_LOGIC;

	signal read_ascan_flag   : STD_LOGIC;
	signal read_ascan_end   : STD_LOGIC;
	
	signal tready_flank_up_flag : STD_LOGIC;
	signal ascan_axis_tdata_1: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal ascan_axis_tdata_2: STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal ascan_axis_tready_i1: STD_LOGIC;
	
	signal ascan_counter: UNSIGNED(REGISTER_W32-1 downto 0);

	signal fir_signal_input_axis_tready  	: STD_LOGIC;
	signal fir_signal_input_axis_tready_i  	: STD_LOGIC;
	signal fir_signal_input_axis_tready_x  	: STD_LOGIC;
	
	signal external_trigger  : STD_LOGIC;
	signal encoder1_pos_i  	 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal encoder1_trigger_counts_i : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal encoder1_trigger_diff : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
		
	signal scan_mode : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal trigger_type : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
	signal trigger_sw : STD_LOGIC;
	signal start_inspection : STD_LOGIC;
	signal start_stop_inspection : STD_LOGIC;
	signal pwm_motor_en_i : STD_LOGIC;

	signal scan_mode_d1 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal trigger_type_d1 : STD_LOGIC_VECTOR(REGISTER_W32-1 downto 0);
    signal start_inspection_d1 : STD_LOGIC;
    signal start_stop_inspection_d1 : STD_LOGIC;

begin

    CHANNEL_ENABLED <= channel_en;
    CONFIG_ON <= there_is_configuration;
	
	--SIGNAL_INPUT_AXIS_TREADY <= coinc_signal_input_axis_tready and general_signal_input_axis_tready;
	SIGNAL_INPUT_AXIS_TREADY <= digital_amplifier_tready and general_signal_input_axis_tready;
						    	
	SCAN_MODE_STATUS <= scan_mode;
	TRIGGER_TYPE_STATUS <= trigger_type;
	START_INSPECTION_STATUS <= start_inspection;
	START_STOP_INSPECTION_STATUS <= start_stop_inspection;
	TRIGGER_SW_STATUS <= trigger_sw;
			
    parameters_process: process(aclk)
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                TRANSMITTER_DELAY <= (others => '0');
				TRANSMITTER_BURST_FREQUENCY	<= (others => '0');	
				transmitter_burst_lrut_on <= '0';																										  
                transmitter_prf_mrut <= (others => '0');
                encoder1_trigger_counts_i <= (others => '0');
                encoder1_trigger_diff <= (others => '0');
                avg_type <= (others => '0');
				pwm_motor_en_i <= '0';																								  
            else
				pwm_motor_en_i <= PWM_MOTOR_EN;
                if(unsigned(scan_mode) = SCAN_MODE_MRUT) then
					-- Transmitter Delay																									  
                    TRANSMITTER_DELAY <= transmitter_delay_mrut;
					-- Transmitter Burst Freq																								  
--					if(unsigned(transmitter_burst_frequency_i) < unsigned(transmitter_burst_frequency_max)) then				
--						TRANSMITTER_BURST_FREQUENCY <= transmitter_burst_frequency_max;	
--						transmitter_burst_lrut_on <= '0';																								  
--					elsif((unsigned(transmitter_burst_frequency_i) >= unsigned(transmitter_burst_frequency_lrut)) and
--					      (unsigned(transmitter_burst_frequency_i) < unsigned(transmitter_burst_frequency_min))) then																									  
--						TRANSMITTER_BURST_FREQUENCY <= transmitter_burst_frequency_i;	
--						transmitter_burst_lrut_on <= '1';																								  
--					elsif(unsigned(transmitter_burst_frequency_i) >= unsigned(transmitter_burst_frequency_min)) then	
--						TRANSMITTER_BURST_FREQUENCY <= transmitter_burst_frequency_min;
--						transmitter_burst_lrut_on <= '1';																									  
--					else
--						TRANSMITTER_BURST_FREQUENCY <= transmitter_burst_frequency_i;		
--						transmitter_burst_lrut_on <= '0';																								  
--					end if;
					TRANSMITTER_BURST_FREQUENCY <= transmitter_burst_frequency_i;	
					-- Averaging type																									  
                    avg_type  <=std_logic_vector(to_signed(AVG_TYPE_NORMAL, avg_type'length));
                else
					-- Transmitter Delay									   
                    TRANSMITTER_DELAY <= transmitter_delay_i;
					-- Transmitter Burst Freq									   
--					if(unsigned(transmitter_burst_frequency_i) < unsigned(transmitter_burst_frequency_max)) then		
--						TRANSMITTER_BURST_FREQUENCY <= transmitter_burst_frequency_max;										   
--					elsif(unsigned(transmitter_burst_frequency_i) >= unsigned(transmitter_burst_frequency_lrut)) then																									  
--						TRANSMITTER_BURST_FREQUENCY <= transmitter_burst_frequency_lrut;																									  																							  
--					else
--						TRANSMITTER_BURST_FREQUENCY <= transmitter_burst_frequency_i;																										  
--					end if;	
					TRANSMITTER_BURST_FREQUENCY <= transmitter_burst_frequency_i;	
					transmitter_burst_lrut_on <= '0';	
					-- Averaging type										   
                    if(unsigned(trigger_type) = TRIGGER_TYPE_SW or unsigned(trigger_type) = TRIGGER_TYPE_SW_PWM) then
                        avg_type  <=std_logic_vector(to_signed(AVG_TYPE_NORMAL, avg_type'length));
                    else
                        avg_type  <=std_logic_vector(to_signed(AVG_TYPE_PIPELINE, avg_type'length));
                    end if;
                end if;     
                transmitter_prf_mrut <= "0" & transmitter_prf(31 downto 1);
                encoder1_trigger_counts_i <= ENCODER1_TRIGGER_COUNTS;
                if(signed(ENCODER1_POS) >= signed(encoder1_pos_i)) then
                    encoder1_trigger_diff <= std_logic_vector(signed(ENCODER1_POS) -  signed(encoder1_pos_i));
                else
                    encoder1_trigger_diff <= std_logic_vector(signed(encoder1_pos_i) -  signed(ENCODER1_POS));
                end if;
            end if;
        end if;
    end process;  
    
    LRUT_EN <= transmitter_burst_lrut_on;
    
    lrut_process: process(aclk)    
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                transmitter_burst_lrut_on_d1 <= '0';
                lrut_wait_flag <= '0';
                lrut_on_counter <= (others => '0');
            else
                transmitter_burst_lrut_on_d1 <= transmitter_burst_lrut_on;
                if(transmitter_burst_lrut_on_d1 /= transmitter_burst_lrut_on) then
                    lrut_wait_flag <= '1';
                    lrut_on_counter <= std_logic_vector(unsigned(lrut_on_counter)+1);
                else
                    if(lrut_wait_end = '1') then
                        lrut_wait_flag <= '0'; 
                    end if;
                end if;
            end if;
        end if;
    end process;
	
	-- State machine for Pulser/receiver - Coinc - Avg
    general_a_sm: process(aclk) 
		variable var_counter : integer range 0 to 999999999 :=0;
		variable mrut_counter : integer range 0 to 65536 :=0;
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				-- config
				config_update_receiver 	  <= '0';
				config_update_transmitter <= '0';
				config_update_magnet 	  <= '0';
				config_update_dsp_1 	  <= '0';
				config_update_dac 		  <= '0';
				TRANSMITTER_START_CYCLE   <= "00";
				-- update_control
				scan_mode <= (others => '0');
				trigger_type <= (others => '0');
				start_inspection <= '0';
				start_stop_inspection <= '0';
				trigger_sw <= '0';
				-- prf 
				prf_on		<= '0';
				prf_counter <= (others => '0');
				prf_counter_i <= (others => '0');
				-- disposition
				disposition_on_i <= '0';
				-- external trigger
				encoder1_pos_i <= (others => '0');
				-- trigger SW
				-- tready
				general_signal_input_axis_tready <= '0';
				-- PWM
				PWM_MOTOR_MOVE <= '0';
				-- LRUT
				lrut_wait_end <= '0';
				lrut_on_counter_pic <= (others => '0');
				-- state
				state_general_a_sm <= idle;
        	else
				if(prf_on = '1') then
					prf_counter <= prf_counter + 1;
				else
					prf_counter <= (others => '0');
				end if;
				case state_general_a_sm is
					when idle =>
						var_counter := 0;
						mrut_counter := 0;
						coinc_counter <= (others => '0');
						general_signal_input_axis_tready <= '0';
						prf_on	<= '0';	
						disposition_on_i   <= '0';
						TRANSMITTER_START_CYCLE   <= "00";
						if(CONFIGURE_CONTROL = '1') then
                            state_general_a_sm <= update_control;
                        elsif(config_new_receiver = '1') then
							state_general_a_sm <= update_receiver;
						elsif(config_new_transmitter = '1') then	
							state_general_a_sm <= update_transmitter;
						elsif(config_new_magnet = '1') then	
							state_general_a_sm <= update_magnet;
						elsif(config_new_dsp_1 = '1') then	
							state_general_a_sm <= update_dsp_1;
--						elsif(config_new_dac = '1') then	
--							state_general_a_sm <= update_dac;
--                        elsif(lrut_wait_flag = '1') then
--                            lrut_on_counter_pic <= lrut_on_counter;
--                            state_general_a_sm <= lrut_wait;
						else
						    trigger_sw <= TRIGGER_SW_CONTROL;
						    start_inspection <= START_INSPECTION_CONTROL;
						    scan_mode <= SCAN_MODE_CONTROL;
						    start_stop_inspection <= START_STOP_INSPECTION_CONTROL;
							if(there_is_configuration = '1' and channel_en = '1') then
							    start_stop_inspection <= START_STOP_INSPECTION_CONTROL;
                                if(start_inspection = '1') then
                                --if(START_INSPECTION_CONTROL = '1') then
									if(unsigned(trigger_type) = TRIGGER_TYPE_NORMAL) then
									    state_general_a_sm <= trigger_start;
									elsif(unsigned(trigger_type) = TRIGGER_TYPE_ROBOT) then
									    if(start_stop_inspection = '1') then
                                            state_general_a_sm <= trigger_channel; 
									    end if;
--										prf_first	<= '1';
--										if(START_STOP_INSPECTION = '1') then
--											prf_on	<= '1';
--											disposition_on_i   <= '1';
--											INSPECTION_INPROGESS <= '1';
--											encoder1_pos_i <= ENCODER1_POS;
--											state_general_a_sm <= trigger_encoder;
--										end if;
                                    elsif(unsigned(trigger_type) = TRIGGER_TYPE_SW) then -- SW trigger
                                        --if(trigger_sw = '1' or TRIGGER_SW_CONTROL = '1') then
                                        if(TRIGGER_SW_CONTROL = '1') then
                                            trigger_sw <= TRIGGER_SW_CONTROL;
                                            state_general_a_sm <= sw_trigger; 
                                        end if;
									elsif(unsigned(trigger_type) = TRIGGER_TYPE_SW_PWM) then -- SW trigger + PWM (encoder)
										if(PWM_MOTOR_EN = '1') then
											if(signed(encoder1_trigger_diff) >= signed(encoder1_trigger_counts_i)) then
											    PWM_MOTOR_MOVE <= '0';
												encoder1_pos_i <= ENCODER1_POS;	
												state_general_a_sm <= wait_stop_motor;
										    else
										        PWM_MOTOR_MOVE <= '1';
											end if;
										else
										    PWM_MOTOR_MOVE <= '0';
											encoder1_pos_i <= ENCODER1_POS;					   
										end if;					   
                                    elsif(unsigned(trigger_type) = TRIGGER_TYPE_ENC) then -- Econder 
										if(signed(encoder1_trigger_diff) >= signed(encoder1_trigger_counts_i)) then
											encoder1_pos_i <= ENCODER1_POS;	
											state_general_a_sm <= trigger_start; 	
										end if;			   
                                    end if;
                                else
                                    if(unsigned(trigger_type) = TRIGGER_TYPE_SW ) then -- SW trigger
                                        --if(trigger_sw = '1' or TRIGGER_SW_CONTROL = '1') then
                                        if(TRIGGER_SW_CONTROL = '1') then
                                            trigger_sw <= TRIGGER_SW_CONTROL;
                                            state_general_a_sm <= sw_trigger; 
                                        end if;
--                                    elsif(unsigned(trigger_type) = TRIGGER_TYPE_ROBOT) then -- Robot
--                                        if(start_stop_inspection = '1') then
--                                            state_general_a_sm <= trigger_channel;
--                                        end if; 
									elsif(unsigned(trigger_type) = TRIGGER_TYPE_SW_PWM) then -- SW trigger + PWM (encoder)
										if(PWM_MOTOR_EN = '1') then
											if(signed(encoder1_trigger_diff) >= signed(encoder1_trigger_counts_i)) then
												PWM_MOTOR_MOVE <= '0';			   
												encoder1_pos_i <= ENCODER1_POS;	
												state_general_a_sm <= wait_stop_motor; 	
											else
												PWM_MOTOR_MOVE <= '1';
											end if;
										else
										    PWM_MOTOR_MOVE <= '0';
											encoder1_pos_i <= ENCODER1_POS;					   
										end if;				   
                                    elsif(unsigned(trigger_type) = TRIGGER_TYPE_ENC) then -- Econder 
										if(signed(encoder1_trigger_diff) >= signed(encoder1_trigger_counts_i)) then
											encoder1_pos_i <= ENCODER1_POS;	
											state_general_a_sm <= trigger_start; 	
										end if;		   
									end if;
								end if;
							end if;
						end if;
					when update_control => 
					   scan_mode <= SCAN_MODE_CONTROL;
                       trigger_type <= TRIGGER_TYPE_CONTROL;
                       start_inspection <= START_INSPECTION_CONTROL;
                       start_stop_inspection <= START_STOP_INSPECTION_CONTROL;
                       --trigger_sw <= TRIGGER_SW_CONTROL;
                       if(unsigned(TRIGGER_TYPE_CONTROL) = TRIGGER_TYPE_ENC or
                          unsigned(TRIGGER_TYPE_CONTROL) = TRIGGER_TYPE_SW_PWM) then 
                          --encoder1_pos_i <= ENCODER1_POS;
                          encoder1_pos_i <= (others => '0');
                       end if; 
					   if(CONFIGURE_CONTROL = '0') then
					       state_general_a_sm <= idle;
					   end if;
					when wait_state =>
						if(var_counter >= 128) then
							state_general_a_sm <= idle;		
						else
							var_counter := var_counter + 1;	
						end if;
				    when lrut_wait =>
				        if(var_counter >= LRUT_WAIT_CYCLES) then
				            if(lrut_wait_flag = '0') then
				                lrut_wait_end <= '0';
				                state_general_a_sm <= idle;
				            else
				                if(lrut_on_counter_pic = lrut_on_counter) then
				                    lrut_wait_end <= '1';
				                else
				                    lrut_on_counter_pic <= lrut_on_counter;
				                    var_counter := 0;    
				                end if;
				            end if;
				        else
				            var_counter := var_counter + 1;
				        end if;
					when trigger_channel =>
					    prf_on	<= '1';
						general_signal_input_axis_tready <= '1';
						coinc_counter <= (others => '0');
                        state_general_a_sm <= coincidence_average_window;
                    when prf_control =>
                        if(prf_counter >= unsigned(transmitter_prf)) then
                            prf_counter_i <= std_logic_vector(prf_counter);
                            state_general_a_sm <= idle;
                        else
                            prf_counter_i <= transmitter_prf; 
                        end if;
                    when prf_control_mrut =>
                        if(prf_counter >= unsigned(transmitter_prf_mrut)) then
                            prf_counter_i <= std_logic_vector(prf_counter);
                            if(mrut_counter >= 2) then
                                state_general_a_sm <= idle;
                            else
                                prf_on	<= '0';	
                                state_general_a_sm <= trigger_start;
                            end if;
                        else
                            prf_counter_i <= transmitter_prf_mrut;    
                        end if;
					when sw_trigger =>
					   trigger_sw <= TRIGGER_SW_CONTROL;
					   if(trigger_sw = '0') then
					       state_general_a_sm <= trigger_start;
					   end if;
--                    when trigger_encoder => -- Not used for the momment
--                       if(scan_mode /= SCAN_MODE_CONTROL or
--                           start_inspection /= START_INSPECTION_CONTROL or
--                           trigger_type /= TRIGGER_TYPE_CONTROL or
--						   pwm_motor_en_i /= PWM_MOTOR_EN) then
--                           state_general_a_sm <= idle;       
--                       else
--                           if(signed(encoder1_trigger_diff) >= signed(encoder1_trigger_counts_i)) then
--                               state_general_a_sm <= trigger_start;
--                           end if;
--                       end if; 
                    when wait_stop_motor =>
                        -- 100 ms
						if(var_counter >= MOTOR_STOP_CYCLES) then  
							var_counter := 0;								   
                            state_general_a_sm <= trigger_start;        
                        else
                            var_counter := var_counter + 1;    
                        end if;
				    when trigger_start =>    
                       prf_on    <= '1';
                       general_signal_input_axis_tready <= '1';
                       if(unsigned(scan_mode) = SCAN_MODE_MRUT) then
                           if(CHANNEL_ID = 0) then
                               if(mrut_counter = 0) then
                                   transmitter_delay_mrut <= (others => '0');
                                   TRANSMITTER_START_CYCLE   <= "00";
                               else
                                   transmitter_delay_mrut <= transmitter_delay_i;
                                   TRANSMITTER_START_CYCLE   <= "01";
                               end if;
                           else
                               if(mrut_counter = 0) then
                                   transmitter_delay_mrut <= transmitter_delay_i;
                                   TRANSMITTER_START_CYCLE   <= "01";
                               else
                                   transmitter_delay_mrut <= (others => '0');
                                   TRANSMITTER_START_CYCLE   <= "00";
                               end if;
                           end if;    
                       end if;
                       coinc_counter <= (others => '0');
                       state_general_a_sm <= coincidence_average_window;                                 
					when trigger_external =>
					    prf_on	<= '1';
						if(signed(encoder1_trigger_diff) >= signed(encoder1_trigger_counts_i)) then
						    coinc_counter <= (others => '0');
							general_signal_input_axis_tready <= '1';
							state_general_a_sm <= coincidence_average_window;
						end if;
					when trigger_external_clear =>
					    start_stop_inspection <= START_STOP_INSPECTION_CONTROL;
					    prf_counter_i <= std_logic_vector(prf_counter);
						if(START_STOP_INSPECTION_CONTROL = '0' and CONFIGURE_CONTROL = '0') then
							prf_on	<= '0';
							disposition_on_i   <= '0';
							state_general_a_sm <= idle;
						end if;  
					when coincidence_average_window =>
						-- Coinc
						if(coinc_result_axis_tlast = '1') then
							coinc_counter <= coinc_counter + 1;
--							if(coinc_counter >= unsigned(dsp_average)-1) then
--								general_signal_input_axis_tready <= '0';	
--							end if;
                            if(unsigned(avg_type) = AVG_TYPE_PIPELINE) then
							    general_signal_input_axis_tready <= '0';	
							else
                                if(coinc_counter >= unsigned(dsp_average)-1) then
                                    general_signal_input_axis_tready <= '0';    
                                end if;
							end if;						   
						end if;
						-- Average
						if(avg_result_axis_tlast = '1') then
							general_signal_input_axis_tready <= '0';
							if(unsigned(trigger_type) = TRIGGER_TYPE_NORMAL) then
							    if(unsigned(scan_mode) = SCAN_MODE_MRUT) then
							        mrut_counter := mrut_counter + 1;
                                    state_general_a_sm <= prf_control_mrut;
                                else
                                    state_general_a_sm <= prf_control;
                                end if;
                            elsif(  (unsigned(trigger_type) = TRIGGER_TYPE_SW) or
                                    (unsigned(trigger_type) = TRIGGER_TYPE_SW_PWM) or 
                                    (unsigned(trigger_type) = TRIGGER_TYPE_ENC) ) then
                                if(unsigned(scan_mode) = SCAN_MODE_MRUT) then
                                    mrut_counter := mrut_counter + 1;
                                    state_general_a_sm <= prf_control_mrut;
                                else
                                    prf_counter_i <= std_logic_vector(prf_counter);
                                    state_general_a_sm <= idle;
                                end if;
                            elsif(unsigned(trigger_type) = TRIGGER_TYPE_ROBOT) then
                                state_general_a_sm <= prf_control;    
                            end if;
						end if;
					when update_receiver =>
						if(config_new_receiver = '0') then
							config_update_receiver <= '0';	
							state_general_a_sm <= idle;											  
						else
							config_update_receiver <= '1';												  
						end if;	
					when update_transmitter =>
						if(config_new_transmitter = '0') then
							config_update_transmitter <= '0';	
							state_general_a_sm <= idle;											  
						else
							config_update_transmitter <= '1';												  
						end if;		
					when update_magnet =>
						if(config_new_magnet = '0') then
							config_update_magnet <= '0';	
							state_general_a_sm <= idle;											  
						else
							config_update_magnet <= '1';												  
						end if;		
					when update_dsp_1 =>
						if(config_new_dsp_1 = '0') then
							config_update_dsp_1 <= '0';	
							state_general_a_sm <= idle;											  
						else
							config_update_dsp_1 <= '1';												  
						end if;
					when update_dac =>
						if(config_new_dac = '0') then
							config_update_dac <= '0';	
							state_general_a_sm <= idle;											  
						else
							config_update_dac <= '1';												  
						end if;		
					when others =>
						null;
				end case;
			end if;
         end if;
    end process;
    
    CHANNEL_STATE_A <= std_logic_vector(to_unsigned(0, CHANNEL_STATE_A'length));
    CHANNEL_STATE_B <= std_logic_vector(to_unsigned(0, CHANNEL_STATE_B'length));
    CHANNEL_STATE_C <= std_logic_vector(to_unsigned(0, CHANNEL_STATE_C'length));
       
--    CHANNEL_STATE_A <= std_logic_vector(to_unsigned(0, CHANNEL_STATE_A'length)) when state_general_a_sm = idle else
--                       std_logic_vector(to_unsigned(1, CHANNEL_STATE_A'length)) when state_general_a_sm = wait_state else
--                       std_logic_vector(to_unsigned(2, CHANNEL_STATE_A'length)) when state_general_a_sm = trigger_channel else
--                       std_logic_vector(to_unsigned(3, CHANNEL_STATE_A'length)) when state_general_a_sm = prf_control else
--                       std_logic_vector(to_unsigned(4, CHANNEL_STATE_A'length)) when state_general_a_sm = trigger_external else
--                       std_logic_vector(to_unsigned(5, CHANNEL_STATE_A'length)) when state_general_a_sm = trigger_external_clear else
--                       --std_logic_vector(to_unsigned(6, CHANNEL_STATE_A'length)) when state_general_a_sm = sw_trigger_clear else
--                       std_logic_vector(to_unsigned(7, CHANNEL_STATE_A'length)) when state_general_a_sm = coincidence_average_window else
--                       std_logic_vector(to_unsigned(8, CHANNEL_STATE_A'length)) when state_general_a_sm = update_receiver else
--                       std_logic_vector(to_unsigned(9, CHANNEL_STATE_A'length)) when state_general_a_sm = update_transmitter else
--                       std_logic_vector(to_unsigned(10, CHANNEL_STATE_A'length)) when state_general_a_sm = update_magnet else
--                       std_logic_vector(to_unsigned(11, CHANNEL_STATE_A'length)) when state_general_a_sm = update_dsp_1 else
--                       std_logic_vector(to_unsigned(12, CHANNEL_STATE_A'length)) when state_general_a_sm = update_dac else
--                       std_logic_vector(to_unsigned(13, CHANNEL_STATE_A'length)) when state_general_a_sm = update_control else
--                       std_logic_vector(to_unsigned(14, CHANNEL_STATE_A'length));
                       
--    CHANNEL_STATE_B <= std_logic_vector(to_unsigned(0, CHANNEL_STATE_B'length)) when state_general_b_sm = idle else
--                       std_logic_vector(to_unsigned(1, CHANNEL_STATE_B'length)) when state_general_b_sm = wait_avg_end else
--                       std_logic_vector(to_unsigned(2, CHANNEL_STATE_B'length)) when state_general_b_sm = wait_fir_end else
--                       std_logic_vector(to_unsigned(3, CHANNEL_STATE_B'length)) when state_general_b_sm = wait_gates_end else
--                       std_logic_vector(to_unsigned(4, CHANNEL_STATE_B'length)) when state_general_b_sm = data_disposition else
--                       std_logic_vector(to_unsigned(5, CHANNEL_STATE_B'length)) when state_general_b_sm = data_disposition_flag else
--                       std_logic_vector(to_unsigned(6, CHANNEL_STATE_B'length)) when state_general_b_sm = read_result else
--                       std_logic_vector(to_unsigned(7, CHANNEL_STATE_B'length)) when state_general_b_sm = read_result_end else
--                       std_logic_vector(to_unsigned(8, CHANNEL_STATE_B'length)) when state_general_b_sm = update_dsp_2 else
--                       std_logic_vector(to_unsigned(9, CHANNEL_STATE_B'length)) when state_general_b_sm = update_FIR else
--                       std_logic_vector(to_unsigned(10, CHANNEL_STATE_B'length));
                       
--    CHANNEL_STATE_C <= std_logic_vector(to_unsigned(0, CHANNEL_STATE_C'length)) when state_read_ascan_sm = idle else
--                       std_logic_vector(to_unsigned(1, CHANNEL_STATE_C'length)) when state_read_ascan_sm = no_read_result else
--                       std_logic_vector(to_unsigned(2, CHANNEL_STATE_C'length)) when state_read_ascan_sm = read_ascan_start else
--                       std_logic_vector(to_unsigned(3, CHANNEL_STATE_C'length)) when state_read_ascan_sm = upload_general_header else
--                       std_logic_vector(to_unsigned(4, CHANNEL_STATE_C'length)) when state_read_ascan_sm = upload_ascan_header else
--                       std_logic_vector(to_unsigned(5, CHANNEL_STATE_C'length)) when state_read_ascan_sm = upload_ascan_data_flank_down else
--                       std_logic_vector(to_unsigned(6, CHANNEL_STATE_C'length)) when state_read_ascan_sm = upload_ascan_data_flank_up else
--                       std_logic_vector(to_unsigned(7, CHANNEL_STATE_C'length)) when state_read_ascan_sm = upload_ascan_end else
--                       std_logic_vector(to_unsigned(8, CHANNEL_STATE_C'length));
                       			 
	fir_signal_input_axis_tready_i <= fir_signal_input_axis_tready and fir_signal_input_axis_tready_x;
			 			 
	-- State machine for FIR/CORR - GATES
    general_b_sm: process(aclk) 
		variable var_counter : integer range 0 to 65536 :=0;
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				-- Disposition
				DISPOSITION_BITS		<= (others => '0');
				DISPOSITION				<= '0';
				-- Read gates
				read_gates_result		<= '0';
				-- Config update
				config_update_dsp_2 	<= '0';
				config_update_fir 		<= '0';
				config_update_gates 	<= '0';
				-- Read ascan
				read_ascan_flag   		<= '0';
				-- FIR tready
				fir_signal_input_axis_tready_x <= '0';
				-- state
				state_general_b_sm <= idle;
        	else
				case state_general_b_sm is
					when idle =>
					    if(avg_result_axis_tlast = '1') then
					        fir_signal_input_axis_tready_x <= '0';
                            state_general_b_sm <= wait_fir_end;
                        elsif(avg_result_axis_tvalid = '1') then
                            fir_signal_input_axis_tready_x <= '1';
                            state_general_b_sm <= wait_avg_end;
						elsif(config_new_dsp_2 = '1') then
						    fir_signal_input_axis_tready_x <= '0';
							state_general_b_sm <= update_dsp_2;
						elsif(config_new_fir = '1') then
						    fir_signal_input_axis_tready_x <= '0';
							state_general_b_sm <= update_FIR;
						elsif(config_new_gates = '1') then
						    fir_signal_input_axis_tready_x <= '0';
							state_general_b_sm <= update_gates;
						else
							fir_signal_input_axis_tready_x <= '1';
						end if;
				    when wait_avg_end =>
                        if(avg_result_axis_tlast = '1') then
                            state_general_b_sm <= wait_fir_end;            
                        end if;				    
					when wait_fir_end =>
					    fir_signal_input_axis_tready_x <= '0';
						if(fir_end = '1') then
							fir_signal_input_axis_tready_x <= '0';
							state_general_b_sm <= wait_gates_end;	
						end if;
					when wait_gates_end =>
						if(gates_en = '1') then
							if(gates_end = '1') then
								read_gates_result	<= '1';
								if(disposition_on_i = '1') then
									state_general_b_sm <= data_disposition;	
								else
									state_general_b_sm <= read_result;	
								end if;
							end if;
						else
							state_general_b_sm <= data_disposition;	
						end if;
					when data_disposition =>
						if(gates_en = '0') then
							DISPOSITION_BITS <= std_logic_vector(to_unsigned(DISPOSITION_NODET, DISPOSITION_BITS'length));
						else
							if(unsigned(GATE2_TOF) >= unsigned(GATE2_TOF_ALARM_MIN_VALUE) and unsigned(GATE2_TOF) <= unsigned(GATE2_TOF_ALARM_MAX_VALUE)) then
								DISPOSITION_BITS <= std_logic_vector(to_unsigned(DISPOSITION_PASS, DISPOSITION_BITS'length));
							else
								DISPOSITION_BITS <= std_logic_vector(to_unsigned(DISPOSITION_FAIL, DISPOSITION_BITS'length));	
							end if;
						end if;
						state_general_b_sm <= data_disposition_flag;																			 
					when data_disposition_flag =>																
						--if(disposition_on_i = '0') then
							--DISPOSITION	<= '0';													 
							state_general_b_sm <= read_result;
						--else
							--DISPOSITION	<= '1';													 
						--end if;
					when read_result =>
						read_gates_result	<= '0';															 
						--DISPOSITION_BITS <= (others => '0');																 
						if(read_ascan_end = '1') then
							read_ascan_flag <= '0';	
							state_general_b_sm <= idle;												 
						else
							read_ascan_flag <= '1';													 													 
						end if;																 
					when update_dsp_2 =>
						if(config_new_dsp_2 = '0') then
							config_update_dsp_2 <= '0';	
							state_general_b_sm <= idle;											  
						else
							config_update_dsp_2 <= '1';												  
						end if;	
					when update_FIR =>
						if(config_new_fir = '0') then
							config_update_fir <= '0';	
							state_general_b_sm <= idle;											  
						else
							config_update_fir <= '1';												  
						end if;		
					when update_gates =>
						if(config_new_gates = '0') then
							config_update_gates <= '0';	
							state_general_b_sm <= idle;											  
						else
							config_update_gates <= '1';												  
						end if;			
					when others =>
						null;
				end case;
			end if;
         end if;
    end process;
																			 
	fir_result_axis_tready_i <= fir_result_axis_tready_x and ASCAN_AXIS_TREADY;
	
	ASCAN_READY <= ascan_ready_i;																		 
																			 																						 
	-- State machine for upload ASCAN
    upload_ascan_sm: process(aclk) 
		variable var_ascan_counter : integer range 0 to 65536 :=0;
		variable var_wait_counter : integer range 0 to 65536 :=0;																			 
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
				ascan_ready_i <= '0';	
				ASCAN_SIZE <= (others => '0');															 
				read_fir_result <= '0';
				no_read_fir_result 	<= '0';																 
				ASCAN_AXIS_TDATA <= (others => '0');
				ASCAN_AXIS_TKEEP <= (others => '0');
				ASCAN_AXIS_TVALID <= '0';
				ASCAN_AXIS_TLAST <= '0';
				fir_result_axis_tready_x <= '0';		
				tready_flank_up_flag <= '0';
				ascan_axis_tdata_1 <= (others => '0');	 
				ascan_axis_tdata_2 <= (others => '0');																	 
				ascan_axis_tready_i1 <= '0';
				read_ascan_end <= '0';	 
				ascan_counter <= (others => '0');													 
				-- state
				state_read_ascan_sm <= idle;
        	else
				ascan_axis_tready_i1 <= ASCAN_AXIS_TREADY;															 
				case state_read_ascan_sm is
					when idle =>
						fir_result_axis_tready_x <= '0';
						read_ascan_end <= '0';															 
						if(read_ascan_flag = '1') then			
							ASCAN_SIZE <= std_logic_vector(to_unsigned(ASCAN_HEADER_SIZE, ASCAN_SIZE'length) + 
							                               resize(unsigned(fir_data_window_half_result), ASCAN_SIZE'length));
							state_read_ascan_sm <= read_ascan_start;													 
						end if;	
					when read_ascan_start =>
						var_ascan_counter:= 0;										  
						if(READ_ASCAN = '1') then
							ascan_ready_i <= '0';	
							-- Prepare read from FIR
							read_fir_result <= '1';
							ASCAN_AXIS_TKEEP <= (others => '1');
							ascan_counter <= (others => '0');
							if(ASCAN_AXIS_TREADY = '1') then
							    state_read_ascan_sm <= upload_general_header;
							end if;
						else
						    if( (unsigned(trigger_type) = TRIGGER_TYPE_SW) or
                                (unsigned(trigger_type) = TRIGGER_TYPE_ENC)) then
                                ascan_ready_i <= '1';	    
                            else
							    if(start_inspection = '0') then
							        if(ascan_ready_i = '0') then
								        state_read_ascan_sm <= no_read_result;
						            end if;
							    else
								    ascan_ready_i <= '1';										  
							    end if;															 
						    end if;
				        end if;													 
					when no_read_result =>		
						if(var_ascan_counter >= WAIT_CYCLES) then
							if(read_ascan_flag = '0') then
							    no_read_fir_result 	<= '0';												 
								state_read_ascan_sm <= idle;
						    else
						        read_ascan_end <= '1';    
							end if;
						else
							no_read_fir_result 	<= '1';
							var_ascan_counter := var_ascan_counter + 1;
						end if;																 												 
					when upload_general_header =>
						if(ASCAN_AXIS_TREADY = '1') then
						    ascan_counter <= ascan_counter + 1;
							ASCAN_AXIS_TVALID <= '1';													 
							if(var_ascan_counter = 0) then	    -- version
								ASCAN_AXIS_TDATA <= std_logic_vector(to_unsigned(0, ASCAN_AXIS_TDATA'length));	
							elsif(var_ascan_counter = 1) then	-- scan mode										 
                                ASCAN_AXIS_TDATA <= scan_mode;
                            elsif(var_ascan_counter = 2) then   -- trigger type
                                ASCAN_AXIS_TDATA <= trigger_type;		
                            elsif(var_ascan_counter = 3) then   -- scan state
                                ASCAN_AXIS_TDATA(0) <= start_inspection;
                                ASCAN_AXIS_TDATA(1) <= start_stop_inspection;
                                ASCAN_AXIS_TDATA(2) <= not PWM_END;
                                ASCAN_AXIS_TDATA(31 downto 3) <= (others => '0');						 
							elsif(var_ascan_counter = 4) then	-- number of channels											 
								ASCAN_AXIS_TDATA <= std_logic_vector(to_unsigned(1, ASCAN_AXIS_TDATA'length));
							elsif(var_ascan_counter = 5) then	-- channel offset (var_ascan_counter + 1)
								ASCAN_AXIS_TDATA <= std_logic_vector(to_unsigned(6, ASCAN_AXIS_TDATA'length));
							end if;
							if(var_ascan_counter >= 5) then
								var_ascan_counter:= 0;
								state_read_ascan_sm <= upload_ascan_header;													 
							else													 
								var_ascan_counter:= var_ascan_counter+1;		
							end if;															 
						end if;
					when upload_ascan_header =>
						if(ASCAN_AXIS_TREADY = '1') then
						    ascan_counter <= ascan_counter + 1;
							ASCAN_AXIS_TVALID <= '1';													 
							if(var_ascan_counter = 0) then		    -- channel_id
								ASCAN_AXIS_TDATA <= std_logic_vector(to_unsigned(CHANNEL_ID, ASCAN_AXIS_TDATA'length));												 
							elsif(var_ascan_counter = 1) then		-- clock_rate											 
								ASCAN_AXIS_TDATA <= fir_sample_freq_result;
							elsif(var_ascan_counter = 2) then		-- prf
								ASCAN_AXIS_TDATA <= prf_counter_i;															 													 
							elsif(var_ascan_counter = 3) then		-- ascan size
								ASCAN_AXIS_TDATA <= fir_data_window_result;	
							elsif(var_ascan_counter = 4) then		-- ascan offset
								ASCAN_AXIS_TDATA <= std_logic_vector(to_unsigned(43, ASCAN_AXIS_TDATA'length));
																				 
							elsif(var_ascan_counter = 5) then		-- gate1 tof
								ASCAN_AXIS_TDATA <= gate1_tof;	
							elsif(var_ascan_counter = 6) then		-- gate1 tof avg
								ASCAN_AXIS_TDATA <= gate1_tof_averaged;	
							elsif(var_ascan_counter = 7) then		-- gate1 amp alarm
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(gate1_amp_alarm), ASCAN_AXIS_TDATA'length));	
							elsif(var_ascan_counter = 8) then		-- gate1 tof alarm
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(gate1_tof_alarm), ASCAN_AXIS_TDATA'length));	
							elsif(var_ascan_counter = 9) then		-- gate1 amp max
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(signed(gate1_max), ASCAN_AXIS_TDATA'length));	
							elsif(var_ascan_counter = 10) then	-- gate1 amp max sample
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(gate1_max_sample), ASCAN_AXIS_TDATA'length));		
							elsif(var_ascan_counter = 11) then	-- gate1 amp min
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(signed(gate1_min), ASCAN_AXIS_TDATA'length));		
							elsif(var_ascan_counter = 12) then	-- gate1 amp min sample
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(gate1_min_sample), ASCAN_AXIS_TDATA'length));	
																				 
							elsif(var_ascan_counter = 13) then	-- gate2 tof
								ASCAN_AXIS_TDATA <= gate2_tof;	
							elsif(var_ascan_counter = 14) then	-- gate2 tof avg
								ASCAN_AXIS_TDATA <= gate2_tof_averaged;	
							elsif(var_ascan_counter = 15) then	-- gate2 amp alarm
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(gate2_amp_alarm), ASCAN_AXIS_TDATA'length));		
							elsif(var_ascan_counter = 16) then	-- gate2 tof alarm
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(gate2_tof_alarm), ASCAN_AXIS_TDATA'length));		
							elsif(var_ascan_counter = 17) then	-- gate2 amp max
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(signed(gate2_max), ASCAN_AXIS_TDATA'length));	
							elsif(var_ascan_counter = 18) then	-- gate2 amp max sample
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(gate2_max_sample), ASCAN_AXIS_TDATA'length));		
							elsif(var_ascan_counter = 19) then	-- gate2 amp min
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(signed(gate2_min), ASCAN_AXIS_TDATA'length));	
							elsif(var_ascan_counter = 20) then	-- gate2 amp min sample
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(gate2_min_sample), ASCAN_AXIS_TDATA'length));		
																				 
							elsif(var_ascan_counter = 21) then	-- gate3 tof
								ASCAN_AXIS_TDATA <= gate3_tof;	
							elsif(var_ascan_counter = 22) then	-- gate3 tof avg
								ASCAN_AXIS_TDATA <= gate3_tof_averaged;	
							elsif(var_ascan_counter = 23) then	-- gate3 amp alarm
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(gate3_amp_alarm), ASCAN_AXIS_TDATA'length));	
							elsif(var_ascan_counter = 24) then	-- gate3 tof alarm
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(gate3_tof_alarm), ASCAN_AXIS_TDATA'length));
							elsif(var_ascan_counter = 25) then	-- gate3 amp max
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(signed(gate3_max), ASCAN_AXIS_TDATA'length));	
							elsif(var_ascan_counter = 26) then	-- gate3 amp max sample
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(gate3_max_sample), ASCAN_AXIS_TDATA'length));		
							elsif(var_ascan_counter = 27) then	-- gate3 amp min
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(signed(gate3_min), ASCAN_AXIS_TDATA'length));	
							elsif(var_ascan_counter = 28) then	-- gate3 amp min sample
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(gate3_min_sample), ASCAN_AXIS_TDATA'length));
																				 
							elsif(var_ascan_counter = 29) then	-- encoder1 position 
								ASCAN_AXIS_TDATA <= ENCODER1_POS;
							elsif(var_ascan_counter = 30) then	-- encoder1 direction 
                                ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(ENCODER1_DIR), ASCAN_AXIS_TDATA'length));
                            elsif(var_ascan_counter = 31) then  -- encoder1 rpm 
                                ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(ENCODER1_RPM), ASCAN_AXIS_TDATA'length));
								
							elsif(var_ascan_counter = 32) then	-- encoder2 position
								ASCAN_AXIS_TDATA <= ENCODER2_POS;
							elsif(var_ascan_counter = 33) then	-- encoder2 direction
                                ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(ENCODER2_DIR), ASCAN_AXIS_TDATA'length));
                                -- Synqcronyze with FIR												
                                fir_result_axis_tready_x <= '1';
                            elsif(var_ascan_counter = 34) then  -- encoder2 rpm 
                                ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(ENCODER2_RPM), ASCAN_AXIS_TDATA'length));    
											 																											 
							elsif(var_ascan_counter = 35) then	-- inputs
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(IO_INPUTS), ASCAN_AXIS_TDATA'length));												
							elsif(var_ascan_counter = 36) then	-- outputs
								ASCAN_AXIS_TDATA <= std_logic_vector(resize(unsigned(IO_OUTPUTS), ASCAN_AXIS_TDATA'length));												 													 
							end if;
																				 
							if(var_ascan_counter >= 36) then
								var_ascan_counter:= 0;						
								state_read_ascan_sm <= upload_ascan_data_flank_down;													 
							else													 
								var_ascan_counter:= var_ascan_counter+1;		
							end if;														 
						end if;												 
					when upload_ascan_data_flank_down =>															 														
                        if(ASCAN_AXIS_TREADY = '1') then
                            ascan_counter <= ascan_counter + 1;
							ASCAN_AXIS_TVALID <= '1';													
                            if(tready_flank_up_flag = '1') then
                                ASCAN_AXIS_TDATA <= ascan_axis_tdata_2;
                                tready_flank_up_flag <= '0';
                            else
                                ASCAN_AXIS_TDATA <= fir_result_axis_tdata;
                            end if;
							if(var_ascan_counter >= to_integer(unsigned(fir_data_window_half_result))-1) then
								ASCAN_AXIS_TLAST <= '1';
								state_read_ascan_sm <= upload_ascan_end;												
							else
								var_ascan_counter := var_ascan_counter + 1;													
							end if;
                        else
                            if(ascan_axis_tready_i1 = '1') then
                                ascan_axis_tdata_1 <= fir_result_axis_tdata;
                                var_wait_counter:= 0;
                                state_read_ascan_sm <= upload_ascan_data_flank_up;
                            end if;
                        end if;																														
					when upload_ascan_data_flank_up =>																									
                        if(var_wait_counter = 0) then
                            ascan_axis_tdata_2 <= fir_result_axis_tdata;
                        end if;
                        if(var_wait_counter < 4) then
                            var_wait_counter := var_wait_counter + 1;
                        end if;
                        if(ASCAN_AXIS_TREADY = '1' and ascan_axis_tready_i1 = '0') then
                            ascan_counter <= ascan_counter + 1;
							ASCAN_AXIS_TVALID <= '1';													
                            if(var_wait_counter = 0) then
                                ASCAN_AXIS_TDATA <= fir_result_axis_tdata;
                            else
                                ASCAN_AXIS_TDATA <= ascan_axis_tdata_1;
                            end if;      
                            tready_flank_up_flag <= '1';
							if(var_ascan_counter >= to_integer(unsigned(fir_data_window_half_result))-1) then
								ASCAN_AXIS_TLAST <= '1';
								state_read_ascan_sm <= upload_ascan_end;												
							else
								var_ascan_counter := var_ascan_counter + 1;		
								state_read_ascan_sm <= upload_ascan_data_flank_down;													
							end if;														
                        end if;											
					when upload_ascan_end => 																 
						ASCAN_AXIS_TLAST <= '0';	
						ASCAN_AXIS_TVALID <= '0';
						ASCAN_AXIS_TKEEP <= (others => '0');
						read_fir_result <= '0';														
						read_ascan_end <= '1';	
						tready_flank_up_flag <= '0';													
						if(READ_ASCAN = '0' and read_ascan_flag = '0') then			
							state_read_ascan_sm <= idle;													
						end if;															
					when others =>
						null;
				end case;
			end if;
         end if;
    end process;
																			
	-- DIGITAL AMPLIFIER																		
	digital_amplifier_inst: digital_amplifier
	generic map(
		REGISTER_W48        => REGISTER_W48,
        REGISTER_W32        => REGISTER_W32,
        REGISTER_W16        => REGISTER_W16,
		DIVIDER_FACTOR      => 20,
		MULTIPLIER_DELAY    => 4
    )
    port map(
        aclk         => aclk,
        aresetn      => aresetn,
		
		-- Gain
		DIGITAL_AMPLIFIER_GAIN	=> receiver_digital_gain_i,
		
		-- Signal in
		SIGNAL_INPUT_AXIS_TREADY 	=> digital_amplifier_tready,
		SIGNAL_INPUT_AXIS_TDATA 	=> SIGNAL_INPUT_AXIS_TDATA,
		SIGNAL_INPUT_AXIS_TLAST		=> SIGNAL_INPUT_AXIS_TLAST,
		SIGNAL_INPUT_AXIS_TVALID  	=> SIGNAL_INPUT_AXIS_TVALID,

		-- Signal out
		SIGNAL_OUTPUT_AXIS_TREADY 	=> coinc_signal_input_axis_tready,
		SIGNAL_OUTPUT_AXIS_TDATA	=> signal_output_axis_tdata_i,
		SIGNAL_OUTPUT_AXIS_TLAST	=> signal_output_axis_tlast_i,
		SIGNAL_OUTPUT_AXIS_TVALID   => signal_output_axis_tvalid_i		
    );
																				 			 						
	-- COINCIDENCE FILTER
	coincidence_filter_inst: coincidence_filter
	generic map(
        REGISTER_W32    => REGISTER_W32,
        REGISTER_W16    => REGISTER_W16,
        REGISTER_W8     => REGISTER_W8,
        TRIGGER_WAIT    => TRIGGER_WAIT,
        MIN_COINC_LEVEL => MIN_COINC_LEVEL
	)
	port map(
	    -- Sync
	    aclk         => aclk,
        aresetn      => aresetn,
	
	    -- Config
	    COINC_LEVEL	    => dsp_noise_reduction_filter,
		
		PHASE_SHIFT_LEVEL     => dsp_phase_shift,
        PHASE_SHIFT           => transmitter_phase_shift,
        N_BURST               => transmitter_n_burst_i,
		
	    DATA_WINDOW	    => RECEIVER_DATA_WINDOW_i,
	    SAMPLE_FREQ     => receiver_sampling_frequency_i,
		DATA_WINDOW_RESULT	=> coinc_data_window_result,
		SAMPLE_FREQ_RESULT  => coinc_sample_freq_result,
		COINC_LEVEL_RESULT  => coinc_level_result, 
		COINC_COUNTER   => SIGNAL_COINC_COUNTER,
		COINC_WINDOW    => SIGNAL_COINC_WINDOW, 

		-- Signal input
		SIGNAL_INPUT_AXIS_TREADY  => coinc_signal_input_axis_tready,
		SIGNAL_INPUT_AXIS_TDATA	  => signal_output_axis_tdata_i,
		SIGNAL_INPUT_AXIS_TLAST	  => signal_output_axis_tlast_i,
		SIGNAL_INPUT_AXIS_TVALID  => signal_output_axis_tvalid_i,

		-- Coincidence result
		COINC_RESULT_AXIS_TVALID  => coinc_result_axis_tvalid,
		COINC_RESULT_AXIS_TDATA	  => coinc_result_axis_tdata,
		COINC_RESULT_AXIS_TLAST	  => coinc_result_axis_tlast,
		COINC_RESULT_AXIS_TREADY  => avg_signal_input_axis_tready
	);
	
	--DSP_COINC_LEVEL <= dsp_noise_reduction_filter(REGISTER_W8-1 downto 0);
	DSP_COINC_LEVEL <= coinc_level_result(REGISTER_W8-1 downto 0);
	TRANSMITTER_N_BURST <= transmitter_n_burst_i;																			
																																							
	-- AVERAGE FILTER
	average_filter_inst: average_filter
	generic map(
        REGISTER_W32    => REGISTER_W32, 
        REGISTER_W16    => REGISTER_W16,
        REGISTER_W8     => REGISTER_W8,
        REGISTER_W14    => REGISTER_W14,
		AVG_TYPE_NORMAL => AVG_TYPE_NORMAL,
		AVG_TYPE_PIPELINE  => AVG_TYPE_PIPELINE,
        TRIGGER_WAIT    => TRIGGER_WAIT,
        MIN_AVG_LEVEL   => MIN_AVG_LEVEL
	)
	port map(
	    -- Sync
	    aclk         => aclk,
        aresetn      => aresetn,
	
	    -- Config
	    AVG_LEVEL	    	=> dsp_average,
	    DATA_WINDOW	    	=> coinc_data_window_result,
	    SAMPLE_FREQ         => coinc_sample_freq_result,
		DATA_WINDOW_RESULT	=> avg_data_window_result,
		SAMPLE_FREQ_RESULT  => avg_sample_freq_result,
		AVG_COUNTER         => avg_counter, 
		AVG_WINDOW			=> SIGNAL_AVG_WINDOW, 
		
		AVG_TYPE			=> avg_type,
		AVG_TYPE_RESULT		=> avg_type_result,
		AVG_LEVEL_RESULT    => avg_level_result,
		AVG_RESET			=> avg_reset, 
		AVG_RESET_ACK		=> avg_reset_ack, 
		
		-- Signal input
		SIGNAL_INPUT_AXIS_TREADY  => avg_signal_input_axis_tready,
		SIGNAL_INPUT_AXIS_TDATA	  => coinc_result_axis_tdata,
		SIGNAL_INPUT_AXIS_TLAST	  => coinc_result_axis_tlast,
		SIGNAL_INPUT_AXIS_TVALID  => coinc_result_axis_tvalid,

		-- Average result
		AVG_RESULT_AXIS_TVALID    => avg_result_axis_tvalid,
		AVG_RESULT_AXIS_TDATA	  => avg_result_axis_tdata,
		AVG_RESULT_AXIS_TLAST	  => avg_result_axis_tlast,
		AVG_RESULT_AXIS_TREADY    => fir_signal_input_axis_tready_i
	);
																
	--DSP_AVG_LEVEL <= dsp_average(REGISTER_W8-1 downto 0);	
	DSP_AVG_LEVEL <= avg_level_result(REGISTER_W8-1 downto 0);																			
	SIGNAL_AVG_COUNTER <= avg_counter;
	DSP_AVG_TYPE <= avg_type_result;
	
    avg_reset_process: process(aclk)
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                avg_reset <= '0';
                start_inspection_d1 <= '0';
                start_stop_inspection_d1 <= '0';
                scan_mode_d1 <= (others => '0');
                trigger_type_d1 <= (others => '0');
            else
                start_inspection_d1 <= start_inspection;
                start_stop_inspection_d1 <= start_stop_inspection;
                scan_mode_d1 <= scan_mode;
                trigger_type_d1 <= trigger_type;
                if((start_inspection /= start_inspection_d1) or 
                   (start_stop_inspection /= start_stop_inspection_d1) or
                   (scan_mode /= scan_mode_d1) or
                   (trigger_type /= trigger_type_d1)
                  ) then
                    avg_reset <= '1';
                else
                    if(avg_reset = avg_reset_ack) then
                        avg_reset <= '0';    
                    end if;
                end if;     
            end if;
        end if;
    end process;														
		
	-- FIR FILTER + CORRELTION
	fir_correlation_inst: fir_correlation
	generic map(
	    REGISTER_W48        => REGISTER_W48,
        REGISTER_W32        => REGISTER_W32,
        REGISTER_W16        => REGISTER_W16,
        N_TAPS              => N_TAPS,
        N_SETS_TAP          => N_SETS_TAP,
        OVERSAMPLING        => OVERSAMPLING,
        WAIT_CYCLES         => WAIT_CYCLES
    )
    port map(
        aclk         => aclk,
        aresetn      => aresetn,
        
		NO_READ_FIR_RESULT			=> no_read_fir_result,
        READ_FIR_RESULT             => read_fir_result,
        DATA_WINDOW                 => avg_data_window_result,
        SAMPLE_FREQ                 => avg_sample_freq_result,
        DATA_WINDOW_RESULT          => fir_data_window_result,
        SAMPLE_FREQ_RESULT          => fir_sample_freq_result,
		DATA_WINDOW_HALF_RESULT	    => fir_data_window_half_result,
		FIR_END                     => fir_end,
        
		-- FIR config
        BAND_PASS_FILTER_EN         => dsp_bandpass_en,
        BAND_PASS_FILTER_L_DIVIDER  => dsp_bandpass_l,
        GAUSSIAN_FILTER_EN          => dsp_gaussian_en,
        GAUSSIAN_FILTER_L_DIVIDER   => dsp_gaussian_l,
        ENVELOPE_FILTER_EN          => dsp_envelope_en,
        ENVELOPE_FILTER_L_DIVIDER   => dsp_envelope_l,
        
        FIR_TAPS_AXIS_TREADY        => fir_taps_axis_tready,
        FIR_TAPS_AXIS_TDATA         => config_fir_axis_tdata,
        FIR_TAPS_AXIS_TLAST         => config_fir_axis_tlast,
        FIR_TAPS_AXIS_TVALID        => config_fir_axis_tvalid,
     
		-- Signal Input
        SIGNAL_INPUT_AXIS_TREADY    => fir_signal_input_axis_tready,
        SIGNAL_INPUT_AXIS_TDATA     => avg_result_axis_tdata,
        SIGNAL_INPUT_AXIS_TLAST     => avg_result_axis_tlast,
        SIGNAL_INPUT_AXIS_TVALID    => avg_result_axis_tvalid,
        
		-- Gates Config
        GATES_EN                    => gates_en,
        GATES_SAMPLE_START          => gates_sample_start,
        GATES_SAMPLE_END            => gates_sample_end,
        
		-- Gates Result
        GATES_AXIS_TVALID           => gates_axis_tvalid,
        GATES_AXIS_TDATA            => gates_axis_tdata,
        GATES_AXIS_TLAST            => gates_axis_tlast,
        GATES_AXIS_TREADY           => gates_axis_tready,
        
		-- Correlation Config
        CORRELATION_EN                  => correlation_en,
        CORRELATION_GATE_SAMPLE_START   => correlation_gate_sample_start,
        CORRELATION_GATE_SAMPLE_END     => correlation_gate_sample_end,
        
		-- Correlation Result
        CORRELATION_AXIS_TVALID     => correlation_axis_tvalid,
        CORRELATION_AXIS_TDATA      => correlation_axis_tdata,
        CORRELATION_AXIS_TLAST      => correlation_axis_tlast,
        CORRELATION_AXIS_TREADY     => correlation_axis_tready,

		-- FIR Result
        FIR_RESULT_AXIS_TVALID      => fir_result_axis_tvalid,
        FIR_RESULT_AXIS_TDATA       => fir_result_axis_tdata,
        FIR_RESULT_AXIS_TLAST       => fir_result_axis_tlast,
        FIR_RESULT_AXIS_TREADY      => fir_result_axis_tready_i
    );
    
    -- Debug
    correlation_counter_inst: process(aclk) 
        variable var_counter : integer range 0 to 65536 :=0;
    begin 
        if (rising_edge(aclk)) then
            if (aresetn = '0') then
                correlation_counter <= (others => '0'); 
            else
                if(CORRELATION_AXIS_TVALID = '1') then 
                    if(CORRELATION_AXIS_TLAST = '1') then 
                        correlation_counter <= (others => '0'); 
                    else
                        correlation_counter <= correlation_counter + 1;
                    end if;
                end if;
            end if;
        end if;
    end process;
	
	-- GATES PROCESSOR
	gates_processor_inst: gates_processor 
	generic map(
		REGISTER_W64  => REGISTER_W64,
		REGISTER_W48  => REGISTER_W48,
        REGISTER_W32  => REGISTER_W32,
        REGISTER_W16  => REGISTER_W16,
        REGISTER_W8   => REGISTER_W8,
        REGISTER_W4   => REGISTER_W4,
        REGISTER_W2   => REGISTER_W2
	)
	port map(
	    --sync
	    aclk           => aclk,
        aresetn        => aresetn,  
	    
	    -- GENERAL    
	    RX_DELAY       => receiver_delay_i,   

        -- GATE1
		GATE1_EN                          => gate1_en,  
		GATE1_START                       => gate1_start,
		GATE1_WIDTH                       => gate1_width,
        GATE1_AMP_THRESHOLD               => gate1_amp_threshold,
        GATE1_AMP_THRESHOLD_ALARM_EN      => gate1_amp_threshold_alarm_en,
        GATE1_AMP_THRESHOLD_ALARM_CROSSING=> gate1_amp_threshold_alarm_crossing,
        GATE1_TOF_ALGORITHM               => gate1_tof_algorithm,
        GATE1_TOF_AVG                     => gate1_tof_avg,
        GATE1_TOF_MIN_THICKNESS           => gate1_tof_min_thickness,
        GATE1_TOF_ALARM_EN                => gate1_tof_alarm_en,
        GATE1_TOF_ALARM_TYPE              => gate1_tof_alarm_type,
        GATE1_TOF_ALARM_CROSSING          => gate1_tof_alarm_crossing,
        GATE1_TOF_ALARM_MIN_VALUE         => gate1_tof_alarm_min_value,
        GATE1_TOF_ALARM_MAX_VALUE         => gate1_tof_alarm_max_value,
        GATE1_TOF                         => gate1_tof,
        GATE1_TOF_AVERAGED                => gate1_tof_averaged,
        GATE1_AMP_ALARM                   => gate1_amp_alarm,
        GATE1_TOF_ALARM                   => gate1_tof_alarm,
        GATE1_MAX                         => gate1_max,
        GATE1_MAX_SAMPLE                  => gate1_max_sample,
        GATE1_MIN                         => gate1_min,
        GATE1_MIN_SAMPLE                  => gate1_min_sample,
        
        -- GATE2
		GATE2_EN                          => gate2_en,  
        GATE2_START                       => gate2_start,
        GATE2_WIDTH                       => gate2_width,
        GATE2_AMP_THRESHOLD               => gate2_amp_threshold,
        GATE2_AMP_THRESHOLD_ALARM_EN      => gate2_amp_threshold_alarm_en,
        GATE2_AMP_THRESHOLD_ALARM_CROSSING=> gate2_amp_threshold_alarm_crossing,
        GATE2_TOF_ALGORITHM               => gate2_tof_algorithm,
        GATE2_TOF_AVG                     => gate2_tof_avg,
        GATE2_TOF_MIN_THICKNESS           => gate2_tof_min_thickness,
        GATE2_TOF_ALARM_EN                => gate2_tof_alarm_en,
        GATE2_TOF_ALARM_TYPE              => gate2_tof_alarm_type,
        GATE2_TOF_ALARM_CROSSING          => gate2_tof_alarm_crossing,
        GATE2_TOF_ALARM_MIN_VALUE         => gate2_tof_alarm_min_value,
        GATE2_TOF_ALARM_MAX_VALUE         => gate2_tof_alarm_max_value,
        GATE2_TOF                         => gate2_tof,
        GATE2_TOF_AVERAGED                => gate2_tof_averaged,
        GATE2_AMP_ALARM                   => gate2_amp_alarm,
        GATE2_TOF_ALARM                   => gate2_tof_alarm,
        GATE2_MAX                         => gate2_max,
        GATE2_MAX_SAMPLE                  => gate2_max_sample,
        GATE2_MIN                         => gate2_min,
        GATE2_MIN_SAMPLE                  => gate2_min_sample,
        
        -- GATE3
		GATE3_EN                          => gate3_en,  
        GATE3_START                       => gate3_start,
        GATE3_WIDTH                       => gate3_width,
        GATE3_AMP_THRESHOLD               => gate3_amp_threshold,
        GATE3_AMP_THRESHOLD_ALARM_EN      => gate3_amp_threshold_alarm_en,
        GATE3_AMP_THRESHOLD_ALARM_CROSSING=> gate3_amp_threshold_alarm_crossing,
        GATE3_TOF_ALGORITHM               => gate3_tof_algorithm,
        GATE3_TOF_AVG                     => gate3_tof_avg,
        GATE3_TOF_MIN_THICKNESS           => gate3_tof_min_thickness,
        GATE3_TOF_ALARM_EN                => gate3_tof_alarm_en,
        GATE3_TOF_ALARM_TYPE              => gate3_tof_alarm_type,
        GATE3_TOF_ALARM_CROSSING          => gate3_tof_alarm_crossing,
        GATE3_TOF_ALARM_MIN_VALUE         => gate3_tof_alarm_min_value,
        GATE3_TOF_ALARM_MAX_VALUE         => gate3_tof_alarm_max_value,
        GATE3_TOF                         => gate3_tof,
        GATE3_TOF_AVERAGED                => gate3_tof_averaged,
        GATE3_AMP_ALARM                   => gate3_amp_alarm,
        GATE3_TOF_ALARM                   => gate3_tof_alarm,
        GATE3_MAX                         => gate3_max,
        GATE3_MAX_SAMPLE                  => gate3_max_sample,
        GATE3_MIN                         => gate3_min,
        GATE3_MIN_SAMPLE                  => gate3_min_sample,

		--GATES DATA
		GATES_CONFIG      => config_update_gates,
		GATES_SAMPLE_START=> gates_sample_start,
        GATES_SAMPLE_END  => gates_sample_end,
        GATES_EN          => gates_en,
        GATES_END         => gates_end,
        READ_GATES_RESULT => read_gates_result,
        
		GATES_AXIS_TREADY => gates_axis_tready,
		GATES_AXIS_TDATA  => gates_axis_tdata,
		GATES_AXIS_TLAST  => gates_axis_tlast,
		GATES_AXIS_TVALID => gates_axis_tvalid,
		
		-- CORRELATION
        CORRELATION_EN              	=> correlation_en,
        CORRELATION_GATE_SAMPLE_START   => correlation_gate_sample_start,
        CORRELATION_GATE_SAMPLE_END     => correlation_gate_sample_end,
        CORRELATION_AXIS_TREADY     	=> correlation_axis_tready,
        CORRELATION_AXIS_TDATA      	=> correlation_axis_tdata,
        CORRELATION_AXIS_TLAST      	=> correlation_axis_tlast,
        CORRELATION_AXIS_TVALID     	=> correlation_axis_tvalid
	);
		
	RECEIVER_DELAY <= receiver_delay_i;
	RECEIVER_DATA_WINDOW <= receiver_data_window_i;
	RECEIVER_SAMPLING_FREQUENCY <= receiver_sampling_frequency_i;
	RECEIVER_DIGITAL_GAIN <= receiver_digital_gain_i;	
																
	-- CHANNEL CONFIGURATION	
	channel_configuration_inst: channel_configuration
	generic map(
		CHANNEL_ID		=> CHANNEL_ID,
        REGISTER_W32    => REGISTER_W32,
        REGISTER_W16    => REGISTER_W16,
		REGISTER_W8     => REGISTER_W8,
		REGISTER_W4     => REGISTER_W4
	)
	port map(
	    -- Sync
	    aclk         => aclk, 
        aresetn      => aresetn,
		
		-- Control
		CONFIG_UPDATE_RECEIVER  	=> config_update_receiver,
		CONFIG_NEW_RECEIVER   		=> config_new_receiver,
		
		CONFIG_UPDATE_TRANSMITTER  	=> config_update_transmitter,
		CONFIG_NEW_TRANSMITTER  	=> config_new_transmitter,
		
		CONFIG_UPDATE_MAGNET 	 	=> config_update_magnet,
		CONFIG_NEW_MAGNET  			=> config_new_magnet,
		
		CONFIG_UPDATE_DSP_1  		=> config_update_dsp_1,
		CONFIG_NEW_DSP_1  			=> config_new_dsp_1,
		
		CONFIG_UPDATE_DSP_2  		=> config_update_dsp_2,
		CONFIG_NEW_DSP_2  			=> config_new_dsp_2,
		
		CONFIG_UPDATE_FIR  	 		=> config_update_fir,
		CONFIG_NEW_FIR  			=> config_new_fir,
		
		CONFIG_UPDATE_GATES  		=> config_update_gates,
		CONFIG_NEW_GATES  			=> config_new_gates,
		
		CONFIG_UPDATE_DAC  	 		=> config_update_dac,
		CONFIG_NEW_DAC  			=> config_new_dac,
		
		THERE_IS_CONFIGURATION		=> there_is_configuration,
		
		-- Channel enable
		CHANNEL_EN   	=> channel_en,
	
		-- Config input
		CONFIG_INPUT_LOAD	  		=> CONFIG_INPUT_LOAD,
		CONFIG_INPUT_AXIS_TREADY 	=> CONFIG_INPUT_AXIS_TREADY,
		CONFIG_INPUT_AXIS_TDATA	  	=> CONFIG_INPUT_AXIS_TDATA,
		CONFIG_INPUT_AXIS_TLAST	  	=> CONFIG_INPUT_AXIS_TLAST,
		CONFIG_INPUT_AXIS_TVALID  	=> CONFIG_INPUT_AXIS_TVALID,
		
		-- Config output receiver
		RECEIVER_SAMPLING_FREQUENCY         => receiver_sampling_frequency_i,  
        RECEIVER_DATA_WINDOW                => receiver_data_window_i, 
        RECEIVER_DELAY                      => receiver_delay_i,
        RECEIVER_ANALOG_GAIN                => RECEIVER_ANALOG_GAIN, 
        RECEIVER_DIGITAL_GAIN               => receiver_digital_gain_i,
        RECEIVER_EXTERNAL_MULTIPLEXER_EN    => RECEIVER_EXTERNAL_MULTIPLEXER_EN,
		
		-- Config output transmitter
		TRANSMITTER_VOLTAGE                 => TRANSMITTER_VOLTAGE,  
        TRANSMITTER_BURST_FREQUENCY         => transmitter_burst_frequency_i, 
        TRANSMITTER_N_CYCLES                => TRANSMITTER_N_CYCLES, 
        TRANSMITTER_N_BURST                 => transmitter_n_burst_i,
        TRANSMITTER_PRF                     => transmitter_prf,
        TRANSMITTER_DELAY                   => transmitter_delay_i,
        TRANSMITTER_DELTA_DELAY             => TRANSMITTER_DELTA_DELAY,
        TRANSMITTER_DELTA_ADD               => TRANSMITTER_DELTA_ADD,
        TRANSMITTER_DIRECTIONAL_PHASING     => TRANSMITTER_DIRECTIONAL_PHASING, 
        TRANSMITTER_PHASE_SHIFT             => transmitter_phase_shift,
		
		-- Config output magnet
		MAGNET_MODE                         => MAGNET_MODE,
        MAGNET_PULSE_WIDTH                  => MAGNET_PULSE_WIDTH,
        MAGNET_INITIAL_DELAY                => MAGNET_INITIAL_DELAY, 
        MAGNET_RAMP_UP_VOLTAGE              => MAGNET_RAMP_UP_VOLTAGE,
        MAGNET_VOLTAGE                      => MAGNET_VOLTAGE,         
		
		-- Config output DSP
		DSP_AVERAGE                         => dsp_average,
        DSP_NOISE_REDUCTION_FILTER          => dsp_noise_reduction_filter,  
        DSP_PHASE_SHIFT                     => dsp_phase_shift,
        DSP_BANDPASS_EN  					=> dsp_bandpass_en,
		DSP_BANDPASS_N_TAPS  				=> dsp_bandpass_n_taps,
		DSP_BANDPASS_L  					=> dsp_bandpass_l,
        DSP_GAUSSIAN_EN  					=> dsp_gaussian_en,
		DSP_GAUSSIAN_N_TAPS  				=> dsp_gaussian_n_taps,
		DSP_GAUSSIAN_L  					=> dsp_gaussian_l,
		DSP_ENVELOPE_EN  					=> dsp_envelope_en,
		DSP_ENVELOPE_N_TAPS  				=> dsp_envelope_n_taps,
		DSP_ENVELOPE_L  					=> dsp_envelope_l,
		DSP_ANALOG_FILTER					=> DSP_ANALOG_FILTER,
				
		-- Config output Gates
		GATE1_EN                          => gate1_en,  
		GATE1_START                       => gate1_start,
		GATE1_WIDTH                       => gate1_width,	
        GATE1_AMP_THRESHOLD               => gate1_amp_threshold,
		GATE1_TOF_ALGORITHM               => gate1_tof_algorithm,
		GATE1_TOF_AVG                     => gate1_tof_avg,
		GATE1_TOF_MIN_THICKNESS           => gate1_tof_min_thickness,
        GATE1_AMP_THRESHOLD_ALARM_EN      => gate1_amp_threshold_alarm_en,
        GATE1_AMP_THRESHOLD_ALARM_CROSSING=> gate1_amp_threshold_alarm_crossing,
        GATE1_TOF_ALARM_EN                => gate1_tof_alarm_en,
        GATE1_TOF_ALARM_TYPE              => gate1_tof_alarm_type,
        GATE1_TOF_ALARM_CROSSING          => gate1_tof_alarm_crossing,
        GATE1_TOF_ALARM_MIN_VALUE         => gate1_tof_alarm_min_value,
        GATE1_TOF_ALARM_MAX_VALUE         => gate1_tof_alarm_max_value,	
		
		GATE2_EN                          => gate2_en,  
        GATE2_START                       => gate2_start,
        GATE2_WIDTH                       => gate2_width,    
        GATE2_AMP_THRESHOLD               => gate2_amp_threshold,
        GATE2_TOF_ALGORITHM               => gate2_tof_algorithm,
        GATE2_TOF_AVG                     => gate2_tof_avg,
        GATE2_TOF_MIN_THICKNESS           => gate2_tof_min_thickness,
        GATE2_AMP_THRESHOLD_ALARM_EN      => gate2_amp_threshold_alarm_en,
        GATE2_AMP_THRESHOLD_ALARM_CROSSING=> gate2_amp_threshold_alarm_crossing,
        GATE2_TOF_ALARM_EN                => gate2_tof_alarm_en,
        GATE2_TOF_ALARM_TYPE              => gate2_tof_alarm_type,
        GATE2_TOF_ALARM_CROSSING          => gate2_tof_alarm_crossing,
        GATE2_TOF_ALARM_MIN_VALUE         => gate2_tof_alarm_min_value,
        GATE2_TOF_ALARM_MAX_VALUE         => gate2_tof_alarm_max_value,    
		
		GATE3_EN                          => gate3_en,  
        GATE3_START                       => gate3_start,
        GATE3_WIDTH                       => gate3_width,    
        GATE3_AMP_THRESHOLD               => gate3_amp_threshold,
        GATE3_TOF_ALGORITHM               => gate3_tof_algorithm,
        GATE3_TOF_AVG                     => gate3_tof_avg,
        GATE3_TOF_MIN_THICKNESS           => gate3_tof_min_thickness,
        GATE3_AMP_THRESHOLD_ALARM_EN      => gate3_amp_threshold_alarm_en,
        GATE3_AMP_THRESHOLD_ALARM_CROSSING=> gate3_amp_threshold_alarm_crossing,
        GATE3_TOF_ALARM_EN                => gate3_tof_alarm_en,
        GATE3_TOF_ALARM_TYPE              => gate3_tof_alarm_type,
        GATE3_TOF_ALARM_CROSSING          => gate3_tof_alarm_crossing,
        GATE3_TOF_ALARM_MIN_VALUE         => gate3_tof_alarm_min_value,
        GATE3_TOF_ALARM_MAX_VALUE         => gate3_tof_alarm_max_value,    
		
		-- Config output FIR
		CONFIG_FIR_AXIS_TREADY    => fir_taps_axis_tready,
		CONFIG_FIR_AXIS_TDATA	  => config_fir_axis_tdata,
		CONFIG_FIR_AXIS_TLAST	  => config_fir_axis_tlast,
		CONFIG_FIR_AXIS_TVALID    => config_fir_axis_tvalid,
		
		-- DAC output
		CONFIG_DAC_AXIS_TREADY    => CONFIG_DAC_AXIS_TREADY,
		CONFIG_DAC_AXIS_TDATA	  => CONFIG_DAC_AXIS_TDATA,
		CONFIG_DAC_AXIS_TLAST	  => CONFIG_DAC_AXIS_TLAST,
		CONFIG_DAC_AXIS_TVALID    => CONFIG_DAC_AXIS_TVALID
	);	
		           
end arch_imp;
